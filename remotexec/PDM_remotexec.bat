echo off
Rem ------------------------------------------------------------------------------------
Rem Loop script for PDM distribution and remote installation control (PDM_remotexec.bat)
Rem INPUT PARAMETERS
Rem   none
Rem ------------------------------------------------------------------------------------

Rem
Rem Initialization
setlocal
set l_retstat=0
Rem
Rem Set working queue directory and parameter file
FOR /F "usebackq delims=="  %%i IN (`echo %0`)  DO set l_scriptdir=%%~dpi
call :initialize
if %errorlevel% NEQ 0  exit /b %errorlevel%


Rem **************************************************************************************************
Rem Main loop
Rem **************************************************************************************************
set /a l_ccnt=0
set l_activetiming=0
echo (%date% %time%) INFO: start checking action folder %l_actiondir% at distribution host %COMPUTERNAME%
if exist "%l_actiondir%\ACTIVETIMING.DO" goto else_nodelat
  del /f /q "%l_actiondir%\*.*"
  goto endif_nodelat
:else_nodelat
  del /f /q "%l_actiondir%\*.*"
  echo true>"%l_actiondir%\ACTIVETIMING.DO"
:endif_nodelat

:loop_run
  rem %Vecho% "LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOP"
  rem pause
  
  Rem
  Rem Setup timing values
  if exist "%l_actiondir%\ACTIVETIMING.DO" goto else_activetiming
    if "%l_activetiming%" == "1" echo (%date% %time%) INFO: switch from active timing to sleep timing
    set l_cyclewait=300
    rem Feedback after every 8 hours in non-active (sleep) mode
    set l_cyclefeedbackcount=96
    set l_activetiming=0
    goto endif_activetiming
  :else_activetiming
    if "%l_activetiming%" == "0" echo (%date% %time%) INFO: switch from sleep timing to active timing
    set l_cyclewait=10
    rem Feedback after every 5 minutes in active mode
    set l_cyclefeedbackcount=30
    set l_activetiming=1
  :endif_activetiming

  Rem
  Rem Loop exit handling (stop 'remote execution' of actions)
  if exist "%l_actiondir%\STOP.DO" goto end_loop_run
  
  Rem
  Rem Check for incoming PING
  if not exist "%l_actiondir%\PING.IN" goto endif_ping
    echo (%date% %time%) INFO: received ping, sending reply
    if exist "%l_actiondir%\PING.OUT" del /f /q "%l_actiondir%\PING.OUT" 
    rename "%l_actiondir%\PING.IN" "PING.OUT" 
  :endif_ping

  Rem
  Rem Check for incoming actions
  if not exist "%l_actiondir%\ACTION.DO" goto endif_dact
    call :process_action
  :endif_dact

  Rem
  Rem Now wait for %l_cyclewait% seconds
  %sleep% %l_cyclewait%
  Rem if "%l_activetiming%" == "1" echo END-WAIT
  
  Rem
  Rem Write a log message every hour
  set /a l_ccnt=%l_ccnt%+1
  if %l_ccnt% LSS %l_cyclefeedbackcount% goto endif_clog
    echo (%date% %time%) INFO: still cycle/wait for actions at distribution host %COMPUTERNAME%
    set /a l_ccnt=0
  :endif_clog

  Rem
  Rem And return to loop begin
  goto loop_run

:end_loop_run
Rem
Rem Stop processing (looping) requested
echo (%date% %time%) INFO: PDM remote loop executer stopped
del /F /Q "%l_actiondir%\STOP.DO"
exit /b %l_retstat%



Rem **************************************************************************************************
Rem SUBPROCEDURE  to initialize
Rem
Rem 'l_scriptdir' must be set to define the (main) script location
Rem
Rem Output variables!!! NO VARIABLE LOCALIZATION!
Rem **************************************************************************************************
:initialize
  set l_retstattmp=0
  echo (%date% %time%) Initialize...

  Rem
  Rem Setup remotexec/softwarestore refs.
  set l_basedir=%l_scriptdir%
  set ant=%l_basedir%\ant\bin\ant.bat
  set l_actiondir=%l_basedir%\action
  set l_logdir=%l_basedir%\log
  set java_home=%l_basedir%\jdk
  set l_softwarestore=%l_basedir%\..\PDMSoftwareStore
  set l_antscript=%l_softwarestore%\resources\InstallAgents.xml
  set l_stillalivefile=%l_actiondir%\STILL.ALIVE

  Rem
  Rem If 'sleep' program is available locally, wait before continuing to let the network startup at system startup time to prevent network errors at initialization
  if not exist sleep.exe goto endif_inisleep
    set l_sustime=60
    if exist "%l_actiondir%\ACTIVETIMING.DO" set l_sustime=1
    echo waiting for network to startup...
    sleep %l_sustime%
  :endif_inisleep

  Rem
  Rem Common parameters (check at least command 'sleep' after call)
  call \\planon-fm.com\dev\DevSup\Toolkit\etc\setcommonparameters 2>&1
  if defined sleep if exist "%sleep%" goto endif_callscpok
    set l_retstattmp=1
    echo ERROR: setting common parameters (setcommonparameters.bat) failed (command 'sleep' not found)
    goto end_initialize
  :endif_callscpok
  Rem

  Rem
  Rem Copy sleep program and 'redirect command' to local version (to avoid network problems in sleep functionality)
  if exist sleep.exe goto endif_copsleep
    copy /y %sleep% .
    if %errorlevel% == 0 goto endif_coplocslp
      set l_retstattmp=1
      echo ERROR: setup local copy of sleep failed at PDM_remotexec loop initialization
    :endif_coplocslp
  :endif_copsleep
  for /f %%i in ('dir /b %sleep%') do set sleep=%%~fi

:end_initialize
  Rem And return to caller: No 'endlocal' due to NO VARIABLE LOCALIZATION!
  exit /b %l_retstattmp%


Rem **************************************************************************************************
Rem SUBPROCEDURE  to process any action from remote
Rem **************************************************************************************************
:process_action
  setlocal
  set l_retstat=0

  Rem get action line
  for /f "tokens=1*" %%i in (%l_actiondir%\ACTION.DO) do set l_actionline=%%i %%j
  echo (%date% %time%) INFO: incoming action:
  echo   (%l_actionline%)
  del %l_actiondir%\ACTION.DO
  
  Rem clean log folder before execution
  del /Q /F %l_logdir%\*.*
  
  Rem
  Rem Execute the action and save log
  call :do_action >%l_logdir%\action.log 2>&1
  Rem Handle the return status
  set l_retstat=%errorlevel%
  echo (%date% %time%) INFO: action execution return status: %l_retstat%
  Rem Save status to file (space required after status variable/before redirection to let it work)
  echo l_retstat=%l_retstat% >%l_actiondir%\ACTION.RETURN
  Rem Finaly generate the 'still alive' file
  echo (%date% %time%) >%l_stillalivefile%
  
:end_process_action  
  Rem And return to caller: do not 'endlocal' to be able to return the status
  exit /b %l_retstat%


Rem **************************************************************************************************
Rem SUBPROCEDURE  to actual perform the action as part of the PDM distribution (ant) script
Rem **************************************************************************************************
:do_action
  setlocal
  set l_retstat=0

  echo (%date% %time%) INFO: executing: %ant% -f %l_antscript% %l_actionline% -Dstillalivefile=%l_stillalivefile%
  call %ant% -f %l_antscript% %l_actionline% -Dstillalivefile=%l_stillalivefile%
  set l_retstat=%errorlevel%

:end_do_action  
  Rem And return to caller: do not 'endlocal' to be able to return the status
  exit /b %l_retstat%
