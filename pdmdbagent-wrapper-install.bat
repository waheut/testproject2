echo off
Rem ================================================================================================
Rem pdmdbagent-wrapper-install
Rem PDM-agent Tanuki service (re)creation (and remove) script
Rem ================================================================================================
Rem This script needs to be located in the root folder where the tanuki home folder residents
Rem The script can only be executed on the intended host  (local execution!)
Rem   Create/install:
Rem     Just call this script (no parameters,  or p1=nopause to return without pause)
Rem   Remove:
Rem     Call this script, parameter 1 = remove (opt.: p2=nopause to return without pause)
Rem   Recreate:  (first remove and then (re)create with latest configuration)
Rem     Call this script, parameter 1 = recreate (opt.: p2=nopause to return without pause)
Rem ================================================================================================
setlocal
Rem Set specifics  (need to be filled in at distribution / per agent)
set l_conffilename=[TANUKI_CONF_FILENAME]
set l_intendedhost=[AGENT_HOST]

Rem
Rem Validate that we are running at the local (intended) computer for the service creation
if /i "%COMPUTERNAME%" NEQ "%l_intendedhost%" goto else_invcomp

  Rem Set script directory (root of tanuki home)
  FOR /F "usebackq delims=="  %%i IN (`echo %0`)  DO set l_tanukiroot=%%~dpi
  set l_tanukihome=%l_tanukiroot%\tanuki
  echo INFO: tanuki home: (%l_tanukihome%)

  if not exist "%l_tanukihome%\logs" mkdir "%l_tanukihome%\logs"
  set l_operation=i
  if /i "%1" == "remove" set l_operation=r
  if /i "%1" == "recreate" set l_operation=r
  "%l_tanukihome%\bin\wrapper" -%l_operation% "%l_tanukihome%\conf\%l_conffilename%"
  set l_retstat=%errorlevel%
  if "%l_retstat%" == "0"  echo INFO: PDM-agent Tanuki service operation '%l_operation%' succeeded
  if "%l_retstat%" NEQ "0" echo INFO: PDM-agent Tanuki service operation '%l_operation%' failed, status = %l_retstat%
  
  if /i "%1" NEQ "recreate" goto endif_recreate
    set l_operation=i
    "%l_tanukihome%\bin\wrapper" -%l_operation% "%l_tanukihome%\conf\%l_conffilename%"
    set l_retstat=%errorlevel%
    if "%l_retstat%" == "0"  echo INFO: PDM-agent Tanuki service operation '%l_operation%' succeeded
    if "%l_retstat%" NEQ "0" echo INFO: PDM-agent Tanuki service operation '%l_operation%' failed, status = %l_retstat%
  :endif_recreate

  goto endif_invcomp
:else_invcomp
  echo ERROR: PDM-agent Tanuki service cannot be (re)created/removed from another host (needs host %l_intendedhost%)
  set l_retstat=1
:endif_invcomp
Rem terminate
if /i "%1" == "nopause" goto nopause
if /i "%2" == "nopause" goto nopause
  pause
:nopause
exit /b %l_retstat%
