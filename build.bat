@echo off
Rem --------------------------------------------------------------------------------
Rem New BUILD.BAT for Planon5
Rem based upon true Ant structure (since 1.7.0/nov 2010) incl. ant.bat
Rem --------------------------------------------------------------------------------

SETLOCAL

Rem Get script location (this Planon5 sandbox)
FOR /F "usebackq delims=="  %%i IN (`echo %0`)  DO set l_scriptdir=%%~dpi

Rem First check for REQUIRED localenv 'jdk' directory
if exist %l_scriptdir%localenv\jdk goto endif_jdkok
  echo ERROR: missing JDK at %l_scriptdir%localenv - please execute SFProjectBootstrap.bat within your sandbox; then try again
  set l_retstat=1
  goto end
:endif_jdkok
Rem Then setup JAVA_HOME to the current JDK
set JAVA_HOME=%l_scriptdir%\localenv\jdk

Rem First check for REQUIRED localenv 'ant' directory
if exist %l_scriptdir%localenv\ant goto endif_antok
  echo ERROR: missing ANT at %l_scriptdir%localenv - please execute SFProjectBootstrap.bat within your sandbox; then try again
  set l_retstat=1
  goto end
:endif_antok
Rem Then setup ANT_HOME to the current Ant
set ANT_HOME=%l_scriptdir%localenv\ant

Rem Setup Java/Ant options
set ANT_OPTS=%ANT_OPTS% -Dbuild.script=build.bat
set ANT_OPTS=%ANT_OPTS% -ea
set ANT_OPTS=%ANT_OPTS% -Xmx768m

Rem Setup Ant arguments
rem set ANT_ARGS=%ANT_ARGS%

Rem Setup (clear) Ant classpath
set CLASSPATH=
Rem Set junit library into Ant classpath
set CLASSPATH=%l_scriptdir%lib\junit\junit.jar

@rem prevent java.nio.BufferOverflowException by settting LANG=nl
set LANG=nl

Rem Setup Ant command line arguments
:setupArgs
if %1a==a goto doneStart
set OTHER_ARGS=%OTHER_ARGS% %1
shift
goto setupArgs

:doneStart

call %ANT_HOME%\bin\ant.bat %OTHER_ARGS%
set l_retstat=%errorlevel%

:end
exit /b %l_retstat%
