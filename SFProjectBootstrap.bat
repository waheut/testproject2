echo off
Rem --------------------------------------------------------------------------------------------------------
Rem REROUTE SCRIPT FOR:
Rem SOFTWAREFACTORY PROJECT PLATFORM BOOTSTRAP SCRIPT
Rem     SFProjectBootstrapInit.bat
Rem -------------------------------------------------------------------------------------------------------
Rem
setlocal
Rem Setup basic common parameters
if exist \\planon-fm.com\Devsup\Toolkit\etc\setcommonparameters.bat goto endif_comparok
  echo ERROR: REROUTE(SFProjectBootstrap): setcommonparameters.bat not found
  exit /b 1
:endif_comparok
call \\planon-fm.com\Devsup\Toolkit\etc\setcommonparameters.bat

if defined sf_ProjectBootstrapInit goto endif_sfpbok
  echo ERROR: REROUTE(SFProjectBootstrap): 'sf_ProjectBootstrapInit' not set in setcommonparameters.bat
  exit /b 1
:endif_sfpbok

Rem Setup target directory as 'this directory' (script location)
FOR /F "usebackq delims=="  %%i IN (`echo %0`)  DO set l_targetdir=%%~dpi
Rem And call SFProjectBootstrapInit script using target dir as input
call %sf_ProjectBootstrapInit% "%l_targetdir%" "" PauseOnError
set l_retstat=%errorlevel%
if %l_retstat% ==0 goto endif_errcall
  echo ERROR: REROUTE(SFProjectBootstrap): error returned from 'sf_ProjectBootstrapInit' call
  exit /b %l_retstat%
:endif_errcall
exit /b 0
