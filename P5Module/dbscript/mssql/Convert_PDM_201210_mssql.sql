/*--------------------------------------------------------------------------------------------------------------*/
/*~!~Section, Nr=0001,  Desc = 'PBL  15725,  Rollout PDM3: Support data type NVARCHAR'                          */
/*--------------------------------------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No                                                    */
/*~!~Step, Desc = 'add temporary procedure to drop default constraints'               */
/*------------------------------------------------------------------------------------*/
create procedure temp_drop_default_constraint
  @P_tablename varchar(30),
  @P_columnname varchar(30)
as
 begin
  declare @l_default_constraint VarChar(100);
  declare @l_statement VarChar(100);

  Select @l_default_constraint = c.name from sys.all_columns a 
      inner join sys.tables b              on a.object_id = b.object_id
      inner join sys.default_constraints c on a.default_object_id = c.object_id
  where b.name = @P_tablename
    and a.name = @P_columnname

  Set @l_statement='Alter Table ' + @P_tablename + ' Drop Constraint ' + @l_default_constraint
  Exec(@l_statement)
    
 end;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0002, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.BEHCODE'                      */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  BEHCODE  NVarChar(10)	 Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0003, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.CODE'                         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  CODE  NVarChar(20)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0004, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.NAME'                          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  NAME NVarChar(255) Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0005, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.ACCESSCODE'                   */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  ACCESSCODE  NVarChar(25)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0006, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.DUMPFILE'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  DUMPFILE  NVarChar(255)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0007, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.PDM_DUMPFILE'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  PDM_DUMPFILE  NVarChar(255)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0008, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.PDM_EXPORTFILE'               */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  PDM_EXPORTFILE  NVarChar(255)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0009, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.TABLESPACENAMES_IN_DUMP'      */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  TABLESPACENAMES_IN_DUMP  NVarChar(309)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0010, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.OBJECT_OWNER_IN_DUMP'         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  OBJECT_OWNER_IN_DUMP  NVarChar(309)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0011, IgnoreError=No                                                    */
/*~!~Step, Desc = 'drop default constraint for field PLN_PDMDATABASE.OPTION_NO_IMPORT'*/
/*------------------------------------------------------------------------------------*/
Exec temp_drop_default_constraint 'PLN_PDMDATABASE', 'OPTION_NO_IMPORT';


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0012, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.OPTION_NO_IMPORT'             */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  OPTION_NO_IMPORT  NVarChar(1)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0013, IgnoreError=No                                                    */
/*~!~Step, Desc = 'restore default value for field PLN_PDMDATABASE.OPTION_NO_IMPORT'  */
/*------------------------------------------------------------------------------------*/
ALTER TABLE
  PLN_PDMDATABASE
ADD
  DEFAULT 'F'
FOR
  OPTION_NO_IMPORT;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0014, IgnoreError=No                                                    */
/*~!~Step, Desc = 'drop default constraint for field PLN_PDMDATABASE.STOP_ON_FAILURE' */
/*------------------------------------------------------------------------------------*/
Exec temp_drop_default_constraint 'PLN_PDMDATABASE', 'STOP_ON_FAILURE';


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0015, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.STOP_ON_FAILURE'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  STOP_ON_FAILURE  NVarChar(1)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0016, IgnoreError=No                                                    */
/*~!~Step, Desc = 'restore default value for field PLN_PDMDATABASE.STOP_ON_FAILURE'   */
/*------------------------------------------------------------------------------------*/
ALTER TABLE
  PLN_PDMDATABASE
ADD
  DEFAULT 'T'
FOR
  STOP_ON_FAILURE;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0017, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE01'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  FREE01  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0018, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE02'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  FREE02  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0019, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE03'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  FREE03  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0020, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASEFREE04'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  FREE04  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0021, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE05'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Alter Column
  FREE05  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0022, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.BEHCODE'                */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  BEHCODE  NVarChar(10)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0023, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CODE'                   */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  CODE  NVarChar(20)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0024, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.NAME'                   */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  NAME  NVarChar(255)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0025, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.ADMIN_USER'             */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  ADMIN_USER  NVarChar(30)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0026, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.ADMIN_PASS'             */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  ADMIN_PASS  NVarChar(30)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0027, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CODE_PAGE'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  CODE_PAGE  NVarChar(50)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0028, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CONNECT_SERVER_NAME'    */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  CONNECT_SERVER_NAME  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0029, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CONNECT_TNS_NAME'       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  CONNECT_TNS_NAME  NVarChar(30)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0030, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.HOST_NAME'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  HOST_NAME  NVarChar(30)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0031, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.DATABASE_INSTANCENAME'  */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  DATABASE_INSTANCENAME  NVarChar(50)  Null;


/*--------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0032, IgnoreError=No                                                      */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.TEMPORARY_TABLESPACENAME' */
/*--------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  TEMPORARY_TABLESPACENAME  NVarChar(30)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0033, IgnoreError=No                                                    */
/*~!~Step, Desc = 'drop default constraint for field PLN_PDMDATABASESERVER.IS_ACTIVE' */
/*------------------------------------------------------------------------------------*/
Exec temp_drop_default_constraint 'PLN_PDMDATABASESERVER', 'IS_ACTIVE';



/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0034, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.IS_ACTIVE'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  IS_ACTIVE  NVarChar(1)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0035, IgnoreError=No                                                    */
/*~!~Step, Desc = 'restore default value for field PLN_PDMDATABASESERVER.IS_ACTIVE'   */
/*------------------------------------------------------------------------------------*/
ALTER TABLE
  PLN_PDMDATABASESERVER
ADD
  DEFAULT 'F'
FOR
  IS_ACTIVE;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0036, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.EXP_LOCATION'           */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  EXP_LOCATION  NVarchar(255)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0037, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.IMP_LOCATION'           */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  IMP_LOCATION  NVarchar(255)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0038, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE01'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  FREE01  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0039, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE02'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  FREE02  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0040, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE03'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  FREE03  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0041, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE04'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  FREE04  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0042, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE05'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Alter Column
  FREE05  NVarChar(100)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0043, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.BEHCODE'                      */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Alter Column
  BEHCODE  NVarChar(10)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0044, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.CODE'                         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Alter Column
  CODE  NVarChar(20)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0045, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.NAME'                         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Alter Column
  NAME  NVarChar(255)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0046, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.LOCATION'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Alter Column
  LOCATION  NVarChar(2000)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0047, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.WEBDAV_USERNAME'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Alter Column
  WEBDAV_USERNAME  NVarChar(50)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0048, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.WEBDAV_PASSWORD'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Alter Column
  WEBDAV_PASSWORD  NVarChar(50)  Null;



/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0049, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMREQUEST_QUEUE.BEHCODE'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMREQUEST_QUEUE
Alter Column
  BEHCODE  NVarChar(10)  Not Null;



/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0050, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_DATABASEVERSION.BEHCODE'                  */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Alter Column
  BEHCODE  NVarChar(10)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0051, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_DATABASEVERSION.CODE'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Alter Column
  CODE  NVarChar(20)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0052, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_DATABASEVERSION.NAME'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Alter Column
  NAME  NVarChar(255)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0053, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMCOMPATIBILITYMATRIX.BEHCODE'           */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Alter Column
  BEHCODE  NVarChar(10)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0054, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMTASK.BEHCODE'                          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMTASK
Alter Column
  BEHCODE  NVarChar(10)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0055, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMTASK.SUBTYPE'                          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMTASK
Alter Column
  SUBTYPE  NVarChar(30)  Not Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0056, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDBSERVER_DATAFILELOC.BEHCODE'          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Alter Column
  BEHCODE  NVarChar(10)  Not Null;


/*---------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0057, IgnoreError=No                                                       */
/*~!~Step, Desc = 'change type of field PLN_PDMDBSERVER_DATAFILELOC.FILE_LOCATION_LOCAL' */
/*---------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Alter Column
  FILE_LOCATION_LOCAL  NVarChar(250)  Not Null;


/*-----------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0058, IgnoreError=No                                                         */
/*~!~Step, Desc = 'change type of field PLN_PDMDBSERVER_DATAFILELOC.FILE_LOCATION_NETWORK' */
/*-----------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Alter Column
  FILE_LOCATION_NETWORK  NVarChar(250)  Null;


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0059, IgnoreError=No                                                    */
/*~!~Step, Desc = 'drop temporary procedure'                                          */
/*------------------------------------------------------------------------------------*/
Drop Procedure temp_drop_default_constraint;

