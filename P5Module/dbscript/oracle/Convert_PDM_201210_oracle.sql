/*--------------------------------------------------------------------------------------------------------------*/
/*~!~Section, Nr=0001,  Desc = 'PBL  15725,  Rollout PDM3: Support data type NVARCHAR'                          */
/*--------------------------------------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.BEHCODE'                      */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  BEHCODE  NVarChar2(10)
)
/

/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0002, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.CODE'                         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  CODE  NVarChar2(20)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0003, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.NAME'                          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  NAME NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0004, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.ACCESSCODE'                   */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  ACCESSCODE  NVarChar2(25)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0005, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.DUMPFILE'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  DUMPFILE  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0006, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.PDM_DUMPFILE'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  PDM_DUMPFILE  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0007, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.PDM_EXPORTFILE'               */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  PDM_EXPORTFILE  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0008, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.TABLESPACENAMES_IN_DUMP'      */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  TABLESPACENAMES_IN_DUMP  NVarChar2(309)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0009, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.OBJECT_OWNER_IN_DUMP'         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  OBJECT_OWNER_IN_DUMP  NVarChar2(309)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0010, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.OPTION_NO_IMPORT'             */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  OPTION_NO_IMPORT  NVarChar2(1)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0011, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.STOP_ON_FAILURE'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  STOP_ON_FAILURE  NVarChar2(1)
)
/



/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0012, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE01'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  FREE01  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0013, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE02'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  FREE02  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0014, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE03'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  FREE03  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0015, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASEFREE04'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  FREE04  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0016, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASE.FREE05'                       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASE
Modify
(
  FREE05  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0017, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.BEHCODE'                */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  BEHCODE  NVarChar2(10)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0018, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CODE'                   */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  CODE  NVarChar2(20)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0019, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.NAME'                   */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  NAME  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0020, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.ADMIN_USER'             */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  ADMIN_USER  NVarChar2(30)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0021, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.ADMIN_PASS'             */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  ADMIN_PASS  NVarChar2(30)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0022, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CODE_PAGE'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  CODE_PAGE  NVarChar2(50)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0023, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CONNECT_SERVER_NAME'    */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  CONNECT_SERVER_NAME  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0024, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.CONNECT_TNS_NAME'       */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  CONNECT_TNS_NAME  NVarChar2(30)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0025, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.HOST_NAME'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  HOST_NAME  NVarChar2(30)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0026, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.DATABASE_INSTANCENAME'  */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  DATABASE_INSTANCENAME  NVarChar2(50)
)
/


/*--------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0027, IgnoreError=No                                                      */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.TEMPORARY_TABLESPACENAME' */
/*--------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  TEMPORARY_TABLESPACENAME  NVarChar2(30)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0028, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.IS_ACTIVE'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  IS_ACTIVE  NVarChar2(1)
)
/



/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0029, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.EXP_LOCATION'           */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  EXP_LOCATION  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0030, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.IMP_LOCATION'           */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  IMP_LOCATION  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0031, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE01'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  FREE01  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0032, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE02'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  FREE02  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0033, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE03'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  FREE03  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0034, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE04'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  FREE04  NVarChar2(100)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0035, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDATABASESERVER.FREE05'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Modify
(
  FREE05  NVarChar2(100) 
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0036, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.BEHCODE'                      */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Modify
(
  BEHCODE  NVarChar2(10)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0037, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.CODE'                         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Modify
(
  CODE  NVarChar2(20)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0038, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.NAME'                         */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Modify
(
  NAME  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0039, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.LOCATION'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Modify
(
  LOCATION  NVarChar2(2000)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0040, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.WEBDAV_USERNAME'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Modify
(
  WEBDAV_USERNAME  NVarChar2(50)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0041, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMLOCATION.WEBDAV_PASSWORD'              */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMLOCATION
Modify
(
  WEBDAV_PASSWORD  NVarChar2(50)
)
/



/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0042, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMREQUEST_QUEUE.BEHCODE'                 */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMREQUEST_QUEUE
Modify
(
  BEHCODE  NVarChar2(10)
)
/



/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0043, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_DATABASEVERSION.BEHCODE'                  */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Modify
(
  BEHCODE  NVarChar2(10)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0044, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_DATABASEVERSION.CODE'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Modify
(
  CODE  NVarChar2(20)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0045, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_DATABASEVERSION.NAME'                     */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Modify
(
  NAME  NVarChar2(255)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0046, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMCOMPATIBILITYMATRIX.BEHCODE'           */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Modify
(
  BEHCODE  NVarChar2(10)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0047, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMTASK.BEHCODE'                          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMTASK
Modify
(
  BEHCODE  NVarChar2(10)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0048, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMTASK.SUBTYPE'                          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMTASK
Modify
(
  SUBTYPE  NVarChar2(30)
)
/


/*------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0049, IgnoreError=No                                                    */
/*~!~Step, Desc = 'change type of field PLN_PDMDBSERVER_DATAFILELOC.BEHCODE'          */
/*------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Modify
(
  BEHCODE  NVarChar2(10)
)
/


/*---------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0050, IgnoreError=No                                                       */
/*~!~Step, Desc = 'change type of field PLN_PDMDBSERVER_DATAFILELOC.FILE_LOCATION_LOCAL' */
/*---------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Modify
(
  FILE_LOCATION_LOCAL  NVarChar2(250)
)
/


/*-----------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0051, IgnoreError=No                                                         */
/*~!~Step, Desc = 'change type of field PLN_PDMDBSERVER_DATAFILELOC.FILE_LOCATION_NETWORK' */
/*-----------------------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Modify
(
  FILE_LOCATION_NETWORK  NVarChar2(250)
)
/

