/********************************************************************************************************/
/*~!~Section, Nr=0001, Desc = 'PDM3 - create initial tables and install system codes'     		*/
/********************************************************************************************************/
 
/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDATABASE'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMDATABASE
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT                    Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  CODE                           NVarChar2(20)   Not Null,
  NAME                           NVarChar2(255)  Null,
  ACCESSCODE                     NVarChar2(25)   Not Null,
  FK_BOSTATE                     Number(10,0)   Not Null,
  DUMPFILE                       NVarChar2(255)  Not Null,
  PDM_DUMPFILE                   NVarChar2(255)  Null,
  PDM_EXPORTFILE                 NVarChar2(255)  Null,
  FK_PDMDATABASESERVER           Number(10,0)   Null,
  FK_ACCOUNT_DATABASEOWNER       Number(10,0)   Not Null,
  TABLESPACENAMES_IN_DUMP        NVarChar2(309)  Null,
  OBJECT_OWNER_IN_DUMP           NVarChar2(309)   Null,
  OPTION_NO_IMPORT               NVarChar2(1)    Default 'F' Not Null,
  STOP_ON_FAILURE                NVarChar2(1)    Default 'T' Not Null,
  SIZE_DATABASE                  Number(16,2)   Null,
  SIZE_DUMPFILE                  Number(16,2)   Null,
  FK_PDMLOCATION                 Number(10,0)   Not Null,
  FK_PLC_PDMDATABASETYPE         Number(10,0)   Null, -- Oracle / MsSql
  FK_PLC_PDMFILETYPE             Number(10,0)   Null,  -- Oracle exp / data pump
  TASK_TIMEOUT                   Number(10,0)   Null,
  FK_PLC_PDM_EXPORT_TYPE         Number(10,0)   Null,
  FREE01                         NVarChar2(100)  Null,
  FREE02                         NVarChar2(100)  Null,
  FREE03                         NVarChar2(100)  Null,
  FREE04                         NVarChar2(100)  Null,
  FREE05                         NVarChar2(100)  Null,
  FREE01_INTEGER                 Number(10,0)   Null,
  FREE02_INTEGER                 Number(10,0)   Null,
  FREE03_INTEGER                 Number(10,0)   Null,
  FREE04_INTEGER                 Number(10,0)   Null,
  FREE05_INTEGER                 Number(10,0)   Null
)

/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0002, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDATABASESERVER'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMDATABASESERVER
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT			 Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  CODE                           NVarChar2(20)   Not Null,
  NAME                           NVarChar2(255)  Null,
  FK_PDMDATABASEVERSION          Number(10,0)   Not Null,
  ADMIN_USER                     NVarChar2(30)   Not Null,
  ADMIN_PASS                     NVarChar2(30)   Not Null,
  CODE_PAGE                      NVarChar2(50)       Null,
  CONNECT_SERVER_NAME            NVarChar2(100)      Null,
  SORT_ORDER                     Number(5,0)        Null,
  CONNECT_TNS_NAME               NVarChar2(30)       Null,
  HOST_NAME                      NVarChar2(30)       Null,
  PORT_NUMBER                    Number(10,0)       Null,
  DATABASE_INSTANCENAME          NVarChar2(50)       Null,
  TEMPORARY_TABLESPACENAME       NVarChar2(30)       Null,
  IS_ACTIVE                      NVarChar2(1)    Default 'F' Not Null,
  FK_PDMLOCATION                 Number(10,0)   Not Null,
  FK_PLC_PDMFILETYPE             Number(10,0)       Null,  -- Oracle exp / data pump
  FK_BODEFINITION                Number(10,0)   Not Null,
  EXP_LOCATION                   NVarChar2(255)      Null,
  IMP_LOCATION                   NVarChar2(255)      Null,
  FREE01                         NVarChar2(100)      Null,
  FREE02                         NVarChar2(100)      Null,
  FREE03                         NVarChar2(100)      Null,
  FREE04                         NVarChar2(100)      Null,
  FREE05                         NVarChar2(100)      Null,
  FREE01_INTEGER                 Number(10,0)       Null,
  FREE02_INTEGER                 Number(10,0)       Null,
  FREE03_INTEGER                 Number(10,0)       Null,
  FREE04_INTEGER                 Number(10,0)       Null,
  FREE05_INTEGER                 Number(10,0)       Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0003, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMLOCATION'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMLOCATION
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT                    Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  CODE                           NVarChar2(20)   Not Null,
  NAME                           NVarChar2(255)  Null,
  LOCATION                       NVarChar2(2000) Not Null,
  WEBDAV_USERNAME                NVarChar2(50)   Null,
  WEBDAV_PASSWORD                NVarChar2(50)   Null
)

/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0004, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMREQUEST_QUEUE'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMREQUEST_QUEUE
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT                    Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  FK_PDMDATABASE                 Number(10,0)   Not Null,
  FK_ACCOUNT_REQUESTOR           Number(10,0)   Null,
  FK_PDMDATABASEVERSION          Number(10,0)   Null,
  FK_PLC_PDMREQUESTTYPE          Number(10,0)   Not Null,
  FK_BOSTATE                     Number(10,0)   Not Null
)

/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0005, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDATABASEVERSION'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMDATABASEVERSION
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT			 Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  CODE                           NVarChar2(20)   Not Null,
  NAME                           NVarChar2(255)  Null,
  FK_BODEFINITION                Number(10,0)   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0006, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMCOMPATIBILITYMATRIX'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMCOMPATIBILITYMATRIX 
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT			 Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  FK_PDMDATABASEVERSION          Number(10,0)   Not Null,
  FK_PDMDATABASESERVER           Number(10,0)   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0007, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMTASK'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMTASK
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT                    Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  SUBTYPE                        NVarChar2(30)   Not Null,
  FK_PDMREQUEST                  Number(10,0)   Not Null,
  FK_BOSTATE                     Number(10,0)   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0008, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDBSERVER_DATAFILELOC'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMDBSERVER_DATAFILELOC
(
  SYSCODE                        Number(10,0)   Not Null,
  UPDATECOUNT			 Number(10,0)   Default 0  Not Null,
  BEHCODE                        NVarChar2(10)   Not Null,
  CHANGEDATE                     Date           Null,
  INSERTDATE                     Date           Null,
  FK_PLANUSER_CHANGE             Number(10,0)   Null,
  GEBRCODE                       Number(10,0)   Null,
  FK_PDMDATABASESERVER           Number(10,0)   Not Null,
  FILE_LOCATION_LOCAL            NVarChar2(250)  Not Null,
  FILE_LOCATION_NETWORK          NVarChar2(250)  Null
)

/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0009, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDBTYPE_PDMFILETYPE'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMDBTYPE_PDMFILETYPE
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT	 		 INTEGER   Default 0  Not Null,
  FK_PLC_PDMDATABASETYPE         INTEGER   Not Null,
  FK_PLC_PDMFILETYPE             INTEGER   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0010, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMANALYZEDFILETYPES'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMANALYZEDFILETYPES
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT                    INTEGER   Default 0  Not Null,
  FK_PLC_PDMFILETYPE             INTEGER   Not Null,
  FK_PDMREQUEST                  INTEGER   Not Null 
)



/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0011, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - Create all plancodes'				    		*/
/*------------------------------------------------------------------------------------------------------*/
DECLARE
    newSyscode               Number;
Begin
    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, 'M', 'MSSQL', 'PDMDATABASETYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, 'O', 'ORACLE', 'PDMDATABASETYPE', '');


    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)     
    values (newSyscode, 'M', 'MSSQL DUMP', 'PDMFILETYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, 'O', 'ORACLE EXP', 'PDMFILETYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, 'D', 'ORACLE DATAPUMP', 'PDMFILETYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, 'DandO', 'ORACLE DATAPUMP AND EXP', 'PDMFILETYPE', '');


    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, '0', 'Initial import of new database', 'PDMREQUESTTYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, '1', 'Export of an existing database', 'PDMREQUESTTYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, '2', 'Removal of existing database', 'PDMREQUESTTYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, '3', 'Reload a db with initial dump', 'PDMREQUESTTYPE', '');

    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode, '4', 'Reload a db with exported dump', 'PDMREQUESTTYPE', '');
End;


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0012, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - Fill compatibility table for PDM Database Types and PDM File Types'				    		*/
/*------------------------------------------------------------------------------------------------------*/
DECLARE
	l_syscode_dbtype_M       Number;
	l_syscode_dbtype_O       Number;
	l_syscode_filetype_M     Number;
	l_syscode_filetype_O     Number;
	l_syscode_filetype_D     Number;
	l_syscode_filetype_DandO Number;
	newSyscode               Number;
Begin

	Select SYSCODE Into l_syscode_dbtype_M From PLANCODE Where GRP = 'PDMDATABASETYPE' And CODE = 'M';
	Select SYSCODE Into l_syscode_dbtype_O From PLANCODE Where GRP = 'PDMDATABASETYPE' And CODE = 'O';
	Select SYSCODE Into l_syscode_filetype_M From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'M';
	Select SYSCODE Into l_syscode_filetype_O From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'O';
	Select SYSCODE Into l_syscode_filetype_D From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'D';
	Select SYSCODE Into l_syscode_filetype_DandO From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'DandO';

	Pln_Getnewid('PLN_PDMDBTYPE_PDMFILETYPE',newSyscode);
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE) 
	Values (newSyscode, l_syscode_dbtype_M, l_syscode_filetype_M);
	
	Pln_Getnewid('PLN_PDMDBTYPE_PDMFILETYPE',newSyscode);
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE) 
	Values (newSyscode, l_syscode_dbtype_O, l_syscode_filetype_O);
	
	Pln_Getnewid('PLN_PDMDBTYPE_PDMFILETYPE',newSyscode);
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE) 
	Values (newSyscode, l_syscode_dbtype_O, l_syscode_filetype_D);
	
	Pln_Getnewid('PLN_PDMDBTYPE_PDMFILETYPE',newSyscode);
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE) 
	Values (newSyscode, l_syscode_dbtype_O, l_syscode_filetype_DandO);
End;


/********************************************************************************************************/
/*~!~Section, Nr=0002, Desc = 'PDM3 - increase space for PLN_PDMDATABASE.CODE'     		*/
/********************************************************************************************************/
 
/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'allow database codes upto 30 chars' */
/*---------------------------------------------------------------------------*/

alter table PLN_PDMDATABASE
modify (code nvarchar2(30))

/********************************************************************************************************/
/*~!~Section, Nr=0003, Desc = 'PDM3 - create initial tables and install system codes'     		*/
/********************************************************************************************************/



/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMLOCATION.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMLOCATION
Add
  Constraint
    PLN_PLN_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0002, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMLOCATION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMLOCATION
Add
  Constraint
    PLN_PLN_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0003, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMLOCATION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PLN_01
On  
   PLN_PDMLOCATION
  (
    FK_PLANUSER_CHANGE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0004, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMLOCATION.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PLN_02
On  
   PLN_PDMLOCATION
  (
    UPPER(CODE)
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0005, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMLOCATION.PLN_PLN_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PLN_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMLOCATION
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0006, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMLOCATION.PLN_PLN_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PLN_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMLOCATION
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0007, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMLOCATION.PLN_PLN_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PLN_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMLOCATION
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMLOCATION.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;



/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0008, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDATABASEVERSION.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASEVERSION
Add
  Constraint
    PLN_PDB_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0009, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASEVERSION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASEVERSION
Add
  Constraint
    PLN_PDB_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0010, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASEVERSION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDB_01
On  
   PLN_PDMDATABASEVERSION
  (
    FK_PLANUSER_CHANGE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0011, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASEVERSION.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDB_02
On  
   PLN_PDMDATABASEVERSION
  (
    UPPER(CODE)
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0012, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASEVERSION.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Add
  Constraint
    PLN_PDB_BDN_FK01
  Foreign Key
  (
    FK_BODEFINITION
  )
  References
    PLN_BODEFINITION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0013, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASEVERSION.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDB_03
On  
   PLN_PDMDATABASEVERSION
  (
     FK_BODEFINITION
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0014, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASEVERSION.PLN_PDB_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PDB_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMDATABASEVERSION
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0015, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASEVERSION.PLN_PDB_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PDB_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMDATABASEVERSION
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0016, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASEVERSION.PLN_PDB_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PDB_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMDATABASEVERSION
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMDATABASEVERSION.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;







/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0017, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDATABASESERVER.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0018, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0019, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_01
On  
   PLN_PDMDATABASESERVER
  (
    FK_PLANUSER_CHANGE
  )
 


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0020, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_02
On  
   PLN_PDMDATABASESERVER
  (
    UPPER(CODE)
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0021, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PDB_FK01
  Foreign Key
  (
    FK_PDMDATABASEVERSION 
  )
  References
    PLN_PDMDATABASEVERSION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0022, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_03
On  
   PLN_PDMDATABASESERVER
  (
    FK_PDMDATABASEVERSION 
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0023, IgnoreError=No */
/*~!~Step, Desc = 'Create check constriant PLN_PDMDATABASESERVER.IS_ACTIVE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_CK01
  Check
    (IS_ACTIVE In ('T', 'F'))



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0024, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PLN_FK01
  Foreign Key
  (
    FK_PDMLOCATION 
  )
  References
    PLN_PDMLOCATION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0025, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_04
On  
   PLN_PDMDATABASESERVER
  (
    FK_PDMLOCATION
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0026, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.PLN_PDV_PLC_FK01' */

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0027, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_05
On  
   PLN_PDMDATABASESERVER
  (
     FK_PLC_PDMFILETYPE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0028, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_BDN_FK01
  Foreign Key
  (
    FK_BODEFINITION
  )
  References
    PLN_BODEFINITION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0029, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_06
On  
   PLN_PDMDATABASESERVER
  (
     FK_BODEFINITION
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0030, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASESERVER.PLN_PDV_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PDV_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMDATABASESERVER
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0031, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASESERVER.PLN_PDV_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PDV_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMDATABASESERVER
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0032, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASESERVER.PLN_PDV_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PDV_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMDATABASESERVER
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMDATABASESERVER.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;





/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0033, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDATABASE.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PK
  Primary Key
  (
    SYSCODE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0034, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0035, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_01
On  
   PLN_PDMDATABASE
  (
    FK_PLANUSER_CHANGE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0036, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_02
On  
   PLN_PDMDATABASE
  (
    UPPER(CODE)
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0037, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_BSE_FK01
  Foreign Key
  (
    FK_BOSTATE
  )
  References
    PLN_BOSTATE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0038, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_03
On  
   PLN_PDMDATABASE
  (
     FK_BOSTATE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0039, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PDV_FK01
  Foreign Key
  (
    FK_PDMDATABASESERVER
  )
  References
    PLN_PDMDATABASESERVER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0040, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_04
On  
   PLN_PDMDATABASE
  (
     FK_PDMDATABASESERVER
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0041, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_ACCOUNT_DATABASEOWNER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PUR_FK02
  Foreign Key
  (
    FK_ACCOUNT_DATABASEOWNER
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0042, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_ACCOUNT_DATABASEOWNER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_05
On  
   PLN_PDMDATABASE
  (
     FK_ACCOUNT_DATABASEOWNER
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0043, IgnoreError=No */
/*~!~Step, Desc = 'Create check constriant PLN_PDMDATABASE.OPTION_NO_IMPORT' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_CK01
  Check
    (OPTION_NO_IMPORT  In ('T', 'F'))


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0044, IgnoreError=No */
/*~!~Step, Desc = 'Create check constriant PLN_PDMDATABASE.STOP_ON_FAILURE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_CK02
  Check
    (STOP_ON_FAILURE  In ('T', 'F'))



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0045, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLN_FK01
  Foreign Key
  (
    FK_PDMLOCATION 
  )
  References
    PLN_PDMLOCATION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0046, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_06
On  
   PLN_PDMDATABASE
  (
    FK_PDMLOCATION
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0047, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.PLN_PDD_PLC_FK01' */

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMDATABASETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0048, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLC_PDMDATABASETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_07
On  
   PLN_PDMDATABASE
  (
     FK_PLC_PDMDATABASETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0049, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.PLN_PDD_PLC_FK02' */

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLC_FK02
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0050, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_08
On  
   PLN_PDMDATABASE
  (
     FK_PLC_PDMFILETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0051, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.PLN_PDD_PLC_FK03' */

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLC_FK03
  Foreign Key
  (
    FK_PLC_PDM_EXPORT_TYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0052, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLC_PDM_EXPORT_TYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_09
On  
   PLN_PDMDATABASE
  (
     FK_PLC_PDM_EXPORT_TYPE 
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0053, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASE.PLN_PDD_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PDD_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMDATABASE
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0054, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASE.PLN_PDD_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PDD_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMDATABASE
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0055, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASE.PLN_PDD_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PDD_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMDATABASE
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMDATABASE.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;





/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0056, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMREQUEST_QUEUE.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0057, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0058, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_01
On  
   PLN_PDMREQUEST_QUEUE
  (
    FK_PLANUSER_CHANGE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0059, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_PDMDATABASE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PUR_FK02
  Foreign Key
  (
    FK_PDMDATABASE
  )
  References
    PLN_PDMDATABASE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0060, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_PDMDATABASE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_02
On  
   PLN_PDMREQUEST_QUEUE
  (
    FK_PDMDATABASE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0061, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_ACCOUNT_REQUESTOR' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PUR_FK03
  Foreign Key
  (
    FK_ACCOUNT_REQUESTOR
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0062, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_ACCOUNT_REQUESTOR' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_03
On  
   PLN_PDMREQUEST_QUEUE
  (
    FK_ACCOUNT_REQUESTOR
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0063, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.PLN_PRE_PLC_FK01' */

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMREQUESTTYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0064, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_PLC_PDMREQUESTTYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_04
On  
   PLN_PDMREQUEST_QUEUE
  (
     FK_PLC_PDMREQUESTTYPE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0065, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_BSE_FK01
  Foreign Key
  (
    FK_BOSTATE
  )
  References
    PLN_BOSTATE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0066, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_05
On  
   PLN_PDMREQUEST_QUEUE
  (
     FK_BOSTATE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0067, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMREQUEST_QUEUE.PLN_PRE_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PRE_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMREQUEST_QUEUE
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0068, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMREQUEST_QUEUE.PLN_PRE_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PRE_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMREQUEST_QUEUE
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0069, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMREQUEST_QUEUE.PLN_PRE_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PRE_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMREQUEST_QUEUE
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMREQUEST_QUEUE.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;









/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0070, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMCOMPATIBILITYMATRIX.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0071, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMCOMPATIBILITYMATRIX.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0072, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMCOMPATIBILITYMATRIX.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PCX_01
On  
   PLN_PDMCOMPATIBILITYMATRIX
  (
    FK_PLANUSER_CHANGE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0073, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PDB_FK01
  Foreign Key
  (
    FK_PDMDATABASEVERSION
  )
  References
    PLN_PDMDATABASEVERSION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0074, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PCX_02
On  
   PLN_PDMCOMPATIBILITYMATRIX
  (
    FK_PDMDATABASEVERSION
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0075, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PDV_FK01
  Foreign Key
  (
    FK_PDMDATABASESERVER
  )
  References
    PLN_PDMDATABASESERVER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0076, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PCX_03
On  
   PLN_PDMCOMPATIBILITYMATRIX
  (
    FK_PDMDATABASESERVER
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0077, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMCOMPATIBILITYMATRIX.PLN_PCX_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PCX_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMCOMPATIBILITYMATRIX
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0078, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMCOMPATIBILITYMATRIX.PLN_PCX_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PCX_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMCOMPATIBILITYMATRIX
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0079, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMCOMPATIBILITYMATRIX.PLN_PCX_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PCX_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMCOMPATIBILITYMATRIX
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMCOMPATIBILITYMATRIX.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;













/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0080, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMTASK.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0081, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMTASK.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0082, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMTASK.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PTK_01
On  
   PLN_PDMTASK
  (
    FK_PLANUSER_CHANGE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0083, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMTASK.FK_PDMREQUEST' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_PRE_FK01
  Foreign Key
  (
    FK_PDMREQUEST
  )
  References
    PLN_PDMREQUEST_QUEUE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0084, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMTASK.FK_PDMREQUEST' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PTK_02
On  
   PLN_PDMTASK
  (
    FK_PDMREQUEST
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0085, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMTASK.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_BSE_FK01
  Foreign Key
  (
    FK_BOSTATE
  )
  References
    PLN_BOSTATE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0086, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMTASK.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PTK_03
On  
   PLN_PDMTASK
  (
     FK_BOSTATE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0087, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMTASK.PLN_PTK_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PTK_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMTASK
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0088, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMTASK.PLN_PTK_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PTK_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMTASK
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0089, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMTASK.PLN_PTK_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PTK_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMTASK
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMTASK.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;




/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0090, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDBSERVER_DATAFILELOC.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Add
  Constraint
    PLN_PSC_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0091, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBSERVER_DATAFILELOC.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Add
  Constraint
    PLN_PSC_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0092, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBSERVER_DATAFILELOC.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PSC_01
On  
   PLN_PDMDBSERVER_DATAFILELOC
  (
    FK_PLANUSER_CHANGE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0093, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBSERVER_DATAFILELOC.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Add
  Constraint
    PLN_PSC_PDV_FK01
  Foreign Key
  (
    FK_PDMDATABASESERVER
  )
  References
    PLN_PDMDATABASESERVER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0094, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBSERVER_DATAFILELOC.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PSC_02
On  
   PLN_PDMDBSERVER_DATAFILELOC
  (
    FK_PDMDATABASESERVER
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0095, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDBSERVER_DATAFILELOC.PLN_PSC_BRI_FILL_INSERTDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PSC_BRI_FILL_INSERTDATE
BEFORE INSERT 
ON
PLN_PDMDBSERVER_DATAFILELOC
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
      :MYNEW.INSERTDATE := SYSDATE;
END;


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0096, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDBSERVER_DATAFILELOC.PLN_PSC_BRU_FILL_CHANGEDATE' */
/*---------------------------------------------------------------------------*/

Create Trigger
  PLN_PSC_BRU_FILL_CHANGEDATE 
BEFORE UPDATE
ON
PLN_PDMDBSERVER_DATAFILELOC
REFERENCING NEW AS MYNEW
FOR EACH ROW
BEGIN
 
 :MYNEW.CHANGEDATE := SYSDATE;
END;

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0097, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDBSERVER_DATAFILELOC.PLN_PSC_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PSC_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMDBSERVER_DATAFILELOC
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMDBSERVER_DATAFILELOC.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;




/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0098, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDBTYPE_PDMFILETYPE.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBTYPE_PDMFILETYPE
Add
  Constraint
    PLN_PPE_PK
  Primary Key
  (
    SYSCODE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0099, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBTYPE_PDMFILETYPE.PLN_PPE_PLC_FK01' */

Alter Table
  PLN_PDMDBTYPE_PDMFILETYPE
Add
  Constraint
    PLN_PPE_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMDATABASETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0100, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBTYPE_PDMFILETYPE.FK_PLC_PDMDATABASETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PPE_01
On  
   PLN_PDMDBTYPE_PDMFILETYPE
  (
     FK_PLC_PDMDATABASETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0101, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBTYPE_PDMFILETYPE.PLN_PPE_PLC_FK02' */

Alter Table
  PLN_PDMDBTYPE_PDMFILETYPE
Add
  Constraint
    PLN_PPE_PLC_FK02
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0102, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBTYPE_PDMFILETYPE.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PPE_02
On  
   PLN_PDMDBTYPE_PDMFILETYPE
  (
     FK_PLC_PDMFILETYPE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0103, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDBTYPE_PDMFILETYPE.PLN_PPE_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PPE_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMDBTYPE_PDMFILETYPE
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMDBTYPE_PDMFILETYPE.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;






/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0104, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMANALYZEDFILETYPES.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMANALYZEDFILETYPES
Add
  Constraint
    PLN_PAF_PK
  Primary Key
  (
    SYSCODE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0105, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMANALYZEDFILETYPES.PLN_PAF_PLC_FK01' */

Alter Table
  PLN_PDMANALYZEDFILETYPES
Add
  Constraint
    PLN_PAF_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0106, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMANALYZEDFILETYPES.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PAF_01
On  
   PLN_PDMANALYZEDFILETYPES
  (
     FK_PLC_PDMFILETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0107, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMANALYZEDFILETYPES.PLN_PAF_PRE_FK01' */

Alter Table
  PLN_PDMANALYZEDFILETYPES
Add
  Constraint
    PLN_PAF_PRE_FK01
  Foreign Key
  (
    FK_PDMREQUEST
  )
  References
    PLN_PDMREQUEST_QUEUE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0108, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMANALYZEDFILETYPES.FK_PDMREQUEST' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PAF_02
On  
   PLN_PDMANALYZEDFILETYPES
  (
     FK_PDMREQUEST
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0109, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMANALYZEDFILETYPES.PLN_PAF_BRU_RAISE_UPDATECOUNT' */
/*---------------------------------------------------------------------------*/

 Create Trigger
   PLN_PAF_BRU_RAISE_UPDATECOUNT 
 BEFORE UPDATE
 ON
 PLN_PDMANALYZEDFILETYPES
 REFERENCING NEW AS MYNEW
             OLD AS MYOLD
 FOR EACH ROW
 DECLARE 
    V_NEW_UPDATECOUNT PLN_PDMANALYZEDFILETYPES.UPDATECOUNT%TYPE;
 BEGIN
 
 PLN_GET_NEW_UPDATECOUNT(:MYOLD.UPDATECOUNT, V_NEW_UPDATECOUNT);
       :MYNEW.UPDATECOUNT := V_NEW_UPDATECOUNT;
 END;

 
 
