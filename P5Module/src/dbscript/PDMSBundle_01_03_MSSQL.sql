/**************************************************************************************************/
/*~!~Section, Nr=0001, Desc = 'TEC-4115, PDM3 - Oracle user permissions for upgrade'    */
/**************************************************************************************************/
 
/*-----------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx 				    							     */
/*~!~Step, Desc = 'Adding new entry - ADMIN for PDMPRIVILEGETYPE'				    	         */
/*-----------------------------------------------------------------------------------------------*/

BEGIN
	DECLARE @newSyscode Integer;
    
	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'A', 'ADMIN', 'PDMPRIVILEGETYPE', '');

END

