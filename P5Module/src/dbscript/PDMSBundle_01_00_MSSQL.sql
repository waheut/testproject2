/********************************************************************************************************/
/*~!~Section, Nr=0001, Desc = 'PDM3 - create initial tables and install system codes'     		*/
/********************************************************************************************************/
 
/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDATABASE'				    		*/
/*------------------------------------------------------------------------------------------------------*/


Create Table PLN_PDMDATABASE
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT                    INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER  Null,
  CODE                           NVarChar(20)   Not Null,
  NAME                           NVarChar(255)  Null,
  ACCESSCODE                     NVarChar(25)   Not Null,
  FK_BOSTATE                     INTEGER   Not Null,
  DUMPFILE                       NVarChar(255)  Not Null,
  PDM_DUMPFILE                   NVarChar(255)  Null,
  PDM_EXPORTFILE                 NVarChar(255)  Null,
  FK_PDMDATABASESERVER           INTEGER   Null,
  FK_ACCOUNT_DATABASEOWNER       INTEGER   Not Null,
  TABLESPACENAMES_IN_DUMP        NVarChar(309)   Null,
  OBJECT_OWNER_IN_DUMP           NVarChar(309)   Null,
  OPTION_NO_IMPORT               NVarChar(1)    Default 'F' Not Null,
  STOP_ON_FAILURE                NVarChar(1)    Default 'T' Not Null,
  SIZE_DATABASE                  NUMERIC(16,2)   Null,
  SIZE_DUMPFILE                  NUMERIC(16,2)   Null,
  TASK_TIMEOUT                   INTEGER   Null,
  FK_PDMLOCATION                 INTEGER   Not Null,
  FK_PLC_PDMDATABASETYPE         INTEGER   Null,
  FK_PLC_PDMFILETYPE             INTEGER   Null,
  FK_PLC_PDM_EXPORT_TYPE         INTEGER   Null,
  FREE01_INTEGER                 INTEGER   Null,
  FREE02_INTEGER                 INTEGER   Null,
  FREE03_INTEGER                 INTEGER   Null,
  FREE04_INTEGER                 INTEGER   Null,
  FREE05_INTEGER                 INTEGER   Null,
  FREE01                         NVarChar(100) Null,
  FREE02                         NVarChar(100) Null,
  FREE03                         NVarChar(100) Null,
  FREE04                         NVarChar(100) Null,
  FREE05                         NVarChar(100) Null
)


 
/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0002, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDATABASESERVER'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMDATABASESERVER
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT			 		 INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER   Null,
  CODE                           NVarChar(20)   Not Null,
  NAME                           NVarChar(255)  Null,
  FK_PDMDATABASEVERSION          INTEGER   Not Null,
  ADMIN_USER                     NVarChar(30)   Not Null,
  ADMIN_PASS                     NVarChar(30)   Not Null,
  CODE_PAGE                      NVarChar(50)       Null,
  CONNECT_SERVER_NAME            NVarChar(100)      Null,
  SORT_ORDER                     INTEGER        Null,
  CONNECT_TNS_NAME               NVarChar(30)       Null,
  HOST_NAME                      NVarChar(30)       Null,
  PORT_NUMBER                    INTEGER        Null,
  DATABASE_INSTANCENAME          NVarChar(50)       Null,
  TEMPORARY_TABLESPACENAME       NVarChar(30)       Null,
  IS_ACTIVE                      NVarChar(1)    Default 'F' Not Null,
  FK_PDMLOCATION                 INTEGER   Not Null,
  FK_PLC_PDMFILETYPE             INTEGER       Null,
  FK_BODEFINITION                INTEGER       Not Null,
  EXP_LOCATION                   NVarchar(255)  Null,
  IMP_LOCATION                   NVarchar(255)  Null,
  FREE01_INTEGER                 INTEGER   Null,
  FREE02_INTEGER                 INTEGER   Null,
  FREE03_INTEGER                 INTEGER   Null,
  FREE04_INTEGER                 INTEGER   Null,
  FREE05_INTEGER                 INTEGER   Null,
  FREE01                         NVarChar(100) Null,
  FREE02                         NVarChar(100) Null,
  FREE03                         NVarChar(100) Null,
  FREE04                         NVarChar(100) Null,
  FREE05                         NVarChar(100) Null
)

/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0003, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMLOCATION'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMLOCATION
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT                    INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER   Null,
  CODE                           NVarChar(20)   Not Null,
  NAME                           NVarChar(255)  Null,
  LOCATION                       NVarChar(2000) Not Null,
  WEBDAV_USERNAME                NVarChar(50)   Null,
  WEBDAV_PASSWORD                NVarChar(50)   Null
)

/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0004, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMREQUEST_QUEUE'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMREQUEST_QUEUE
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT                    INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER   Null,
  FK_PDMDATABASE                 INTEGER   Not Null,
  FK_ACCOUNT_REQUESTOR           INTEGER   Null,
  FK_PDMDATABASEVERSION          INTEGER   Null,
  FK_PLC_PDMREQUESTTYPE          INTEGER   Not Null,
  FK_BOSTATE                     INTEGER   Not Null
)

/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0005, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDATABASEVERSION'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMDATABASEVERSION
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT	 		 INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER   Null,
  CODE                           NVarChar(20)   Not Null,
  NAME                           NVarChar(255)  Null,
  FK_BODEFINITION                INTEGER   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0006, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMCOMPATIBILITYMATRIX'				    		*/
/*------------------------------------------------------------------------------------------------------*/




Create Table PLN_PDMCOMPATIBILITYMATRIX 
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT			 INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER   Null,
  FK_PDMDATABASEVERSION          INTEGER   Not Null,
  FK_PDMDATABASESERVER           INTEGER   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0007, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMTASK'				    		*/
/*------------------------------------------------------------------------------------------------------*/




Create Table PLN_PDMTASK
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT                    INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER   Null,
  SUBTYPE                        NVarChar(30)   Not Null,
  FK_PDMREQUEST                  INTEGER   Not Null,
  FK_BOSTATE                     INTEGER   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0008, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDBSERVER_DATAFILELOC'				    		*/
/*------------------------------------------------------------------------------------------------------*/





Create Table PLN_PDMDBSERVER_DATAFILELOC
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT			 INTEGER   Default 0  Not Null,
  BEHCODE                        NVarChar(10)    Not Null,
  CHANGEDATE                     DATETIME           Null,
  INSERTDATE                     DATETIME           Null,
  FK_PLANUSER_CHANGE             INTEGER   Null,
  GEBRCODE                       INTEGER   Null,
  FK_PDMDATABASESERVER           INTEGER   Not Null,
  FILE_LOCATION_LOCAL            NVarChar(250)  Not Null,
  FILE_LOCATION_NETWORK          NVarChar(250)  Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0009, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMDBTYPE_PDMFILETYPE'				    		*/
/*------------------------------------------------------------------------------------------------------*/



Create Table PLN_PDMDBTYPE_PDMFILETYPE
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT	 		 INTEGER   Default 0  Not Null,
  FK_PLC_PDMDATABASETYPE         INTEGER   Not Null,
  FK_PLC_PDMFILETYPE             INTEGER   Not Null
)


/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0010, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - create table PLN_PDMANALYZEDFILETYPES'				    		*/
/*------------------------------------------------------------------------------------------------------*/




Create Table PLN_PDMANALYZEDFILETYPES
(
  SYSCODE                        INTEGER   Not Null,
  UPDATECOUNT                    INTEGER   Default 0  Not Null,
  FK_PLC_PDMFILETYPE             INTEGER   Not Null,
  FK_PDMREQUEST                  INTEGER   Not Null 
)




/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0011, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - Create all plancodes'				    		*/
/*------------------------------------------------------------------------------------------------------*/
BEGIN
	DECLARE @newSyscode Integer;
    
	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'M', 'MSSQL', 'PDMDATABASETYPE', '');

	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'O', 'ORACLE', 'PDMDATABASETYPE', '');



	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'M', 'MSSQL DUMP', 'PDMFILETYPE', '');

	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'D', 'ORACLE DATAPUMP', 'PDMFILETYPE', '');

	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'O', 'ORACLE EXP', 'PDMFILETYPE', '');

	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'DandO', 'ORACLE DATAPUMP AND EXP', 'PDMFILETYPE', '');
  


	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, '0', 'Initial import of new database', 'PDMREQUESTTYPE', '');
	  
	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, '1', 'Export of an existing database', 'PDMREQUESTTYPE', '');


	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, '2', 'Removal of existing database', 'PDMREQUESTTYPE', '');

	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, '3', 'Reload a db with initial dump', 'PDMREQUESTTYPE', '');

	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, '4', 'Reload a db with exported dump', 'PDMREQUESTTYPE', '');
END



/*------------------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0012, IgnoreError=No, DBMR=xxx 																*/
/*~!~Step, Desc = 'PDM3  - Fill compatibility table for PDM Database Types and PDM File Types'				    		*/
/*------------------------------------------------------------------------------------------------------*/
Begin
	declare @l_syscode_dbtype_M       Integer;
	declare @l_syscode_dbtype_O       Integer;
	declare @l_syscode_filetype_M     Integer;
	declare @l_syscode_filetype_O     Integer;
	declare @l_syscode_filetype_D     Integer;
	declare @l_syscode_filetype_DandO Integer;
	declare @newSyscode               Integer;

	Select @l_syscode_dbtype_M = SYSCODE From PLANCODE Where GRP = 'PDMDATABASETYPE' And CODE = 'M';
	Select @l_syscode_dbtype_O = SYSCODE From PLANCODE Where GRP = 'PDMDATABASETYPE' And CODE = 'O';
	Select @l_syscode_filetype_M = SYSCODE From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'M';
	Select @l_syscode_filetype_O = SYSCODE From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'O';
	Select @l_syscode_filetype_D = SYSCODE From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'D';
	Select @l_syscode_filetype_DandO = SYSCODE From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'DandO';

	Exec Pln_Getnewid 'PLN_PDMDBTYPE_PDMFILETYPE', @newSyscode Output
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE) 
	Values (@newSyscode, @l_syscode_dbtype_M, @l_syscode_filetype_M)

	Exec Pln_Getnewid 'PLN_PDMDBTYPE_PDMFILETYPE', @newSyscode Output
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE)
	Values (@newSyscode, @l_syscode_dbtype_O, @l_syscode_filetype_O)

	Exec Pln_Getnewid 'PLN_PDMDBTYPE_PDMFILETYPE', @newSyscode Output
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE) 
	Values (@newSyscode, @l_syscode_dbtype_O, @l_syscode_filetype_D)

	Exec Pln_Getnewid 'PLN_PDMDBTYPE_PDMFILETYPE', @newSyscode Output
	Insert Into PLN_PDMDBTYPE_PDMFILETYPE (SYSCODE, FK_PLC_PDMDATABASETYPE, FK_PLC_PDMFILETYPE) 
	Values (@newSyscode, @l_syscode_dbtype_O, @l_syscode_filetype_DandO)
End



/********************************************************************************************************/
/*~!~Section, Nr=0002, Desc = 'PDM3 - increase space for PLN_PDMDATABASE.CODE'     		*/
/********************************************************************************************************/
 
/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'allow database codes upto 30 chars' */
/*---------------------------------------------------------------------------*/
  
alter table PLN_PDMDATABASE
alter column code nvarchar(30) not null
  




/********************************************************************************************************/
/*~!~Section, Nr=0003, Desc = 'PDM3 - create initial tables and install system codes'     		*/
/********************************************************************************************************/


/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMLOCATION.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMLOCATION
Add
  Constraint
    PLN_PLN_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0002, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMLOCATION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMLOCATION
Add
  Constraint
    PLN_PLN_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0003, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMLOCATION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PLN_01
On  
   PLN_PDMLOCATION
  (
    FK_PLANUSER_CHANGE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0004, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMLOCATION.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PLN_02
On  
   PLN_PDMLOCATION
  (
    CODE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0005, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMLOCATION.PLN_PLN_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PLN_I
  On
    PLN_PDMLOCATION
  For Insert
  As
  Begin
    UPDATE PLN_PDMLOCATION
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMLOCATION ON Inserted.SYSCODE = PLN_PDMLOCATION.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0006, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMLOCATION.PLN_PLN_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PLN_U
  On
    PLN_PDMLOCATION
  For Update
  As
  Begin
    Update
      PLN_PDMLOCATION
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMLOCATION
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMLOCATION.SYSCODE
 
 Update
      PLN_PDMLOCATION
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMLOCATION
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMLOCATION.SYSCODE) And (PLN_PDMLOCATION.UPDATECOUNT >= 2147483647)
  End



/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0007, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDATABASEVERSION.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASEVERSION
Add
  Constraint
    PLN_PDB_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0008, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASEVERSION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASEVERSION
Add
  Constraint
    PLN_PDB_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0009, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASEVERSION.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDB_01
On  
   PLN_PDMDATABASEVERSION
  (
    FK_PLANUSER_CHANGE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0010, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASEVERSION.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDB_02
On  
   PLN_PDMDATABASEVERSION
  (
    CODE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0011, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASEVERSION.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASEVERSION
Add
  Constraint
    PLN_PDB_BDN_FK01
  Foreign Key
  (
    FK_BODEFINITION
  )
  References
    PLN_BODEFINITION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0012, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASEVERSION.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDB_03
On  
   PLN_PDMDATABASEVERSION
  (
     FK_BODEFINITION
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0013, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASEVERSION.PLN_PDB_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PDB_I
  On
    PLN_PDMDATABASEVERSION
  For Insert
  As
  Begin
    UPDATE PLN_PDMDATABASEVERSION
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMDATABASEVERSION ON Inserted.SYSCODE = PLN_PDMDATABASEVERSION.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0014, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASEVERSION.PLN_PDB_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PDB_U
  On
    PLN_PDMDATABASEVERSION
  For Update
  As
  Begin
    Update
      PLN_PDMDATABASEVERSION
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMDATABASEVERSION
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMDATABASEVERSION.SYSCODE
 
 Update
      PLN_PDMDATABASEVERSION
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMDATABASEVERSION
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMDATABASEVERSION.SYSCODE) And (PLN_PDMDATABASEVERSION.UPDATECOUNT >= 2147483647)
  End










/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0015, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDATABASESERVER.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0016, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0017, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_01
On  
   PLN_PDMDATABASESERVER
  (
    FK_PLANUSER_CHANGE
  )
 


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0018, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_02
On  
   PLN_PDMDATABASESERVER
  (
    CODE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0019, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PDB_FK01
  Foreign Key
  (
    FK_PDMDATABASEVERSION 
  )
  References
    PLN_PDMDATABASEVERSION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0020, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_03
On  
   PLN_PDMDATABASESERVER
  (
    FK_PDMDATABASEVERSION 
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0021, IgnoreError=No */
/*~!~Step, Desc = 'Create check constriant PLN_PDMDATABASESERVER.IS_ACTIVE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_CK01
  Check
    (IS_ACTIVE In ('T', 'F'))



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0022, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PLN_FK01
  Foreign Key
  (
    FK_PDMLOCATION 
  )
  References
    PLN_PDMLOCATION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0023, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_04
On  
   PLN_PDMDATABASESERVER
  (
    FK_PDMLOCATION
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0024, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.PLN_PDV_PLC_FK01' */

Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0025, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_05
On  
   PLN_PDMDATABASESERVER
  (
     FK_PLC_PDMFILETYPE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0026, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASESERVER.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/
Alter Table
  PLN_PDMDATABASESERVER
Add
  Constraint
    PLN_PDV_BDN_FK01
  Foreign Key
  (
    FK_BODEFINITION
  )
  References
    PLN_BODEFINITION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0027, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASESERVER.FK_BODEFINITION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDV_06
On  
   PLN_PDMDATABASESERVER
  (
     FK_BODEFINITION
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0028, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASESERVER.PLN_PDV_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PDV_I
  On
    PLN_PDMDATABASESERVER
  For Insert
  As
  Begin
    UPDATE PLN_PDMDATABASESERVER
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMDATABASESERVER ON Inserted.SYSCODE = PLN_PDMDATABASESERVER.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0029, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASESERVER.PLN_PDV_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PDV_U
  On
    PLN_PDMDATABASESERVER
  For Update
  As
  Begin
    Update
      PLN_PDMDATABASESERVER
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMDATABASESERVER
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMDATABASESERVER.SYSCODE
 
 Update
      PLN_PDMDATABASESERVER
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMDATABASESERVER
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMDATABASESERVER.SYSCODE) And (PLN_PDMDATABASESERVER.UPDATECOUNT >= 2147483647)
  End






/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0030, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDATABASE.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PK
  Primary Key
  (
    SYSCODE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0031, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0032, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_01
On  
   PLN_PDMDATABASE
  (
    FK_PLANUSER_CHANGE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0033, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.CODE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_02
On  
   PLN_PDMDATABASE
  (
    CODE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0034, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_BSE_FK01
  Foreign Key
  (
    FK_BOSTATE
  )
  References
    PLN_BOSTATE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0035, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_03
On  
   PLN_PDMDATABASE
  (
     FK_BOSTATE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0036, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PDV_FK01
  Foreign Key
  (
    FK_PDMDATABASESERVER
  )
  References
    PLN_PDMDATABASESERVER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0037, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_04
On  
   PLN_PDMDATABASE
  (
     FK_PDMDATABASESERVER
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0038, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_ACCOUNT_DATABASEOWNER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PUR_FK02
  Foreign Key
  (
    FK_ACCOUNT_DATABASEOWNER
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0039, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_ACCOUNT_DATABASEOWNER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_05
On  
   PLN_PDMDATABASE
  (
     FK_ACCOUNT_DATABASEOWNER
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0040, IgnoreError=No */
/*~!~Step, Desc = 'Create check constriant PLN_PDMDATABASE.OPTION_NO_IMPORT' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_CK01
  Check
    (OPTION_NO_IMPORT  In ('T', 'F'))


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0041, IgnoreError=No */
/*~!~Step, Desc = 'Create check constriant PLN_PDMDATABASE.STOP_ON_FAILURE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_CK02
  Check
    (STOP_ON_FAILURE  In ('T', 'F'))



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0042, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLN_FK01
  Foreign Key
  (
    FK_PDMLOCATION 
  )
  References
    PLN_PDMLOCATION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0043, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PDMLOCATION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_06
On  
   PLN_PDMDATABASE
  (
    FK_PDMLOCATION
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0044, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.PLN_PDD_PLC_FK01' */

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMDATABASETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0045, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLC_PDMDATABASETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_07
On  
   PLN_PDMDATABASE
  (
     FK_PLC_PDMDATABASETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0046, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.PLN_PDD_PLC_FK02' */

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLC_FK02
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0047, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_08
On  
   PLN_PDMDATABASE
  (
     FK_PLC_PDMFILETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0048, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.PLN_PDD_PLC_FK03' */

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLC_FK03
  Foreign Key
  (
    FK_PLC_PDM_EXPORT_TYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0049, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLC_PDM_EXPORT_TYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PDD_09
On  
   PLN_PDMDATABASE
  (
     FK_PLC_PDM_EXPORT_TYPE 
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0050, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASE.PLN_PDD_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PDD_I
  On
    PLN_PDMDATABASE
  For Insert
  As
  Begin
    UPDATE PLN_PDMDATABASE
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMDATABASE ON Inserted.SYSCODE = PLN_PDMDATABASE.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0051, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDATABASE.PLN_PDD_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PDD_U
  On
    PLN_PDMDATABASE
  For Update
  As
  Begin
    Update
      PLN_PDMDATABASE
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMDATABASE
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMDATABASE.SYSCODE
 
 Update
      PLN_PDMDATABASE
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMDATABASE
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMDATABASE.SYSCODE) And (PLN_PDMDATABASE.UPDATECOUNT >= 2147483647)
  End








 






/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0052, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMREQUEST_QUEUE.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0053, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0054, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_01
On  
   PLN_PDMREQUEST_QUEUE
  (
    FK_PLANUSER_CHANGE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0055, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_PDMDATABASE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PUR_FK02
  Foreign Key
  (
    FK_PDMDATABASE
  )
  References
    PLN_PDMDATABASE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0056, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_PDMDATABASE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_02
On  
   PLN_PDMREQUEST_QUEUE
  (
    FK_PDMDATABASE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0057, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_ACCOUNT_REQUESTOR' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PUR_FK03
  Foreign Key
  (
    FK_ACCOUNT_REQUESTOR
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0058, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_ACCOUNT_REQUESTOR' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_03
On  
   PLN_PDMREQUEST_QUEUE
  (
    FK_ACCOUNT_REQUESTOR
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0059, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.PLN_PRE_PLC_FK01' */

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMREQUESTTYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0060, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_PLC_PDMREQUESTTYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_04
On  
   PLN_PDMREQUEST_QUEUE
  (
     FK_PLC_PDMREQUESTTYPE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0061, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMREQUEST_QUEUE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMREQUEST_QUEUE
Add
  Constraint
    PLN_PRE_BSE_FK01
  Foreign Key
  (
    FK_BOSTATE
  )
  References
    PLN_BOSTATE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0062, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMREQUEST_QUEUE.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PRE_05
On  
   PLN_PDMREQUEST_QUEUE
  (
     FK_BOSTATE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0063, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMREQUEST_QUEUE.PLN_PRE_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PRE_I
  On
    PLN_PDMREQUEST_QUEUE
  For Insert
  As
  Begin
    UPDATE PLN_PDMREQUEST_QUEUE
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMREQUEST_QUEUE ON Inserted.SYSCODE = PLN_PDMREQUEST_QUEUE.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0064, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMREQUEST_QUEUE.PLN_PRE_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PRE_U
  On
    PLN_PDMREQUEST_QUEUE
  For Update
  As
  Begin
    Update
      PLN_PDMREQUEST_QUEUE
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMREQUEST_QUEUE
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMREQUEST_QUEUE.SYSCODE
 
 Update
      PLN_PDMREQUEST_QUEUE
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMREQUEST_QUEUE
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMREQUEST_QUEUE.SYSCODE) And (PLN_PDMREQUEST_QUEUE.UPDATECOUNT >= 2147483647)
  End








/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0065, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMCOMPATIBILITYMATRIX.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0066, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMCOMPATIBILITYMATRIX.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0067, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMCOMPATIBILITYMATRIX.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PCX_01
On  
   PLN_PDMCOMPATIBILITYMATRIX
  (
    FK_PLANUSER_CHANGE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0068, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PDB_FK01
  Foreign Key
  (
    FK_PDMDATABASEVERSION
  )
  References
    PLN_PDMDATABASEVERSION
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0069, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASEVERSION' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PCX_02
On  
   PLN_PDMCOMPATIBILITYMATRIX
  (
    FK_PDMDATABASEVERSION
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0070, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMCOMPATIBILITYMATRIX
Add
  Constraint
    PLN_PCX_PDV_FK01
  Foreign Key
  (
    FK_PDMDATABASESERVER
  )
  References
    PLN_PDMDATABASESERVER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0071, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMCOMPATIBILITYMATRIX.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PCX_03
On  
   PLN_PDMCOMPATIBILITYMATRIX
  (
    FK_PDMDATABASESERVER
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0072, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMCOMPATIBILITYMATRIX.PLN_PCX_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PCX_I
  On
    PLN_PDMCOMPATIBILITYMATRIX
  For Insert
  As
  Begin
    UPDATE PLN_PDMCOMPATIBILITYMATRIX
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMCOMPATIBILITYMATRIX ON Inserted.SYSCODE = PLN_PDMCOMPATIBILITYMATRIX.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0073, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMCOMPATIBILITYMATRIX.PLN_PCX_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PCX_U
  On
    PLN_PDMCOMPATIBILITYMATRIX
  For Update
  As
  Begin
    Update
      PLN_PDMCOMPATIBILITYMATRIX
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMCOMPATIBILITYMATRIX
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMCOMPATIBILITYMATRIX.SYSCODE
 
 Update
      PLN_PDMCOMPATIBILITYMATRIX
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMCOMPATIBILITYMATRIX
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMCOMPATIBILITYMATRIX.SYSCODE) And (PLN_PDMCOMPATIBILITYMATRIX.UPDATECOUNT >= 2147483647)
  End






/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0074, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMTASK.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0075, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMTASK.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0076, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMTASK.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PTK_01
On  
   PLN_PDMTASK
  (
    FK_PLANUSER_CHANGE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0077, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMTASK.FK_PDMREQUEST' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_PRE_FK01
  Foreign Key
  (
    FK_PDMREQUEST
  )
  References
    PLN_PDMREQUEST_QUEUE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0078, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMTASK.FK_PDMREQUEST' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PTK_02
On  
   PLN_PDMTASK
  (
    FK_PDMREQUEST
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0079, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMTASK.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMTASK
Add
  Constraint
    PLN_PTK_BSE_FK01
  Foreign Key
  (
    FK_BOSTATE
  )
  References
    PLN_BOSTATE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0080, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMTASK.FK_BOSTATE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PTK_03
On  
   PLN_PDMTASK
  (
     FK_BOSTATE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0081, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMTASK.PLN_PTK_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PTK_I
  On
    PLN_PDMTASK
  For Insert
  As
  Begin
    UPDATE PLN_PDMTASK
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMTASK ON Inserted.SYSCODE = PLN_PDMTASK.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0082, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMTASK.PLN_PTK_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PTK_U
  On
    PLN_PDMTASK
  For Update
  As
  Begin
    Update
      PLN_PDMTASK
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMTASK
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMTASK.SYSCODE
 
 Update
      PLN_PDMTASK
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMTASK
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMTASK.SYSCODE) And (PLN_PDMTASK.UPDATECOUNT >= 2147483647)
  End









/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0083, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDBSERVER_DATAFILELOC.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Add
  Constraint
    PLN_PSC_PK
  Primary Key
  (
    SYSCODE
  )




/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0084, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBSERVER_DATAFILELOC.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Add
  Constraint
    PLN_PSC_PUR_FK01
  Foreign Key
  (
    FK_PLANUSER_CHANGE
  )
  References
    PLANUSER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0085, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBSERVER_DATAFILELOC.FK_PLANUSER_CHANGE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PSC_01
On  
   PLN_PDMDBSERVER_DATAFILELOC
  (
    FK_PLANUSER_CHANGE
  )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0086, IgnoreError=No */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBSERVER_DATAFILELOC.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBSERVER_DATAFILELOC
Add
  Constraint
    PLN_PSC_PDV_FK01
  Foreign Key
  (
    FK_PDMDATABASESERVER
  )
  References
    PLN_PDMDATABASESERVER
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0087, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBSERVER_DATAFILELOC.FK_PDMDATABASESERVER' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PSC_02
On  
   PLN_PDMDBSERVER_DATAFILELOC
  (
    FK_PDMDATABASESERVER
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0088, IgnoreError=No */
/*~!~Step, Desc = 'Create trigger PLN_PDMDBSERVER_DATAFILELOC.PLN_PSC_I' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PSC_I
  On
    PLN_PDMDBSERVER_DATAFILELOC
  For Insert
  As
  Begin
    UPDATE PLN_PDMDBSERVER_DATAFILELOC
    SET INSERTDATE = Getdate()
    FROM Inserted
    INNER JOIN PLN_PDMDBSERVER_DATAFILELOC ON Inserted.SYSCODE = PLN_PDMDBSERVER_DATAFILELOC.SYSCODE
  End

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0089, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDBSERVER_DATAFILELOC.PLN_PSC_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PSC_U
  On
    PLN_PDMDBSERVER_DATAFILELOC
  For Update
  As
  Begin
    Update
      PLN_PDMDBSERVER_DATAFILELOC
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1,
      CHANGEDATE  = Current_TimeStamp
      
    From
      PLN_PDMDBSERVER_DATAFILELOC
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMDBSERVER_DATAFILELOC.SYSCODE
 
 Update
      PLN_PDMDBSERVER_DATAFILELOC
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMDBSERVER_DATAFILELOC
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMDBSERVER_DATAFILELOC.SYSCODE) And (PLN_PDMDBSERVER_DATAFILELOC.UPDATECOUNT >= 2147483647)
  End





/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0090, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMDBTYPE_PDMFILETYPE.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDBTYPE_PDMFILETYPE
Add
  Constraint
    PLN_PPE_PK
  Primary Key
  (
    SYSCODE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0091, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBTYPE_PDMFILETYPE.PLN_PPE_PLC_FK01' */

Alter Table
  PLN_PDMDBTYPE_PDMFILETYPE
Add
  Constraint
    PLN_PPE_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMDATABASETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0092, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBTYPE_PDMFILETYPE.FK_PLC_PDMDATABASETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PPE_01
On  
   PLN_PDMDBTYPE_PDMFILETYPE
  (
     FK_PLC_PDMDATABASETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0093, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDBTYPE_PDMFILETYPE.PLN_PPE_PLC_FK02' */

Alter Table
  PLN_PDMDBTYPE_PDMFILETYPE
Add
  Constraint
    PLN_PPE_PLC_FK02
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0094, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMDBTYPE_PDMFILETYPE.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PPE_02
On  
   PLN_PDMDBTYPE_PDMFILETYPE
  (
     FK_PLC_PDMFILETYPE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0095, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMDBTYPE_PDMFILETYPE.PLN_PPE_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PPE_U
  On
    PLN_PDMDBTYPE_PDMFILETYPE
  For Update
  As
  Begin
    Update
      PLN_PDMDBTYPE_PDMFILETYPE
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1
      
    From
      PLN_PDMDBTYPE_PDMFILETYPE
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMDBTYPE_PDMFILETYPE.SYSCODE
 
 Update
      PLN_PDMDBTYPE_PDMFILETYPE
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMDBTYPE_PDMFILETYPE
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMDBTYPE_PDMFILETYPE.SYSCODE) And (PLN_PDMDBTYPE_PDMFILETYPE.UPDATECOUNT >= 2147483647)
  End






/*-----------------------------------------------------------------------------------*/
/*~!~Step, Nr=0096, IgnoreError=No                                                   */
/*~!~Step, Desc = 'Create primary key PLN_PDMANALYZEDFILETYPES.SYSCODE'             */
/*-----------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMANALYZEDFILETYPES
Add
  Constraint
    PLN_PAF_PK
  Primary Key
  (
    SYSCODE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0097, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMANALYZEDFILETYPES.PLN_PAF_PLC_FK01' */

Alter Table
  PLN_PDMANALYZEDFILETYPES
Add
  Constraint
    PLN_PAF_PLC_FK01
  Foreign Key
  (
    FK_PLC_PDMFILETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0098, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMANALYZEDFILETYPES.FK_PLC_PDMFILETYPE' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PAF_01
On  
   PLN_PDMANALYZEDFILETYPES
  (
     FK_PLC_PDMFILETYPE
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0099, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMANALYZEDFILETYPES.PLN_PAF_PRE_FK01' */

Alter Table
  PLN_PDMANALYZEDFILETYPES
Add
  Constraint
    PLN_PAF_PRE_FK01
  Foreign Key
  (
    FK_PDMREQUEST
  )
  References
    PLN_PDMREQUEST_QUEUE
    (
      SYSCODE
    )



/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0100, IgnoreError=No */
/*~!~Step, Desc = 'Create index PLN_PDMANALYZEDFILETYPES.FK_PDMREQUEST' */
/*---------------------------------------------------------------------------*/

Create Index
  PLN_PAF_02
On  
   PLN_PDMANALYZEDFILETYPES
  (
     FK_PDMREQUEST
  )


/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0101, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PLN_PDMANALYZEDFILETYPES.PLN_PAF_U' */
/*---------------------------------------------------------------------------*/

 CREATE Trigger
    PLN_PAF_U
  On
    PLN_PDMANALYZEDFILETYPES
  For Update
  As
  Begin
    Update
      PLN_PDMANALYZEDFILETYPES
    Set
      UPDATECOUNT = DELETED.UPDATECOUNT + 1
      
    From
      PLN_PDMANALYZEDFILETYPES
      Inner Join DELETED On DELETED.SYSCODE = PLN_PDMANALYZEDFILETYPES.SYSCODE
 
 Update
      PLN_PDMANALYZEDFILETYPES
    Set
      UPDATECOUNT = 0
    From
      PLN_PDMANALYZEDFILETYPES
      Inner Join DELETED On (DELETED.SYSCODE = PLN_PDMANALYZEDFILETYPES.SYSCODE) And (PLN_PDMANALYZEDFILETYPES.UPDATECOUNT >= 2147483647)
  End


   
