/**************************************************************************************************/
/*~!~Section, Nr=0001, Desc = 'TEC-4115, PDM3 - Oracle user permissions for upgrade'    */
/**************************************************************************************************/
 
/*-----------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx 				    							     */
/*~!~Step, Desc = 'Adding new entry - ADMIN for PDMPRIVILEGETYPE'				    	         */
/*-----------------------------------------------------------------------------------------------*/

DECLARE
    newSyscode               Number;
Begin
    Pln_Getnewid('PLANCODE',newSyscode);
    Insert Into PLANCODE (SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY) 
    values (newSyscode,  'A', 'ADMIN', 'PDMPRIVILEGETYPE', '');

End;

