/**************************************************************************************************/
/*~!~Section, Nr=0001, Desc = 'TEC-2302, PDM3: Loading a DB with a user with different rights'    */
/**************************************************************************************************/
 
/*-----------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx 				    							     */
/*~!~Step, Desc = 'Adding data or entries for PDMPRIVILEGETYPE'				    	             */
/*-----------------------------------------------------------------------------------------------*/

BEGIN
	DECLARE @newSyscode Integer;
    
	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'S', 'STANDARD', 'PDMPRIVILEGETYPE', '');

	Exec Pln_Getnewid 'PLANCODE', @newSyscode Output
	INSERT INTO PLANCODE(SYSCODE, CODE, NAAM, GRP, X_NAAM_TRANSKEY)
	VALUES (@newSyscode, 'U', 'UPGRADE', 'PDMPRIVILEGETYPE', '');

END

/*-----------------------------------------------------------------------------------------------*/
/*~!~Step, Nr=0002, IgnoreError=No, DBMR=xxx 				    							     */
/*~!~Step, Desc = 'Adding new column FK_PLC_PDMPRIVILEGETYPE to PLN_PDMDATABASE'	             */
/*-----------------------------------------------------------------------------------------------*/

ALTER TABLE
	PLN_PDMDATABASE
ADD
	FK_PLC_PDMPRIVILEGETYPE		INTEGER NULL
	

	
/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0003, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PLN_PDMDATABASE.PLN_PDD_PLC_FK04' */
/*-----------------------------------------------------------------------------------------------*/

Alter Table
  PLN_PDMDATABASE
Add
  Constraint
    PLN_PDD_PLC_FK04
  Foreign Key
  (
    FK_PLC_PDMPRIVILEGETYPE
  )
  References
    PLANCODE
    (
      SYSCODE
    )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0004, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PLN_PDMDATABASE.FK_PLC_PDMPRIVILEGETYPE'  */
/*-----------------------------------------------------------------------------------------------*/  

Create Index
  PLN_PDD_12
On  
   PLN_PDMDATABASE
  (
     FK_PLC_PDMPRIVILEGETYPE
  )

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0005, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Update existing records of PLN_PDMDATABASE.FK_PLC_PDMPRIVILEGETYPE'  */
/*-----------------------------------------------------------------------------------------------*/  
  
UPDATE PLN_PDMDATABASE SET FK_PLC_PDMPRIVILEGETYPE=(SELECT SYSCODE FROM PLANCODE WHERE NAAM='UPGRADE' AND GRP='PDMPRIVILEGETYPE')	

