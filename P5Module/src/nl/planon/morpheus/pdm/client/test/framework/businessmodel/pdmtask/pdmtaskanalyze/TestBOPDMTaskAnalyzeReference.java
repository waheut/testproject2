// Planon Enterprise Edition Source file: TestBOPDMTaskAnalyzeReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskanalyze;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMTaskAnalyzeReference
 */
public class TestBOPDMTaskAnalyzeReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskAnalyzeReference object.
   */
  public TestBOPDMTaskAnalyzeReference()
  {
    super(BOTypePDMP5Module.PDM_TASK_ANALYZE_IMPORT);
  }
}
