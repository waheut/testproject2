// Planon Enterprise Edition Source file: TestBOPDMDatabaseServerOracleReference.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle;

import nl.planon.dionysus.framework.field.*;

/**
 * TestBOPDMDatabaseServerReference
 */
public class TestBOPDMDatabaseServerOracleReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseServerReference object.
   */
  public TestBOPDMDatabaseServerOracleReference()
  {
    super(TestBOPDMDatabaseServerOracleFactory.BOTYPE);
  }
}
