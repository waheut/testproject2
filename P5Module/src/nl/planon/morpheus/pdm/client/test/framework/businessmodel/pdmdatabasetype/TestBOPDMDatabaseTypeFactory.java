// Planon Enterprise Edition Source file: TestBOPDMDatabaseTypeFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;

import nl.planon.pdm.core.common.*;

/**
 * TestBOPDMDatabaseTypeFactory
 */
public class TestBOPDMDatabaseTypeFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final IBOType BOTYPE = TestBOPDMDatabaseType.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseTypeFactory object.
   */
  public TestBOPDMDatabaseTypeFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * given the code of the pdm database type, return its primary key in the database
   *
   * @param  aCode database type code
   *
   * @return primary key of database type
   *
   * @throws PnErrorListException
   * @throws RuntimeException
   */
  public static final IBaseValue getPDMDatabaseTypeByCode(String aCode) throws PnErrorListException
  {
    IBaseValue result = null;
    try
    {
      ISearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMDATABASETYPE);
      ISCOperator code = sc.getOperatorEx(BOPDMDatabaseTypeDef.PN_CODE, SCOperatorType.EQUAL);
      code.addValue(aCode);
      IProxyListValue proxyListValue = createBySearchCriteriaEx(sc);
      IProxyValue pdmDatabaseType = proxyListValue.getFirstProxy();

      result = pdmDatabaseType.getPrimaryKeyValue();
    }
    catch (FieldNotFoundException ex)
    {
      throw new RuntimeException(ex);
    }

    return result;
  }


  /**
   * gets pk of MSSQL PDMDatabaseType
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static final IBaseValue getPDMDatabaseTypeMssql() throws PnErrorListException
  {
    return getPDMDatabaseTypeByCode(PDMDatabaseType.MSSQL.getCode());
  }


  /**
   * gets pk of Oracle PDMDatabaseType
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static final IBaseValue getPDMDatabaseTypeOracle() throws PnErrorListException
  {
    return getPDMDatabaseTypeByCode(PDMDatabaseType.ORACLE.getCode());
  }


  /**
   * Creates a new TestBOPDMDatabaseType object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField IBOFieldValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDatabaseType
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMDatabaseType readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMDatabaseType object containing data of specified primary key. Generates
   * a runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDatabaseType
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMDatabaseType readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMDatabaseType pdmDatabaseType = getReadOnlyTestBO(boValue);
    return pdmDatabaseType;
  }


  /**
   * Creates a new TestBOPDMDatabaseType object containing data of value in specified primary key
   * field.
   *
   * @param  aPrimaryKeyField TestBOFieldValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDatabaseType
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMDatabaseType readInstance(TestBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Returns a TestBOPDMDatabaseType object containing data aBOValue which is not destroyed after
   * the test.
   *
   * @param  aBOValue the BOValue the value to be used
   *
   * @return TestBOPDMDatabaseType the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMDatabaseType getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMDatabaseType pdmDatabaseType = new TestBOPDMDatabaseType(aBOValue);
    pdmDatabaseType.setAutomaticFree(false);
    return pdmDatabaseType;
  }
}
