// Planon Enterprise Edition Source file: TestBOPDMRequestTypeReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequesttype;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestTypeReference
 */
public class TestBOPDMRequestTypeReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestTypeReference object.
   */
  public TestBOPDMRequestTypeReference()
  {
    super(BOTypePDMP5Module.PDM_REQUEST_TYPE);
  }
}
