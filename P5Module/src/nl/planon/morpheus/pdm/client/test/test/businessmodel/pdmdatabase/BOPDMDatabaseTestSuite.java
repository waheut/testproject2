// Planon Enterprise Edition Source file: BOPDMDatabaseTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.bom.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.fieldchange.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.bom.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation.*;
import nl.planon.morpheus.pdm.client.test.test.request.mssql.*;
import nl.planon.morpheus.pdm.client.test.test.request.oracle.*;

/**
 * BOPDMDatabaseTestSuite
 */
public class BOPDMDatabaseTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseTestSuite object.
   */
  public BOPDMDatabaseTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMDatabaseBOMAvailableTest.class);
    addTestSuite(BRAllowDeleteDatabaseBOTest.class);
    addTestSuite(BRAllowImportDatabaseTest.class);

    // field change tests
    addTestSuite(FCTakeOverDatabaseServerFieldsTest.class);

    // validation rule tests
    addTestSuite(VRCannotDeleteNonInitialPDMDatabaseTest.class);
    addTestSuite(VRDbTypeEqualToVersionDbTypeTest.class);
    addTestSuite(VROnlyWordCharsAllowedInFieldCodeTest.class);
    addTestSuite(VRNoImportWithMultipleSchemaNamesTest.class);
    addTestSuite(VRPDMLocationEqualToPDMLocationDBServerTest.class);
    addTestSuite(VRDbTypeMatchesFileTypeAndDbServerTypeTest.class);
    addTestSuite(VRDatabaseToRequestStateTest.class);
    // generic test
    addTestSuite(BOPDMDatabaseTest.class);
    // pdm requests tests
    addTestSuite(OracleDatapumpRequestTest.class);
    addTestSuite(OracleOldStyleRequestTest.class);
    addTestSuite(MSSQLRequestTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseTestSuite object.
   *
   * @return BOPDMDatabaseTestSuite
   */
  public static Test suite()
  {
    return new BOPDMDatabaseTestSuite();
  }
}
