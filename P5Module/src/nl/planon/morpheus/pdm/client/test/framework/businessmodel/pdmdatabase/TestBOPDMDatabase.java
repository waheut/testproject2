// Planon Enterprise Edition Source file: TestBOPDMDatabase.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase;

import junit.framework.*;

import java.io.*;
import java.util.*;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.dionysus.businessmodeltest.client.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.util.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.common.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * TestBOPDMDatabase
 */
public class TestBOPDMDatabase extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = TestBOPDMDatabaseFactory.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabase object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabase() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMDatabase object.
   *
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabase(IBOValue aValue) throws PnErrorListException
  {
    super(BOTYPE, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * replacement for up- and downloading files via WebDAV, that normally is done automatically via
   * an Executer when f.e. saving a PDM database. That does not work in the JUnit tests, so here it
   * is done explicitly by copying
   *
   * @return File
   *
   * @throws IOException
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public String copyDumpFileToWebDAVLocation() throws IOException, AuthorizationException, PnErrorListException
  {
    String destinationFileName = generateDestinationFileName();
    String destinationPathAndFile = PDMTestFieldValues.ABSOLUTE_WEBDAV_LOCATION + destinationFileName;
    File f1 = new File(this.getFieldDumpFile().getAsString());
    File f2 = new File(destinationPathAndFile);

    //create output path
    File parent = f2.getParentFile();
    if (!parent.exists())
    {
      parent.mkdirs();
    }
    InputStream in = new FileInputStream(f1);

    //For Overwrite the file.
    OutputStream out = new FileOutputStream(f2);

    byte[] buf = new byte[1024];
    int len;
    while ((len = in.read(buf)) > 0)
    {
      out.write(buf, 0, len);
    }
    in.close();
    out.close();

    return PDMTestFieldValues.PDMLOCATION_LOCATION_TEST_VALUE + destinationFileName;
  }


  /**
   * Execute clean up pdm database BOM.
   *
   * @throws PnErrorListException
   */
  public void executeCleanupPDMDatabaseBOM() throws PnErrorListException
  {
    final IBOM bom = getBOMByPnName(IBOPDMDatabaseDef.PN_BOM_DELETE_DATABASE);
    this.boValue = executeIBOM(bom);
  }


  /**
   * Execute export pdm database BOM.
   *
   * @throws PnErrorListException
   */
  public void executeExportPDMDatabaseBOM() throws PnErrorListException
  {
    final IBOM bom = getBOMByPnName(IBOPDMDatabaseDef.PN_BOM_EXPORT_DATABASE);
    this.boValue = executeIBOM(bom);
  }


  /**
   * Execute import pdm database BOM.
   *
   * @throws PnErrorListException
   */
  public void executeImportPDMDatabaseBOM() throws PnErrorListException
  {
    final IBOM bom = getBOMByPnName(IBOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE);
    this.boValue = executeIBOM(bom);
  }


  /**
   * Execute reload exported dump PDM database BOM.
   *
   * @throws PnErrorListException
   */
  public void executeReloadExportedPDMDatabaseBOM() throws PnErrorListException
  {
    final IBOM bom = getBOMByPnName(IBOPDMDatabaseDef.PN_BOM_RELOAD_EXPORTED_DATABASE);
    this.boValue = executeIBOM(bom);
  }


  /**
   * Execute reload initial dump PDM database BOM.
   *
   * @throws PnErrorListException
   */
  public void executeReloadInitialPDMDatabaseBOM() throws PnErrorListException
  {
    final IBOM bom = getBOMByPnName(IBOPDMDatabaseDef.PN_BOM_RELOAD_INITIAL_DATABASE);
    this.boValue = executeIBOM(bom);
  }


  /**
   * The destination file name on the file server is composed of the primary key of the linked PDM
   * database, followed by "_Import" and completed by the same extension as the original dump file.
   *
   * @return the generated dump file name
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public String generateDestinationFileName() throws PnErrorListException, AuthorizationException
  {
    String destinationFileName = this.getFieldPrimaryKey().getAsString() + WebDAVUtilities.DUMPFILE_FOR_IMPORT;

    String localFileName = this.getFieldDumpFile().getAsString();
    // use the same extension, if any
    int indexOfExtension = localFileName.lastIndexOf(".");
    if (indexOfExtension > -1)
    {
      destinationFileName = destinationFileName + localFileName.substring(indexOfExtension, localFileName.length());
    }
    return destinationFileName;
  }


  /**
   * convenience method for field PN_ACCESSCODE
   *
   * @return field PN_ACCESSCODE
   */
  public TestBOFieldValue getFieldAccessCode()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_ACCESSCODE);
  }


  /**
   * convenience method for field PN_ACCOUNT_DATABASEOWNER_REF
   *
   * @return field PN_ACCOUNT_DATABASEOWNER_REF
   */
  public TestBOFieldValue getFieldAccountDatabaseOwnerRef()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_ACCOUNT_DATABASEOWNER_REF);
  }


  /**
   * convenience method for field PN_DATABASESERVER_REF
   *
   * @return field PN_DATABASESERVER_REF
   */
  public TestBOFieldValue getFieldDatabaseServerRef()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
  }


  /**
   * convenience method for field PN_REF_BOSTATE
   *
   * @return field PN_REF_BOSTATE
   */
  public TestBOFieldValue getFieldDatabaseStatus()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_REF_BOSTATE);
  }


  /**
   * convenience method for field PN_DATABASE_TYPE
   *
   * @return field PN_DATABASE_TYPE
   */
  public TestBOFieldValue getFieldDatabaseType()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF);
  }


  /**
   * convenience method for field PN_DESCRIPTION
   *
   * @return field PN_DESCRIPTION
   */
  public TestBOFieldValue getFieldDescription()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_DESCRIPTION);
  }


  /**
   * convenience method for field PN_DUMPFILE
   *
   * @return field PN_DUMPFILE
   */
  public TestBOFieldValue getFieldDumpFile()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_DUMPFILE);
  }


  /**
   * convenience method for field PN_FILE_TYPE_REF
   *
   * @return field PN_FILE_TYPE_REF
   */
  public TestBOFieldValue getFieldFileTypeRef()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
  }


  /**
   * convenience method for field PN_PDMLOCATION_REF
   *
   * @return field PN_PDMLOCATION_REF
   */
  public TestBOFieldValue getFieldLocationRef()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_PDMLOCATION_REF);
  }


  /**
   * convenience method for field PN_NAME
   *
   * @return field PN_NAME
   */
  public TestBOFieldValue getFieldName()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_NAME);
  }


  /**
   * convenience method for field PN_OBJECT_OWNER_IN_DUMP
   *
   * @return field PN_OBJECT_OWNER_IN_DUMP
   */
  public TestBOFieldValue getFieldObjectOwnerInDump()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_OBJECT_OWNER_IN_DUMP);
  }


  /**
   * convenience method for field PN_PDM_DUMPFILE
   *
   * @return field PN_PDM_DUMPFILE
   */
  public TestBOFieldValue getFieldPDMDumpFile()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_PDM_DUMPFILE);
  }


  /**
   * convenience method for field PN_PDM_EXPORTFILE
   *
   * @return field PN_PDM_EXPORTFILE
   */
  public TestBOFieldValue getFieldPDMExportFile()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE);
  }


  /**
   * convenience method for field PN_TABLESPACENAMES_IN_DUMP
   *
   * @return field PN_TABLESPACENAMES_IN_DUMP
   */
  public TestBOFieldValue getFieldTableSpaceNamesInDump()
  {
    return getFieldByPnName(IBOPDMDatabaseDef.PN_TABLESPACENAMES_IN_DUMP);
  }


  /**
   * gets last added TestBORequest
   *
   * @return list of TestBOBasePDMRequest
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public TestBOBasePDMRequest getLastAddedRequest() throws AuthorizationException, PnErrorListException
  {
    return getRequests().get(0);
  }


  /**
   * gets last added BOPDMTask
   *
   * @return the last added BOPDMTask
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public TestBOBasePDMTask getLastAddedTask() throws AuthorizationException, PnErrorListException
  {
    return getRequests().get(0).getTasks().get(0);
  }


  /**
   * convenience method to get a list of all requests linked to this database, ordered descending by
   * the date the requests are inserted
   *
   * @return a list of requests
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public List<TestBOBasePDMRequest> getRequests() throws PnErrorListException, AuthorizationException
  {
    TestSearchCriteria sc = TestSearchCriteria.getPVSearchCriteria(PVTypePDMP5Module.PDMREQUEST);
    sc.setOperatorEx(BOBasePDMRequestDef.PN_DATABASE_REF, SCOperatorType.EQUAL, this);

    final TestProxyListValue plv = sc.getProxyListvalue();
    ProxyValueComparator comparator = new ProxyValueComparator(plv);
    //comparator.addSortBOType(SortDirection.ASCENDING);
    comparator.addSortItem(IBOBasePDMRequestDef.PN_PRIMARYKEY, SortDirection.DESCENDING);
    plv.sort(comparator);
    List<TestBOBasePDMRequest> result = new ArrayList<TestBOBasePDMRequest>();
    for (TestProxyValue pv : plv.getTestProxyValues())
    {
      TestBOBasePDMRequest pdmRequest = TestBOBasePDMRequestFactory.readInstance(pv.getPrimaryKeyValue());
      result.add(pdmRequest);
    }
    return result;
  }


  /**
   * gets list of TestBORequest and verifies expected size
   *
   * @param  aExpectedSize list should be of this size
   *
   * @return list of TestBOBasePDMRequest
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public List<TestBOBasePDMRequest> getRequests(int aExpectedSize) throws AuthorizationException, PnErrorListException
  {
    List<TestBOBasePDMRequest> result = getRequests();
    Assert.assertEquals(aExpectedSize, result.size());

    return result;
  }


  /**
   * normally when saving a PDM Database and executer is invoked for uploading the dumpfile via
   * WebDAV to the file server. In the JUnit-test this doesn't work. Therefore an explicit copy of
   * the original dump file to the file server is done here.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws IOException
   */
  public void saveAndUploadDumpFile() throws PnErrorListException, AuthorizationException, IOException
  {
    this.save();
    String remoteDumpFile = copyDumpFileToWebDAVLocation();
    this.getFieldPDMDumpFile().setStringValue(remoteDumpFile);
    this.save();
  }
}
