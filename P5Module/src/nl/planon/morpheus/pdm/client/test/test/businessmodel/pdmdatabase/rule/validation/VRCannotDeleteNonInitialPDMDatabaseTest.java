// Planon Enterprise Edition Source file: VRCannotDeleteNonInitialPDMDatabaseTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;

/**
 * VRCannotDeleteNonInitialPDMDatabaseTest
 */
public class VRCannotDeleteNonInitialPDMDatabaseTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRCannotDeleteNonInitialPDMDatabaseTest object.
   *
   * @param aName
   */
  public VRCannotDeleteNonInitialPDMDatabaseTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  // Following test is disabled, because it is already covered in
  // BRAllowDeleteDatabaseBOTest
  // /**
  // * Cannot delete a PDM database that is not in state Initial. If a PDM
  // database is not in state
  // * initial, it means that it is imported somewhere and not cleaned up yet.
  // *
  // * @throws PnErrorListException
  // * @throws AuthorizationException
  // */
  // public void testDeleteAvailablePDMDatabaseFail() throws
  // PnErrorListException, AuthorizationException
  // {
  // TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
  // pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_BUSY);
  // pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_AVAILABLE);
  // try
  // {
  // pdmDatabase.delete();
  // failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_DELETE_NON_INITIAL_PDMDATABASE,
  // "Not allowed to delete PDM Database that is in state Available.");
  // }
  // catch (PnErrorListException ex)
  // {
  // assertContainsMessageType(ex,
  // ErrorNumberPDMP5Module.EC_CANNOT_DELETE_NON_INITIAL_PDMDATABASE);
  // }
  // }

  // Following test is disabled, because it is already covered in
  // BRAllowDeleteDatabaseBOTest
  // /**
  // * Cannot delete a PDM database that is in state Busy.
  // *
  // * @throws PnErrorListException
  // * @throws AuthorizationException
  // */
  // public void testDeleteBusyPDMDatabaseFail() throws PnErrorListException,
  // AuthorizationException
  // {
  // TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
  // pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_BUSY);
  // try
  // {
  // pdmDatabase.delete();
  // failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_DELETE_NON_INITIAL_PDMDATABASE,
  // "Not allowed to delete PDM Database that is in state Busy.");
  // }
  // catch (PnErrorListException ex)
  // {
  // assertContainsMessageType(ex,
  // ErrorNumberPDMP5Module.EC_CANNOT_DELETE_NON_INITIAL_PDMDATABASE);
  // }
  // }

  /**
   * Can delete a PDM Database that is in initial state.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeletePDMDatabaseIniInitialStateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    try
    {
      pdmDatabase.delete();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
