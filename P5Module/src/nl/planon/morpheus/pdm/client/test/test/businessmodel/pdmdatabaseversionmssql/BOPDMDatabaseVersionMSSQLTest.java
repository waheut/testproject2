// Planon Enterprise Edition Source file: BOPDMDatabaseVersionMSSQLTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionmssql;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;

/**
 * BOPDMDatabaseVersionTest
 */
public class BOPDMDatabaseVersionMSSQLTest extends BOPDMDatabaseVersionMSSQLTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseVersionMSSQLTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of BOPDMDatabaseVersionTest
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionMSSQLFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseVersionTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO_2 = TestBOPDMDatabaseVersionMSSQLFactory.readInstance(primaryKey);
      pdmDatabaseVersionTestBO_2.read();
      pdmDatabaseVersionTestBO_2.delete();

      pdmDatabaseVersionTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMDatabaseVersion
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();

    pdmDatabaseVersionTestBO.read();
    pdmDatabaseVersionTestBO.delete();
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();
    IBOFieldValue primaryKey = pdmDatabaseVersionTestBO.getPrimaryKeyField();

    pdmDatabaseVersionTestBO.read();
    pdmDatabaseVersionTestBO.delete();

    try
    {
      TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO_2 = TestBOPDMDatabaseVersionMSSQLFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMDatabaseVersion.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseVersionTestBO.getPrimaryKeyField();
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO_2 = TestBOPDMDatabaseVersionMSSQLFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMDatabaseVersion.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();

    pdmDatabaseVersionTestBO.getFieldCode().setValueRandom();
    pdmDatabaseVersionTestBO.save();
  }
}
