// Planon Enterprise Edition Source file: TestBOPDMTaskImport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskimport;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMTaskImport
 */
public class TestBOPDMTaskImport extends TestBOBasePDMTask
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_TASK_IMPORT;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskImport object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskImport() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMTaskImport object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskImport(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }


  /**
   * Creates a new TestBOPDMTaskImport object.
   *
   * @param  aBOType
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskImport(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOPDMTaskImport object.
   *
   * @param  aBOType
   * @param  aBOValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskImport(IBOType aBOType, IBOValue aBOValue) throws PnErrorListException
  {
    super(aBOType, aBOValue);
  }
}
