// Planon Enterprise Edition Source file: VRNoSpacesesAllowedInFieldsFileLocationsTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.*;

/**
 * VRNoSpacesesAllowedInFieldsFileLocationsTest
 */
public class VRNoSpacesesAllowedInFieldsFileLocationsTest extends BOPDMDBServerDataFileLocationTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRNoSpacesesAllowedInFieldsFileLocationsTest object.
   *
   * @param aName
   */
  public VRNoSpacesesAllowedInFieldsFileLocationsTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * test if there occur a error when the user use a space in field FileLocationsLocal
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testLocalFieldNoSpaceAllowed() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDatabase = TestBOPDMDBServerDataFileLocationFactory.randomInstance();
    pdmDatabase.getFieldByPnName(BOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL).setValue("ds ds"); // with space
    try
    {
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_NO_SPACE_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_NO_SPACE_ALLOWED);
    }
  }


  /**
   * test if there occur a error when the user use a space in field FileLocationsLocal
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testNetwerkFieldNoSpaceAllowed() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDatabase = TestBOPDMDBServerDataFileLocationFactory.randomInstance();
    pdmDatabase.getFieldByPnName(BOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_NETWORK).setValue("ds ds"); // with space
    try
    {
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_NO_SPACE_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_NO_SPACE_ALLOWED);
    }
  }
}
