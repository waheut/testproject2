// Planon Enterprise Edition Source file: TestBOPDMDBServerDataFileLocationReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation;

import nl.planon.dionysus.framework.field.*;

/**
 * TestBOPDMDBServerDataFileLocationReference
 */
public class TestBOPDMDBServerDataFileLocationReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDBServerDataFileLocationReference object.
   */
  public TestBOPDMDBServerDataFileLocationReference()
  {
    super(TestBOPDMDBServerDataFileLocationFactory.BOTYPE);
  }
}
