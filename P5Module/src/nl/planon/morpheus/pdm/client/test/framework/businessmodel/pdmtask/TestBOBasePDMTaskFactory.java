// Planon Enterprise Edition Source file: TestBOBasePDMTaskFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskanalyze.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskdelete.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskexport.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskimport.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOBasePDMTaskFactory
 */
public class TestBOBasePDMTaskFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final IBOType BOTYPE = TestBOBasePDMTask.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOBaseRequestFactory object.
   */
  private TestBOBasePDMTaskFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * Creates a new TestBOPDMRequestImport object by the given IBOFieldValue
   *
   * @param  aFieldValue
   *
   * @return TestBOPDMRequestImport
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOBasePDMTask readInstance(IBOFieldValue aFieldValue) throws PnErrorListException
  {
    return readInstance(aFieldValue.getBaseValue());
  }


  /**
   * Creates a new TestBOBasePDMRequest object by the given IBaseValue
   *
   * @param  aBaseValue
   *
   * @return TestBOBasePDMRequest
   *
   * @throws PnErrorListException
   */
  public static final TestBOBasePDMTask readInstance(IBaseValue aBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aBaseValue);
    TestBOBasePDMTask testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new TestBOBasePDMRequest object by the given BOFieldValue
   *
   * @param  aBOFieldValue
   *
   * @return TestBOBasePDMRequest
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOBasePDMTask readInstance(TestBOFieldValue aBOFieldValue) throws PnErrorListException
  {
    return readInstance(aBOFieldValue.getBaseValue());
  }


  /**
   * Returns a TestBOPDMRequestImport object by the given IBOValue
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMRequestImport the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOBasePDMTask getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOBasePDMTask testBO = null;
    IBOType boType = aBOValue.getBOType();
    if (BOTypePDMP5Module.PDM_TASK_IMPORT.equals(boType))
    {
      testBO = new TestBOPDMTaskImport(aBOValue);
    }
    else if (BOTypePDMP5Module.PDM_TASK_EXPORT.equals(boType))
    {
      testBO = new TestBOPDMTaskExport(aBOValue);
    }
    else if (BOTypePDMP5Module.PDM_TASK_DELETE.equals(boType))
    {
      testBO = new TestBOPDMTaskDelete(aBOValue);
    }

    assert BOTypePDMP5Module.PDM_TASK_ANALYZE_IMPORT.equals(boType);

    testBO = new TestBOPDMTaskAnalyze(aBOValue);

    testBO.setAutomaticFree(false);
    return testBO;
  }
}
