// Planon Enterprise Edition Source file: BOPDMRequestImportBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMRequestImportBOMAvailableTest
 */
public class BOPDMRequestImportBOMAvailableTest extends BOPDMRequestImportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestImportBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMRequestImportBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test Log Request bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableLogRequestBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_LOG_PDMREQUEST);
  }


  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_SAVE);
  }


  /**
   * Test update db version field bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableUpdateDBversionFieldBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_UPDATE_DBVERSION_FIELD);
  }
}
