// Planon Enterprise Edition Source file: PDMControllerAgentTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseservermssql.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionmssql.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionoracle.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestexport.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskexport.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskimport.*;

/**
 * TestSuite for PDMControllerAgent
 */
public class PDMControllerAgentTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMControllerAgentTestSuite object.
   */
  public PDMControllerAgentTestSuite()
  {
    addTest(new BOPDMDatabaseVersionMSSQLTestSuite());
    addTest(new BOPDMDatabaseVersionOracleTestSuite());
    addTest(new BOPDMLocationTestSuite());
    addTest(new BOPDMDatabaseServerMSSQLTestSuite());
    addTest(new BOPDMCompatibilityMatrixTestSuite());
    addTest(new BOPDMDatabaseTestSuite());
    addTest(new BOPDMDBServerDataFileLocationTestSuite());
    addTest(new BOPDMRequestExportTestSuite());
    addTest(new BOPDMRequestImportTestSuite());
    addTest(new BOPDMTaskExportTestSuite());
    addTest(new BOPDMTaskImportTestSuite());
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns a new PDMControllerAgent test suite
   *
   * @return PDMControllerAgent testsuite
   */
  public static Test suite()
  {
    return new PDMControllerAgentTestSuite();
  }
}
