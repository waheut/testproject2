// Planon Enterprise Edition Source file: TestBOPDMDatabaseVersionOracle.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.*;

/**
 * TestBOPDMDatabaseVersion
 */
public class TestBOPDMDatabaseVersionOracle extends TestBOPDMDatabaseVersion
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = TestBOPDMDatabaseVersionOracleFactory.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseVersion object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseVersionOracle() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMDatabaseVersion object.
   *
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseVersionOracle(IBOValue aValue) throws PnErrorListException
  {
    super(BOTYPE, aValue);
  }
}
