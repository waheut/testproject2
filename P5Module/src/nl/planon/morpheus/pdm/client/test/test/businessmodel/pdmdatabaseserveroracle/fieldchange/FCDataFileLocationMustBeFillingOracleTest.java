// Planon Enterprise Edition Source file: FCDataFileLocationMustBeFillingOracleTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserveroracle.fieldchange;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserveroracle.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * FCSupportVersionsMustBeFillingTest
 */
public class FCDataFileLocationMustBeFillingOracleTest extends BOPDMDatabaseServerOracleTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new FCSupportVersionsMustBeFillingTest object.
   *
   * @param aName
   */
  public FCDataFileLocationMustBeFillingOracleTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * test if there occur a error when is active change to true and where is no supporterd version
   * assigend
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testChangeIsActiveToTrue() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabase = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDatabase.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PDMDBFILELOCATIONDETAIL).clear();
    pdmDatabase.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE).setBooleanValue(false);
    try
    {
      pdmDatabase.save();
      pdmDatabase.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE).setValue(true).fieldChange();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_NO_SUPPORTED_VERSION_ADDED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_NO_SUPPORTED_VERSION_ADDED);
    }
  }
}
