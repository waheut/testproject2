// Planon Enterprise Edition Source file: TestBOPDMTaskImportFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskimport;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

/**
 * TestBOPDMTaskImportFactory
 */
public class TestBOPDMTaskImportFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final IBOType BOTYPE = TestBOPDMTaskImport.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskImportFactory object.
   */
  private TestBOPDMTaskImportFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMTaskImport object.
   *
   * @return TestBOPDMTaskImport
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskImport newInstance() throws PnErrorListException, AuthorizationException
  {
    IBOM addBOM = getBOMByPnName(BOTYPE, PnBOMName.PN_BOM_ADD);

    IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    TestBOPDMTaskImport testBO = new TestBOPDMTaskImport(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMTaskImport object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMTaskImport
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskImport randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMTaskImport testBO = newInstance();
    testBO.fillFields();
    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskImport object by the given IBOFieldValue
   *
   * @param  aFieldValue
   *
   * @return TestBOPDMTaskImport
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskImport readInstance(IBOFieldValue aFieldValue) throws PnErrorListException
  {
    return readInstance(aFieldValue.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMTaskImport object by the given IBaseValue
   *
   * @param  aBaseValue
   *
   * @return TestBOPDMTaskImport
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMTaskImport readInstance(IBaseValue aBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aBaseValue);
    TestBOPDMTaskImport testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskImport object by the given BOFieldValue
   *
   * @param  aBOFieldValue
   *
   * @return TestBOPDMTaskImport
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskImport readInstance(TestBOFieldValue aBOFieldValue) throws PnErrorListException
  {
    return readInstance(aBOFieldValue.getBaseValue());
  }


  /**
   * Returns a TestBOPDMTaskImport object by the given IBOValue
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMTaskImport the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMTaskImport getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMTaskImport testBO = new TestBOPDMTaskImport(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
