// Planon Enterprise Edition Source file: BOPDMLocationTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmlocation;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmlocation.bom.*;

/**
 * BOPDMLocationTestSuite
 */
public class BOPDMLocationTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMLocationTestSuite object.
   */
  public BOPDMLocationTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMLocationBOMAvailableTest.class);

    // field change tests

    // generic test
    addTestSuite(BOPDMLocationTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMLocationTestSuite object.
   *
   * @return BOPDMLocationTestSuite
   */
  public static Test suite()
  {
    return new BOPDMLocationTestSuite();
  }
}
