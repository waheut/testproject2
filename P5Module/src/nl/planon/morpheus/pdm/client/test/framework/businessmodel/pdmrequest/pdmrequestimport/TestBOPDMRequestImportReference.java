// Planon Enterprise Edition Source file: TestBOPDMRequestImportReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestImport
 */
public class TestBOPDMRequestImportReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestImport object.
   */
  public TestBOPDMRequestImportReference()
  {
    super(BOTypePDMP5Module.PDM_REQUEST_IMPORT);
  }
}
