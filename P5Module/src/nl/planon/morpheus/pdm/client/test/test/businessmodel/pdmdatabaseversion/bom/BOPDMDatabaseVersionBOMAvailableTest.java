// Planon Enterprise Edition Source file: BOPDMDatabaseVersionBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversion.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionmssql.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMDatabaseVersionBOMAvailableTest
 */
public class BOPDMDatabaseVersionBOMAvailableTest extends BOPDMDatabaseVersionMSSQLTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseVersionBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test add bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableAddBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseVersionDef.PN_BOM_ADD);
  }


  /**
   * Test copy bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableCopyBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseVersionDef.PN_BOM_COPY);
  }


  /**
   * Test delete bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableDeleteBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseVersionDef.PN_BOM_DELETE);
  }


  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseVersionDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseVersionDef.PN_BOM_SAVE);
  }
}
