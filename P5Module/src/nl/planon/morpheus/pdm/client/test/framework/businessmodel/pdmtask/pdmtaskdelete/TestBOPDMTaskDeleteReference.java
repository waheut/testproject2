// Planon Enterprise Edition Source file: TestBOPDMTaskDeleteReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskdelete;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMTaskDeleteReference
 */
public class TestBOPDMTaskDeleteReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskDeleteReference object.
   */
  public TestBOPDMTaskDeleteReference()
  {
    super(BOTypePDMP5Module.PDM_TASK_DELETE);
  }
}
