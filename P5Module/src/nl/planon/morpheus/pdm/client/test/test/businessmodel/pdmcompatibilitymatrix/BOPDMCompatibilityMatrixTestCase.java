// Planon Enterprise Edition Source file: BOPDMCompatibilityMatrixTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmcompatibilitymatrix.*;

/**
 * BOPDMCompatibilityMatrixTestCase
 */
public class BOPDMCompatibilityMatrixTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMCompatibilityMatrixTestCase object.
   *
   * @param aName
   */
  public BOPDMCompatibilityMatrixTestCase(String aName)
  {
    super(aName, TestBOPDMCompatibilityMatrix.BOTYPE);
  }
}
