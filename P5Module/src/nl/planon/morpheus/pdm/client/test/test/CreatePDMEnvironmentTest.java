// Planon Enterprise Edition Source file: CreatePDMEnvironmentTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;

/**
 * CreatePDMEnvironmentTest
 */
public class CreatePDMEnvironmentTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CreatePDMEnvironmentTest object.
   *
   * @param aName
   */
  public CreatePDMEnvironmentTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * setsup default pdm 3 configuration for on peters local eclipse environment
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testSetup() throws PnErrorListException, AuthorizationException
  {
    setAutomaticFree(false);
    TestBOPDMDatabaseVersionOracle versionOracle11 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("11");
    TestBOPDMDatabaseVersionOracle versionOracle11_02 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("11.02");
    TestBOPDMDatabaseVersionOracle versionOracle11_02_00 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("11.02.00.00.00");
    TestBOPDMDatabaseVersionOracle versionOracle09_00_00 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("09.00.00.00.00");
    TestBOPDMDatabaseVersionOracle versionOracle09_00_01 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("09.00.01.00.00");
    TestBOPDMDatabaseVersionOracle versionOracle09_02_00 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("09.02.00.00.00");
    TestBOPDMDatabaseVersionOracle versionOracle09_02_08 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("09.02.08.00.00");
    TestBOPDMDatabaseVersionOracle versionOracle10_02_00_04 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("10.02.00.04.00");
    TestBOPDMDatabaseVersionOracle versionOracle10_02_00_05 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("10.02.00.05.00");
    TestBOPDMDatabaseVersionOracle versionOracle10_02_01 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("10.02.01.00.00");
    TestBOPDMDatabaseVersionOracle versionOracle11_01_00 = TestBOPDMDatabaseVersionOracleFactory.randomInstance("11.01.00.00.00");

    TestBOPDMDatabaseVersionMSSQL versionMssql10_50_00 = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance("10.50.00.00.00");
    TestBOPDMDatabaseVersionMSSQL versionMssql11_00_00 = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance("11.00.00.00.00");

    TestBOPDMLocation location = TestBOPDMLocationFactory.newInstance();
    location.getFieldCode().setValue("NL");
    location.getFieldLocation().setValue("http://localhost:8070/webdav/");
    location.save();

    TestBOPDMDatabaseServerMSSQL serverMssql = TestBOPDMDatabaseServerMSSQLFactory.newInstance();
    serverMssql.getFieldAdminPassword().setValue("300E050807045E"); //secret
    serverMssql.getFieldAdminUser().setValue("SA");
    serverMssql.getFieldCode().setValue("SQLEXPRESS");
    serverMssql.getFieldDatabaseVersionRef().setValue(versionMssql11_00_00);
    serverMssql.getFieldName().setValue("db ms");
    serverMssql.getFieldFileTypeRef().setLookupValue("M");
    serverMssql.getFieldHostName().setValue("PC05860");
    serverMssql.getFieldPDMLocationRef().setValue(location);
    serverMssql.save();

    TestBOPDMDatabaseServerOracle serverOracle = TestBOPDMDatabaseServerOracleFactory.newInstance();
    serverOracle.getFieldAdminPassword().setValue("13070714021D"); //secret
    serverOracle.getFieldAdminUser().setValue("system");
    serverOracle.getFieldCode().setValue("OraDbServer");
    serverOracle.getFieldConnectTNSName().setValue("dummy");
    serverOracle.getFieldDbInstanceName().setValue("PE");
    serverOracle.getFieldDatabaseVersionRef().setValue(versionOracle11);
    serverOracle.getFieldName().setValue("oracle db server");
    serverOracle.getFieldFileTypeRef().setLookupValue("DandO");
    serverOracle.getFieldHostName().setValue("localhost");
    serverOracle.getFieldTemporaryTableSpace().setValue("PDM_TMP_TBLSPACE");
    serverOracle.getFieldPDMLocationRef().setValue(location);
    serverOracle.getFieldPortNumber().setValue(1521);
    serverOracle.getFieldExportLocation().setValue("C:\\oracle\\product\\11.2.0\\dbhome_1\\BIN\\exp.exe");
    serverOracle.getFieldImportLocation().setValue("C:\\oracle\\product\\11.2.0\\dbhome_1\\BIN\\imp.exe");
    serverOracle.save();

    serverMssql.linkVersion(versionMssql10_50_00);
    serverOracle.linkVersion(versionOracle11);
    serverOracle.linkVersion(versionOracle11_02);
    serverOracle.linkVersion(versionOracle09_00_00);
    serverOracle.linkVersion(versionOracle09_00_01);
    serverOracle.linkVersion(versionOracle09_02_00);
    serverOracle.linkVersion(versionOracle09_02_08);
    serverOracle.linkVersion(versionOracle10_02_00_04);
    serverOracle.linkVersion(versionOracle10_02_00_05);
    serverOracle.linkVersion(versionOracle10_02_01);
    serverOracle.linkVersion(versionOracle11_01_00);
    serverOracle.linkVersion(versionOracle11_02_00);

    TestBOPDMDatabase dbUnknownMssql = TestBOPDMDatabaseFactory.newInstance();
    dbUnknownMssql.getFieldName().setValue("mssql_NO_Extension");
    dbUnknownMssql.getFieldAccessCode().setValue("secret");
    dbUnknownMssql.getFieldAccountDatabaseOwnerRef().setLookupValue("PASMIN");
    dbUnknownMssql.getFieldDumpFile().setValue("D:\\db_mssql\\mssqlNOExtension_bak");
    dbUnknownMssql.getFieldLocationRef().setValue(location);
    dbUnknownMssql.save();

    TestBOPDMDatabase dbUnknownOracleDmp = TestBOPDMDatabaseFactory.newInstance();
    dbUnknownOracleDmp.getFieldName().setValue("ora_dmp_NO_Extension");
    dbUnknownOracleDmp.getFieldAccessCode().setValue("secret");
    dbUnknownOracleDmp.getFieldAccountDatabaseOwnerRef().setLookupValue("PASMIN");
    dbUnknownOracleDmp.getFieldDumpFile().setValue("D:\\db_oracle\\201007-SFCB201206211508087852_Exp_dmp");
    dbUnknownOracleDmp.getFieldLocationRef().setValue(location);
    dbUnknownOracleDmp.save();

    TestBOPDMDatabase dbUnknownOracleDPD = TestBOPDMDatabaseFactory.newInstance();
    dbUnknownOracleDPD.getFieldName().setValue("ora_dpd_NO_Extension");
    dbUnknownOracleDPD.getFieldAccessCode().setValue("secret");
    dbUnknownOracleDPD.getFieldAccountDatabaseOwnerRef().setLookupValue("PASMIN");
    dbUnknownOracleDPD.getFieldDumpFile().setValue("D:\\db_oracle\\Oracle_noext_dpd");
    dbUnknownOracleDPD.getFieldLocationRef().setValue(location);
    dbUnknownOracleDPD.save();

    TestBOPDMDBServerDataFileLocation serverLocationMSSQL = TestBOPDMDBServerDataFileLocationFactory.newInstance();
    serverLocationMSSQL.getFieldDatabaseServerRef().setValue(serverMssql);
    serverLocationMSSQL.getFieldFileLocationLocal().setValue("H:\\mssqldata"); // fix this value for your workspace
    serverLocationMSSQL.save();

    TestBOPDMDBServerDataFileLocation serverLocationOra = TestBOPDMDBServerDataFileLocationFactory.newInstance();
    serverLocationOra.getFieldDatabaseServerRef().setValue(serverOracle);
    serverLocationOra.getFieldFileLocationLocal().setValue("C:\\oracle\\admin\\pe\\dpdump"); // fix this value for your workspace
    serverLocationOra.save();

    serverMssql.getFieldIsActive().setValue(true);
    serverMssql.save();
    serverOracle.getFieldIsActive().setValue(true);
    serverOracle.save();
  }
}
