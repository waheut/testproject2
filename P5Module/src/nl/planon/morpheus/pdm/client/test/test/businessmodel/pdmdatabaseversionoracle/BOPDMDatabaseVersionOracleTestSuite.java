// Planon Enterprise Edition Source file: BOPDMDatabaseVersionOracleTestSuite.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionoracle;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversion.bom.*;

/**
 * BOPDMDatabaseVersionOracleTestSuite
 */
public class BOPDMDatabaseVersionOracleTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionTestSuite object.
   */
  public BOPDMDatabaseVersionOracleTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMDatabaseVersionBOMAvailableTest.class);

    // rules tests

    // generic test
    addTestSuite(BOPDMDatabaseVersionOracleTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionTestSuite object.
   *
   * @return BOPDMDatabaseVersionTestSuite
   */
  public static Test suite()
  {
    return new BOPDMDatabaseVersionOracleTestSuite();
  }
}
