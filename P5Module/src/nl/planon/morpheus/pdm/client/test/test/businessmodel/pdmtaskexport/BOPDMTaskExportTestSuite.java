// Planon Enterprise Edition Source file: BOPDMTaskExportTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskexport;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskexport.bom.*;

/**
 * BOPDMTaskExportTestSuite
 */
public class BOPDMTaskExportTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskExportTestSuite object.
   */
  public BOPDMTaskExportTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMTaskExportBOMAvailableTest.class);

    // field change tests

    // generic test
    // addTestSuite(BOPDMTaskExportTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskExportTestSuite object.
   *
   * @return BOPDMTaskExportTestSuite
   */
  public static Test suite()
  {
    return new BOPDMTaskExportTestSuite();
  }
}
