// Planon Enterprise Edition Source file: BOPDMDatabaseServerMSSQLTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseservermssql;

import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * BOPDMDatabaseServerTest
 */
public class BOPDMDatabaseServerMSSQLTest extends BOPDMDatabaseServerMSSQLTestCase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String[] DATABASE_SERVER_SETTING_FIELDS = new String[]
  { //
    BOBasePDMDatabaseServerDef.PN_ADMIN_USER, //
    BOBasePDMDatabaseServerDef.PN_ADMIN_PASS, //
    BOBasePDMDatabaseServerDef.PN_HOST_NAME, //
    BOBasePDMDatabaseServerDef.PN_DATABASE_INSTANCENAME, //
    BOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF, //
    BOBasePDMDatabaseServerDef.PN_CODE_PAGE__NOT_IN_ALL_SUBTYPES, //
    BOBasePDMDatabaseServerDef.PN_SORT_ORDER__NOT_IN_ALL_SUBTYPES //
  };

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseServerMSSQLTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of BOPDMDatabaseServerTest
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO = TestBOPDMDatabaseServerMSSQLFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseServerTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO_2 = TestBOPDMDatabaseServerMSSQLFactory.readInstance(primaryKey);
      pdmDatabaseServerTestBO_2.read();
      pdmDatabaseServerTestBO_2.delete();

      pdmDatabaseServerTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();

    pdmDatabaseServerTestBO.read();
    pdmDatabaseServerTestBO.delete();
  }


  /**
   * Checks whether all the fields that hold specific database server settings are read-only when
   * the database server is active (field IsActive = true)
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testIfDatabaseServerActiveThenSettingFieldsReadOnlyOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDbServer = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDbServer.getFieldIsActive().setBooleanValue(true);
    pdmDbServer.save();

    for (String fieldName : DATABASE_SERVER_SETTING_FIELDS)
    {
      TestBOFieldValue field = pdmDbServer.getFieldByPnName(fieldName);
      assertTrue(field.isReadonly());
    }
  }


  /**
   * Checks whether all the fields that hold specific database server settings are not read-only
   * when the database server is inactive (field IsActive = false)
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testIfDatabaseServerInActiveSettingFieldsNotReadOnlyOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDbServer = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDbServer.getFieldIsActive().setBooleanValue(false);
    pdmDbServer.save();

    for (String fieldName : DATABASE_SERVER_SETTING_FIELDS)
    {
      TestBOFieldValue field = pdmDbServer.getFieldByPnName(fieldName);
      assertFalse(field.isReadonly());
    }
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    IBOFieldValue primaryKey = pdmDatabaseServerTestBO.getPrimaryKeyField();

    pdmDatabaseServerTestBO.read();
    pdmDatabaseServerTestBO.delete();

    try
    {
      TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO_2 = TestBOPDMDatabaseServerMSSQLFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMDatabaseServer.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseServerTestBO.getPrimaryKeyField();
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO_2 = TestBOPDMDatabaseServerMSSQLFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMDatabaseServer.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();

    pdmDatabaseServerTestBO.getFieldCode().setValueRandom();
    pdmDatabaseServerTestBO.save();
  }
}
