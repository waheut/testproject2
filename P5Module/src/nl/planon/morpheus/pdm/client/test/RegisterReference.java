// Planon Enterprise Edition Source file: RegisterReference.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * RegisterReference
 */
public class RegisterReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new RegisterReference object.
   */
  public RegisterReference()
  {
    FieldDataFactory.add(FieldTypePDMP5Module.PDM_DATABASE, TestBOPDMDatabaseReference.class);
    FieldDataFactory.add(FieldTypePDMP5Module.BASE_PDM_DATABASE_SERVER, TestBOPDMDatabaseServerOracleReference.class);
    FieldDataFactory.add(FieldTypePDMP5Module.BASE_PDM_DATABASE_VERSION, TestBOPDMDatabaseVersionMSSQLReference.class);
    FieldDataFactory.add(FieldTypePDMP5Module.PDM_LOCATION, TestBOPDMLocationReference.class);
    FieldDataFactory.add(FieldTypePDMP5Module.BASE_PDM_REQUEST, TestBOPDMRequestImportReference.class);
  }
}
