// Planon Enterprise Edition Source file: TestBOPDMTaskDeleteFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskdelete;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

/**
 * TestBOPDMTaskDeleteFactory
 */
public class TestBOPDMTaskDeleteFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskDeleteFactory object.
   */
  private TestBOPDMTaskDeleteFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMTaskDelete.BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMTaskDelete object.
   *
   * @return TestBOPDMTaskDelete
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskDelete newInstance() throws PnErrorListException, AuthorizationException
  {
    IBOM addBOM = getBOMByPnName(getBOType(), PnBOMName.PN_BOM_ADD);

    IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    TestBOPDMTaskDelete testBO = new TestBOPDMTaskDelete(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMTaskDelete object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMTaskDelete
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskDelete randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMTaskDelete testBO = newInstance();
    testBO.fillFields();
    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskDelete object by the given IBOFieldValue
   *
   * @param  aFieldValue
   *
   * @return TestBOPDMTaskDelete
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskDelete readInstance(IBOFieldValue aFieldValue) throws PnErrorListException
  {
    return readInstance(aFieldValue.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMTaskDelete object by the given IBaseValue
   *
   * @param  aBaseValue
   *
   * @return TestBOPDMTaskDelete
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMTaskDelete readInstance(IBaseValue aBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(getBOType(), aBaseValue);
    TestBOPDMTaskDelete testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskDelete object by the given BOFieldValue
   *
   * @param  aBOFieldValue
   *
   * @return TestBOPDMTaskDelete
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskDelete readInstance(TestBOFieldValue aBOFieldValue) throws PnErrorListException
  {
    return readInstance(aBOFieldValue.getBaseValue());
  }


  /**
   * Returns a TestBOPDMTaskDelete object by the given IBOValue
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMTaskDelete the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMTaskDelete getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMTaskDelete testBO = new TestBOPDMTaskDelete(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
