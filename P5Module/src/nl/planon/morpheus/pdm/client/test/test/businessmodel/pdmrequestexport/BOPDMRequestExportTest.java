// Planon Enterprise Edition Source file: BOPDMRequestExportTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestexport;

/**
 * BOPDMRequestExportTest
 */
public class BOPDMRequestExportTest extends BOPDMRequestExportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestExportTest object.
   *
   * @param aName
   */
  public BOPDMRequestExportTest(String aName)
  {
    super(aName);
  }
}
