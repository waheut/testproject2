// Planon Enterprise Edition Source file: TestBOPDMDatabaseServerMSSQLFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMDatabaseServerFactory
 */
public class TestBOPDMDatabaseServerMSSQLFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * create a database server BO of type MSSQL
   *
   * @param  aPKPDMDatabaseVersion primary key of database server this BO must reference
   * @param  aPKPDMLocation        primary key of pdm location this BO must reference
   *
   * @return a completed Database Server BO
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static TestBOPDMDatabaseServerMSSQL getStandardMSSQLDatabaseServer(IBaseValue aPKPDMDatabaseVersion, IBaseValue aPKPDMLocation) throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = newInstance();
    pdmDatabaseServerMSSQL.getFieldCode().setStringValue(PDMTestFieldValues.PDMDBSERVER_MSSQL_CODE_TEST_VALUE);
    pdmDatabaseServerMSSQL.getFieldAdminUser().setStringValue(PDMTestFieldValues.PDMDBSERVER_MSSQL_ADMINUSER_TEST_VALUE);
    pdmDatabaseServerMSSQL.getFieldAdminPassword().setStringValue(new nl.planon.hades.authentication.PasswordHasher().encryptPassword(PDMTestFieldValues.PDMDBSERVER_MSSQL_ADMINPASS_TEST_VALUE));
    pdmDatabaseServerMSSQL.getFieldCodePage().setStringValue(PDMTestFieldValues.PDMDBSERVER_MSSQL_CODEPAGE_TEST_VALUE);
    pdmDatabaseServerMSSQL.getFieldHostName().setStringValue(PDMTestFieldValues.PDMDBSERVER_MSSQL_HOSTNAME_TEST_VALUE);
    pdmDatabaseServerMSSQL.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeMSSQLDump());
    pdmDatabaseServerMSSQL.getFieldDatabaseVersionRef().setAsBaseValue(aPKPDMDatabaseVersion);
    pdmDatabaseServerMSSQL.getFieldPDMLocationRef().setAsBaseValue(aPKPDMLocation);
    pdmDatabaseServerMSSQL.getFieldIsActive().setBooleanValue(true);
    pdmDatabaseServerMSSQL.fillMandatoryFields();
    pdmDatabaseServerMSSQL.save();
    return pdmDatabaseServerMSSQL;
  }


  /**
   * Creates a new empty unsaved TestBOPDMDatabaseServer object.
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabaseServerMSSQL newInstance() throws PnErrorListException, AuthorizationException
  {
    final IBOM addBOM = getBOMByPnName(BOTYPE, IBOBasePDMDatabaseServerDef.PN_BOM_ADD);

    final IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    final TestBOPDMDatabaseServerMSSQL testBO = new TestBOPDMDatabaseServerMSSQL(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMDatabaseServer object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabaseServerMSSQL randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();
    pdmDatabaseVersionMSSQL.fillFields();
    pdmDatabaseVersionMSSQL.save();
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = newInstance();
    pdmDatabaseServerMSSQL.getFieldDatabaseVersionRef().setAsBaseValue(pdmDatabaseVersionMSSQL.getPrimaryKeyValue());
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();
    return pdmDatabaseServerMSSQL;
  }


  /**
   * Creates a new TestBOPDMDatabaseServer object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField : IBOFieldValue FieldValue of primary TestBOPDMDatabaseServer to be
   *                          loaded
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMDatabaseServerMSSQL readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMDatabaseServer object containing data of specified primary key.
   * Generates a runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue : IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMDatabaseServerMSSQL readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMDatabaseServerMSSQL testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new instance of TestBOPDMDatabaseServer for testing purposes.
   *
   * @return TestBOPDMDatabaseServer.
   *
   * @throws PnErrorListException
   */
  private static TestBOPDMDatabaseServerMSSQL createTestBOPDMDatabaseServer() throws PnErrorListException
  {
    return new TestBOPDMDatabaseServerMSSQL();
  }


  /**
   * Returns a TestBOPDMDatabaseServer object containing data aBOValue which is not destroyed after
   * the test.
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMDatabaseServer the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMDatabaseServerMSSQL getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMDatabaseServerMSSQL testBO = new TestBOPDMDatabaseServerMSSQL(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
