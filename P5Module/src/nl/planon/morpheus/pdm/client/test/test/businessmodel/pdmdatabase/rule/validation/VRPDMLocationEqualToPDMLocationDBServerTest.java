// Planon Enterprise Edition Source file: VRPDMLocationEqualToPDMLocationDBServerTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VRPDMLocationEqualToPDMLocationDBServerTest
 */
public class VRPDMLocationEqualToPDMLocationDBServerTest extends BOPDMDatabaseTestCase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType ERROR_MESSAGE = ErrorNumberPDMP5Module.EC_LOCATION_DOES_NOT_MATCH_LOCATION_DBSERVER;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRPDMLocationEqualToPDMLocationDBServerTest object.
   *
   * @param aName
   */
  public VRPDMLocationEqualToPDMLocationDBServerTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test to check if the validation rule works if the pdm location filled in is not equal to the
   * pdm location of the linked database server.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testPDMLocationEqualToPDMLocationDBServerFail() throws PnErrorListException, AuthorizationException
  {
    // create PDM location of database server
    TestBOPDMLocation pdmLocationDbServer = TestBOPDMLocationFactory.newInstance();
    pdmLocationDbServer.fillAllFields();
    pdmLocationDbServer.save();
    // create PDM location of database
    TestBOPDMLocation pdmLocationDatabase = TestBOPDMLocationFactory.newInstance();
    pdmLocationDatabase.fillAllFields();
    pdmLocationDatabase.save();

    // create PDM database server and link the PDM location of the server to it
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDatabaseServerOracle.fillAllFields();
    pdmDatabaseServerOracle.getFieldPDMLocationRef().setValue(pdmLocationDbServer);
    pdmDatabaseServerOracle.save();

    // for PDM database set DatabaseServerRef to added 'Oracle' PDM database
    // server and link the PDM Location of the database to it
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerOracle);
    pdmDatabase.getFieldLocationRef().setValue(pdmLocationDatabase);

    try
    {
      pdmDatabase.save();
      failMessageTypeExpected(ERROR_MESSAGE);
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ERROR_MESSAGE);
    }
  }


  /**
   * Test to check if the validation rule works if the pdm location filled in is equal to the pdm
   * location of the linked database server.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testPDMLocationEqualToPDMLocationDBServerOk() throws PnErrorListException, AuthorizationException
  {
    // create PDM location of database server
    TestBOPDMLocation pdmLocationDbServer = TestBOPDMLocationFactory.newInstance();
    pdmLocationDbServer.fillAllFields();
    pdmLocationDbServer.save();

    // create PDM database server and link the PDM location of the server to it
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDatabaseServerOracle.fillAllFields();
    pdmDatabaseServerOracle.getFieldPDMLocationRef().setValue(pdmLocationDbServer);
    pdmDatabaseServerOracle.save();

    // for PDM database set DatabaseServerRef to added 'Oracle' PDM database
    // server and link the PDM Location of the database to it
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerOracle);
    pdmDatabase.getFieldLocationRef().setValue(pdmLocationDbServer);

    try
    {
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
