// Planon Enterprise Edition Source file: BOPDMDatabaseVersionOracleTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionoracle;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;

/**
 * BOPDMDatabaseVersionTest
 */
public class BOPDMDatabaseVersionOracleTest extends BOPDMDatabaseVersionOracleTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseVersionOracleTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of BOPDMDatabaseVersionTest
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionOracleFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionOracleFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseVersionTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO_2 = TestBOPDMDatabaseVersionOracleFactory.readInstance(primaryKey);
      pdmDatabaseVersionTestBO_2.read();
      pdmDatabaseVersionTestBO_2.delete();

      pdmDatabaseVersionTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMDatabaseVersion
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionOracleFactory.randomInstance();

    pdmDatabaseVersionTestBO.read();
    pdmDatabaseVersionTestBO.delete();
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionOracleFactory.randomInstance();
    IBOFieldValue primaryKey = pdmDatabaseVersionTestBO.getPrimaryKeyField();

    pdmDatabaseVersionTestBO.read();
    pdmDatabaseVersionTestBO.delete();

    try
    {
      TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO_2 = TestBOPDMDatabaseVersionOracleFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMDatabaseVersion.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionOracleFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseVersionTestBO.getPrimaryKeyField();
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO_2 = TestBOPDMDatabaseVersionOracleFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMDatabaseVersion.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionOracleFactory.randomInstance();

    pdmDatabaseVersionTestBO.getFieldCode().setValueRandom();
    pdmDatabaseVersionTestBO.save();
  }
}
