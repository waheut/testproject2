// Planon Enterprise Edition Source file: BOPDMDBServerDataFileLocationTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation.bom.*;

/**
 * BOPDMDBServerDataFileLocationTestSuite
 */
public class BOPDMDBServerDataFileLocationTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDBServerDataFileLocationTestSuite object.
   */
  public BOPDMDBServerDataFileLocationTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMDBServerDataFileLocationBOMAvailableTest.class);

    // field change tests

    // generic test
    addTestSuite(BOPDMDBServerDataFileLocationTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDBServerDataFileLocationTestSuite object.
   *
   * @return BOPDMDBServerDataFileLocationTestSuite
   */
  public static Test suite()
  {
    return new BOPDMDBServerDataFileLocationTestSuite();
  }
}
