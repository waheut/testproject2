// Planon Enterprise Edition Source file: VRDbTypeEqualToVersionDbTypeTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;

/**
 * VRDbTypeEqualToVersionDbTypeTest
 */
public class VRDbTypeEqualToVersionDbTypeTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRDbTypeEqualToVersionDbTypeTest object.
   *
   * @param aName
   */
  public VRDbTypeEqualToVersionDbTypeTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

//This test is outdated, because of a field change rule that is introduced:
//FCClearDbServerOfDifferentDbType: on change of field database type the field database server needs to be cleared
//if it is linked to a database version of a different database type.
//
//  /**
//   * Test to check if the validation rule works if the database type filled in is not equal to the
//   * database type of the database version that is linked to the database server filled in.
//   *
//   * @throws PnErrorListException
//   * @throws AuthorizationException
//   */
//  public void testDbTypeEqualToVersionDbTypeFail() throws PnErrorListException, AuthorizationException
//  {
//    // create PDM database version of type MSSQL
//    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.newInstance();
//    pdmDatabaseVersionMSSQL.fillAllFields();
//    pdmDatabaseVersionMSSQL.save();
//
//    // create PDM database server and link to version of type MSSQL
//    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
//    pdmDatabaseServerMSSQL.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionMSSQL).fieldChange();
//    pdmDatabaseServerMSSQL.fillAllFields();
//    pdmDatabaseServerMSSQL.save();
//
//    // for PDM database set DatabaseServerRef to added 'MSSQL' PDM database
//    // server and DatabaseTypeRef to 'Oracle'
//    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
//    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
//    pdmDatabase.getFieldDatabaseType().setLookupValue(PDMDatabaseType.ORACLE.getCode());
//
//    try
//    {
//      pdmDatabase.save();
//      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH_VERSION_DBTYPE);
//    }
//    catch (PnErrorListException ex)
//    {
//      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH_VERSION_DBTYPE);
//    }
//  }

  /**
   * Test to check if the database type filled in is equal to the database type of the database
   * version that is linked to the database server filled in.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDbTypeEqualToVersionDbTypeOK() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type MSSQL
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.newInstance();
    pdmDatabaseVersionMSSQL.fillAllFields();
    pdmDatabaseVersionMSSQL.save();

    // create PDM database server and link to version of type MSSQL
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionMSSQL).fieldChange();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    // for PDM database set DatabaseServerRef to added 'MSSQL' PDM database
    // server and DatabaseTypeRef to 'MSSQL'
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmDatabase.getFieldLocationRef().setValue(pdmDatabaseServerMSSQL.getFieldPDMLocationRef());
    pdmDatabase.getFieldDatabaseType().setAsBaseValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeMssql());

    try
    {
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
