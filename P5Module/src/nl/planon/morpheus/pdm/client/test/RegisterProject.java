// Planon Enterprise Edition Source file: RegisterProject.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test;

import nl.planon.dionysus.framework.common.*;

/**
 * RegisterProject
 */
public class RegisterProject extends DefaultRegisterModuleProject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new RegisterProject object.
   */
  public RegisterProject()
  {
    setTestSuiteClass(nl.planon.morpheus.pdm.client.test.test.PDMControllerAgentTestSuite.class);
    setRegisterFactoryClass(RegisterFactory.class);
    setRegisterReferenceClass(RegisterReference.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   *
   * <p>Override because the path that is used in OSGi bundles differs from the default paths to the
   * businessmodel package in the framework</p>
   */
  @Override public String getBusinessModelPackageName()
  {
    return "nl.planon.morpheus." + getName().toLowerCase() + ".client.test.framework.businessmodel";
  }
}
