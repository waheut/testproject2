// Planon Enterprise Edition Source file: TestBOPDMRequestImportFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

/**
 * TestBOPDMRequestImportFactory
 */
public class TestBOPDMRequestImportFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestImportFactory object.
   */
  private TestBOPDMRequestImportFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMRequestImport.BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMRequestImport object.
   *
   * @return TestBOPDMRequestImport
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMRequestImport newInstance() throws PnErrorListException, AuthorizationException
  {
    IBOM addBOM = getBOMByPnName(getBOType(), PnBOMName.PN_BOM_ADD);

    IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    TestBOPDMRequestImport testBO = new TestBOPDMRequestImport(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMRequestImport object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMRequestImport
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMRequestImport randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMRequestImport testBO = newInstance();
    testBO.fillFields();
    testBO.save();
    return testBO;
  }
}
