// Planon Enterprise Edition Source file: TestBOPDMRequestReloadInitialReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadinitial;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestReloadInitialReference
 */
public class TestBOPDMRequestReloadInitialReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestReloadInitialReference object.
   */
  public TestBOPDMRequestReloadInitialReference()
  {
    super(BOTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL);
  }
}
