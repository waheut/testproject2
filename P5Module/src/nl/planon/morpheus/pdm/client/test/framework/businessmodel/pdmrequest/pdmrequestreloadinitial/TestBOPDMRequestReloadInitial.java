// Planon Enterprise Edition Source file: TestBOPDMRequestReloadInitial.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadinitial;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestReloadInitial
 */
public class TestBOPDMRequestReloadInitial extends TestBOBasePDMRequest
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestReloadInitial object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadInitial() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMRequestReloadInitial object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadInitial(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }


  /**
   * Creates a new TestBOPDMRequestReloadInitial object.
   *
   * @param  aBOType
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadInitial(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOPDMRequestReloadInitial object.
   *
   * @param  aBOType
   * @param  aBOValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadInitial(IBOType aBOType, IBOValue aBOValue) throws PnErrorListException
  {
    super(aBOType, aBOValue);
  }
}
