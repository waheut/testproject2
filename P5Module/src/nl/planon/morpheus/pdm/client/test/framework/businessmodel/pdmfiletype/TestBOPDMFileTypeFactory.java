// Planon Enterprise Edition Source file: TestBOPDMFileTypeFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * TestBOPDMFileTypeFactory
 */
public class TestBOPDMFileTypeFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final IBOType BOTYPE = TestBOPDMFileType.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMFileTypeFactory object.
   */
  public TestBOPDMFileTypeFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * given the code of the pdm file type, return its primary key in the database
   *
   * @param  aCode file type code
   *
   * @return primary key of filee type
   *
   * @throws PnErrorListException
   * @throws RuntimeException
   */
  public static final IBaseValue getPDMFileTypeByCode(String aCode) throws PnErrorListException
  {
    IBaseValue result = null;
    try
    {
      ISearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMFILETYPE);
      ISCOperator code = sc.getOperatorEx(BOPDMFileTypeDef.PN_CODE, SCOperatorType.EQUAL);
      code.addValue(aCode);
      IProxyListValue proxyListValue = createBySearchCriteriaEx(sc);
      IProxyValue pdmDatabaseType = proxyListValue.getFirstProxy();

      result = pdmDatabaseType.getPrimaryKeyValue();
    }
    catch (FieldNotFoundException ex)
    {
      throw new RuntimeException(ex);
    }

    return result;
  }


  /**
   * gets pk of DATAPUMP PDMFileType
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPDMFileTypeDataPump() throws PnErrorListException
  {
    return getPDMFileTypeByCode(PDMFileType.DATAPUMP.getCode());
  }


  /**
   * gets pk of MSSQL_DUMP PDMFileType
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPDMFileTypeMSSQLDump() throws PnErrorListException
  {
    return getPDMFileTypeByCode(PDMFileType.MSSQL_DUMP.getCode());
  }


  /**
   * gets pk of DATAPUMP_AND_EXP PDMFileType
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPDMFileTypeOracleDataPumpAndOldExport() throws PnErrorListException
  {
    return getPDMFileTypeByCode(PDMFileType.DATAPUMP_AND_EXP.getCode());
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPDMFileTypeOracleOldExport() throws PnErrorListException
  {
    return getPDMFileTypeByCode("O"); //PDMFileType.ORA_EXP.getCode()
  }


  /**
   * Creates a new TestBOPDMFileType object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField IBOFieldValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMFileType
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMFileType readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMFileType object containing data of specified primary key. Generates a
   * runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMFileType
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMFileType readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMFileType pdmFileType = getReadOnlyTestBO(boValue);
    return pdmFileType;
  }


  /**
   * Creates a new TestBOPDMFileType object containing data of value in specified primary key field.
   *
   * @param  aPrimaryKeyField TestBOFieldValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMFileType
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMFileType readInstance(TestBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Returns a TestBOPDMFileType object containing data aBOValue which is not destroyed after the
   * test.
   *
   * @param  aBOValue the BOValue the value to be used
   *
   * @return TestBOPDMFileType the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMFileType getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMFileType pdmFileType = new TestBOPDMFileType(aBOValue);
    pdmFileType.setAutomaticFree(false);
    return pdmFileType;
  }
}
