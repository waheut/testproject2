// Planon Enterprise Edition Source file: TestBOPDMRequestType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequesttype;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;

/**
 * TestBOPDMRequestType
 */
public class TestBOPDMRequestType extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_REQUEST_TYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestType object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestType() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMRequestType object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestType(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the code field.
   *
   * @return the code field.
   */
  public TestBOFieldValue getFieldCode()
  {
    return getFieldByPnName(BOPDMRequestTypeDef.PN_CODE);
  }


  /**
   * Returns the name field.
   *
   * @return the name field.
   */
  public TestBOFieldValue getFieldName()
  {
    return getFieldByPnName(BOPDMRequestTypeDef.PN_NAME);
  }
}
