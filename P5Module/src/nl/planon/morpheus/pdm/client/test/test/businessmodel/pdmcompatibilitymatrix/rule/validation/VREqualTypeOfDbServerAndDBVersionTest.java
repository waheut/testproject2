// Planon Enterprise Edition Source file: VREqualTypeOfDbServerAndDBVersionTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VREqualTypeOfDbServerAndDBVersionTest
 */
public class VREqualTypeOfDbServerAndDBVersionTest extends BOPDMCompatibilityMatrixTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VREqualTypeOfDbServerAndDBVersionTest object.
   *
   * @param aName
   */
  public VREqualTypeOfDbServerAndDBVersionTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test to check the validation rule if the type of database server filled in matches type of the
   * database version filled in if both are of type MSSQL.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testMSSQLServerWithMSSQLVersionOk() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type MSSQL
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.newInstance();
    pdmDatabaseVersionMSSQL.fillAllFields();
    pdmDatabaseVersionMSSQL.save();

    // create PDM database server of type MSSQL
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrix = TestBOPDMCompatibilityMatrixFactory.randomInstance();
    pdmCompatibilityMatrix.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmCompatibilityMatrix.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionMSSQL);
    try
    {
      pdmCompatibilityMatrix.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


  /**
   * Test to check the validation rule if the type (MSSQL) of database server filled in does not
   * match type (Oracle) of the database version filled in.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testMSSQLServerWithOracleVersionFail() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type Oracle
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionOracle = TestBOPDMDatabaseVersionOracleFactory.newInstance();
    pdmDatabaseVersionOracle.fillAllFields();
    pdmDatabaseVersionOracle.save();

    // create PDM database server of type MSSQL
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrix = TestBOPDMCompatibilityMatrixFactory.randomInstance();
    pdmCompatibilityMatrix.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmCompatibilityMatrix.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionOracle);
    try
    {
      pdmCompatibilityMatrix.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DBTYPE_SERVER_DOES_NOT_MATCH_DBTYPE_VERSION);
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_DBTYPE_SERVER_DOES_NOT_MATCH_DBTYPE_VERSION);
    }
  }


  /**
   * Test to check the validation rule if the type (Oracle) of database server filled in does not
   * match type (MSSQL) of the database version filled in.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testOracleServerWithMSSQLVersionFail() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type MSSQL
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.newInstance();
    pdmDatabaseVersionMSSQL.fillAllFields();
    pdmDatabaseVersionMSSQL.save();

    // create PDM database server of type Oracle
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDatabaseServerOracle.fillAllFields();
    pdmDatabaseServerOracle.save();

    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrix = TestBOPDMCompatibilityMatrixFactory.randomInstance();
    pdmCompatibilityMatrix.getFieldDatabaseServerRef().setValue(pdmDatabaseServerOracle);
    pdmCompatibilityMatrix.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionMSSQL);
    try
    {
      pdmCompatibilityMatrix.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DBTYPE_SERVER_DOES_NOT_MATCH_DBTYPE_VERSION);
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_DBTYPE_SERVER_DOES_NOT_MATCH_DBTYPE_VERSION);
    }
  }


  /**
   * Test to check the validation rule if the type of database server filled in matches type of the
   * database version filled in if both are of type Oracle.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testOracleServerWithOracleVersionOk() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type Oracle
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionOracle = TestBOPDMDatabaseVersionOracleFactory.newInstance();
    pdmDatabaseVersionOracle.fillAllFields();
    pdmDatabaseVersionOracle.save();

    // create PDM database server of type Oracle
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDatabaseServerOracle.fillAllFields();
    pdmDatabaseServerOracle.save();

    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrix = TestBOPDMCompatibilityMatrixFactory.randomInstance();
    pdmCompatibilityMatrix.getFieldDatabaseServerRef().setValue(pdmDatabaseServerOracle);
    pdmCompatibilityMatrix.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionOracle);
    try
    {
      pdmCompatibilityMatrix.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
