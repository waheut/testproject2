// Planon Enterprise Edition Source file: BOPDMLocationTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmlocation;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;

/**
 * BOPDMLocationTest
 */
public class BOPDMLocationTest extends BOPDMLocationTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMLocationTest object.
   *
   * @param aName
   */
  public BOPDMLocationTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of BOPDMLocationTest
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocationTestBO = TestBOPDMLocationFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocationTestBO = TestBOPDMLocationFactory.randomInstance();

    IBOFieldValue primaryKey = pdmLocationTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMLocation pdmLocationTestBO_2 = TestBOPDMLocationFactory.readInstance(primaryKey);
      pdmLocationTestBO_2.read();
      pdmLocationTestBO_2.delete();

      pdmLocationTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMLocation
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocationTestBO = TestBOPDMLocationFactory.randomInstance();

    pdmLocationTestBO.read();
    pdmLocationTestBO.delete();
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocationTestBO = TestBOPDMLocationFactory.randomInstance();
    IBOFieldValue primaryKey = pdmLocationTestBO.getPrimaryKeyField();

    pdmLocationTestBO.read();
    pdmLocationTestBO.delete();

    try
    {
      TestBOPDMLocation pdmLocationTestBO_2 = TestBOPDMLocationFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMLocation.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocationTestBO = TestBOPDMLocationFactory.randomInstance();

    IBOFieldValue primaryKey = pdmLocationTestBO.getPrimaryKeyField();
    TestBOPDMLocation pdmLocationTestBO_2 = TestBOPDMLocationFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMLocation.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocationTestBO = TestBOPDMLocationFactory.randomInstance();

    pdmLocationTestBO.getFieldName().setValueRandom();
    pdmLocationTestBO.save();
  }
}
