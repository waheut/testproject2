// Planon Enterprise Edition Source file: BOPDMCompatibilityMatrixBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMCompatibilityMatrixBOMAvailableTest
 */
public class BOPDMCompatibilityMatrixBOMAvailableTest extends BOPDMCompatibilityMatrixTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMCompatibilityMatrixBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMCompatibilityMatrixBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test add bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableAddBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMCompatibilityMatrixDef.PN_BOM_ADD);
  }


  /**
   * Test copy bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableCopyBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMCompatibilityMatrixDef.PN_BOM_COPY);
  }


  /**
   * Test delete database bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableDeleteBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMCompatibilityMatrixDef.PN_BOM_DELETE);
  }


  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMCompatibilityMatrixDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMCompatibilityMatrixDef.PN_BOM_SAVE);
  }
}
