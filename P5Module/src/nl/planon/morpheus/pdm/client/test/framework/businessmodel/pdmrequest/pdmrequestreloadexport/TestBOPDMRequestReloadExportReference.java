// Planon Enterprise Edition Source file: TestBOPDMRequestReloadExportReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadexport;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestReloadExportReference
 */
public class TestBOPDMRequestReloadExportReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestReloadExportReference object.
   */
  public TestBOPDMRequestReloadExportReference()
  {
    super(BOTypePDMP5Module.PDM_REQUEST_RELOAD_EXPORT);
  }
}
