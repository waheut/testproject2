// Planon Enterprise Edition Source file: TestBOPDMDBServerDataFileLocation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * TestBOPDMDBServerDataFileLocation
 */
public class TestBOPDMDBServerDataFileLocation extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = TestBOPDMDBServerDataFileLocationFactory.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDBServerDataFileLocation object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDBServerDataFileLocation() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMDBServerDataFileLocation object.
   *
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDBServerDataFileLocation(IBOValue aValue) throws PnErrorListException
  {
    super(BOTYPE, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * convenience method for field PN_DATABASESERVER_REF
   *
   * @return field PN_DATABASESERVER_REF
   */
  public TestBOFieldValue getFieldDatabaseServerRef()
  {
    return getFieldByPnName(IBOPDMDBServerDataFileLocationDef.PN_DATABASESERVER_REF);
  }


  /**
   * convenience method for field PN_FILE_LOCATION_LOCAL
   *
   * @return field PN_FILE_LOCATION_LOCAL
   */
  public TestBOFieldValue getFieldFileLocationLocal()
  {
    return getFieldByPnName(IBOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL);
  }
}
