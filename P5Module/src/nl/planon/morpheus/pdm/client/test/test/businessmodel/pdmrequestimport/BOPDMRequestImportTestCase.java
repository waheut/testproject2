// Planon Enterprise Edition Source file: BOPDMRequestImportTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport.*;

/**
 * BOPDMRequestImportTestCase
 */
public class BOPDMRequestImportTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestImportTestCase object.
   *
   * @param aName
   */
  public BOPDMRequestImportTestCase(String aName)
  {
    super(aName, TestBOPDMRequestImport.BOTYPE);
  }
}
