// Planon Enterprise Edition Source file: TestBOPDMCompatibilityMatrixFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmcompatibilitymatrix;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMCompatibilityMatrixFactory
 */
public class TestBOPDMCompatibilityMatrixFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMCompatibilityMatrix object.
   *
   * @return TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMCompatibilityMatrix newInstance() throws PnErrorListException, AuthorizationException
  {
    final IBOM addBOM = getBOMByPnName(BOTYPE, IBOPDMCompatibilityMatrixDef.PN_BOM_ADD);

    final IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    final TestBOPDMCompatibilityMatrix testBO = new TestBOPDMCompatibilityMatrix(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMCompatibilityMatrix object containing random data. Errors result
   * in an ErrorListException
   *
   * @return TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMCompatibilityMatrix randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionOracleFactory.randomInstance();

    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO = newInstance();
    pdmCompatibilityMatrixTestBO.getFieldDatabaseVersionRef().setAsBaseValue(pdmDatabaseVersionTestBO.getPrimaryKeyValue());
    pdmCompatibilityMatrixTestBO.getFieldDatabaseServerRef().setAsBaseValue(pdmDatabaseServerTestBO.getPrimaryKeyValue());
    pdmCompatibilityMatrixTestBO.save();
    return pdmCompatibilityMatrixTestBO;
  }


  /**
   * links version to specified databaseserver
   *
   * @param  aTestBOPDMDatabaseServer
   * @param  aVersion
   *
   * @return TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static TestBOPDMCompatibilityMatrix randomInstance(TestBOPDMDatabaseServer aTestBOPDMDatabaseServer, TestBOPDMDatabaseVersion aVersion) throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix matrix = newInstance();
    matrix.getFieldDatabaseServerRef().setValue(aTestBOPDMDatabaseServer);
    matrix.getFieldDatabaseVersionRef().setValue(aVersion);

    matrix.save();
    return matrix;
  }


  /**
   * Creates a new TestBOPDMCompatibilityMatrix object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField : IBOFieldValue FieldValue of primary TestBOPDMCompatibilityMatrix to
   *                          be loaded
   *
   * @return TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMCompatibilityMatrix readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMCompatibilityMatrix object containing data of specified primary key.
   * Generates a runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue : IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMCompatibilityMatrix readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMCompatibilityMatrix testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new instance of TestBOPDMCompatibilityMatrix for testing purposes.
   *
   * @return TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException
   */
  private static TestBOPDMCompatibilityMatrix createTestBOPDMCompatibilityMatrix() throws PnErrorListException
  {
    return new TestBOPDMCompatibilityMatrix();
  }


  /**
   * Returns a TestBOPDMCompatibilityMatrix object containing data aBOValue which is not destroyed
   * after the test.
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMCompatibilityMatrix the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMCompatibilityMatrix getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMCompatibilityMatrix testBO = new TestBOPDMCompatibilityMatrix(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
