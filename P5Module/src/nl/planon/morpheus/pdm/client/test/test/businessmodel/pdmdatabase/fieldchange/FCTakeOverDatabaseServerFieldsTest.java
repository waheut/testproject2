// Planon Enterprise Edition Source file: FCTakeOverDatabaseServerFieldsTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.fieldchange;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;

/**
 * FCTakeOverDatabaseTypeTest
 */
public class FCTakeOverDatabaseServerFieldsTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new FCTakeOverDatabaseTypeTest object.
   *
   * @param aName
   */
  public FCTakeOverDatabaseServerFieldsTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <ul>
   *   <li>create PDM database version of type MSSQL</li>
   *   <li>create PDM database version of type ORACLE</li>
   *   <li>create PDM database server and link to version of type MSSQL</li>
   *   <li>create PDM database server and link to version of type ORACLE</li>
   *   <li>create PDM database and clear link to server and type</li>
   *   <li>for PDM database set ServerRef to first added PDM database server</li>
   *   <li>check if DatabaseTypeRef is set equal to the data base type of the linked (first added)
   *     database version</li>
   *   <li>check if PDM Location is set equal to the PDM Location of the linked (first added)
   *     database server</li>
   *   <li>for PDM database set ServerRef to last added PDM database server</li>
   *   <li>check if DatabaseTypeRef is set equal to the data base type of the linked (last added)
   *     database version</li>
   *   <li>check if PDM Location is set equal to the PDM Location of the linked (last added)
   *     database server</li>
   * </ul>
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testChangeDbServerRefSetDbTypeRef() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.newInstance();
    pdmDatabaseVersionMSSQL.fillAllFields();
    pdmDatabaseVersionMSSQL.save();

    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionOracle = TestBOPDMDatabaseVersionOracleFactory.newInstance();
    pdmDatabaseVersionOracle.fillAllFields();
    pdmDatabaseVersionOracle.save();

    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionMSSQL).fieldChange();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDatabaseServerOracle.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionOracle).fieldChange();
    pdmDatabaseServerOracle.fillAllFields();
    pdmDatabaseServerOracle.save();

    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().clear();
    pdmDatabase.getFieldDatabaseType().clear();

    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmDatabase.fieldChange();
    assertEquals(pdmDatabase.getFieldDatabaseType().getBaseValue(), TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeMssql());
    assertEquals(pdmDatabase.getFieldLocationRef().getBaseValue(), pdmDatabaseServerMSSQL.getFieldPDMLocationRef().getBaseValue());

    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerOracle);
    pdmDatabase.fieldChange();
    assertEquals(pdmDatabase.getFieldDatabaseType().getBaseValue(), TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeOracle());
    assertEquals(pdmDatabase.getFieldLocationRef().getBaseValue(), pdmDatabaseServerOracle.getFieldPDMLocationRef().getBaseValue());
  }
}
