// Planon Enterprise Edition Source file: OracleDatapumpRequestTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.request.oracle;

import java.io.*;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.test.request.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * OracleDatapumpImportRequestTest
 */
public class OracleDatapumpRequestTest extends PDMRequestsTest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleDatapumpImportRequestTest object.
   *
   * @param aName
   */
  public OracleDatapumpRequestTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Test a failing Oracle datapump import request due to invalid user name to connect to
   * databaseserver.</p>
   *
   * <p>After adding a standard configuration of a pdm Oracle datapump database, the field <code>
   * AdminUser</code> is changed to an invalid value. Then an import request is executed, which is
   * then expected to fail.</p>
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws IOException
   */
  public void testImportOracleDataPumpDumpFail() throws PnErrorListException, AuthorizationException, IOException
  {
    TestBOPDMDatabase pdmDatabaseDataPump = TestBOPDMDatabaseFactory.createStandardPDMDatabaseOfTypeOracleDatapump();
    pdmDatabaseDataPump.saveAndUploadDumpFile();
    TestBOPDMDatabaseServerOracle pdmDatabaseServer = TestBOPDMDatabaseServerOracleFactory.readInstance(pdmDatabaseDataPump.getFieldDatabaseServerRef().getBaseValue());
    pdmDatabaseServer.activate(false);
    pdmDatabaseServer.getFieldAdminUser().setStringValue("InvalidUserName");
    pdmDatabaseServer.save();
    pdmDatabaseServer.activate(true);
    pdmDatabaseDataPump.executeImportPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseDataPump);
    waitForRequestToFinish(pdmDatabaseDataPump, MAX_MINUTES_TO_WAIT_FOR_IMPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedFailedRequest(pdmDatabaseDataPump, IBOPDMDatabaseDef.STATE_INITIAL);
  }


  /**
   * <p>Test standard Oracle datapump dump requests.</p>
   *
   * <p>After the creation of a standard configuration of a pdm Oracle datapump database, the
   * following requests are executed:</p>
   *
   * <ul>
   *   <li>import</li>
   *   <li>reload initial</li>
   *   <li>export</li>
   *   <li>reload export</li>
   *   <li>clean up</li>
   * </ul>
   *
   * <p>For each request is tested if it is picked up and if it is executed successfully.</p>
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws IOException
   */
  public void testOracleDatapumpRequestsOk() throws PnErrorListException, AuthorizationException, IOException
  {
    TestBOPDMDatabase pdmDatabaseOracle = TestBOPDMDatabaseFactory.createStandardPDMDatabaseOfTypeOracleDatapump();
    pdmDatabaseOracle.saveAndUploadDumpFile();
    pdmDatabaseOracle.executeImportPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseOracle);
    waitForRequestToFinish(pdmDatabaseOracle, MAX_MINUTES_TO_WAIT_FOR_IMPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseOracle, IBOPDMDatabaseDef.STATE_AVAILABLE);

    pdmDatabaseOracle.executeReloadInitialPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseOracle);
    waitForRequestToFinish(pdmDatabaseOracle, MAX_MINUTES_TO_WAIT_FOR_RELOAD_INITIAL_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseOracle, IBOPDMDatabaseDef.STATE_AVAILABLE);

    pdmDatabaseOracle.executeExportPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseOracle);
    waitForRequestToFinish(pdmDatabaseOracle, MAX_MINUTES_TO_WAIT_FOR_EXPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseOracle, IBOPDMDatabaseDef.STATE_AVAILABLE);
    // Extra check: after a successful import the field PDMExportFile should be filled with the name of the export file.
    assertFalse("Field PDM Export File is expected to be filled, but it is not !!", pdmDatabaseOracle.getFieldPDMExportFile().isEmpty());

    pdmDatabaseOracle.executeReloadExportedPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseOracle);
    waitForRequestToFinish(pdmDatabaseOracle, MAX_MINUTES_TO_WAIT_FOR_RELOAD_EXPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseOracle, IBOPDMDatabaseDef.STATE_AVAILABLE);

    pdmDatabaseOracle.executeCleanupPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseOracle);
    waitForRequestToFinish(pdmDatabaseOracle, MAX_MINUTES_TO_WAIT_FOR_CLEANUP_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseOracle, IBOPDMDatabaseDef.STATE_INITIAL);
  }
}
