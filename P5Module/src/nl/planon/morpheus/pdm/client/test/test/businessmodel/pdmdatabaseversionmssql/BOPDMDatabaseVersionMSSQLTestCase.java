// Planon Enterprise Edition Source file: BOPDMDatabaseVersionMSSQLTestCase.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionmssql;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;

/**
 * BOPDMDatabaseVersionMSSQLTestCase
 */
public class BOPDMDatabaseVersionMSSQLTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionTestCase object.
   *
   * @param aName
   */
  public BOPDMDatabaseVersionMSSQLTestCase(String aName)
  {
    super(aName, TestBOPDMDatabaseVersionMSSQL.BOTYPE);
  }
}
