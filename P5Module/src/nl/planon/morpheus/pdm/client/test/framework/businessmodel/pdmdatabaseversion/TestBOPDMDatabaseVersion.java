// Planon Enterprise Edition Source file: TestBOPDMDatabaseVersion.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * TestBOPDMDatabaseVersion
 */
public class TestBOPDMDatabaseVersion extends TestBO
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseVersion object.
   *
   * @param  aBOType DOCUMENT ME!
   *
   * @throws PnErrorListException DOCUMENT ME!
   */
  public TestBOPDMDatabaseVersion(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOPDMDatabaseVersion object.
   *
   * @param  aBOType DOCUMENT ME!
   * @param  aValue  DOCUMENT ME!
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseVersion(IBOType aBOType, IBOValue aValue) throws PnErrorListException
  {
    super(aBOType, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * convenience method for field PN_CODE
   *
   * @return field PN_CODE
   */
  public TestBOFieldValue getFieldCode()
  {
    return getFieldByPnName(IBOBasePDMDatabaseVersionDef.PN_CODE);
  }


  /**
   * convenience method for field PN_NAME
   *
   * @return field PN_NAME
   */
  public TestBOFieldValue getFieldName()
  {
    return getFieldByPnName(IBOBasePDMDatabaseVersionDef.PN_NAME);
  }
}
