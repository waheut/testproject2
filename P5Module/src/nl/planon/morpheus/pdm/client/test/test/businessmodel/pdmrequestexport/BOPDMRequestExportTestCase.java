// Planon Enterprise Edition Source file: BOPDMRequestExportTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestexport;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestexport.*;

/**
 * BOPDMRequestExportTestCase
 */
public class BOPDMRequestExportTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestExportTestCase object.
   *
   * @param aName
   */
  public BOPDMRequestExportTestCase(String aName)
  {
    super(aName, TestBOPDMRequestExport.BOTYPE);
  }
}
