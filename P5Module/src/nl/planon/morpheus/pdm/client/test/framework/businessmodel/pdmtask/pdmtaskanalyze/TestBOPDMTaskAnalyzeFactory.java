// Planon Enterprise Edition Source file: TestBOPDMTaskAnalyzeFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskanalyze;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

/**
 * TestBOPDMTaskAnalyzeFactory
 */
public class TestBOPDMTaskAnalyzeFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskAnalyzeFactory object.
   */
  private TestBOPDMTaskAnalyzeFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMTaskAnalyze.BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMTaskAnalyze object.
   *
   * @return TestBOPDMTaskAnalyze
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskAnalyze newInstance() throws PnErrorListException, AuthorizationException
  {
    IBOM addBOM = getBOMByPnName(getBOType(), PnBOMName.PN_BOM_ADD);

    IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    TestBOPDMTaskAnalyze testBO = new TestBOPDMTaskAnalyze(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMTaskAnalyze object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMTaskAnalyze
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskAnalyze randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMTaskAnalyze testBO = newInstance();
    testBO.fillFields();
    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskAnalyze object by the given IBOFieldValue
   *
   * @param  aFieldValue
   *
   * @return TestBOPDMTaskAnalyze
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskAnalyze readInstance(IBOFieldValue aFieldValue) throws PnErrorListException
  {
    return readInstance(aFieldValue.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMTaskAnalyze object by the given IBaseValue
   *
   * @param  aBaseValue
   *
   * @return TestBOPDMTaskAnalyze
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMTaskAnalyze readInstance(IBaseValue aBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(getBOType(), aBaseValue);
    TestBOPDMTaskAnalyze testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskAnalyze object by the given BOFieldValue
   *
   * @param  aBOFieldValue
   *
   * @return TestBOPDMTaskAnalyze
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskAnalyze readInstance(TestBOFieldValue aBOFieldValue) throws PnErrorListException
  {
    return readInstance(aBOFieldValue.getBaseValue());
  }


  /**
   * Returns a TestBOPDMTaskAnalyze object by the given IBOValue
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMTaskAnalyze the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMTaskAnalyze getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMTaskAnalyze testBO = new TestBOPDMTaskAnalyze(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
