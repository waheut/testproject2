// Planon Enterprise Edition Source file: BOPDMTaskExportTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskexport;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskexport.*;

/**
 * BOPDMTaskExportTestCase
 */
public class BOPDMTaskExportTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskExportTestCase object.
   *
   * @param aName
   */
  public BOPDMTaskExportTestCase(String aName)
  {
    super(aName, TestBOPDMTaskExport.BOTYPE);
  }
}
