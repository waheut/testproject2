// Planon Enterprise Edition Source file: TestBOBasePDMRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest;

import junit.framework.*;

import java.util.*;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.dionysus.businessmodeltest.client.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.util.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * TestBOBasePDMRequest
 */
public class TestBOBasePDMRequest extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.BASE_PDM_REQUEST;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOBasePDMRequest object.
   *
   * @param  aBOType
   *
   * @throws PnErrorListException
   */
  public TestBOBasePDMRequest(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOBasePDMRequest object.
   *
   * @param  aBOType
   * @param  aBOValue
   *
   * @throws PnErrorListException
   */
  public TestBOBasePDMRequest(IBOType aBOType, IBOValue aBOValue) throws PnErrorListException
  {
    super(aBOType, aBOValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * getFieldDatabaseRef
   *
   * @return
   */
  public TestBOFieldValue getFieldDatabaseRef()
  {
    return getFieldByPnName(IBOBasePDMRequestDef.PN_DATABASE_REF);
  }


  /**
   * convenience method to get a list of all tasks linked to this request, ordered descending by the
   * date the tasks are inserted
   *
   * @return a list of tasks
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public List<TestBOBasePDMTask> getTasks() throws PnErrorListException, AuthorizationException
  {
    TestSearchCriteria sc = TestSearchCriteria.getPVSearchCriteria(PVTypePDMP5Module.PDMTASK);
    sc.setOperatorEx(BOBasePDMTaskDef.PN_PDMREQUEST_REF, SCOperatorType.EQUAL, this);

    final TestProxyListValue plv = sc.getProxyListvalue();
    ProxyValueComparator comparator = new ProxyValueComparator(plv);
    comparator.addSortBOType(SortDirection.ASCENDING);
    comparator.addSortItem(IBOBasePDMTaskDef.PN_PRIMARYKEY, SortDirection.DESCENDING);
    plv.sort(comparator);
    List<TestBOBasePDMTask> result = new ArrayList<TestBOBasePDMTask>();

    for (TestProxyValue pv : plv.getTestProxyValues())
    {
      TestBOBasePDMTask task = TestBOBasePDMTaskFactory.readInstance(pv.getPrimaryKeyValue());
      result.add(task);
    }
    return result;
  }


  /**
   * gets list of TestBORequest and verifies expected size
   *
   * @param  aExpectedSize list should be of this size
   *
   * @return list of TestBOBasePDMRequest
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public List<TestBOBasePDMTask> getTasks(int aExpectedSize) throws AuthorizationException, PnErrorListException
  {
    List<TestBOBasePDMTask> result = getTasks();
    Assert.assertEquals(aExpectedSize, result.size());

    return result;
  }
}
