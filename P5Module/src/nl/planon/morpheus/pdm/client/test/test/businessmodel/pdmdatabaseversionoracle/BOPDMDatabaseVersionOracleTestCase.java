// Planon Enterprise Edition Source file: BOPDMDatabaseVersionOracleTestCase.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionoracle;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;

/**
 * BOPDMDatabaseVersionOracleTestCase
 */
public class BOPDMDatabaseVersionOracleTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionOracleTestCase object.
   *
   * @param aName
   */
  public BOPDMDatabaseVersionOracleTestCase(String aName)
  {
    super(aName, TestBOPDMDatabaseVersionOracle.BOTYPE);
  }
}
