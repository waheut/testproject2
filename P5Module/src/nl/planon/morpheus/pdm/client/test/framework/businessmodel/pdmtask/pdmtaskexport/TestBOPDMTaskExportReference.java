// Planon Enterprise Edition Source file: TestBOPDMTaskExportReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskexport;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMTaskExportReference
 */
public class TestBOPDMTaskExportReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskExportReference object.
   */
  public TestBOPDMTaskExportReference()
  {
    super(BOTypePDMP5Module.PDM_TASK_EXPORT);
  }
}
