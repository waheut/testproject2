// Planon Enterprise Edition Source file: TestBOPDMDatabaseVersionOracleReference.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle;

import nl.planon.dionysus.framework.field.*;

/**
 * TestBOPDMDatabaseVersionOracleReference
 */
public class TestBOPDMDatabaseVersionOracleReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseVersionReference object.
   */
  public TestBOPDMDatabaseVersionOracleReference()
  {
    super(TestBOPDMDatabaseVersionOracleFactory.BOTYPE);
  }
}
