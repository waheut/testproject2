// Planon Enterprise Edition Source file: TestBOPDMLocationReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation;

import nl.planon.dionysus.framework.field.*;

/**
 * TestBOPDMLocationReference
 */
public class TestBOPDMLocationReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMLocationReference object.
   */
  public TestBOPDMLocationReference()
  {
    super(TestBOPDMLocationFactory.BOTYPE);
  }
}
