// Planon Enterprise Edition Source file: TestBOPDMRequestExportFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestexport;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.businessmodel.*;

/**
 * TestBOPDMRequestExportFactory
 */
public class TestBOPDMRequestExportFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestExportFactory object.
   */
  private TestBOPDMRequestExportFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMRequestExport.BOTYPE;
  }
}
