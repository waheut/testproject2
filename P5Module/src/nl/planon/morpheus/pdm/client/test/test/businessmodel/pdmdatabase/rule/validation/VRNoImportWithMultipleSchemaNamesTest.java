// Planon Enterprise Edition Source file: VRNoImportWithMultipleSchemaNamesTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VRNoImportWithMultipleSchemaNamesTest
 */
public class VRNoImportWithMultipleSchemaNamesTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRNoImportWithMultipleSchemaNamesTest object.
   *
   * @param aName
   */
  public VRNoImportWithMultipleSchemaNamesTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that field code is only allowed to have word characters ([a-zA-Z_0-9]) in it when adding a
   * new database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testNoImportWithMultipleSchemaNamesOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    try
    {
      pdmDatabase.getFieldObjectOwnerInDump().setStringValue("SchemaName1,SchemaName2");
      pdmDatabase.save();
      pdmDatabase.executeImportPDMDatabaseBOM();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_IMPORT_WITH_MULTIPLE_SCHEMANAMES, "No import when there are multiple schema names declared");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_CANNOT_IMPORT_WITH_MULTIPLE_SCHEMANAMES);
    }
  }
}
