// Planon Enterprise Edition Source file: TestBOPDMRequestDeleteReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestdelete;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestDeleteReference
 */
public class TestBOPDMRequestDeleteReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestDeleteReference object.
   */
  public TestBOPDMRequestDeleteReference()
  {
    super(BOTypePDMP5Module.PDM_REQUEST_DELETE);
  }
}
