// Planon Enterprise Edition Source file: TestBOPDMDatabaseVersionMSSQLFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMDatabaseVersionFactory
 */
public class TestBOPDMDatabaseVersionMSSQLFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMDatabaseVersion object.
   *
   * @return TestBOPDMDatabaseVersion
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabaseVersionMSSQL newInstance() throws PnErrorListException, AuthorizationException
  {
    final IBOM addBOM = getBOMByPnName(BOTYPE, IBOBasePDMDatabaseVersionDef.PN_BOM_ADD);

    final IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    final TestBOPDMDatabaseVersionMSSQL testBO = new TestBOPDMDatabaseVersionMSSQL(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMDatabaseVersion object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMDatabaseVersion
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabaseVersionMSSQL randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL testBO = newInstance();
    testBO.fillFields();

    testBO.save();
    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMDatabaseVersion object with specified name (eq. version nr)
   *
   * @param  aCode version code
   *
   * @return TestBOPDMDatabaseVersion
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabaseVersionMSSQL randomInstance(String aCode) throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionMSSQL testBO = newInstance();
    testBO.fillFields();
    testBO.getFieldCode().setValue(aCode);
    testBO.getFieldName().setValue("MSSQL " + aCode);

    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMDatabaseVersion object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField : IBOFieldValue FieldValue of primary TestBOPDMDatabaseVersion to be
   *                          loaded
   *
   * @return TestBOPDMDatabaseVersion
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMDatabaseVersionMSSQL readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMDatabaseVersion object containing data of specified primary key.
   * Generates a runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue : IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDatabaseVersion
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMDatabaseVersionMSSQL readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMDatabaseVersionMSSQL testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new instance of TestBOPDMDatabaseVersion for testing purposes.
   *
   * @return TestBOPDMDatabaseVersion.
   *
   * @throws PnErrorListException
   */
  private static TestBOPDMDatabaseVersionMSSQL createTestBOPDMDatabaseVersion() throws PnErrorListException
  {
    return new TestBOPDMDatabaseVersionMSSQL();
  }


  /**
   * Returns a TestBOPDMDatabaseVersion object containing data aBOValue which is not destroyed after
   * the test.
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMDatabaseVersion the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMDatabaseVersionMSSQL getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMDatabaseVersionMSSQL testBO = new TestBOPDMDatabaseVersionMSSQL(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
