// Planon Enterprise Edition Source file: TestBOPDMFileTypeReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMFileTypeReference
 */
public class TestBOPDMFileTypeReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMFileTypeReference object.
   */
  public TestBOPDMFileTypeReference()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_FILE_TYPE);
  }
}
