// Planon Enterprise Edition Source file: BOPDMDatabaseServerMSSQLTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseservermssql;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserver.bom.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserver.rule.*;

/**
 * BOPDMDatabaseServerTestSuite
 */
public class BOPDMDatabaseServerMSSQLTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerTestSuite object.
   */
  public BOPDMDatabaseServerMSSQLTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMDatabaseServerBOMAvailableTest.class);

    // field change tests

    // generic test
    addTestSuite(BOPDMDatabaseServerMSSQLTest.class);

    // validation rule tests
    addTestSuite(VRReadOnlyFieldsForActiveDbServerTest.class);
    addTestSuite(VRCannotInactivateDbServerIfBusyDbPresentTest.class);
    // action rule tests
    addTestSuite(ARUpdatePDMLocationLinkedDatabasesTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerTestSuite object.
   *
   * @return BOPDMDatabaseServerTestSuite
   */
  public static Test suite()
  {
    return new BOPDMDatabaseServerMSSQLTestSuite();
  }
}
