// Planon Enterprise Edition Source file: TestBOPDMDatabaseServerOracleFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMDatabaseServerFactory
 */
public class TestBOPDMDatabaseServerOracleFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * create a database server BO of type Oracle
   *
   * @param  aPKPDMDatabaseVersion primary key of database server this BO must reference
   * @param  aPKPDMLocation        primary key of pdm location this BO must reference
   *
   * @return a completed Database Server BO
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static TestBOPDMDatabaseServerOracle getStandardOracleDatabaseServer(IBaseValue aPKPDMDatabaseVersion, IBaseValue aPKPDMLocation) throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = newInstance();
    pdmDatabaseServerOracle.getFieldCode().setStringValue(PDMTestFieldValues.PDMDBSERVER_ORACLE_CODE_TEST_VALUE);
    pdmDatabaseServerOracle.getFieldAdminUser().setStringValue(PDMTestFieldValues.PDMDBSERVER_ORACLE_ADMINUSER_TEST_VALUE);
    pdmDatabaseServerOracle.getFieldAdminPassword().setStringValue(new nl.planon.hades.authentication.PasswordHasher().encryptPassword(PDMTestFieldValues.PDMDBSERVER_ORACLE_ADMINPASS_TEST_VALUE));
    pdmDatabaseServerOracle.getFieldHostName().setStringValue(PDMTestFieldValues.PDMDBSERVER_ORACLE_HOSTNAME_TEST_VALUE);
    pdmDatabaseServerOracle.getFieldPortNumber().setIntegerValue(PDMTestFieldValues.PDMDATABASESERVER_PORTNUMBER_TEST_VALUE);
    pdmDatabaseServerOracle.getFieldDbInstanceName().setStringValue(PDMTestFieldValues.PDMDBSERVER_ORACLE_DBINSTANCENAME_TEST_VALUE);
    pdmDatabaseServerOracle.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeOracleDataPumpAndOldExport());
    pdmDatabaseServerOracle.getFieldPDMLocationRef().setAsBaseValue(aPKPDMLocation);
    pdmDatabaseServerOracle.getFieldDatabaseVersionRef().setAsBaseValue(aPKPDMDatabaseVersion);
    pdmDatabaseServerOracle.getFieldExportLocation().setStringValue(PDMTestFieldValues.PDMDBSERVER_ORACLE_EXP_LOCATION_TEST_VALUE);
    pdmDatabaseServerOracle.getFieldImportLocation().setStringValue(PDMTestFieldValues.PDMDBSERVER_ORACLE_IMP_LOCATION_TEST_VALUE);
    pdmDatabaseServerOracle.getFieldIsActive().setBooleanValue(true);
    pdmDatabaseServerOracle.fillMandatoryFields();
    pdmDatabaseServerOracle.save();
    return pdmDatabaseServerOracle;
  }


  /**
   * Creates a new empty unsaved TestBOPDMDatabaseServer object.
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabaseServerOracle newInstance() throws PnErrorListException, AuthorizationException
  {
    final IBOM addBOM = getBOMByPnName(BOTYPE, IBOBasePDMDatabaseServerDef.PN_BOM_ADD);

    final IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    final TestBOPDMDatabaseServerOracle testBO = new TestBOPDMDatabaseServerOracle(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMDatabaseServer object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabaseServerOracle randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionOracle = TestBOPDMDatabaseVersionOracleFactory.randomInstance();
    pdmDatabaseVersionOracle.fillFields();
    pdmDatabaseVersionOracle.save();
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = newInstance();
    pdmDatabaseServerOracle.getFieldDatabaseVersionRef().setAsBaseValue(pdmDatabaseVersionOracle.getPrimaryKeyValue());
    pdmDatabaseServerOracle.fillAllFields();
    pdmDatabaseServerOracle.save();
    return pdmDatabaseServerOracle;
  }


  /**
   * Creates a new TestBOPDMDatabaseServer object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField : IBOFieldValue FieldValue of primary TestBOPDMDatabaseServer to be
   *                          loaded
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMDatabaseServerOracle readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMDatabaseServer object containing data of specified primary key.
   * Generates a runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue : IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMDatabaseServerOracle readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMDatabaseServerOracle testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new instance of TestBOPDMDatabaseServer for testing purposes.
   *
   * @return TestBOPDMDatabaseServer.
   *
   * @throws PnErrorListException
   */
  private static TestBOPDMDatabaseServerOracle createTestBOPDMDatabaseServer() throws PnErrorListException
  {
    return new TestBOPDMDatabaseServerOracle();
  }


  /**
   * Returns a TestBOPDMDatabaseServer object containing data aBOValue which is not destroyed after
   * the test.
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMDatabaseServer the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMDatabaseServerOracle getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMDatabaseServerOracle testBO = new TestBOPDMDatabaseServerOracle(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
