// Planon Enterprise Edition Source file: TestBOPDMTaskAnalyze.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskanalyze;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMTaskAnalyze
 */
public class TestBOPDMTaskAnalyze extends TestBOBasePDMTask
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_TASK_ANALYZE_IMPORT;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskAnalyze object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskAnalyze() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMTaskAnalyze object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskAnalyze(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }


  /**
   * Creates a new TestBOPDMTaskAnalyze object.
   *
   * @param  aBOType
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskAnalyze(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOPDMTaskAnalyze object.
   *
   * @param  aBOType
   * @param  aBOValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMTaskAnalyze(IBOType aBOType, IBOValue aBOValue) throws PnErrorListException
  {
    super(aBOType, aBOValue);
  }
}
