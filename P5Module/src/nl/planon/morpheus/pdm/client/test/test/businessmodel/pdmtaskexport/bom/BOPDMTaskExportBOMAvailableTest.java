// Planon Enterprise Edition Source file: BOPDMTaskExportBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskexport.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskexport.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMTaskExportBOMAvailableTest
 */
public class BOPDMTaskExportBOMAvailableTest extends BOPDMTaskExportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskExportBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMTaskExportBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMTaskDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMTaskDef.PN_BOM_SAVE);
  }
}
