// Planon Enterprise Edition Source file: TestBOPDMTaskExportFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskexport;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

/**
 * TestBOPDMTaskExportFactory
 */
public class TestBOPDMTaskExportFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskExportFactory object.
   */
  private TestBOPDMTaskExportFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMTaskExport.BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMTaskExport object.
   *
   * @return TestBOPDMTaskExport
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskExport newInstance() throws PnErrorListException, AuthorizationException
  {
    IBOM addBOM = getBOMByPnName(getBOType(), PnBOMName.PN_BOM_ADD);

    IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    TestBOPDMTaskExport testBO = new TestBOPDMTaskExport(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMTaskExport object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMTaskExport
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMTaskExport randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMTaskExport testBO = newInstance();
    testBO.fillFields();
    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskExport object by the given IBOFieldValue
   *
   * @param  aFieldValue
   *
   * @return TestBOPDMTaskExport
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskExport readInstance(IBOFieldValue aFieldValue) throws PnErrorListException
  {
    return readInstance(aFieldValue.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMTaskExport object by the given IBaseValue
   *
   * @param  aBaseValue
   *
   * @return TestBOPDMTaskExport
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMTaskExport readInstance(IBaseValue aBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(getBOType(), aBaseValue);
    TestBOPDMTaskExport testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new TestBOPDMTaskExport object by the given BOFieldValue
   *
   * @param  aBOFieldValue
   *
   * @return TestBOPDMTaskExport
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMTaskExport readInstance(TestBOFieldValue aBOFieldValue) throws PnErrorListException
  {
    return readInstance(aBOFieldValue.getBaseValue());
  }


  /**
   * Returns a TestBOPDMTaskExport object by the given IBOValue
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMTaskExport the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMTaskExport getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMTaskExport testBO = new TestBOPDMTaskExport(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
