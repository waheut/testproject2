// Planon Enterprise Edition Source file: BOPDMDatabaseServerOracleTestCase.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserveroracle;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;

/**
 * BOPDMDatabaseServerTestCase
 */
public class BOPDMDatabaseServerOracleTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerTestCase object.
   *
   * @param aName
   */
  public BOPDMDatabaseServerOracleTestCase(String aName)
  {
    super(aName, TestBOPDMDatabaseServerOracle.BOTYPE);
  }
}
