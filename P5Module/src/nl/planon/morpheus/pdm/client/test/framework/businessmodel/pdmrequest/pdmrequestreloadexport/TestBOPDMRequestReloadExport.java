// Planon Enterprise Edition Source file: TestBOPDMRequestReloadExport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadexport;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestReloadExport
 */
public class TestBOPDMRequestReloadExport extends TestBOBasePDMRequest
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_REQUEST_RELOAD_EXPORT;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestReloadExport object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadExport() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMRequestReloadExport object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadExport(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }


  /**
   * Creates a new TestBOPDMRequestReloadExport object.
   *
   * @param  aBOType
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadExport(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOPDMRequestReloadExport object.
   *
   * @param  aBOType
   * @param  aBOValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestReloadExport(IBOType aBOType, IBOValue aBOValue) throws PnErrorListException
  {
    super(aBOType, aBOValue);
  }
}
