// Planon Enterprise Edition Source file: VRDbTypeMatchesFileTypeAndDbServerTypeTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VRDbTypeMatchesFileTypeAndDbServerTypeTest
 */
public class VRDbTypeMatchesFileTypeAndDbServerTypeTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRDbTypeMatchesFileTypeAndDbServerTypeTest object.
   *
   * @param aName
   */
  public VRDbTypeMatchesFileTypeAndDbServerTypeTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test to check if the validation rule works if the database type filled in does not match with
   * the file type filled in.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDbServerTypeAndFileTypeMatchFail() throws PnErrorListException, AuthorizationException
  {
    // create PDM database server and link to version of type MSSQL
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    // for PDM database set DatabaseServerRef to added 'MSSQL' PDM database server and FileTypeRef to 'Oracle'
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmDatabase.getFieldLocationRef().setValue(pdmDatabaseServerMSSQL.getFieldPDMLocationRef());
    pdmDatabase.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeOracleOldExport());

    try
    {
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH);
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH);
    }
  }


  /**
   * Test to check if the validation rule works if the database type filled in does match with the
   * file type filled in.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDbServerTypeAndFileTypeMatchOK() throws PnErrorListException, AuthorizationException
  {
    // create PDM database server and link to version of type MSSQL
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    // for PDM database set both DatabaseServerRef and FileTypeRef to 'MSSQL'
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmDatabase.getFieldLocationRef().setValue(pdmDatabaseServerMSSQL.getFieldPDMLocationRef());
    pdmDatabase.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeMSSQLDump());

    try
    {
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


  /**
   * Test to check if the validation rule works if the database type filled in does match with the
   * file type filled in.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDbTypeAndDbServerMatchOK() throws PnErrorListException, AuthorizationException
  {
    // create PDM database server and link to version of type MSSQL
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    // for PDM database set both DatabaseServerRef and DatabaseTypeRef to 'MSSQL'
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmDatabase.getFieldLocationRef().setValue(pdmDatabaseServerMSSQL.getFieldPDMLocationRef());
    pdmDatabase.getFieldDatabaseType().setAsBaseValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeMssql());

    try
    {
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


//This test is outdated, because of a field change rule that is introduced:
//FCClearDbServerOfDifferentDbType: on change of field database type the field database server needs to be cleared
//if it is linked to a database version of a different database type.
//
//  /**
//   * Test to check if the validation rule works if the database type filled in does not match with
//   * the file type filled in.
//   *
//   * @throws PnErrorListException
//   * @throws AuthorizationException
//   */
//  public void testDbTypeAndDbServerTypeMatchFail() throws PnErrorListException, AuthorizationException
//  {
//    // create PDM database server and link to version of type MSSQL
//    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
//    pdmDatabaseServerMSSQL.fillAllFields();
//    pdmDatabaseServerMSSQL.save();
//
//    // for PDM database set DatabaseServerRef to added 'MSSQL' PDM database server and DatabaseTypeRef to 'Oracle'
//    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
//    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
//    pdmDatabase.getFieldLocationRef().setValue(pdmDatabaseServerMSSQL.getFieldPDMLocationRef());
//    pdmDatabase.getFieldDatabaseType().setLookupValue(PDMDatabaseType.ORACLE.getCode());
//
//    try
//    {
//      pdmDatabase.save();
//      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH);
//    }
//    catch (PnErrorListException ex)
//    {
//      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH);
//    }
//  }

// This test is outdated, because the lookup values are limited:
//   It is not possible to select Oracle database type, once MSSQL is filled in ad file type.
//
//  /**
//   * Test to check if the validation rule works if the database type filled in does not match with
//   * the file type filled in.
//   *
//   * @throws PnErrorListException
//   * @throws AuthorizationException
//   */
//  public void testDbTypeAndFileTypeMatchFail() throws PnErrorListException, AuthorizationException
//  {
//    // for PDM database set FileTypeRef to 'MSSQL' and DatabaseTypeRef to 'Oracle'
//    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
//    pdmDatabase.getFieldFileTypeRef().setLookupValue(PDMFileType.MSSQL_DUMP.getCode());
//    pdmDatabase.getFieldDatabaseType().setLookupValue(PDMDatabaseType.ORACLE.getCode());
//
//    try
//    {
//      pdmDatabase.save();
//      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH);
//    }
//    catch (PnErrorListException ex)
//    {
//      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH);
//    }
//  }

  /**
   * Test to check if the validation rule works if the database type filled in does match with the
   * file type filled in.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDbTypeAndFileTypeMatchOK() throws PnErrorListException, AuthorizationException
  {
    // for PDM database set DatabaseServerRef to added 'MSSQL' PDM database
    // server and DatabaseTypeRef to 'Oracle'
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeOracleOldExport());
    pdmDatabase.getFieldDatabaseType().setAsBaseValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeOracle());

    try
    {
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
