// Planon Enterprise Edition Source file: BRAllowDeleteDatabaseBOTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.bom;

import junit.framework.*;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * BRAllowDeleteDatabaseBOTest
 */
public class BRAllowDeleteDatabaseBOTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BRAllowDeleteDatabaseBOTest object.
   *
   * @param aName
   */
  public BRAllowDeleteDatabaseBOTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that PN_BOM_DELETE is available when contract is in state initial
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public void testDeleteBOMAvailableForNonInitialDatabase() throws AuthorizationException, PnErrorListException
  {
    final TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();

    // test: bom should be available...
    Assert.assertTrue("BOM " + BOPDMDatabaseDef.PN_BOM_DELETE + " not found!", pdmDatabase.getBOValue().getBOMListManager().hasBOMByPnName(BOPDMDatabaseDef.PN_BOM_DELETE));
  }


  /**
   * Test that PN_BOM_DELETE is not available when contract is not in state initial
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public void testDeleteBOMNotAvailableForNonInitialDatabase() throws AuthorizationException, PnErrorListException
  {
    final TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.setStateAndSave(BOPDMDatabaseDef.STATE_BUSY);
    pdmDatabase.read();

    // test: bom should NOT be available...
    Assert.assertTrue("BOM " + BOPDMDatabaseDef.PN_BOM_DELETE + " found!", !pdmDatabase.getBOValue().getBOMListManager().hasBOMByPnName(BOPDMDatabaseDef.PN_BOM_DELETE));
  }
}
