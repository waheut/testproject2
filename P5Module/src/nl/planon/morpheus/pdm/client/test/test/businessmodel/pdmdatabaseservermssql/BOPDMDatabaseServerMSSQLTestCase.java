// Planon Enterprise Edition Source file: BOPDMDatabaseServerMSSQLTestCase.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseservermssql;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;

/**
 * BOPDMDatabaseServerTestCase
 */
public class BOPDMDatabaseServerMSSQLTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerTestCase object.
   *
   * @param aName
   */
  public BOPDMDatabaseServerMSSQLTestCase(String aName)
  {
    super(aName, TestBOPDMDatabaseServerMSSQL.BOTYPE);
  }
}
