// Planon Enterprise Edition Source file: TestBOPDMRequestImport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestImport
 */
public class TestBOPDMRequestImport extends TestBOBasePDMRequest
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_REQUEST_IMPORT;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestImport object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestImport() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMRequestImport object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestImport(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }


  /**
   * Creates a new TestBOPDMRequestImport object.
   *
   * @param  aBOType
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestImport(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOPDMRequestImport object.
   *
   * @param  aBOType
   * @param  aBOValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMRequestImport(IBOType aBOType, IBOValue aBOValue) throws PnErrorListException
  {
    super(aBOType, aBOValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Execute log pdm request BOM.
   *
   * @throws PnErrorListException
   */
  public void executeLogPDMRequestBOM() throws PnErrorListException
  {
    final IBOM bom = getBOMByPnName(IBOBasePDMRequestDef.PN_BOM_LOG_PDMREQUEST);
    bom.getFieldByPnName(IBOBasePDMRequestDef.ARG_MESSAGE_INFORMATION).setAsObject("Succesful logging of PDM Request");
    bom.getFieldByPnName(IBOBasePDMRequestDef.ARG_SEVERITY_OF_LOG).setAsObject(EventLogTypeSeverity.ERROR);
    executeBOM(bom);
  }
}
