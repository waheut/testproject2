// Planon Enterprise Edition Source file: TestBOPDMRequestExportReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestexport;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMRequestExportReference
 */
public class TestBOPDMRequestExportReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestExportReference object.
   */
  public TestBOPDMRequestExportReference()
  {
    super(BOTypePDMP5Module.PDM_REQUEST_EXPORT);
  }
}
