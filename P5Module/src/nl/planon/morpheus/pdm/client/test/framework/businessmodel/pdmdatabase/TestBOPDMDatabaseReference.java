// Planon Enterprise Edition Source file: TestBOPDMDatabaseReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase;

import nl.planon.dionysus.framework.field.*;

/**
 * TestBOPDMDatabaseReference
 */
public class TestBOPDMDatabaseReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseReference object.
   */
  public TestBOPDMDatabaseReference()
  {
    super(TestBOPDMDatabaseFactory.BOTYPE);
  }
}
