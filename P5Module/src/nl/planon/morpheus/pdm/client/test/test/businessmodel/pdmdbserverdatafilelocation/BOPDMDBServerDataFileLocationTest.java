// Planon Enterprise Edition Source file: BOPDMDBServerDataFileLocationTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation.*;

/**
 * BOPDMDBServerDataFileLocationTest
 */
public class BOPDMDBServerDataFileLocationTest extends BOPDMDBServerDataFileLocationTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDBServerDataFileLocationTest object.
   *
   * @param aName
   */
  public BOPDMDBServerDataFileLocationTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of BOPDMDbServerDataFileLocationTest
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDbServerDataFileLocationTestBO = TestBOPDMDBServerDataFileLocationFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocationTestBO = TestBOPDMDBServerDataFileLocationFactory.newInstance();

    IBOFieldValue primaryKey = pdmDBServerDataFileLocationTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMDBServerDataFileLocation pdmDbServerDataFileLocationTestBO_2 = TestBOPDMDBServerDataFileLocationFactory.readInstance(primaryKey);
      pdmDbServerDataFileLocationTestBO_2.read();
      pdmDbServerDataFileLocationTestBO_2.delete();

      pdmDBServerDataFileLocationTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMDBServerDataFileLocation
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocationTestBO = TestBOPDMDBServerDataFileLocationFactory.randomInstance();
    pdmDBServerDataFileLocationTestBO.read();
    pdmDBServerDataFileLocationTestBO.delete();
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocationTestBO = TestBOPDMDBServerDataFileLocationFactory.randomInstance();
    IBOFieldValue primaryKey = pdmDBServerDataFileLocationTestBO.getPrimaryKeyField();

    pdmDBServerDataFileLocationTestBO.read();
    pdmDBServerDataFileLocationTestBO.delete();

    try
    {
      TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocationTestBO_2 = TestBOPDMDBServerDataFileLocationFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMDBServerDataFileLocation.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocationTestBO = TestBOPDMDBServerDataFileLocationFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDBServerDataFileLocationTestBO.getPrimaryKeyField();
    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocationTestBO_2 = TestBOPDMDBServerDataFileLocationFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMDBServerDataFileLocation
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocationTestBO = TestBOPDMDBServerDataFileLocationFactory.randomInstance();
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();

    pdmDBServerDataFileLocationTestBO.getFieldDatabaseServerRef().setAsBaseValue(pdmDatabaseServerOracle.getPrimaryKeyValue());

    pdmDBServerDataFileLocationTestBO.save();
  }
}
