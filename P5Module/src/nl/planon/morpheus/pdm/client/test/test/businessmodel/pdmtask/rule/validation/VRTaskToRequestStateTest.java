// Planon Enterprise Edition Source file: VRTaskToRequestStateTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtask.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * Testcase to test (im)possible state changes on task level, which is dependent on the state of the
 * linked request.
 */
public class VRTaskToRequestStateTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRRequestToTaskStateTest object.
   *
   * @param aName
   */
  public VRTaskToRequestStateTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that it is not possible to put a task to status NotOk, when the linked request is still in
   * state Initial
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testTaskNokRequestInInitial() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);

    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it to Available. not possible!
    try
    {
      //forming states
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }

      task.setStateAndSave(IBOBasePDMRequestDef.STATE_NOK);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_TASK_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_TASK_STATE_IS_NOT_ALLOWED);
    }
  }


  /**
   * Test that it is not possible to put a task to status Ok, when the linked request is still in
   * state Initial
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testTaskokRequestInInitial() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);

    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it to Available. not possible!
    try
    {
      //forming states
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }

      task.setStateAndSave(IBOBasePDMRequestDef.STATE_OK);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_TASK_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_TASK_STATE_IS_NOT_ALLOWED);
    }
  }
}
