// Planon Enterprise Edition Source file: TestBOBasePDMRequestFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestdelete.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestexport.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadexport.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadinitial.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOBaseRequestFactory
 */
public class TestBOBasePDMRequestFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final IBOType BOTYPE = TestBOBasePDMRequest.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOBaseRequestFactory object.
   */
  private TestBOBasePDMRequestFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * Creates a new TestBOPDMRequestImport object by the given IBOFieldValue
   *
   * @param  aFieldValue
   *
   * @return TestBOPDMRequestImport
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOBasePDMRequest readInstance(IBOFieldValue aFieldValue) throws PnErrorListException
  {
    return readInstance(aFieldValue.getBaseValue());
  }


  /**
   * Creates a new TestBOBasePDMRequest object by the given IBaseValue
   *
   * @param  aBaseValue
   *
   * @return TestBOBasePDMRequest
   *
   * @throws PnErrorListException
   */
  public static final TestBOBasePDMRequest readInstance(IBaseValue aBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aBaseValue);
    TestBOBasePDMRequest testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new TestBOBasePDMRequest object by the given BOFieldValue
   *
   * @param  aBOFieldValue
   *
   * @return TestBOBasePDMRequest
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOBasePDMRequest readInstance(TestBOFieldValue aBOFieldValue) throws PnErrorListException
  {
    return readInstance(aBOFieldValue.getBaseValue());
  }


  /**
   * Returns a TestBOPDMRequestImport object by the given IBOValue
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMRequestImport the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOBasePDMRequest getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOBasePDMRequest testBO = null;
    IBOType boType = aBOValue.getBOType();
    if (BOTypePDMP5Module.PDM_REQUEST_IMPORT.equals(boType))
    {
      testBO = new TestBOPDMRequestImport(aBOValue);
    }
    else if (BOTypePDMP5Module.PDM_REQUEST_EXPORT.equals(boType))
    {
      testBO = new TestBOPDMRequestExport(aBOValue);
    }
    else if (BOTypePDMP5Module.PDM_REQUEST_RELOAD_EXPORT.equals(boType))
    {
      testBO = new TestBOPDMRequestReloadExport(aBOValue);
    }
    else if (BOTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL.equals(boType))
    {
      testBO = new TestBOPDMRequestReloadInitial(aBOValue);
    }
    assert BOTypePDMP5Module.PDM_REQUEST_DELETE.equals(boType);

    testBO = new TestBOPDMRequestDelete(aBOValue);

    testBO.setAutomaticFree(false);
    return testBO;
  }
}
