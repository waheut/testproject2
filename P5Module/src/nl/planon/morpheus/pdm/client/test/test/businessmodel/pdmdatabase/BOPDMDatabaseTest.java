// Planon Enterprise Edition Source file: BOPDMDatabaseTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;

/**
 * BOPDMDatabaseTest
 */
public class BOPDMDatabaseTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of BOPDMDatabaseTest
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabaseTestBO = TestBOPDMDatabaseFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabaseTestBO = TestBOPDMDatabaseFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMDatabase pdmDatabaseTestBO_2 = TestBOPDMDatabaseFactory.readInstance(primaryKey);
      pdmDatabaseTestBO_2.read();
      pdmDatabaseTestBO_2.delete();

      pdmDatabaseTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMDatabase
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabaseTestBO = TestBOPDMDatabaseFactory.randomInstance();

    pdmDatabaseTestBO.read();
    pdmDatabaseTestBO.delete();
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabaseTestBO = TestBOPDMDatabaseFactory.randomInstance();
    IBOFieldValue primaryKey = pdmDatabaseTestBO.getPrimaryKeyField();

    pdmDatabaseTestBO.read();
    pdmDatabaseTestBO.delete();

    try
    {
      TestBOPDMDatabase pdmDatabaseTestBO_2 = TestBOPDMDatabaseFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMDatabase.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabaseTestBO = TestBOPDMDatabaseFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseTestBO.getPrimaryKeyField();
    TestBOPDMDatabase pdmDatabaseTestBO_2 = TestBOPDMDatabaseFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMDatabase.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabaseTestBO = TestBOPDMDatabaseFactory.randomInstance();

    pdmDatabaseTestBO.getFieldDescription().setValueRandom();
    pdmDatabaseTestBO.save();
  }
}
