// Planon Enterprise Edition Source file: TestBOPDMRequestReloadInitialFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadinitial;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.businessmodel.*;

/**
 * TestBOPDMRequestReloadInitialFactory
 */
public class TestBOPDMRequestReloadInitialFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestReloadInitialFactory object.
   */
  private TestBOPDMRequestReloadInitialFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMRequestReloadInitial.BOTYPE;
  }
}
