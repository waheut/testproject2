// Planon Enterprise Edition Source file: BOPDMTaskImportTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskimport;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskimport.bom.*;

/**
 * BOPDMTaskImportTestSuite
 */
public class BOPDMTaskImportTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImportTestSuite object.
   */
  public BOPDMTaskImportTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMTaskImportBOMAvailableTest.class);

    // field change tests

    // generic test
    // addTestSuite(BOPDMTaskImportTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImportTestSuite object.
   *
   * @return BOPDMTaskImportTestSuite
   */
  public static Test suite()
  {
    return new BOPDMTaskImportTestSuite();
  }
}
