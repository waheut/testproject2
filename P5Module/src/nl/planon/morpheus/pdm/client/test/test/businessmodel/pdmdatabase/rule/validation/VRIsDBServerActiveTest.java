// Planon Enterprise Edition Source file: VRIsDBServerActiveTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VRCannotDeleteNonInitialPDMDatabaseTest
 */
public class VRIsDBServerActiveTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRCannotDeleteNonInitialPDMDatabaseTest object.
   *
   * @param aName
   */
  public VRIsDBServerActiveTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that field code is only allowed to have word characters ([a-zA-Z_0-9]) in it when adding a
   * new database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testIsDatabaseServerActive() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServer = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE).setValue(false);
//    pdmDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY).setValue(1);

    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
//    pdmDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).setValue(1);
    try
    {
      pdmDatabase.save();
      pdmDatabase.executeImportPDMDatabaseBOM();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_IS_DB_SERVER_ACTIVE);
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_IS_DB_SERVER_ACTIVE);
    }
  }
}
