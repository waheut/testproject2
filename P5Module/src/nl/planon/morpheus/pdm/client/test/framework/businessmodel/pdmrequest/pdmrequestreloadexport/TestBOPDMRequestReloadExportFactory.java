// Planon Enterprise Edition Source file: TestBOPDMRequestReloadExportFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestreloadexport;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.businessmodel.*;

/**
 * TestBOPDMRequestReloadExportFactory
 */
public class TestBOPDMRequestReloadExportFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestReloadExportFactory object.
   */
  private TestBOPDMRequestReloadExportFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMRequestReloadExport.BOTYPE;
  }
}
