// Planon Enterprise Edition Source file: TestBOPDMDatabaseTypeReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMDatabaseTypeReference
 */
public class TestBOPDMDatabaseTypeReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseTypeReference object.
   */
  public TestBOPDMDatabaseTypeReference()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_TYPE);
  }
}
