// Planon Enterprise Edition Source file: BOPDMRequestExportTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestexport;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestexport.bom.*;

/**
 * BOPDMRequestExportTestSuite
 */
public class BOPDMRequestExportTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestExportTestSuite object.
   */
  public BOPDMRequestExportTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMRequestExportBOMAvailableTest.class);

    // field change tests

    // generic test
    // addTestSuite(BOPDMRequestExportTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestExportTestSuite object.
   *
   * @return BOPDMRequestExportTestSuite
   */
  public static Test suite()
  {
    return new BOPDMRequestExportTestSuite();
  }
}
