// Planon Enterprise Edition Source file: BRAllowImportDatabaseTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.bom;

import junit.framework.*;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * BRAllowImportDatabaseTest
 */
public class BRAllowImportDatabaseTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BRAllowImportDatabaseTest object.
   *
   * @param aName
   */
  public BRAllowImportDatabaseTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that PN_BOM_DELETE is available when contract is in state initial
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public void testImportBOMAvailableForNotImportedDatabase() throws AuthorizationException, PnErrorListException
  {
    final TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();

    // test: bom should be available...
    Assert.assertTrue("BOM " + BOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE + " not found!", pdmDatabase.getBOValue().getBOMListManager().hasBOMByPnName(BOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE));
  }


  /**
   * Test that PN_BOM_DELETE is not available when contract is not in state initial
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public void testImportBOMNotAvailableForImportedDatabase() throws AuthorizationException, PnErrorListException
  {
    final TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.setStateAndSave(BOPDMDatabaseDef.STATE_BUSY);
    pdmDatabase.setStateAndSave(BOPDMDatabaseDef.STATE_AVAILABLE);
    pdmDatabase.read();

    // test: bom should NOT be available...
    Assert.assertTrue("BOM " + BOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE + " found!", !pdmDatabase.getBOValue().getBOMListManager().hasBOMByPnName(BOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE));
  }
}
