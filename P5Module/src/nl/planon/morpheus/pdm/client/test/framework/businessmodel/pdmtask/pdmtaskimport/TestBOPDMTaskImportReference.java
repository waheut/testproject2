// Planon Enterprise Edition Source file: TestBOPDMTaskImportReference.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskimport;

import nl.planon.dionysus.framework.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMTaskImportReference
 */
public class TestBOPDMTaskImportReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMTaskImportReference object.
   */
  public TestBOPDMTaskImportReference()
  {
    super(BOTypePDMP5Module.PDM_TASK_IMPORT);
  }
}
