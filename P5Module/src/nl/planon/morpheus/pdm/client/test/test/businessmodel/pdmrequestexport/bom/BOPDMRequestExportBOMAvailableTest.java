// Planon Enterprise Edition Source file: BOPDMRequestExportBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestexport.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestexport.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMRequestExportBOMAvailableTest
 */
public class BOPDMRequestExportBOMAvailableTest extends BOPDMRequestExportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestExportBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMRequestExportBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test Log Request bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableLogRequestBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_LOG_PDMREQUEST);
  }


  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_SAVE);
  }


  /**
   * Test update db version field bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableUpdateDBversionFieldBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMRequestDef.PN_BOM_UPDATE_DBVERSION_FIELD);
  }
}
