// Planon Enterprise Edition Source file: VROnlyWordCharsAllowedInFieldCodeTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VROnlyWordCharsAllowedInFieldCodeTest
 */
public class VROnlyWordCharsAllowedInFieldCodeTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VROnlyWordCharsAllowedInFieldCodeTest object.
   *
   * @param aName
   */
  public VROnlyWordCharsAllowedInFieldCodeTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that field code is not allowed to start with non-alphanumeric character when adding a new
   * database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCodeStartWithNonAlphaNumericCharacterWhenAddingNewDatabaseFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    try
    {
      pdmDatabase.getFieldName().setStringValue("1ABC");
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED, "Only word characters are allowed in field code");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED);
    }
  }


  /**
   * Test that field code is not allowed to have blanks in it when updating the code field of an
   * existing database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCodeStartWithNonAlphaNumericCharacterWhenUpdatingDatabaseFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    try
    {
      pdmDatabase.getFieldName().setStringValue("1ABC");
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED, "Only word characters are allowed in field code");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED);
    }
  }


  /**
   * Test that field code is only allowed to have word characters ([a-zA-Z_0-9]) in it when updating
   * the code field of an existing database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCodeWithOnlyWordCharsWhenUpdatingDatabaseOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    try
    {
      pdmDatabase.getFieldName().setStringValue("AaZz_0123456789BbCcD");
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


  /**
   * Test that field code is only allowed to have word characters ([a-zA-Z_0-9]) in it when adding a
   * new database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCodeWithOnlyWordCharsWhenWhenAddingNewDatabaseOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.newInstance();
    pdmDatabase.getFieldDatabaseServerRef().setAsBaseValue(pdmDbServerOracle.getPrimaryKeyValue());
    pdmDatabase.getFieldLocationRef().setValue(pdmDbServerOracle.getFieldPDMLocationRef());
    pdmDatabase.fillMandatoryFields();
    try
    {
      pdmDatabase.getFieldName().setStringValue("AaZz_0123456789BbCcD");
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


  /**
   * Test that field code is not allowed to have blanks in it when adding a new database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCodeWithSpaceWhenAddingNewDatabaseFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    try
    {
      pdmDatabase.getFieldName().setStringValue("WITH WHITESPACE");
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED, "Only word characters are allowed in field code");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED);
    }
  }


  /**
   * Test that field code is not allowed to have blanks in it when updating the code field of an
   * existing database.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCodeWithSpaceWhenUpdatingDatabaseFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    try
    {
      pdmDatabase.getFieldName().setStringValue("WITH WHITESPACE");
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED, "Only word characters are allowed in field code");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED);
    }
  }
}
