// Planon Enterprise Edition Source file: VRCannotInactivateDbServerIfBusyDbPresentTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserver.rule;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseservermssql.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VRCannotInactivateDbServerIfBusyDbPresentTest
 */
public class VRCannotInactivateDbServerIfBusyDbPresentTest extends BOPDMDatabaseServerMSSQLTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRCannotInactivateDbServerIfBusyDbPresentTest object.
   *
   * @param aName
   */
  public VRCannotInactivateDbServerIfBusyDbPresentTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that field IsActive of database server cannot be changed when there are databases in state
   * Busy linked to it.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldIsActiveFail() throws PnErrorListException, AuthorizationException
  {
    // Create database, will be in state initial
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.readInstance(pdmDatabase.getFieldDatabaseServerRef().getBaseValue());
    // Set database to state Busy
    pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_BUSY);

    pdmDbServer.getFieldIsActive().setBooleanValue(false);
    try
    {
      // Since there is a database in state Busy linked, it is not possible to
      // inactivate the database server.
      pdmDbServer.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_INACTIVATE_DBSERVER_WITH_BUSY_DB, "Not allowed to deactivate database server");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_CANNOT_INACTIVATE_DBSERVER_WITH_BUSY_DB);
    }
  }


  /**
   * Test that field PDM location of database server cannot be changed when there are databases in
   * state Busy linked to it.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldPDMLocationFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocation = TestBOPDMLocationFactory.randomInstance();
    // Create database, will be in state initial
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.readInstance(pdmDatabase.getFieldDatabaseServerRef().getBaseValue());

    // Set database to state Busy
    pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_BUSY);

    pdmDbServer.getFieldPDMLocationRef().setAsBaseValue(pdmLocation.getPrimaryKeyValue());
    try
    {
      // Since there is a database in state Busy linked, it is not possible to
      // inactivate the database server.
      pdmDbServer.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_INACTIVATE_DBSERVER_WITH_BUSY_DB, "Not allowed to change PDM location of database server");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_CANNOT_INACTIVATE_DBSERVER_WITH_BUSY_DB);
    }
  }


  /**
   * Test that field IsActive of database server can be changed when there are databases in state
   * Initial linked to it.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyIsActiveWhenInitialDbsOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    // Create database, will be in state initial
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setAsBaseValue(pdmDbServer.getPrimaryKeyValue());
    try
    {
      // Since there are no databases in state busy linked to the database
      // server, it can be set to inactive.
      pdmDbServer.getFieldIsActive().setBooleanValue(false);
      pdmDbServer.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


  /**
   * Test that field IsActive of database server can be changed when there are no databases in state
   * Busy linked to it.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyIsActiveWhenNoDbsOk() throws PnErrorListException, AuthorizationException
  {
    // create new database server, so there are no databases linked to it yet
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    try
    {
      pdmDbServer.getFieldIsActive().setBooleanValue(false);
      pdmDbServer.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


  /**
   * Test that field PDM location of database server can be changed when there are databases in
   * state Initial linked to it.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyPDMLocationWhenInitialDbsOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    TestBOPDMLocation pdmLocation = TestBOPDMLocationFactory.randomInstance();
    // Create database, will be in state initial
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setAsBaseValue(pdmDbServer.getPrimaryKeyValue());
    try
    {
      // Since there are no databases in state busy linked to the database
      // server, it can be set to inactive.
      pdmDbServer.getFieldPDMLocationRef().setAsBaseValue(pdmLocation.getPrimaryKeyValue());
      pdmDbServer.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }


  /**
   * Test that field PDM location of database server can be changed when there are no databases in
   * state Busy linked to it.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyPDMLocationWhenNoDbsOk() throws PnErrorListException, AuthorizationException
  {
    // create new database server, so there are no databases linked to it yet
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    TestBOPDMLocation pdmLocation = TestBOPDMLocationFactory.randomInstance();
    try
    {
      pdmDbServer.getFieldPDMLocationRef().setAsBaseValue(pdmLocation.getPrimaryKeyValue());
      pdmDbServer.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
