// Planon Enterprise Edition Source file: VRReadOnlyFieldsForNonInitialDatabaseTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VRReadOnlyFieldsForNonInitialDatabaseTest
 */
public class VRReadOnlyFieldsForNonInitialDatabaseTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRReadOnlyFieldsForNonInitialDatabaseTest object.
   *
   * @param aName
   */
  public VRReadOnlyFieldsForNonInitialDatabaseTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that fields of database cannot be changed when it is in state Available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldOfAvailableDatabaseFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_BUSY);
    pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_AVAILABLE);
    try
    {
      pdmDatabase.getFieldName().setValueRandom();
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_NON_INITIAL_DATABASE, "Not allowed to change fields");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_NON_INITIAL_DATABASE);
    }
  }


  /**
   * Test that fields of database cannot be changed when it is in busy state.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldOfBusyDatabaseFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_BUSY);
    try
    {
      pdmDatabase.getFieldName().setValueRandom();
      pdmDatabase.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_NON_INITIAL_DATABASE, "Not allowed to change fields");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_NON_INITIAL_DATABASE);
    }
  }


  /**
   * Test that fields of database can be changed when it is in intial state.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldsOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_INITIAL);
    try
    {
      pdmDatabase.getFieldName().setValueRandom();
      pdmDatabase.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
