// Planon Enterprise Edition Source file: TestBOPDMDatabaseServer.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * TestBOPDMDatabaseServer
 */
public class TestBOPDMDatabaseServer extends TestBO
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseServer object.
   *
   * @param  aBOType DOCUMENT ME!
   *
   * @throws PnErrorListException DOCUMENT ME!
   */
  public TestBOPDMDatabaseServer(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOPDMDatabaseServer object.
   *
   * @param  aBOType
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseServer(IBOType aBOType, IBOValue aValue) throws PnErrorListException
  {
    super(aBOType, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * convenience method for field PN_ADMIN_PASS
   *
   * @return field PN_ADMIN_PASS
   */
  public TestBOFieldValue getFieldAdminPassword()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS);
  }


  /**
   * convenience method for field PN_ADMIN_USER
   *
   * @return field PN_ADMIN_USER
   */
  public TestBOFieldValue getFieldAdminUser()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_USER);
  }


  /**
   * convenience method for field PN_CODE
   *
   * @return field PN_CODE
   */
  public TestBOFieldValue getFieldCode()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_CODE);
  }


  /**
   * convenience method for field PN_DATABASE_VERSION_REF
   *
   * @return field PN_DATABASE_VERSION_REF
   */
  public TestBOFieldValue getFieldDatabaseVersionRef()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);
  }


  /**
   * convenience method for field PN_FILE_TYPE_REF
   *
   * @return field PN_FILE_TYPE_REF
   */
  public TestBOFieldValue getFieldFileTypeRef()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF);
  }


  /**
   * convenience method for field PN_HOST_NAME
   *
   * @return field PN_HOST_NAME
   */
  public TestBOFieldValue getFieldHostName()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_HOST_NAME);
  }


  /**
   * convenience method for field PN_IS_ACTIVE
   *
   * @return field PN_IS_ACTIVE
   */
  public TestBOFieldValue getFieldIsActive()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE);
  }


  /**
   * convenience method for field PN_NAME
   *
   * @return field PN_NAME
   */
  public TestBOFieldValue getFieldName()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_NAME);
  }


  /**
   * convenience method for field PN_DATABASE_VERSION_REF
   *
   * @return field PN_DATABASE_VERSION_REF
   */
  public TestBOFieldValue getFieldPDMLocationRef()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF);
  }


  /**
   * links version
   *
   * @param  aVersion version to link
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void linkVersion(TestBOPDMDatabaseVersion aVersion) throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix compatible = TestBOPDMCompatibilityMatrixFactory.randomInstance(this, aVersion);
  }
}
