// Planon Enterprise Edition Source file: MSSQLRequestTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.request.mssql;

import java.io.*;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.test.request.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * MSSQLRequestTest
 */
public class MSSQLRequestTest extends PDMRequestsTest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLRequestTest object.
   *
   * @param aName
   */
  public MSSQLRequestTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Test a failing MSSQL import request due to invalid user name to connect to
   * databaseserver.</p>
   *
   * <p>After adding a standard configuration of a pdm MSSQL database, the field <code>
   * AdminUser</code> is changed to an invalid value. Then an import request is executed, which is
   * then expected to fail.</p>
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws IOException
   */
  public void testImportMSSQLDumpFail() throws PnErrorListException, AuthorizationException, IOException
  {
    TestBOPDMDatabase pdmDatabaseMSSQL = TestBOPDMDatabaseFactory.createStandardPDMDatabaseOfTypeMSSQL();
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServer = TestBOPDMDatabaseServerMSSQLFactory.readInstance(pdmDatabaseMSSQL.getFieldDatabaseServerRef().getBaseValue());
    pdmDatabaseServer.activate(false);
    pdmDatabaseServer.getFieldAdminUser().setStringValue("InvalidUserName");
    pdmDatabaseServer.save();
    pdmDatabaseServer.activate(true);
    pdmDatabaseMSSQL.executeImportPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseMSSQL);
    waitForRequestToFinish(pdmDatabaseMSSQL, MAX_MINUTES_TO_WAIT_FOR_IMPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedFailedRequest(pdmDatabaseMSSQL, IBOPDMDatabaseDef.STATE_INITIAL);
  }


  /**
   * <p>Test standard MSSQL dump requests.</p>
   *
   * <p>After the creation of a standard configuration of a pdm mssql database, the following
   * requests are executed:</p>
   *
   * <ul>
   *   <li>import</li>
   *   <li>reload initial</li>
   *   <li>export</li>
   *   <li>reload export</li>
   *   <li>clean up</li>
   * </ul>
   *
   * <p>For each request is tested if it is picked up and if it is executed successfully.</p>
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws IOException
   */
  public void testMSSQLDumpRequestsOk() throws PnErrorListException, AuthorizationException, IOException
  {
    TestBOPDMDatabase pdmDatabaseMSSQL = TestBOPDMDatabaseFactory.createStandardPDMDatabaseOfTypeMSSQL();
    pdmDatabaseMSSQL.saveAndUploadDumpFile();
    pdmDatabaseMSSQL.executeImportPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseMSSQL);
    waitForRequestToFinish(pdmDatabaseMSSQL, MAX_MINUTES_TO_WAIT_FOR_IMPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseMSSQL, IBOPDMDatabaseDef.STATE_AVAILABLE);

    pdmDatabaseMSSQL.executeReloadInitialPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseMSSQL);
    waitForRequestToFinish(pdmDatabaseMSSQL, MAX_MINUTES_TO_WAIT_FOR_RELOAD_INITIAL_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseMSSQL, IBOPDMDatabaseDef.STATE_AVAILABLE);

    pdmDatabaseMSSQL.executeExportPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseMSSQL);
    waitForRequestToFinish(pdmDatabaseMSSQL, MAX_MINUTES_TO_WAIT_FOR_EXPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseMSSQL, IBOPDMDatabaseDef.STATE_AVAILABLE);

    pdmDatabaseMSSQL.executeReloadExportedPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseMSSQL);
    waitForRequestToFinish(pdmDatabaseMSSQL, MAX_MINUTES_TO_WAIT_FOR_RELOAD_EXPORT_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseMSSQL, IBOPDMDatabaseDef.STATE_AVAILABLE);

    pdmDatabaseMSSQL.executeCleanupPDMDatabaseBOM();
    sleepingTime(MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP); // wait a while to have the request picked up by controller agent
    checkStatesForPickedUpRequest(pdmDatabaseMSSQL);
    waitForRequestToFinish(pdmDatabaseMSSQL, MAX_MINUTES_TO_WAIT_FOR_CLEANUP_REQUEST_TO_FINISH);
    checkStatesForFinishedSuccessfulRequest(pdmDatabaseMSSQL, IBOPDMDatabaseDef.STATE_INITIAL);
  }
}
