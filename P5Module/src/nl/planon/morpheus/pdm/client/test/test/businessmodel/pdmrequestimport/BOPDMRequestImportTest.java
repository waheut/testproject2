// Planon Enterprise Edition Source file: BOPDMRequestImportTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport;

/**
 * BOPDMRequestImportTest
 */
public class BOPDMRequestImportTest extends BOPDMRequestImportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestImportTest object.
   *
   * @param aName
   */
  public BOPDMRequestImportTest(String aName)
  {
    super(aName);
  }
}
