// Planon Enterprise Edition Source file: RegisterFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test;

import nl.planon.dionysus.framework.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * RegisterFactory
 */
public class RegisterFactory
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new RegisterFactory object.
   */
  public RegisterFactory()
  {
    TestBOFactory.addFactory(BOTypePDMP5Module.PDM_DATABASE, TestBOPDMDatabaseFactory.class);
    TestBOFactory.addFactory(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, TestBOPDMDatabaseServerOracleFactory.class);
    TestBOFactory.addFactory(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION, TestBOPDMDatabaseVersionMSSQLFactory.class);
    TestBOFactory.addFactory(BOTypePDMP5Module.PDM_LOCATION, TestBOPDMLocationFactory.class);
    TestBOFactory.addFactory(BOTypePDMP5Module.BASE_PDM_REQUEST, TestBOPDMRequestImportFactory.class);
  }
}
