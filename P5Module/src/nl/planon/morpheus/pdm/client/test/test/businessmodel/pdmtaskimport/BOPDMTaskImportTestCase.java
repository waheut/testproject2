// Planon Enterprise Edition Source file: BOPDMTaskImportTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskimport;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.pdmtaskimport.*;

/**
 * BOPDMTaskImportTestCase
 */
public class BOPDMTaskImportTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImportTestCase object.
   *
   * @param aName
   */
  public BOPDMTaskImportTestCase(String aName)
  {
    super(aName, TestBOPDMTaskImport.BOTYPE);
  }
}
