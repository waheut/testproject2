// Planon Enterprise Edition Source file: BOPDMCompatibilityMatrixTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix.bom.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix.rule.validation.*;

/**
 * BOPDMCompatibilityMatrixTestSuite
 */
public class BOPDMCompatibilityMatrixTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMCompatibilityMatrixTestSuite object.
   */
  public BOPDMCompatibilityMatrixTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMCompatibilityMatrixBOMAvailableTest.class);

    // field change tests

    // generic test
    addTestSuite(BOPDMCompatibilityMatrixTest.class);

    // validation rule test
    addTestSuite(VREqualTypeOfDbServerAndDBVersionTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMCompatibilityMatrixTestSuite object.
   *
   * @return BOPDMCompatibilityMatrixTestSuite
   */
  public static Test suite()
  {
    return new BOPDMCompatibilityMatrixTestSuite();
  }
}
