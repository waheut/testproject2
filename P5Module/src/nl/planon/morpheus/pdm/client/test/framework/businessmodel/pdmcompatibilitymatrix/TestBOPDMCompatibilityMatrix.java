// Planon Enterprise Edition Source file: TestBOPDMCompatibilityMatrix.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmcompatibilitymatrix;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * TestBOPDMCompatibilityMatrix
 */
public class TestBOPDMCompatibilityMatrix extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = TestBOPDMCompatibilityMatrixFactory.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMCompatibilityMatrix object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMCompatibilityMatrix() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMCompatibilityMatrix object.
   *
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMCompatibilityMatrix(IBOValue aValue) throws PnErrorListException
  {
    super(BOTYPE, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * convenience method for field PN_DATABASESERVER_REF
   *
   * @return field PN_DATABASESERVER_REF
   */
  public TestBOFieldValue getFieldDatabaseServerRef()
  {
    return getFieldByPnName(IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
  }


  /**
   * convenience method for field PN_ACCOUNT_DATABASEOWNER_REF
   *
   * @return field PN_ACCOUNT_DATABASEOWNER_REF
   */
  public TestBOFieldValue getFieldDatabaseVersionRef()
  {
    return getFieldByPnName(IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
  }
}
