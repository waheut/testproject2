// Planon Enterprise Edition Source file: TestBOBasePDMTask.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOBasePDMTask
 */
public class TestBOBasePDMTask extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.BASE_PDM_TASK;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOBasePDMTask object.
   *
   * @param  aBOType
   *
   * @throws PnErrorListException
   */
  public TestBOBasePDMTask(IBOType aBOType) throws PnErrorListException
  {
    super(aBOType);
  }


  /**
   * Creates a new TestBOBasePDMTask object.
   *
   * @param  aBOType
   * @param  aBOValue
   *
   * @throws PnErrorListException
   */
  public TestBOBasePDMTask(IBOType aBOType, IBOValue aBOValue) throws PnErrorListException
  {
    super(aBOType, aBOValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * getFieldRequestRef
   *
   * @return
   */
  public TestBOFieldValue getFieldRequestRef()
  {
    return getFieldByPnName(IBOBasePDMTaskDef.PN_PDMREQUEST_REF);
  }
}
