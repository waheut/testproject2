// Planon Enterprise Edition Source file: VRReadOnlyFieldsForActiveDbServerTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserver.rule;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseservermssql.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * VRReadOnlyFieldsForActiveDbServerTest
 */
public class VRReadOnlyFieldsForActiveDbServerTest extends BOPDMDatabaseServerMSSQLTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRReadOnlyFieldsForActiveDbServerTest object.
   *
   * @param aName
   */
  public VRReadOnlyFieldsForActiveDbServerTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that fields of database server cannot be changed when it is active.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldsFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDbServer.getFieldIsActive().setBooleanValue(true);
    pdmDbServer.save();
    try
    {
      pdmDbServer.getFieldAdminUser().setValueRandom();
      pdmDbServer.save();
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_ACTIVE_DBSERVER, "Not allowed to change fields");
    }
    catch (PnErrorListException ex)
    {
      assertContainsMessageType(ex, ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_ACTIVE_DBSERVER);
    }
  }


  /**
   * Test that fields of database server can be changed when it is not active.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldsOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDbServer.getFieldIsActive().setBooleanValue(false);
    pdmDbServer.save();
    try
    {
      pdmDbServer.getFieldAdminUser().setValueRandom();
      pdmDbServer.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
  }
}
