// Planon Enterprise Edition Source file: VRRequestToTaskStateTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequest.rule.validation;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * Testcase to test (im)possible state changes on request level, which is dependent on the state of
 * the last added task.
 */
public class VRRequestToTaskStateTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRRequestToTaskStateTest object.
   *
   * @param aName
   */
  public VRRequestToTaskStateTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that it is not possible to put a request to status NotOk, when the last added task is
   * still in state InProgress
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testRequestNokTaskinProgress() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);

    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it to NotOk. not possible!
    try
    {
      //forming states
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      if (request.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        request.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }

      request.setStateAndSave(IBOBasePDMRequestDef.STATE_NOK);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
  }


  /**
   * Test that it is not possible to put a request to status NotOk, when the last added task is in
   * state Ok
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testRequestNokTaskOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);

    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it to Available. not possible!
    try
    {
      //forming states
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      if (request.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        request.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_IN_PROGRESS))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_OK);
      }

      request.setStateAndSave(IBOBasePDMRequestDef.STATE_NOK);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
  }


  /**
   * Test that it is not possible to put a request to status Ok, when the last added task is still
   * in state InProgress
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testRequestOkTaskinProgress() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);

    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it to Available. not possible!
    try
    {
      //forming states
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      if (request.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        request.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      request.setStateAndSave(IBOBasePDMRequestDef.STATE_OK);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
  }


  /**
   * Test that it is not possible to put a request to status Ok, when the last added task is in
   * state NotOk
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testRequestOkTaskNOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);

    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it to Available. not possible!
    try
    {
      //forming states
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }

      if (request.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        request.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_IN_PROGRESS))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_NOK);
      }

      request.setStateAndSave(IBOBasePDMRequestDef.STATE_OK);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED);
    }
  }
}
