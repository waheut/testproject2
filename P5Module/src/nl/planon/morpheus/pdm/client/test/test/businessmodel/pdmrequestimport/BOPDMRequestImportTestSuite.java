// Planon Enterprise Edition Source file: BOPDMRequestImportTestSuite.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport.bom.*;

/**
 * BOPDMRequestImportTestSuite
 */
public class BOPDMRequestImportTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestImportTestSuite object.
   */
  public BOPDMRequestImportTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMRequestImportBOMAvailableTest.class);

    // field change tests

    // generic test
    // addTestSuite(BOPDMRequestImportTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestImportTestSuite object.
   *
   * @return BOPDMRequestImportTestSuite
   */
  public static Test suite()
  {
    return new BOPDMRequestImportTestSuite();
  }
}
