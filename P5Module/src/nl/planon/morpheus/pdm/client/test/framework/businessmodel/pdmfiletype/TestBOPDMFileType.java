// Planon Enterprise Edition Source file: TestBOPDMFileType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * TestBOPDMFileType
 */
public class TestBOPDMFileType extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_DATABASE_FILE_TYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMFileType object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMFileType() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMFileType object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMFileType(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the code field.
   *
   * @return the code field.
   */
  public TestBOFieldValue getFieldCode()
  {
    return getFieldByPnName(BOPDMFileTypeDef.PN_CODE);
  }


  /**
   * Returns the name field.
   *
   * @return the name field.
   */
  public TestBOFieldValue getFieldName()
  {
    return getFieldByPnName(BOPDMFileTypeDef.PN_NAME);
  }
}
