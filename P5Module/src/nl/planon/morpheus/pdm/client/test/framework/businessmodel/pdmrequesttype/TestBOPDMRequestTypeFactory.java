// Planon Enterprise Edition Source file: TestBOPDMRequestTypeFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequesttype;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

/**
 * TestBOPDMRequestTypeFactory
 */
public class TestBOPDMRequestTypeFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final IBOType BOTYPE = TestBOPDMRequestType.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestTypeFactory object.
   */
  public TestBOPDMRequestTypeFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * Creates a new TestBOPDMRequestType object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField IBOFieldValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMRequestType
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMRequestType readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMRequestType object containing data of specified primary key. Generates a
   * runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMRequestType
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMRequestType readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMRequestType pdmRequestType = getReadOnlyTestBO(boValue);
    return pdmRequestType;
  }


  /**
   * Creates a new TestBOPDMRequestType object containing data of value in specified primary key
   * field.
   *
   * @param  aPrimaryKeyField TestBOFieldValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMRequestType
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMRequestType readInstance(TestBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Returns a TestBOPDMRequestType object containing data aBOValue which is not destroyed after the
   * test.
   *
   * @param  aBOValue the BOValue the value to be used
   *
   * @return TestBOPDMRequestType the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMRequestType getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMRequestType pdmRequestType = new TestBOPDMRequestType(aBOValue);
    pdmRequestType.setAutomaticFree(false);
    return pdmRequestType;
  }
}
