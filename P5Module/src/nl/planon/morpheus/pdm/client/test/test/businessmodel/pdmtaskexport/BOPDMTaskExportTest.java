// Planon Enterprise Edition Source file: BOPDMTaskExportTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskexport;

/**
 * BOPDMTaskExportTest
 */
public class BOPDMTaskExportTest extends BOPDMTaskExportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskExportTest object.
   *
   * @param aName
   */
  public BOPDMTaskExportTest(String aName)
  {
    super(aName);
  }
}
