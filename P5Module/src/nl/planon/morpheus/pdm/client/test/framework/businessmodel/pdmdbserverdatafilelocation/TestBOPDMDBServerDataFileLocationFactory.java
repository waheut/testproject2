// Planon Enterprise Edition Source file: TestBOPDMDBServerDataFileLocationFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMDBServerDataFileLocationFactory
 */
public class TestBOPDMDBServerDataFileLocationFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_DBSERVER_DATAFILELOCATION;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * create a PDM Database Server Data File Location BO with a valid local file location filled in
   *
   * @param  aPKPDMDatabaseServer primary key of database server to which this BO belongs
   *
   * @return a completed PDM Database Server Data File Location BO
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static TestBOPDMDBServerDataFileLocation getStandardBOPDMDBServerDataFileLocation(IBaseValue aPKPDMDatabaseServer) throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocation = newInstance();
    pdmDBServerDataFileLocation.getFieldDatabaseServerRef().setAsBaseValue(aPKPDMDatabaseServer);
    pdmDBServerDataFileLocation.getFieldFileLocationLocal().setStringValue(PDMTestFieldValues.PDMDBSERVER_DATAFILE_LOCATION_LOCATIONLOCAL);
    pdmDBServerDataFileLocation.fillMandatoryFields();
    pdmDBServerDataFileLocation.save();
    return pdmDBServerDataFileLocation;
  }


  /**
   * Creates a new empty unsaved TestBOPDMDBServerDataFileLocation object.
   *
   * @return TestBOPDMDBServerDataFileLocation
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMDBServerDataFileLocation newInstance() throws PnErrorListException, AuthorizationException
  {
    final IBOM addBOM = getBOMByPnName(BOTYPE, IBOPDMDBServerDataFileLocationDef.PN_BOM_ADD);

    final IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    final TestBOPDMDBServerDataFileLocation testBO = new TestBOPDMDBServerDataFileLocation(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMDBServerDataFileLocation object containing random data. Errors
   * result in an ErrorListException
   *
   * @return TestBOPDMDBServerDataFileLocation
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMDBServerDataFileLocation randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();

    TestBOPDMDBServerDataFileLocation testBO = newInstance();
    testBO.getFieldDatabaseServerRef().setAsBaseValue(pdmDatabaseServerMSSQL.getPrimaryKeyValue());
    testBO.fillFields();
    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMDBServerDataFileLocation object containing data of specified primary
   * key.
   *
   * @param  aPrimaryKeyField : IBOFieldValue FieldValue of primary
   *                          TestBOPDMDBServerDataFileLocation to be loaded
   *
   * @return TestBOPDMDBServerDataFileLocation
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMDBServerDataFileLocation readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMDBServerDataFileLocation object containing data of specified primary
   * key. Generates a runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue : IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDBServerDataFileLocation
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMDBServerDataFileLocation readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMDBServerDataFileLocation testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new instance of TestBOPDMDBServerDataFileLocation for testing purposes.
   *
   * @return TestBOPDMDBServerDataFileLocation
   *
   * @throws PnErrorListException
   */
  private static TestBOPDMDBServerDataFileLocation createTestBOPDMDBServerDataFileLocation() throws PnErrorListException
  {
    return new TestBOPDMDBServerDataFileLocation();
  }


  /**
   * Returns a TestBOPDMDBServerDataFileLocation object containing data aBOValue which is not
   * destroyed after the test.
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMDBServerDataFileLocation the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMDBServerDataFileLocation getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMDBServerDataFileLocation testBO = new TestBOPDMDBServerDataFileLocation(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
