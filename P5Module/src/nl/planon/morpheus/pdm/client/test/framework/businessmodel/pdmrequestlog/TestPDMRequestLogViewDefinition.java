// Planon Enterprise Edition Source file: TestPDMRequestLogViewDefinition.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequestlog;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlog.linked.*;
import nl.planon.hades.businessmodel.eventlogbusinessobject.*;
import nl.planon.hades.dionysus.businessmodeltest.client.framework.businessmodel.bodefinition.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestPDMRequestLogViewDefinition
 */
public class TestPDMRequestLogViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  PVJoinDef joinEventLogBusinessObject;
  PVJoinDef joinPDMRequest;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestPDMRequestLogViewDefinition object.
   */
  public TestPDMRequestLogViewDefinition()
  {
    super(BOTypeHades.LINKED_EVENT_LOG);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * @see PVDefinition#createFieldDefs()
   */
  @Override protected void createFieldDefs(PnContext aPnContext)
  {
    addFieldDef(new PVSelectFieldDef(BOLinkedEventLogDef.PN_LOGTYPE));
  }


  /**
   * @see PVDefinition#createJoinDefs()
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinEventLogBusinessObject = addInnerJoinDef(BOTypeHades.EVENTLOGBUSINESSOBJECT, IBOEventLog_BusinessObjectDef.PN_EVENTLOG_REF);
    this.joinPDMRequest = this.joinEventLogBusinessObject.addInnerJoinDef(IBOEventLog_BusinessObjectDef.PN_BO_REF);
  }


  /**
   * @see PVDefinition#createOrderDefs()
   */
  @Override protected void createOrderDefs(final PnContext aPnContext) throws PnErrorListException
  {
    super.createOrderDefs(aPnContext);
    addSortDef(new PVSortFieldDef(BOLinkedEventLogDef.PN_SYSINSERTDATETIME, false));
  }


  /**
   * @see PVDefinition#createWhereDefs()
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchDef(new PVExpressionNodeDef(this.joinEventLogBusinessObject, IBOEventLog_BusinessObjectDef.PN_BODEFINITION_REF, SCOperatorType.EQUAL, TestBOBaseBODefinitionFactory.getPrimarykey(BOTypePDMP5Module.BASE_PDM_REQUEST)));
  }
}
