// Planon Enterprise Edition Source file: TestBOPDMDatabaseServerOracle.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle;

import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle.*;

import nl.planon.util.pnlogging.*;

/**
 * TestBOPDMDatabaseServerOracle
 */
public class TestBOPDMDatabaseServerOracle extends TestBOPDMDatabaseServer
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(TestBOPDMDatabaseServerOracle.class);
  public static final IBOType BOTYPE = TestBOPDMDatabaseServerOracleFactory.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseServer object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseServerOracle() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMDatabaseVersion object.
   *
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseServerOracle(IBOValue aValue) throws PnErrorListException
  {
    super(BOTYPE, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * activate/deactivate Database Server
   *
   * @param  active true to activate / false to deactivate database server
   *
   * @throws PnErrorListException
   */
  public void activate(boolean active) throws PnErrorListException
  {
    this.getFieldIsActive().setBooleanValue(active);
    this.save();
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillDefaultFields() throws PnErrorListException, AuthorizationException
  {
    getFieldFileTypeRef().setAsBaseValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeOracle());
  }


  /**
   * convenience method for field PN_CONNECT_TNS_NAME
   *
   * @return field PN_CONNECT_TNS_NAME
   */
  public TestBOFieldValue getFieldConnectTNSName()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_CONNECT_TNS_NAME__NOT_IN_ALL_SUBTYPES);
  }


  /**
   * convenience method for field PN_DATABASE_INSTANCENAME
   *
   * @return field PN_DATABASE_INSTANCENAME
   */
  public TestBOFieldValue getFieldDbInstanceName()
  {
    return getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_DATABASE_INSTANCENAME);
  }


  /**
   * convenience method for field PN_EXP_LOCATION
   *
   * @return field PN_EXP_LOCATION
   */
  public TestBOFieldValue getFieldExportLocation()
  {
    return getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_EXP_LOCATION);
  }


  /**
   * convenience method for field PN_EXP_LOCATION
   *
   * @return field PN_EXP_LOCATION
   */
  public TestBOFieldValue getFieldImportLocation()
  {
    return getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_IMP_LOCATION);
  }


  /**
   * convenience method for field PN_PORT_NUMBER
   *
   * @return field PN_PORT_NUMBER
   */
  public TestBOFieldValue getFieldPortNumber()
  {
    return getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_PORT_NUMBER);
  }


  /**
   * convenience method for field PN_TEMPORARY_TABLESPACENAME
   *
   * @return field PN_TEMPORARY_TABLESPACENAME
   */
  public TestBOFieldValue getFieldTemporaryTableSpace()
  {
    return getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_TEMPORARY_TABLESPACENAME__NOT_IN_ALL_SUBTYPES);
  }
}
