// Planon Enterprise Edition Source file: TestBOPDMDatabaseServerMSSQL.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql;

import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql.*;

import nl.planon.util.pnlogging.*;

/**
 * TestBOPDMDatabaseServer
 */
public class TestBOPDMDatabaseServerMSSQL extends TestBOPDMDatabaseServer
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(TestBOPDMDatabaseServerMSSQL.class);
  public static final IBOType BOTYPE = TestBOPDMDatabaseServerMSSQLFactory.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseServer object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseServerMSSQL() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMDatabaseVersion object.
   *
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseServerMSSQL(IBOValue aValue) throws PnErrorListException
  {
    super(BOTYPE, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * activate/deactivate Database Server
   *
   * @param  active true to activate / false to deactivate database server
   *
   * @throws PnErrorListException
   */
  public void activate(boolean active) throws PnErrorListException
  {
    this.getFieldIsActive().setBooleanValue(active);
    this.save();
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillDefaultFields() throws PnErrorListException, AuthorizationException
  {
    getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeMSSQLDump());
  }


  /**
   * convenience method for field PN_CODE_PAGE
   *
   * @return field PN_CODE_PAGE
   */
  public TestBOFieldValue getFieldCodePage()
  {
    return getFieldByPnName(BOPDMDatabaseServerMSSQLDef.PN_CODE_PAGE);
  }


  /**
   * convenience method for field PN_SORT_ORDER
   *
   * @return field PN_SORT_ORDER
   */
  public TestBOFieldValue getFieldSortOrder()
  {
    return getFieldByPnName(BOPDMDatabaseServerMSSQLDef.PN_SORT_ORDER);
  }
}
