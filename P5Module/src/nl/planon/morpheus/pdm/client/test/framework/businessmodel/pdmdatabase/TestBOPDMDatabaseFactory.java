// Planon Enterprise Edition Source file: TestBOPDMDatabaseFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase;

import java.io.*;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMDatabaseFactory
 */
public class TestBOPDMDatabaseFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_DATABASE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * create a fully completed PDM Database BO of type MSSQL. After creating the necessary BO's that
   * are referenced by this very same BO.
   *
   * @return the fully completed PDM Database BO
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws IOException
   */
  public static TestBOPDMDatabase createStandardPDMDatabaseOfTypeMSSQL() throws PnErrorListException, AuthorizationException, IOException
  {
    // create PDM database version of type MSSQL
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();
    IBaseValue pkPDMDatabaseVersion = pdmDatabaseVersionMSSQL.getPrimaryKeyValue();

    TestBOPDMLocation pdmLocation = TestBOPDMLocationFactory.getStandardPDMLocation();
    IBaseValue pkPDMLocation = pdmLocation.getPrimaryKeyValue();

    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.getStandardMSSQLDatabaseServer(pkPDMDatabaseVersion, pkPDMLocation);
    IBaseValue pkPDMDatabaseServer = pdmDatabaseServerMSSQL.getPrimaryKeyValue();

    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocation = TestBOPDMDBServerDataFileLocationFactory.getStandardBOPDMDBServerDataFileLocation(pkPDMDatabaseServer);

    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.newInstance();
    pdmDatabase.getFieldLocationRef().setAsBaseValue(pkPDMLocation);
    pdmDatabase.getFieldDatabaseServerRef().setAsBaseValue(pkPDMDatabaseServer);
    pdmDatabase.getFieldDumpFile().setStringValue(PDMTestFieldValues.DEFAULT_DIRECTORY_FOR_DUMP_FILES.concat(PDMTestFieldValues.MSSQL_TEST_DUMP_FILE));
    pdmDatabase.getFieldDatabaseType().setAsBaseValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeMssql());
    pdmDatabase.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeMSSQLDump());
    pdmDatabase.fillMandatoryFields();
    pdmDatabase.save();

    return pdmDatabase;
  }


  /**
   * create a fully completed PDM Database BO of type Oracle Datapump. After creating the necessary
   * BO's that are referenced by this very same BO.
   *
   * @return the fully completed PDM Database BO
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static TestBOPDMDatabase createStandardPDMDatabaseOfTypeOracleDatapump() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type Oracle
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionOracle = TestBOPDMDatabaseVersionOracleFactory.randomInstance();
    IBaseValue pkPDMDatabaseVersion = pdmDatabaseVersionOracle.getPrimaryKeyValue();

    TestBOPDMLocation pdmLocation = TestBOPDMLocationFactory.getStandardPDMLocation();
    IBaseValue pkPDMLocation = pdmLocation.getPrimaryKeyValue();

    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.getStandardOracleDatabaseServer(pkPDMDatabaseVersion, pkPDMLocation);
    IBaseValue pkPDMDatabaseServer = pdmDatabaseServerOracle.getPrimaryKeyValue();

    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocation = TestBOPDMDBServerDataFileLocationFactory.getStandardBOPDMDBServerDataFileLocation(pkPDMDatabaseServer);

    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.newInstance();
    pdmDatabase.getFieldLocationRef().setAsBaseValue(pkPDMLocation);
    pdmDatabase.getFieldDatabaseServerRef().setAsBaseValue(pkPDMDatabaseServer);
    pdmDatabase.getFieldDumpFile().setStringValue(PDMTestFieldValues.DEFAULT_DIRECTORY_FOR_DUMP_FILES.concat(PDMTestFieldValues.ORACLE_DATAPUMP_TEST_DUMP_FILE));
    pdmDatabase.getFieldDatabaseType().setAsBaseValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeOracle());
    pdmDatabase.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeDataPump());
    pdmDatabase.fillMandatoryFields();
    pdmDatabase.save();

    return pdmDatabase;
  }


  /**
   * create a fully completed PDM Database BO of type Oracle Old Style. After creating the necessary
   * BO's that are referenced by this very same BO.
   *
   * @return the fully completed PDM Database BO
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static TestBOPDMDatabase createStandardPDMDatabaseOfTypeOracleOldStyle() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type Oracle
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionOracle = TestBOPDMDatabaseVersionOracleFactory.randomInstance();
    IBaseValue pkPDMDatabaseVersion = pdmDatabaseVersionOracle.getPrimaryKeyValue();

    TestBOPDMLocation pdmLocation = TestBOPDMLocationFactory.getStandardPDMLocation();
    IBaseValue pkPDMLocation = pdmLocation.getPrimaryKeyValue();

    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.getStandardOracleDatabaseServer(pkPDMDatabaseVersion, pkPDMLocation);
    IBaseValue pkPDMDatabaseServer = pdmDatabaseServerOracle.getPrimaryKeyValue();

    TestBOPDMDBServerDataFileLocation pdmDBServerDataFileLocation = TestBOPDMDBServerDataFileLocationFactory.getStandardBOPDMDBServerDataFileLocation(pkPDMDatabaseServer);

    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.newInstance();
    pdmDatabase.getFieldLocationRef().setAsBaseValue(pkPDMLocation);
    pdmDatabase.getFieldDatabaseServerRef().setAsBaseValue(pkPDMDatabaseServer);
    pdmDatabase.getFieldDumpFile().setStringValue(PDMTestFieldValues.DEFAULT_DIRECTORY_FOR_DUMP_FILES.concat(PDMTestFieldValues.ORACLE_OLDSTYLE_TEST_DUMP_FILE));
    pdmDatabase.getFieldDatabaseType().setAsBaseValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeOracle());
    pdmDatabase.getFieldFileTypeRef().setAsBaseValue(TestBOPDMFileTypeFactory.getPDMFileTypeOracleOldExport());
    pdmDatabase.fillMandatoryFields();
    pdmDatabase.save();

    return pdmDatabase;
  }


  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * Creates a new empty unsaved TestBOPDMDatabase object.
   *
   * @return TestBOPDMDatabase
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabase newInstance() throws PnErrorListException, AuthorizationException
  {
    final IBOM addBOM = getBOMByPnName(BOTYPE, IBOPDMDatabaseDef.PN_BOM_ADD);

    final IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    final TestBOPDMDatabase testBO = new TestBOPDMDatabase(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMDatabase object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMDatabase
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMDatabase randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle testBOPDMDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    TestBOPDMDatabase testBO = newInstance();
    testBO.fillFields();
    testBO.getFieldDatabaseServerRef().setAsBaseValue(testBOPDMDbServer.getPrimaryKeyValue());
    testBO.getFieldLocationRef().setAsBaseValue(testBOPDMDbServer.getFieldPDMLocationRef().getBaseValue());
    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMDatabase object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField : IBOFieldValue FieldValue of primary TestBOPDMDatabase to be loaded
   *
   * @return TestBOPDMDatabase
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMDatabase readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMDatabase object containing data of specified primary key. Generates a
   * runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue : IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMDatabase
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMDatabase readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMDatabase testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new instance of TestBOPDMDatabase for testing purposes.
   *
   * @return TestBOPDMDatabase.
   *
   * @throws PnErrorListException
   */
  private static TestBOPDMDatabase createTestBOPDMDatabase() throws PnErrorListException
  {
    return new TestBOPDMDatabase();
  }


  /**
   * Returns a TestBOPDMDatabase object containing data aBOValue which is not destroyed after the
   * test.
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMDatabase the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMDatabase getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMDatabase testBO = new TestBOPDMDatabase(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
