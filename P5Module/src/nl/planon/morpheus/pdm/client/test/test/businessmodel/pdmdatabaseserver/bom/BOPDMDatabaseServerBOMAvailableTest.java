// Planon Enterprise Edition Source file: BOPDMDatabaseServerBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserver.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseservermssql.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMDatabaseServerBOMAvailableTest
 */
public class BOPDMDatabaseServerBOMAvailableTest extends BOPDMDatabaseServerMSSQLTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseServerBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test add bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableAddBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseServerDef.PN_BOM_ADD);
  }


  /**
   * Test copy bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableCopyBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseServerDef.PN_BOM_COPY);
  }


  /**
   * Test delete bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableDeleteBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseServerDef.PN_BOM_DELETE);
  }


  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseServerDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMDatabaseServerDef.PN_BOM_SAVE);
  }
}
