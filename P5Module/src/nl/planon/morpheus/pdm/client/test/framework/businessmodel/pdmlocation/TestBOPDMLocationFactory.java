// Planon Enterprise Edition Source file: TestBOPDMLocationFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * TestBOPDMLocationFactory
 */
public class TestBOPDMLocationFactory extends TestBOFactoryBase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_LOCATION;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return BOTYPE;
  }


  /**
   * create a PDM Location BO with a valid WebDAV location filled in
   *
   * @return a completed PDM Location BO
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static TestBOPDMLocation getStandardPDMLocation() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation pdmLocation = newInstance();
    pdmLocation.getFieldLocation().setStringValue(PDMTestFieldValues.PDMLOCATION_LOCATION_TEST_VALUE);
    pdmLocation.fillMandatoryFields();
    pdmLocation.save();
    return pdmLocation;
  }


  /**
   * Creates a new empty unsaved TestBOPDMLocation object.
   *
   * @return TestBOPDMLocation
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static final TestBOPDMLocation newInstance() throws PnErrorListException, AuthorizationException
  {
    final IBOM addBOM = getBOMByPnName(BOTYPE, IBOPDMLocationDef.PN_BOM_ADD);

    final IBOValue boValue = BOMResourceLocator.getInstance().execute(addBOM);
    final TestBOPDMLocation testBO = new TestBOPDMLocation(boValue);

    return testBO;
  }


  /**
   * Creates a new saved TestBOPDMLocation object containing random data. Errors result in an
   * ErrorListException
   *
   * @return TestBOPDMLocation
   *
   * @throws PnErrorListException   when the creation of this TestBO fails
   * @throws AuthorizationException
   */
  public static final TestBOPDMLocation randomInstance() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMLocation testBO = newInstance();
    testBO.fillFields();
    testBO.save();
    return testBO;
  }


  /**
   * Creates a new TestBOPDMLocation object containing data of specified primary key.
   *
   * @param  aPrimaryKeyField : IBOFieldValue FieldValue of primary TestBOPDMLocation to be loaded
   *
   * @return TestBOPDMLocation
   *
   * @throws PnErrorListException when the creation of this TestBO or the read fails
   */
  public static final TestBOPDMLocation readInstance(IBOFieldValue aPrimaryKeyField) throws PnErrorListException
  {
    return readInstance(aPrimaryKeyField.getBaseValue());
  }


  /**
   * Creates a new TestBOPDMLocation object containing data of specified primary key. Generates a
   * runtime exception if this fails
   *
   * @param  aPrimaryKeyBaseValue : IBaseValue FieldValue of primary key to be loaded
   *
   * @return TestBOPDMLocation
   *
   * @throws PnErrorListException
   */
  public static final TestBOPDMLocation readInstance(IBaseValue aPrimaryKeyBaseValue) throws PnErrorListException
  {
    IBOValue boValue = TestBO.readBOValue(BOTYPE, aPrimaryKeyBaseValue);
    TestBOPDMLocation testBO = getReadOnlyTestBO(boValue);
    return testBO;
  }


  /**
   * Creates a new instance of TestBOPDMLocation for testing purposes.
   *
   * @return TestBOPDMLocation
   *
   * @throws PnErrorListException
   */
  private static TestBOPDMLocation createTestBOPDMLocation() throws PnErrorListException
  {
    return new TestBOPDMLocation();
  }


  /**
   * Returns a TestBOPDMLocation object containing data aBOValue which is not destroyed after the
   * test.
   *
   * @param  aBOValue : the BOValue the value to be used
   *
   * @return TestBOPDMLocation the TestBO containing a readonly aBOValue
   *
   * @throws PnErrorListException
   */
  private static final TestBOPDMLocation getReadOnlyTestBO(IBOValue aBOValue) throws PnErrorListException
  {
    TestBOPDMLocation testBO = new TestBOPDMLocation(aBOValue);
    testBO.setAutomaticFree(false);
    return testBO;
  }
}
