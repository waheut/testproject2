// Planon Enterprise Edition Source file: BOPDMDBServerDataFileLocationBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMDBServerDataFileLocationBOMAvailableTest
 */
public class BOPDMDBServerDataFileLocationBOMAvailableTest extends BOPDMDBServerDataFileLocationTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDBServerDataFileLocationBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMDBServerDataFileLocationBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test add bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableAddBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDBServerDataFileLocationDef.PN_BOM_ADD);
  }


  /**
   * Test copy bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableCopyBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDBServerDataFileLocationDef.PN_BOM_COPY);
  }


  /**
   * Test delete database bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableDeleteBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDBServerDataFileLocationDef.PN_BOM_DELETE);
  }


  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDBServerDataFileLocationDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDBServerDataFileLocationDef.PN_BOM_SAVE);
  }
}
