// Planon Enterprise Edition Source file: BOPDMCompatibilityMatrixTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmcompatibilitymatrix;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;

/**
 * BOPDMCompatibilityMatrixTest
 */
public class BOPDMCompatibilityMatrixTest extends BOPDMCompatibilityMatrixTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMCompatibilityMatrixTest object.
   *
   * @param aName
   */
  public BOPDMCompatibilityMatrixTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO = TestBOPDMCompatibilityMatrixFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO = TestBOPDMCompatibilityMatrixFactory.newInstance();

    IBOFieldValue primaryKey = pdmCompatibilityMatrixTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO_2 = TestBOPDMCompatibilityMatrixFactory.readInstance(primaryKey);
      pdmCompatibilityMatrixTestBO_2.read();
      pdmCompatibilityMatrixTestBO_2.delete();

      pdmCompatibilityMatrixTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMCompatibilityMatrix
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO = TestBOPDMCompatibilityMatrixFactory.randomInstance();

    pdmCompatibilityMatrixTestBO.read();
    pdmCompatibilityMatrixTestBO.delete();
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO = TestBOPDMCompatibilityMatrixFactory.randomInstance();
    IBOFieldValue primaryKey = pdmCompatibilityMatrixTestBO.getPrimaryKeyField();

    pdmCompatibilityMatrixTestBO.read();
    pdmCompatibilityMatrixTestBO.delete();

    try
    {
      TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO_2 = TestBOPDMCompatibilityMatrixFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMCompatibilityMatrix.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO = TestBOPDMCompatibilityMatrixFactory.randomInstance();

    IBOFieldValue primaryKey = pdmCompatibilityMatrixTestBO.getPrimaryKeyField();
    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO_2 = TestBOPDMCompatibilityMatrixFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMCompatibilityMatrix.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMCompatibilityMatrix pdmCompatibilityMatrixTestBO = TestBOPDMCompatibilityMatrixFactory.randomInstance();

    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerTestBO = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionTestBO = TestBOPDMDatabaseVersionMSSQLFactory.randomInstance();

    pdmCompatibilityMatrixTestBO.getFieldDatabaseVersionRef().setAsBaseValue(pdmDatabaseVersionTestBO.getPrimaryKeyValue());
    pdmCompatibilityMatrixTestBO.getFieldDatabaseServerRef().setAsBaseValue(pdmDatabaseServerTestBO.getPrimaryKeyValue());
    pdmCompatibilityMatrixTestBO.save();
  }
}
