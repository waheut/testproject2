// Planon Enterprise Edition Source file: BOPDMDBServerDataFileLocationTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdbserverdatafilelocation;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdbserverdatafilelocation.*;

/**
 * BOPDMDBServerDataFileLocationTestCase
 */
public class BOPDMDBServerDataFileLocationTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDBServerDataFileLocationTestCase object.
   *
   * @param aName
   */
  public BOPDMDBServerDataFileLocationTestCase(String aName)
  {
    super(aName, TestBOPDMDBServerDataFileLocation.BOTYPE);
  }
}
