// Planon Enterprise Edition Source file: FCClearDbServerOfDifferenDbTypeTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.fieldchange;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.oracle.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;

/**
 * FCClearDBServerOfDifferenDBTypeTest
 */
public class FCClearDbServerOfDifferenDbTypeTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new FCClearDBServerOfDifferenDBTypeTest object.
   *
   * @param aName
   */
  public FCClearDbServerOfDifferenDbTypeTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <ul>
   *   <li>create PDM database version of type MSSQL</li>
   *   <li>create PDM database version of type ORACLE</li>
   *   <li>create PDM database server and link to version of type MSSQL</li>
   *   <li>create PDM database server and link to version of type ORACLE</li>
   *   <li>for PDM database set ServerRef to first added PDM database server</li>
   *   <li>change DatabaseTypeRef of PDM database to another value</li>
   *   <li>check if DatabaseServerRef is cleared</li>
   * </ul>
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testChangeDbTypeRefClearDbServerRef() throws PnErrorListException, AuthorizationException
  {
    // create PDM database version of type MSSQL
    TestBOPDMDatabaseVersionMSSQL pdmDatabaseVersionMSSQL = TestBOPDMDatabaseVersionMSSQLFactory.newInstance();
    pdmDatabaseVersionMSSQL.fillAllFields();
    pdmDatabaseVersionMSSQL.save();

    // create PDM database version of type ORACLE
    TestBOPDMDatabaseVersionOracle pdmDatabaseVersionOracle = TestBOPDMDatabaseVersionOracleFactory.newInstance();
    pdmDatabaseVersionOracle.fillAllFields();
    pdmDatabaseVersionOracle.save();

    // create PDM database server and link to version of type MSSQL
    TestBOPDMDatabaseServerMSSQL pdmDatabaseServerMSSQL = TestBOPDMDatabaseServerMSSQLFactory.randomInstance();
    pdmDatabaseServerMSSQL.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionMSSQL).fieldChange();
    pdmDatabaseServerMSSQL.fillAllFields();
    pdmDatabaseServerMSSQL.save();

    // create PDM database server and link to version of type ORACLE
    TestBOPDMDatabaseServerOracle pdmDatabaseServerOracle = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDatabaseServerOracle.getFieldDatabaseVersionRef().setValue(pdmDatabaseVersionOracle).fieldChange();
    pdmDatabaseServerOracle.fillAllFields();
    pdmDatabaseServerOracle.save();

    // for PDM database set ServerRef to first added PDM database server
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.getFieldDatabaseServerRef().setValue(pdmDatabaseServerMSSQL);
    pdmDatabase.fieldChange();

    // change DatabaseTypeRef of PDM database to another value
    pdmDatabase.getFieldDatabaseType().setValue(TestBOPDMDatabaseTypeFactory.getPDMDatabaseTypeOracle());
    pdmDatabase.fieldChange();

    // check if DatabaseServerRef is cleared
    pdmDatabase.getFieldDatabaseServerRef().assertEmpty();
  }
}
