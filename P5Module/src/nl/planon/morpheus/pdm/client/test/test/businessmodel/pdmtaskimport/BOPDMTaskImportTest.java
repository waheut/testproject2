// Planon Enterprise Edition Source file: BOPDMTaskImportTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskimport;

/**
 * BOPDMTaskImportTest
 */
public class BOPDMTaskImportTest extends BOPDMTaskImportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImportTest object.
   *
   * @param aName
   */
  public BOPDMTaskImportTest(String aName)
  {
    super(aName);
  }
}
