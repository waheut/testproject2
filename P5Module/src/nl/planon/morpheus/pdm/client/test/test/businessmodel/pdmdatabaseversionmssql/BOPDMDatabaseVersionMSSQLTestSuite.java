// Planon Enterprise Edition Source file: BOPDMDatabaseVersionMSSQLTestSuite.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversionmssql;

import junit.framework.*;

import nl.planon.junit.framework.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseversion.bom.*;

/**
 * BOPDMDatabaseVersionMSSQLTestSuite
 */
public class BOPDMDatabaseVersionMSSQLTestSuite extends PlanonTestSuite
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionTestSuite object.
   */
  public BOPDMDatabaseVersionMSSQLTestSuite()
  {
    // bom tests
    addTestSuite(BOPDMDatabaseVersionBOMAvailableTest.class);

    // rules tests

    // generic test
    addTestSuite(BOPDMDatabaseVersionMSSQLTest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionTestSuite object.
   *
   * @return BOPDMDatabaseVersionTestSuite
   */
  public static Test suite()
  {
    return new BOPDMDatabaseVersionMSSQLTestSuite();
  }
}
