// Planon Enterprise Edition Source file: BOPDMTaskImportBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskimport.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmtaskimport.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMTaskImportBOMAvailableTest
 */
public class BOPDMTaskImportBOMAvailableTest extends BOPDMTaskImportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImportBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMTaskImportBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMTaskDef.PN_BOM_READ);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOBasePDMTaskDef.PN_BOM_SAVE);
  }
}
