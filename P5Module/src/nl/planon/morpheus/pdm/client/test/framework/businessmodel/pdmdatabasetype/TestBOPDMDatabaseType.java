// Planon Enterprise Edition Source file: TestBOPDMDatabaseType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabasetype;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;

/**
 * TestBOPDMDatabaseType
 */
public class TestBOPDMDatabaseType extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = BOTypePDMP5Module.PDM_DATABASE_TYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseType object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseType() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMDatabaseType object.
   *
   * @param  aBOValue The BOValue to initialize this testBO with
   *
   * @throws PnErrorListException
   */
  public TestBOPDMDatabaseType(IBOValue aBOValue) throws PnErrorListException
  {
    super(BOTYPE, aBOValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the code field.
   *
   * @return the code field.
   */
  public TestBOFieldValue getFieldCode()
  {
    return getFieldByPnName(BOPDMDatabaseTypeDef.PN_CODE);
  }


  /**
   * Returns the name field.
   *
   * @return the name field.
   */
  public TestBOFieldValue getFieldName()
  {
    return getFieldByPnName(BOPDMDatabaseTypeDef.PN_NAME);
  }
}
