// Planon Enterprise Edition Source file: TestBOPDMLocation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation;

import nl.planon.dionysus.framework.common.*;
import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * TestBOPDMLocation
 */
public class TestBOPDMLocation extends TestBO
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BOTYPE = TestBOPDMLocationFactory.BOTYPE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMLocation object.
   *
   * @throws PnErrorListException
   */
  public TestBOPDMLocation() throws PnErrorListException
  {
    super(BOTYPE);
  }


  /**
   * Creates a new TestBOPDMLocation object.
   *
   * @param  aValue
   *
   * @throws PnErrorListException
   */
  public TestBOPDMLocation(IBOValue aValue) throws PnErrorListException
  {
    super(BOTYPE, aValue);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillDefaultFields() throws PnErrorListException, AuthorizationException
  {
    getFieldLocation().setStringValue(PDMTestFieldValues.PDMLOCATION_LOCATION_TEST_VALUE);
  }


  /**
   * convenience method for field PN_CODE
   *
   * @return field PN_ACCESSCODE
   */
  public TestBOFieldValue getFieldCode()
  {
    return getFieldByPnName(IBOPDMLocationDef.PN_CODE);
  }


  /**
   * convenience method for field PN_LOCATION
   *
   * @return field PN_LOCATION
   */
  public TestBOFieldValue getFieldLocation()
  {
    return getFieldByPnName(IBOPDMLocationDef.PN_LOCATION);
  }


  /**
   * convenience method for field PN_NAME
   *
   * @return field PN_NAME
   */
  public TestBOFieldValue getFieldName()
  {
    return getFieldByPnName(IBOPDMLocationDef.PN_NAME);
  }
}
