// Planon Enterprise Edition Source file: BOPDMDatabaseTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;

/**
 * BOPDMDatabaseTestCase
 */
public class BOPDMDatabaseTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseTestCase object.
   *
   * @param aName
   */
  public BOPDMDatabaseTestCase(String aName)
  {
    super(aName, TestBOPDMDatabase.BOTYPE);
  }
}
