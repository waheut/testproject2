// Planon Enterprise Edition Source file: BOPDMDatabaseBOMAvailableTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMDatabaseBOMAvailableTest
 */
public class BOPDMDatabaseBOMAvailableTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseBOMAvailableTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseBOMAvailableTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test add bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableAddBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_ADD);
  }


  /**
   * Test copy bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableCopyBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_COPY);
  }


  /**
   * Test delete bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableDeleteBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_DELETE);
  }


  /**
   * Test delete database bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableDeleteDatabaseBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_DELETE_DATABASE);
  }


  /**
   * Test download dumpfile bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableDownloadDumpfileBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_DOWNLOAD_DUMPFILE);
  }


  /**
   * Test export database bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableExportDatabaseBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_EXPORT_DATABASE);
  }


  /**
   * Test import database bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableImportDatabaseBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE);
  }


  /**
   * Test read bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReadBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_READ);
  }


  /**
   * Test new database import bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReloadExportedDatabaseBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_RELOAD_EXPORTED_DATABASE);
  }


  /**
   * Test reload database bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableReloadInitialDatabaseBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_RELOAD_INITIAL_DATABASE);
  }


  /**
   * Test save bom
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testAvailableSaveBOMOK() throws PnErrorListException, AuthorizationException
  {
    checkBomAvailable(IBOPDMDatabaseDef.PN_BOM_SAVE);
  }
}
