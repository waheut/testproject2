// Planon Enterprise Edition Source file: BOPDMLocationTestCase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmlocation;

import nl.planon.facilityoffice.dionysus.client.test.common.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;

/**
 * BOPDMLocationTestCase
 */
public class BOPDMLocationTestCase extends BOTestCaseFacilityOffice
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMLocationTestCase object.
   *
   * @param aName
   */
  public BOPDMLocationTestCase(String aName)
  {
    super(aName, TestBOPDMLocation.BOTYPE);
  }
}
