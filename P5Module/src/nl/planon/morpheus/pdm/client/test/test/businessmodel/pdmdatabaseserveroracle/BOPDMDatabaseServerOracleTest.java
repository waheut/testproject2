// Planon Enterprise Edition Source file: BOPDMDatabaseServerOracleTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserveroracle;

import nl.planon.dionysus.framework.field.*;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * BOPDMDatabaseServerTest
 */
public class BOPDMDatabaseServerOracleTest extends BOPDMDatabaseServerOracleTestCase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String[] DATABASE_SERVER_SETTING_FIELDS = new String[]
  { //
    BOBasePDMDatabaseServerDef.PN_ADMIN_USER, //
    BOBasePDMDatabaseServerDef.PN_ADMIN_PASS, //
    BOBasePDMDatabaseServerDef.PN_HOST_NAME, //
    BOBasePDMDatabaseServerDef.PN_DATABASE_INSTANCENAME, //
    BOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF, //
    BOBasePDMDatabaseServerDef.PN_CONNECT_TNS_NAME__NOT_IN_ALL_SUBTYPES, //
    BOBasePDMDatabaseServerDef.PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES, //
    BOBasePDMDatabaseServerDef.PN_TEMPORARY_TABLESPACENAME__NOT_IN_ALL_SUBTYPES //
  };

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerOracleTest object.
   *
   * @param aName
   */
  public BOPDMDatabaseServerOracleTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creation of BOPDMDatabaseServerTest
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testCreateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO = TestBOPDMDatabaseServerOracleFactory.newInstance();
  }


  /**
   * Tests that a record cannot be deleted that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO = TestBOPDMDatabaseServerOracleFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseServerTestBO.getPrimaryKeyField();
    try
    {
      TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO_2 = TestBOPDMDatabaseServerOracleFactory.readInstance(primaryKey);
      pdmDatabaseServerTestBO_2.read();
      pdmDatabaseServerTestBO_2.delete();

      pdmDatabaseServerTestBO.delete();
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test delete of TestBOPDMDatabaseServer
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDeleteOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO = TestBOPDMDatabaseServerOracleFactory.randomInstance();

    pdmDatabaseServerTestBO.read();
    pdmDatabaseServerTestBO.delete();
  }


  /**
   * Checks whether all the fields that hold specific database server settings are read-only when
   * the database server is active (field IsActive = true)
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testIfDatabaseServerActiveThenSettingFieldsReadOnlyOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDbServer.getFieldIsActive().setBooleanValue(true);
    pdmDbServer.save();

    for (String fieldName : DATABASE_SERVER_SETTING_FIELDS)
    {
      TestBOFieldValue field = pdmDbServer.getFieldByPnName(fieldName);
      assertTrue(field.isReadonly());
    }
  }


  /**
   * Checks whether all the fields that hold specific database server settings are not read-only
   * when the database server is inactive (field IsActive = false)
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testIfDatabaseServerInActiveSettingFieldsNotReadOnlyOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    pdmDbServer.getFieldIsActive().setBooleanValue(false);
    pdmDbServer.save();

    for (String fieldName : DATABASE_SERVER_SETTING_FIELDS)
    {
      TestBOFieldValue field = pdmDbServer.getFieldByPnName(fieldName);
      assertFalse(field.isReadonly());
    }
  }


  /**
   * Tests that a record cannot be read that is not available.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    IBOFieldValue primaryKey = pdmDatabaseServerTestBO.getPrimaryKeyField();

    pdmDatabaseServerTestBO.read();
    pdmDatabaseServerTestBO.delete();

    try
    {
      TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO_2 = TestBOPDMDatabaseServerOracleFactory.readInstance(primaryKey);
      failMessageTypeExpected(ErrorNumberHades.EC_FINDEREXCEPTION, "instance already deleted");
    }
    catch (PnErrorListException expected)
    {
      assertContainsMessageType(expected, ErrorNumberHades.EC_FINDEREXCEPTION);
    }
  }


  /**
   * Test read of TestBOPDMDatabaseServer.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testReadOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO = TestBOPDMDatabaseServerOracleFactory.randomInstance();

    IBOFieldValue primaryKey = pdmDatabaseServerTestBO.getPrimaryKeyField();
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO_2 = TestBOPDMDatabaseServerOracleFactory.readInstance(primaryKey);
  }


  /**
   * Test update of TestBOPDMDatabaseServer.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testUpdateOk() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDatabaseServerTestBO = TestBOPDMDatabaseServerOracleFactory.randomInstance();

    pdmDatabaseServerTestBO.getFieldCode().setValueRandom();
    pdmDatabaseServerTestBO.save();
  }
}
