// Planon Enterprise Edition Source file: PDMRequestsTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.request;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * PDMRequestsTest
 */
public class PDMRequestsTest extends BOPDMDatabaseTestCase
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  protected static final long MINUTES_TO_WAIT_FOR_REQUEST_TO_BE_PICKED_UP = 1;
  protected static final long MAX_MINUTES_TO_WAIT_FOR_IMPORT_REQUEST_TO_FINISH = 30; // import task
  protected static final long MAX_MINUTES_TO_WAIT_FOR_EXPORT_REQUEST_TO_FINISH = 30; // export task
  protected static final long MAX_MINUTES_TO_WAIT_FOR_CLEANUP_REQUEST_TO_FINISH = 30; // delete task
  protected static final long MAX_MINUTES_TO_WAIT_FOR_RELOAD_EXPORT_REQUEST_TO_FINISH = 60; //delete task + import task
  protected static final long MAX_MINUTES_TO_WAIT_FOR_RELOAD_INITIAL_REQUEST_TO_FINISH = 60; //delete task + import task

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestsTest object.
   *
   * @param aName
   */
  public PDMRequestsTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * check if the PDM Database, the request and the task are in an expected state after a failed
   * request. After a failed request the state of both the request and the task is expected to be
   * NotOk. The expected state of the database is dependent on the type of request.
   *
   * @param  aPDMDatabase           the PDM database BO to which the request applies to
   * @param  aExpectedDatabaseState the expected state of the database
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  protected void checkStatesForFinishedFailedRequest(TestBOPDMDatabase aPDMDatabase, String aExpectedDatabaseState) throws AuthorizationException, PnErrorListException
  {
    aPDMDatabase.read();
    TestBOBasePDMRequest pdmrequest = aPDMDatabase.getLastAddedRequest();
    TestBOBasePDMTask pdmtask = aPDMDatabase.getLastAddedTask();

    assertEquals(aExpectedDatabaseState, aPDMDatabase.getSystemStateName());
    assertEquals(IBOBasePDMRequestDef.STATE_NOK, pdmrequest.getSystemStateName());
    assertEquals(IBOBasePDMTaskDef.STATE_NOK, pdmtask.getSystemStateName());
  }


  /**
   * check if the PDM Database, the request and the task are in an expected state after a successful
   * request. After a successful request the state of both the request and the task is expected to
   * be Ok. The expected state of the database is dependent on the type of request.
   *
   * @param  aPDMDatabase           the PDM database BO to which the request applies to
   * @param  aExpectedDatabaseState the expected state of the database
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  protected void checkStatesForFinishedSuccessfulRequest(TestBOPDMDatabase aPDMDatabase, String aExpectedDatabaseState) throws AuthorizationException, PnErrorListException
  {
    aPDMDatabase.read();
    TestBOBasePDMRequest pdmrequest = aPDMDatabase.getLastAddedRequest();
    TestBOBasePDMTask pdmtask = aPDMDatabase.getLastAddedTask();

    assertEquals(aExpectedDatabaseState, aPDMDatabase.getSystemStateName());
    assertEquals(IBOBasePDMRequestDef.STATE_OK, pdmrequest.getSystemStateName());
    assertEquals(IBOBasePDMTaskDef.STATE_OK, pdmtask.getSystemStateName());
  }


  /**
   * check if the PDM request and the PDM task are in an expected state after a request is picked
   * up. The state of both the request and the task is expected to have passed the Initial state.
   *
   * @param  aPDMDatabase the PDM database BO to which the request applies to
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  protected void checkStatesForPickedUpRequest(TestBOPDMDatabase aPDMDatabase) throws AuthorizationException, PnErrorListException
  {
    aPDMDatabase.read();
    TestBOBasePDMRequest pdmrequest = aPDMDatabase.getLastAddedRequest();
    TestBOBasePDMTask pdmtask = aPDMDatabase.getLastAddedTask();

    assertFalse("The request is not picked up", IBOBasePDMRequestDef.STATE_INITIAL.equals(pdmrequest.getSystemStateName()));
    assertFalse("The task is not picked up", IBOBasePDMTaskDef.STATE_INITIAL.equals(pdmtask.getSystemStateName()));
  }


  /**
   * force a sleeping time
   *
   * @param aMinutes number of minutes to sleep
   */
  protected void sleepingTime(long aMinutes)
  {
    try
    {
      //Thread.sleep(aMinutes * 60 * 1000);
      Thread.sleep(40000);
    }
    catch (InterruptedException ex)
    {
      fail(ex);
    }
  }


  /**
   * this method waits for a request to finish. For a certain amount of time (aMaxWaitingTime) it
   * checks every 5 minutes if there has been a state change in either the database or the request
   * or the task that are at the basis of this request.
   *
   * @param  aPDMDatabase    database for which the request is executed
   * @param  aMaxWaitingTime maximum time to wait for the request to finish
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected void waitForRequestToFinish(TestBOPDMDatabase aPDMDatabase, long aMaxWaitingTime) throws PnErrorListException, AuthorizationException
  {
    long endTimeOfCheck = System.currentTimeMillis() + (aMaxWaitingTime * 60 * 1000);

    boolean requestInProgress = true;
    while (requestInProgress && (System.currentTimeMillis() <= endTimeOfCheck))
    {
      aPDMDatabase.read();
      // if database still in state busy, check request and task if there has been a state change
      if (aPDMDatabase.getSystemStateName().equals(IBOPDMDatabaseDef.STATE_BUSY))
      {
        TestBOBasePDMRequest pdmrequest = aPDMDatabase.getLastAddedRequest();
        TestBOBasePDMTask pdmtask = aPDMDatabase.getLastAddedTask();
        // request is finished if either the request or the task is no longer in progress
        requestInProgress = ((pdmrequest.getSystemStateName().equals(IBOBasePDMRequestDef.STATE_IN_PROGRESS)) //
          && (pdmtask.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_IN_PROGRESS)));
      }
      else
      {
        // database is no longer in busy status, hence request has finished
        requestInProgress = false;
      }
      // wait for 1 minute before repeating the check
      sleepingTime(1);
    }
  }
}
