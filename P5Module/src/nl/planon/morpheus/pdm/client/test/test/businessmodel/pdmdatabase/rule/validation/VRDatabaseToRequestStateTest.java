// Planon Enterprise Edition Source file: VRDatabaseToRequestStateTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.rule.validation;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmtask.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * Testcase to test (im)possible state changes on database level, which is dependent on the state of
 * the last added request.
 */
public class VRDatabaseToRequestStateTest extends BOPDMDatabaseTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new VRDatabaseToRequestState object.
   *
   * @param aName
   */
  public VRDatabaseToRequestStateTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that it is not possible to put a database to status Available, when the last added request
   * is still in state InProgress
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDatabaseAvailableRequestInProgress() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);
    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it to Available. not possible!
    try
    {
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      if (request.getSystemStateName().equals(IBOBasePDMRequestDef.STATE_INITIAL))
      {
        request.setStateAndSave(IBOBasePDMRequestDef.STATE_IN_PROGRESS);
      }
      pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_AVAILABLE);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DATABASE_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_DATABASE_STATE_IS_NOT_ALLOWED);
    }
  }


  /**
   * Test that it is not possible to put a database to status Initial, when the last added request
   * is still in state InProgress
   *
   * @throws IllegalBOMStateChange
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testDatabaseInitialRequestInProgress() throws IllegalBOMStateChange, PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabase pdmDatabase = TestBOPDMDatabaseFactory.randomInstance();
    pdmDatabase.executeImportPDMDatabaseBOM();
    TestBOBasePDMRequest request = pdmDatabase.getRequests(1).get(0);
    TestBOBasePDMTask task = request.getTasks(1).get(0);

// try to set it back to initial. not possible!
    try
    {
      if (task.getSystemStateName().equals(IBOBasePDMTaskDef.STATE_INITIAL))
      {
        task.setStateAndSave(IBOBasePDMTaskDef.STATE_IN_PROGRESS);
      }
      if (request.getSystemStateName().equals(IBOBasePDMRequestDef.STATE_INITIAL))
      {
        request.setStateAndSave(IBOBasePDMRequestDef.STATE_IN_PROGRESS);
      }
      pdmDatabase.setStateAndSave(IBOPDMDatabaseDef.STATE_INITIAL);
      failMessageTypeExpected(ErrorNumberPDMP5Module.EC_DATABASE_STATE_IS_NOT_ALLOWED);
    }
    catch (PnErrorListException e)
    {
      assertContainsMessageType(e, ErrorNumberPDMP5Module.EC_DATABASE_STATE_IS_NOT_ALLOWED);
    }
  }
}
