// Planon Enterprise Edition Source file: TestBOPDMDatabaseVersionMSSQLReference.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseversion.mssql;

import nl.planon.dionysus.framework.field.*;

/**
 * TestBOPDMDatabaseVersionMSSQLReference
 */
public class TestBOPDMDatabaseVersionMSSQLReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseVersionReference object.
   */
  public TestBOPDMDatabaseVersionMSSQLReference()
  {
    super(TestBOPDMDatabaseVersionMSSQLFactory.BOTYPE);
  }
}
