// Planon Enterprise Edition Source file: BOMLogRequestImportTest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport.bom;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmrequestimport.*;

/**
 * BOMLogRequestImportTest
 */
public class BOMLogRequestImportTest extends BOPDMRequestImportTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMLogRequestImportTest object.
   *
   * @param aName
   */
  public BOMLogRequestImportTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test import bom.
   *
   * <ul>
   *   <li>create PDM request</li>
   *   <li>execute the log PDM request BOM</li>
   *   <li>check if the log is added</li>
   * </ul>
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testBOMLogImportRequestOk() throws PnErrorListException, AuthorizationException
  {
    // TestBOPDMDatabaseVersion pdmDatabaseVersion =
    // TestBOPDMDatabaseVersionFactory.randomInstance();
    //
    // TestBOPDMDatabaseServer pdmDatabaseServer =
    // TestBOPDMDatabaseServerFactory.randomInstance();
    // pdmDatabaseServer.getFieldDatabaseVersionRef().setAsBaseValue(pdmDatabaseVersion.getPrimaryKeyValue());
    // pdmDatabaseServer.fillAllFields();
    // pdmDatabaseServer.clearDbTypeSpecificFields(pdmDatabaseVersion.getFieldDatabaseTypeRef().getLookupValue().getAsString());
    // pdmDatabaseServer.save();

    TestBOPDMRequestImport pdmRequestImport = TestBOPDMRequestImportFactory.randomInstance();
    pdmRequestImport.executeLogPDMRequestBOM();
  }
}
