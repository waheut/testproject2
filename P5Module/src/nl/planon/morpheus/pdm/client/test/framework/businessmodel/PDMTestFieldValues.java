// Planon Enterprise Edition Source file: PDMTestFieldValues.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel;

/**
 * PDMTestFieldValues
 */
public class PDMTestFieldValues
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Absolute path and URL to WebDAV location
  public static final String ABSOLUTE_WEBDAV_LOCATION = "D:\\EE\\201012\\workspace\\Planon5\\Appserver\\tomcat-5.5\\webapps\\webdav\\";
  public static final String PDMLOCATION_LOCATION_TEST_VALUE = "http://localhost:8070/webdav/";
  // Configuration for 'standard' Oracle Database Server (TestBOPDMDatabaseServerOracleFactory.getStandardOracleDatabaseServer)
  public static final String PDMDBSERVER_ORACLE_CODE_TEST_VALUE = "NL-DEVS20";
  public static final String PDMDBSERVER_ORACLE_ADMINUSER_TEST_VALUE = "system";
  public static final String PDMDBSERVER_ORACLE_ADMINPASS_TEST_VALUE = "Welcome02";
  public static final String PDMDBSERVER_ORACLE_HOSTNAME_TEST_VALUE = "localhost";
  public static final String PDMDBSERVER_ORACLE_DBINSTANCENAME_TEST_VALUE = "PE";
  public static final int PDMDATABASESERVER_PORTNUMBER_TEST_VALUE = 1521;
  public static final String PDMDBSERVER_ORACLE_EXP_LOCATION_TEST_VALUE = "C:\\OraclePE\\product\\11.2.0\\dbhome_2\\BIN\\exp.exe";
  public static final String PDMDBSERVER_ORACLE_IMP_LOCATION_TEST_VALUE = "C:\\OraclePE\\product\\11.2.0\\dbhome_2\\BIN\\imp.exe";
  // Configuration for 'standard' MSSQL Database Server (TestBOPDMDatabaseServerMSSQLFactory.getStandardMSSQLDatabaseServer)
  public static final String PDMDBSERVER_MSSQL_CODE_TEST_VALUE = "NL-DEVS21";
  public static final String PDMDBSERVER_MSSQL_ADMINUSER_TEST_VALUE = "plandba";
  public static final String PDMDBSERVER_MSSQL_ADMINPASS_TEST_VALUE = "plansql";
  public static final String PDMDBSERVER_MSSQL_CODEPAGE_TEST_VALUE = "1";
  public static final String PDMDBSERVER_MSSQL_CONNECTSERVERNAME_TEST_VALUE = "jdbc:sqlserver://localhost";
  public static final String PDMDBSERVER_MSSQL_HOSTNAME_TEST_VALUE = "localhost";
  // Information about dumps and their original location
  public static final String DEFAULT_DIRECTORY_FOR_DUMP_FILES = "C:\\temp\\PDM3\\Test\\";
  public static final String MSSQL_TEST_DUMP_FILE = "MSSQLDump.bak";
  public static final String ORACLE_OLDSTYLE_TEST_DUMP_FILE = "OracleOldStyle.dmp";
  public static final String ORACLE_DATAPUMP_TEST_DUMP_FILE = "OracleDataPump.dpd";

  public static final String PDMDBSERVER_DATAFILE_LOCATION_LOCATIONLOCAL = "C:\\Temp\\DatapumpFiles";
}
