// Planon Enterprise Edition Source file: TestBOPDMDatabaseServerMSSQLReference.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.mssql;

import nl.planon.dionysus.framework.field.*;

/**
 * TestBOPDMDatabaseServerReference
 */
public class TestBOPDMDatabaseServerMSSQLReference extends FieldDataReference
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMDatabaseServerReference object.
   */
  public TestBOPDMDatabaseServerMSSQLReference()
  {
    super(TestBOPDMDatabaseServerMSSQLFactory.BOTYPE);
  }
}
