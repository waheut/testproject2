// Planon Enterprise Edition Source file: ARUpdatePDMLocationLinkedDatabasesTest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserver.rule;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmdatabaseserver.oracle.*;
import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.client.test.test.businessmodel.pdmdatabaseserveroracle.*;

/**
 * ARUpdatePDMLocationLinkedDatabasesTest
 */
public class ARUpdatePDMLocationLinkedDatabasesTest extends BOPDMDatabaseServerOracleTestCase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ARUpdatePDMLocationLinkedDatabasesTest object.
   *
   * @param aName
   */
  public ARUpdatePDMLocationLinkedDatabasesTest(String aName)
  {
    super(aName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Test that if field PDM Location of the PDM Database Server is changed, the field PDM location
   * of the linked PDM databases is changed to the same value
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void testModifyFieldPDMLocationFail() throws PnErrorListException, AuthorizationException
  {
    TestBOPDMDatabaseServerOracle pdmDbServer = TestBOPDMDatabaseServerOracleFactory.randomInstance();
    // Create two databases, will be in state initial
    TestBOPDMDatabase pdmDatabaseOne = TestBOPDMDatabaseFactory.randomInstance();
    TestBOPDMDatabase pdmDatabaseTwo = TestBOPDMDatabaseFactory.randomInstance();
    // Link both databases to the database server
    try
    {
      pdmDatabaseOne.getFieldDatabaseServerRef().setAsBaseValue(pdmDbServer.getPrimaryKeyValue());
      pdmDatabaseOne.getFieldLocationRef().setValue(pdmDbServer.getFieldPDMLocationRef());
      pdmDatabaseOne.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
    try
    {
      pdmDatabaseTwo.getFieldDatabaseServerRef().setAsBaseValue(pdmDbServer.getPrimaryKeyValue());
      pdmDatabaseTwo.getFieldLocationRef().setValue(pdmDbServer.getFieldPDMLocationRef());
      pdmDatabaseTwo.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
    // get a new location
    TestBOPDMLocation pdmLocation = TestBOPDMLocationFactory.randomInstance();

    // change PDM location field of database server to new location
    try
    {
      pdmDbServer.getFieldPDMLocationRef().setAsBaseValue(pdmLocation.getPrimaryKeyValue());
      pdmDbServer.save();
    }
    catch (PnErrorListException ex)
    {
      fail(ex);
    }
    // refresh databases
    pdmDatabaseOne.read();
    pdmDatabaseTwo.read();
    // check if the PDM locations of the databases is changed and equal to the
    // location specified in the database server.
    assertEquals(pdmDatabaseOne.getFieldLocationRef().getBaseValue(), pdmDbServer.getFieldPDMLocationRef().getBaseValue());
    assertEquals(pdmDatabaseTwo.getFieldLocationRef().getBaseValue(), pdmDbServer.getFieldPDMLocationRef().getBaseValue());
  }
}
