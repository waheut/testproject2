// Planon Enterprise Edition Source file: TestBOPDMRequestDeleteFactory.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestdelete;

import nl.planon.dionysus.framework.common.*;

import nl.planon.hades.businessmodel.*;

import nl.planon.morpheus.pdm.client.test.framework.businessmodel.pdmrequest.pdmrequestimport.*;

/**
 * TestBOPDMRequestDeleteFactory
 */
public class TestBOPDMRequestDeleteFactory extends TestBOFactoryBase
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new TestBOPDMRequestDeleteFactory object.
   */
  private TestBOPDMRequestDeleteFactory()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Gets the BOType of this factory.
   *
   * @return IBOType
   */
  public static final IBOType getBOType()
  {
    return TestBOPDMRequestImport.BOTYPE;
  }
}
