// Planon Enterprise Edition Source file: SwingActivator.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.swing.osgi;

import nl.planon.ares.osgi.integration.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * SwingActivator
 */
public class SwingActivator extends SwingBundleActivator
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void start()
  {
    registerService(ModulePDMP5Module.NAME, new SwingClientBundleServicePDMP5Module(), null, null, null, null);
  }
}
