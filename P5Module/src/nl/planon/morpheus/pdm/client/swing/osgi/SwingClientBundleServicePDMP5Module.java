// Planon Enterprise Edition Source file: SwingClientBundleServicePDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.swing.osgi;

import java.util.*;

import nl.planon.ares.resourcelocators.*;

import nl.planon.hades.module.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.client.common.factory.*;
import nl.planon.morpheus.pdm.client.common.image.*;
import nl.planon.morpheus.pdm.client.swing.factory.*;

import nl.planon.zeus.actionpanel.*;
import nl.planon.zeus.dialogs.*;
import nl.planon.zeus.editorregister.*;
import nl.planon.zeus.editors.fieldvalueeditors.*;
import nl.planon.zeus.graphicalcomponents.pndialog.*;
import nl.planon.zeus.methods.*;
import nl.planon.zeus.osgi.integration.*;

/**
 * SwingClientBundleServicePDMP5Module
 */
public class SwingClientBundleServicePDMP5Module extends BaseClientBundleService
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public Map<IExecuterKey, IExecuterLoader> getBOMExecuters()
  {
    return BOMExecuterFactoryPDMP5Module.getBOMExecuters();
  }


  /**
   * {@inheritDoc}
   */
  @Override public List<IStepMethodLoader> getCommonStepMethods()
  {
    return SwingStepMethodFactoryPDMP5Module.getCommonStepMethods();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<Class<? extends PnBaseDialogController>, IDialogLoader> getDialogs()
  {
    return SwingDialogFactoryPDMP5Module.getDialogs();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<IFieldValueEditorKey, IFieldValueEditorLoader> getFieldValueEditors()
  {
    return FieldValueEditorFactoryPDMP5Module.getFieldValueEditors();
  }


  /**
   * {@inheritDoc}
   */
  @Override public BaseImageResourceLocator getImageResourceLocator()
  {
    return PDMP5ModuleImageResourceLocator.getInstance();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<StepFieldTypeBoTypePnNameKey, IPVType> getPopupPVTypes()
  {
    return PopupSearchCriteriaFactoryPDMP5Module.getPopupPVTypes();
  }


  /**
   * {@inheritDoc}
   */
  @Override public final Map<FieldTypeBoTypePnNameKey, IDialogControllerLoader> getReferenceDialogControllers()
  {
    return ReferenceDialogControllerFactoryPDMP5Module.getReferenceDialogControllers();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<IDrilldownStepType, List<IStepMethodLoader>> getStepMethods()
  {
    return SwingStepMethodFactoryPDMP5Module.getStepMethods();
  }
}
