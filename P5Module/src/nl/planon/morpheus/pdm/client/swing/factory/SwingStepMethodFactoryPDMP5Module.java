// Planon Enterprise Edition Source file: SwingStepMethodFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.swing.factory;

import java.util.*;

import nl.planon.hades.module.*;

import nl.planon.zeus.methods.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * SwingStepMethodFactoryPDMP5Module
 */
public class SwingStepMethodFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new factory instance that contains a registry. This registry is filled with
   * registrations in this method. The return value will be a list which contains <b>common</b> step
   * methods that will be registered
   *
   * @return list with all <b>common</b> step methods
   */
  public static List<IStepMethodLoader> getCommonStepMethods()
  {
    SwingStepMethodFactory factory = new SwingStepMethodFactory();

    // factory.registerCommonStepMethod(ExampleStepMethod.class);

    return factory.getCommonStepMethods();
  }


  /**
   * Creates a new factory instance that contains a registry. This registry is filled with
   * registrations in this method. The return value will be a map which contains steps for which one
   * or more steps methods are registered
   *
   * @return map with all step methods per drill down step
   */
  public static Map<IDrilldownStepType, List<IStepMethodLoader>> getStepMethods()
  {
    SwingStepMethodFactory factory = new SwingStepMethodFactory();

    // factory.registerStepMethod(ModulePDMP5Module.EXAMPLE_STEP,
    // ExampleStepMethod.class);

    return factory.getStepMethods();
  }
}
