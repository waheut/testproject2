// Planon Enterprise Edition Source file: SwingDialogFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.swing.factory;

import java.util.*;

import nl.planon.zeus.graphicalcomponents.pndialog.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * SwingDialogFactoryPDMP5Module
 */
public class SwingDialogFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns loaders for the dialogs registered by this module.
   *
   * @return A map of loaders keyed on dialog controller.
   */
  public static final Map<Class<? extends PnBaseDialogController>, IDialogLoader> getDialogs()
  {
    SwingDialogFactory dialogFactory = new SwingDialogFactory();
    return dialogFactory.getDialogs();
  }
}
