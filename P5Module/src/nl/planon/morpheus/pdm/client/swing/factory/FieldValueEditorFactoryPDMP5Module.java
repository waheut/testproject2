// Planon Enterprise Edition Source file: FieldValueEditorFactoryPDMP5Module.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.client.swing.factory;

import java.util.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.zeus.editors.fieldvalueeditors.*;
import nl.planon.zeus.editors.fieldvalueeditors.editortypes.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * FieldValueEditorFactoryPDMP5Module
 */
public class FieldValueEditorFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Register your field value editors here for this bundle
   *
   * @return Map<IFieldValueEditorKey, IFieldValueEditorLoader>
   */
  public static Map<IFieldValueEditorKey, IFieldValueEditorLoader> getFieldValueEditors()
  {
    FieldValueEditorRegisterFactory factory = new FieldValueEditorRegisterFactory();
    //Registed swing client field value editors
    factory.registerEditor(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS, PasswordFieldValueEditor.class);
    factory.registerEditor(BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL, IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS, PasswordFieldValueEditor.class);
    factory.registerEditor(BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE, IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS, PasswordFieldValueEditor.class);

    return factory.getFieldValueEditors();
  }
}
