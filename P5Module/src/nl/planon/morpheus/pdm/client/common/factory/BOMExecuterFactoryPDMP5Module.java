// Planon Enterprise Edition Source file: BOMExecuterFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.common.factory;

import java.util.*;

import nl.planon.morpheus.pdm.client.common.pdmdatabase.executer.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.zeus.actionpanel.*;
import nl.planon.zeus.actionpanel.executers.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * BOMExecuterFactoryPDMP5Module
 */
public class BOMExecuterFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns loaders for the BOM executers registered by this module.
   *
   * @return A map of loaders keyed on executer key.
   */
  public static final Map<IExecuterKey, IExecuterLoader> getBOMExecuters()
  {
    BOMExecuterFactory factory = new BOMExecuterFactory();

    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_BOM_SAVE, PnUploadPDMDumpFileExecuter.class);
    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_BOM_DELETE, PnDeletePDMDumpFileExecuter.class);
    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_BOM_DOWNLOAD_DUMPFILE, PnDownloadPDMDumpFileExecuter.class);
    // PCIS 189897: when saving a pdm database the local dumpfile is uploaded to a file server. The same must be applied after a pdm database is copied.
    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_BOM_COPY, PnCopyWithSaveExecuterExecuter.class);

    return factory.getBOMExecuters();
  }
}
