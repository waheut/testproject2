// Planon Enterprise Edition Source file: PnDeletePDMDumpFileExecuter.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.client.common.pdmdatabase.executer;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.pdm.core.common.*;

import nl.planon.util.pnlogging.*;

import nl.planon.zeus.actionpanel.executers.*;
import nl.planon.zeus.actionpanel.listeners.*;
import nl.planon.zeus.controller.*;
import nl.planon.zeus.graphicalcomponents.dialog.*;
import nl.planon.zeus.selections.*;

/**
 * Executer to delete files (dumpfiles, logfiles) from the file server associated with the deleted
 * PDM database. files are deleted after the records are deleted from db. If file delete fails an
 * error is shown but the delete is still successfull
 */
public class PnDeletePDMDumpFileExecuter extends PnExecuter implements IPnInSelectionExecuter
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOGGER = PnLogger.getLogger(PnDeletePDMDumpFileExecuter.class);

  //~ Classes --------------------------------------------------------------------------------------

  /**
   * DeleteCallback
   */
  class DeleteCallback implements IPnExecuterCallback
  {
    IPnExecuterCallback parentCallback;

    /**
     * Creates a new DeleteCallback object.
     *
     * @param aCallback parent
     */
    DeleteCallback(IPnExecuterCallback aCallback)
    {
      this.parentCallback = aCallback;
    }

    /**
     * {@inheritDoc}
     */
    @Override public void afterExecution() throws PnErrorListException, AuthorizationException
    {
      if (this.parentCallback != null)
      {
        this.parentCallback.afterExecution();
      }
    }


    /**
     * {@inheritDoc}
     */
    @Override public void beforeExecution() throws PnErrorListException, AuthorizationException
    {
      if (this.parentCallback != null)
      {
        this.parentCallback.beforeExecution();
      }
    }


    /**
     * {@inheritDoc}
     */
    @Override public void onFailure() throws PnErrorListException, AuthorizationException
    {
      if (this.parentCallback != null)
      {
        this.parentCallback.onFailure();
      }
    }


    /**
     * do delete files on file server after database is deleted
     *
     * @param  aBOMExecutedEvent BOMExecutedEvent
     *
     * @throws PnErrorListException
     * @throws AuthorizationException
     */
    @Override public void onSuccess(BOMExecutedEvent aBOMExecutedEvent) throws PnErrorListException, AuthorizationException
    {
      IBOValue boValueDatabase = aBOMExecutedEvent.getBOM().getBOValue();
      assert BOTypePDMP5Module.PDM_DATABASE.equals(boValueDatabase.getBOType()) : "The bovalue should be of type PDMDatabase";

      WebDAVFileserverCleaner cleaner = new WebDAVFileserverCleaner();

      PnCursorContext cursorContext = MouseCursorController.getInstance().pushWaitCursor();
      boolean fileServerClean;
      try
      {
        String dumpFilePath = boValueDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).getAsString();
        boolean dumpFileCleaned = cleaner.clean(dumpFilePath);

        String exportFilePath = boValueDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE).getAsString();
        boolean exportFileCleaned = cleaner.clean(exportFilePath);

        if (!dumpFileCleaned || !exportFileCleaned)
        {
          if (LOGGER.isErrorEnabled())
          {
            LOGGER.error("For databaseBO: '" + boValueDatabase.getPrimaryKeyValue().getAsString() + "'. Failed to delete the dumpFile or the exportFile form the server. dumpFile: (path: '" + dumpFilePath + "', deleted: '" + dumpFileCleaned + "'), exportFile: (path: '" + exportFilePath + "', deleted: '" + exportFileCleaned + "');");
          }
        }

        if (dumpFileCleaned && !exportFileCleaned)
        {
          PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_FAILED_DELETING_FILE_ON_FILESERVER.getLocalizebleKey(), exportFilePath);
        }
        else if (!dumpFileCleaned && exportFileCleaned)
        {
          PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_FAILED_DELETING_FILE_ON_FILESERVER.getLocalizebleKey(), dumpFilePath);
        }
        else if (!dumpFileCleaned && !exportFileCleaned)
        {
          PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_FAILED_DELETING_FILE_ON_FILESERVER.getLocalizebleKey(), dumpFilePath);
        }

        fileServerClean = dumpFileCleaned && exportFileCleaned;
      }
      finally
      {
        MouseCursorController.getInstance().popMouseCursor(cursorContext);
      }
      if (!fileServerClean)
      {
        PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_FAILED_DELETING_FILE_ON_FILESERVER.getLocalizebleKey());
      }

      if (this.parentCallback != null)
      {
        this.parentCallback.onSuccess(aBOMExecutedEvent);
      }
    }
  }

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PnDeleteDumpFileExecuter object.
   */
  public PnDeletePDMDumpFileExecuter()
  {
    super(CATEGORY_DELETE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * The dump file will be deleted from a specific location in the PDM domain.
   *
   * @param  aBOM
   * @param  aCallback
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  @Override public void execute(final IBOM aBOM, final IPnExecuterCallback aCallback) throws PnErrorListException, AuthorizationException
  {
    executeBOM(aBOM, new DeleteCallback(aCallback));
  }


  /**
   * {@inheritDoc}
   */
  @Override public void executeInSelection(final IBOM aBOM, final IPnInSelectionExecuterCallback aInSelectionCallback, final IPnExecuterCallback aExecuterCallback) throws AuthorizationException, PnErrorListException
  {
    executeInSelectionBOM(aBOM, aInSelectionCallback, new DeleteCallback(aExecuterCallback));
  }
}
