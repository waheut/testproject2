// Planon Enterprise Edition Source file: PopupSearchCriteriaFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.common.factory;

import java.util.*;

import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.zeus.editorregister.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * PopupSearchCriteriaFactoryPDMP5Module
 */
public class PopupSearchCriteriaFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the popup PV types registered by this module.
   *
   * @return A map of popup PV types keyed on combined PnName.
   */
  public static final Map<StepFieldTypeBoTypePnNameKey, IPVType> getPopupPVTypes()
  {
    PopupSearchCriteriaFactory factory = new PopupSearchCriteriaFactory();

    factory.registerPVType(FieldTypePDMP5Module.PDM_DATABASE_TYPE, BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF, PVTypePDMP5Module.PN_POPUP_PDMDATABASETYPE);
    factory.registerPVType(FieldTypePDMP5Module.BASE_PDM_DATABASE_SERVER, BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_DATABASESERVER_REF, PVTypePDMP5Module.PN_POPUP_PDMDATABASESERVER);
    factory.registerPVType(FieldTypePDMP5Module.PDM_DATABASE_FILE_TYPE, BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_FILE_TYPE_REF, PVTypePDMP5Module.PN_POPUP_PDMFILETYPE);
    factory.registerPVType(FieldTypePDMP5Module.PDM_DATABASE_FILE_TYPE, BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_EXPORT_FILE_TYPE, PVTypePDMP5Module.PN_POPUP_PDMFILETYPE);
    factory.registerPVType(FieldTypePDMP5Module.BASE_PDM_DATABASE_SERVER, BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF, PVTypePDMP5Module.PN_POPUP_DBSERVERS_BY_DBVERSION);
    factory.registerPVType(FieldTypePDMP5Module.BASE_PDM_DATABASE_VERSION, BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF, PVTypePDMP5Module.PN_POPUP_DBVERSIONS_BY_DBSERVER);

    return factory.getPopupPVTypes();
  }
}
