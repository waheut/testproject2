// Planon Enterprise Edition Source file: PnDownloadPDMDumpFileExecuter.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.client.common.pdmdatabase.executer;

import org.apache.commons.httpclient.*;

import javax.swing.*;

import java.io.*;
import java.net.*;

import nl.planon.ares.*;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.util.pnlogging.*;
import nl.planon.util.webdav.*;

import nl.planon.zeus.actionpanel.executers.*;
import nl.planon.zeus.controller.*;
import nl.planon.zeus.graphicalcomponents.dialog.*;
import nl.planon.zeus.graphicalcomponents.error.*;
import nl.planon.zeus.localize.*;

/**
 * Executer to download a dumpfile to a location chosen by the user, after it has been exported by
 * PDM.
 */
public class PnDownloadPDMDumpFileExecuter extends PnExecuter<IBOM>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(PnDownloadPDMDumpFileExecuter.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PnDownloadPDMDumpFileExecuter object.
   */
  public PnDownloadPDMDumpFileExecuter()
  {
    super(CATEGORY_SAVE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * download a dumpfile from a fileserver to a local location
   *
   * @param  aBOValue          BOValue of PDMDatabase of which the dumpfile must be downloaded
   * @param  aDumpFile         The name of the dumpfile
   * @param  aLocalDestination Local destination to where the dumpfile must be downloaded
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void downloadPDMDumpfile(IBOValue aBOValue, String aDumpFile, String aLocalDestination) throws PnErrorListException, AuthorizationException
  {
    try
    {
      WebDAVUtilities.getInstance().downloadPDMDumpFile(aDumpFile, aLocalDestination);

      if (LOG.isInfoEnabled())
      {
        LOG.info("    --> success!\n\tRemote file: " + aDumpFile + "\n\tLocal file: " + aLocalDestination);
      }
    }
    catch (HttpException ex)
    {
      LOG.info("    --> failed with HttpException: " + ex.getLocalizedMessage());
    }
    catch (ConnectException connectException)
    {
      // Show error message when WebDAV server is not reacting due to ConnectException
      String errorMessage = SessionAres.get().getTranslator().getString(ErrorNumberPDMP5Module.EC_WEBDAV_CONNECTION_ERROR.getLocalizebleKey());
      PlanonMessageDialog.showErrorDialog(errorMessage);
    }
    catch (IOException ex)
    {
      if (ex instanceof PnWebDavException)
      {
        if (((PnWebDavException) ex).isUnAuthorized())
        {
          PnErrorDialogController.showResourceDialog(ZeusResource.FILE_WEBDAV_NOT_AUTHORIZED);
        }
        if (((PnWebDavException) ex).isNotFound())
        {
          PnErrorDialogController.showResourceDialog(ZeusResource.FILE_WEBDAV_NOT_FOUND, aDumpFile);
          // If export dump file is not found on the specified location, the corresponding field must be cleared.
          clearPDMExportedDumpFileField(aBOValue);
        }
      }
      LOG.info("    --> failed with IOException: " + ex.getLocalizedMessage());
    }
  }


  /**
   * The exported dump file will be downloaded to a location the user chooses.
   *
   * @param  aBOM
   * @param  aCallback
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  @Override public void execute(final IBOM aBOM, final IPnExecuterCallback aCallback) throws PnErrorListException, AuthorizationException
  {
    // System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
    // System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
    // System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire.header", "debug");
    // System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");

    IBOValue boValue = aBOM.getBOValue();
    String webDAVDumpFile = boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE).getAsString();

    PlanonFileChooser fileChooser = new PlanonFileChooser();
    //fileChooser.addChoosableFileFilter(new TextFileFilter());
    // use the extension of the exported dumpfile as default extension
    fileChooser.setDefaultExtension(webDAVDumpFile.substring(webDAVDumpFile.lastIndexOf("."), webDAVDumpFile.length()));
    fileChooser.setDialogTitle(SessionAres.get().getTranslator().getString(ZeusResource.LANGUAGE_SAVEDIALOG_TITLE));
    if (fileChooser.showSaveDialog() == JFileChooser.APPROVE_OPTION)
    {
      // PBL 14560: show hourglass cursor during downloading the dumpfile to a file server.
      PnCursorContext cursorContext = MouseCursorController.getInstance().pushWaitCursor();
      try
      {
        downloadPDMDumpfile(boValue, webDAVDumpFile, fileChooser.getSelectedFile().getAbsolutePath());
      }
      finally
      {
        MouseCursorController.getInstance().popMouseCursor(cursorContext);
      }
    }
  }


  /**
   * Clear the field in which the location and name of the exported dump file is specified.
   *
   * @param  aBOValue
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private void clearPDMExportedDumpFileField(IBOValue aBOValue) throws PnErrorListException, AuthorizationException
  {
    aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE).clear();
    IBOM saveDatabaseBOM = aBOValue.getBOMListManager().getSaveBOMEx();

    BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();
    bomResourceLocator.execute(saveDatabaseBOM);
  }
}
