// Planon Enterprise Edition Source file: PDMP5ModuleImageResourceLocator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.common.image;

import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.zeus.osgi.integration.*;

/**
 * PDMP5ModuleImageResourceLocator
 */
public class PDMP5ModuleImageResourceLocator extends ModuleImageResourceLocator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String RESOURCE_FILE = "image.properties";
  private static final String RESOURCE_LOCATION = "/nl/planon/morpheus/pdm/client/common/image";
  private static PDMP5ModuleImageResourceLocator instance;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMP5ModuleImageResourceLocator object.
   */
  private PDMP5ModuleImageResourceLocator()
  {
    super(ModulePDMP5Module.NAME, RESOURCE_LOCATION, RESOURCE_FILE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Create a PDMP5ModuleImageResourceLocator instance.
   *
   * @return PDMP5ModuleImageResourceLocator instance.
   */
  public static PDMP5ModuleImageResourceLocator getInstance()
  {
    if (instance == null)
    {
      instance = new PDMP5ModuleImageResourceLocator();
    }
    return instance;
  }
}
