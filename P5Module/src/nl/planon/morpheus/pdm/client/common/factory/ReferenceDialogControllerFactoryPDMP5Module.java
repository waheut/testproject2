// Planon Enterprise Edition Source file: ReferenceDialogControllerFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.common.factory;

import java.util.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.zeus.dialogs.*;
import nl.planon.zeus.dialogs.reference.*;
import nl.planon.zeus.editorregister.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * ReferenceDialogControllerFactoryPDMP5Module
 */
public class ReferenceDialogControllerFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Retrieves the registered dialog controllers in context of their reference fields.
   *
   * @return Hashmap of dialog controllers in context of their reference fields.
   */
  public static final Map<FieldTypeBoTypePnNameKey, IDialogControllerLoader> getReferenceDialogControllers()
  {
    IReferenceDialogControllerFactory factory = new ReferenceDialogControllerFactory();
    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF, PnProxyRefDialogControllerWithContext.class);
    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_FILE_TYPE_REF, PnProxyRefDialogControllerWithContext.class);
    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_EXPORT_FILE_TYPE, PnProxyRefDialogControllerWithContext.class);
    factory.register(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_DATABASESERVER_REF, PnProxyRefDialogControllerWithContext.class);
    return factory.getReferenceDialogControllers();
  }
}
