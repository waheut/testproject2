// Planon Enterprise Edition Source file: PnUploadPDMDumpFileExecuter.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.client.common.pdmdatabase.executer;

import org.apache.commons.httpclient.*;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import nl.planon.ares.aresutilities.*;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.util.pnlogging.*;
import nl.planon.util.webdav.*;

import nl.planon.zeus.actionpanel.executers.*;
import nl.planon.zeus.controller.*;
import nl.planon.zeus.graphicalcomponents.dialog.*;

/**
 * Executer to upload a dumpfile to a specified WebDAV location, so it will be available for the
 * database server at which it has to be imported.
 */
public class PnUploadPDMDumpFileExecuter extends PnExecuter<IBOM> implements IPnInSelectionExecuter
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(PnUploadPDMDumpFileExecuter.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PnUploadDumpFileExecuter object.
   */
  public PnUploadPDMDumpFileExecuter()
  {
    super(CATEGORY_SAVE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * The dump file will be uploaded to a specific location in the PDM domain for further processing.
   *
   * @param  aBOM
   * @param  aCallback
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  @Override public void execute(final IBOM aBOM, final IPnExecuterCallback aCallback) throws PnErrorListException, AuthorizationException
  {
    // PBL 14560: show hourglass cursor during uploading the dumpfile to a file server.
    PnCursorContext cursorContext = MouseCursorController.getInstance().pushWaitCursor();
    try
    {
      IBOValue boValue = aBOM.getBOValue();

      assert BOTypePDMP5Module.PDM_DATABASE.equals(boValue.getBOType()) : "boValue must be of type PDM_DATABASE";

      // First check whether all the mandatory fields of PDMDatabase are filled-in or not.
      // This check is added to improve performance as we are checking at client side itself before uploading the database into the file server.
      // Refer PCIS # 301188.00.
      if (areAllMandatoryFieldsFilled(boValue))
      {
        // Also, check the validation of the Name field of PDMDatabase.
        // This check is very similar to "VROnlyWordCharsAllowedInFieldName" except the fact that this is client side validation.
        String name = boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_NAME).getAsString();
        String trimmedName = name.trim();
        if (!Pattern.matches("[A-Za-z]{1}(\\w)+", trimmedName))
        {
          PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED.getLocalizebleKey(), //
            boValue.getBODefinitionValue().getFieldDefByPnName(IBOPDMDatabaseDef.PN_NAME).getPnName());
          return;
        }

        // If everything above is fine, then go ahead to upload the database into the file server.
        IBOFieldValue fieldValueDumpFile = boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DUMPFILE);
        IBOFieldValue fieldValuePDMDumpFile = boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE);
        // only upload dumpfile when the original dumpfile field is not empty and is changed (in case of save) or
        // when the original dumpfile field is not empty but the destination dumpfile is (in case of copy)
        if ((!fieldValueDumpFile.isEmpty()) //
          && ((fieldValueDumpFile.isChanged()) //
            || (fieldValuePDMDumpFile.isEmpty())))
        {
          String destinationHost = getDestinationHost(boValue);
          // Upload only possible if pdm location is filled in
          if (destinationHost != null)
          {
            MessageType messageType = WebDAVUtilities.getInstance().doUploadPDMDumpFile(boValue, destinationHost);
            if (messageType != null)
            {
              PlanonMessageDialog.showResourceErrorDialog(messageType.getLocalizebleKey(), //
                boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DUMPFILE).getAsString());
            }
          }
        }

        // If for any other reason, the BOMSave fails to save the BOPDMDatabase then the dump file that was uploaded onto the file server
        // should be removed. Otherwise it would stay there in the file server as an orphan.
        // This is added as part of the fix for PCIS # 301188.00.
        final IPnExecuterCallback executerCallback = new PnExecuterCallback(aCallback)
        {
          @Override public void onFailure() throws PnErrorListException, AuthorizationException
          {
            IBOValue boValue = aBOM.getBOValue();
            String destinationHost = getDestinationHost(boValue);
            if (destinationHost != null)
            {
              try
              {
                PnWebDAVResource resource = new PnWebDAVResource(new HttpURL(destinationHost));
                if (resource != null)
                {
                  if (!boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).isEmpty())
                  {
                    resource.deleteMethod(boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).getAsString());
                  }
                }
              }
              catch (URIException ex)
              {
                PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_WEBDAV_CONNECTION_ERROR.getLocalizebleKey(), //
                  boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DUMPFILE).getAsString());
              }
              catch (HttpException ex)
              {
                PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_WEBDAV_CONNECTION_ERROR.getLocalizebleKey(), //
                  boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DUMPFILE).getAsString());
              }
              catch (IOException ex)
              {
                PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_WEBDAV_CONNECTION_ERROR.getLocalizebleKey(), //
                  boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DUMPFILE).getAsString());
              }
              super.onFailure();
            }
          }
        };

        executeBOM(aBOM, executerCallback);
      }
    }
    finally
    {
      MouseCursorController.getInstance().popMouseCursor(cursorContext);
    }
  }


  /**
   * Returns 'True' of 'False' based on whether all the mandatory fields are filled-in or not.
   *
   * @param  aBOValue PDMDatabase BO Value
   *
   * @return boolean T or F
   *
   * @throws PnErrorListException
   */
  private boolean areAllMandatoryFieldsFilled(IBOValue aBOValue) throws PnErrorListException
  {
    IErrorHandler errorManager = null;
    List<IFOTab> tabs = ((IFOValue) aBOValue).getTabList();
    int numberOfTabs = tabs.size();
    for (int i = 0; i < numberOfTabs; i++)
    {
      IFOTab tab = tabs.get(i);
      List<IFOTabMember> tabMembers = tab.getTabMembers();
      int numberOfMembers = tabMembers.size();
      for (int j = 0; j < numberOfMembers; j++)
      {
        IFOTabMember member = tabMembers.get(j);
        if (member instanceof FOFieldValue)
        {
          FOFieldValue fieldValue = (FOFieldValue) member;

          if (fieldValue.isMandatory() && fieldValue.getBaseValue().isEmpty())
          {
            if ((fieldValue.isTSIReadonly() || !fieldValue.isReadonly()) & (!fieldValue.isSystemField()))
            {
              PlanonMessageDialog.showResourceErrorDialog(ErrorNumberPDMP5Module.EC_MANDATORYFIELD_NOT_FILLED.getLocalizebleKey(), //
                fieldValue.getFieldDefinitionValue().getDisplayName());
              return false;
            }
          }
        }
      }
    }
    return true;
  }


  /**
   * Returns the destination host that was filled-in PDMLocation.WeDavLocation field. This will be
   * retrieved using bovalue of PDMDatabase.
   *
   * @param  boValue PDMDatabase value
   *
   * @return String of destination host. Can be null.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws FieldNotFoundException
   */
  private String getDestinationHost(IBOValue boValue) throws PnErrorListException, AuthorizationException, FieldNotFoundException
  {
    IFOValue pdmLocation = AresUtilities.getInstance().getBOValueByPrimaryKeyEx(BOTypePDMP5Module.PDM_LOCATION, boValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDMLOCATION_REF).getBaseValue());
    if (pdmLocation != null)
    {
      return pdmLocation.getFieldByPnNameEx(IBOPDMLocationDef.PN_LOCATION).getAsString();
    }
    return null;
  }
}
