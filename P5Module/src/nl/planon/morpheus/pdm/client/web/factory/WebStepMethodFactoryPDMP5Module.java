// Planon Enterprise Edition Source file: WebStepMethodFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.web.factory;

import java.util.*;

import nl.planon.hades.module.*;

import nl.planon.hera.osgi.integration.helper.*;

import nl.planon.zeus.methods.*;

/**
 * WebStepMethodFactoryPDMP5Module
 */
public class WebStepMethodFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Creates a new factory instance that contains a registry. This registry is filled with
   * registrations in this method. The return value will be a list which contains <b>common</b> step
   * methods that will be registered.
   *
   * @return list with all <b>common</b> step methods
   */
  public static List<IStepMethodLoader> getCommonStepMethods()
  {
    WebStepMethodFactory factory = new WebStepMethodFactory();

    // factory.registerCommonStepMethod(WebExampleStepMethod.class);

    return factory.getCommonStepMethods();
  }


  /**
   * Creates a new factory instance that contains a registry. This registry is filled with
   * registrations in this method. The return value will be a map which contains steps for which one
   * or more steps methods are registered.
   *
   * @return map with all step methods per drill down step
   */
  public static Map<IDrilldownStepType, List<IStepMethodLoader>> getStepMethods()
  {
    WebStepMethodFactory factory = new WebStepMethodFactory();

    // factory.registerStepMethod(ModulePDMP5Module.EXAMPLE_STEP,
    // PnWebExampleStepMethod.class);

    return factory.getStepMethods();
  }
}
