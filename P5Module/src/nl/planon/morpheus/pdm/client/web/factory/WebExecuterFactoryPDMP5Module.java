// Planon Enterprise Edition Source file: WebExecuterFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.web.factory;

import java.util.*;

import nl.planon.morpheus.pdm.client.common.factory.*;

import nl.planon.zeus.actionpanel.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * WebExecuterFactoryPDMP5Module<br>
 *
 * <p>This factory is used for registring executers that have a special implementation for the web.
 * <br>
 * For example when a file has to be printed the file has to be streamed to the client.</p>
 */
public class WebExecuterFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * returns a Map of registered executers.<br>
   * The common executers for swing and web are combined with the special executers for the web.
   *
   * @return Map
   */
  public static final Map<IExecuterKey, IExecuterLoader> getBOMExecuters()
  {
    Map<IExecuterKey, IExecuterLoader> executerKeys = new HashMap<IExecuterKey, IExecuterLoader>();

    executerKeys.putAll(BOMExecuterFactoryPDMP5Module.getBOMExecuters());

    BOMExecuterFactory factory = new BOMExecuterFactory();
    // factory.register(BOTypePDMP5Module.EXAMPLE,
    // IBOExampleDef.PN_BOM_EXAMPLE, PnWebPreviewExampleExecuter.class);
    executerKeys.putAll(factory.getBOMExecuters());

    return Collections.unmodifiableMap(executerKeys);
  }
}
