// Planon Enterprise Edition Source file: WebDialogFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.web.factory;

import java.util.*;

import nl.planon.hera.osgi.integration.helper.*;

import nl.planon.zeus.graphicalcomponents.pndialog.*;
import nl.planon.zeus.osgi.integration.helper.*;

/**
 * WebDialogFactoryPDMP5Module
 */
public class WebDialogFactoryPDMP5Module
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns loaders for the dialogs registered by this module.
   *
   * @return A map of loaders keyed on dialog controller.
   */
  public static final Map<Class<? extends PnBaseDialogController>, IDialogLoader> getDialogs()
  {
    WebDialogFactory webDialogFactory = new WebDialogFactory();

    // webDialogFactory.register(PnExampleDialogController.class,
    // PnWebGridActionDialog.class);

    return ((IDialogFactory) webDialogFactory).getDialogs();
  }
}
