// Planon Enterprise Edition Source file: WebClientBundleServicePDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.client.web.osgi;

import java.util.*;

import nl.planon.ares.resourcelocators.*;

import nl.planon.hades.module.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.client.common.factory.*;
import nl.planon.morpheus.pdm.client.common.image.*;
import nl.planon.morpheus.pdm.client.web.factory.*;

import nl.planon.zeus.actionpanel.*;
import nl.planon.zeus.editorregister.*;
import nl.planon.zeus.graphicalcomponents.pndialog.*;
import nl.planon.zeus.methods.*;
import nl.planon.zeus.osgi.integration.*;

/**
 * WebClientBundleServicePDMP5Module
 */
public class WebClientBundleServicePDMP5Module extends BaseClientBundleService
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public Map<IExecuterKey, IExecuterLoader> getBOMExecuters()
  {
    return WebExecuterFactoryPDMP5Module.getBOMExecuters();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<Class<? extends PnBaseDialogController>, IDialogLoader> getDialogs()
  {
    return WebDialogFactoryPDMP5Module.getDialogs();
  }


  /**
   * {@inheritDoc}
   */
  @Override public BaseImageResourceLocator getImageResourceLocator()
  {
    return PDMP5ModuleImageResourceLocator.getInstance();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<StepFieldTypeBoTypePnNameKey, IPVType> getPopupPVTypes()
  {
    return PopupSearchCriteriaFactoryPDMP5Module.getPopupPVTypes();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<IDrilldownStepType, List<IStepMethodLoader>> getStepMethods()
  {
    return WebStepMethodFactoryPDMP5Module.getStepMethods();
  }
}
