// Planon Enterprise Edition Source file: WebActivator.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.client.web.osgi;

import nl.planon.hera.osgi.integration.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * WebActivator
 */
public class WebActivator extends WebBundleActivator
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void start()
  {
    registerService(ModulePDMP5Module.NAME, new WebClientBundleServicePDMP5Module(), null, null, null, null);
  }
}
