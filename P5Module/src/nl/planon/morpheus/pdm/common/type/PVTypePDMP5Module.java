// Planon Enterprise Edition Source file: PVTypePDMP5Module.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.common.type;

import nl.planon.hades.proxyview.*;

/**
 * PVTypePDMP5Module
 */
public class PVTypePDMP5Module
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IPVType DATAFILELOCATIONS_OF_DBSERVER = new PVType("DATAFILELOCATIONS_OF_DBSERVER");
  public static final IPVType DBSERVERBYDBTYPELOOKUP = new PVType("DBSERVERBYDBTYPELOOKUP");
  public static final IPVType PDM_DATA_FILE_LOCATION_MUST_BE_FILLED = new PVType("PDMDATAFILELOCATIONMUSTBEFILLED");
  public static final IPVType PDM_DATABASE_SERVER_SEARCH = new PVType("PDMDATABASESERVERSEARCH");
  public static final IPVType PDM_SEARCH_FOR_HIGHEST_DB_SERVER_VERSION = new PVType("PDM_SEARCH_FOR_HIGHEST_DB_SERVER_VERSION");
  public static final IPVType PDM_SUPPORTED_VERSION_MUST_BE_FILLED = new PVType("PDMSUPPORTEDVERSIONMUSTBEFILLED");
  public static final IPVType PDM_SUPPORTED_VERSIONS_FROM_ONE_SERVER = new PVType("PDMSUPPORTEDVERSIONSFROMONESERVER");
  public static final IPVType PDM_TASK_PENDING = new PVType("PDMTaskPending");
  public static final IPVType PDMANALYZE = new PVType("PDMANALYZE");
  public static final IPVType PDMANALYZE_BY_PKREQUEST = new PVType("PDMANALYZEBYPKREQUEST");
  public static final IPVType PDMDATABASE = new PVType("PDMDATABASE");
  public static final IPVType PDMDATABASETYPE = new PVType("PDMDATABASETYPE");
  public static final IPVType PDMDB_FILE_LOCATION_IN_CONTEXT_OF_PDMDBSERVER = new PVType("PDMDBFILELOCATIONINCONTEXTOFPDMDBSERVER");
  public static final IPVType PDMDBSERVER = new PVType("PDMDBSERVER");
  public static final IPVType PDMDBSERVER_IN_CONTEXT_OF_PDMDBVERSION = new PVType("PDMDBSERVERINCONTEXTOFPDMDBVERSION");
  public static final IPVType PDMDBSERVER_MTON_PDMDBVERSION_MSSQL = new PVType("PDMDBSERVERMTONPDMDBVERSIONMSSQL");
  public static final IPVType PDMDBSERVER_MTON_PDMDBVERSION_MSSQL_IN_CONTEXT = new PVType("PDMDBSERVERMTONPDMDBVERSIONMSSQLINCONTEXT");
  public static final IPVType PDMDBSERVER_MTON_PDMDBVERSION_ORACLE = new PVType("PDMDBSERVERMTONPDMDBVERSIONORACLE");
  public static final IPVType PDMDBSERVER_MTON_PDMDBVERSION_ORACLE_IN_CONTEXT = new PVType("PDMDBSERVERMTONPDMDBVERSIONORACLEINCONTEXT");
  public static final IPVType PDMDBVERSION = new PVType("PDMDBVERSION");
  public static final IPVType PDMDBVERSION_IN_CONTEXT_OF_PDMDBSERVER = new PVType("PDMDBVERSIONINCONTEXTOFPDMDBSERVER");
  public static final IPVType PDMDBVERSION_MTON_PDMDBSERVER_MSSQL = new PVType("PDMDBVERSIONMTONPDMDBSERVERMSSQL");
  public static final IPVType PDMDBVERSION_MTON_PDMDBSERVER_MSSQL_IN_CONTEXT = new PVType("PDMDBVERSIONMTONPDMDBSERVERMSSQLINCONTEXT");
  public static final IPVType PDMDBVERSION_MTON_PDMDBSERVER_ORACLE = new PVType("PDMDBVERSIONMTONPDMDBSERVERORACLE");
  public static final IPVType PDMDBVERSION_MTON_PDMDBSERVER_ORACLE_IN_CONTEXT = new PVType("PDMDBVERSIONMTONPDMDBSERVERORACLEINCONTEXT");
  public static final IPVType PDMFILETYPE = new PVType("PDMFILETYPE");
  public static final IPVType PDMFILETYPE_CODE = new PVType("PDMDBSERVERCODE");
  public static final IPVType PDMFILETYPEMSSQL = new PVType("PDMFILETYPEMSSQL");
  public static final IPVType PDMFILETYPEORACLE = new PVType("PDMFILETYPEORACLE");
  public static final IPVType PDMLOCATION = new PVType("PDMLOCATION");
  public static final IPVType PDMREQUEST = new PVType("PDMREQUEST");
  public static final IPVType PDMREQUEST_DUMPFILE = new PVType("PDMRequestDumpFile");
  public static final IPVType PDMREQUEST_TYPE_CODE = new PVType("PDMREQUEST_TYPE_CODE");
  public static final IPVType PDMTASK = new PVType("PDMTask");
  public static final IPVType PDMTASK_REQUEST_DATABASE = new PVType("PDMTASK_REQUEST_DATABASE");
  public static final IPVType PDMREQUEST_INCONTEXT_DATABASE = new PVType("PDMREQUEST_INCONTEXT_DATABASE");
  public static final IPVType PDMTASKS_IN_INITIAL_STATE = new PVType("PDMTASKS_IN_INITIAL_STATE");
  public static final IPVType PN_LOOKUP_PDMDATABASESERVER = new PVType("PnLookupPDMDatabaseServer");
  public static final IPVType PN_LOOKUP_PDMDATABASETYPE = new PVType("PnLookupPDMDatabaseType");
  public static final IPVType PN_LOOKUP_PDMEXPORTFILETYPE = new PVType("PnLookupPDMExportFileType");
  public static final IPVType PN_LOOKUP_PDMFILETYPE = new PVType("PnLookupPDMFileType");
  public static final IPVType PN_POPUP_DBSERVERS_BY_DBTYPE = new PVType("PnPopupDBServersByDBType");
  public static final IPVType PN_POPUP_DBSERVERS_BY_DBVERSION = new PVType("PnPopupDBServersByDBVersion");
  public static final IPVType PN_POPUP_DBVERSIONS_BY_DBSERVER = new PVType("PnPopupDBVersionsByDBServer");
  public static final IPVType PN_POPUP_PDMDATABASESERVER = new PVType("PnPopupPDMDatabaseServer");
  public static final IPVType PN_POPUP_PDMDATABASETYPE = new PVType("PnPopupPDMDatabaseType");
  public static final IPVType PN_POPUP_PDMFILETYPE = new PVType("PnPopupPDMFileType");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_COMPATIBILITYMATRIX = new PVType("PnStepPDMAdminModuleCompatibilityMatrix");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_DATABASE_SERVERS = new PVType("PnStepPDMAdminModuleDatabaseServers");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_DATABASE_VERSIONS = new PVType("PnStepPDMAdminModuleDatabaseVersions");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_DBSERVER_DATAFILELOC = new PVType("PnStepPDMAdminModuleDbServerDataFileLoc");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_PDM_DATABASES = new PVType("PnStepPDMAdminModulePDMDatabases");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_PDM_LOCATIONS = new PVType("PnStepPDMAdminModulePDMLocations");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_REQUEST_LOGS = new PVType("PnStepPDMAdminModuleRequestLogs");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_REQUESTS = new PVType("PnStepPDMAdminModuleRequests");
  public static final IPVType PN_STEP_PDM_ADMIN_MODULE_TASKS = new PVType("PnStepPDMAdminModuleTasks");
  public static final IPVType PDM_LATEST_EVENTLOGS = new PVType("PDM_LATEST_EVENTLOGS");
}
