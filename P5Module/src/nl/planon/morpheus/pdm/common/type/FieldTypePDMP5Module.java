// Planon Enterprise Edition Source file: FieldTypePDMP5Module.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.common.type;

import nl.planon.hades.field.*;

/**
 * FieldTypePDMP5Module
 */
public class FieldTypePDMP5Module
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Create the FieldType constants of the FieldTypePDMP5Module module here.
  // !! Keep them alphabetic !!

  public static final FieldType BASE_PDM_DATABASE_SERVER = new FieldType("BasePDMDatabaseServer");
  public static final FieldType BASE_PDM_DATABASE_VERSION = new FieldType("BasePDMDatabaseVersion");
  public static final FieldType BASE_PDM_REQUEST = new FieldType("BasePDMRequest");
  public static final FieldType BASE_PDM_TASK = new FieldType("BasePDMTask");
  public static final FieldType PDM_DATABASE = new FieldType("PDMDatabase");
  public static final FieldType PDM_DATABASE_FILE_TYPE = new FieldType("PDMDatabaseFileType");
  public static final FieldType PDM_DATABASE_SERVER_MSSQL = new FieldType("PDMDatabaseServerMSSQL");
  public static final FieldType PDM_DATABASE_SERVER_ORACLE = new FieldType("PDMDatabaseServerOracle");
  public static final FieldType PDM_DATABASE_STATE = new FieldType("PDMDatabaseState");
  public static final FieldType PDM_DATABASE_TYPE = new FieldType("PDMDatabaseType");
  public static final FieldType PDM_DATABASE_PRIVILEGE_TYPE = new FieldType("PDMDatabasePrivilegeType");
  public static final FieldType PDM_DATABASE_VERSION_MSSQL = new FieldType("PDMDatabaseVersionMSSQL");
  public static final FieldType PDM_DATABASE_VERSION_ORACLE = new FieldType("PDMDatabaseVersionOracle");
  public static final FieldType PDM_LOCATION = new FieldType("PDMLocation");
  public static final FieldType PDM_REQUEST_DELETE = new FieldType("PDMRequestDelete");
  public static final FieldType PDM_REQUEST_EXECUTE_STATE = new FieldType("PDMRequestExecuteState");
  public static final FieldType PDM_REQUEST_EXPORT = new FieldType("PDMRequestExport");
  public static final FieldType PDM_REQUEST_IMPORT = new FieldType("PDMRequestImport");
  public static final FieldType PDM_REQUEST_RELOAD_EXPORT = new FieldType("PDMRequestReloadExport");
  public static final FieldType PDM_REQUEST_RELOAD_INITIAL = new FieldType("PDMRequestReloadInitial");
  public static final FieldType PDM_REQUEST_TYPE = new FieldType("PDMRequestType");
  public static final FieldType PDM_TASK_ANALYZE_IMPORT = new FieldType("PDMTaskAnalyzeImport");
  public static final FieldType PDM_TASK_EXECUTE_STATE = new FieldType("PDMTaskExecuteState");
  public static final FieldType PDM_TASK_DELETE = new FieldType("PDMTaskDelete");
  public static final FieldType PDM_TASK_EXPORT = new FieldType("PDMTaskExport");
  public static final FieldType PDM_TASK_IMPORT = new FieldType("PDMTaskImport");
}
