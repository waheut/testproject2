// Planon Enterprise Edition Source file: ErrorNumberPDMP5Module.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.common.type;

import java.util.*;

import nl.planon.hades.errorhandling.*;
import nl.planon.hades.osgi.integration.*;

import nl.planon.pdm.core.common.error.*;

/**
 * ErrorNumberPDMP5Module
 */
public class ErrorNumberPDMP5Module implements IPlanonBundleMessageTypeService
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PREFIX = ModulePDMP5Module.PREFIX;
  private static MessageTypeRegistry registry = new MessageTypeRegistry(PREFIX, ModulePDMP5Module.NAME);

  public static final IMessageType EC_ALL_POSSIBLE_FILETYPES_TRIED = createError(ErrorDefinition.ALL_POSSIBLE_FILETYPES_TRIED);
  public static final MessageType EC_CANNOT_APPLY_CHANGES_TO_ACTIVE_DBSERVER = (ErrorMessageType) registry.createError("0006", "ecCanNotApplyChangesToActiveDBServer", "Database server is active. Cannot apply modification.");
  public static final MessageType EC_CANNOT_APPLY_CHANGES_TO_NON_INITIAL_DATABASE = (ErrorMessageType) registry.createError("0010", "ecCannotApplyChangesToNonInitialDatabase", "Database is not in initial state. Cannot apply modification.");
  public static final MessageType EC_CANNOT_DELETE_NON_INITIAL_PDMDATABASE = (ErrorMessageType) registry.createError("0002", "ecCannotDeleteNonInitialPDMDatabase", "It is not allowed to delete this PDM Database, since it is not cleaned up yet.");
  public static final MessageType EC_CANNOT_IMPORT_WITH_MULTIPLE_SCHEMANAMES = (ErrorMessageType) registry.createError("0032", "ecCannotIimportWithMultipleShemaNames", "There are multiple schema names defined. Please select one of them before importing.");
  public static final MessageType EC_CANNOT_INACTIVATE_DBSERVER_WITH_BUSY_DB = (ErrorMessageType) registry.createError("0007", "ecCannotInactivateDbServerWithBusyDb", "Cannot set db server to inactive or change the PDM location: it has Pending db''s.");
  public static final MessageType EC_CODEC_EXCEPTION_IN_REQUEST = (ErrorMessageType) registry.createError("0039", "ecCodecExceptionInRequest", "Request failed: a CodecException occurred when handling the request.");
  public static final IMessageType EC_CORRUPTED_DUMP_FILE = createError(ErrorDefinition.CORRUPTED_DUMP_FILE);
  public static final IMessageType EC_DATABASE_SERVER_VERSION_NOT_MATCH = createError(ErrorDefinition.DATABASE_SERVER_VERSION_NOT_MATCH);
  public static final MessageType EC_DATABASE_STATE_IS_NOT_ALLOWED = (ErrorMessageType) registry.createError("0045", "ecDatabaseStateIsNotAllowed", "Database state cannot be changed to {0}, because the state of the last request is {1}");
  public static final MessageType EC_DBSERVER_DONT_EQUAL_WITH_DATABASE_FILETYPE = (ErrorMessageType) registry.createError("0059", "ecDbserverDontEqualWithDatabaseFiletype", "Database server file type do not support database file type.");
  public static final MessageType EC_DBTYPE_DOES_NOT_MATCH = (ErrorMessageType) registry.createError("0036", "ecDbTypeDoesNotMatch", "The values for the fields {0}, {1} and {2} do not correspond to the same database type.");
  public static final MessageType EC_DBTYPE_DOES_NOT_MATCH_VERSION_DBTYPE = (ErrorMessageType) registry.createError("0004", "ecDbTypeDoesNotMatchVersionDbType", "Database type {0} does not match with db type of version {1} linked to database server {2}.");
  public static final MessageType EC_DBTYPE_SERVER_DOES_NOT_MATCH_DBTYPE_VERSION = (ErrorMessageType) registry.createError("0011", "ecDbtypeServerDoesNotMatchDbtypeVersion", "Database type of server {0} does not match with database type of version {1}.");
  public static final IMessageType EC_DUMP_VERSION_NOT_SUPPORT = createError(ErrorDefinition.DUMP_VERSION_NOT_SUPPORT);
  public static final IMessageType EC_ERROR_ON_EXPORTING_DUMPFILE = createError(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE);
  public static final IMessageType EC_ERROR_ON_FILLING_RESPONSE_MESSAGE = createError(ErrorDefinition.ERROR_ON_FILLING_RESPONSE_MESSAGE);
  public static final IMessageType EC_ERROR_ON_IMPORTING_DUMPFILE = createError(ErrorDefinition.ERROR_ON_IMPORTING_DUMPFILE);
  public static final IMessageType EC_ERROR_ON_IMPORTING_DUMPFILE_MSSQL = createError(ErrorDefinition.ERROR_ON_IMPORTING_DUMPFILE_MSSQL);
  public static final IMessageType EC_ERROR_ON_IMPORTING_DUMPFILE_OLDSTYLE = createError(ErrorDefinition.ERROR_ON_IMPORTING_DUMPFILE_OLDSTYLE);
  public static final IMessageType EC_EXPORT_LOCATION_NOT_SPECIFIED = createError(ErrorDefinition.EXPORT_LOCATION_NOT_SPECIFIED);
  public static final IMessageType EC_FAILED_DELETING_FILE_ON_DBSERVER = createError(ErrorDefinition.FAILED_DELETING_FILE_ON_DBSERVER);
  public static final MessageType EC_FAILED_DELETING_FILE_ON_FILESERVER = (ErrorMessageType) registry.createError("0049", "ecFailedDeletingFileOnFileServer", "Failed to delete original file on file server: {0}");
  public static final IMessageType EC_FAILED_INSTANTIATION_AGENT_ACTION = createError(ErrorDefinition.FAILED_INSTANTIATION_AGENT_ACTION);
  public static final IMessageType EC_FAILED_LOGGING_TO_WEBDAV_LOCATION = createError(ErrorDefinition.FAILED_LOGGING_TO_WEBDAV_LOCATION);
  public static final IMessageType EC_FAILED_PARSING_NO_SCHEMA_FOUND = createError(ErrorDefinition.FAILED_PARSING_NO_SCHEMA_FOUND);
  public static final IMessageType EC_FAILED_PARSING_NO_TABLESPACE_FOUND = createError(ErrorDefinition.FAILED_PARSING_NO_TABLESPACE_FOUND);
  public static final IMessageType EC_FAILED_RETRIEVAL_DUMPFILE = createError(ErrorDefinition.FAILED_RETRIEVAL_DUMPFILE);
  public static final IMessageType EC_FAILED_UPLOADING_TO_WEBDAV_LOCATION = createError(ErrorDefinition.FAILED_UPLOADING_TO_WEBDAV_LOCATION);
  public static final MessageType EC_FIELD_MANDATORY_FOR_REQUEST = (ErrorMessageType) registry.createError("0066", "ecFieldMandatoryForRequest", "The field {0} is mandatory for this type of request");
  public static final MessageType EC_FILESERVER_NOT_AVAILABLE = (ErrorMessageType) registry.createError("0047", "ecFileServerNotAvailable", "Fileserver not available via WebDAV-location {0}");
  public static final MessageType EC_FILEUPLOAD_FAILED = (ErrorMessageType) registry.createError("0037", "ecFileUploadFailed", "Upload dumpfile to fileserver {0} failed");
  public static final IMessageType EC_FILE_NOT_FOUND = createError(ErrorDefinition.FILE_NOT_FOUND);
  public static final IMessageType EC_FILE_TO_PARSE_NOT_FOUND = createError(ErrorDefinition.FILE_TO_PARSE_NOT_FOUND);
  public static final MessageType EC_FILE_TO_UPLOAD_NOT_FOUND = (ErrorMessageType) registry.createError("0067", "ecFileToUploadNotFound", "File not found when trying to upload dump file");
  public static final MessageType EC_FILLED_IN_FIELDS_NOT_CORRECT = (ErrorMessageType) registry.createError("0001", "ecFilledInFieldsNotCorrect", "The {0} is {1}. The following fields are mandatory: {2}. The following fields must be empty: {3}.");
  public static final MessageType EC_ILLEGAL_ACCESS_EXCEPTION_IN_REQUEST = (ErrorMessageType) registry.createError("0041", "ecIllegalAccessExceptionInRequest", "Request failed: IllegalAccessException when trying to create an instance of a request.");
  public static final IMessageType EC_IMPORT_LOCATION_NOT_SPECIFIED = createError(ErrorDefinition.IMPORT_LOCATION_NOT_SPECIFIED);
  public static final MessageType EC_INSTANTATION_EXCEPTION_IN_REQUEST = (ErrorMessageType) registry.createError("0042", "ecInstantationExceptionInRequest", "Request failed: InstantationException when trying to create an instance of a request.");
  public static final IMessageType EC_INTERNAL_ERROR = createError(ErrorDefinition.INTERNAL_ERROR);
  public static final MessageType EC_INVALID_REQUEST = (ErrorMessageType) registry.createError("0043", "ecInvalidRequest", "Request failed: combination of tasktype and filetype did not lead to a valid request.");
  public static final IMessageType EC_IO_ERROR_READING_ANALYZE_FILE = createError(ErrorDefinition.IO_ERROR_READING_ANALYZE_FILE);
  public static final MessageType EC_IS_DB_SERVER_ACTIVE = (ErrorMessageType) registry.createError("0061", "ecIsDbServerActive", "The database server, {0} is currently not active and can't be used");
  public static final MessageType EC_LOCATION_DOES_NOT_MATCH_LOCATION_DBSERVER = (ErrorMessageType) registry.createError("0034", "ecInvalidRequest", "PDM Location {0} does not match with PDM Location {1} of the linked database server {2}.");
  public static final MessageType EC_MANDATORYFIELD_NOT_FILLED = (ErrorMessageType) registry.createError("0075", "ecMandatoryFieldNotFilled", "The field {0} must contain a value");
  public static final IMessageType EC_MESSAGE_CONTENT_CANNOT_BE_HANDLED = createError(ErrorDefinition.MESSAGE_CONTENT_CANNOT_BE_HANDLED);
  public static final IMessageType EC_MULTIPLE_SCHEMAS_FOUND_IN_DUMP = createError(ErrorDefinition.MULTIPLE_SCHEMAS_FOUND_IN_DUMP);
  public static final IMessageType EC_NOT_MSSQL_DUMPFILE = createError(ErrorDefinition.NOT_MSSQL_DUMPFILE);
  public static final IMessageType EC_NOT_ORACLE_DATAPUMPFILE = createError(ErrorDefinition.NOT_ORACLE_DATAPUMPFILE);
  public static final IMessageType EC_NOT_ORACLE_DMP_DUMPFILE = createError(ErrorDefinition.NOT_ORACLE_DMP_DUMPFILE);
  public static final MessageType EC_NO_PDMAGENT_FOUND = (ErrorMessageType) registry.createError("0003", "ecNoPDMAgentFound", "There is no agent found that could execute this request.");
  public static final MessageType EC_NO_SPACE_ALLOWED = (ErrorMessageType) registry.createError("0062", "ecNoSpaceAllowed", "There is no Space allowed for field {0}.");
  public static final MessageType EC_NO_SUPPORTED_DATA_FILE_TYPES_ADDED = (ErrorMessageType) registry.createError("0063", "ecNoSupportedDataFileLocationsAdded", "There is no Supported data file location assigned.");
  public static final MessageType EC_NO_SUPPORTED_VERSION_ADDED = (ErrorMessageType) registry.createError("0060", "ecNoSupportedVersionAdded", "There is no Supported Version assigned.");
  public static final MessageType EC_ONLY_WORD_CHARS_ALLOWED = (ErrorMessageType) registry.createError("0029", "ecOnlyWordCharsAllowed", "For field {0} only letters, numbers and ''_'' are allowed");
  public static final MessageType EC_ONTOLOGY_EXCEPTION_IN_REQUEST = (ErrorMessageType) registry.createError("0040", "ecOntologyExceptionInRequest", "Request failed: an OntologyException occurred when handling the request.");
  public static final MessageType EC_REQUEST_STATE_IS_NOT_ALLOWED = (ErrorMessageType) registry.createError("0046", "ecRequestStateIsNotAllowed", "Request state cannot be changed to {0}, because the state of last task is {1}");
  public static final IMessageType EC_SQL_ERROR_ANALYZING_DB_DUMPFILE = createError(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE);
  public static final IMessageType EC_SQL_ERROR_CLOSING_CONNECTION_DB_SERVER = createError(ErrorDefinition.SQL_ERROR_CLOSING_CONNECTION_DB_SERVER);
  public static final IMessageType EC_SQL_ERROR_CONNECTING_TO_DB_SERVER = createError(ErrorDefinition.SQL_ERROR_CONNECTING_TO_DB_SERVER);
  public static final IMessageType EC_SQL_ERROR_CREATING_TABLESPACE = createError(ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);
  public static final IMessageType EC_SQL_ERROR_DELETING_DATABASE = createError(ErrorDefinition.SQL_ERROR_DELETING_DATABASE);
  public static final IMessageType EC_SQL_ERROR_DROPPING_DATABASE = createError(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
  public static final IMessageType EC_SQL_ERROR_IMPORTING_DB_DUMPFILE = createError(ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE);
  public static final IMessageType EC_TABLESPACE_TO_SMALL_FOR_DUMP = createError(ErrorDefinition.TABLESPACE_TO_SMALL_FOR_DUMP);
  public static final MessageType EC_TASK_STATE_IS_NOT_ALLOWED = (ErrorMessageType) registry.createError("0052", "ecTaskStateIsNotAllowed", "Task state cannot be changed to {0}, because the state of the linked request is {1}");
  public static final IMessageType EC_TIMEOUT_ERROR_ON_EXECUTING_DBTASK = createError(ErrorDefinition.TIMEOUT_ERROR_ON_EXECUTING_DBTASK);
  public static final MessageType EC_UNDETERMINED_REQUEST_STATE = (ErrorMessageType) registry.createError("0058", "ecUndeterminedRequestState", "The agent executing the request has died. The status of the request is undetermined.");
  public static final IMessageType EC_UNKNOWN_MESSAGE_CONTENT = createError(ErrorDefinition.UNKNOWN_MESSAGE_CONTENT);
  public static final IMessageType EC_USER_STILL_CONNECTED = createError(ErrorDefinition.USER_STILL_CONNECTED);
  public static final MessageType EC_WEBDAV_CONNECTION_ERROR = (ErrorMessageType) registry.createError("0005", "ecWebDAVConnectionError", "An error occurred while trying to connect to the WebDAV server.");
  public static final MessageType EC_WRONG_PORTNUMBER_FOR_DB_SERVER = (ErrorMessageType) registry.createError("0009", "ecWrongPortNumberForDbServer", "Wrong port number for database server");
  public static final MessageType EC_WRONG_USERNAME_PASSWORD_FOR_DB_SERVER = (ErrorMessageType) registry.createError("0008", "ecWrongUsernamePasswordForDbServer", "Wrong user name and/or password for database server");
  // last used: 75

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get message type by code
   *
   * @param  aCode
   *
   * @return IMessageType
   */
  public static MessageType getMessageTypeByCode(String aCode)
  {
    Set<MessageType> messageTypes = registry.getMessageTypes().keySet();
    Iterator iterator = messageTypes.iterator();
    while (iterator.hasNext())
    {
      MessageType messageType = (MessageType) iterator.next();
      if (messageType.getCode().equals(aCode))
      {
        return messageType;
      }
    }
    return null;
  }


  /**
   * {@inheritDoc}
   */
  @Override public Map<MessageType, String> getMessageTypes()
  {
    return registry.getMessageTypes();
  }


  /**
   * creates MessageType for specified Error Definition
   *
   * @param  aDefinition ErrorDefinition
   *
   * @return MessageType
   */
  private static MessageType createError(ErrorDefinition aDefinition)
  {
    assert ErrorDefinition.getPrefix().equals(PREFIX) : "prefixes must match";
    return (MessageType) registry.createError(aDefinition.getShortCode(), aDefinition.getID(), aDefinition.getDefaultMessage());
  }
}
