// Planon Enterprise Edition Source file: BOTypePDMP5Module.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.common.type;

import nl.planon.hades.businessmodel.*;

/**
 * BOTypePDMP5Module
 */
public class BOTypePDMP5Module
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IBOType BASE_PDM_DATABASE_SERVER = new BOType("BasePDMDatabaseServer");
  public static final IBOType BASE_PDM_DATABASE_VERSION = new BOType("BasePDMDatabaseVersion");
  public static final IBOType BASE_PDM_REQUEST = new BOType("BasePDMRequest");
  public static final IBOType BASE_PDM_TASK = new BOType("BasePDMTask");
  public static final IBOType PDM_ANALYZE = new BOType("PDMAnalyze");
  public static final IBOType PDM_COMPATIBILITY_MATRIX = new BOType("PDMCompatibilityMatrix");
  public static final IBOType PDM_DATABASE = new BOType("PDMDatabase");
  public static final IBOType PDM_DATABASE_FILE_TYPE = new BOType("PDMDatabaseFileType");
  public static final IBOType PDM_DATABASE_SERVER_MSSQL = new BOType("PDMDatabaseServerMSSQL");
  public static final IBOType PDM_DATABASE_SERVER_ORACLE = new BOType("PDMDatabaseServerOracle");
  public static final IBOType PDM_DATABASE_STATE = new BOType("PDMDatabaseState");
  public static final IBOType PDM_DATABASE_TYPE = new BOType("PDMDatabaseType");
  public static final IBOType PDM_DB_TYPE_FILE_TYPE = new BOType("PDMDbTypeFileType");
  public static final IBOType PDM_DATABASE_PRIVILEGE_TYPE = new BOType("PDMDatabasePrivilegeType");
  public static final IBOType PDM_DATABASE_VERSION_MSSQL = new BOType("PDMDatabaseVersionMSSQL");
  public static final IBOType PDM_DATABASE_VERSION_ORACLE = new BOType("PDMDatabaseVersionOracle");
  public static final IBOType PDM_DBSERVER_DATAFILELOCATION = new BOType("PDMDbServerDataFileLocation");
  public static final IBOType PDM_LOCATION = new BOType("PDMLocation");
  public static final IBOType PDM_REQUEST_DELETE = new BOType("PDMRequestDelete");
  public static final IBOType PDM_REQUEST_EXPORT = new BOType("PDMRequestExport");
  public static final IBOType PDM_REQUEST_IMPORT = new BOType("PDMRequestImport");
  public static final IBOType PDM_REQUEST_RELOAD_EXPORT = new BOType("PDMRequestReloadExport");
  public static final IBOType PDM_REQUEST_RELOAD_INITIAL = new BOType("PDMRequestReloadInitial");
  public static final IBOType PDM_REQUEST_EXECUTE_STATE = new BOType("PDMRequestExecuteState");
  public static final IBOType PDM_REQUEST_TYPE = new BOType("PDMRequestType");
  public static final IBOType PDM_TASK_ANALYZE_IMPORT = new BOType("PDMTaskAnalyzeImport");
  public static final IBOType PDM_TASK_EXECUTE_STATE = new BOType("PDMTaskExecuteState");
  public static final IBOType PDM_TASK_DELETE = new BOType("PDMTaskDelete");
  public static final IBOType PDM_TASK_EXPORT = new BOType("PDMTaskExport");
  public static final IBOType PDM_TASK_IMPORT = new BOType("PDMTaskImport");
}
