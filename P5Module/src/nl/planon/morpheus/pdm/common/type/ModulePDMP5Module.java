// Planon Enterprise Edition Source file: ModulePDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.common.type;

import nl.planon.hades.module.*;

/**
 * ModulePDMP5Module
 */
public class ModulePDMP5Module
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  /** Unique name for this OSGi module. */
  public static final String NAME = "PDMP5Module";

  /**
   * Prefix for this module.
   *
   * <p>You must pick a unique prefix for your module that:</p>
   *
   * <ul>
   *   <li>Is exactly four characters long.</li>
   *   <li>Consists of only uppercase alphabetic letters (no digits).</li>
   *   <li>Has a relation to the module name.</li>
   *   <li>Is not used already by some other module (case insensitive).</li>
   * </ul>
   *
   * <p>In the future, this module prefix will be used in combination with other entities that have
   * to be unique across the system (such as database table names) and it's uniqueness will be
   * validated once the module starts.</p>
   */
  public static final String PREFIX = "PDMS";

  // Create module
  public static final IModuleType PDM_P5_MODULE = new ModuleType(NAME);

  // Create entries - In the platform, entries start with uppercase and don't
  // have a suffix
  public static final IEntryType PDM_ADMIN_MODULE_ENTRY = new EntryType("PDMAdminModuleEntry", EntryType.getBasicEntryLicenseCode());

  /* ------------------------- */
  /* ENTRY PDM ADMIN           */
  /* ------------------------- */
  // Create levels
  public static final IDrilldownLevelType PDM_ADMIN_MODULE_DATABASE_VERSIONS_LEVEL = new DrilldownLevelType("pdmAdminModuleDatabaseVersionsLevel");
  public static final IDrilldownLevelType PDM_ADMIN_MODULE_DATABASE_SERVERS_LEVEL = new DrilldownLevelType("pdmAdminModuleDatabaseServersLevel");
  public static final IDrilldownLevelType PDM_ADMIN_MODULE_DATABASES_LEVEL = new DrilldownLevelType("pdmAdminModuleDatabasesLevel");
  public static final IDrilldownLevelType PDM_ADMIN_MODULE_REQUESTS_LEVEL = new DrilldownLevelType("pdmAdminModuleRequestsLevel");
  public static final IDrilldownLevelType PDM_ADMIN_MODULE_PDM_TASKS_LEVEL = new DrilldownLevelType("pdmAdminModulePDMTasksLevel");

  // Create PDM Database Versions level steps
  public static final IDrilldownStepType PDM_ADMIN_MODULE_DATABASE_VERSIONS_STEP = new DrilldownStepType("pdmAdminModuleDatabaseVersionsStep");
  public static final IDrilldownStepType PDM_ADMIN_MODULE_PDM_LOCATIONS_STEP = new DrilldownStepType("pdmAdminModulePDMLocationsStep");

  // Create PDM Database Servers level steps
  public static final IDrilldownStepType PDM_ADMIN_MODULE_DATABASE_SERVERS_STEP = new DrilldownStepType("pdmAdminModuleDatabaseServersStep");

  // Create PDM Databases level steps
  public static final IDrilldownStepType PDM_ADMIN_MODULE_DATABASES_STEP = new DrilldownStepType("pdmAdminModuleDatabasesStep");
  public static final IDrilldownStepType PDM_ADMIN_MODULE_DBSERVER_DATAFILELOC_STEP = new DrilldownStepType("pdmAdminModuleDbServerDataFileLocStep");
  public static final IDrilldownStepType PDM_ADMIN_MODULE_DBSERVER_LOG_STEP = new DrilldownStepType("pdmAdminModuleDbServerLogStep");

  // Create PDM Request level steps
  public static final IDrilldownStepType PDM_ADMIN_MODULE_REQUESTS_STEP = new DrilldownStepType("pdmAdminModuleRequestsStep");

  // Create PDM Database Tasks level steps
  public static final IDrilldownStepType PDM_ADMIN_MODULE_PDM_TASKS_STEP = new DrilldownStepType("pdmAdminModulePDMTasksStep");
  public static final IDrilldownStepType PDM_ADMIN_MODULE_REQUEST_LOG_STEP = new DrilldownStepType("pdmAdminModuleRequestLogStep");
}
