// Planon Enterprise Edition Source file: MtoNAssociationTypePDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.common.type;

import nl.planon.hades.businessmodel.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * MtoNAssociationTypePDMP5Module
 */
public class MtoNAssociationTypePDMP5Module
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final IMtoNAssociationType PN_PDMDBSERVER_MTON_PDMDBVERSION = new MtoNAssociationType("PDMDatabaseServer_PDMDatabaseVersion", BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF, BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION, IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
  public static final IMtoNAssociationType PN_PDMDBVERSION_MTON_PDMDBSERVER = new MtoNAssociationType("PDMDatabaseVersion_PDMDatabaseServer", BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION, IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF, BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
}
