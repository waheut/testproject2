// Planon Enterprise Edition Source file: PDMP5ModuleResourceBundle.java
// Copyright Planon 1997-2014. All Rights Reserved.
package nl.planon.morpheus.pdm.common.resourcebundle;

import java.util.*;

import nl.planon.hades.osgi.integration.*;

/**
 * PDMP5ModuleResourceBundle
 */
public class PDMP5ModuleResourceBundle extends PlanonBundleResource
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Translation key prefix for this bundle. Should end with a dot (".")!
  public static final String TRANSKEY_PREFIX = "rPDMP5Module.";

  // Array of keys (see constants above) and their default translation (in
  // Planon English). Keys are case sensitive.
  private static final String[][] KEYS = {
    // NOTE: Keep alphabetically ordered.
  };

  // is this the correct way to register the content resources
  private static final String[][] CONTENTS = {};

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public Map<String, String> getResources()
  {
    Map<String, String> result = new HashMap<String, String>();
    addResource(result, KEYS);
    addResource(result, CONTENTS);
    return result;
  }
}
