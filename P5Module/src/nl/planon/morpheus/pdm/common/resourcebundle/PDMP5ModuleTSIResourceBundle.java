// Planon Enterprise Edition Source file: PDMP5ModuleTSIResourceBundle.java
// Copyright Planon 1997-2014. All Rights Reserved.
package nl.planon.morpheus.pdm.common.resourcebundle;

import java.util.*;

import nl.planon.apollo.businessmodel.tsitab.*;
import nl.planon.apollo.tsieditors.editors.*;

import nl.planon.hades.osgi.integration.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMP5ModuleTSIResourceBundle
 */
public class PDMP5ModuleTSIResourceBundle extends PlanonBundleResource implements IPlanonBundleTSIResourceService
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // TSI Bar translations
  // Array of keys and their default translation (in Planon English). Keys are
  // case sensitive.
  private static final String[][] TSIBARS = { // Syntax should be:
    // {BOTSIBar.createTranskey(BOTypePDMP5ModuleModule.PDM_DATABASE_SERVER,
    // "PBALK001"), TSIEditorDefault.TABNAME_GENERAL},
  };

  // TSI Step Group translations
  // Array of keys and their default translation (in Planon English). Keys are
  // case sensitive.
  private static final String[][] TSISTEPGROUPS = { // Syntax should be:
    // {BOTSIDrilldownStepGroup.createTranskey(DrilldownStepGroupTypePDMP5Module.PN_XXXXXXX),
    // "DefaultTranslation"},
  };

  // TSI Tabs translations
  // Array of keys and their default translation (in Planon English). Keys are
  // case sensitive.
  private static final String[][] TSITABS =
  {
    {BOTSITab.createTranskey(BOTypePDMP5Module.BASE_PDM_REQUEST, TSIEditor.TABNAME_GENERAL), TSIEditor.TABNAME_GENERAL},
    {BOTSITab.createTranskey(BOTypePDMP5Module.BASE_PDM_TASK, TSIEditor.TABNAME_GENERAL), TSIEditor.TABNAME_GENERAL},
    {BOTSITab.createTranskey(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, TSIEditor.TABNAME_GENERAL), TSIEditor.TABNAME_GENERAL},
    {BOTSITab.createTranskey(BOTypePDMP5Module.PDM_DATABASE, TSIEditor.TABNAME_GENERAL), TSIEditor.TABNAME_GENERAL},
    {BOTSITab.createTranskey(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, TSIEditor.TABNAME_GENERAL), TSIEditor.TABNAME_GENERAL},
    {BOTSITab.createTranskey(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION, TSIEditor.TABNAME_GENERAL), TSIEditor.TABNAME_GENERAL},
    {BOTSITab.createTranskey(BOTypePDMP5Module.PDM_LOCATION, TSIEditor.TABNAME_GENERAL), TSIEditor.TABNAME_GENERAL}
  };

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public Map<String, String> getResources()
  {
    Map<String, String> result = new HashMap<String, String>();

    addResource(result, TSIBARS);
    addResource(result, TSITABS);
    addResource(result, TSISTEPGROUPS);

    return result;
  }
}
