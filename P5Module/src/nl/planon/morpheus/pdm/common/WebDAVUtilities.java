// Planon Enterprise Edition Source file: WebDAVUtilities.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.common;

import org.apache.commons.httpclient.*;

import java.io.*;
import java.net.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.util.pnlogging.*;
import nl.planon.util.webdav.*;

/**
 * WebDAV Utility class
 */
public final class WebDAVUtilities
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String DEFAULT_DB_EXTENSION = "db";

  private static WebDAVUtilities instance;
  private static final PnLogger LOG = PnLogger.getLogger(WebDAVUtilities.class, PnLogCategory.DEFAULT);
  public static final String DUMPFILE_FOR_IMPORT = "_Import";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new WebDAVUtilities object.
   */
  private WebDAVUtilities()
  {
    //
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get a instance of this class
   *
   * @return WebDAVUtilities
   */
  public static WebDAVUtilities getInstance()
  {
    if (instance == null)
    {
      instance = new WebDAVUtilities();
    }
    return instance;
  }


  /**
   * upload a database dumpfile to a specific location via WebDAV and accordingly update the bo
   *
   * @param  aBOValue         the bovalue of the pdm database
   * @param  aDestinationHost destination to where the dumpfile must be copied to
   *
   * @return messagetype if something went wrong, null if all went well
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public MessageType doUploadPDMDumpFile(IBOValue aBOValue, String aDestinationHost) throws PnErrorListException, AuthorizationException
  {
    assert BOTypePDMP5Module.PDM_DATABASE.equals(aBOValue.getBOType()) : "argument bovalue must be of type PDM_DATABASE";
    MessageType result = null;
    String dumpFileName = generateDumpFileName(aBOValue);

    try
    {
      PnWebDAVResource resource = new PnWebDAVResource(new HttpURL(aDestinationHost));
      if (resource != null)
      {
        // First delete the old dump file (if any). In case the extension is different it would not be overwritten.
        if (!aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).isEmpty())
        {
          resource.deleteMethod(aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).getAsString());
        }
        // since there will be a new dumpfile, the fields owner in dump and tablespace in dump are cleared.
        aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_OBJECT_OWNER_IN_DUMP).clear();
        aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_TABLESPACENAMES_IN_DUMP).clear();

        // PCIS 206448: error handling when upload fails
        String pathName = resource.getPath() + "/" + dumpFileName;
        String fileName = aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DUMPFILE).getAsString();
        if (resource.putMethod(pathName, new File(fileName)))
        {
          aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).setAsBaseValue(new BaseStringValue(aDestinationHost + dumpFileName));
        }
        else
        {
          result = ErrorNumberPDMP5Module.EC_FILEUPLOAD_FAILED;
        }
      }
    }
    catch (ConnectException connectException)
    {
      // PCIS 197787: Show error message when WebDAV server is not reacting due to ConnectException
      result = ErrorNumberPDMP5Module.EC_WEBDAV_CONNECTION_ERROR;
    }
    catch (URIException uriException)
    {
      // PCIS 199263: Show error message when WebDAV server is not reacting due to URIException
      result = ErrorNumberPDMP5Module.EC_WEBDAV_CONNECTION_ERROR;
    }
    catch (FileNotFoundException ex)
    {
      // PCIS 199263: Show error message when WebDAV server is not reacting due to IOException
      result = ErrorNumberPDMP5Module.EC_FILE_TO_UPLOAD_NOT_FOUND;
    }
    catch (IOException ex)
    {
      // PCIS 199263: Show error message when WebDAV server is not reacting due to IOException
      result = ErrorNumberPDMP5Module.EC_WEBDAV_CONNECTION_ERROR;
    }
    return result;
  }


  /**
   * download a database dumpfile to a specific local location via WebDAV
   *
   * @param  aDumpFile         name of dumpfile to download
   * @param  aLocalDestination destination to where the dumpfile must be downloaded
   *
   * @throws HttpException
   * @throws IOException
   */
  public void downloadPDMDumpFile(String aDumpFile, String aLocalDestination) throws HttpException, IOException
  {
    File localFile = null;
    HttpURL httpURL = new HttpURL(aDumpFile);
    PnWebDAVResource resource = new PnWebDAVResource(httpURL);
    if (resource != null)
    {
      localFile = new File(aLocalDestination);

      if (!resource.getMethod(localFile))
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("  !!! Error: " + resource.getStatusCode() + " - " + resource.getStatusMessage());
        }
      }
    }
  }


  /**
   * A name for the dumpfile is generated. Every dumpfile should be uniquely identified (PCIS 187713
   * also PBL # 20933).
   *
   * <p>In case of importing: {@literal <}PDMDatabase.AccountDatabaseOwnerRef{@literal >}_{@literal
   * <}PDMDatabase.PrimaryKey{@literal >}_Import.{@literal <}filetype{@literal >} whereby the file
   * extension should be the same as the original dumpfile.</p>
   *
   * @param  aBOValue BOValue of the PDMDatabase
   *
   * @return the generated dump file name
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public String generateDumpFileName(IBOValue aBOValue) throws PnErrorListException, AuthorizationException
  {
    // PBL # 20933, PDM - Extend uploaded filename with Ownername for easy identification
    // So, prepend with the AccountDatabaseOwner.
    String ownerLookUpValue = aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_ACCOUNT_DATABASEOWNER_REF).getLookupValue().getAsString();
    String dumpFileName = ownerLookUpValue + "_" + aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PRIMARYKEY).getAsString() + DUMPFILE_FOR_IMPORT;

    String localFileName = aBOValue.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DUMPFILE).getAsString();
    // use the same extension, if any
    int indexOfExtension = localFileName.lastIndexOf(".");
    if (indexOfExtension > -1)
    {
      dumpFileName = dumpFileName + localFileName.substring(indexOfExtension, localFileName.length());
    }
    else
    {
      // fix extension, because low level oracle analysys package cannot deal with dumps without an extension
      dumpFileName = dumpFileName + '.' + DEFAULT_DB_EXTENSION;
    }
    return dumpFileName;
  }


  /**
   * Check if file server is available by trying to set up a connection
   *
   * @param  aDestinationHost location of file server
   *
   * @return true is file server is available, false if not.
   */
  public boolean isFileServerAvailable(String aDestinationHost)
  {
    boolean result = false;
    try
    {
      PnWebDAVResource resource = new PnWebDAVResource(new HttpURL(aDestinationHost));
      result = (resource != null);
    }
    catch (Exception ex)
    {
      result = false;
    }
    return result;
  }


  /**
   * convenience method for the PDM command line client to return a translated error message instead
   * of the MessageType
   *
   * @return String translated error message if something went wrong, null if all went well
   *
   * @see    doUploadPDMDumpFile
   */
  public String uploadPDMDumpFile(IBOValue aBOValue, String aDestinationHost) throws PnErrorListException, AuthorizationException
  {
    String result = null;
    MessageType messageType = doUploadPDMDumpFile(aBOValue, aDestinationHost);
    if (messageType != null)
    {
      result = messageType.getLocalizebleKey();
    }
    return result;
  }
}
