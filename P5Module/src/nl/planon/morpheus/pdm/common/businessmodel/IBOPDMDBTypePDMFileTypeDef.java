// Planon Enterprise Edition Source file: IBOPDMDBTypePDMFileTypeDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.common.*;

/**
 * IBOPDMDBTypePDMFileType
 *
 * @version $Revision$
 */
public interface IBOPDMDBTypePDMFileTypeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_PRIMARYKEY = PnFieldName.PRIMARYKEY;
  public static final String PN_SYSUPDATECOUNT = PnFieldName.SYSUPDATECOUNT;
  public static final String PN_DATABASE_TYPE_REF = "DatabaseTypeRef";
  public static final String PN_FILE_TYPE_REF = "FileTypeRef";
  public static final String PN_EXPORT_FILE_TYPE_REF = "ExportFileType";
}
