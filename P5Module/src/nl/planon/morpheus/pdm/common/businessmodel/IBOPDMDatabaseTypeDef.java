// Planon Enterprise Edition Source file: IBOPDMDatabaseTypeDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessmodel.systemcode.*;

/**
 * IBOPDMDatabaseTypeDef
 *
 * @version $Revision$
 */
public interface IBOPDMDatabaseTypeDef extends IBOSystemCodeDef
{
}
