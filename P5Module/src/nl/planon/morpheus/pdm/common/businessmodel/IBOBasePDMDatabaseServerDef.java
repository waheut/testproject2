// Planon Enterprise Edition Source file: IBOBasePDMDatabaseServerDef.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.common.*;

/**
 * IBOPDMDatabaseServerDef
 *
 * @version $Revision$
 */
public interface IBOBasePDMDatabaseServerDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_CODE = PnFieldName.CODE;
  public static final String PN_CONNECTION_URL = "ConnectionURL";
  public static final String PN_PRIMARYKEY = PnFieldName.PRIMARYKEY;
  public static final String PN_SYSACCOUNT_REF = PnFieldName.SYSACCOUNT_REF;
  public static final String PN_SYSCHANGEACCOUNT_REF = PnFieldName.SYSCHANGEACCOUNT_REF;
  public static final String PN_SYSCHANGEDATETIME = PnFieldName.SYSCHANGEDATETIME;
  public static final String PN_SYSDATASECTION_REF = PnFieldName.SYSDATASECTION_REF;
  public static final String PN_SYSINSERTDATETIME = PnFieldName.SYSINSERTDATETIME;
  public static final String PN_SYSUPDATECOUNT = PnFieldName.SYSUPDATECOUNT;

  public static final String PN_NAME = PnFieldName.DESCRIPTION;
  public static final String PN_DATABASEVERSION_REF = "DatabaseVersionRef";
  public static final String PN_ADMIN_USER = "AdminUser";
  public static final String PN_ADMIN_PASS = "AdminPass";
  public static final String PN_CODE_PAGE__NOT_IN_ALL_SUBTYPES = "CodePage";
  public static final String PN_SORT_ORDER__NOT_IN_ALL_SUBTYPES = "SortOrder";
  public static final String PN_CONNECT_TNS_NAME__NOT_IN_ALL_SUBTYPES = "ConnectTNSName";
  public static final String PN_HOST_NAME = "HostName";
  public static final String PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES = "PortNumber";
  public static final String PN_DATABASE_INSTANCENAME = "DatabaseInstanceName";
  public static final String PN_TEMPORARY_TABLESPACENAME__NOT_IN_ALL_SUBTYPES = "TemporaryTableSpaceName";
  //
  public static final String PN_EXPORT_LOCATION__NOT_IN_ALL_SUBTYPES = "ExpLocation";
  public static final String PN_IMPORT_LOCATION__NOT_IN_ALL_SUBTYPES = "ImpLocation";
  //
  public static final String PN_FILE_TYPE_REF = "FileTypeRef";
  public static final String PN_IS_ACTIVE = "IsActive";
  public static final String PN_PDMLOCATION_REF = "PDMLocationRef";
  public static final String PN_BUSINESSOBJECTDEFINITION_REF = "BusinessObjectDefinitionRef";

  public static final String PN_FREEINT1 = PnFieldName.FREEINT1;
  public static final String PN_FREEINT2 = PnFieldName.FREEINT2;
  public static final String PN_FREEINT3 = PnFieldName.FREEINT3;
  public static final String PN_FREEINT4 = PnFieldName.FREEINT4;
  public static final String PN_FREEINT5 = PnFieldName.FREEINT5;

  public static final String PN_FREESTR1 = PnFieldName.FREESTR1;
  public static final String PN_FREESTR2 = PnFieldName.FREESTR2;
  public static final String PN_FREESTR3 = PnFieldName.FREESTR3;
  public static final String PN_FREESTR4 = PnFieldName.FREESTR4;
  public static final String PN_FREESTR5 = PnFieldName.FREESTR5;

  // extra gui field to show DB Versions linked to selected DB Server
  public static final String PN_PDMDBVERSIONDETAIL = "PDMDBVersionDetail";
  // extra gui field to show DB file location linked to selected DB Server
  public static final String PN_PDMDBFILELOCATIONDETAIL = "PDMDBFileLocationDetail";

  // BOMs
  public static final String PN_BOM_ADD = PnBOMName.PN_BOM_ADD;
  public static final String PN_BOM_COPY = PnBOMName.PN_BOM_COPY;
  public static final String PN_BOM_DELETE = PnBOMName.PN_BOM_DELETE;
  public static final String PN_BOM_READ = PnBOMName.PN_BOM_READ;
  public static final String PN_BOM_SAVE = PnBOMName.PN_BOM_SAVE;
  public static final String PN_BOM_LINK_PDMDBVERSION = "PDMDatabaseServer_PDMDatabaseVersion";
  public static final String PN_BOM_SEND_MAIL = "BomSendMail";
}
