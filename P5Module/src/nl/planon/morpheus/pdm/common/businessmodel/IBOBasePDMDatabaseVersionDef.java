// Planon Enterprise Edition Source file: IBOBasePDMDatabaseVersionDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.common.*;

/**
 * IBOPDMDatabaseVersionDef
 *
 * @version $Revision$
 */
public interface IBOBasePDMDatabaseVersionDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_PRIMARYKEY = PnFieldName.PRIMARYKEY;
  public static final String PN_SYSDATASECTION_REF = PnFieldName.SYSDATASECTION_REF;
  public static final String PN_SYSUPDATECOUNT = PnFieldName.SYSUPDATECOUNT;
  public static final String PN_SYSCHANGEDATETIME = PnFieldName.SYSCHANGEDATETIME;
  public static final String PN_SYSINSERTDATETIME = PnFieldName.SYSINSERTDATETIME;
  public static final String PN_SYSACCOUNT_REF = PnFieldName.SYSACCOUNT_REF;
  public static final String PN_SYSCHANGEACCOUNT_REF = PnFieldName.SYSCHANGEACCOUNT_REF;
  public static final String PN_CODE = "Version";
  public static final String PN_NAME = PnFieldName.DESCRIPTION;
  public static final String PN_BUSINESSOBJECTDEFINITION_REF = "BusinessObjectDefinitionRef";

  // extra gui field to show DB Servers linked to selected DB Version
  public static final String PN_PDMDBSERVERDETAIL = "PDMDBServerDetail";

  // BOMs
  public static final String PN_BOM_ADD = PnBOMName.PN_BOM_ADD;
  public static final String PN_BOM_COPY = PnBOMName.PN_BOM_COPY;
  public static final String PN_BOM_DELETE = PnBOMName.PN_BOM_DELETE;
  public static final String PN_BOM_READ = PnBOMName.PN_BOM_READ;
  public static final String PN_BOM_SAVE = PnBOMName.PN_BOM_SAVE;
  public static final String PN_BOM_LINK_PDMDBSERVER = "PDMDatabaseVersion_PDMDatabaseServer";
}
