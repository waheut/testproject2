// Planon Enterprise Edition Source file: IBOPDMDatabaseDef.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.common.*;

import nl.planon.pdm.core.common.*;

/**
 * IBOPDMDatabaseDef
 *
 * @version $Revision$
 */
public interface IBOPDMDatabaseDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_PRIMARYKEY = PnFieldName.PRIMARYKEY;
  public static final String PN_SYSDATASECTION_REF = PnFieldName.SYSDATASECTION_REF;
  public static final String PN_SYSUPDATECOUNT = PnFieldName.SYSUPDATECOUNT;
  public static final String PN_SYSCHANGEDATETIME = PnFieldName.SYSCHANGEDATETIME;
  public static final String PN_SYSINSERTDATETIME = PnFieldName.SYSINSERTDATETIME;
  public static final String PN_SYSACCOUNT_REF = PnFieldName.SYSACCOUNT_REF;
  public static final String PN_SYSCHANGEACCOUNT_REF = PnFieldName.SYSCHANGEACCOUNT_REF;
  public static final String PN_NAME = PnFieldName.NAME;

  public static final String PN_ACCESSCODE = "AccessCode";
  public static final String PN_REF_BOSTATE = PnFieldName.PN_REF_BOSTATE_SYSTEM;
  public static final String PN_DUMPFILE = "DumpFile";
  public static final String PN_DATABASESERVER_REF = "DatabaseServerRef";
  public static final String PN_ACCOUNT_DATABASEOWNER_REF = "AccountDatabaseOwnerRef";
  public static final String PN_DESCRIPTION = PnFieldName.DESCRIPTION;
  public static final String PN_OBJECT_OWNER_IN_DUMP = "ObjectOwnerInDump";
  public static final String PN_TABLESPACENAMES_IN_DUMP = "TableSpaceNamesInDump"; // contains comma separated list for at least 10 table space names
  public static final String PN_OPTION_NO_IMPORT = "OptionNoImport";
  public static final String PN_STOP_ON_FAILURE = "StopOnFailure";
  public static final String PN_PDM_DUMPFILE = "PDMDumpFile";
  public static final String PN_PDM_EXPORTFILE = "PDMExportFile";
  public static final String PN_SIZE_DATABASE = "SizeDatabase";
  public static final String PN_DATABASE_TYPE_REF = "DatabaseTypeRef";
  public static final String PN_FILE_TYPE_REF = "FileTypeRef";
  public static final String PN_EXPORT_FILE_TYPE = "ExportFileType";
  public static final String PN_PDMLOCATION_REF = "PDMLocationRef";
  public static final String PN_XML_DATASOURCE_EXAMPLE = "XMLDataSourceExample";
  public static final String PN_PLANON_DB_DETAILS = "PlanonDBDetailsFromDumpFile";
  public static final String PN_TASK_TIMEOUT = "PDMTaskTimeOut";
  public static final String PN_PRIVILEGE_TYPE = "PrivilegeType";
  public static final String PN_FREEINT1 = PnFieldName.FREEINT1;
  public static final String PN_FREEINT2 = PnFieldName.FREEINT2;
  public static final String PN_FREEINT3 = PnFieldName.FREEINT3;
  public static final String PN_FREEINT4 = PnFieldName.FREEINT4;
  public static final String PN_FREEINT5 = PnFieldName.FREEINT5;

  public static final String PN_FREESTR1 = PnFieldName.FREESTR1;
  public static final String PN_FREESTR2 = PnFieldName.FREESTR2;
  public static final String PN_FREESTR3 = PnFieldName.FREESTR3;
  public static final String PN_FREESTR4 = PnFieldName.FREESTR4;
  public static final String PN_FREESTR5 = PnFieldName.FREESTR5;

  // BOMs
  public static final String PN_BOM_ADD = PnBOMName.PN_BOM_ADD;
  public static final String PN_BOM_DELETE = PnBOMName.PN_BOM_DELETE;
  public static final String PN_BOM_COPY = PnBOMName.PN_BOM_COPY;
  public static final String PN_BOM_IMPORT_DATABASE = "BomImportDatabase";
  public static final String PN_BOM_EXPORT_DATABASE = "BomExportDatabase";
  public static final String PN_BOM_DELETE_DATABASE = "BomDeleteDatabase";
  public static final String PN_BOM_RELOAD_INITIAL_DATABASE = "BomReloadInitialDatabase";
  public static final String PN_BOM_RELOAD_EXPORTED_DATABASE = "BomReloadExportedDatabase";
  public static final String PN_BOM_DOWNLOAD_DUMPFILE = "BomDownloadDumpfile";
  public static final String PN_BOM_READ = PnBOMName.PN_BOM_READ;
  public static final String PN_BOM_SAVE = PnBOMName.PN_BOM_SAVE;

  // BOM arguments
  public static final String ARG_BOM_IMPORT = "ARG_BOM_IMPORT";

  // States
  public static final String STATE_INITIAL = PDMDatabaseState.INITIAL.getPnName();
  public static final String STATE_BUSY = PDMDatabaseState.BUSY.getPnName();
  public static final String STATE_AVAILABLE = PDMDatabaseState.AVAILABLE.getPnName();
}
