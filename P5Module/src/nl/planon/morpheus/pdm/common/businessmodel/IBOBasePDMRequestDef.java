// Planon Enterprise Edition Source file: IBOBasePDMRequestDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.common.*;
import nl.planon.pdm.core.common.*;

/**
 * IBOPDMRequestDef
 *
 * @version $Revision$
 */
public interface IBOBasePDMRequestDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_PRIMARYKEY = PnFieldName.PRIMARYKEY;
  public static final String PN_SYSDATASECTION_REF = PnFieldName.SYSDATASECTION_REF;
  public static final String PN_SYSUPDATECOUNT = PnFieldName.SYSUPDATECOUNT;
  public static final String PN_SYSCHANGEDATETIME = PnFieldName.SYSCHANGEDATETIME;
  public static final String PN_SYSINSERTDATETIME = PnFieldName.SYSINSERTDATETIME;
  public static final String PN_SYSACCOUNT_REF = PnFieldName.SYSACCOUNT_REF;
  public static final String PN_SYSCHANGEACCOUNT_REF = PnFieldName.SYSCHANGEACCOUNT_REF;

  public static final String PN_DATABASE_REF = "DatabaseRef";
  public static final String PN_ACCOUNT_REQUESTOR_REF = "AccountRequestorRef";
  public static final String PN_DATABASEVERSION_REF = "DatabaseVersionRef";
  public static final String PN_REQUESTTYPE_REF = "RequestTypeRef";
  public static final String PN_REF_BOSTATE = PnFieldName.PN_REF_BOSTATE_SYSTEM;

  // BOMs
  public static final String PN_BOM_READ = PnBOMName.PN_BOM_READ;
  public static final String PN_BOM_SAVE = PnBOMName.PN_BOM_SAVE;
  public static final String PN_BOM_UPDATE_DBVERSION_FIELD = "BomUpdateDBVersionField";
  public static final String PN_BOM_LOG_PDMREQUEST = "BomLogPDMRequest";
  public static final String PN_BOM_LOG_PDMDBSERVER = "BomLogPDMDbServer";

  // BOM arguments
  public static final String ARG_MESSAGE_INFORMATION = "ARG_INFORMATION_MESSAGE";
  public static final String ARG_SEVERITY_OF_LOG = "ARG_SEVERITY_OF_LOG";
  public static final String ARG_ERROR_INFORMATION = "ARG_MESSAGE_VARIABLES";

  // States
  public static final String STATE_INITIAL = PDMRequestState.INITIAL.getPnName();
  public static final String STATE_IN_PROGRESS = PDMRequestState.IN_PROGRESS.getPnName();
  public static final String STATE_OK = PDMRequestState.OK.getPnName();
  public static final String STATE_NOK = PDMRequestState.NOT_OK.getPnName();
}
