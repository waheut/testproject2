// Planon Enterprise Edition Source file: IBOBasePDMTaskDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.common.*;
import nl.planon.pdm.core.common.*;

/**
 * IBOPDMTaskDef
 *
 * @version $Revision$
 */
public interface IBOBasePDMTaskDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_PRIMARYKEY = PnFieldName.PRIMARYKEY;
  public static final String PN_SYSDATASECTION_REF = PnFieldName.SYSDATASECTION_REF;
  public static final String PN_SYSUPDATECOUNT = PnFieldName.SYSUPDATECOUNT;
  public static final String PN_SYSCHANGEDATETIME = PnFieldName.SYSCHANGEDATETIME;
  public static final String PN_SYSINSERTDATETIME = PnFieldName.SYSINSERTDATETIME;
  public static final String PN_SYSACCOUNT_REF = PnFieldName.SYSACCOUNT_REF;
  public static final String PN_SYSCHANGEACCOUNT_REF = PnFieldName.SYSCHANGEACCOUNT_REF;

  public static final String PN_SYSSYSTEMTYPE = PnFieldName.SYSSYSTEMTYPE;
  public static final String PN_PDMREQUEST_REF = "PDMRequestRef";
  public static final String PN_REF_BOSTATE = PnFieldName.PN_REF_BOSTATE_SYSTEM;

  // BOMs
  public static final String PN_BOM_READ = PnBOMName.PN_BOM_READ;
  public static final String PN_BOM_SAVE = PnBOMName.PN_BOM_SAVE;
  public static final String PN_BOM_ADD = PnBOMName.PN_BOM_ADD;

  // States
  public static final String STATE_INITIAL = PDMTaskState.INITIAL.getPnName();
  public static final String STATE_IN_PROGRESS = PDMTaskState.IN_PROGRESS.getPnName();
  public static final String STATE_OK = PDMTaskState.OK.getPnName();
  public static final String STATE_NOK = PDMTaskState.NOT_OK.getPnName();
}
