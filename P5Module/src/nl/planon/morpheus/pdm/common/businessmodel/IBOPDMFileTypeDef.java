// Planon Enterprise Edition Source file: IBOPDMFileTypeDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessmodel.systemcode.*;

/**
 * IBOPDMFileTypeDef
 *
 * @version $Revision$
 */
public interface IBOPDMFileTypeDef extends IBOSystemCodeDef
{
}
