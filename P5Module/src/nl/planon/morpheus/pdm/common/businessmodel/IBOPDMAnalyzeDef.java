// Planon Enterprise Edition Source file: IBOPDMAnalyzeDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.common.businessmodel;

import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.common.*;

/**
 * IBOPDMDBTypePDMFileType
 *
 * @version $Revision$
 */
public interface IBOPDMAnalyzeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

//boms
  public static final String PN_BOM_ADD = PnBOMName.PN_BOM_ADD;
  public static final String PN_BOM_SAVE = PnBOMName.PN_BOM_SAVE;
//
  public static final String PN_PDMREQUEST_REF = "PDMRequestRef";
  public static final String PN_FILE_TYPE_REF = "FileTypeRef";
  public static final String PN_SYSUPDATECOUNT = PnFieldName.SYSUPDATECOUNT;
  public static final String PN_PRIMARYKEY = PnFieldName.PRIMARYKEY;
}
