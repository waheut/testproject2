// Planon Enterprise Edition Source file: IApplicationLifecycleListener.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.osgi;

import org.osgi.framework.*;

/**
 * Allows objects to listen to the lifecycle of the Planon application server.
 *
 * <p>{@code activated} will be called when the bundle is informed that the appserver is running in
 * {@code NORMAL_MODE} and {@code deActivated} is called when the bundle (or the entire server) is
 * stopped.</p>
 *
 * @see AppServerInit.AppServerRunMode#NORMAL_MODE
 * @see ServerBundleActivator#registerLifecycleListener
 */
public interface IApplicationLifecycleListener
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Invoked when the module has become fully operational (appserver has entered the {@code
   * NORMAL_MODE} or after hot deploy).
   *
   * @param aContext BundleContext for the bundle the listening object is from, never {@code null}.
   */
  void activated(BundleContext aContext);


  /**
   * Invoked when the module is deactivated (stopped), either because just the bundle itself was
   * stopped, or because the whole server is being stopped.
   *
   * @param aContext BundleContext for the bundle the listening object is from, never {@code null}.
   */
  void deActivated(BundleContext aContext);
}
