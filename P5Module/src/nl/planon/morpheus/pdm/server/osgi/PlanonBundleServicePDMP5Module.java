// Planon Enterprise Edition Source file: PlanonBundleServicePDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.osgi;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.module.*;
import nl.planon.hades.osgi.integration.*;
import nl.planon.hades.upgrade.*;

import nl.planon.morpheus.pdm.server.factory.*;

/**
 * PlanonBundleServicePDMP5Module
 */
public class PlanonBundleServicePDMP5Module extends BasePlanonBundleService
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public ISystemModule create(IModuleType aModuleType, PnContext aPnContext) throws PnErrorListException
  {
    return ModuleFactoryPDMP5Module.getInstance().create(aModuleType, aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override public IBODefinition create(final IBOType aBOType, final IBODefinitionCache aCache, final IBOType aSystemBOType)
  {
    return BOTypeFactoryPDMP5Module.getInstance().create(aBOType, aCache, aSystemBOType);
  }


  /**
   * {@inheritDoc}
   */
  @Override public FieldDefinition createFieldDefinition(final FieldType aFieldType, final BODefinition aBODefinition, final IBOType aBOType)
  {
    return FieldTypeFactoryPDMP5Module.getInstance().createFieldDefinition(aFieldType, aBODefinition, aBOType);
  }


  /**
   * {@inheritDoc}
   */
  @Override public FieldDefinition createFreeFieldDefinition(final FieldType aFieldType, final FreeFieldConversionInfo aConversionInfo, final boolean aMustSucceed)
  {
    return FieldTypeFactoryPDMP5Module.getInstance().createFreeFieldDefinition(aFieldType, aConversionInfo, aMustSucceed);
  }


  /**
   * {@inheritDoc}
   */
  @Override public IBOType getBODefinitionExtensionTargetBOType(IBODefinitionExtensionType aBODefinitionExtensionType)
  {
    return BODefinitionExtensionFactoryPDMP5Module.getInstance().getTargetBOType(aBODefinitionExtensionType);
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IBODefinitionExtensionType> getBODefinitionExtensionTypes()
  {
    return BODefinitionExtensionFactoryPDMP5Module.getInstance().getBODefinitionExtensionTypes();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IBOType> getBOTypes()
  {
    return BOTypeFactoryPDMP5Module.getInstance().getBOTypes();
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getDescription()
  {
    return "PDM P5 Module";
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IDrilldownLevelType> getDrilldownLevelTypes()
  {
    return ModuleFactoryPDMP5Module.getInstance().getLevels();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IDrilldownStepType> getDrilldownStepTypes()
  {
    return ModuleFactoryPDMP5Module.getInstance().getSteps();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IEntryType> getEntryTypes()
  {
    return ModuleFactoryPDMP5Module.getInstance().getEntries();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<FieldType> getFieldTypes()
  {
    return FieldTypeFactoryPDMP5Module.getInstance().getFieldTypes();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IGUIDMetadata> getGUIDs()
  {
    return GUIDRegistryPDMP5Module.getInstance().getGuids();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IModuleType> getModuleTypes()
  {
    return ModuleFactoryPDMP5Module.getInstance().getModules();
  }


  /**
   * {@inheritDoc}
   */
  @Override public Set<IMtoNAssociationType> getMToNAssociationTypes()
  {
    return MtoNAssociationTypeFactoryPDMP5Module.getInstance().getMtoNAssociationTypes();
  }


  /**
   * {@inheritDoc}
   */
  @Override public boolean validateFieldDefinition(final FieldType aFieldType)
  {
    return FieldTypeFactoryPDMP5Module.getInstance().validateFieldDefinition(aFieldType);
  }


  /**
   * {@inheritDoc}
   */
  @Override public boolean validateFreeFieldDefinition(final FieldType aFieldType)
  {
    return FieldTypeFactoryPDMP5Module.getInstance().validateFreeFieldDefinition(aFieldType);
  }
}
