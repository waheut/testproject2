// Planon Enterprise Edition Source file: ServerActivator.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.server.osgi;

import org.osgi.framework.*;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.osgi.integration.*;

import nl.planon.heracles.server.osgi.integration.*;

import nl.planon.morpheus.pdm.common.resourcebundle.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.factory.*;

import nl.planon.util.pnlogging.*;

/**
 * ServerActivator, does also activate controller agent OSGI module
 */
public class ServerActivator extends ServerBundleActivator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(ServerActivator.class, PnLogCategory.DEFAULT);

  //~ Classes --------------------------------------------------------------------------------------

  /**
   * required because ControllerAgent should be only triggered if appserver is up and running
   */
  class DelayedTriggerControllerAgent implements IAppServerLifecycleListener
  {
    /**
     * {@inheritDoc} activate controller agent
     */
    @Override public void activated(BundleContext aContext, PnContext aPnContext) throws PnErrorListException, AuthorizationException
    {
      IApplicationLifecycleListener service = getControllerAgentService();
      if (service != null)
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("found controller agent: going to trigger it to activate....");
        }
        BundleContext bundleContext = FrameworkUtil.getBundle(service.getClass()).getBundleContext();

        service.activated(bundleContext);
      }
    }


    /**
     * {@inheritDoc} de-activate controller agent
     */
    @Override public void deActivated(BundleContext aContext, PnContext aPnContext) throws PnErrorListException, AuthorizationException
    {
      IApplicationLifecycleListener service = getControllerAgentService();
      if (service != null)
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("found controller agent: going to trigger it to de-activate....");
        }
        BundleContext bundleContext = FrameworkUtil.getBundle(service.getClass()).getBundleContext();

        service.deActivated(bundleContext);
      }
    }


    /**
     * gets service for controller agent
     *
     * @return IApplicationLifecycleListener
     */
    private IApplicationLifecycleListener getControllerAgentService()
    {
      BundleContext bundleContext = getBundleContext();
      try
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("searching controller agent....");
        }
        ServiceReference[] serviceReferences = bundleContext.getServiceReferences(IApplicationLifecycleListener.class.getName(), null);
        if (serviceReferences != null)
        {
          for (ServiceReference serviceReference : serviceReferences)
          {
            return (IApplicationLifecycleListener) bundleContext.getService(serviceReference);
          }
        }
      }
      catch (InvalidSyntaxException ex)
      {
        // can never happen because of null filter (poor osgi interface)
      }
      LOG.error("Failed to activate/deactivate controller agent (not found)!");
      return null;
    }
  }

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ServerActivator object.
   */
  public ServerActivator()
  {
    super(ModulePDMP5Module.NAME);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void start()
  {
    //register the PVType factory
    registerService(IPlanonPVTypeFactoryService.class.getName(), new PVTypeFactoryPDMP5Module());

    registerPlanonBundleService(new PlanonBundleServicePDMP5Module(), null, null, null, null);

    //register translation resources
    registerTranslationResources();

    registerMessageTypes();

    // register upgrade service
    registerService(IPlanonBundleUpgradeService.class.getName(), new PlanonBundleUpgradeServicePDMP5Module());

    // register controller agent
    registerLifecycleListener(new DelayedTriggerControllerAgent());
  }


  /**
   * Registers messagetypes.
   */
  private void registerMessageTypes()
  {
    Properties properties = new Properties();
    properties.put(IPlanonBundleMessageTypeService.BUNDLENAME_FILTER, getDependencyManager().getBundleContext().getBundle().getSymbolicName());
    registerService(IPlanonBundleMessageTypeService.class.getName(), new ErrorNumberPDMP5Module(), properties);
  }


  /**
   * Registers translation resources.
   */
  private void registerTranslationResources()
  {
    Properties properties = new Properties();
    properties.put(IPlanonBundleResourceService.BUNDLENAME_FILTER, getDependencyManager().getBundleContext().getBundle().getSymbolicName());

    registerService(IPlanonBundleResourceService.class.getName(), new PDMP5ModuleResourceBundle(), properties);
    registerService(IPlanonBundleTSIResourceService.class.getName(), new PDMP5ModuleTSIResourceBundle(), properties);
  }
}
