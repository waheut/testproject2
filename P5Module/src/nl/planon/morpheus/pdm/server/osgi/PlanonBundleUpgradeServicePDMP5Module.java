// Planon Enterprise Edition Source file: PlanonBundleUpgradeServicePDMP5Module.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.osgi;

import nl.planon.heracles.server.osgi.integration.*;

import nl.planon.morpheus.pdm.common.type.*;


/**
 * is the upgrade service of PDM P5Module. This service delivers additional upgrade steps/scripts
 * which will be executed during the upgrade.
 */
public class PlanonBundleUpgradeServicePDMP5Module
    extends BasePlanonBundleUpgradeService {

    /**
     * Creates a new PlanonBundleUpgradeServicePDMP5Module object.
     */
    public PlanonBundleUpgradeServicePDMP5Module() {
        super(ModulePDMP5Module.PREFIX);
        addVersion(1, 0);
        addVersion(1, 1);
        addVersion(1, 2);
        addVersion(1, 3);
    }
}
