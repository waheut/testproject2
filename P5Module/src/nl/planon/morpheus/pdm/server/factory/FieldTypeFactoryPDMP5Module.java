// Planon Enterprise Edition Source file: FieldTypeFactoryPDMP5Module.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.factory;

import nl.planon.hades.guid.*;
import nl.planon.hades.osgi.integration.helper.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasestate.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.deleterequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.exportrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.importrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.reloadexportrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.reloadinitialrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequestexecutestate.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtaskexecutestate.*;

/**
 * FieldTypeFactoryPDMP5Module
 */
public class FieldTypeFactoryPDMP5Module extends BaseFieldTypeFactory
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static IFieldTypeFactory instance;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Retrieve the instance of the factory.
   *
   * @return FieldTypeFactoryPDMP5Module
   */
  public static synchronized IFieldTypeFactory getInstance()
  {
    if (instance == null)
    {
      instance = new FieldTypeFactoryPDMP5Module();
    }
    return instance;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void registerGUIDs(IGUIDRegistry aGUIDRegistry)
  {
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.BASE_PDM_DATABASE_SERVER, "CE54EE2C-9504-683B-8939-406B219598EB");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.BASE_PDM_DATABASE_VERSION, "96C78AA1-BA56-C768-4EE6-79EA0D3EBC8E");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.BASE_PDM_REQUEST, "DA01F7AF-BD9E-1FD2-E675-39739D6D260C");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE, "0E8815AA-88EA-60B5-CE2A-DBD39C405764");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_FILE_TYPE, "247B3BE6-A3FF-07A5-1594-DC51F156376D");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL, "D18E61DA-20F4-162E-ABC4-9A8DB93FC26C");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE, "319E988D-83A1-27FF-8653-76EDAA8E6534");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_STATE, "1BE6FB2A-D855-B0D9-DA6D-11437297B8B2");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_TYPE, "6476CB0A-E8F6-A4CD-1587-ECFDB822B9AB");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_PRIVILEGE_TYPE, "48FD35A9-CF3C-7AB5-FDAA-C74C3E9FCA4E");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL, "D311F6B3-A29A-1EE0-6199-C6ACAA4C4733");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE, "05C89DE2-DBB8-F7C7-7843-507ABF57084E");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_LOCATION, "F4AF2A9A-3A0C-3265-2323-1A4E9EE91DB5");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_REQUEST_DELETE, "F27C65BF-EA83-1628-8CC4-7B87026AFD8D");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_REQUEST_EXECUTE_STATE, "FBCE45EE-FF8A-B287-4A8E-B6A4CA6D6A66");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_REQUEST_EXPORT, "6EBB6B54-8DB1-881A-B08D-9920B32EA4E4");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_REQUEST_IMPORT, "964DEB9E-2238-95A1-64CF-5F7B5E2A9A41");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_REQUEST_RELOAD_EXPORT, "45B1A9EE-362D-E17D-DD51-4D4C8FB774BC");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL, "ABEF9ABC-A645-3569-967E-128D3AE26F70");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_REQUEST_TYPE, "C805E24E-8B21-EC2A-8FD0-771AA0D8C653");
    aGUIDRegistry.addGUIDForFieldType(FieldTypePDMP5Module.PDM_TASK_EXECUTE_STATE, "C13D292D-DEC5-2610-237A-60FFAE70BA13");
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void fillRegistry()
  {
    register(FieldTypePDMP5Module.BASE_PDM_DATABASE_SERVER, BasePDMDatabaseServerReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.BASE_PDM_DATABASE_VERSION, BasePDMDatabaseVersionReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.BASE_PDM_REQUEST, BasePDMRequestReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE, PDMDatabaseReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_FILE_TYPE, PDMFileTypeReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_PRIVILEGE_TYPE, PDMPrivilegeTypeReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL, PDMDatabaseServerMSSQLReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE, PDMDatabaseServerOracleReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_STATE, PDMDatabaseStateReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_TYPE, PDMDatabaseTypeReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL, PDMDatabaseVersionMSSQLReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE, PDMDatabaseVersionOracleReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_LOCATION, PDMLocationReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_REQUEST_DELETE, PDMRequestDeleteReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_REQUEST_EXECUTE_STATE, PDMRequestExecuteStateReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_REQUEST_EXPORT, PDMRequestExportReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_REQUEST_IMPORT, PDMRequestImportReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_REQUEST_RELOAD_EXPORT, PDMRequestReloadExportReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL, PDMRequestReloadInitialReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_REQUEST_TYPE, PDMRequestTypeReferenceFieldDefinition.class);
    register(FieldTypePDMP5Module.PDM_TASK_EXECUTE_STATE, PDMTaskExecuteStateReferenceFieldDefinition.class);
  }
}
