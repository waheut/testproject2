// Planon Enterprise Edition Source file: MtoNAssociationTypeFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.factory;

import nl.planon.hades.guid.*;
import nl.planon.hades.osgi.integration.helper.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * MtoNAssociationTypeFactoryPDMP5Module
 */
public class MtoNAssociationTypeFactoryPDMP5Module extends BaseMtoNAssociationTypeFactory
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static IMtoNAssociationTypeFactory instance;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MtoNAssociationTypeFactoryPDMP5Module object.
   */
  private MtoNAssociationTypeFactoryPDMP5Module()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Retrieve the instance of the factory.
   *
   * @return MtoNAssociationTypeFactoryPDMP5Module
   */

  public static synchronized IMtoNAssociationTypeFactory getInstance()
  {
    if (instance == null)
    {
      instance = new MtoNAssociationTypeFactoryPDMP5Module();
    }
    return instance;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void registerGUIDs(IGUIDRegistry aGUIDRegistry)
  {
    aGUIDRegistry.addGUIDForMtoNAssociationType(MtoNAssociationTypePDMP5Module.PN_PDMDBSERVER_MTON_PDMDBVERSION, "53E9DC3E-4236-5409-68EC-8210620CC700");
    aGUIDRegistry.addGUIDForMtoNAssociationType(MtoNAssociationTypePDMP5Module.PN_PDMDBVERSION_MTON_PDMDBSERVER, "56D77E26-44C3-E975-715C-E62F2193D2EC");
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void fillRegistry()
  {
    register(MtoNAssociationTypePDMP5Module.PN_PDMDBSERVER_MTON_PDMDBVERSION);
    register(MtoNAssociationTypePDMP5Module.PN_PDMDBVERSION_MTON_PDMDBSERVER);
  }
}
