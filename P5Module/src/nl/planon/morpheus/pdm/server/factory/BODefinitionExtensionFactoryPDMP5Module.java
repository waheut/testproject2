// Planon Enterprise Edition Source file: BODefinitionExtensionFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.factory;

import nl.planon.hades.osgi.integration.helper.*;

/**
 * BODefinitionExtensionFactoryPDMP5Module
 */
public class BODefinitionExtensionFactoryPDMP5Module extends BaseBODefinitionExtensionFactory
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static IBODefinitionExtensionFactory instance;

  //~ Constructors ---------------------------------------------------------------------------------

  // private static final IBODefinitionExtensionType EXAMPLE = new
  // BODefinitionExtensionType("Example");

  /**
   * Creates a new BODefinitionExtensionFactoryPDMP5Module object.
   */
  private BODefinitionExtensionFactoryPDMP5Module()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Retrieve the instance of the factory.
   *
   * @return IBODefinitionExtensionFactory
   */

  public static synchronized IBODefinitionExtensionFactory getInstance()
  {
    if (instance == null)
    {
      instance = new BODefinitionExtensionFactoryPDMP5Module();
    }
    return instance;
  }


  /**
   * Inject platform BO's with extensions from bundle
   */
  @Override protected void fillRegistry()
  {
    // register(EXAMPLE, BOTypeAphrodite.LEASE_CONTRACT,
    // BOContractDefExtension.class);
  }
}
