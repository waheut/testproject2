// Planon Enterprise Edition Source file: ModuleFactoryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.factory;

import nl.planon.hades.guid.*;
import nl.planon.hades.osgi.integration.helper.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.module.*;

/**
 * ModuleFactoryPDMP5Module
 */
public class ModuleFactoryPDMP5Module extends BaseModuleFactory
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static IModuleFactory instance;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ModuleFactoryPDMP5Module object.
   */
  private ModuleFactoryPDMP5Module()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Retrieve the instance of the factory.
   *
   * @return ModuleFactoryPDMP5Module
   */

  public static synchronized IModuleFactory getInstance()
  {
    if (instance == null)
    {
      instance = new ModuleFactoryPDMP5Module();
    }
    return instance;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void registerGUIDs(IGUIDRegistry aGUIDRegistry)
  {
    // Register module
    aGUIDRegistry.addGUIDForSysModule(ModulePDMP5Module.PDM_P5_MODULE, "F9E8F0CA-1A4C-899B-8479-5863CA11B129");

    // Register entries
    aGUIDRegistry.addGUIDForSysEntry(ModulePDMP5Module.PDM_ADMIN_MODULE_ENTRY, "26C94AF4-CC12-AE4D-0A2A-C43AE625FA2E");

    /* ------------------------- */
    /* ENTRY PDM Admin           */
    /* ------------------------- */
    // PDM Database Versions level
    aGUIDRegistry.addGUIDForSysLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_LEVEL, "E9F543B6-7E81-8A22-D111-3CE337E052E9");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_LEVEL, "BB2C6A2F-064D-2E54-8F15-9637C1CD1F34");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_LOCATIONS_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_LEVEL, "A52C9960-41D3-5CB4-86A4-4DCD11EB0480");

    // PDM Database Servers level
    aGUIDRegistry.addGUIDForSysLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_SERVERS_LEVEL, "D1C83951-FCC5-4EA7-F059-A3DCBA3B7D25");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_SERVERS_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_SERVERS_LEVEL, "1C19A966-552D-23CE-0E52-2A377AB26C09");

    // PDM databases level
    aGUIDRegistry.addGUIDForSysLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_LEVEL, "0A650920-7EC0-D3EC-0D74-5D66ABEAB5A9");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_LEVEL, "4D3F37AF-E6B0-2967-35C2-9AC8094EF0AE");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DBSERVER_DATAFILELOC_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_LEVEL, "0FBD4814-BAF0-6AFD-8FB3-5A090BDFF1A8");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DBSERVER_LOG_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_LEVEL, "D590CAC0-DC26-F0F7-2A09-4C19B9D0C6B2");

    // PDM Requests level
    aGUIDRegistry.addGUIDForSysLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_REQUESTS_LEVEL, "3CE47115-79D4-D28B-F757-B5D6D3259BD6");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_REQUESTS_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_REQUESTS_LEVEL, "20C115A3-E2C0-40F0-BE1A-41102E24493E");

    // PDM Tasks level
    aGUIDRegistry.addGUIDForSysLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_LEVEL, "4508BEF0-BF04-335C-014D-22BFBD19FBCC");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_LEVEL, "FCCEFE25-0D5A-B2DB-4F0D-0D041421666F");
    aGUIDRegistry.addGUIDForSysStep(ModulePDMP5Module.PDM_ADMIN_MODULE_REQUEST_LOG_STEP, ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_LEVEL, "0F52BC6E-8138-B93A-D152-1D94FA1ADB2B");
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void fillRegistry()
  {
    // Register module
    registerModule(ModulePDMP5Module.PDM_P5_MODULE, PDMP5Module.class);

    // Register entries
    registerEntry(ModulePDMP5Module.PDM_ADMIN_MODULE_ENTRY);

    /* ------------------------- */
    /* ENTRY PDM Admin Process   */
    /* ------------------------- */

    // PDM Database Versions level
    registerLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_LEVEL);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_STEP);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_LOCATIONS_STEP);

    // PDM Database Servers level
    registerLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_SERVERS_LEVEL);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_SERVERS_STEP);

    // PDM Databases level
    registerLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_LEVEL);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_STEP);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_DBSERVER_LOG_STEP);

    // PDM Request level
    registerLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_REQUESTS_LEVEL);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_REQUESTS_STEP);

    // PDM Tasks level
    registerLevel(ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_LEVEL);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_STEP);
    registerStep(ModulePDMP5Module.PDM_ADMIN_MODULE_REQUEST_LOG_STEP);
  }
}
