// Planon Enterprise Edition Source file: GUIDRegistryPDMP5Module.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.factory;

import nl.planon.hades.osgi.integration.helper.*;

/**
 * GUIDRegistryPDMP5Module
 */
public class GUIDRegistryPDMP5Module extends OSGIGUIDRegistry
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static IOSGIGuidRegistry instance;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new GUIDRegistryPDMP5Module object.
   */
  private GUIDRegistryPDMP5Module()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Retrieve the instance of the factory.
   *
   * @return IOSGIGuidRegistry
   */

  public static synchronized IOSGIGuidRegistry getInstance()
  {
    if (instance == null)
    {
      instance = new GUIDRegistryPDMP5Module();
    }
    return instance;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void fillRegistry()
  {
    BOTypeFactoryPDMP5Module.getInstance().registerGUIDs(this);
    ModuleFactoryPDMP5Module.getInstance().registerGUIDs(this);
    FieldTypeFactoryPDMP5Module.getInstance().registerGUIDs(this);
    MtoNAssociationTypeFactoryPDMP5Module.getInstance().registerGUIDs(this);
  }
}
