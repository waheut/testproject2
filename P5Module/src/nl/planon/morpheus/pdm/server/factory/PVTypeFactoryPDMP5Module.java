// Planon Enterprise Edition Source file: PVTypeFactoryPDMP5Module.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.factory;

import nl.planon.hades.database.*;
import nl.planon.hades.osgi.integration.helper.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmanalyze.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmlocation.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview.*;
import nl.planon.morpheus.pdm.server.module.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.server.module.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.module.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.module.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.server.module.pdmdbserverdatafilelocation.*;
import nl.planon.morpheus.pdm.server.module.pdmlocation.*;
import nl.planon.morpheus.pdm.server.module.pdmrequest.*;
import nl.planon.morpheus.pdm.server.module.pdmtask.*;

/**
 * PVTypeFactoryPDMP5Module
 */
public class PVTypeFactoryPDMP5Module extends BasePlanonPVTypeFactoryService
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillRegistry()
  {
    // !! Keep them alphabetic !!
    registerBundle(PVTypePDMP5Module.DATAFILELOCATIONS_OF_DBSERVER, DatafileLocationsOfDBServerViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDM_DATA_FILE_LOCATION_MUST_BE_FILLED, PDMDataFileLocationRelatedToDBServerViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDM_DATABASE_SERVER_SEARCH, PDMSearchForDatabaseServerProxyViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDM_SEARCH_FOR_HIGHEST_DB_SERVER_VERSION, PDMSearchForHighestDBServerVersionProxyViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDM_SUPPORTED_VERSION_MUST_BE_FILLED, PDMCompatibilitieMatrixRelatedToDBServerViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDM_SUPPORTED_VERSIONS_FROM_ONE_SERVER, PDMDBServerSupportedVersionsViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDM_TASK_PENDING, PDMPendingTaskViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMANALYZE, AnalyzeProxyViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMANALYZE_BY_PKREQUEST, AnalyzeGetByPKRequestProxyViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDATABASE, PDMDatabaseProxyViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDATABASETYPE, PDMDatabaseTypeViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDB_FILE_LOCATION_IN_CONTEXT_OF_PDMDBSERVER, PDMDBServerInContextOfPDMDBFileLocationViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBSERVER, PDMDBServerViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBSERVER_IN_CONTEXT_OF_PDMDBVERSION, PDMDBVersionInContextOfPDMDBServerViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_MSSQL, PDMDbServerPDMDbVersionMSSQLMtoNViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_MSSQL_IN_CONTEXT, PDMDbServerPDMDbVersionMSSQLMtoNInContextViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_ORACLE, PDMDbServerPDMDbVersionOracleMtoNViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_ORACLE_IN_CONTEXT, PDMDbServerPDMDbVersionOracleMtoNInContextViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBVERSION, PDMDBVersionViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBVERSION_IN_CONTEXT_OF_PDMDBSERVER, PDMDBServerInContextOfPDMDBVersionViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_MSSQL, PDMDbVersionPDMDbServerMSSQLMtoNViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_MSSQL_IN_CONTEXT, PDMDbVersionPDMDbServerMSSQLMtoNInContextViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_ORACLE, PDMDbVersionPDMDbServerOracleMtoNViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_ORACLE_IN_CONTEXT, PDMDbVersionPDMDbServerOracleMtoNInContextViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMFILETYPE, PDMFileTypeViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMFILETYPEMSSQL, PDMFileTypeMSSQLViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMFILETYPEORACLE, PDMFileTypeOracleViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMLOCATION, PDMLocationViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMREQUEST, PDMRequestViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMREQUEST_DUMPFILE, PDMRequestDatabaseViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDM_LATEST_EVENTLOGS, LatestEventLogsForPDMDatabaseViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMREQUEST_TYPE_CODE, PDMRequestTypeCodeReferenceViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMTASK, PDMTaskViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMTASK_REQUEST_DATABASE, PDMTaskRequestDatabaseViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMREQUEST_INCONTEXT_DATABASE, PDMRequestsInContextofDatabaseViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PDMTASKS_IN_INITIAL_STATE, PDMTasksInInitialStateViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_LOOKUP_PDMDATABASESERVER, PDMDatabaseServerLookupViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_LOOKUP_PDMDATABASETYPE, PDMDatabaseTypeLookupViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_LOOKUP_PDMFILETYPE, PDMFileTypeLookupViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_POPUP_DBSERVERS_BY_DBTYPE, PopupDBServerByDBTypeViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_POPUP_DBSERVERS_BY_DBVERSION, PopupDBServerByDBVersionViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_POPUP_DBVERSIONS_BY_DBSERVER, PopupDbVersionByDBServerViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_POPUP_PDMDATABASESERVER, PopupPDMDatabaseServerViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_POPUP_PDMDATABASETYPE, PopupPDMDatabaseTypeViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_POPUP_PDMFILETYPE, PopupPDMFileTypeViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_COMPATIBILITYMATRIX, PDMCompatibilityMatrixStepViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_DATABASE_SERVERS, PDMDatabaseServerStepViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_DATABASE_VERSIONS, PDMDatabaseVersionStepViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_DBSERVER_DATAFILELOC, PDMDbServerDataFileLocationStepViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_PDM_DATABASES, PDMDatabaseStepViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_PDM_LOCATIONS, PDMLocationStepViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_REQUESTS, PDMRequestStepViewDefinition.class);
    registerBundle(PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_TASKS, PDMTaskStepViewDefinition.class);
  }


  /**
   * Register a PVType with its PVBaseDef Class. Does replace class by db dependent variant if
   * available
   *
   * @param aPVType
   * @param aPVDefinitionClass
   */
  protected final void registerBundle(final IPVType aPVType, final Class<? extends PVBaseDef> aPVDefinitionClass)
  {
    Class<? extends PVBaseDef> pvDefinitionClass = getRuntimeClass(aPVDefinitionClass);
    register(aPVType, pvDefinitionClass);
  }


  /**
   * gets class for specific database
   *
   * @param  aPVDefinitionClass DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  private Class<? extends PVBaseDef> getRuntimeClass(Class<? extends PVBaseDef> aPVDefinitionClass)
  {
    Class<? extends PVBaseDef> pvDefinitionClass;
    try
    {
      // When not found in the cache try the find the Class for a specific database.
      String classNameSpecific = DBSettings.getInstance().getDatabaseVendor().getVendorSpecificClassName(aPVDefinitionClass.getName());
      pvDefinitionClass = (Class<? extends PVBaseDef>) Class.forName(classNameSpecific);
    }
    catch (ClassNotFoundException ex)
    {
      // When not found use the default Class.
      pvDefinitionClass = aPVDefinitionClass;
    }
    return pvDefinitionClass;
  }
}
