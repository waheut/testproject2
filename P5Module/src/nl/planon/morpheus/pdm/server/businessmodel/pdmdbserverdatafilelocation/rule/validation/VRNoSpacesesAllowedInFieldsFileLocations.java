// Planon Enterprise Edition Source file: VRNoSpacesesAllowedInFieldsFileLocations.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.rule.validation;

import java.util.regex.*;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.*;

/**
 * validation rule on field Code of database. There are only word characters allowed and it should
 * start with an alphanumeric character.
 */
public class VRNoSpacesesAllowedInFieldsFileLocations extends ValidationRule<BOPDMDBServerDataFileLocation>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * There are only word characters allowed and it should start with an alphanumeric character.
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDBServerDataFileLocation aBO, BOPDMDBServerDataFileLocation aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    String fileLocationLocal = aBO.getFieldByPnName(BOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL).getAsString();
    String fileLocationNetwerk = aBO.getFieldByPnName(BOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_NETWORK).getAsString();
    String trimmedLocal = fileLocationLocal.trim();
    if (fileLocationNetwerk != null)
    {
      String trimmedNetwerk = fileLocationNetwerk.trim();

      if (Pattern.matches(".*\\s+.*", trimmedNetwerk))
      {
        PnMessage msg = aPnContext.getPnErrorManager().createMessage(ErrorNumberPDMP5Module.EC_NO_SPACE_ALLOWED);
        String transkey = aBO.getFieldByPnName(BOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_NETWORK).getLocalizableKey();
//        msg.addLocalizebleKeyArgument(transkey);
        msg.addStringArgument(transkey);
        aPnContext.getPnErrorManager().addFatal(msg);
      }
    }
    if (Pattern.matches(".*\\s+.*", trimmedLocal))
    {
      PnMessage msg = aPnContext.getPnErrorManager().createMessage(ErrorNumberPDMP5Module.EC_NO_SPACE_ALLOWED);
      String transkey = aBO.getFieldByPnName(BOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL).getLocalizableKey();
//      msg.addLocalizebleKeyArgument(transkey);
      msg.addStringArgument(transkey);
      aPnContext.getPnErrorManager().addFatal(msg);
    }

//    if (!trimmedCode.equals(code))
//    {
//      aBO.getFieldByPnName(BOPDMDatabaseDef.PN_CODE).setAsObject(trimmedCode);
//    }
  }
}
