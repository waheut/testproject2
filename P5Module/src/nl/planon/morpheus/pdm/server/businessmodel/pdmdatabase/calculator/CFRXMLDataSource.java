// Planon Enterprise Edition Source file: CFRXMLDataSource.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.calculator;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.calculation.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;

import nl.planon.pdm.core.common.*;

/**
 * calculates content of planon-ds.xml configuration file
 */
public class CFRXMLDataSource extends BaseFieldValueCalculator<BOPDMDatabase>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String TOKEN_NEW_CONNECTION_SQL = "$$NewConnectionSQL$$";

  private static final String TOKEN_VALID_CONN_SQL = "$$ValidConnectionSQL$$";

  private static final String TOKEN_PASSWORD = "$$Password$$";

  private static final String TOKEN_USER_NAME = "$$UserName$$";

  private static final String TOKEN_CONNECTION_URL = "$$ConnectionURL$$";

  private static final String TOKEN_DRIVER_CLASS = "$$DriverClass$$";

  private static final String XML_DATASOURCE = new StringBuilder() //
.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n") //
.append("<datasources>\n") //
.append("  <local-tx-datasource>\n") //
.append("    <jndi-name>PlanonDS</jndi-name>\n") //
.append("    <driver-class>").append(TOKEN_DRIVER_CLASS).append("</driver-class>\n") //
.append("    <connection-url>").append(TOKEN_CONNECTION_URL).append("</connection-url>\n") //
.append("    <user-name>").append(TOKEN_USER_NAME).append("</user-name>\n") //
.append("    <password>").append(TOKEN_PASSWORD).append("</password>\n") //
.append("    <min-pool-size>10</min-pool-size>\n") //
.append("    <max-pool-size>30</max-pool-size>\n") //
.append("    <new-connection-sql>").append(TOKEN_NEW_CONNECTION_SQL).append("</new-connection-sql>\n") //
.append("    <check-valid-connection-sql>").append(TOKEN_VALID_CONN_SQL).append("</check-valid-connection-sql>\n") //
.append("    <prepared-statement-cache-size>100</prepared-statement-cache-size>\n") //
.append("    <shared-prepared-statements>true</shared-prepared-statements>\n") //
.append("	<!-- Automatic reconnection parameters -->\n") //
.append("    <autoReconnect>true</autoReconnect>\n") //
.append("    <failOverReadOnly>false</failOverReadOnly>\n") //
.append("    <maxReconnects>50</maxReconnects>\n") //
.append("    <initialTimeout>15</initialTimeout>\n") //
.append("  </local-tx-datasource>\n") //
.append("</datasources>\n") //
.toString();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CFRXMLDataSource object.
   *
   * @param aFieldDefinition
   */
  public CFRXMLDataSource(FieldDefinition aFieldDefinition)
  {
    super(aFieldDefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * calculate content of planon-ds.xml
   *
   * @param  aBO
   * @param  aPnContext
   *
   * @return string containing xml of datasource
   *
   * @throws PnErrorListException
   */
  @Override protected IBaseValue calculateFieldValue(BOPDMDatabase aBO, PnContext aPnContext) throws PnErrorListException
  {
    BOBasePDMDatabaseServer databaseServer = (BOBasePDMDatabaseServer) aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
    String ds = "";

    ReferenceField fieldDBType = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF);
    if (!fieldDBType.isEmpty())
    {
      String code = fieldDBType.getFieldValueOfReferencedBO(BOPDMDatabaseTypeDef.PN_CODE).getAsString();
      String userName = aBO.getFieldByPnName(IBOPDMDatabaseDef.PN_NAME).getAsString();
      String password = aBO.getFieldByPnName(IBOPDMDatabaseDef.PN_ACCESSCODE).getAsString();
      PDMDatabaseType databaseType = PDMDatabaseType.getPDMDatabaseTypeByCode(code);
      if ((databaseType != null) && (databaseServer != null) && (userName != null) && (password != null))
      {
        ds = replace(XML_DATASOURCE, TOKEN_DRIVER_CLASS, databaseType.getDriverName());
        ds = replace(ds, TOKEN_NEW_CONNECTION_SQL, databaseType.getNewConnectionSQL());
        ds = replace(ds, TOKEN_VALID_CONN_SQL, databaseType.getValidConnectionSQL());
        ds = replace(ds, TOKEN_CONNECTION_URL, databaseServer.getConnectionURL().replaceAll("\\\\", "\\\\\\\\")); // fix gui issue: replace \ bij \\
        ds = replace(ds, TOKEN_USER_NAME, userName);
        ds = replace(ds, TOKEN_PASSWORD, password);
      }
    }
    return new BaseStringValue(ds);
  }


  /**
   * replaces first token with value in <code>aValue</code>
   *
   * @param  aValue      string containing tokens to be replaced
   * @param  aToken      token to replace
   * @param  aTokenValue value to replace token with
   *
   * @return replaced new value
   */
  private String replace(String aValue, String aToken, String aTokenValue)
  {
    String token = aToken.replaceAll("\\$", "\\\\\\$");
    String tokenValue = aTokenValue.replaceAll("\\$", "\\\\\\$");
    return aValue.replaceFirst(token, tokenValue);
  }
}
