// Planon Enterprise Edition Source file: VROnlyWordCharsAllowedInFieldName.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import java.util.regex.*;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * validation rule on field Name of database. There are only word characters allowed and it should
 * start with an alphanumeric character.
 */
public class VROnlyWordCharsAllowedInFieldName extends ValidationRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * There are only word characters allowed and it should start with an alphanumeric character.
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    String name = aBO.getFieldByPnName(BOPDMDatabaseDef.PN_NAME).getAsString();
    String trimmedName = name.trim();

    if (!Pattern.matches("[A-Za-z]{1}(\\w)+", trimmedName))
    {
      PnMessage msg = aPnContext.getPnErrorManager().createMessage(ErrorNumberPDMP5Module.EC_ONLY_WORD_CHARS_ALLOWED);
      String transkey = aBO.getFieldByPnName(BOPDMDatabaseDef.PN_NAME).getLocalizableKey();
      msg.addLocalizebleKeyArgument(transkey);
      aPnContext.getPnErrorManager().addFatal(msg);
    }

    if (!trimmedName.equals(name))
    {
      aBO.getFieldByPnName(BOPDMDatabaseDef.PN_NAME).setAsObject(trimmedName);
    }
  }
}
