// Planon Enterprise Edition Source file: PDMDbVersionPDMDbServerMSSQLMtoNViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql.proxyview;

import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDbVersionPDMDbServerMSSQLMtoNViewDefinition
 */
public class PDMDbVersionPDMDbServerMSSQLMtoNViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDbVersionPDMDbServerMSSQLMtoNViewDefinition object.
   */
  public PDMDbVersionPDMDbServerMSSQLMtoNViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL);
  }
}
