// Planon Enterprise Edition Source file: BOPDMDatabaseServerOracle.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMDatabaseServerOracle
 */
public class BOPDMDatabaseServerOracle extends BOBasePDMDatabaseServer
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String PREFIX_CONNECTION_URL = "jdbc:oracle:thin:@";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerOracle object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMDatabaseServerOracle(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets connection URL
   *
   * @return String
   *
   * @throws PnErrorListException
   */
  @Override public String getConnectionURL() throws PnErrorListException
  {
    String hostName = getFieldByPnName(BOPDMDatabaseServerMSSQLDef.PN_HOST_NAME).getAsString();
    Integer portNumber = getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_PORT_NUMBER).getAsInteger();
    String dbInstanceName = getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_DATABASE_INSTANCENAME).getAsString();

    StringBuilder builder = new StringBuilder();
    builder.append(PREFIX_CONNECTION_URL).append(hostName).append(':').append(portNumber).append(':').append(dbInstanceName);

    return builder.toString();
  }


  /**
   * PDM Database Type of databaseserver
   *
   * @return PDM Database Type Oracle
   */
  @Override public PDMDatabaseType getPDMDatabaseType()
  {
    return PDMDatabaseType.ORACLE;
  }
}
