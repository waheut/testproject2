// Planon Enterprise Edition Source file: BOPDMDatabaseType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMDatabaseType
 */
public class BOPDMDatabaseType extends BOSystemCode
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseType object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMDatabaseType(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
