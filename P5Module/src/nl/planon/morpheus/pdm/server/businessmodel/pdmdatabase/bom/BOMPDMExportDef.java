// Planon Enterprise Edition Source file: BOMPDMExportDef.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.exportrequest.*;

import nl.planon.pdm.core.common.*;

/**
 * BOMExportDatabaseDef
 */
public class BOMPDMExportDef extends BOMPDMDatabaseRequestDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMExportDatabaseDef object.
   *
   * @param aBODef     BO definition!
   * @param aBOMPnName PnName of BOM
   */
  public BOMPDMExportDef(BODefinition aBODef, String aBOMPnName)
  {
    super(aBODef, aBOMPnName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Check if WebDAV is available. If not, the request is useless
   *
   * @param  aPDMDatabase
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doBeforeCreateRequest(final BOPDMDatabase aPDMDatabase, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateRequest(aPDMDatabase, aPnContext);
    BusinessObject pdmLocation = aPnContext.getBOByPrimaryKey(BOTypePDMP5Module.PDM_LOCATION, aPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_PDMLOCATION_REF).getBaseValue());
    String destinationHost = pdmLocation.getFieldByPnName(IBOPDMLocationDef.PN_LOCATION).getAsString();
    if (!WebDAVUtilities.getInstance().isFileServerAvailable(destinationHost))
    {
      PnErrorManager errorManager = aPnContext.getPnErrorManager();
      PnMessage message = errorManager.createMessage(ErrorNumberPDMP5Module.EC_FILESERVER_NOT_AVAILABLE);
      message.addStringArgument(destinationHost);
      errorManager.addFatal(message); // Throw the error directly, continuing the request is useless.
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IBOType getBOTypeRequest()
  {
    return BOTypePDMP5Module.PDM_REQUEST_EXPORT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IBOType getBOTypeTask(BOPDMDatabase aBOPDMDatabase)
  {
    return BOTypePDMP5Module.PDM_TASK_EXPORT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected PDMRequestType getRequestType() throws PnErrorListException
  {
    return BOPDMRequestExportDef.PDMREQUEST_TYPE;
  }


  /**
   * Add bom rules to this bom.
   */
  private void addBOMRules()
  {
    final BRDatabaseImported brDatabaseImported = new BRDatabaseImported();

    addBOMRule(brDatabaseImported);
  }
}
