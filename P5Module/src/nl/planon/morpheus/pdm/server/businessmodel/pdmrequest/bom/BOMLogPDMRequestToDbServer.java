// Planon Enterprise Edition Source file: BOMLogPDMRequestToDbServer.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOMLogPDMDatabaseServer
 */
public class BOMLogPDMRequestToDbServer extends BaseBOMLogPDMRequestDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMLogPDMRequestToDbServer object.
   *
   * @param aBODef
   * @param aPnBOMName
   */
  public BOMLogPDMRequestToDbServer(BODefinition aBODef, String aPnBOMName)
  {
    super(aBODef, aPnBOMName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject getLinkedBO(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    BusinessObject boPDMDatabase = this.boPDMRequest.getReferenceFieldByPnName(IBOBasePDMRequestDef.PN_DATABASE_REF).getReferenceBO();
    return boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getSource(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    BusinessObject boPDMDatabase = this.boPDMRequest.getReferenceFieldByPnName(IBOBasePDMRequestDef.PN_DATABASE_REF).getReferenceBO();
    BusinessObject boPDMDbServer = boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
    String serverInfo = "<NoServer>";
    if (boPDMDbServer != null)
    {
      serverInfo = boPDMDbServer.getDisplayValue(aPnContext);
    }
    return serverInfo + " - " + boPDMDatabase.getDisplayValue(aPnContext) + " - " + getTranslatedDisplayText(aPnContext);
  }
}
