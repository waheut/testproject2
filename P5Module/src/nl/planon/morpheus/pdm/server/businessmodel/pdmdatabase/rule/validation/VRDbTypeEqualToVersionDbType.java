// Planon Enterprise Edition Source file: VRDbTypeEqualToVersionDbType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

import nl.planon.pdm.core.common.*;

/**
 * the filled in database type must be equal to the database type of the database version that is
 * linked to the filled in database server
 */
public class VRDbTypeEqualToVersionDbType extends ValidationRule<BOPDMDatabase>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType ERROR_MESSAGE = ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH_VERSION_DBTYPE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * It is not possible to fill in a database type that is not equal to the database type of the
   * database version that is linked to the filled in database server
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    ReferenceField pdmDbTypeOfDatabaseRefField = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF);
    ReferenceField pdmDatabaseServerRefField = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    if (!(pdmDatabaseServerRefField.isEmpty() || pdmDbTypeOfDatabaseRefField.isEmpty()))
    {
      PDMDatabaseType serverDatabaseType = aBO.getDatabaseTypeOfServer(aPnContext);
      PDMDatabaseType databaseType = aBO.getPDMDatabaseType();
      assert databaseType != null : "database type is mandatory";
      if (!databaseType.equals(serverDatabaseType))
      {
        PnErrorManager manager = aPnContext.getPnErrorManager();
        PnMessage message = manager.createMessage(ERROR_MESSAGE);
        message.addStringArgument(pdmDbTypeOfDatabaseRefField.getLocalizableDisplayValue());
        message.addStringArgument(serverDatabaseType.getCode() + " " + serverDatabaseType.getName());
        message.addFieldNameArgument(pdmDatabaseServerRefField);

        manager.addFatal(message);
      }
    }
  }
}
