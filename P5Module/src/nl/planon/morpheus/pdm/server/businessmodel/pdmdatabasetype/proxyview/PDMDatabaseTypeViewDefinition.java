// Planon Enterprise Edition Source file: PDMDatabaseTypeViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.proxyview;

import nl.planon.hades.businessmodel.systemcode.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMFileTypeViewDefinition
 */
public class PDMDatabaseTypeViewDefinition extends SystemCodeViewDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseTypeViewDefinition object.
   */
  public PDMDatabaseTypeViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_TYPE);
  }
}
