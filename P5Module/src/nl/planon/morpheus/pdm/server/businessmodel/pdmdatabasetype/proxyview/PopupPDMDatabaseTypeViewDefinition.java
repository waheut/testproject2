// Planon Enterprise Edition Source file: PopupPDMDatabaseTypeViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * PopupPDMDatabaseTypeViewDefinition
 */
public class PopupPDMDatabaseTypeViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PopupPDMDatabaseTypeViewDefinition object.
   */
  public PopupPDMDatabaseTypeViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_TYPE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createSearchDefs(aPnContext);

    addSearchFieldDef(BOPDMDatabaseTypeDef.PN_CODE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(final PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(BOPDMDatabaseTypeDef.PN_CODE, SCOperatorType.IN);
  }


  /**
   * Filters the selected PDM Database Types to choose only those database types that correspond
   * with the PDM File Type or kind of PDM Database Server that is filled in
   *
   * @param  aProxyListContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doBeforeCreateSQLStatement(ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateSQLStatement(aProxyListContext, aPnContext);

    BusinessObject contextBO = aProxyListContext.getBO();
    if (contextBO != null)
    {
      if (contextBO instanceof BOPDMDatabase)
      {
        // get file type if filled in...
        BOPDMDatabase boPDMDatabase = (BOPDMDatabase) contextBO;
        PDMFileType pdmFileType = boPDMDatabase.getPDMFileType();
        if (pdmFileType != null)
        {
          ISCOperator operator = getPermanentSearchCriteria(aPnContext).getOperator(BOPDMFileTypeDef.PN_CODE, SCOperatorType.IN);
          if ((PDMFileType.DATAPUMP.equals(pdmFileType)) || (PDMFileType.ORA_EXP.equals(pdmFileType)))
          {
            operator.addBaseValue(new BaseStringValue(BOPDMDatabaseTypeDef.DATABASETYPE_ORACLE));
          }
          else if (PDMFileType.MSSQL_DUMP.equals(pdmFileType))
          {
            operator.addBaseValue(new BaseStringValue(BOPDMDatabaseTypeDef.DATABASETYPE_MSSQL));
          }
        }
        else
        {
          // .. if not filled in, get database type of database server if filled in...
          BOBasePDMDatabaseServer boPDMDatabaseServer = (BOBasePDMDatabaseServer) contextBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
          if (boPDMDatabaseServer != null)
          {
            PDMDatabaseType pdmDatabaseType = boPDMDatabaseServer.getPDMDatabaseType();
            ISCOperator operator = getPermanentSearchCriteria(aPnContext).getOperator(BOPDMFileTypeDef.PN_CODE, SCOperatorType.IN);
            if (PDMDatabaseType.ORACLE.equals(pdmDatabaseType))
            {
              operator.addBaseValue(new BaseStringValue(BOPDMDatabaseTypeDef.DATABASETYPE_ORACLE));
            }
            else if (PDMDatabaseType.MSSQL.equals(pdmDatabaseType))
            {
              operator.addBaseValue(new BaseStringValue(BOPDMDatabaseTypeDef.DATABASETYPE_MSSQL));
            }
          }
        }
      }
    }
  }
}
