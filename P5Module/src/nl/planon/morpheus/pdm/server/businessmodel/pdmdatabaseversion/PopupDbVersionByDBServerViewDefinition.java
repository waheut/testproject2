// Planon Enterprise Edition Source file: PopupDbVersionByDBServerViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle.*;

/**
 * PopupDbVersionByDBServerViewDefinition
 */
public class PopupDbVersionByDBServerViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PopupDbVersionByDBServerViewDefinition object.
   */
  public PopupDbVersionByDBServerViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /*
   * (non-Javadoc)
   * @see
   * nl.planon.hades.proxyview.PVDefinition#createFieldDefs(nl.planon.hades.
   * beans.PnContext)
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(BOBasePDMDatabaseVersionDef.PN_CODE);
    addSelectFieldDef(BOBasePDMDatabaseVersionDef.PN_NAME);
  }


  /*
   * (non-Javadoc)
   * @see
   * nl.planon.hades.proxyview.PVBaseDef#createWhereDefs(nl.planon.hades.beans
   * .PnContext)
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(new PVExpressionNodeDef(getMasterJoinDef(), BOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doAfterCreateProxyListValue(ProxyListValue aProxyListValue, ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doAfterCreateProxyListValue(aProxyListValue, aProxyListContext, aPnContext);
  }


  /*
   * (non-Javadoc)
   * @see
   * nl.planon.hades.proxyview.PVBaseDef#doBeforeCreateSQLStatement(nl.planon
   * .hades.proxyview.ProxyListContext, nl.planon.hades.beans.PnContext)
   */
  @Override protected void doBeforeCreateSQLStatement(ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateSQLStatement(aProxyListContext, aPnContext);

    BusinessObject contextBO = aProxyListContext.getBO();
    if (contextBO != null)
    {
      IBaseValue pkBODefinition = null;
      ReferenceField dbServerField = contextBO.getReferenceFieldByPnName(BOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
      if (!dbServerField.isEmpty())
      {
        if (dbServerField instanceof PDMDatabaseServerOracleReferenceField)
        {
          pkBODefinition = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE).getPrimaryKeyInDB();
        }
        else if (dbServerField instanceof PDMDatabaseServerMSSQLReferenceField)
        {
          pkBODefinition = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL).getPrimaryKeyInDB();
        }

        SearchCriteria sc = getPermanentSearchCriteria(aPnContext);
        ISCOperator operator = sc.getOperator(BOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL);
        operator.addBaseValue(pkBODefinition);
      }
    }
  }
}
