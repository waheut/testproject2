// Planon Enterprise Edition Source file: PDMSearchForDatabaseServerProxyViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * PDMSearchForDatabaseServerProxyViewDefinition
 */
public class PDMSearchForDatabaseServerProxyViewDefinition extends BasePDMSearchForDatabaseServerProxyViewDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String ALIAS_DB_SERVER = "srv";
  private static final String ALIAS_DATABASE = "db";
  private static final String ALIAS_VERSION = "ver";
  private static final String ALIAS_PLAN_CODE = "PLNCODE";
  private static final String ALIAS_PLAN_MATRIX = "matrix";

  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinDefDatabase;
  private PVJoinDef joinDefDBVersion;
  private PVJoinDef joinDefFileTypeCode;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMSearchForDatabaseServerProxyViewDefinition object.
   */
  public PDMSearchForDatabaseServerProxyViewDefinition()
  {
    setGroupBy(true);
    setSelectDefaultFields(false);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_CODE, ALIAS_SERVER_CODE);
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY, ALIAS_PRIMARYKEY);
    this.joinDefDBVersion.addSelectFieldDef(IBOBasePDMDatabaseVersionDef.PN_CODE, ALIAS_VERSION_NUMBER);
    this.joinDefFileTypeCode.addSelectFieldDef(BOSystemCodeDef.PN_CODE, ALIAS_FILE_TYPE_CODE);
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_ADMIN_USER, ALIAS_ADMIN_USER);
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS, ALIAS_ADMIN_PASSWORD);
    addFieldDef(new PVDummyStringFieldDef(BOBasePDMDatabaseServerDef.PN_CONNECTION_URL, ALIAS_CONNECTION_URL)); // calculated field
    this.joinDefDatabase.addFieldDef(new PVCountFieldDef(IBOPDMDatabaseDef.PN_PRIMARYKEY));
  }


  /**
   * {@inheritDoc}
   *
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDefDatabase = addLeftJoinDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY, BOTypePDMP5Module.PDM_DATABASE, BOPDMDatabaseDef.PN_DATABASESERVER_REF);
    this.joinDefFileTypeCode = addLeftJoinDef(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF);
    this.joinDefDBVersion = addLeftJoinDef(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDefFileTypeCode.addSearchFieldDef(BOPDMFileTypeDef.PN_CODE);
    addSearchFieldDef(BOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF);
    this.joinDefDBVersion.addSearchFieldDef(BOBasePDMDatabaseVersionDef.PN_CODE);

    //alias names
    getMasterJoinDef().setAliasTableName(ALIAS_DB_SERVER);
    this.joinDefDatabase.setAliasTableName(ALIAS_DATABASE);
    this.joinDefDBVersion.setAliasTableName(ALIAS_VERSION);
    this.joinDefFileTypeCode.setAliasTableName(ALIAS_PLAN_CODE);
  }


  /**
   * Created for adding a OrderBy COUNT(*)
   *
   * @param  aProxyListContext
   * @param  aPnContext
   *
   * @return SQL Query
   *
   * @throws PnErrorListException
   */
  @Override protected String doGetSQLStatement(ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    BODefinition boDefDatabase = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE);
    BODefinition boDefCompatibilityMatrix = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX);
    BODefinition boDefDBServerDef = aPnContext.getBODefinition(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
    BODefinition boDefDBVersionDef = aPnContext.getBODefinition(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
    BODefinition boDefinitionPlanCode = aPnContext.getBODefinition(BOTypeHades.SYSTEMCODE);

    String whereSQL = getSearchCriteria().getWhereSQL(aProxyListContext, aPnContext);
    SQLBuilder sql = new SQLBuilder();
    sql.appendSelect() //
       .append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_CODE)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY)) //
       .append(", ").append(ALIAS_VERSION, boDefDBVersionDef.getFieldDefByPnName(IBOBasePDMDatabaseVersionDef.PN_CODE)) //
       .append(", ").append(ALIAS_PLAN_CODE, boDefinitionPlanCode.getFieldDefByPnName(IBOSystemCodeDef.PN_CODE)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_USER)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS)) //
       .append(", null ");
    sql.append(", COUNT(") //
       .append(ALIAS_DATABASE, boDefDatabase.getFieldDefByPnName(IBOPDMDatabaseDef.PN_PRIMARYKEY)) //
       .append(")");
    sql.appendFrom(boDefDBServerDef.getTableName(), ALIAS_DB_SERVER);
    sql.appendInnerJoin(boDefCompatibilityMatrix.getTableName(), ALIAS_PLAN_MATRIX) //
       .appendOn() //
       .append(ALIAS_DB_SERVER, boDefDBServerDef.getPrimaryKeyFieldDef()) //
       .append(" = ") //
       .append(ALIAS_PLAN_MATRIX, boDefCompatibilityMatrix.getFieldDefByPnName(IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF)) //
       .append(" And ") //
       .append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE)).append(" = 'T'");

    sql.appendInnerJoin(boDefinitionPlanCode.getTableName(), ALIAS_PLAN_CODE) //
       .appendOn() //
       .append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF)) //
       .append(" = ") //
       .append(ALIAS_PLAN_CODE, boDefinitionPlanCode.getPrimaryKeyFieldDef());

    sql.appendInnerJoin(boDefDBVersionDef.getTableName(), ALIAS_VERSION) //
       .appendOn() //
       .append(ALIAS_PLAN_MATRIX, boDefCompatibilityMatrix.getFieldDefByPnName(IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF)) //
       .append(" = ") //
       .append(ALIAS_VERSION, boDefDBVersionDef.getPrimaryKeyFieldDef());

    sql.appendLeftJoin(boDefDatabase.getTableName(), ALIAS_DATABASE) //
       .appendOn() //
       .append(ALIAS_DB_SERVER, boDefDBServerDef.getPrimaryKeyFieldDef()) //
       .append(" = ") //
       .append(ALIAS_DATABASE, boDefDatabase.getFieldDefByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF)); //

    sql.appendWhere() //
       .append(whereSQL);
    sql.append(" Group BY ") //
       .append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_CODE)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_HOST_NAME)) //
       .append(", ").append(ALIAS_VERSION, boDefDBVersionDef.getFieldDefByPnName(IBOBasePDMDatabaseVersionDef.PN_CODE)) //
       .append(", ").append(ALIAS_PLAN_CODE, boDefinitionPlanCode.getFieldDefByPnName(IBOSystemCodeDef.PN_CODE)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_USER)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASE_INSTANCENAME)) //
       .append(", ").append(ALIAS_DB_SERVER, boDefDBServerDef.getFieldDefByPnName(IBOBasePDMDatabaseServerDef.PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES));
    sql.append(" ORDER BY COUNT(") //
       .append(ALIAS_DATABASE, boDefDatabase.getPrimaryKeyFieldDef()) //
       .append(")");

    return sql.toString();
  }
}
