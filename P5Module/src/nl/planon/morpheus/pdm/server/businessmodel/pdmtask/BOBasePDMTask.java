// Planon Enterprise Edition Source file: BOBasePDMTask.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

/**
 * BOBasePDMTask
 */
public class BOBasePDMTask extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOBasePDMTask object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOBasePDMTask(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
