// Planon Enterprise Edition Source file: PDMDataFileLocationRelatedToDBServerViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseServerLookupViewDefinition
 */
public class PDMDataFileLocationRelatedToDBServerViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseServerLookupViewDefinition object.
   */
  public PDMDataFileLocationRelatedToDBServerViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DBSERVER_DATAFILELOCATION);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOPDMDBServerDataFileLocationDef.PN_DATABASESERVER_REF);
  }
}
