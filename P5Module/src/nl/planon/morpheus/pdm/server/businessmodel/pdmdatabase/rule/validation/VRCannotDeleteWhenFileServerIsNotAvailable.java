// Planon Enterprise Edition Source file: VRCannotDeleteWhenFileServerIsNotAvailable.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * VRCannotDeleteWhenFileServerIsNotAvailable
 */
public class VRCannotDeleteWhenFileServerIsNotAvailable extends ValidationRule<BOPDMDatabase>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType ERROR_MESSAGE = ErrorNumberPDMP5Module.EC_FILESERVER_NOT_AVAILABLE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * It is not allowed to delete a PDM Database when the file server is not available through
   * WebDAV. There are log and dump files on the file server that also need to be deleted.
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    BusinessObject pdmLocation = aPnContext.getBOByPrimaryKey(BOTypePDMP5Module.PDM_LOCATION, aBO.getFieldByPnName(IBOPDMDatabaseDef.PN_PDMLOCATION_REF).getBaseValue());
    String destinationHost = pdmLocation.getFieldByPnName(IBOPDMLocationDef.PN_LOCATION).getAsString();
    if (!WebDAVUtilities.getInstance().isFileServerAvailable(destinationHost))
    {
      PnErrorManager errorManager = aPnContext.getPnErrorManager();
      PnMessage message = errorManager.createMessage(ErrorNumberPDMP5Module.EC_FILESERVER_NOT_AVAILABLE);
      message.addStringArgument(destinationHost);
      errorManager.addFatal(message);
    }
  }
}
