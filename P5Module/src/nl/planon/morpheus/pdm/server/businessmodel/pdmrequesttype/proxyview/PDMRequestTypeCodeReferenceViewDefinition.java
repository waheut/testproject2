// Planon Enterprise Edition Source file: PDMRequestTypeCodeReferenceViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.proxyview;

import nl.planon.hades.businessmodel.systemcode.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMRequestTypeCodeViewDefinition
 */
public class PDMRequestTypeCodeReferenceViewDefinition extends SystemCodeViewDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestTypeCodeViewDefinition object.
   */
  public PDMRequestTypeCodeReferenceViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_REQUEST_TYPE);
  }
}
