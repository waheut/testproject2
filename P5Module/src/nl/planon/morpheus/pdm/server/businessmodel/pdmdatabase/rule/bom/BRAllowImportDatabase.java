// Planon Enterprise Edition Source file: BRAllowImportDatabase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * BRAllowImportDatabase
 */
public class BRAllowImportDatabase extends BOMRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * The BOM 'Import database' is only avaible when:<br>
   * - The database is in state initial<br>
   *
   * @param  aBO
   * @param  aPnContext
   *
   * @return true is the BOM should be visible, false when the BOM should be invisible
   *
   * @throws PnErrorListException
   */
  @Override protected boolean execute(BOPDMDatabase aBO, PnContext aPnContext) throws PnErrorListException
  {
    return IBOPDMDatabaseDef.STATE_INITIAL.equals(aBO.getSystemState().getPnName());
  }
}
