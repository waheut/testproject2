// Planon Enterprise Edition Source file: BOPDMLocation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmlocation;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMLocation
 */
public class BOPDMLocation extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMLocation object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMLocation(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
