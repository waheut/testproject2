// Planon Enterprise Edition Source file: LatestEventLogsForPDMDatabaseViewDefinition.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.businessobject.*;
import nl.planon.hades.businessmodel.eventlog.linked.*;
import nl.planon.hades.businessmodel.eventlogbusinessobject.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * This returns the event logs that are assoicated with the supplied PDM Database PK.
 *
 * <p>The equivalent query looks to be:</p>
 *
 * <pre>SELECT TABLE_1.SYSCODE AS "SYS_Syscode1"
  ,TABLE_1.SYSCODE AS "SYS_Syscode2"
  ,TABLE_1.LOGMESSAGE AS "LogMessage3"
FROM PLN_EVENTLOG TABLE_1
INNER JOIN PLN_EVENTLOGBUSINESSOBJECT TABLE_2 ON TABLE_1.SYSCODE = TABLE_2.FK_EVENTLOG
INNER JOIN PLN_BODEFINITION TABLE_3 ON TABLE_3.SYSCODE=TABLE_2.FK_BODEFINITION
INNER JOIN PLN_PDMREQUEST_QUEUE TABLE_4 ON TABLE_2.BUSINESSOBJECT_SYSCODE = TABLE_4.SYSCODE
  AND (TABLE_4.BEHCODE = (@P0))
WHERE 1 = 1
  AND (TABLE_1.SUBTYPE = (@P1))
  AND ((TABLE_1.ISARCHIVED = (@P2)))
  AND (TABLE_3.PNNAME='BasePDMRequest')
  AND (((TABLE_4.FK_PDMDATABASE = (@P3))))
 </pre>
 */
public class LatestEventLogsForPDMDatabaseViewDefinition extends PVDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_PDMDATABASE_PK = "PN_PDMDATABASE_PK";

  public static final String PN_REQUEST_PK = "PN_REQUEST_PK";

  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef boDefJoin;
  private PVJoinDef eventLogBusinessObjectJoin;
  private PVJoinDef pdmRequestJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new LatestEventLogsForPDMDatabaseViewDefinition object.
   */
  public LatestEventLogsForPDMDatabaseViewDefinition()
  {
    super(BOTypeHades.LINKED_EVENT_LOG);
    setDisplayValueFormatString(null);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(BOLinkedEventLogDef.PN_LOGMESSAGE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.eventLogBusinessObjectJoin = addInnerJoinDef(BOTypeHades.EVENTLOGBUSINESSOBJECT, IBOEventLog_BusinessObjectDef.PN_EVENTLOG_REF);
    this.boDefJoin = this.eventLogBusinessObjectJoin.addInnerJoinDef(IBOEventLog_BusinessObjectDef.PN_BODEFINITION_REF);
    this.pdmRequestJoin = this.eventLogBusinessObjectJoin.addInnerJoinDef(IBOEventLog_BusinessObjectDef.PN_BO_REF, BOTypePDMP5Module.BASE_PDM_REQUEST, IBOBasePDMRequestDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmRequestJoin.addSearchFieldDef(IBOBasePDMRequestDef.PN_DATABASE_REF, PN_PDMDATABASE_PK);
    this.pdmRequestJoin.addSearchFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY, PN_REQUEST_PK);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.boDefJoin.addWhereDef(BOBaseBODefinitionDef.PN_PNNAME, SCOperatorType.EQUAL, new BaseStringValue(BOTypePDMP5Module.BASE_PDM_REQUEST.getPnName()));
    this.pdmRequestJoin.addWhereDef(IBOBasePDMRequestDef.PN_PRIMARYKEY, SCOperatorType.EQUAL);
  }
}
