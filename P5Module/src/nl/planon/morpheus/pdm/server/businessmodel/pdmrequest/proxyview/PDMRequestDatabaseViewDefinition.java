// Planon Enterprise Edition Source file: PDMRequestDatabaseViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMRequestDatabaseViewDefinition
 */
public class PDMRequestDatabaseViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinDatabase;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestDatabaseViewDefinition object.
   */
  public PDMRequestDatabaseViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_REQUEST);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createFieldDefs(aPnContext);
    addSelectFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY);
    this.joinDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_DUMPFILE);
  }


  /**
   * join for finding the right PDMFileTypeCode by the primary key
   *
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDatabase = addInnerJoinDef(IBOBasePDMRequestDef.PN_DATABASE_REF);
  }


  /**
   * @see PVDefinition#createSearchDefs()
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY);
  }
}
