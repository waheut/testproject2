// Planon Enterprise Edition Source file: PDMRequestTypeReferenceField.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.pdm.core.common.*;

/**
 * PDMRequestTypeReferenceField
 */
public class PDMRequestTypeReferenceField extends SystemCodeReferenceField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestTypeReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMRequestTypeReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public String getCodeValue() throws PnErrorListException
  {
    return (String) super.getCodeValue();
  }


  /**
   * Get PDM Request type according to code
   *
   * @return PDM Request Type
   *
   * @throws PnErrorListException
   */
  public PDMRequestType getPDMRequestType() throws PnErrorListException
  {
    return PDMRequestType.getPDMRequestType(getCodeValue());
  }


  /**
   * sets specified type
   *
   * @param  aType type
   *
   * @throws PnErrorListException
   */
  public void setPDMRequestType(PDMRequestType aType) throws PnErrorListException
  {
    setCodeValue(aType.getCode());
  }
}
