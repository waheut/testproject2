// Planon Enterprise Edition Source file: PDMRequestsInContextofDatabaseViewDefinition.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMRequestsInContextofDatabaseViewDefinition
 */
public class PDMRequestsInContextofDatabaseViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDatabaseJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestsInContextofDatabaseViewDefinition object.
   */
  public PDMRequestsInContextofDatabaseViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_REQUEST);
    setDistinct(true);
    setSelectDefaultFields(true);
    setDisplayValueFormatString(null);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDatabaseJoin = addInnerJoinDef(IBOBasePDMRequestDef.PN_DATABASE_REF);
    this.pdmDatabaseJoin.setEnabled(true);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOBasePDMRequestDef.PN_DATABASE_REF);
  }
}
