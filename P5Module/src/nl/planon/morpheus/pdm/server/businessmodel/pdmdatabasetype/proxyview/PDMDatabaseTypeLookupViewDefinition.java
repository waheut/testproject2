// Planon Enterprise Edition Source file: PDMDatabaseTypeLookupViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseTypeLookupViewDefinition
 */
public class PDMDatabaseTypeLookupViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinDbTypeFileType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseTypeLookupViewDefinition object.
   */
  public PDMDatabaseTypeLookupViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_TYPE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDbTypeFileType = addLeftJoinDef(BOTypePDMP5Module.PDM_DB_TYPE_FILE_TYPE, IBOPDMDBTypePDMFileTypeDef.PN_DATABASE_TYPE_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDbTypeFileType.addSearchFieldDef(IBOPDMDBTypePDMFileTypeDef.PN_FILE_TYPE_REF);
  }
}
