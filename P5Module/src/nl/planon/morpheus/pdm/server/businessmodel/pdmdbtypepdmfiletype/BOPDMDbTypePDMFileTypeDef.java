// Planon Enterprise Edition Source file: BOPDMDbTypePDMFileTypeDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdbtypepdmfiletype;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * BOPDMDbTypePDMFileTypeDef
 */
public class BOPDMDbTypePDMFileTypeDef extends BODefinition implements IBOPDMDBTypePDMFileTypeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Database fields
  private static final String DB_DATABASE_TYPE_REF = "FK_PLC_PDMDATABASETYPE";
  private static final String DB_FILE_TYPE_REF = "FK_PLC_PDMFILETYPE";

  private static final String TBL_NAME = "PLN_PDMDBTYPE_PDMFILETYPE";
  // Display format
  private static final String DISPLAY_FORMAT = getDisplayFormat(PN_DATABASE_TYPE_REF, PN_FILE_TYPE_REF);

  // Unique constraints
  private static final String UFS_PDMDBTYPE_PDMFILETYPE = "UFSPDMDbTypePDMFileType";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDbTypePDMFileTypeDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDbTypePDMFileTypeDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    FieldDefinition fieldDef;
    ReferenceFieldDefinition refFieldDef;

    // General fields
    //----------------------------------------------------------------------------------------------
    removeSysDataSectionReferenceFieldDefinition();
    //----------------------------------------------------------------------------------------------

    /* ---------------------------------------------------------------------- */
    refFieldDef = new PDMDatabaseTypeReferenceFieldDefinition(this, PN_DATABASE_TYPE_REF, DB_DATABASE_TYPE_REF);
    refFieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(refFieldDef);
    /* ---------------------------------------------------------------------- */
    refFieldDef = new PDMFileTypeReferenceFieldDefinition(this, PN_FILE_TYPE_REF, DB_FILE_TYPE_REF);
    refFieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(refFieldDef);

    //----------------------------------------------------------------------------------------------
    // Uniqueness rules
    // --------------------------------------------------------------------------
    UniqueSetOfFieldDefs ufsPDMDbTypePDMFileType = new UniqueSetOfFieldDefs(UFS_PDMDBTYPE_PDMFILETYPE);
    ufsPDMDbTypePDMFileType.addFieldDef(getFieldDefByPnName(PN_DATABASE_TYPE_REF));
    ufsPDMDbTypePDMFileType.addFieldDef(getFieldDefByPnName(PN_FILE_TYPE_REF));
    addUniqueFieldSet(ufsPDMDbTypePDMFileType);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDbTypePDMFileType(this);
  }
}
