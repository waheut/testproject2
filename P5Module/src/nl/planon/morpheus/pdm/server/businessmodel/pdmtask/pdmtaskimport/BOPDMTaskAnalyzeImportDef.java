// Planon Enterprise Edition Source file: BOPDMTaskAnalyzeImportDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskimport;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMTaskAnalyzeImportDef
 */
public class BOPDMTaskAnalyzeImportDef extends BOBasePDMTaskDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Sub type identifier
  private static final String SUB_TYPE_PDM_TASK_IMPORT = PDMTaskType.IMPORT_ANALYZE.getCode();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImportDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMTaskAnalyzeImportDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_TASK);
    setSystemSubTypeFieldValue(new BaseStringValue(SUB_TYPE_PDM_TASK_IMPORT));
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_TASK_ANALYZE_IMPORT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
    addBOMDef(new BOMAddDef(this, PN_BOM_ADD)).setSystem(true); // needed for the analyze step. not fisible for the user
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BOPDMTaskAnalyzeImport doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMTaskAnalyzeImport(this);
  }
}
