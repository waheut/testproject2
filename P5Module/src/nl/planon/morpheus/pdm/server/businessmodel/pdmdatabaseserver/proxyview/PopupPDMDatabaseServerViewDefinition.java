// Planon Enterprise Edition Source file: PopupPDMDatabaseServerViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * PopupPDMDatabaseServerViewDefinition
 */
public class PopupPDMDatabaseServerViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PopupPDMDatabaseServerViewDefinition object.
   */
  public PopupPDMDatabaseServerViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createSearchDefs(aPnContext);

    addSearchFieldDef(BOBasePDMDatabaseServerDef.PN_CODE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(final PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(BOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF, SCOperatorType.IN);
    addWhereDef(BOBasePDMDatabaseServerDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL);
    addWhereDef(BOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF, SCOperatorType.EQUAL);
  }


  /**
   * Filters the selected PDM Database Servers to choose only those servers that correspond with the
   * PDM File Type or PDM Database Type that is filled in
   *
   * @param  aProxyListContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doBeforeCreateSQLStatement(ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateSQLStatement(aProxyListContext, aPnContext);

    BusinessObject contextBO = aProxyListContext.getBO();
    if (contextBO != null)
    {
      if (contextBO instanceof BOPDMDatabase)
      {
        // get file type if filled in...
        BOPDMDatabase boPDMDatabase = (BOPDMDatabase) contextBO;
        PDMFileType pdmFileType = boPDMDatabase.getPDMFileType();
        IBaseValue pdmLocationRef = boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_PDMLOCATION_REF).getBaseValue();
        if (pdmLocationRef != null)
        {
          ISCOperator operator = getPermanentSearchCriteria(aPnContext).getOperator(BOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF, SCOperatorType.EQUAL);
          operator.addBaseValue(pdmLocationRef);
        }
        if (pdmFileType != null)
        {
          ISCOperator operator = getPermanentSearchCriteria(aPnContext).getOperator(BOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF, SCOperatorType.IN);
          if (PDMFileType.DATAPUMP.equals(pdmFileType))
          {
            operator.addBaseValue(BOPDMFileTypeDef.getPKValue(PDMFileType.DATAPUMP.getCode(), aPnContext));
            operator.addBaseValue(BOPDMFileTypeDef.getPKValue(PDMFileType.DATAPUMP_AND_EXP.getCode(), aPnContext));
          }
          else if (PDMFileType.ORA_EXP.equals(pdmFileType))
          {
            operator.addBaseValue(BOPDMFileTypeDef.getPKValue(PDMFileType.ORA_EXP.getCode(), aPnContext));
            operator.addBaseValue(BOPDMFileTypeDef.getPKValue(PDMFileType.DATAPUMP_AND_EXP.getCode(), aPnContext));
          }
          else if (PDMFileType.MSSQL_DUMP.equals(pdmFileType))
          {
            operator.addBaseValue(BOPDMFileTypeDef.getPKValue(PDMFileType.MSSQL_DUMP.getCode(), aPnContext));
          }
        }
        else
        {
          PDMDatabaseType pdmDatabaseType = boPDMDatabase.getPDMDatabaseType();
          if (pdmDatabaseType != null)
          {
            IBaseValue pkInDB = null;
            if (PDMDatabaseType.MSSQL.equals(pdmDatabaseType))
            {
              pkInDB = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL).getPrimaryKeyInDB();
            }
            else if (PDMDatabaseType.ORACLE.equals(pdmDatabaseType))
            {
              pkInDB = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE).getPrimaryKeyInDB();
            }
            if (pkInDB != null)
            {
              ISCOperator operator = getPermanentSearchCriteria(aPnContext).getOperator(BOBasePDMDatabaseServerDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL);
              operator.addBaseValue(pkInDB);
            }
          }
        }
      }
    }
  }
}
