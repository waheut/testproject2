// Planon Enterprise Edition Source file: BOBasePDMRequest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

/**
 * BOBasePDMRequest
 */
public class BOBasePDMRequest extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequest object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOBasePDMRequest(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
