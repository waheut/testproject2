// Planon Enterprise Edition Source file: PDMDatabaseVersionOracleReferenceField.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

/**
 * PDMDatabaseVersionOracleReferenceField
 */
public class PDMDatabaseVersionOracleReferenceField extends ReferenceIntegerField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseVersionOracleReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMDatabaseVersionOracleReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }
}
