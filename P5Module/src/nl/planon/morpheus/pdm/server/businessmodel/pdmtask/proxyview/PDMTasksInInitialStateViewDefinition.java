// Planon Enterprise Edition Source file: PDMTasksInInitialStateViewDefinition.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * view definition to retrieve all tasks in initial state, together with all the information needed
 */
public class PDMTasksInInitialStateViewDefinition extends PVDefinition implements IPDMTasksInInitialStateViewDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinDatabaseOwner;

  private PVJoinDef joinExportPDMFileType;
  private PVJoinDef joinPDMDatabase;
  private PVJoinDef joinPDMDatabaseServer;
  private PVJoinDef joinPDMDatabaseType;
  private PVJoinDef joinPDMFileType;
  private PVJoinDef joinPDMLocation;
  private PVJoinDef joinPDMPrivilegeType;
  private PVJoinDef joinPDMRequest;
  private PVJoinDef joinPDMRequestType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTasksInInitialStateViewDefinition object.
   */
  public PDMTasksInInitialStateViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_TASK);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMTaskDef.PN_PRIMARYKEY, ALIAS_PK_PDMTASK);
    addSelectFieldDef(BOBasePDMTaskDef.PN_SYSSYSTEMTYPE, ALIAS_PDMTASK_SUBTYPE);
    this.joinPDMRequest.addSelectFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY, ALIAS_PK_PDMREQUEST);
    this.joinPDMRequestType.addSelectFieldDef(BOPDMRequestTypeDef.PN_CODE, ALIAS_REQUESTTYPE_CODE);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_PDMLOCATION_REF, ALIAS_PK_PDMDATABASE_LOCATION);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_PRIMARYKEY, ALIAS_PK_PDMDATABASE);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_ACCESSCODE, ALIAS_PDMDATABASE_PASSWORD);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_PDM_DUMPFILE, ALIAS_PDMDATABASE_PDMDUMPFILE);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_SIZE_DATABASE, ALIAS_PDMDATABASE_SIZE);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE, ALIAS_PDMDATABASE_PDMEXPORTFILE);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_OBJECT_OWNER_IN_DUMP, ALIAS_PDMDATABASE_OBJECT_OWNER_IN_DUMP);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_NAME, ALIAS_PDMDATABASE_OBJECT_OWNER_IN_IMPORT);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_TABLESPACENAMES_IN_DUMP, ALIAS_PDMDATABASE_TABLESPACENAME_IN_DUMP);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_SIZE_DATABASE, ALIAS_PDMDATABASE_DUMP_SIZE);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_STOP_ON_FAILURE, ALIAS_PDMDATABASE_STOP_ON_FAILURE);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_OPTION_NO_IMPORT, ALIAS_PDMDATABASE_OPTION_NO_IMPORT);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_TASK_TIMEOUT, ALIAS_PDMDATABASE_TASK_TIMEOUT);
    this.joinDatabaseOwner.addSelectFieldDef(BOAccountDef.PN_ACCOUNTNAME, ALIAS_DATABASE_OWNER_NAME);
    this.joinPDMLocation.addSelectFieldDef(IBOPDMLocationDef.PN_LOCATION, ALIAS_LOCATION);
    this.joinPDMDatabaseServer.addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_CODE, ALIAS_PDMDBSERVER_CODE);
    this.joinPDMDatabaseServer.addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY, ALIAS_PK_PDMDATABASESERVER);
    this.joinPDMDatabaseType.addSelectFieldDef(BOPDMDatabaseTypeDef.PN_CODE, ALIAS_PDMDBTYPE_CODE);
    this.joinPDMFileType.addSelectFieldDef(BOPDMFileTypeDef.PN_CODE, ALIAS_PDMFILETYPE_CODE);
    this.joinExportPDMFileType.addSelectFieldDef(BOPDMFileTypeDef.PN_CODE, ALIAS_EXPORTFILETYPE_CODE);
    this.joinPDMPrivilegeType.addSelectFieldDef(BOPDMPrivilegeTypeDef.PN_CODE, ALIAS_PDMDATABASE_PRIVILEGE_TYPE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinPDMRequest = addInnerJoinDef(BOBasePDMTaskDef.PN_PDMREQUEST_REF);
    this.joinPDMRequestType = this.joinPDMRequest.addInnerJoinDef(BOBasePDMRequestDef.PN_REQUESTTYPE_REF);
    this.joinPDMDatabase = this.joinPDMRequest.addInnerJoinDef(BOBasePDMRequestDef.PN_DATABASE_REF);
    this.joinPDMLocation = this.joinPDMDatabase.addInnerJoinDef(BOPDMDatabaseDef.PN_PDMLOCATION_REF);
    this.joinPDMDatabaseServer = this.joinPDMDatabase.addLeftJoinDef(BOPDMDatabaseDef.PN_DATABASESERVER_REF);
    this.joinPDMDatabaseType = this.joinPDMDatabase.addLeftJoinDef(BOPDMDatabaseDef.PN_DATABASE_TYPE_REF);
    this.joinPDMFileType = this.joinPDMDatabase.addLeftJoinDef(BOPDMDatabaseDef.PN_FILE_TYPE_REF);
    this.joinExportPDMFileType = this.joinPDMDatabase.addLeftJoinDef(BOPDMDatabaseDef.PN_EXPORT_FILE_TYPE);
    this.joinDatabaseOwner = this.joinPDMDatabase.addLeftJoinDef(BOPDMDatabaseDef.PN_ACCOUNT_DATABASEOWNER_REF);
    this.joinPDMPrivilegeType = this.joinPDMDatabase.addLeftJoinDef(BOPDMDatabaseDef.PN_PRIVILEGE_TYPE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    IBaseValue bvStateInitialImport = getPKInitialStateTask(aPnContext, BOTypePDMP5Module.PDM_TASK_IMPORT);
    IBaseValue bvStateInitialAnalyzeImport = getPKInitialStateTask(aPnContext, BOTypePDMP5Module.PDM_TASK_ANALYZE_IMPORT);
    IBaseValue bvStateInitialExport = getPKInitialStateTask(aPnContext, BOTypePDMP5Module.PDM_TASK_EXPORT);
    IBaseValue bvStateInitialDelete = getPKInitialStateTask(aPnContext, BOTypePDMP5Module.PDM_TASK_DELETE);

    addWhereDef(IBOBasePDMTaskDef.PN_REF_BOSTATE, SCOperatorType.IN, bvStateInitialAnalyzeImport, bvStateInitialImport, bvStateInitialExport, bvStateInitialDelete);
  }


  /**
   * Get primary key of initial state of certain task
   *
   * @param  aPnContext
   * @param  aBOType
   *
   * @return primary key of initial state
   *
   * @throws PnErrorListException
   */
  private IBaseValue getPKInitialStateTask(PnContext aPnContext, IBOType aBOType) throws PnErrorListException
  {
    return aPnContext.getBODefinition(aBOType).getStateControllerDef().getSystemStateByPnName(IBOBasePDMTaskDef.STATE_INITIAL).getPrimaryKeyInDB(aPnContext).getBaseValue();
  }
}
