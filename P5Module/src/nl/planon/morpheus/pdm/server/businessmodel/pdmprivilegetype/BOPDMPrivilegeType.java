// Planon Enterprise Edition Source file: BOPDMPrivilegeType.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMPrivilegeType
 */
public class BOPDMPrivilegeType extends BOSystemCode
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMPrivilegeType object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMPrivilegeType(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
