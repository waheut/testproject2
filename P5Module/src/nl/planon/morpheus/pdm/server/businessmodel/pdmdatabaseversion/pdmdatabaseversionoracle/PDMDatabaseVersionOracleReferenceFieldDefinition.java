// Planon Enterprise Edition Source file: PDMDatabaseVersionOracleReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseVersionOracleReferenceFieldDefinition
 */
public class PDMDatabaseVersionOracleReferenceFieldDefinition extends ReferenceIntegerFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseVersionOracleReferenceFieldDefinition object.
   *
   * @param aFreeFieldConversionInfo
   */
  public PDMDatabaseVersionOracleReferenceFieldDefinition(FreeFieldConversionInfo aFreeFieldConversionInfo)
  {
    this(aFreeFieldConversionInfo.getBODefinition(), aFreeFieldConversionInfo.getPnName());
    setDB(aFreeFieldConversionInfo.getDBName(), aFreeFieldConversionInfo.getDBType());
  }


  /**
   * Creates a new PDMDatabaseVersionOracleReferenceFieldDefinition object.
   *
   * <p>Default constructor to retrieve an instance of the field definition. Use
   * FieldTypeManager.constructFieldDefinition to construct a field definition</p>
   *
   * @param aBODefinition BusinessObject definition
   * @param aBOType       BOType
   */
  public PDMDatabaseVersionOracleReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMDatabaseVersionOracleReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   */
  public PDMDatabaseVersionOracleReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName)
  {
    super(aBODefinition, aPlanonName, BOTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE);
    setPlanonFieldType(FieldTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE);
  }


  /**
   * Creates a new PDMDatabaseVersionOracleReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   * @param aDBName       the name of the database field.
   */
  public PDMDatabaseVersionOracleReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName, String aDBName)
  {
    this(aBODefinition, aPlanonName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public boolean isFreeFieldConversionAllowed()
  {
    return true;
  }


  /**
   * Create a PDMDatabaseVersionOracleReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return PDMDatabaseVersionOracleReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMDatabaseVersionOracleReferenceField(aBO, this);
  }
}
