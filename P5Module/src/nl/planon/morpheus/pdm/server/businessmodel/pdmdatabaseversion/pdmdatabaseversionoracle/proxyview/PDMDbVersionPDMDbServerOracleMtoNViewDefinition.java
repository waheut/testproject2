// Planon Enterprise Edition Source file: PDMDbVersionPDMDbServerMtoNViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle.proxyview;

import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDbVersionPDMDbServerMtoNViewDefinition
 */
public class PDMDbVersionPDMDbServerOracleMtoNViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDbVersionPDMDbServerMtoNViewDefinition object.
   */
  public PDMDbVersionPDMDbServerOracleMtoNViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE);
  }
}
