// Planon Enterprise Edition Source file: VREqualTypeOfDbServerAndDBVersion.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;

/**
 * validation rule to check if the type of database server filled in, matches the type of database
 * version filled in.<br>
 *
 * <p>If the database server is of type Oracle the database version must also be of type Oracle<br>
 * <br>
 * If the database server is of type MSSQL the database version must also be of type MSSQL<br>
 * </p>
 */
public class VREqualTypeOfDbServerAndDBVersion extends ValidationRule<BOPDMCompatibilityMatrix>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * validation rule to check if the type of database server filled in matches the type of database
   * version filled in.<br>
   *
   * <p>If the database server is of type Oracle the database version must also be of type Oracle
   * <br>
   * <br>
   * If the database server is of type MSSQL the database version must also be of type MSSQL<br>
   * </p>
   *
   * @param  aBO        BOPDMCompatibilityMatrix
   * @param  aOldBO     BOPDMCompatibilityMatrix
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMCompatibilityMatrix aBO, BOPDMCompatibilityMatrix aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    ReferenceField pdmDatabaseServerRefField = aBO.getReferenceFieldByPnName(IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
    ReferenceField pdmDatabaseVersionRefField = aBO.getReferenceFieldByPnName(IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);

    if (!(pdmDatabaseServerRefField.isEmpty() || pdmDatabaseVersionRefField.isEmpty()))
    {
      IBaseValue pkBOTypeDbServer = pdmDatabaseServerRefField.getFieldValueOfReferencedBO(IBOBasePDMDatabaseServerDef.PN_BUSINESSOBJECTDEFINITION_REF);
      IBaseValue pkBOTypeDbVersion = pdmDatabaseVersionRefField.getFieldValueOfReferencedBO(IBOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF);

      if (((pkBOTypeDbServer.equals(aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL).getPrimaryKeyInDB())) //
          && (!pkBOTypeDbVersion.equals(aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL).getPrimaryKeyInDB()))) //
        || ((pkBOTypeDbServer.equals(aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE).getPrimaryKeyInDB())) //
          && (!pkBOTypeDbVersion.equals(aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE).getPrimaryKeyInDB()))))
      {
        PnErrorManager manager = aPnContext.getPnErrorManager();
        PnMessage message = manager.createMessage(ErrorNumberPDMP5Module.EC_DBTYPE_SERVER_DOES_NOT_MATCH_DBTYPE_VERSION);
        message.addStringArgument(pdmDatabaseServerRefField.getLocalizableDisplayValue());
        message.addStringArgument(pdmDatabaseVersionRefField.getLocalizableDisplayValue());
        message.addFieldNameArgument(pdmDatabaseServerRefField);

        manager.addFatal(message);
      }
    }
  }
}
