// Planon Enterprise Edition Source file: ARUpdatePDMLocationLinkedDatabases.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.rule.action;

import java.util.*;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxy.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * Action rule to update the PDM location of the PDM databases linked to this PDM database server,
 * provided that the PDM Location of the PDM database server is changes and the linked PDM database
 * is not in state BUSY. This update will happen only when the isActiveField in
 * BOBasePDMDatabaseServer is changed and made as true.
 *
 * <p>Ref: TEC-2254 for more information.</p>
 */
public class ARUpdatePDMLocationLinkedDatabases extends ActionRule<BOBasePDMDatabaseServer>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * This method checks if the PDM Location of the PDM Database server is changed, and, if so,
   * changes the PDM Location of all PDM Databases that are linked to this PDM Database Server to
   * the same value.
   *
   * @param  aBO        BOBasePDMDatabaseServer
   * @param  aOldBO     BOBasePDMDatabaseServer
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMDatabaseServer aBO, BOBasePDMDatabaseServer aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    Field isActiveField = aBO.getFieldByPnName(BOBasePDMDatabaseServerDef.PN_IS_ACTIVE);
    if (isActiveField.isChanged() && isActiveField.getAsBoolean().booleanValue())
    {
      IBaseValue pdmLocationRef = aBO.getFieldByPnName(BOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF).getBaseValue();
      List<IProxyValue> linkedPDMDatabases = aBO.getLinkedPDMDatabases(aPnContext);
      if (updateRequiredForPDMDatabase(pdmLocationRef, linkedPDMDatabases)) // Check for the need to update
      {
        for (IProxyValue pvPDMDatabase : linkedPDMDatabases)
        {
          BOPDMDatabase boPDMDatabase = (BOPDMDatabase) aPnContext.getBOByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE, pvPDMDatabase.getPrimaryKeyValue());
          String pdmDatabaseState = boPDMDatabase.getSystemState().getPnName();
          // Only update PDM databases that are not in state BUSY
          if (!BOPDMDatabaseDef.STATE_BUSY.equals(pdmDatabaseState))
          {
            boPDMDatabase.getFieldByPnName(BOPDMDatabaseDef.PN_PDMLOCATION_REF).setAsBaseValue(pdmLocationRef);
            boPDMDatabase.saveNoRead(aPnContext);
          }
        }
      }
    }
  }


  /**
   * Check the need for the update of PDM Database. If any of the linked PDMDatabases has a
   * different PDMLocationRef than the PDMLocationRef in the PDMDatabaseServer then an update of all
   * the PDMDatabases is required.
   *
   * @param  aPDMLocationRefInPDMDBServer DOCUMENT ME!
   * @param  aLinkedPDMDatabases          All linked PDM Databases ProxyList
   *
   * @return boolean
   */
  private boolean updateRequiredForPDMDatabase(IBaseValue aPDMLocationRefInPDMDBServer, List<IProxyValue> aLinkedPDMDatabases)
  {
    for (IProxyValue iProxyValue : aLinkedPDMDatabases)
    {
      if (!aPDMLocationRefInPDMDBServer.equals(iProxyValue.getProxyItem(iProxyValue.getColumnNumberByAliasName(BOPDMDatabaseDef.PN_PDMLOCATION_REF)).getBaseValue()))
      {
        return true;
      }
    }
    return false;
  }
}
