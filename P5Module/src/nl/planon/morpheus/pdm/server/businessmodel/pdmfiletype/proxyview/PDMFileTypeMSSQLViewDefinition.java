// Planon Enterprise Edition Source file: PDMFileTypeMSSQLViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.proxyview;

import nl.planon.hades.basevalue.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * PDMFileTypeMSSQLViewDefinition
 */
public class PDMFileTypeMSSQLViewDefinition extends PDMFileTypeViewDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMFileTypeMSSQLViewDefinition object.
   */
  public PDMFileTypeMSSQLViewDefinition()
  {
    super(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_MSSQL_DUMP));
  }
}
