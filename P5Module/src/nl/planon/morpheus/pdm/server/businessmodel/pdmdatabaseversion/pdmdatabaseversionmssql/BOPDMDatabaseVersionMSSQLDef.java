// Planon Enterprise Edition Source file: BOPDMDatabaseVersionMSSQLDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

/**
 * BOPDMDatabaseVersionMSSQLDef
 */
public class BOPDMDatabaseVersionMSSQLDef extends BOBasePDMDatabaseVersionDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionMSSQLDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDatabaseVersionMSSQLDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDatabaseVersionMSSQL(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNInContextPVType()
  {
    return PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_MSSQL_IN_CONTEXT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNPVType()
  {
    return PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_MSSQL;
  }
}
