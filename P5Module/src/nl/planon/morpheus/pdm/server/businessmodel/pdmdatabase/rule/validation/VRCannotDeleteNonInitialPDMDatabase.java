// Planon Enterprise Edition Source file: VRCannotDeleteNonInitialPDMDatabase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * Validation rule to prevent deleting PDM database BO that is not in initial state.
 */
public class VRCannotDeleteNonInitialPDMDatabase extends ValidationRule<BOPDMDatabase>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType ERROR_MESSAGE = ErrorNumberPDMP5Module.EC_CANNOT_DELETE_NON_INITIAL_PDMDATABASE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * It is not possible to delete a PDM Database that is not in initial state.
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    String pdmDatabaseState = aBO.getSystemState().getPnName();
    if (!IBOPDMDatabaseDef.STATE_INITIAL.equals(pdmDatabaseState))
    {
      aPnContext.getPnErrorManager().add(ERROR_MESSAGE);
    }
  }
}
