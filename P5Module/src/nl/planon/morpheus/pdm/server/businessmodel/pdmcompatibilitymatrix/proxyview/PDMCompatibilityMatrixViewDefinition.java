// Planon Enterprise Edition Source file: PDMCompatibilityMatrixViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;

/**
 * PDMCompatibilityMatrixViewDefinition
 */
public class PDMCompatibilityMatrixViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMCompatibilityMatrixViewDefinition object.
   */
  public PDMCompatibilityMatrixViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(BOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
    addSelectFieldDef(BOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(BOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
    addSearchFieldDef(BOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
  }
}
