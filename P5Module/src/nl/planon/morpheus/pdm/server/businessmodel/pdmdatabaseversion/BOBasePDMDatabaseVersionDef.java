// Planon Enterprise Edition Source file: BOBasePDMDatabaseVersionDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodel.businessobject.system.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * BOPDMDatabaseVersionDef
 */
public class BOBasePDMDatabaseVersionDef extends BODefinition implements IBOBasePDMDatabaseVersionDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Database fields
  private static final String DB_CODE = DbFieldName.CODE;
  private static final String DB_NAME = DbFieldName.NAME;
  private static final String DB_FK_BODEFINITION = "FK_BODEFINITION";
  private static final String DB_VERSION = "DATABASEVERSION";

  private static final String TBL_NAME = "PLN_PDMDATABASEVERSION";

  public static final String DISPLAY_FORMAT = getDisplayFormat(PN_CODE, PN_NAME);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOBasePDMDatabaseVersionDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMAddSubTypeDef(this, PN_BOM_ADD));
    addBOMDef(new BOMCopyDef(this, PN_BOM_COPY));
    addBOMDef(new BOMDeleteDef(this, PN_BOM_DELETE));
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
    addBOMDef(new BOMMtoNDef(this, PN_BOM_LINK_PDMDBSERVER, MtoNAssociationTypePDMP5Module.PN_PDMDBVERSION_MTON_PDMDBSERVER, getMtoNInContextPVType(), getMtoNPVType()));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    addFieldDef(new SysChangeDateTimeFieldDefinition(this));
    addFieldDef(new SysInsertDateTimeFieldDefinition(this));

    {
      FieldDefinition fieldDef = new SysChangeAccountReferenceFieldDefinition(this);
      fieldDef.setAndFreezePlanonReadOnly(true);
      fieldDef.setPlanonStandardCopy(false);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new SysInsertAccountReferenceFieldDefinition(this);
      fieldDef.setAndFreezePlanonReadOnly(true);
      fieldDef.setPlanonStandardCopy(false);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new StringFieldDefinition(this, PN_CODE, DB_CODE, 20);
      fieldDef.setAndFreezePlanonMandatory(true);
      fieldDef.setAndFreezePlanonUnique(true);
      fieldDef.setPlanonQuickSearch(true);
      setLookupField(fieldDef);
      addFieldDef(fieldDef);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_NAME, DB_NAME, 255);
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      stringFieldDef.setPlanonQuickSearch(true);
      addFieldDef(stringFieldDef);
    }
    {
      SystemBODefinitionReferenceFieldDefinition refFieldDef = new SystemBODefinitionReferenceFieldDefinition(this, PN_BUSINESSOBJECTDEFINITION_REF, DB_FK_BODEFINITION);
      refFieldDef.setAndFreezePlanonStandardCopy(true);
      refFieldDef.setAndFreezePlanonMandatory(true);
      refFieldDef.setAndFreezePlanonInSelection(true);
      refFieldDef.removeSearchOperatorType(SCOperatorType.NOT_NULL, SCOperatorType.NULL);
      addFieldDef(refFieldDef);
    }
    {
      FieldDefinition fieldDef = new MtoNPVTypeFieldDefinition(this, PN_PDMDBSERVERDETAIL, PVTypePDMP5Module.PDMDBSERVER_IN_CONTEXT_OF_PDMDBVERSION);
      addFieldDef(fieldDef);
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateBaseType()
  {
    setSystemSubTypeFieldDef(PN_BUSINESSOBJECTDEFINITION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOBasePDMDatabaseVersion(this);
  }


  /**
   * get PVType to retrieve all items on the N side (PDM Database Servers) in context of the M side
   * (PDM Database Versions) of the association.
   *
   * @return PVType to retrieve all items on the N side in context of the M side of the association
   */
  protected IPVType getMtoNInContextPVType()
  {
    return null;
  }


  /**
   * get PVType to retrieve all items on the N side (PDM Database Servers) of the association.
   *
   * @return PVType to retrieve all items on the N side of the association
   */
  protected IPVType getMtoNPVType()
  {
    return null;
  }
}
