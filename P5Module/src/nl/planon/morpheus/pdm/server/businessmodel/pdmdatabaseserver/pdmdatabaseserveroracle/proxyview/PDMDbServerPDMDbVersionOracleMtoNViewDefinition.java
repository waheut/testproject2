// Planon Enterprise Edition Source file: PDMDbServerPDMDbVersionOracleMtoNViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle.proxyview;

import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDbServerPDMDbVersionOracleMtoNViewDefinition
 */
public class PDMDbServerPDMDbVersionOracleMtoNViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDbServerPDMDbVersionOracleMtoNViewDefinition object.
   */
  public PDMDbServerPDMDbVersionOracleMtoNViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE);
  }
}
