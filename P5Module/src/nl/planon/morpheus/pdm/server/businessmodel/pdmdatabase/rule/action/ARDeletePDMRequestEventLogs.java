// Planon Enterprise Edition Source file: ARDeletePDMRequestEventLogs.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.action;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlogbusinessobject.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * This is registered on {@link BOPDMDatabaseDef}
 *
 * <p>is an action rule which deletes all the event logs associated to all the requests (Import,
 * Export, Delete, etcc..) for the database.</p>
 *
 * <p>Refer: PBL # 21787, PDM - Delete without warnings</p>
 */
public class ARDeletePDMRequestEventLogs extends BusinessAction
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   *
   * <p>First find out the requests assoicated to the current database. Next, identify the event
   * logs to be deleted. Then delete those event logs.</p>
   */
  @Override protected void execute(BusinessObject aBO, BusinessObject aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    // First find out the requests associated to the current database.
    // Then delete the event logs associated to the requests.
    SearchCriteria sc = aPnContext.createPVSearchCriteria(PVTypePDMP5Module.PDMREQUEST_INCONTEXT_DATABASE);
    sc.getOperator(IBOBasePDMRequestDef.PN_DATABASE_REF, SCOperatorType.EQUAL).addBaseValue(aBO.getPrimaryKeyValue());
    IProxyListValue pdmRequestProxyListValues = aPnContext.createBySearchCriteria(sc);

    if (!pdmRequestProxyListValues.isEmpty())
    {
      // Now delete the event logs assoicated with these pdm requests.
      // So first make Event Log as master table.
      PVRuntimeDefinition eventLogPVD = new PVRuntimeDefinition(BOTypeHades.LINKED_EVENT_LOG);
      // Join EventLog with EventLogBusinessObject on
      // EventLogBusinessObject.EventLogRef = BasePDMRequest.Syscode
      PVJoinDef eventLogBusinessObjJoin = eventLogPVD.addInnerJoinDef(BOTypeHades.EVENTLOGBUSINESSOBJECT, IBOEventLog_BusinessObjectDef.PN_EVENTLOG_REF);
      // join with pdm request table.
      PVJoinDef pdmRequestJoin = eventLogBusinessObjJoin.addInnerJoinDef(IBOEventLog_BusinessObjectDef.PN_BO_REF, BOTypePDMP5Module.BASE_PDM_REQUEST, IBOBasePDMRequestDef.PN_PRIMARYKEY);
      PVExpressionNodeDef requestNode = new PVExpressionNodeDef(pdmRequestJoin, IBOBasePDMRequestDef.PN_PRIMARYKEY, SCOperatorType.IN);
      for (IProxyValue pdmRequest : pdmRequestProxyListValues.getProxyValues())
      {
        requestNode.addBaseValue(pdmRequest.getPrimaryKeyValue());
      }
      // make sure joins returns the event logs for this pdm request only.
      pdmRequestJoin.addWhereDef(requestNode);

      List<IProxyValue> eventLogsTobeDeleted = aPnContext.createByPVDefinition(eventLogPVD).getProxyValues();

      // delete event log records.
      for (IProxyValue eventLogPV : eventLogsTobeDeleted)
      {
        BusinessObject bo = aPnContext.getBOByPrimaryKey(eventLogPV);
        bo.delete(aPnContext);
        if (bo instanceof CompositeBO)
        {
          // when composite bo and child bo's are marked for deletion and associations are checked and records are not yet deleted in the database, this
          // is done when the save() is called.
          // when NOT composite the bo is deleted in database.
          bo.saveNoRead(aPnContext);
        }
      }
    }
  }
}
