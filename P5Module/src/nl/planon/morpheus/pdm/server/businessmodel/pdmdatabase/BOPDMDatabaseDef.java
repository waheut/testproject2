// Planon Enterprise Edition Source file: BOPDMDatabaseDef.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;
import nl.planon.hades.statemachine.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.calculator.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.fieldchange.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.action.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasestate.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmlocation.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype.*;

/**
 * Definition of Business object PDMDatabase.
 */
public class BOPDMDatabaseDef extends BODefinition implements IBOPDMDatabaseDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // System state id's
  private static final String ID_INITIAL = "I";
  private static final String ID_BUSY = "B";
  private static final String ID_AVAILABLE = "A";

  // Database fields
  private static final String DB_CODE = DbFieldName.CODE;
  private static final String DB_NAME = DbFieldName.NAME;
  private static final String DB_ACCESSCODE = "ACCESSCODE";
  private static final String DB_FK_BOSTATE = DbFieldName.FK_BOSTATE;
  private static final String DB_DUMPFILE = "DUMPFILE";
  private static final String DB_FK_DATABASESERVER = "FK_PDMDATABASESERVER";
  private static final String DB_FK_ACCOUNT_DATABASEOWNER = "FK_ACCOUNT_DATABASEOWNER";
  private static final String DB_OBJECT_OWNER_IN_DUMP = "OBJECT_OWNER_IN_DUMP";
  private static final String DB_TABLESPACENAMES_IN_DUMP = "TABLESPACENAMES_IN_DUMP";
  private static final String DB_OPTION_NO_IMPORT = "OPTION_NO_IMPORT";
  private static final String DB_STOP_ON_FAILURE = "STOP_ON_FAILURE";
  private static final String DB_PDM_DUMPFILE = "PDM_DUMPFILE";
  private static final String DB_PDM_EXPORTFILE = "PDM_EXPORTFILE";
  private static final String DB_SIZE_DATABASE = "SIZE_DATABASE";
  private static final String DB_TASK_TIMEOUT = "TASK_TIMEOUT";
  private static final String DB_DATABASE_TYPE_REF = "FK_PLC_PDMDATABASETYPE";
  private static final String DB_FILE_TYPE_REF = "FK_PLC_PDMFILETYPE";
  private static final String DB_EXPORT_FILE_TYPE = "FK_PLC_PDM_EXPORT_TYPE";
  private static final String DB_PDMLOCATION_REF = "FK_PDMLOCATION";
  private static final String DB_PLANON_DB_DETAILS = "PLANON_DB_DETAILS";
  private static final String DB_PRIVILEGE_TYPE = "FK_PLC_PDMPRIVILEGETYPE";

  private static final String TBL_NAME = "PLN_PDMDATABASE";

  public static final String DISPLAY_FORMAT = getDisplayFormat(PN_NAME, PN_DESCRIPTION);

  // list of fields that hold specific settings of the database server
  private static final String[] DATABASE_SETTING_FIELDNAMES =
  {
    PN_DATABASESERVER_REF, //
    PN_DATABASE_TYPE_REF, //
    PN_FILE_TYPE_REF, //
    PN_OBJECT_OWNER_IN_DUMP, //
    PN_ACCESSCODE, //
    PN_TABLESPACENAMES_IN_DUMP, //
    PN_NAME
  };

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDatabaseDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get name of fields that hold specific settings for the database
   *
   * @return array of the names of the fields that hold specific settings for the database
   */
  public static String[] getDatabaseSettingFieldNames()
  {
    return DATABASE_SETTING_FIELDNAMES;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMAddDef(this, PN_BOM_ADD));
    BOMDeleteDef bomDeleteDef = new BOMDeleteDef(this, PN_BOM_DELETE);
    bomDeleteDef.setSchedulingAllowed();
    addBOMDef(bomDeleteDef);
    addBOMDef(new BOMCopyDef(this, PN_BOM_COPY));
    addBOMDef(new BOMPDMImportDef(this, PN_BOM_IMPORT_DATABASE));
    addBOMDef(new BOMPDMExportDef(this, PN_BOM_EXPORT_DATABASE));
    BOMPDMDeleteDef boPDMDeleteDef = new BOMPDMDeleteDef(this, PN_BOM_DELETE_DATABASE);
    boPDMDeleteDef.setSchedulingAllowed();
    addBOMDef(boPDMDeleteDef);
    addBOMDef(new BOMPDMReloadInitialDef(this, PN_BOM_RELOAD_INITIAL_DATABASE));
    addBOMDef(new BOMPDMReloadExportDef(this, PN_BOM_RELOAD_EXPORTED_DATABASE));
    addBOMDef(new BOMDownloadPDMDumpDef(this, PN_BOM_DOWNLOAD_DUMPFILE));
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    addFieldDef(new SysChangeDateTimeFieldDefinition(this));
    addFieldDef(new SysInsertDateTimeFieldDefinition(this));
    {
      FieldDefinition fieldDef = new SysChangeAccountReferenceFieldDefinition(this);
      fieldDef.setAndFreezePlanonReadOnly(true);
      fieldDef.setPlanonStandardCopy(false);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new SysInsertAccountReferenceFieldDefinition(this);
      fieldDef.setAndFreezePlanonReadOnly(true);
      fieldDef.setPlanonStandardCopy(false);
      addFieldDef(fieldDef);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_NAME, DB_CODE, 26);
      stringFieldDef.setAndFreezePlanonMandatory(true);
      stringFieldDef.setUpperCase(true);
      stringFieldDef.setAndFreezePlanonUnique(true);
      stringFieldDef.setPlanonQuickSearch(true);
      setLookupField(stringFieldDef);
      addFieldDef(stringFieldDef);
    }
    {
      FieldDefinition fieldDef = new StringFieldDefinition(this, PN_ACCESSCODE, DB_ACCESSCODE, 25);
      fieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new PDMDatabaseStateReferenceFieldDefinition(this, PN_REF_BOSTATE, DB_FK_BOSTATE);
      fieldDef.setAndFreezePlanonMandatory(true);
      fieldDef.setAndFreezePlanonReadOnly(true);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new DocumentFieldDefinition(this, PN_DUMPFILE, DB_DUMPFILE);
      fieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new DocumentFieldDefinition(this, PN_PDM_DUMPFILE, DB_PDM_DUMPFILE);
      fieldDef.setAndFreezePlanonStandardCopy(false); // PCIS 189897.00
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new DocumentFieldDefinition(this, PN_PDM_EXPORTFILE, DB_PDM_EXPORTFILE);
      fieldDef.setAndFreezePlanonStandardCopy(false);
      addFieldDef(fieldDef);
    }
    {
      ReferenceFieldDefinition refFieldDef = new BasePDMDatabaseServerReferenceFieldDefinition(this, PN_DATABASESERVER_REF, DB_FK_DATABASESERVER);
      refFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PN_LOOKUP_PDMDATABASESERVER, PN_DATABASE_TYPE_REF, IBOPDMDBTypePDMFileTypeDef.PN_DATABASE_TYPE_REF);
      addFieldDef(refFieldDef);
    }
    {
      FieldDefinition fieldDef = new AccountReferenceFieldDefinition(this, PN_ACCOUNT_DATABASEOWNER_REF, DB_FK_ACCOUNT_DATABASEOWNER);
      fieldDef.setAndFreezePlanonStandardCopy(false);
      fieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(fieldDef);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_DESCRIPTION, DB_NAME, 255);
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      stringFieldDef.setPlanonQuickSearch(true);
      addFieldDef(stringFieldDef);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_OBJECT_OWNER_IN_DUMP, DB_OBJECT_OWNER_IN_DUMP, 309); // 10*30 + 9 commas
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      addFieldDef(stringFieldDef);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_TABLESPACENAMES_IN_DUMP, DB_TABLESPACENAMES_IN_DUMP, 309); // 10*30 + 9 commas
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      addFieldDef(stringFieldDef);
    }
    {
      StringExtendedFieldDefinition stringFieldDef = new StringExtendedFieldDefinition(this, PN_XML_DATASOURCE_EXAMPLE);
      stringFieldDef.setAndFreezePlanonReadOnly(true);
      stringFieldDef.setFieldValueCalculator(new CFRXMLDataSource(stringFieldDef));
      addFieldDef(stringFieldDef);
    }
    {
      StringExtendedFieldDefinition stringFieldDef = new StringExtendedFieldDefinition(this, PN_PLANON_DB_DETAILS, DB_PLANON_DB_DETAILS, 300);
      stringFieldDef.setAndFreezePlanonInSelection(false);
      stringFieldDef.setStorageFeature(StorageFeature.FULLY_SEARCHABLE);
      addFieldDef(stringFieldDef);
    }
    {
      FieldDefinition fieldDef = new BooleanFieldDefinition(this, PN_OPTION_NO_IMPORT, DB_OPTION_NO_IMPORT);
      fieldDef.setAndFreezePlanonMandatory(true);
      fieldDef.setSystemDefaultValue(BaseBooleanValue.FALSE);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new BooleanFieldDefinition(this, PN_STOP_ON_FAILURE, DB_STOP_ON_FAILURE);
      fieldDef.setAndFreezePlanonMandatory(true);
      fieldDef.setSystemDefaultValue(BaseBooleanValue.FALSE);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new BigDecimalFieldDefinition(this, PN_SIZE_DATABASE, DB_SIZE_DATABASE, 16, 2);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new IntegerFieldDefinition(this, PN_TASK_TIMEOUT, DB_TASK_TIMEOUT, 5);
      addFieldDef(fieldDef);
    }
    {
      ReferenceFieldDefinition refFieldDef = new PDMDatabaseTypeReferenceFieldDefinition(this, PN_DATABASE_TYPE_REF, DB_DATABASE_TYPE_REF);
      refFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PN_LOOKUP_PDMDATABASETYPE, PN_FILE_TYPE_REF, IBOPDMDBTypePDMFileTypeDef.PN_FILE_TYPE_REF);
      // refFieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(refFieldDef);
    }
    {
      ReferenceFieldDefinition refFieldDef = new PDMFileTypeReferenceFieldDefinition(this, PN_FILE_TYPE_REF, DB_FILE_TYPE_REF);
      refFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PN_LOOKUP_PDMFILETYPE, PN_DATABASE_TYPE_REF, IBOPDMDBTypePDMFileTypeDef.PN_DATABASE_TYPE_REF);
      addFieldDef(refFieldDef);
    }
    {
      ReferenceFieldDefinition refFieldDef = new PDMFileTypeReferenceFieldDefinition(this, PN_EXPORT_FILE_TYPE, DB_EXPORT_FILE_TYPE);
      refFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PN_LOOKUP_PDMFILETYPE, PN_DATABASE_TYPE_REF, IBOPDMDBTypePDMFileTypeDef.PN_DATABASE_TYPE_REF);
      refFieldDef.setAndFreezePlanonMandatory(false);
      addFieldDef(refFieldDef);
    }
    {
      PDMLocationReferenceFieldDefinition fieldDef = new PDMLocationReferenceFieldDefinition(this, PN_PDMLOCATION_REF, DB_PDMLOCATION_REF);
      fieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(fieldDef);
    }

    {
      PDMPrivilegeTypeReferenceFieldDefinition refFieldDef = new PDMPrivilegeTypeReferenceFieldDefinition(this, PN_PRIVILEGE_TYPE, DB_PRIVILEGE_TYPE);
      refFieldDef.setPlanonInsertOnly(true);
      refFieldDef.setSystemDefaultValue(SystemCodeReferenceFieldDefinition.getPKValue(BOPDMPrivilegeTypeDef.PRIVILEGE_TYPE_UPGRADE, new BaseStringValue(BOPDMPrivilegeTypeDef.SUBTYPE), aPnContext));
      addFieldDef(refFieldDef);
    }

    addFieldDef(new FreeIntegerFieldDefinition(this, PN_FREEINT1, DbFieldName.FREE01_INTEGER));
    addFieldDef(new FreeIntegerFieldDefinition(this, PN_FREEINT2, DbFieldName.FREE02_INTEGER));
    addFieldDef(new FreeIntegerFieldDefinition(this, PN_FREEINT3, DbFieldName.FREE03_INTEGER));
    addFieldDef(new FreeIntegerFieldDefinition(this, PN_FREEINT4, DbFieldName.FREE04_INTEGER));
    addFieldDef(new FreeIntegerFieldDefinition(this, PN_FREEINT5, DbFieldName.FREE05_INTEGER));
    addFieldDef(new FreeStringFieldDefinition(this, PN_FREESTR1, DbFieldName.FREE01, 100));
    addFieldDef(new FreeStringFieldDefinition(this, PN_FREESTR2, DbFieldName.FREE02, 100));
    addFieldDef(new FreeStringFieldDefinition(this, PN_FREESTR3, DbFieldName.FREE03, 100));
    addFieldDef(new FreeStringFieldDefinition(this, PN_FREESTR4, DbFieldName.FREE04, 100));
    addFieldDef(new FreeStringFieldDefinition(this, PN_FREESTR5, DbFieldName.FREE05, 100));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldSequences()
  {
    addFieldChangeOrder(IBOPDMDatabaseDef.PN_DATABASESERVER_REF, IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF, IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateBOMRules()
  {
    BRAllowImportDatabase brImportDatabase = new BRAllowImportDatabase();
    BRDatabaseImported brDatabaseImported = new BRDatabaseImported();
    BRDatabaseExported brDatabaseExported = new BRDatabaseExported();
    BRAllowDeleteDatabaseBO brDeleteDatabase = new BRAllowDeleteDatabaseBO();

    addBOMRule(PN_BOM_IMPORT_DATABASE, brImportDatabase);
    addBOMRule(PN_BOM_EXPORT_DATABASE, brDatabaseImported);
    addBOMRule(PN_BOM_DELETE_DATABASE, brDatabaseImported);
    addBOMRule(PN_BOM_RELOAD_INITIAL_DATABASE, brDatabaseImported);
    addBOMRule(PN_BOM_RELOAD_EXPORTED_DATABASE, brDatabaseExported);
    addBOMRule(PN_BOM_DOWNLOAD_DUMPFILE, brDatabaseExported);
    addBOMRule(PN_BOM_DELETE, brDeleteDatabase);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDatabase(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateFieldChangeActions()
  {
    addFieldChangeActionToField(PN_DATABASESERVER_REF, new FCTakeOverDatabaseServerFields());
    addFieldChangeActionToField(PN_DATABASE_TYPE_REF, new FCClearDbServerOfDifferentDbType());
    addFieldChangeActionToField(PN_FILE_TYPE_REF, new FCTakeOverDatabaseImportFileType());
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateRules()
  {
    VRIsDBServerActive vrIsDBServerActive = new VRIsDBServerActive();
    VRDbTypeEqualToVersionDbType vrDbTypeEqualToVersionDBType = new VRDbTypeEqualToVersionDbType();
    VRDatabaseToRequestState vrDatabaseToRequestState = new VRDatabaseToRequestState();
    VROnlyWordCharsAllowedInFieldName vrOnlyWordCharsAllowedInFieldName = new VROnlyWordCharsAllowedInFieldName();
    VRPDMLocationEqualToPDMLocationDBServer vrPDMLocationEqualToPDMLocationDBServer = new VRPDMLocationEqualToPDMLocationDBServer();
    VRDbTypeMatchesFileTypeAndDbServerType vrDatabaseTypeMatchesFileType = new VRDbTypeMatchesFileTypeAndDbServerType();
    VRDatabaseServerMustSupportFileType vrDatabaseServerMustSupportFileType = new VRDatabaseServerMustSupportFileType();

    addRuleValidateBeforeDBInsert(vrDbTypeEqualToVersionDBType);
    addRuleValidateBeforeDBInsert(vrOnlyWordCharsAllowedInFieldName);
    addRuleValidateBeforeDBInsert(vrPDMLocationEqualToPDMLocationDBServer);
    addRuleValidateBeforeDBInsert(vrDatabaseTypeMatchesFileType);

    addRuleValidateBeforeDBUpdate(vrIsDBServerActive);
    addRuleValidateBeforeDBUpdate(vrDbTypeEqualToVersionDBType);
    addRuleValidateBeforeDBUpdate(vrDatabaseToRequestState);
    addRuleValidateBeforeDBUpdate(vrDatabaseServerMustSupportFileType);
    addRuleValidateBeforeDBUpdate(vrOnlyWordCharsAllowedInFieldName);
    addRuleValidateBeforeDBUpdate(vrPDMLocationEqualToPDMLocationDBServer);
    addRuleValidateBeforeDBUpdate(vrDatabaseTypeMatchesFileType);

    addRuleValidateBeforeDBDelete(new VRCannotDeleteNonInitialPDMDatabase());
    addRuleValidateBeforeDBDelete(new VRCannotDeleteWhenFileServerIsNotAvailable());

    addRuleBeforeDBDelete(new ARDeletePDMRequestEventLogs()); // Added as part of PBL # 21787, PDM - Delete without warnings
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateSystemStates()
  {
    StateControllerDef stateControllerDef = new StateControllerDef(this);
    this.setStateControllerDef(stateControllerDef);
    stateControllerDef.setSystemStateFieldDef(getFieldDefByPnName(IBOPDMDatabaseDef.PN_REF_BOSTATE));

    // States definition
    final SystemState initialState = stateControllerDef.addSystemState(ID_INITIAL, STATE_INITIAL);
    final SystemState busyState = stateControllerDef.addSystemState(ID_BUSY, STATE_BUSY);
    final SystemState availableState = stateControllerDef.addSystemState(ID_AVAILABLE, STATE_AVAILABLE);

    // state changes
    SystemStateChange stateChange;
    stateChange = stateControllerDef.addSystemStateChange(initialState, busyState);
    stateChange = stateControllerDef.addSystemStateChange(busyState, initialState);
    stateChange = stateControllerDef.addSystemStateChange(busyState, availableState);
    stateChange = stateControllerDef.addSystemStateChange(availableState, busyState);

    // Set (default) initial system state
    stateControllerDef.addInitialSystemState(initialState);
    stateControllerDef.setDefaultInitialSystemState(initialState);
  }
}
