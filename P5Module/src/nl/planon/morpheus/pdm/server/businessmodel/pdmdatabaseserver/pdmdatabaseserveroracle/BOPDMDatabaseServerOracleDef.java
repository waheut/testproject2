// Planon Enterprise Edition Source file: BOPDMDatabaseServerOracleDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * BOPDMDatabaseServerOracleDef
 */
public class BOPDMDatabaseServerOracleDef extends BOBasePDMDatabaseServerDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_TEMPORARY_TABLESPACENAME = PN_TEMPORARY_TABLESPACENAME__NOT_IN_ALL_SUBTYPES;
  public static final String PN_CONNECT_TNS_NAME = PN_CONNECT_TNS_NAME__NOT_IN_ALL_SUBTYPES;
  public static final String PN_PORT_NUMBER = PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES;
  //
  public static final String PN_EXP_LOCATION = PN_EXPORT_LOCATION__NOT_IN_ALL_SUBTYPES;
  public static final String PN_IMP_LOCATION = PN_IMPORT_LOCATION__NOT_IN_ALL_SUBTYPES;
  //
  // list of fields that hold specific settings of the database server
  protected static final String[] DATABASE_SERVER_SETTING_FIELDNAMES =
  { //
    PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES, //
    PN_TEMPORARY_TABLESPACENAME //
  };

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerOracleDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDatabaseServerOracleDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public Set<String> getDatabaseServerSettingFieldNames()
  {
    Set<String> result = super.getDatabaseServerSettingFieldNames();

    result.add(PN_TEMPORARY_TABLESPACENAME);
    result.add(PN_CONNECT_TNS_NAME);
    result.add(PN_PORT_NUMBER);

    return result;
  }


  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDatabaseServerOracle(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateFieldDefs(PnContext aPnContext)
  {
    removeFieldDefByPnName(PN_CODE_PAGE__NOT_IN_ALL_SUBTYPES);
    removeFieldDefByPnName(PN_SORT_ORDER__NOT_IN_ALL_SUBTYPES);
    removeFieldDefByPnName(PN_DATABASEVERSION_REF);

    PDMFileTypeReferenceFieldDefinition pdmFileTypeRefFieldDef = (PDMFileTypeReferenceFieldDefinition) getReferenceFieldDefByPnName(PN_FILE_TYPE_REF);
    pdmFileTypeRefFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PDMFILETYPEORACLE);

    PDMDatabaseVersionOracleReferenceFieldDefinition fieldDef = new PDMDatabaseVersionOracleReferenceFieldDefinition(this, PN_DATABASEVERSION_REF, DB_DATABASEVERSION_REF);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNInContextPVType()
  {
    return PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_ORACLE_IN_CONTEXT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNPVType()
  {
    return PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_ORACLE;
  }
}
