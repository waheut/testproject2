// Planon Enterprise Edition Source file: BOPDMDatabase.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMDatabase
 */
public class BOPDMDatabase extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabase object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMDatabase(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void doCopy(BusinessObject aDestinationBO, PnContext aPnContext) throws PnErrorListException
  {
    aDestinationBO.getFieldByPnName(BOPDMDatabaseDef.PN_NAME).setAsObject(getFieldByPnName(IBOPDMDatabaseDef.PN_NAME).getAsString().concat("_2"));
  }


  /**
   * get PDMDatabaseType of the linked database server. There is no direct link between the database
   * server and the database type. It is determined via the database version that is direcly linked
   * to the database server.
   *
   * @param  aPnContext
   *
   * @return Database type of database server, can be null if field database server is not filled
   *
   * @throws PnErrorListException
   */
  public PDMDatabaseType getDatabaseTypeOfServer(PnContext aPnContext) throws PnErrorListException
  {
    if (getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).isEmpty())
    {
      return null;
    }
    else
    {
      BusinessObject boPDMDatabaseServer = getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
      BOBasePDMDatabaseVersion boPDMDatabaseVersion = (BOBasePDMDatabaseVersion) boPDMDatabaseServer.getReferenceFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF).getReferenceBO();
      return boPDMDatabaseVersion.getPDMDatabaseType();
    }
  }


  /**
   * Get the PDM Database Type
   *
   * @return PDM Database Type
   *
   * @throws PnErrorListException
   */
  public PDMDatabaseType getPDMDatabaseType() throws PnErrorListException
  {
    PDMDatabaseTypeReferenceField pdmDatabaseTypeRef = (PDMDatabaseTypeReferenceField) getFieldByPnName(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF);
    return pdmDatabaseTypeRef.getPDMDatabaseType();
  }


  /**
   * Get the PDM File Type
   *
   * @return PDM File Type can be null
   *
   * @throws PnErrorListException
   */
  public PDMFileType getPDMFileType() throws PnErrorListException
  {
    PDMFileTypeReferenceField pdmFileTypeRef = (PDMFileTypeReferenceField) getFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
    return pdmFileTypeRef.getPDMFileType();
  }


  /**
   * Checks whether this pdm database is in status Busy).
   *
   * @return true if the pdm database is in state Busy, false otherwise.
   *
   * @throws PnErrorListException
   */
  public boolean isBusy() throws PnErrorListException
  {
    boolean isBusy = IBOPDMDatabaseDef.STATE_BUSY.equals(getSystemState().getPnName());
    return isBusy;
  }


  /**
   * Checks whether this pdm database is exported (Field ExportDump is filled).
   *
   * @return true if the pdm database is exported, false otherwise.
   *
   * @throws PnErrorListException
   */
  public boolean isExported() throws PnErrorListException
  {
    boolean isExported = (!getFieldByPnName(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE).isEmpty());
    return isExported;
  }


  /**
   * Checks whether this pdm database is already imported (Database is in status Available).
   *
   * @return true if the pdm database is already imported, false otherwise.
   *
   * @throws PnErrorListException
   */
  public boolean isImported() throws PnErrorListException
  {
    boolean isImported = IBOPDMDatabaseDef.STATE_AVAILABLE.equals(getSystemState().getPnName());
    return isImported;
  }


  /**
   * {@inheritDoc}
   *
   * <p>Several fields must be disabled dependent on the status of the PDM Database.</p>
   */
  @Override protected void doCreateBOValue(BOValue aBOValue, PnContext aPnContext) throws PnErrorListException
  {
    String currentState = getCurrentState().getPnName();
    if (!currentState.equals(IBOPDMDatabaseDef.STATE_INITIAL))
    {
      String[] databaseSettingFieldNames = BOPDMDatabaseDef.getDatabaseSettingFieldNames();
      for (String fieldName : databaseSettingFieldNames)
      {
        aBOValue.disableField(fieldName);
      }
    }
    // PCIS 201389: Not allowed to change field PDM location ref when database
    // is in state Busy.
    if (currentState.equals(IBOPDMDatabaseDef.STATE_BUSY))
    {
      aBOValue.disableField(IBOPDMDatabaseDef.PN_PDMLOCATION_REF);
    }

    aBOValue.disableField(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE);
    aBOValue.disableField(IBOPDMDatabaseDef.PN_PLANON_DB_DETAILS);
  }


  /**
   * set value of account owner to current user
   *
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doSetDefaultValueInFields(PnContext aPnContext) throws PnErrorListException
  {
    super.doSetDefaultValueInFields(aPnContext);
    getFieldByPnName(BOPDMDatabaseDef.PN_ACCOUNT_DATABASEOWNER_REF).setAsBaseValue(aPnContext.getUserID());
  }
}
