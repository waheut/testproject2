// Planon Enterprise Edition Source file: BOPDMDBServerDataFileLocationDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.rule.validation.*;

/**
 * BOPDMDBServerDataFileLocationDef
 */
public class BOPDMDBServerDataFileLocationDef extends BODefinition implements IBOPDMDBServerDataFileLocationDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Database fields
  private static final String DB_FK_DATABASESERVER = "FK_PDMDATABASESERVER";
  private static final String DB_FILE_LOCATION_LOCAL = "FILE_LOCATION_LOCAL";
  private static final String DB_FILE_LOCATION_NETWORK = "FILE_LOCATION_NETWORK";

  private static final String TBL_NAME = "PLN_PDMDBSERVER_DATAFILELOC";

  // Display format
  private static final String DISPLAY_FORMAT = getDisplayFormat(PN_DATABASESERVER_REF, PN_FILE_LOCATION_LOCAL);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDBServerDataFileLocationDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMAddDef(this, PN_BOM_ADD));
    addBOMDef(new BOMCopyDef(this, PN_BOM_COPY));
    addBOMDef(new BOMDeleteDef(this, PN_BOM_DELETE));
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    addFieldDef(new SysChangeDateTimeFieldDefinition(this));
    addFieldDef(new SysInsertDateTimeFieldDefinition(this));
    addFieldDef(new SysChangeAccountReferenceFieldDefinition(this));
    addFieldDef(new SysInsertAccountReferenceFieldDefinition(this));

    {
      FieldDefinition fieldDef = new BasePDMDatabaseServerReferenceFieldDefinition(this, PN_DATABASESERVER_REF, DB_FK_DATABASESERVER);
      fieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(fieldDef);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_FILE_LOCATION_LOCAL, DB_FILE_LOCATION_LOCAL, 250);
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      stringFieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(stringFieldDef);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_FILE_LOCATION_NETWORK, DB_FILE_LOCATION_NETWORK, 250);
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      addFieldDef(stringFieldDef);
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDBServerDataFileLocation(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateRules()
  {
    VRNoSpacesesAllowedInFieldsFileLocations vrNoSpacesesAllowedInFieldsFileLocations = new VRNoSpacesesAllowedInFieldsFileLocations();

    addRuleValidateBeforeDBInsert(vrNoSpacesesAllowedInFieldsFileLocations);
    addRuleValidateBeforeDBUpdate(vrNoSpacesesAllowedInFieldsFileLocations);
  }
}
