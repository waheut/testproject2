// Planon Enterprise Edition Source file: BOMPDMDatabaseRequestDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.bom;

import nl.planon.aphrodite.businessobjectsmethod.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

import nl.planon.pdm.core.common.*;

/**
 * BOMPDMDatabaseRequestDef
 */
public abstract class BOMPDMDatabaseRequestDef extends BOMDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMPDMDatabaseRequestDef object.
   *
   * @param aBODef     BusinessObject definition;
   * @param aBOMPnName Planon name of BOM, this name has to be unique for all BOM defs within a
   *                   BODefinition.
   */
  public BOMPDMDatabaseRequestDef(BODefinition aBODef, String aBOMPnName)
  {
    super(aBODef, aBOMPnName, BOMTypeAphrodite.GENERIC);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * returns the bo type of the request
   *
   * @return the bo type of the request
   */
  protected abstract IBOType getBOTypeRequest();


  /**
   * returns the bo type of the task
   *
   * @param  aPDMDatabase BO of database to which task is linked to
   *
   * @return the bo type of the task
   *
   * @throws PnErrorListException
   */
  protected abstract IBOType getBOTypeTask(BOPDMDatabase aPDMDatabase) throws PnErrorListException;


  /**
   * returns the request type
   *
   * @return the request type
   *
   * @throws PnErrorListException
   */
  protected abstract PDMRequestType getRequestType() throws PnErrorListException;


  /**
   * this method checks if a field that is mandatory for a request to be filled, is indeed filled.
   * If not an error message is shown.
   *
   * @param  aPDMDatabase the database BO to which the request applies
   * @param  aFieldName   the field that is supposed to be mandatory
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  protected void checkFieldMandatoryForRequest(final BOPDMDatabase aPDMDatabase, String aFieldName, PnContext aPnContext) throws PnErrorListException
  {
    Field mandatoryField = aPDMDatabase.getFieldByPnName(aFieldName);
    if (mandatoryField.isEmpty())
    {
      PnErrorManager errorManager = aPnContext.getPnErrorManager();
      PnMessage message = errorManager.createMessage(ErrorNumberPDMP5Module.EC_FIELD_MANDATORY_FOR_REQUEST);
      message.addStringArgument(mandatoryField.getLocalizableKey());
      errorManager.addFatal(message); // Throw the error directly, continuing the request is useless.
    }
  }


  /**
   * Provides a hook to subclasses to execute code before the request is created
   *
   * @param  aPDMDatabase
   * @param  aPnContext   aPDMRequest
   *
   * @throws PnErrorListException
   */
  protected void doBeforeCreateRequest(final BOPDMDatabase aPDMDatabase, PnContext aPnContext) throws PnErrorListException
  {
  }


  /**
   * {@inheritDoc}
   */
  @Override protected final BusinessObject execute(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    final IBOValue aBOValue = aBOM.getBOValue();
    final BOPDMDatabase pdmDatabase = (BOPDMDatabase) convertBOValueToBO(aBOValue, aPnContext);

    // check beforehand if request can be executed.
    doBeforeCreateRequest(pdmDatabase, aPnContext);
    //-------------------------------------------------------------

    // first create the request

    BOBasePDMRequest pdmRequest = (BOBasePDMRequest) aPnContext.createBO(getBOTypeRequest());
    pdmRequest.getReferenceFieldByPnName(IBOBasePDMRequestDef.PN_DATABASE_REF).setAsBaseValue(pdmDatabase.getPrimaryKeyValue());
    pdmRequest.getFieldByPnName(IBOBasePDMRequestDef.PN_ACCOUNT_REQUESTOR_REF).setAsBaseValue(aPnContext.getUserID());
    PDMRequestTypeReferenceField pdmRequestTypeField = (PDMRequestTypeReferenceField) pdmRequest.getFieldByPnName(BOBasePDMRequestDef.PN_REQUESTTYPE_REF);
    pdmRequestTypeField.setPDMRequestType(getRequestType());
    pdmRequest.saveAndRead(aPnContext);

    IBOType boTypeTask = getBOTypeTask(pdmDatabase);
    BOBasePDMTask boPDMTask = (BOBasePDMTask) aPnContext.createBO(boTypeTask);
    boPDMTask.getReferenceFieldByPnName(IBOBasePDMTaskDef.PN_PDMREQUEST_REF).setAsBaseValue(pdmRequest.getPrimaryKeyValue());

    boPDMTask.saveAndRead(aPnContext);

    // After the request is created, the status of the database has to be set to
    // 'in queue'
    pdmDatabase.setCurrentStateByPnName(IBOPDMDatabaseDef.STATE_BUSY, aPnContext);
    BusinessObject result = pdmDatabase.saveAndRead(aPnContext);

    return result;
  }
}
