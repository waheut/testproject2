// Planon Enterprise Edition Source file: AnalyzeGetByPKRequestProxyViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmanalyze.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * ProxyViewDefinition
 */
public class AnalyzeGetByPKRequestProxyViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinFileType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AnalyzeProxyViewDefinition object.
   */
  public AnalyzeGetByPKRequestProxyViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_ANALYZE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOPDMAnalyzeDef.PN_FILE_TYPE_REF);
    addSelectFieldDef(IBOPDMAnalyzeDef.PN_PRIMARYKEY);
    addSelectFieldDef(IBOPDMAnalyzeDef.PN_PDMREQUEST_REF);
    this.joinFileType.addSelectFieldDef(BOPDMFileTypeDef.PN_CODE);
  }


  /**
   * join for finding the right PDMFileTypeCode by the primary key
   *
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinFileType = addInnerJoinDef(IBOPDMAnalyzeDef.PN_FILE_TYPE_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOPDMAnalyzeDef.PN_PDMREQUEST_REF);
  }
}
