// Planon Enterprise Edition Source file: FCDataFileLocationMustBeFilling.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.fieldchange;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * FCTakeOverDatabaseImportFileType
 */
public class FCDataFileLocationMustBeFilling extends FieldChangeAction<BOBasePDMDatabaseServer>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * if field ImportFiletype filling, ExportFileType get the same
   *
   * @param  aFCContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void execute(FieldChangeActionContext<BOBasePDMDatabaseServer> aFCContext, PnContext aPnContext) throws PnErrorListException
  {
    BOBasePDMDatabaseServer boPDMDatabaseServer = aFCContext.getBusinessObject();

    Field isActive = boPDMDatabaseServer.getFieldByPnName(BOBasePDMDatabaseServerDef.PN_IS_ACTIVE);
    Integer sysCode = boPDMDatabaseServer.getPrimaryKeyField().getAsInteger();

    if (!isActive.isEmpty())
    {
      if (isActive.getAsBoolean().booleanValue())
      {
        SearchCriteria sc = aPnContext.createPVSearchCriteria(PVTypePDMP5Module.PDM_DATA_FILE_LOCATION_MUST_BE_FILLED);
        sc.getOperator(IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF, SCOperatorType.EQUAL).addValue(sysCode);
        IProxyListValue plvDatabaseCount = aPnContext.createBySearchCriteria(sc);
        if (plvDatabaseCount.isEmpty())
        {
          PnErrorManager manager = aPnContext.getPnErrorManager();
          PnMessage message = manager.createMessage(ErrorNumberPDMP5Module.EC_NO_SUPPORTED_DATA_FILE_TYPES_ADDED);
          manager.addFatal(message);
        }
      }
    }
  }
}
