// Planon Enterprise Edition Source file: PDMTaskExecuteStateReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtaskexecutestate;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.state.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMTaskExecuteStateReferenceFieldDefinition
 */
public class PDMTaskExecuteStateReferenceFieldDefinition extends BaseBOStateReferenceFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTaskExecuteStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aBOType
   */
  public PDMTaskExecuteStateReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMTaskExecuteStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   */
  public PDMTaskExecuteStateReferenceFieldDefinition(BODefinition aBODefinition, String aPnName)
  {
    super(aBODefinition, aPnName, BOTypePDMP5Module.PDM_TASK_EXECUTE_STATE, FieldTypePDMP5Module.PDM_TASK_EXECUTE_STATE);
  }


  /**
   * Creates a new PDMTaskExecuteStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   * @param aDBName
   */
  public PDMTaskExecuteStateReferenceFieldDefinition(BODefinition aBODefinition, String aPnName, String aDBName)
  {
    this(aBODefinition, aPnName);
    setDB(aDBName);
  }
}
