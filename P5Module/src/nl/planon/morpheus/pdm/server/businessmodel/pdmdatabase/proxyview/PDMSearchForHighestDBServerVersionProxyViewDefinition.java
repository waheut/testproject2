// Planon Enterprise Edition Source file: PDMSearchForHighestDBServerVersionProxyViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.businessobject.system.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * PDMSearchForHighestDBServerVersionProxyViewDefinition
 */
public class PDMSearchForHighestDBServerVersionProxyViewDefinition extends BasePDMSearchForDatabaseServerProxyViewDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

// BE AWARE hardcoded sub queries....
  public static final String TABLE_ALIAS_DATABASE = "db";
  public static final String TABLE_ALIAS_DB_SERVER = "srv";
  public static final String TABLE_ALIAS_PLANCODE = "planCode";
  public static final String TABLE_ALIAS_SYSTEM_BO_DEFINITION = "boDef";
  public static final String TABLE_ALIAS_VERSION = "ver";
  public static final String PDM_DATABASE_VERSION_MSSQL = BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL.getPnName();

  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinDefSystemBO;
  private PVJoinDef joinFileTypeCode;
  private PVJoinDef joinVersion;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY, ALIAS_PRIMARYKEY);
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_CODE, ALIAS_SERVER_CODE);
    this.joinVersion.addSelectFieldDef(IBOBasePDMDatabaseVersionDef.PN_CODE, ALIAS_VERSION_NUMBER);
    this.joinFileTypeCode.addSelectFieldDef(BOSystemCodeDef.PN_CODE, ALIAS_FILE_TYPE_CODE);
    addSelectFieldDef(BOBasePDMDatabaseServerDef.PN_ADMIN_USER, ALIAS_ADMIN_USER);
    addSelectFieldDef(BOBasePDMDatabaseServerDef.PN_ADMIN_PASS, ALIAS_ADMIN_PASSWORD);
    addFieldDef(new PVDummyStringFieldDef(BOBasePDMDatabaseServerDef.PN_CONNECTION_URL, ALIAS_CONNECTION_URL)); // calculated field
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinVersion = addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);
    this.joinDefSystemBO = this.joinVersion.addInnerJoinDef(IBOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF);
    this.joinFileTypeCode = addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF);

    this.joinDefSystemBO.setAliasTableName(TABLE_ALIAS_SYSTEM_BO_DEFINITION);
    getMasterJoinDef().setAliasTableName(TABLE_ALIAS_DB_SERVER);
    this.joinFileTypeCode.setAliasTableName(TABLE_ALIAS_PLANCODE);
    this.joinVersion.setAliasTableName(TABLE_ALIAS_VERSION);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createOrderDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createOrderDefs(aPnContext);
    addSortDef(new PVSortFieldDef(IBOBasePDMDatabaseVersionDef.PN_CODE, ALIAS_VERSION_NUMBER, false));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDefSystemBO.addSearchFieldDef(BOSystemBODefinitionDef.PN_PNNAME);
    this.joinFileTypeCode.addSearchFieldDef(BOSystemCodeDef.PN_CODE);
    getMasterJoinDef().addSearchFieldDef(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(new PVExpressionNodeDef(getMasterJoinDef(), BOBasePDMDatabaseServerDef.PN_IS_ACTIVE, SCOperatorType.EQUAL, BaseBooleanValue.TRUE));
  }
}
