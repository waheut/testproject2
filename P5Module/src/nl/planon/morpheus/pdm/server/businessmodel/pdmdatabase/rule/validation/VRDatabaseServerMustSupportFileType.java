// Planon Enterprise Edition Source file: VRDatabaseServerMustSupportFileType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * VRDatabaseServerMustSupportFileType
 */
public class VRDatabaseServerMustSupportFileType extends ValidationRule<BOPDMDatabase>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType ERROR_MESSAGE = ErrorNumberPDMP5Module.EC_DBSERVER_DONT_EQUAL_WITH_DATABASE_FILETYPE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * It is not possible to fill in a database type that is not equal to the database type of the
   * database version that is linked to the filled in database server
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    ReferenceField pdmFileTypeRefField = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
    ReferenceField pdmDatabaseServerRefField = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    if (!(pdmDatabaseServerRefField.isEmpty() || pdmFileTypeRefField.isEmpty()))
    {
      IBaseValue pkDBServer = pdmDatabaseServerRefField.getFieldValueOfReferencedBO(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF);
      IBaseValue dbFileType = pdmFileTypeRefField.getBaseValue();
      IBaseValue pkOraExp = BOPDMFileTypeDef.getPKValue(PDMFileType.ORA_EXP.getCode(), aPnContext);
      IBaseValue pkDatapump = BOPDMFileTypeDef.getPKValue(PDMFileType.DATAPUMP.getCode(), aPnContext);
      IBaseValue pkMSSQL = BOPDMFileTypeDef.getPKValue(PDMFileType.MSSQL_DUMP.getCode(), aPnContext);
      IBaseValue pkDandO = BOPDMFileTypeDef.getPKValue(PDMFileType.DATAPUMP_AND_EXP.getCode(), aPnContext);
      if (pkDBServer.equals(pkOraExp) || pkDBServer.equals(pkDatapump))
      {
        boolean equal = pkDBServer.equals(dbFileType);
        equal = pkDBServer.equals(pkDandO) ? true : equal;
        if (!equal)
        {
          throwError(aPnContext);
        }
      }
      else if (pkDBServer.equals(pkMSSQL))
      {
        if (!pkDBServer.equals(dbFileType))
        {
          throwError(aPnContext);
        }
      }
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aPnContext DOCUMENT ME!
   *
   * @throws PnErrorListException DOCUMENT ME!
   */
  private static void throwError(PnContext aPnContext) throws PnErrorListException
  {
    PnErrorManager manager = aPnContext.getPnErrorManager();
    PnMessage message = manager.createMessage(ERROR_MESSAGE);
//        message.addStringArgument(pdmDbTypeOfDatabaseRefField.getLocalizableDisplayValue());
//        message.addStringArgument(serverDatabaseType.getCode() + " " + serverDatabaseType.getName());
//        message.addFieldNameArgument(pdmDatabaseServerRefField);

    manager.addFatal(message);
  }
}
