// Planon Enterprise Edition Source file: PDMDatabaseVersionMSSQLReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseVersionMSSQLReferenceFieldDefinition
 */
public class PDMDatabaseVersionMSSQLReferenceFieldDefinition extends ReferenceIntegerFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseVersionMSSQLReferenceFieldDefinition object.
   *
   * @param aFreeFieldConversionInfo
   */
  public PDMDatabaseVersionMSSQLReferenceFieldDefinition(FreeFieldConversionInfo aFreeFieldConversionInfo)
  {
    this(aFreeFieldConversionInfo.getBODefinition(), aFreeFieldConversionInfo.getPnName());
    setDB(aFreeFieldConversionInfo.getDBName(), aFreeFieldConversionInfo.getDBType());
  }


  /**
   * Creates a new PDMDatabaseVersionMSSQLReferenceFieldDefinition object.
   *
   * <p>Default constructor to retrieve an instance of the field definition. Use
   * FieldTypeManager.constructFieldDefinition to construct a field definition</p>
   *
   * @param aBODefinition BusinessObject definition
   * @param aBOType       BOType
   */
  public PDMDatabaseVersionMSSQLReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMDatabaseVersionMSSQLReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   */
  public PDMDatabaseVersionMSSQLReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName)
  {
    super(aBODefinition, aPlanonName, BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL);
    setPlanonFieldType(FieldTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL);
  }


  /**
   * Creates a new PDMDatabaseVersionMSSQLReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   * @param aDBName       the name of the database field.
   */
  public PDMDatabaseVersionMSSQLReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName, String aDBName)
  {
    this(aBODefinition, aPlanonName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public boolean isFreeFieldConversionAllowed()
  {
    return true;
  }


  /**
   * Create a PDMDatabaseVersionMSSQLReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return PDMDatabaseVersionMSSQLReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMDatabaseVersionMSSQLReferenceField(aBO, this);
  }
}
