// Planon Enterprise Edition Source file: BOPDMAnalyzeDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmanalyze;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * BOPDMAnalyze
 */
public class BOPDMAnalyzeDef extends BODefinition implements IBOPDMAnalyzeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  protected static final String VR_ANALYZED_FILETYPE_PER_REQUEST = "VrAnalyzedFileTypePerRequest";

  // Database fields
  private static final String DB_FK_PDMREQUEST = "FK_PDMREQUEST";
  private static final String DB_FK_PLC_PDMFILETYPE = "FK_PLC_PDMFILETYPE";

  private static final String TBL_NAME = "PLN_PDMANALYZEDFILETYPES";

  // Display format
  private static final String DISPLAY_FORMAT = getDisplayFormat(PN_PDMREQUEST_REF, PN_FILE_TYPE_REF);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMAnalyzeDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMAnalyzeDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   *
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMAddDef(this, PN_BOM_ADD));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    removeSysDataSectionReferenceFieldDefinition();

    /* ---------------------------------------------------------------------- */
    ReferenceFieldDefinition refFieldDef = new PDMFileTypeReferenceFieldDefinition(this, PN_FILE_TYPE_REF, DB_FK_PLC_PDMFILETYPE);
    refFieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(refFieldDef);
    /* ---------------------------------------------------------------------- */
    refFieldDef = new BasePDMRequestReferenceFieldDefinition(this, PN_PDMREQUEST_REF, DB_FK_PDMREQUEST);
    refFieldDef.setAssociationActionType(AssociationActionType.DELETE);
//    refFieldDef.setAndFreezePlanonReadOnly(true);
    refFieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(refFieldDef);

    // Unique field sets: combination of analyzed filetypes per request must be unique
    UniqueSetOfFieldDefs usfAnalyzedFileTypePerRequest = new UniqueSetOfFieldDefs(VR_ANALYZED_FILETYPE_PER_REQUEST);
    usfAnalyzedFileTypePerRequest.addFieldDef(getFieldDefByPnName(PN_PDMREQUEST_REF));
    usfAnalyzedFileTypePerRequest.addFieldDef(getFieldDefByPnName(PN_FILE_TYPE_REF));
    addUniqueFieldSet(usfAnalyzedFileTypePerRequest);
  }
}
