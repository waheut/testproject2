// Planon Enterprise Edition Source file: BOPDMRequestReloadExportDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.reloadexportrequest;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMRequestReloadExportDef
 */
public class BOPDMRequestReloadExportDef extends BOBasePDMRequestDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Type
  public static final PDMRequestType PDMREQUEST_TYPE = PDMRequestType.RELOAD_EXPORT;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestReloadExportDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMRequestReloadExportDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_REQUEST_RELOAD_EXPORT;
  }


  /**
   * {@inheritDoc}
   *
   * @throws PnErrorListException
   */
  @Override protected void doCreateSubType(PnContext aPnContext) throws PnErrorListException
  {
    setParentBOType(BOTypePDMP5Module.BASE_PDM_REQUEST);
    setSystemSubTypeFieldValue(getPDMRequestTypePrimaryKeyByCode(aPnContext, PDMREQUEST_TYPE, true));
  }
}
