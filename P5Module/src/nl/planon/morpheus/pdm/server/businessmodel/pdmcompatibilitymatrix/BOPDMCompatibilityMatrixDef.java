// Planon Enterprise Edition Source file: BOPDMCompatibilityMatrixDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.rule.validation.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

/**
 * BOPDMCompatibilityMatrixDef
 */
public class BOPDMCompatibilityMatrixDef extends BODefinition implements IBOPDMCompatibilityMatrixDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  protected static final String VR_DBVERSION_ASSIGNED_TO_DBSERVER = "VrDbVersionAssignedToDbServer";

  // Database fields
  private static final String DB_FK_DATABASESERVER = "FK_PDMDATABASESERVER";
  private static final String DB_FK_DATABASEVERSION = "FK_PDMDATABASEVERSION";

  private static final String TBL_NAME = "PLN_PDMCOMPATIBILITYMATRIX";

  // Display format
  private static final String DISPLAY_FORMAT = getDisplayFormat(PN_DATABASESERVER_REF, PN_DATABASEVERSION_REF);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMCompatibilityMatrixDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMCompatibilityMatrixDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMAddDef(this, PN_BOM_ADD));
    addBOMDef(new BOMCopyDef(this, PN_BOM_COPY));
    addBOMDef(new BOMDeleteDef(this, PN_BOM_DELETE));
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    FieldDefinition fieldDef;
    ReferenceFieldDefinition refFieldDef;

    fieldDef = new SysChangeDateTimeFieldDefinition(this);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysInsertDateTimeFieldDefinition(this);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysChangeAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    fieldDef.setPlanonStandardCopy(false);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysInsertAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    fieldDef.setPlanonStandardCopy(false);
    addFieldDef(fieldDef);

    /* ---------------------------------------------------------------------- */
    refFieldDef = new BasePDMDatabaseServerReferenceFieldDefinition(this, PN_DATABASESERVER_REF, DB_FK_DATABASESERVER);
    refFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PN_POPUP_DBSERVERS_BY_DBVERSION);
    refFieldDef.setAndFreezePlanonMandatory(true);
    refFieldDef.setPlanonQuickSearch(true);
    addFieldDef(refFieldDef);
    /* ---------------------------------------------------------------------- */
    refFieldDef = new BasePDMDatabaseVersionReferenceFieldDefinition(this, PN_DATABASEVERSION_REF, DB_FK_DATABASEVERSION);
    refFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PN_POPUP_DBVERSIONS_BY_DBSERVER);
    refFieldDef.setAndFreezePlanonMandatory(true);
    refFieldDef.setPlanonQuickSearch(true);
    addFieldDef(refFieldDef);

    // Unique field sets: combination of database version and database server
    // must be unique
    UniqueSetOfFieldDefs usfDbVersionAssignedToDbServer = new UniqueSetOfFieldDefs(VR_DBVERSION_ASSIGNED_TO_DBSERVER);
    usfDbVersionAssignedToDbServer.addFieldDef(getFieldDefByPnName(PN_DATABASESERVER_REF));
    usfDbVersionAssignedToDbServer.addFieldDef(getFieldDefByPnName(PN_DATABASEVERSION_REF));
    addUniqueFieldSet(usfDbVersionAssignedToDbServer);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMCompatibilityMatrix(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateRules()
  {
    VREqualTypeOfDbServerAndDBVersion vrEqualTypeOfDbServerAndDBVersion = new VREqualTypeOfDbServerAndDBVersion();

    addRuleValidateBeforeDBInsert(vrEqualTypeOfDbServerAndDBVersion);
    addRuleValidateBeforeDBUpdate(vrEqualTypeOfDbServerAndDBVersion);
  }
}
