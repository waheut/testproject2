// Planon Enterprise Edition Source file: AnalyzeProxyViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmanalyze.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * ProxyViewDefinition
 */
public class AnalyzeProxyViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AnalyzeProxyViewDefinition object.
   */
  public AnalyzeProxyViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_ANALYZE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOPDMAnalyzeDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOPDMAnalyzeDef.PN_PDMREQUEST_REF);
    addSearchFieldDef(IBOPDMAnalyzeDef.PN_FILE_TYPE_REF);
  }
}
