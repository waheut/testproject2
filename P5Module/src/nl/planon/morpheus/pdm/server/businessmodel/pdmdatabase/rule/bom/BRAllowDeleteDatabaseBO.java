// Planon Enterprise Edition Source file: BRAllowDeleteDatabaseBO.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * specifies if it is allowed to delete a pdmdatabase bo.
 */
public class BRAllowDeleteDatabaseBO extends BOMRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * The BOM 'Delete' is only available when the pdm database is in initial state. This means that:
   * <br>
   * - the database is not imported yet or<br>
   * - the database is deleted from the database server and totally cleaned-up
   *
   * @param  aBO
   * @param  aPnContext
   *
   * @return true is the BOM should be visible, false when the BOM should be invisible
   *
   * @throws PnErrorListException
   */
  @Override protected boolean execute(BOPDMDatabase aBO, PnContext aPnContext) throws PnErrorListException
  {
    return aBO.getSystemState().getPnName().equals(BOPDMDatabaseDef.STATE_INITIAL);
  }
}
