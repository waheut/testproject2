// Planon Enterprise Edition Source file: BOPDMDatabaseVersionOracle.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMDatabaseVersionOracle
 */
public class BOPDMDatabaseVersionOracle extends BOBasePDMDatabaseVersion
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionOracle object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMDatabaseVersionOracle(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseType getPDMDatabaseType()
  {
    return PDMDatabaseType.ORACLE;
  }
}
