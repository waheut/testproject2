// Planon Enterprise Edition Source file: BOPDMLocationDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmlocation;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOPDMLocationDef
 */
public class BOPDMLocationDef extends BODefinition implements IBOPDMLocationDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Database fields
  private static final String DB_CODE = DbFieldName.CODE;
  private static final String DB_NAME = DbFieldName.NAME;
  private static final String DB_LOCATION = "LOCATION";
  private static final String DB_WEBDAV_USERNAME = "WEBDAV_USERNAME";
  private static final String DB_WEBDAV_PASSWORD = "WEBDAV_PASSWORD";

  private static final String TBL_NAME = "PLN_PDMLOCATION";

  // Display format
  private static final String DISPLAY_FORMAT = getDisplayFormat(PN_CODE, PN_NAME);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMLocationDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMLocationDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMAddDef(this, PN_BOM_ADD));
    addBOMDef(new BOMCopyDef(this, PN_BOM_COPY));
    addBOMDef(new BOMDeleteDef(this, PN_BOM_DELETE));
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    FieldDefinition fieldDef;

    fieldDef = new SysChangeDateTimeFieldDefinition(this);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysInsertDateTimeFieldDefinition(this);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysChangeAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    fieldDef.setPlanonStandardCopy(false);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysInsertAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    fieldDef.setPlanonStandardCopy(false);
    addFieldDef(fieldDef);

    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_CODE, DB_CODE, 20);
    fieldDef.setAndFreezePlanonMandatory(true);
    fieldDef.setPlanonQuickSearch(true);
    setLookupField(fieldDef);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_NAME, DB_NAME, 255);
    stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
    stringFieldDef.setPlanonQuickSearch(true);
    addFieldDef(stringFieldDef);
    /* ---------------------------------------------------------------------- */
    FileLocationFieldDefinition fileLocationFieldDef = new FileLocationFieldDefinition(this, PN_LOCATION, DB_LOCATION);
    fileLocationFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
    fileLocationFieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fileLocationFieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_WEBDAV_USERNAME, DB_WEBDAV_USERNAME, 50);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new PasswordFieldDefinition(this, PN_WEBDAV_PASSWORD, DB_WEBDAV_PASSWORD, 50);
    addFieldDef(fieldDef);
  }
}
