// Planon Enterprise Edition Source file: PDMPrivilegeTypeLookupViewDefinition.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype;

import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMPrivilegeTypeLookupViewDefinition
 */
public class PDMPrivilegeTypeLookupViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMPrivilegeTypeLookupViewDefinition object.
   */
  public PDMPrivilegeTypeLookupViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_PRIVILEGE_TYPE);
  }
}
