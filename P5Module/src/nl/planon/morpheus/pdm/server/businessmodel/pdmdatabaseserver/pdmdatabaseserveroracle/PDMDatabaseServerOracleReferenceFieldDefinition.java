// Planon Enterprise Edition Source file: PDMDatabaseServerOracleReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseServerOracleReferenceFieldDefinition
 */
public class PDMDatabaseServerOracleReferenceFieldDefinition extends ReferenceIntegerFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseServerOracleReferenceFieldDefinition object.
   *
   * @param aFreeFieldConversionInfo
   */
  public PDMDatabaseServerOracleReferenceFieldDefinition(FreeFieldConversionInfo aFreeFieldConversionInfo)
  {
    this(aFreeFieldConversionInfo.getBODefinition(), aFreeFieldConversionInfo.getPnName());
    setDB(aFreeFieldConversionInfo.getDBName(), aFreeFieldConversionInfo.getDBType());
  }


  /**
   * Creates a new PDMDatabaseServerOracleReferenceFieldDefinition object.
   *
   * <p>Default constructor to retrieve an instance of the field definition. Use
   * FieldTypeManager.constructFieldDefinition to construct a field definition</p>
   *
   * @param aBODefinition BusinessObject definition
   * @param aBOType       BOType
   */
  public PDMDatabaseServerOracleReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMDatabaseServerOracleReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   */
  public PDMDatabaseServerOracleReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName)
  {
    super(aBODefinition, aPlanonName, BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE);
    setPlanonFieldType(FieldTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE);
  }


  /**
   * Creates a new PDMDatabaseServerOracleReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   * @param aDBName       the name of the database field.
   */
  public PDMDatabaseServerOracleReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName, String aDBName)
  {
    super(aBODefinition, aPlanonName, aDBName, BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public boolean isFreeFieldConversionAllowed()
  {
    return true;
  }


  /**
   * Create a PDMDatabaseServerOracleReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return SystemCodeReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMDatabaseServerOracleReferenceField(aBO, this);
  }
}
