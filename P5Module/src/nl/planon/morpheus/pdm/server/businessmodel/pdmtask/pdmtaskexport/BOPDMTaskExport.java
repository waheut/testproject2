// Planon Enterprise Edition Source file: BOPDMTaskExport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskexport;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * BOPDMTaskExport
 */
public class BOPDMTaskExport extends BOBasePDMTask
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskExport object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  protected BOPDMTaskExport(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
