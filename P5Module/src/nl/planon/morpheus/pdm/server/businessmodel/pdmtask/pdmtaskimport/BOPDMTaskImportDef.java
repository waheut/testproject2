// Planon Enterprise Edition Source file: BOPDMTaskImportDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskimport;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMTaskImportDef
 */
public class BOPDMTaskImportDef extends BOBasePDMTaskDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Sub type identifier
  private static final String SUB_TYPE_PDM_TASK_IMPORT = PDMTaskType.IMPORT.getCode();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImportDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMTaskImportDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_TASK);
    setSystemSubTypeFieldValue(new BaseStringValue(SUB_TYPE_PDM_TASK_IMPORT));
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_TASK_IMPORT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BOPDMTaskImport doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMTaskImport(this);
  }
}
