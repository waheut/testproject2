// Planon Enterprise Edition Source file: VRImportAndExportLocationFieldsMandatory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * Validation rule to check if field FileType = Ora_exp or Datapump and EXP. fields import_Location
 * and Export_location must be filling befor you can save.
 */
public class VRImportAndExportLocationFieldsMandatory extends ValidationRule<BOBasePDMDatabaseServer>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * when you create a Ora_exp or Datapump and Exp, you need to fill in Import location and Export
   * location. its needed for the oldstyle imp/exp.
   *
   * @param  aBO        BOPDMDatabaseServer
   * @param  aOldBO     BOPDMDatabaseServer
   * @param  aPnContext aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMDatabaseServer aBO, BOBasePDMDatabaseServer aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    if (!aBO.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF).isEmpty())
    {
      String fileTypeCode = aBO.getReferenceFieldByPnName(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF).getFieldValueOfReferencedBO(BOPDMFileTypeDef.PN_CODE).getAsString();
      String ora_export = PDMFileType.ORA_EXP.getCode();
      if (fileTypeCode.equals(PDMFileType.ORA_EXP.getCode()) || fileTypeCode.equals(PDMFileType.DATAPUMP_AND_EXP.getCode()))
      {
        if (aBO.getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_IMP_LOCATION).isEmpty())
        {
          PnMessage message = aPnContext.getPnErrorManager().createMessage(ErrorNumberHades.EC_FIELD_IS_MANDATORY);
          message.addLocalizebleTextArgument(aBO.getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_IMP_LOCATION).getLocalizableDisplayName());
          aPnContext.getPnErrorManager().add(message);
        }

        if (aBO.getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_EXP_LOCATION).isEmpty())
        {
          PnMessage message = aPnContext.getPnErrorManager().createMessage(ErrorNumberHades.EC_FIELD_IS_MANDATORY);
          message.addLocalizebleTextArgument(aBO.getFieldByPnName(BOPDMDatabaseServerOracleDef.PN_EXP_LOCATION).getLocalizableDisplayName());
          aPnContext.getPnErrorManager().add(message);
        }
      }
    }
  }
}
