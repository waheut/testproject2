// Planon Enterprise Edition Source file: BOBasePDMDatabaseServerDef.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver;

import java.util.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodel.businessobject.system.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.calculator.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.fieldchange.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.rule.action.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.rule.validation.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmlocation.*;

/**
 * BOPDMDatabaseServerDef
 */
public class BOBasePDMDatabaseServerDef extends BODefinition implements IBOBasePDMDatabaseServerDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Database fields
  private static final String DB_CODE = DbFieldName.CODE;
  private static final String DB_NAME = DbFieldName.NAME;
  protected static final String DB_DATABASEVERSION_REF = "FK_PDMDATABASEVERSION";
  private static final String DB_ADMIN_USER = "ADMIN_USER";
  private static final String DB_ADMIN_PASS = "ADMIN_PASS";
  private static final String DB_CODE_PAGE = "CODE_PAGE";
  private static final String DB_CONNECT_SERVER_NAME = "CONNECT_SERVER_NAME";
  private static final String DB_SORT_ORDER = "SORT_ORDER";
  private static final String DB_CONNECT_TNS_NAME = "CONNECT_TNS_NAME";
  private static final String DB_HOST_NAME = "HOST_NAME";
  private static final String DB_PORT_NUMBER = "PORT_NUMBER";
  private static final String DB_DATABASE_INSTANCENAME = "DATABASE_INSTANCENAME";
  private static final String DB_TEMPORARY_TABLESPACENAME = "TEMPORARY_TABLESPACENAME";

  private static final String DB_FILE_TYPE_REF = "FK_PLC_PDMFILETYPE";
  private static final String DB_IS_ACTIVE = "IS_ACTIVE";
  private static final String DB_PDMLOCATION_REF = "FK_PDMLOCATION";
  private static final String DB_FK_BODEFINITION = "FK_BODEFINITION";
  private static final String DB_EXP_LOCATION = "EXP_LOCATION";
  private static final String DB_IMP_LOCATION = "IMP_LOCATION";

  private static final String TBL_NAME = "PLN_PDMDATABASESERVER";

  public static final String DISPLAY_FORMAT = getDisplayFormat(PN_CODE, PN_NAME);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOBasePDMDatabaseServerDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
    setEventLogAware();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get name of fields that hold specific settings for the database server
   *
   * @return array of the names of the fields that hold specific settings for the database server
   */
  public Set<String> getDatabaseServerSettingFieldNames()
  {
    Set<String> result = new HashSet<String>();

    result.add(PN_ADMIN_USER);
    result.add(PN_ADMIN_PASS);
    result.add(PN_HOST_NAME);
    result.add(PN_DATABASE_INSTANCENAME);
    result.add(PN_PDMLOCATION_REF);

    return result;
  }


  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.BASE_PDM_DATABASE_SERVER;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMAddSubTypeDef(this, PN_BOM_ADD));
    addBOMDef(new BOMCopyDef(this, PN_BOM_COPY));
    addBOMDef(new BOMDeleteDef(this, PN_BOM_DELETE));
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
    addBOMDef(new BOMMtoNDef(this, PN_BOM_LINK_PDMDBVERSION, MtoNAssociationTypePDMP5Module.PN_PDMDBSERVER_MTON_PDMDBVERSION, getMtoNInContextPVType(), getMtoNPVType()));
    addBOMDef(new BOMSendMailDef(this, PN_BOM_SEND_MAIL));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    FieldDefinition fieldDef;

    addFieldDef(new SysChangeDateTimeFieldDefinition(this));
    addFieldDef(new SysInsertDateTimeFieldDefinition(this));
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysChangeAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysInsertAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    fieldDef.setPlanonStandardCopy(false);
    addFieldDef(fieldDef);
    {
      StringFieldDefinition fieldDefCode = new StringFieldDefinition(this, PN_CODE, DB_CODE, 20);
      fieldDefCode.setAndFreezePlanonMandatory(true);
      fieldDefCode.setPlanonQuickSearch(true);
      fieldDefCode.setAndFreezePlanonUnique(true);
      fieldDefCode.setUseExtensionForCopy(true);
      setLookupField(fieldDefCode);
      addFieldDef(fieldDefCode);
    }
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_NAME, DB_NAME, 255);
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      stringFieldDef.setAndFreezePlanonMandatory(true);
      stringFieldDef.setPlanonQuickSearch(true);
      addFieldDef(stringFieldDef);
    }
    /* ---------------------------------------------------------------------- */
    fieldDef = new BasePDMDatabaseVersionReferenceFieldDefinition(this, PN_DATABASEVERSION_REF, DB_DATABASEVERSION_REF);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_ADMIN_USER, DB_ADMIN_USER, 30);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new PasswordFieldDefinition(this, PN_ADMIN_PASS, DB_ADMIN_PASS, 30);
    fieldDef.setPlanonStandardCopy(true);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_CODE_PAGE__NOT_IN_ALL_SUBTYPES, DB_CODE_PAGE, 50);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_HOST_NAME, DB_HOST_NAME, 30);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new IntegerFieldDefinition(this, PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES, DB_PORT_NUMBER, 4);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_DATABASE_INSTANCENAME, DB_DATABASE_INSTANCENAME, 50);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new IntegerFieldDefinition(this, PN_SORT_ORDER__NOT_IN_ALL_SUBTYPES, DB_SORT_ORDER, 5);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_CONNECT_TNS_NAME__NOT_IN_ALL_SUBTYPES, DB_CONNECT_TNS_NAME, 30);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new StringFieldDefinition(this, PN_TEMPORARY_TABLESPACENAME__NOT_IN_ALL_SUBTYPES, DB_TEMPORARY_TABLESPACENAME, 30);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new PDMFileTypeReferenceFieldDefinition(this, PN_FILE_TYPE_REF, DB_FILE_TYPE_REF);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_EXPORT_LOCATION__NOT_IN_ALL_SUBTYPES, DB_EXP_LOCATION, 255);
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      stringFieldDef.setAndFreezePlanonMandatory(false);
      addFieldDef(stringFieldDef);
    }
    /* ---------------------------------------------------------------------- */ {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_IMPORT_LOCATION__NOT_IN_ALL_SUBTYPES, DB_IMP_LOCATION, 255);
      stringFieldDef.setStorageFeature(StorageFeature.INDEXABLE_AND_FULLY_SEARCHABLE); // be backwards compatible: do not break filters/indices of existing customers...
      stringFieldDef.setAndFreezePlanonMandatory(false);
      addFieldDef(stringFieldDef);
    }
    /* ---------------------------------------------------------------------- */
    fieldDef = new BooleanFieldDefinition(this, PN_IS_ACTIVE, DB_IS_ACTIVE);
    fieldDef.setAndFreezePlanonMandatory(true);
    fieldDef.setSystemDefaultValue(BaseBooleanValue.FALSE);
    fieldDef.setAndFreezePlanonStandardCopy(false);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new PDMLocationReferenceFieldDefinition(this, PN_PDMLOCATION_REF, DB_PDMLOCATION_REF);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */ {
      SystemBODefinitionReferenceFieldDefinition refFieldDef = new SystemBODefinitionReferenceFieldDefinition(this, PN_BUSINESSOBJECTDEFINITION_REF, DB_FK_BODEFINITION);
      refFieldDef.setAndFreezePlanonStandardCopy(true);
      refFieldDef.setAndFreezePlanonMandatory(true);
      refFieldDef.setAndFreezePlanonInSelection(true);
      refFieldDef.removeSearchOperatorType(SCOperatorType.NOT_NULL, SCOperatorType.NULL);
      addFieldDef(refFieldDef);
    }
    /* ---------------------------------------------------------------------- */
    fieldDef = new MtoNPVTypeFieldDefinition(this, PN_PDMDBVERSIONDETAIL, PVTypePDMP5Module.PDMDBVERSION_IN_CONTEXT_OF_PDMDBSERVER);
    addFieldDef(fieldDef);
    /*-----------------------------------------------------------------------*/
    fieldDef = new MtoNPVTypeFieldDefinition(this, PN_PDMDBFILELOCATIONDETAIL, PVTypePDMP5Module.PDMDB_FILE_LOCATION_IN_CONTEXT_OF_PDMDBSERVER);
    addFieldDef(fieldDef);

    /*-----------------------------------------------------------------------*/
    fieldDef = new FreeIntegerFieldDefinition(this, PN_FREEINT1, DbFieldName.FREE01_INTEGER);
    addFieldDef(fieldDef);

    fieldDef = new FreeIntegerFieldDefinition(this, PN_FREEINT2, DbFieldName.FREE02_INTEGER);
    addFieldDef(fieldDef);

    fieldDef = new FreeIntegerFieldDefinition(this, PN_FREEINT3, DbFieldName.FREE03_INTEGER);
    addFieldDef(fieldDef);

    fieldDef = new FreeIntegerFieldDefinition(this, PN_FREEINT4, DbFieldName.FREE04_INTEGER);
    addFieldDef(fieldDef);

    fieldDef = new FreeIntegerFieldDefinition(this, PN_FREEINT5, DbFieldName.FREE05_INTEGER);
    addFieldDef(fieldDef);

    fieldDef = new FreeStringFieldDefinition(this, PN_FREESTR1, DbFieldName.FREE01, 100);
    addFieldDef(fieldDef);

    fieldDef = new FreeStringFieldDefinition(this, PN_FREESTR2, DbFieldName.FREE02, 100);
    addFieldDef(fieldDef);

    fieldDef = new FreeStringFieldDefinition(this, PN_FREESTR3, DbFieldName.FREE03, 100);
    addFieldDef(fieldDef);

    fieldDef = new FreeStringFieldDefinition(this, PN_FREESTR4, DbFieldName.FREE04, 100);
    addFieldDef(fieldDef);

    fieldDef = new FreeStringFieldDefinition(this, PN_FREESTR5, DbFieldName.FREE05, 100);
    addFieldDef(fieldDef);

    {
      StringFieldDefinition stringFieldDef = new StringFieldDefinition(this, PN_CONNECTION_URL);
      stringFieldDef.setAndFreezePlanonReadOnly(true);
      stringFieldDef.setFieldValueCalculator(new CFRConnectionURL(stringFieldDef));
      addFieldDef(stringFieldDef);
    }

    doCreateFieldDefs(aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldSequences()
  {
    addFieldChangeOrder(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF, PN_IS_ACTIVE);
  }


  /**
   * Creates the action rules for this BO
   */
  protected final void doCreateActionRules()
  {
    addRuleAfterDBUpdate(new ARUpdatePDMLocationLinkedDatabases());
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateBaseType()
  {
    setSystemSubTypeFieldDef(PN_BUSINESSOBJECTDEFINITION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateFieldChangeActions()
  {
    addFieldChangeActionToField(PN_IS_ACTIVE, new FCSupportVersionsMustBeFilling());
    addFieldChangeActionToField(PN_IS_ACTIVE, new FCDataFileLocationMustBeFilling());
  }


  /**
   * to support subtype
   *
   * @param aPnContext
   */
  protected void doCreateFieldDefs(PnContext aPnContext)
  {
    // override this in subs;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateRules()
  {
    doCreateValidationRules();
    doCreateActionRules();
  }


  /**
   * Creates the validation rules for this BO
   */
  protected final void doCreateValidationRules()
  {
    VRImportAndExportLocationFieldsMandatory vrImportAndExportLocationFieldsMandatory = new VRImportAndExportLocationFieldsMandatory();
    // BEFORE INSERT
    addRuleValidateBeforeDBInsert(vrImportAndExportLocationFieldsMandatory);
    // BEFORE UPDATE
    addRuleValidateBeforeDBUpdate(new VRReadOnlyFieldsForActiveDbServer());
    addRuleBeforeDBUpdate(new VRCannotInactivateDbServerIfBusyDbPresent());
    addRuleValidateBeforeDBUpdate(vrImportAndExportLocationFieldsMandatory);
  }


  /**
   * get PVType to retrieve all items on the N side (PDM Database Versions) in context of the M side
   * (PDM Database Servers) of the association.
   *
   * @return PVType to retrieve all items on the N side in context of the M side of the association
   */
  protected IPVType getMtoNInContextPVType()
  {
    return null;
  }


  /**
   * get PVType to retrieve all items on the N side (PDM Database Versions) of the association.
   *
   * @return PVType to retrieve all items on the N side of the association
   */
  protected IPVType getMtoNPVType()
  {
    return null;
  }
}
