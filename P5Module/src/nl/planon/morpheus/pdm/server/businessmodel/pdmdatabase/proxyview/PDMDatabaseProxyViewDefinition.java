// Planon Enterprise Edition Source file: PDMDatabaseProxyViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseProxyViewDefinition
 */
public class PDMDatabaseProxyViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  PVJoinDef planCode;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseProxyViewDefinition object.
   */
  public PDMDatabaseProxyViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOPDMDatabaseDef.PN_DUMPFILE);
    addSelectFieldDef(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    addSelectFieldDef(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
    this.planCode.addSelectFieldDef(BOSystemCodeDef.PN_CODE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.planCode = addLeftJoinDef(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF, BOTypeHades.SYSTEMCODE, BOSystemCodeDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOPDMDatabaseDef.PN_PRIMARYKEY);
  }
}
