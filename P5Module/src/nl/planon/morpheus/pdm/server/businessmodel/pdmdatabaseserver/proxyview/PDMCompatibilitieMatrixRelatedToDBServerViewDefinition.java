// Planon Enterprise Edition Source file: PDMCompatibilitieMatrixRelatedToDBServerViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseServerLookupViewDefinition
 */
public class PDMCompatibilitieMatrixRelatedToDBServerViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseServerLookupViewDefinition object.
   */
  public PDMCompatibilitieMatrixRelatedToDBServerViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
  }
}
