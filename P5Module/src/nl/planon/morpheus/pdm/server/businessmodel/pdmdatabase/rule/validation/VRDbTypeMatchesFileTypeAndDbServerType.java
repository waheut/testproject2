// Planon Enterprise Edition Source file: VRDbTypeMatchesFileTypeAndDbServerType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * Validation rule to check if the filled-in database type, file type and database server all
 * correspond to the same database type
 */
public class VRDbTypeMatchesFileTypeAndDbServerType extends ValidationRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Validation rule to check if the filled-in database type, file type and database server all
   * correspond to the same database type (they should all correspond to Oracle or they should all
   * correspond to MSSQL)
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    // get PDMDatabaseType as filled-in in PDMDatabase
    PDMDatabaseType pdmDatabaseTypeOfDatabase = aBO.getPDMDatabaseType();

    // get PDMDatabaseType that corresponds to the filled-in PDMFileType
    PDMDatabaseType pdmDatabaseTypeOfFileType = null;
    PDMFileTypeReferenceField pdmFileTypeReferenceField = (PDMFileTypeReferenceField) aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
    if (pdmFileTypeReferenceField != null)
    {
      pdmDatabaseTypeOfFileType = pdmFileTypeReferenceField.getPDMDatabaseType();
    }

    // check if these PDMDatabaseTypes match
    boolean databaseTypesMatch = pdmDatabaseTypesMatch(pdmDatabaseTypeOfDatabase, pdmDatabaseTypeOfFileType);

    // if it still matches, also check the PDMDatabaseType of the filled-in PDM Database Server
    if (databaseTypesMatch)
    {
      // get PDMDatabaseType that corresponds to the filled-in PDM Database Server
      PDMDatabaseType pdmDatabaseTypeOfDatabaseServer = null;
      BOBasePDMDatabaseServer boPDMDatabaseServer = (BOBasePDMDatabaseServer) aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
      if (boPDMDatabaseServer != null)
      {
        pdmDatabaseTypeOfDatabaseServer = boPDMDatabaseServer.getPDMDatabaseType();
      }

      // check if the PDMDatabaseType matches with both the PDMDatabaseType of the PDM Database and the PDMDatabaseType of the PFMFileType
      databaseTypesMatch = (pdmDatabaseTypesMatch(pdmDatabaseTypeOfDatabaseServer, pdmDatabaseTypeOfDatabase) //
        && pdmDatabaseTypesMatch(pdmDatabaseTypeOfDatabaseServer, pdmDatabaseTypeOfFileType));
    }

    // generate an error message if there is a mismatch
    if (!databaseTypesMatch)
    {
      PnMessage message = aPnContext.getPnErrorManager().createMessage(ErrorNumberPDMP5Module.EC_DBTYPE_DOES_NOT_MATCH);
      message.addLocalizebleKeyArgument(aBO.getFieldByPnName(BOPDMDatabaseDef.PN_DATABASESERVER_REF).getLocalizableKey());
      message.addLocalizebleKeyArgument(aBO.getFieldByPnName(BOPDMDatabaseDef.PN_DATABASE_TYPE_REF).getLocalizableKey());
      message.addLocalizebleKeyArgument(aBO.getFieldByPnName(BOPDMDatabaseDef.PN_FILE_TYPE_REF).getLocalizableKey());
      aPnContext.getPnErrorManager().add(message);
    }
  }


  /**
   * Check if the two PDMDatabaseType that are passed as parameter are equal to each other
   *
   * @param  aPDMDbType1 first comparison value
   * @param  aPDMDbType2 second comparison value
   *
   * @return true if the PDMDatabaseTypes are equal or either one of them is empty, false otherwise
   */
  private boolean pdmDatabaseTypesMatch(PDMDatabaseType aPDMDbType1, PDMDatabaseType aPDMDbType2)
  {
    boolean result = true;
    if ((aPDMDbType1 != null) && (aPDMDbType2 != null))
    {
      return aPDMDbType1.equals(aPDMDbType2);
    }
    return result;
  }
}
