// Planon Enterprise Edition Source file: BOPDMTaskExportDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskexport;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMTaskExportDef
 */
public class BOPDMTaskExportDef extends BOBasePDMTaskDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Sub type identifier
  private static final String SUB_TYPE_PDM_TASK_EXPORT = PDMTaskType.EXPORT.getCode();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskExportDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMTaskExportDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_TASK);
    setSystemSubTypeFieldValue(new BaseStringValue(SUB_TYPE_PDM_TASK_EXPORT));
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_TASK_EXPORT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BOPDMTaskExport doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMTaskExport(this);
  }
}
