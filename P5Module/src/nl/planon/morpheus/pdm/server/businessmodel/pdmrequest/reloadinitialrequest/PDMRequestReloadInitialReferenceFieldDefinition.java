// Planon Enterprise Edition Source file: PDMRequestReloadInitialReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.reloadinitialrequest;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMRequestReloadInitialReferenceFieldDefinition
 */
public class PDMRequestReloadInitialReferenceFieldDefinition extends ReferenceIntegerFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestReloadInitialReferenceFieldDefinition object.
   *
   * @param aFreeFieldConversionInfo
   */
  public PDMRequestReloadInitialReferenceFieldDefinition(FreeFieldConversionInfo aFreeFieldConversionInfo)
  {
    this(aFreeFieldConversionInfo.getBODefinition(), aFreeFieldConversionInfo.getPnName());
    setDB(aFreeFieldConversionInfo.getDBName(), aFreeFieldConversionInfo.getDBType());
  }


  /**
   * Creates a new PDMRequestReloadInitialReferenceFieldDefinition object.
   *
   * <p>Default constructor to retrieve an instance of the field definition. Use
   * FieldTypeManager.constructFieldDefinition to construct a field definition</p>
   *
   * @param aBODefinition BusinessObject definition
   * @param aBOType       BOType
   */
  public PDMRequestReloadInitialReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMRequestReloadInitialReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   */
  public PDMRequestReloadInitialReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName)
  {
    super(aBODefinition, aPlanonName, BOTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL);
    setPlanonFieldType(FieldTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL);
  }


  /**
   * Creates a new PDMRequestReloadInitialReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   * @param aDBName       the name of the database field.
   */
  public PDMRequestReloadInitialReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName, String aDBName)
  {
    this(aBODefinition, aPlanonName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public boolean isFreeFieldConversionAllowed()
  {
    return true;
  }
}
