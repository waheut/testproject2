// Planon Enterprise Edition Source file: BOPDMRequestImportDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.importrequest;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.importrequest.rule.validation.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMRequestImportDef
 */
public class BOPDMRequestImportDef extends BOBasePDMRequestDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Type
  public static final PDMRequestType PDMREQUEST_TYPE = PDMRequestType.IMPORT;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestImport object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMRequestImportDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_REQUEST_IMPORT;
  }


  /**
   * {@inheritDoc}
   *
   * @throws PnErrorListException
   */
  @Override protected void doCreateSubType(PnContext aPnContext) throws PnErrorListException
  {
    setParentBOType(BOTypePDMP5Module.BASE_PDM_REQUEST);
    setSystemSubTypeFieldValue(getPDMRequestTypePrimaryKeyByCode(aPnContext, PDMREQUEST_TYPE, true));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateValidationRulesOfSub()
  {
    addRuleValidateBeforeDBInsert(new VRNoImportWithMultipleSchemaNames());
  }
}
