// Planon Enterprise Edition Source file: PDMDatabaseTypeReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseTypeReferenceFieldDefinition
 */
public class PDMDatabaseTypeReferenceFieldDefinition extends SystemCodeReferenceFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aBOType
   */
  public PDMDatabaseTypeReferenceFieldDefinition(final BODefinition aBODefinition, final IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMFileTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   */
  public PDMDatabaseTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName)
  {
    super(aBODefinition, aPnName, BOTypePDMP5Module.PDM_DATABASE_TYPE, FieldTypePDMP5Module.PDM_DATABASE_TYPE);
  }


  /**
   * Creates a new PDMDatabaseTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   * @param aDBName
   */
  public PDMDatabaseTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName, final String aDBName)
  {
    this(aBODefinition, aPnName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Create a PDMDatabaseTypeReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return SystemCodeReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMDatabaseTypeReferenceField(aBO, this);
  }
}
