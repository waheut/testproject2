// Planon Enterprise Edition Source file: BOPDMRequestExport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.exportrequest;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * BOPDMRequestExport
 */
public class BOPDMRequestExport extends BOBasePDMRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestExport object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMRequestExport(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
