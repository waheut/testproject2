// Planon Enterprise Edition Source file: BOMSendMailDef.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.bom;

import javax.mail.*;
import javax.mail.internet.*;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.businessobjectsmethod.bomfield.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.mail.*;
import nl.planon.hades.systemsetting.*;

import nl.planon.util.pnlogging.*;

/**
 * Sends an email based on the settings provided in SystemSettingEmail.
 */
public class BOMSendMailDef extends BOMDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(BOMSendMailDef.class, PnLogCategory.DEFAULT);

  public static final String ARG_EMAIL_CONTENT = "PN_EMAIL_CONTENT"; // The argument which can be used to get/set the content to be sent.
  public static final String ARG_EMAIL_SUBJECT = "PN_EMAIL_SUBJECT"; // The argument which can be used to get/set the subject.

  private static final String MAIL_SMTP_HOST = "mail.smtp.host";
  private static final String MAIL_SMTP_PORT = "mail.smtp.port";
  private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
  private static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
  private static final String MAIL_TRANSPORT_PROTOCOL_SMTP = "smtp";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  public BOMSendMailDef(BODefinition aBODef, String aBOMPnName)
  {
    super(aBODef, aBOMPnName, BOMTypeHades.GENERIC);
    setSystem(true);
    setVisible(false);
    setNeedsBOValue(false);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateBOMFieldDefs()
  {
    addBOMFieldDef(new BOMObjectFieldDef(this, ARG_EMAIL_CONTENT));
    addBOMFieldDef(new BOMObjectFieldDef(this, ARG_EMAIL_SUBJECT));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject execute(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    String emailTextContent = aBOM.getFieldByPnName(ARG_EMAIL_CONTENT).getAsString();
    String emailSubject = aBOM.getFieldByPnName(ARG_EMAIL_SUBJECT).getAsString();

    boolean success = sendEmail(emailTextContent, emailSubject, aPnContext);
    if (success)
    {
      if (LOG.isDebugEnabled())
      {
        LOG.debug("Mail sent successfully.");
      }
    }

    return null;
  }


  /**
   * Method to get mail Session.
   *
   * @param  aPnContext the application server context, never {@code null}.
   *
   * @return Session  the instance of javax.mail.Session
   *
   * @throws PnErrorListException
   */
  private Session getMailSession(PnContext aPnContext) throws PnErrorListException
  {
    SystemSettingMail systemSettingMail = aPnContext.getSystemSettingCache().getMail();
    // create properties
    Properties props = new Properties();
    props.put(MAIL_TRANSPORT_PROTOCOL, MAIL_TRANSPORT_PROTOCOL_SMTP);
    props.put(MAIL_SMTP_HOST, systemSettingMail.getSMTPHost());
    props.put(MAIL_SMTP_PORT, Integer.toString(systemSettingMail.getSMTPPort()));
    // create session
    javax.mail.Session mailSession;
    if (systemSettingMail.getSMTPAuthentication())
    {
      props.put(MAIL_SMTP_AUTH, "true");
      MailAuthenticator mailAuthenticator = new MailAuthenticator();
      mailAuthenticator.setUserName(systemSettingMail.getSMTPUserName());
      mailAuthenticator.setPassWord(systemSettingMail.getSMTPUserPassword());
      mailSession = javax.mail.Session.getInstance(props, mailAuthenticator);
    }
    else
    {
      mailSession = javax.mail.Session.getInstance(props);
    }
    return mailSession;
  }


  /**
   * Sends an email to team.technology@gmail.com with the information of died agents.
   *
   * @param  aEmailTextContent Email text
   * @param  aEmailSubject     Email subject
   * @param  aPnContext        PnContext
   *
   * @return boolean Returns the status whether success or not.
   *
   * @throws PnErrorListException
   */
  private boolean sendEmail(String aEmailTextContent, String aEmailSubject, PnContext aPnContext) throws PnErrorListException
  {
    Session session = getMailSession(aPnContext);

    try
    {
      MimeMessage message = new MimeMessage(session);
      //FROM
      message.setFrom(new InternetAddress(aPnContext.getSystemSettingCache().getMail().getMailSenderEmail()));
      //TO address is same as the Reply-To in Email/SMTP settings
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(aPnContext.getSystemSettingCache().getMail().getMailReplyEmail()));
      //Subject
      message.setSubject(aEmailSubject);
      //Content
      message.setContent(aEmailTextContent, "text/html; charset=utf-8");
      //Send
      Transport.send(message);
    }
    catch (MessagingException mex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("There seems to be a problem in sending email to Team.Technology@planonsoftware.com. Reason", mex);
      }
      return false;
    }
    return true;
  }
}
