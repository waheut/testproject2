// Planon Enterprise Edition Source file: PDMTaskViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * PDMTaskViewDefinition
 */
public class PDMTaskViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTaskViewDefinition object.
   */
  public PDMTaskViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_TASK);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createFieldDefs(aPnContext);
    addSelectFieldDef(IBOBasePDMTaskDef.PN_PRIMARYKEY);
  }


  /**
   * @see PVDefinition#createSearchDefs()
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(BOBasePDMTaskDef.PN_PDMREQUEST_REF);
  }
}
