// Planon Enterprise Edition Source file: PDMRequestExecuteStateReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequestexecutestate;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.state.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMRequestExecuteStateReferenceFieldDefinition
 */
public class PDMRequestExecuteStateReferenceFieldDefinition extends BaseBOStateReferenceFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestExecuteStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aBOType
   */
  public PDMRequestExecuteStateReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMRequestExecuteStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   */
  public PDMRequestExecuteStateReferenceFieldDefinition(BODefinition aBODefinition, String aPnName)
  {
    super(aBODefinition, aPnName, BOTypePDMP5Module.PDM_REQUEST_EXECUTE_STATE, FieldTypePDMP5Module.PDM_REQUEST_EXECUTE_STATE);
  }


  /**
   * Creates a new PDMRequestExecuteStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   * @param aDBName
   */
  public PDMRequestExecuteStateReferenceFieldDefinition(BODefinition aBODefinition, String aPnName, String aDBName)
  {
    this(aBODefinition, aPnName);
    setDB(aDBName);
  }
}
