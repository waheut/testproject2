// Planon Enterprise Edition Source file: BRDatabaseImported.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * BRDatabaseImported
 */
public class BRDatabaseImported extends BOMRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * The BOMs 'Reload database', 'Delete database', etc. are only available when:<br>
   * - The database is already imported<br>
   *
   * @param  aBO
   * @param  aPnContext
   *
   * @return true if the BOM should be visible, false when the BOM should be invisible
   *
   * @throws PnErrorListException
   */
  @Override protected boolean execute(BOPDMDatabase aBO, PnContext aPnContext) throws PnErrorListException
  {
    return aBO.isImported();
  }
}
