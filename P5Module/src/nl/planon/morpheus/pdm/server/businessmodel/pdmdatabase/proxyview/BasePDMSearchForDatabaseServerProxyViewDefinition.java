// Planon Enterprise Edition Source file: BasePDMSearchForDatabaseServerProxyViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * BasePDMSearchForDatabaseServerProxyViewDefinition
 */
public class BasePDMSearchForDatabaseServerProxyViewDefinition extends PVDefinition implements IPDMSearchDatabaseServer
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BasePDMSearchForDatabaseServerProxyViewDefinition object.
   */
  public BasePDMSearchForDatabaseServerProxyViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
    setDisplayValueFormatString(null);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * add connection url, only show highest version number (this can also be done in sql, but that
   * results in hardcoded difficult to maintain code) {@inheritDoc}
   *
   * @param  aProxyListValue
   * @param  aProxyListContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doAfterCreateProxyListValue(ProxyListValue aProxyListValue, ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doAfterCreateProxyListValue(aProxyListValue, aProxyListContext, aPnContext);

    // fill PN_CONNECTION_URL
    int ixURL = aProxyListValue.getColumnNumberByAliasName(ALIAS_CONNECTION_URL);
    int ixPk = aProxyListValue.getColumnNumberByAliasName(ALIAS_PRIMARYKEY);

    for (IProxyValue proxyValue : aProxyListValue.getProxyValues()) // limited set
    {
      IBaseValue pk = proxyValue.getProxyItem(ixPk).getBaseValue(); // cannot use getPrimarykeyValue because of aggregate
      if (!pk.isEmpty())
      {
        BOBasePDMDatabaseServer server = (BOBasePDMDatabaseServer) aPnContext.getBOByPrimaryKey(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, pk);
        String connectionURL = server.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_CONNECTION_URL).getAsString();
        ((BaseValue) proxyValue.getProxyItem(ixURL)).setAsObject(connectionURL);
      }
    }
  }
}
