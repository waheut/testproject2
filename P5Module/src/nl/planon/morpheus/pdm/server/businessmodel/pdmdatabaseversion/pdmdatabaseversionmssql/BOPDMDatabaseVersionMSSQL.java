// Planon Enterprise Edition Source file: BOPDMDatabaseVersionMSSQL.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMDatabaseVersionMSSQL
 */
public class BOPDMDatabaseVersionMSSQL extends BOBasePDMDatabaseVersion
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionMSSQL object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMDatabaseVersionMSSQL(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseType getPDMDatabaseType()
  {
    return PDMDatabaseType.MSSQL;
  }
}
