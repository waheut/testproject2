// Planon Enterprise Edition Source file: BOPDMPrivilegeTypeDef.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMPrivilegeTypeDef
 */
public class BOPDMPrivilegeTypeDef extends BOSystemCodeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String SUBTYPE = "PDMPRIVILEGETYPE";

  public static final String PRIVILEGE_TYPE_ADMIN = PDMPrivilegeType.ADMIN.getCode();
  public static final String PRIVILEGE_TYPE_UPGRADE = PDMPrivilegeType.UPGRADE.getCode();
  public static final String PRIVILEGE_TYPE_STANDARD = PDMPrivilegeType.STANDARD.getCode();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMPrivilegeTypeDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMPrivilegeTypeDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the primary key of the given FileType code.
   *
   * @param  aCode
   * @param  aPnContext
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPKValue(Object aCode, PnContext aPnContext) throws PnErrorListException
  {
    return BOSystemCodeDef.getPKValue(aCode, new BaseStringValue(SUBTYPE), aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMPrivilegeType(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateSubType(PnContext aPnContext)
  {
    setParentBOType(BOTypeHades.SYSTEMCODE);
    setSystemSubTypeFieldValue(new BaseStringValue(SUBTYPE));
  }
}
