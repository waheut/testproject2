// Planon Enterprise Edition Source file: PDMDBServerSupportedVersionsViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;

/**
 * PDMDBServerSupportedVersionsViewDefinition
 */
public class PDMDBServerSupportedVersionsViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDBVersionJoin;
  private PVJoinDef pdmMatrixJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDBVersionInContextOfPDMDBServer object.
   */
  public PDMDBServerSupportedVersionsViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDBVersionJoin.addSelectFieldDef(IBOBasePDMDatabaseVersionDef.PN_CODE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmMatrixJoin = addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY, BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, BOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
    this.pdmDBVersionJoin = this.pdmMatrixJoin.addInnerJoinDef(BOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF, BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION, IBOBasePDMDatabaseVersionDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(final PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
  }
}
