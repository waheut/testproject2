// Planon Enterprise Edition Source file: IPDMTasksInInitialStateViewDefinition.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview;

import nl.planon.pdm.core.datamessage.*;

/**
 * IPDMTasksInInitialStateViewDefinition
 *
 * @version $Revision$
 */
public interface IPDMTasksInInitialStateViewDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String ALIAS_DATABASE_OWNER_NAME = IRequestInformationFields.FIELD_DATABASE_OWNER_NAME;
  public static final String ALIAS_EXPORTFILETYPE_CODE = IRequestInformationFields.FIELD_EXPORTFILETYPE_CODE;
  public static final String ALIAS_LOCATION = IRequestInformationFields.FIELD_LOCATION;
  public static final String ALIAS_PDMDATABASE_DUMP_SIZE = IRequestInformationFields.FIELD_PDMDATABASE_DUMP_SIZE;
  public static final String ALIAS_PDMDATABASE_OBJECT_OWNER_IN_DUMP = IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_DUMP;
  public static final String ALIAS_PDMDATABASE_OBJECT_OWNER_IN_IMPORT = IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT;
  public static final String ALIAS_PDMDATABASE_OPTION_NO_IMPORT = IRequestInformationFields.FIELD_PDMDATABASE_OPTION_NO_IMPORT;
  public static final String ALIAS_PDMDATABASE_PASSWORD = IRequestInformationFields.FIELD_PDMDATABASE_PASSWORD;
  public static final String ALIAS_PDMDATABASE_PDMDUMPFILE = IRequestInformationFields.FIELD_PDMDATABASE_PDMDUMPFILE;
  public static final String ALIAS_PDMDATABASE_PDMEXPORTFILE = IRequestInformationFields.FIELD_PDMDATABASE_PDMEXPORTFILE;
  public static final String ALIAS_PDMDATABASE_SIZE = IRequestInformationFields.FIELD_PDMDATABASE_SIZE;
  public static final String ALIAS_PDMDATABASE_STOP_ON_FAILURE = IRequestInformationFields.FIELD_PDMDATABASE_STOP_ON_FAILURE;
  public static final String ALIAS_PDMDATABASE_TABLESPACENAME_IN_DUMP = IRequestInformationFields.FIELD_PDMDATABASE_TABLESPACENAME_IN_DUMP;
  public static final String ALIAS_PDMDATABASE_TASK_TIMEOUT = IRequestInformationFields.FIELD_PDMDATABASE_TASK_TIMEOUT;
  public static final String ALIAS_PDMDATABASE_PRIVILEGE_TYPE = IRequestInformationFields.FIELD_PDMDATABASE_PRIVILEGE_TYPE;
  public static final String ALIAS_PDMDBSERVER_CODE = IRequestInformationFields.FIELD_PDMDBSERVER_CODE;
  public static final String ALIAS_PDMDBTYPE_CODE = IRequestInformationFields.FIELD_PDMDBTYPE_CODE;
  public static final String ALIAS_PDMFILETYPE_CODE = IRequestInformationFields.FIELD_PDMFILETYPE_CODE;
  public static final String ALIAS_PDMTASK_SUBTYPE = IRequestInformationFields.FIELD_PDMTASK_SUBTYPE;
  public static final String ALIAS_PK_PDMDATABASE = IRequestInformationFields.FIELD_PK_PDMDATABASE;
  public static final String ALIAS_PK_PDMDATABASESERVER = IRequestInformationFields.FIELD_PK_PDMDATABASESERVER;
  public static final String ALIAS_PK_PDMDATABASE_LOCATION = IRequestInformationFields.FIELD_PK_PDMDATABASE_LOCATION;
  public static final String ALIAS_PK_PDMREQUEST = IRequestInformationFields.FIELD_PK_PDMREQUEST;
  public static final String ALIAS_PK_PDMTASK = IRequestInformationFields.FIELD_PK_PDMTASK;
  public static final String ALIAS_REQUESTTYPE_CODE = IRequestInformationFields.FIELD_REQUESTTYPE_CODE;
}
