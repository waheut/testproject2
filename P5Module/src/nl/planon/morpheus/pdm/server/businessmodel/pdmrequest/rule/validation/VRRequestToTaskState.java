// Planon Enterprise Edition Source file: VRRequestToTaskState.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.statemachine.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequestexecutestate.*;

/**
 * validation rule on state change Request, which is dependent on state last added Task.
 */
public class VRRequestToTaskState extends ValidationRule<BOBasePDMRequest>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <br>
   * Validation rule on state change PDMRequest. Rules:
   *
   * <ul>
   *   <li>Request cannot be set to Initial, if last added task is not in state Initial</li>
   *   <li>Request cannot be set to InProgress, if last added task is not in state InProgress</li>
   *   <li>Request cannot be set to Ok, if last added task is not in state Ok</li>
   *   <li>Request cannot be set to NotOk, if last added task is not in state NotOk</li>
   * </ul>
   *
   * @param  aBO        bo pdmrequest after change
   * @param  aOldBO     bo pdmrequest before change
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMRequest aBO, BOBasePDMRequest aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    State state = aBO.getSystemState();
    State oldState = aOldBO.getSystemState();

    if (!state.equals(oldState))
    {
      PVRuntimeDefinition pvDef = new PVRuntimeDefinition(BOTypePDMP5Module.BASE_PDM_TASK);
      PVJoinDef joinTaskSystemState = pvDef.addInnerJoinDef(IBOBasePDMTaskDef.PN_REF_BOSTATE);
      pvDef.addEqualWhereDef(IBOBasePDMTaskDef.PN_PDMREQUEST_REF, aBO.getPrimaryKeyValue());
      pvDef.addSortDef(IBOBasePDMRequestDef.PN_SYSINSERTDATETIME, false); // newest first
      joinTaskSystemState.addSelectFieldDef(BOPDMRequestExecuteStateDef.PN_PNNAME);

      pvDef.setResultLimit(1);

      ProxyListValue plvPDMRequest = aPnContext.createByPVDefinition(pvDef);

      if (!plvPDMRequest.isEmpty())
      {
        int pkPDMRequest = plvPDMRequest.getColumnNumberByAliasName(BOPDMRequestExecuteStateDef.PN_PNNAME);
        String taskStateName = plvPDMRequest.getFirstProxy().getProxyItem(pkPDMRequest).getAsString();

        String requestStateName = state.getPnName();
        String displayValueRequestState = state.getLocalizableKey();
        String displayValueTaskState = aPnContext.getBODefinition(BOTypePDMP5Module.BASE_PDM_REQUEST).getStateControllerDef().getSystemStateByPnName(requestStateName).getLocalizableKey();

        // it is not allowed to set request state to Initial, if the state of the last added task is InProgress, Notok or Ok.
        if (BOBasePDMRequestDef.STATE_INITIAL.equals(requestStateName) //
          && (IBOBasePDMTaskDef.STATE_IN_PROGRESS.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_NOK.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_OK.equals(taskStateName)))
        {
          errorCreate(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED, displayValueRequestState, displayValueTaskState, aPnContext);
        }
        // it is not allowed to set request state to InProgress, if the state of the last added task is Initial, Notok or Ok.
        if (BOBasePDMRequestDef.STATE_IN_PROGRESS.equals(requestStateName) //
          && (IBOBasePDMTaskDef.STATE_INITIAL.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_NOK.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_OK.equals(taskStateName)))
        {
          errorCreate(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED, displayValueRequestState, displayValueTaskState, aPnContext);
        }
        // it is not allowed to set request state to Ok, if the state of the last added task is Initial, InProgress, or Notok.
        if (BOBasePDMRequestDef.STATE_OK.equals(requestStateName) //
          && (IBOBasePDMTaskDef.STATE_INITIAL.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_IN_PROGRESS.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_NOK.equals(taskStateName)))
        {
          errorCreate(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED, displayValueRequestState, displayValueTaskState, aPnContext);
        }
        // it is not allowed to set request state to Notok, if the state of the last added task is Initial, InProgress, or Ok.
        if (BOBasePDMRequestDef.STATE_NOK.equals(requestStateName) //
          && (IBOBasePDMTaskDef.STATE_INITIAL.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_IN_PROGRESS.equals(taskStateName) //
            || IBOBasePDMTaskDef.STATE_OK.equals(taskStateName)))
        {
          errorCreate(ErrorNumberPDMP5Module.EC_REQUEST_STATE_IS_NOT_ALLOWED, displayValueRequestState, displayValueTaskState, aPnContext);
        }
      }
    }
  }


  /**
   * throw an error
   *
   * @param  aMessageType  error message
   * @param  aRequestState display value of request state
   * @param  aTaskState    display value of task state
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  private void errorCreate(MessageType aMessageType, String aRequestState, String aTaskState, PnContext aPnContext) throws PnErrorListException
  {
    PnMessage msg = aPnContext.getPnErrorManager().createMessage(aMessageType);
    msg.addLocalizebleKeyArgument(aRequestState);
    msg.addLocalizebleKeyArgument(aTaskState);
    aPnContext.getPnErrorManager().addFatal(msg);
  }
}
