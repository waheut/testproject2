// Planon Enterprise Edition Source file: PDMPendingTaskViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * view to retrieve all pending tasks
 */
public class PDMPendingTaskViewDefinition extends PVDefinition implements IPDMPendingTaskViewDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinPDMDatabase;
  private PVJoinDef joinPDMRequest;
  private PVJoinDef joinPDMRequestType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMPendingTaskViewDefinition object.
   */
  public PDMPendingTaskViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_TASK);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMTaskDef.PN_PRIMARYKEY, ALIAS_PK_PDMTASK);
    addSelectFieldDef(BOBasePDMTaskDef.PN_SYSSYSTEMTYPE, ALIAS_PDMTASK_SUBTYPE);
    this.joinPDMRequestType.addSelectFieldDef(BOPDMRequestTypeDef.PN_CODE, ALIAS_REQUESTTYPE_CODE);
    this.joinPDMRequest.addSelectFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY, ALIAS_PK_PDMREQUEST);
    this.joinPDMDatabase.addSelectFieldDef(IBOPDMDatabaseDef.PN_PRIMARYKEY, ALIAS_PK_PDMDATABASE);
    this.joinPDMDatabase.addSelectFieldDef(BOPDMDatabaseDef.PN_DATABASESERVER_REF, ALIAS_PK_PDMDATABASESERVER);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinPDMRequest = addInnerJoinDef(BOBasePDMTaskDef.PN_PDMREQUEST_REF);
    this.joinPDMDatabase = this.joinPDMRequest.addInnerJoinDef(BOBasePDMRequestDef.PN_DATABASE_REF);
    this.joinPDMRequestType = this.joinPDMRequest.addInnerJoinDef(BOBasePDMRequestDef.PN_REQUESTTYPE_REF);
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aPnContext DOCUMENT ME!
   *
   * @throws PnErrorListException DOCUMENT ME!
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createSearchDefs(aPnContext);
    addSearchFieldDef(IBOBasePDMTaskDef.PN_PRIMARYKEY, ALIAS_PK_PDMTASK);
    this.joinPDMRequest.addSearchFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY, ALIAS_PK_PDMREQUEST);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    // get states of all sub system bo definitions.. like import, ...
    PVExpressionNodeDef nodeDef = new PVExpressionNodeDef(this.getMasterJoinDef(), IBOBasePDMTaskDef.PN_REF_BOSTATE, SCOperatorType.IN);
    BODefinition boDefinition = aPnContext.getBODefinition(BOTypePDMP5Module.BASE_PDM_TASK);
    for (BODefinition subBBODefinition : boDefinition.getSubBOList())
    {
      nodeDef.addBaseValue(getPkState(subBBODefinition, BOBasePDMTaskDef.STATE_IN_PROGRESS, aPnContext));
    }

    addWhereDef(nodeDef);
  }


  /**
   * gets primarykey of state <code>aStateName</code> of BO <code>aBODefinition</code>.
   *
   * @param  aBODefinition BODefinition of state
   * @param  aStateName    name of state
   * @param  aPnContext
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  private IBaseValue getPkState(BODefinition aBODefinition, String aStateName, PnContext aPnContext) throws PnErrorListException
  {
    IBaseValue pkState = aBODefinition.getStateControllerDef().getSystemStateByPnName(BOBasePDMTaskDef.STATE_IN_PROGRESS).getPrimaryKeyInDB(aPnContext);
    return pkState;
  }
}
