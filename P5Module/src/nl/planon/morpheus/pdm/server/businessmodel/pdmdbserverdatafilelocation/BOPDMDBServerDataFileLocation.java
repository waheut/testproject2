// Planon Enterprise Edition Source file: BOPDMDBServerDataFileLocation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMDBServerDataFileLocation
 */
public class BOPDMDBServerDataFileLocation extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDBServerDataFileLocation object.
   *
   * @param  aBODefinition DOCUMENT ME!
   *
   * @throws PnErrorListException DOCUMENT ME!
   */
  protected BOPDMDBServerDataFileLocation(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
