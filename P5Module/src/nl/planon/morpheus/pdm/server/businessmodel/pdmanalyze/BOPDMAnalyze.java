// Planon Enterprise Edition Source file: BOPDMAnalyze.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmanalyze;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMAnalyze
 */
public class BOPDMAnalyze extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMAnalyze object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMAnalyze(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
