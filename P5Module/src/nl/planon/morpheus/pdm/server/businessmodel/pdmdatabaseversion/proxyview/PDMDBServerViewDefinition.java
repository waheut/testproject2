// Planon Enterprise Edition Source file: PDMDBServerViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDBServerViewDefinition
 */
public class PDMDBServerViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDBServerViewDefinition object.
   */
  public PDMDBServerViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_NAME);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOBasePDMDatabaseServerDef.PN_CODE);
    addSearchFieldDef(IBOBasePDMDatabaseServerDef.PN_NAME);
  }
}
