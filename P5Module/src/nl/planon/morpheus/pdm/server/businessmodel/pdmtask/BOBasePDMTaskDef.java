// Planon Enterprise Edition Source file: BOBasePDMTaskDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;
import nl.planon.hades.statemachine.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.rule.action.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.rule.validation.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtaskexecutestate.*;

/**
 * BOPDMTaskDef
 */
public class BOBasePDMTaskDef extends BODefinition implements IBOBasePDMTaskDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // System state id's
  private static final String ID_INITIAL = "I";
  private static final String ID_INPROGRESS = "IP";
  private static final String ID_OK = "OK";
  private static final String ID_NOT_OK = "NOK";

  // Database fields
  private static final String DB_SUBTYPE = DbFieldName.SUBTYPE;
  private static final String DB_REQUEST_REF = "FK_PDMREQUEST";
  private static final String DB_FK_BOSTATE = DbFieldName.FK_BOSTATE;

  private static final String TBL_NAME = "PLN_PDMTASK";

  public static final String DISPLAY_FORMAT = getDisplayFormat(PN_SYSSYSTEMTYPE);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOBasePDMTaskDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));
    addBOMDef(new BOMAddDef(this, PN_BOM_ADD)).setSystem(true);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    addFieldDef(new SysChangeDateTimeFieldDefinition(this));
    addFieldDef(new SysInsertDateTimeFieldDefinition(this));

    {
      FieldDefinition fieldDef = new SysChangeAccountReferenceFieldDefinition(this);
      fieldDef.setAndFreezePlanonReadOnly(true);
      fieldDef.setPlanonStandardCopy(false);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new SysInsertAccountReferenceFieldDefinition(this);
      fieldDef.setAndFreezePlanonReadOnly(true);
      fieldDef.setPlanonStandardCopy(false);
      addFieldDef(fieldDef);
    }
    {
      FieldDefinition fieldDef = new StringFieldDefinition(this, PN_SYSSYSTEMTYPE, DB_SUBTYPE, 30);
      fieldDef.setAndFreezePlanonSystemField(true);
      fieldDef.setAndFreezePlanonMandatory(true);
      fieldDef.setAndFreezePlanonReadOnly(true);
      fieldDef.setPlanonQueryWithUpperCase(false);
      addFieldDef(fieldDef);
    }
    {
      ReferenceFieldDefinition refFieldDef = new BasePDMRequestReferenceFieldDefinition(this, PN_PDMREQUEST_REF, DB_REQUEST_REF);
      refFieldDef.setAssociationActionType(AssociationActionType.DELETE);
      refFieldDef.setAndFreezePlanonSystemField(true);
      refFieldDef.setAndFreezePlanonMandatory(true);
      addFieldDef(refFieldDef);
    }
    {
      FieldDefinition fieldDef = new PDMTaskExecuteStateReferenceFieldDefinition(this, PN_REF_BOSTATE, DB_FK_BOSTATE);
      fieldDef.setAndFreezePlanonMandatory(true);
      fieldDef.setAndFreezePlanonReadOnly(true);
      addFieldDef(fieldDef);
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateBaseType()
  {
    setSystemSubTypeFieldDef(PN_SYSSYSTEMTYPE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateRules()
  {
    doCreateStateChangeRules();
    doCreateValidationRules();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateSystemStates()
  {
    StateControllerDef stateControllerDef = new StateControllerDef(this);
    this.setStateControllerDef(stateControllerDef);
    stateControllerDef.setSystemStateFieldDef(getFieldDefByPnName(IBOBasePDMTaskDef.PN_REF_BOSTATE));

    // States definition
    final SystemState initialState = stateControllerDef.addSystemState(ID_INITIAL, STATE_INITIAL);
    final SystemState inProgressState = stateControllerDef.addSystemState(ID_INPROGRESS, STATE_IN_PROGRESS);
    final SystemState okState = stateControllerDef.addSystemState(ID_OK, STATE_OK);
    final SystemState notOkState = stateControllerDef.addSystemState(ID_NOT_OK, STATE_NOK);

    // state changes
    SystemStateChange stateChange;
    stateChange = stateControllerDef.addSystemStateChange(initialState, inProgressState);
    stateChange = stateControllerDef.addSystemStateChange(inProgressState, okState);
    stateChange = stateControllerDef.addSystemStateChange(inProgressState, notOkState);

    // Set (default) initial system state
    stateControllerDef.addInitialSystemState(initialState);
    stateControllerDef.setDefaultInitialSystemState(initialState);
  }


  /**
   * validations rules for Sub BOTypes
   */
  protected void doCreateValidationRulesOfSub()
  {
    // override this in subs
  }


  /**
   * specify which state change rules do apply
   */
  private void doCreateStateChangeRules()
  {
    getStateControllerDef().addRuleForStateChangeWithToStateAfterDBUpdate(new ARCreateNextPDMTask(), STATE_OK);
  }


  /**
   * create validation rules that apply to pdm tasks
   */
  private void doCreateValidationRules()
  {
    VRTaskToRequestState vrTaskToRequestState = new VRTaskToRequestState();
    addRuleValidateBeforeDBUpdate(vrTaskToRequestState);

    doCreateValidationRulesOfSub();
  }
}
