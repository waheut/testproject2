// Planon Enterprise Edition Source file: IPDMPendingTaskViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview;

import nl.planon.pdm.core.datamessage.*;

/**
 * IPDMPendingTaskViewDefinition
 *
 * @version $Revision$
 */
public interface IPDMPendingTaskViewDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String ALIAS_PK_PDMTASK = IRequestInformationFields.FIELD_PK_PDMTASK;
  public static final String ALIAS_PK_PDMREQUEST = IRequestInformationFields.FIELD_PK_PDMREQUEST;
  public static final String ALIAS_PK_PDMDATABASE = IRequestInformationFields.FIELD_PK_PDMDATABASE;
  public static final String ALIAS_PDMTASK_SUBTYPE = IRequestInformationFields.FIELD_PDMTASK_SUBTYPE;
  public static final String ALIAS_REQUESTTYPE_CODE = IRequestInformationFields.FIELD_REQUESTTYPE_CODE;
  public static final String ALIAS_PK_PDMDATABASESERVER = IRequestInformationFields.FIELD_PK_PDMDATABASESERVER;
}
