// Planon Enterprise Edition Source file: BOPDMRequestReloadExport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.reloadexportrequest;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * BOPDMRequestReloadExport
 */
public class BOPDMRequestReloadExport extends BOBasePDMRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestReloadExport object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMRequestReloadExport(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
