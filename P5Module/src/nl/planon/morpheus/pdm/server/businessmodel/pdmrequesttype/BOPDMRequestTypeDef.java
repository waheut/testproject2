// Planon Enterprise Edition Source file: BOPDMRequestTypeDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMRequestTypeDef
 */
public class BOPDMRequestTypeDef extends BOSystemCodeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String SUBTYPE = "PDMREQUESTTYPE";

  private static final String DISPLAY_FORMAT = getDisplayFormat(PN_PNNAME_TRANSKEY);

  /*
   * use ENUM PDMRequestType for possible values!
   */
  public static final PDMRequestType REQUESTTYPE_IMPORT = PDMRequestType.IMPORT;
  public static final PDMRequestType REQUESTTYPE_EXPORT = PDMRequestType.EXPORT;
  public static final PDMRequestType REQUESTTYPE_RELOAD_EXPORT = PDMRequestType.RELOAD_EXPORT;
  public static final PDMRequestType REQUESTTYPE_RELOAD_INITIAL = PDMRequestType.RELOAD_INITIAL;
  public static final PDMRequestType REQUESTTYPE_DELETE = PDMRequestType.DELETE;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestTypeDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMRequestTypeDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the primarykey for the given aCode.
   *
   * @param  aPnContext PnContext
   * @param  aCode      PDM Requesttype code.
   *
   * @return primarykey
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPDMRequestTypePrimaryKeyByCode(PnContext aPnContext, String aCode) throws PnErrorListException
  {
    IBaseValue result = null;

    SearchCriteria sc = aPnContext.createPVSearchCriteria(PVTypePDMP5Module.PDMREQUEST_TYPE_CODE);
    ISCOperator operatorCode = sc.getOperator(BOPDMRequestTypeDef.PN_CODE, SCOperatorType.EQUAL);
    operatorCode.addValue(aCode);

    IProxyValue pv = aPnContext.createBySearchCriteria(sc).getFirstProxy();
    if (pv != null)
    {
      result = pv.getPrimaryKeyValue();
    }

    return result;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateSubType(PnContext aPnContext)
  {
    setParentBOType(BOTypeHades.SYSTEMCODE);
    setSystemSubTypeFieldValue(new BaseStringValue(SUBTYPE));
  }
}
