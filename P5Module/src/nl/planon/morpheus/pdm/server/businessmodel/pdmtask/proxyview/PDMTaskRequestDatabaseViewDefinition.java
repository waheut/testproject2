// Planon Enterprise Edition Source file: PDMTaskRequestDatabaseViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * PDMTaskRequestDatabaseViewDefinition
 */
public class PDMTaskRequestDatabaseViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinPDMRequest;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTaskRequestDatabaseViewDefinition object.
   */
  public PDMTaskRequestDatabaseViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_TASK);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.addSelectFieldDef(IBOBasePDMTaskDef.PN_SYSSYSTEMTYPE);
    this.joinPDMRequest.addSelectFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY);
    this.joinPDMRequest.addSelectFieldDef(IBOBasePDMRequestDef.PN_DATABASE_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinPDMRequest = addInnerJoinDef(BOBasePDMTaskDef.PN_PDMREQUEST_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOBasePDMTaskDef.PN_PRIMARYKEY);
  }
}
