// Planon Enterprise Edition Source file: BOPDMTaskAnalyzeImport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskimport;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * BOPDMTaskAnalyzeImport
 */
public class BOPDMTaskAnalyzeImport extends BOBasePDMTask
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskAnalyzeImport object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  protected BOPDMTaskAnalyzeImport(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
