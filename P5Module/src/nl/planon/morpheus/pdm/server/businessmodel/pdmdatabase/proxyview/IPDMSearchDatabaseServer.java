// Planon Enterprise Edition Source file: IPDMSearchDatabaseServer.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * IPDMSearchDatabaseServer
 *
 * @version $Revision$
 */
public interface IPDMSearchDatabaseServer
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PREFIX = "A_";
  public static final String ALIAS_PRIMARYKEY = PREFIX + IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY;
  public static final String ALIAS_ADMIN_USER = PREFIX + IBOBasePDMDatabaseServerDef.PN_ADMIN_USER;
  public static final String ALIAS_ADMIN_PASSWORD = PREFIX + IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS;
  public static final String ALIAS_FILE_TYPE_CODE = PREFIX + "FileTypeCode";
  public static final String ALIAS_SERVER_CODE = PREFIX + "ServerCode";
  public static final String ALIAS_VERSION_NUMBER = PREFIX + "VersionNumber";
  public static final String ALIAS_CONNECTION_URL = PREFIX + "ConnectionURL";
}
