// Planon Enterprise Edition Source file: BOMPDMReloadInitialDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;

import nl.planon.pdm.core.common.*;

/**
 * BOMPDMReloadInitialDef
 */
public class BOMPDMReloadInitialDef extends BOMPDMDatabaseRequestDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMPDMReloadInitialDef object.
   *
   * @param aBODef     BO definition!
   * @param aBOMPnName PnName of BOM
   */
  public BOMPDMReloadInitialDef(BODefinition aBODef, String aBOMPnName)
  {
    super(aBODef, aBOMPnName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Check if field Dump file on PDM server is filled in. If not, the request is useless
   *
   * @param  aPDMDatabase
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doBeforeCreateRequest(final BOPDMDatabase aPDMDatabase, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateRequest(aPDMDatabase, aPnContext);
    checkFieldMandatoryForRequest(aPDMDatabase, IBOPDMDatabaseDef.PN_PDM_DUMPFILE, aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IBOType getBOTypeRequest()
  {
    return BOTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IBOType getBOTypeTask(BOPDMDatabase aBOPDMDatabase)
  {
    return BOTypePDMP5Module.PDM_TASK_DELETE;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected PDMRequestType getRequestType() throws PnErrorListException
  {
    return BOPDMRequestTypeDef.REQUESTTYPE_RELOAD_INITIAL;
  }


  /**
   * Add bom rules to this bom.
   */
  private void addBOMRules()
  {
    final BRDatabaseImported brDatabaseImported = new BRDatabaseImported();

    addBOMRule(brDatabaseImported);
  }
}
