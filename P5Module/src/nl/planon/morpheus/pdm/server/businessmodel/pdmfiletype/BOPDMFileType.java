// Planon Enterprise Edition Source file: BOPDMFileType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMFileType
 */
public class BOPDMFileType extends BOSystemCode
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMFileType object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMFileType(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
