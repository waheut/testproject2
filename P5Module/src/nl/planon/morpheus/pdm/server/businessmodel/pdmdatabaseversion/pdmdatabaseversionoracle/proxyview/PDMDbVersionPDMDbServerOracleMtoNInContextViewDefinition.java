// Planon Enterprise Edition Source file: PDMDbVersionPDMDbServerOracleMtoNInContextViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

/**
 * PopupPDMDBVersionOracleViewDefinition
 */
public class PDMDbVersionPDMDbServerOracleMtoNInContextViewDefinition extends PDMDbVersionPDMDbServerOracleMtoNViewDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef compatibilityMatrixJoin;

  private PVJoinDef databaseServerJoin;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createJoinDefs(aPnContext);

    this.compatibilityMatrixJoin = addInnerJoinDef(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
    this.databaseServerJoin = this.compatibilityMatrixJoin.addInnerJoinDef(IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.databaseServerJoin, IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    IBaseValue pkBODefPDMDbVersion = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE).getPrimaryKeyInDB();
    addWhereDef(BOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL, pkBODefPDMDbVersion);
  }
}
