// Planon Enterprise Edition Source file: BOPDMRequestReloadInitial.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.reloadinitialrequest;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * BOPDMRequestReloadInitial
 */
public class BOPDMRequestReloadInitial extends BOBasePDMRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestReloadInitial object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMRequestReloadInitial(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
