// Planon Enterprise Edition Source file: BOMPDMImportDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.importrequest.*;

import nl.planon.pdm.core.common.*;

/**
 * BOMImportDatabaseDef
 */
public class BOMPDMImportDef extends BOMPDMDatabaseRequestDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMImportDatabaseDef object.
   *
   * @param aBODef
   * @param aBOMPnName
   */
  public BOMPDMImportDef(BODefinition aBODef, String aBOMPnName)
  {
    super(aBODef, aBOMPnName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Check if field Dumpfile on PDM server is filled in. If not, the request is useless
   *
   * @param  aPDMDatabase
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doBeforeCreateRequest(final BOPDMDatabase aPDMDatabase, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateRequest(aPDMDatabase, aPnContext);
    checkFieldMandatoryForRequest(aPDMDatabase, IBOPDMDatabaseDef.PN_PDM_DUMPFILE, aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IBOType getBOTypeRequest()
  {
    return BOTypePDMP5Module.PDM_REQUEST_IMPORT;
  }


  /**
   * {@inheritDoc}
   *
   * @throws PnErrorListException
   */
  @Override protected IBOType getBOTypeTask(BOPDMDatabase aPDMDatabase) throws PnErrorListException
  {
    if (aPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF).isEmpty() || aPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).isEmpty())
    {
      return BOTypePDMP5Module.PDM_TASK_ANALYZE_IMPORT;
    }

    return BOTypePDMP5Module.PDM_TASK_IMPORT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected PDMRequestType getRequestType() throws PnErrorListException
  {
    return BOPDMRequestImportDef.PDMREQUEST_TYPE;
  }


  /**
   * Add bom rules to this bom.
   */
  private void addBOMRules()
  {
    BRAllowImportDatabase brImportDatabase = new BRAllowImportDatabase();

    addBOMRule(brImportDatabase);
  }
}
