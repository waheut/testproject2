// Planon Enterprise Edition Source file: BOPDMFileTypeDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;

import nl.planon.pdm.core.common.*;

/**
 * Defines the file types that can be applied to a DatabaseVersion that PDM supports.
 */
public class BOPDMFileTypeDef extends BOSystemCodeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String SUBTYPE = "PDMFILETYPE";

  public static final String FILETYPE_MSSQL_DUMP = PDMFileType.MSSQL_DUMP.getCode();
  public static final String FILETYPE_ORACLE_EXP = PDMFileType.ORA_EXP.getCode();
  public static final String FILETYPE_ORACLE_DATAPUMP = PDMFileType.DATAPUMP.getCode();
  public static final String FILETYPE_ORACLE_DATAPUMP_AND_EXP = PDMFileType.DATAPUMP_AND_EXP.getCode();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMFileTypeDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMFileTypeDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the primary key of the given FileType code.
   *
   * @param  aCode
   * @param  aPnContext
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPKValue(Object aCode, PnContext aPnContext) throws PnErrorListException
  {
    return BOSystemCodeDef.getPKValue(aCode, new BaseStringValue(SUBTYPE), aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMFileType(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateSubType(PnContext aPnContext)
  {
    setParentBOType(BOTypeHades.SYSTEMCODE);
    setSystemSubTypeFieldValue(new BaseStringValue(SUBTYPE));
  }
}
