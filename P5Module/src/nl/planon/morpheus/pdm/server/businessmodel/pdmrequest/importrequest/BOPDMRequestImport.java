// Planon Enterprise Edition Source file: BOPDMRequestImport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.importrequest;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * BOPDMRequestImport
 */
public class BOPDMRequestImport extends BOBasePDMRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestImport object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMRequestImport(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
