// Planon Enterprise Edition Source file: BOMAddDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.syssource.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.businessobjectsmethod.rule.bom.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;

/**
 * BOMAddDef
 */
public class BOMAddDef extends BOMDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMAddDef object.
   *
   * @param aBODef     BODefinition
   * @param aPnBOMName Planon name of BOM, use when possible PnBOMName.PN_BOM_ADD.
   */
  public BOMAddDef(BODefinition aBODef, String aPnBOMName)
  {
    super(aBODef, aPnBOMName, BOMTypeHades.ADD);
    setSystem(true);
    init();
  }


  /**
   * Creates a new BOMAddDef object, with custom aBOMType.
   *
   * @param aBODef
   * @param aPnBOMName
   * @param aBOMType
   */
  protected BOMAddDef(BODefinition aBODef, String aPnBOMName, IBOMType aBOMType)
  {
    super(aBODef, aPnBOMName, aBOMType);
    init();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected BOM doCreateBOM(BusinessObject aBO, PnContext aPnContext) throws PnErrorListException
  {
    return new BOMAdd();
  }


  /**
   * Creates a new BO and sets the syssource if given and supported by the BO
   *
   * @throws PnErrorListException
   *
   * @see    BOM#execute
   */
  @Override protected BusinessObject execute(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    BusinessObject result = getBODefinition().createBO(aPnContext);
    result = setSysSource(result, aBOM);
    return result;
  }


  /**
   * Will read the syssource field from the BOM and set it in the BO. If the value was not set in
   * the BOM or the BO does not support such a field, this method does nothing.
   *
   * @param  aBO
   * @param  aBom
   *
   * @return The passed BO with the field set if applicable
   *
   * @throws PnErrorListException
   * @throws RuntimeException     when the syssource was not supported.
   */
  protected BusinessObject setSysSource(BusinessObject aBO, BOM aBom) throws PnErrorListException
  {
    String sysSource = aBom.getSysSource();

    // don't try to set the field when it is not there...
    if (!aBO.hasFieldByPnName(PnFieldName.INSERTSOURCESYSTEM_REF))
    {
      return aBO;
    }

    // the framework will set a syssource when none is given, so do nothing in that case
    if ((sysSource == null) || "".equals(sysSource))
    {
      return aBO;
    }

    if (!SUPPORTED_SYSSOURCES.contains(sysSource))
    {
      throw new RuntimeException("Unsupported syssource:" + sysSource);
    }

    SysSourceReferenceField fieldSourceInsert = (SysSourceReferenceField) aBO.getFieldByPnName(PnFieldName.INSERTSOURCESYSTEM_REF);
    fieldSourceInsert.setValueBySystemSourceCode(sysSource);
    return aBO;
  }


  /**
   * do extra initialisation
   */
  private void init()
  {
    addBOMRule(new BRNotNew());
    setBORequired(false);
    setNeedsBOValue(false);
    setExecuteAllowedWhenchanged();
  }
}
