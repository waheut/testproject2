// Planon Enterprise Edition Source file: DatafileLocationsOfDBServerViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * view definition for retrieving all datafile locations linked to a specific database server.
 */
public class DatafileLocationsOfDBServerViewDefinition extends PVDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_DATABASESERVER_REF = IBOPDMDBServerDataFileLocationDef.PN_DATABASESERVER_REF;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DatafileLocationsOfDBServersViewDefinition object.
   */
  public DatafileLocationsOfDBServerViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DBSERVER_DATAFILELOCATION);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(IBOPDMDBServerDataFileLocationDef.PN_DATABASESERVER_REF);
  }
}
