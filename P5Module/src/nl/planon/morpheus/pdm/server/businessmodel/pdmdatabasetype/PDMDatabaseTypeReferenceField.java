// Planon Enterprise Edition Source file: PDMDatabaseTypeReferenceField.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.pdm.core.common.*;

/**
 * PDMDatabaseTypeReferenceField
 */
public class PDMDatabaseTypeReferenceField extends SystemCodeReferenceField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseTypeReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMDatabaseTypeReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public String getCodeValue() throws PnErrorListException
  {
    return (String) super.getCodeValue();
  }


  /**
   * Get PDM Database type according to value of code
   *
   * @return PDM Database type
   *
   * @throws PnErrorListException
   */
  public PDMDatabaseType getPDMDatabaseType() throws PnErrorListException
  {
    return PDMDatabaseType.getPDMDatabaseTypeByCode(getCodeValue());
  }


  /**
   * sets specified type
   *
   * @param  aType type
   *
   * @throws PnErrorListException
   */
  public void setPDMDatabaseType(PDMDatabaseType aType) throws PnErrorListException
  {
    setCodeValue(aType.getCode());
  }
}
