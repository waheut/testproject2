// Planon Enterprise Edition Source file: BOPDMTaskDeleteDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskdelete;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMTaskDeleteDef
 */
public class BOPDMTaskDeleteDef extends BOBasePDMTaskDef implements IBOBasePDMTaskDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Sub type identifier
  private static final String SUB_TYPE_PDM_TASK_DELETE = PDMTaskType.DELETE.getCode();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskDeleteDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMTaskDeleteDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_TASK);
    setSystemSubTypeFieldValue(new BaseStringValue(SUB_TYPE_PDM_TASK_DELETE));
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_TASK_DELETE;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BOPDMTaskDelete doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMTaskDelete(this);
  }
}
