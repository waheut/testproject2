// Planon Enterprise Edition Source file: PDMDBVersionInContextOfPDMDBServerViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;

/**
 * PDMDBVersionInContextOfPDMDBServerViewDefinition
 */
public class PDMDBVersionInContextOfPDMDBServerViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDBServerpdmDBVersionJoin;

  private PVJoinDef pdmDBVersionJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDBVersionInContextOfPDMDBServerViewDefinition object.
   */
  public PDMDBVersionInContextOfPDMDBServerViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createJoinDefs(aPnContext);
    this.pdmDBServerpdmDBVersionJoin = addInnerJoinDef(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, BOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
    this.pdmDBVersionJoin = this.pdmDBServerpdmDBVersionJoin.addInnerJoinDef(BOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(final PnContext aPnContext) throws PnErrorListException
  {
    super.createStepSearchDefs(aPnContext);
    addStepSearchFieldDef(this.pdmDBVersionJoin, IBOBasePDMDatabaseVersionDef.PN_PRIMARYKEY);
  }
}
