// Planon Enterprise Edition Source file: PDMFileTypeOracleViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.proxyview;

import nl.planon.hades.basevalue.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * PDMFileTypeOracleViewDefinition
 */
public class PDMFileTypeOracleViewDefinition extends PDMFileTypeViewDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMFileTypeOracleViewDefinition object.
   */
  public PDMFileTypeOracleViewDefinition()
  {
    super(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_DATAPUMP), //
      new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_EXP), //
      new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_DATAPUMP_AND_EXP));
  }
}
