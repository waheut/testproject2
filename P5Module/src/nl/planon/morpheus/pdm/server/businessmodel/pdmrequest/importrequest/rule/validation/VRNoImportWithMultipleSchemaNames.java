// Planon Enterprise Edition Source file: VRNoImportWithMultipleSchemaNames.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.importrequest.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * VRNoImportWithMultipleSchemaNames
 */
public class VRNoImportWithMultipleSchemaNames extends ValidationRule<BOBasePDMRequest>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType MESSAGE_TYPE = ErrorNumberPDMP5Module.EC_CANNOT_IMPORT_WITH_MULTIPLE_SCHEMANAMES;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * If there are multiple schema names defined in the field OBJECT_OWNER_IN_DUMP of the PDM
   * database, the dump cannot be imported
   *
   * @param  aBO        BOBasePDMRequest
   * @param  aOldBO     BOBasePDMRequest
   * @param  aPnContext aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMRequest aBO, BOBasePDMRequest aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    BusinessObject boPDMDatabase = aBO.getReferenceFieldByPnName(IBOBasePDMRequestDef.PN_DATABASE_REF).getReferenceBO();
    String schemaName = boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_OBJECT_OWNER_IN_DUMP).getAsString();

    if ((schemaName != null) && (schemaName.contains(",")))
    {
      PnMessage message = aPnContext.getPnErrorManager().createMessage(MESSAGE_TYPE);
      aPnContext.getPnErrorManager().add(message);
    }
  }
}
