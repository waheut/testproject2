// Planon Enterprise Edition Source file: BOMPDMDeleteDef.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.bom;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.deleterequest.*;

import nl.planon.pdm.core.common.*;

/**
 * BOMDeleteDatabaseDef
 */
public class BOMPDMDeleteDef extends BOMPDMDatabaseRequestDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMPDMDeleteDef object.
   *
   * @param aBODef
   * @param aBOMPnName
   */
  public BOMPDMDeleteDef(BODefinition aBODef, String aBOMPnName)
  {
    super(aBODef, aBOMPnName);
    setChangeWithinSelectionAllowed(true);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected IBOType getBOTypeRequest()
  {
    return BOTypePDMP5Module.PDM_REQUEST_DELETE;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IBOType getBOTypeTask(BOPDMDatabase aBOPDMDatabase)
  {
    return BOTypePDMP5Module.PDM_TASK_DELETE;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected PDMRequestType getRequestType() throws PnErrorListException
  {
    return BOPDMRequestDeleteDef.PDMREQUEST_TYPE;
  }


  /**
   * Add bom rules to this bom.
   */
  private void addBOMRules()
  {
    BRAllowDeleteDatabase brDeleteDatabase = new BRAllowDeleteDatabase();

    addBOMRule(brDeleteDatabase);
  }
}
