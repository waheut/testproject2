// Planon Enterprise Edition Source file: BOPDMDatabaseStateDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasestate;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.state.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * BOPDMDatabaseStateDef
 */
public class BOPDMDatabaseStateDef extends TemplateStateDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseStateDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDatabaseStateDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected BODefFilter doCreatePermanentWhere(PnContext aPnContext) throws PnErrorListException
  {
    BODefinition boDef = getBODefinition(BOTypePDMP5Module.PDM_DATABASE);
    BODefFilter result = new BODefFilter(this, BOBaseBOStateDef.PN_BUSINESSOBJECT_REF, SCOperatorType.EQUAL, boDef.getPrimaryKeyInDB());
    result.addSCNode(getSystemStatesSCNode());
    return result;
  }
}
