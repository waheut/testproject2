// Planon Enterprise Edition Source file: FCTakeOverDatabaseServerFields.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.fieldchange;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * FCTakeOverDatabaseServerFields
 */
public class FCTakeOverDatabaseServerFields extends FieldChangeAction<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * If field DatabaseServerRef changes the field pdmLocationRef is copied from the
   * BOPDMDatabaseServer and the field DatabaseTypeRef is copied from the BODatabaseVersion that can
   * be retrieved via the BODatabaseServer
   *
   * @param  aFCContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void execute(FieldChangeActionContext<BOPDMDatabase> aFCContext, PnContext aPnContext) throws PnErrorListException
  {
    BOPDMDatabase boPDMDatabase = aFCContext.getBusinessObject();

    ReferenceField pdmDatabaseServerRefField = boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    ReferenceField pdmDatabaseTypeRefField = boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF);

    if (!pdmDatabaseServerRefField.isEmpty())
    {
      ReferenceField pdmPDMLocationRefField = boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_PDMLOCATION_REF);
      pdmPDMLocationRefField.setAsBaseValue(pdmDatabaseServerRefField.getFieldValueOfReferencedBO(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF));

      PDMDatabaseType serverDatabaseType = boPDMDatabase.getDatabaseTypeOfServer(aPnContext);
      pdmDatabaseTypeRefField.setAsBaseValue(BOPDMDatabaseTypeDef.getPKValue(serverDatabaseType.getCode(), aPnContext));

      BusinessObject businessObject = boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
      ReferenceField databaseServerFileTypeRefField = businessObject.getReferenceFieldByPnName(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF);
      IBaseValue pdmServerFileType = databaseServerFileTypeRefField.getFieldValueOfReferencedBO(BOPDMFileTypeDef.PN_CODE);

      if (pdmServerFileType != null)
      {
        if (!pdmServerFileType.getAsString().equals(PDMFileType.DATAPUMP_AND_EXP.getCode()))
        {
          if (boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF).isEmpty())
          {
            boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF).setAsBaseValue(new BaseIntegerValue(databaseServerFileTypeRefField.getAsInteger()));
          }
          if (boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_EXPORT_FILE_TYPE).isEmpty())
          {
            boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_EXPORT_FILE_TYPE).setAsBaseValue(new BaseIntegerValue(databaseServerFileTypeRefField.getAsInteger()));
          }
        }
      }
    }
  }
}
