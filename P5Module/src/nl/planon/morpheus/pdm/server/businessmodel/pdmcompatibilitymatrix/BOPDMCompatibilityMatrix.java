// Planon Enterprise Edition Source file: BOPDMCompatibilityMatrix.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMCompatibilityMatrix
 */
public class BOPDMCompatibilityMatrix extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMCompatibilityMatrix object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMCompatibilityMatrix(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
