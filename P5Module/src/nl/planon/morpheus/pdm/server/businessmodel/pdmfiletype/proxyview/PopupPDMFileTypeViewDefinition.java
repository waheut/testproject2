// Planon Enterprise Edition Source file: PopupPDMFileTypeViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * PopupPDMFileTypeViewDefinition
 */
public class PopupPDMFileTypeViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PopupPDMFileTypeViewDefinition object.
   */
  public PopupPDMFileTypeViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_FILE_TYPE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createSearchDefs(aPnContext);

    addSearchFieldDef(BOPDMFileTypeDef.PN_CODE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(final PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(BOPDMFileTypeDef.PN_CODE, SCOperatorType.NOT_IN);
  }


  /**
   * Filters the selected PDM File Types to choose only those file types that correspond with the
   * PDM Database Type or kind of PDM Database Server that is filled in
   *
   * @param  aProxyListContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doBeforeCreateSQLStatement(ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateSQLStatement(aProxyListContext, aPnContext);

    BusinessObject contextBO = aProxyListContext.getBO();
    if (contextBO != null)
    {
      if (contextBO instanceof BOPDMDatabase)
      {
        // get database type if filled in...
        BOPDMDatabase boPDMDatabase = (BOPDMDatabase) contextBO;
        PDMDatabaseType pdmDatabaseType = boPDMDatabase.getPDMDatabaseType();
        if (pdmDatabaseType == null)
        {
          // .. if not filled in, get database type of database server if filled in...
          BOBasePDMDatabaseServer boPDMDatabaseServer = (BOBasePDMDatabaseServer) contextBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();
          if (boPDMDatabaseServer != null)
          {
            pdmDatabaseType = boPDMDatabaseServer.getPDMDatabaseType();
          }
        }
        ISCOperator operator = getPermanentSearchCriteria(aPnContext).getOperator(BOPDMFileTypeDef.PN_CODE, SCOperatorType.NOT_IN);
        // Always exclude combined filetype Datapump and Exp, the user is not allowed to select this one for a PDM Database
        operator.addBaseValue(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_DATAPUMP_AND_EXP));
        // add extra criteria depending on the database type that is found
        BOBasePDMDatabaseServer boPDMDatabaseServer = (BOBasePDMDatabaseServer) contextBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getReferenceBO();

        if (boPDMDatabaseServer != null)
        {
          if (pdmDatabaseType.equals(PDMDatabaseType.ORACLE))
          {
            String fileType = boPDMDatabaseServer.getReferenceFieldByPnName(IBOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF).getFieldValueOfReferencedBO(BOPDMFileTypeDef.PN_CODE).getAsString();

            // in case of database type Oracle, exclude filetypes corresponding MSSQL
            operator.addBaseValue(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_MSSQL_DUMP));

            if (fileType.equals(BOPDMFileTypeDef.FILETYPE_ORACLE_DATAPUMP))
            {
              operator.addBaseValue(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_EXP));
            }
            else if (fileType.equals(BOPDMFileTypeDef.FILETYPE_ORACLE_EXP))
            {
              operator.addBaseValue(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_DATAPUMP));
            }
          }
          else if (PDMDatabaseType.MSSQL.equals(pdmDatabaseType))
          {
            // in case of database type MSSQL, exclude filetypes corresponding Oracle
            operator.addBaseValue(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_DATAPUMP));
            operator.addBaseValue(new BaseStringValue(BOPDMFileTypeDef.FILETYPE_ORACLE_EXP));
          }
        }
      }
    }
  }
}
