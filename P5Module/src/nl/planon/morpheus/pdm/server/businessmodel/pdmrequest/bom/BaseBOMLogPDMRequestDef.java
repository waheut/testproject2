// Planon Enterprise Edition Source file: BaseBOMLogPDMRequestDef.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.bom;

import java.util.*;

import nl.planon.aphrodite.businessobjectsmethod.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.businessobjectsmethod.bomfield.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.localize.*;
import nl.planon.hades.util.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;
import nl.planon.morpheus.pdm.server.logging.requestlogging.*;

import nl.planon.util.pnlogging.*;

/**
 * BOM Definition to actually log information to the event log of a PDM Request.
 */
public abstract class BaseBOMLogPDMRequestDef extends BOMDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(BaseBOMLogPDMRequestDef.class);

  //~ Instance Variables ---------------------------------------------------------------------------

  protected BOBasePDMRequest boPDMRequest;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMBaseLogPDMRequestDef object.
   *
   * @param aBODef
   * @param aPnBOMName
   */
  public BaseBOMLogPDMRequestDef(BODefinition aBODef, String aPnBOMName)
  {
    super(aBODef, aPnBOMName, BOMTypeAphrodite.GENERIC);
    setSystem(true);
    setVisible(false);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get BO to which the event log must be linked to
   *
   * @param  aBOM
   * @param  aPnContext
   *
   * @return bo to which the event log must be linked to
   *
   * @throws PnErrorListException
   */
  protected abstract BusinessObject getLinkedBO(BOM aBOM, PnContext aPnContext) throws PnErrorListException;


  /**
   * constructs the content that has to be filled in in field Source of the event log
   *
   * @param  aBOM
   * @param  aPnContext
   *
   * @return the value that has to be filled in in the source field of the event log
   *
   * @throws PnErrorListException
   */
  protected abstract String getSource(BOM aBOM, PnContext aPnContext) throws PnErrorListException;


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateBOMFieldDefs()
  {
    addBOMFieldDef(new BOMObjectFieldDef(this, IBOBasePDMRequestDef.ARG_MESSAGE_INFORMATION));
    addBOMFieldDef(new BOMObjectFieldDef(this, IBOBasePDMRequestDef.ARG_SEVERITY_OF_LOG));
    addBOMFieldDef(new BOMObjectFieldDef(this, IBOBasePDMRequestDef.ARG_ERROR_INFORMATION));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject execute(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    this.boPDMRequest = (BOBasePDMRequest) convertBOValueToBO(aBOM.getBOValue(), aPnContext);
    // error information
    StringBuilder ex_msg = getMessageInformation(aBOM);
    List<Pair<String, String>> errorInformationList = getErrorInformation(aBOM);
    String exceptionMessage = replaceErrorCodesWithTranslations(ex_msg, errorInformationList, aPnContext);
    StringBuilder sbDetails = new StringBuilder();
    sbDetails.append(exceptionMessage).append(PDMRequestLogger.NEW_LINE);

    // severity information
    EventLogTypeSeverity severity = getSeverity(aBOM);

    // log failure to Event Log
    PDMRequestLog pdmRequestLog = new PDMRequestLog(severity, sbDetails.toString(), false, aPnContext);
    BusinessObject linkedBO = getLinkedBO(aBOM, aPnContext);
    if (linkedBO != null) // can be null e.g. if no db server yet is determined
    {
      PDMRequestLogger.logMessagePerPDMRequest(getSource(aBOM, aPnContext), pdmRequestLog, linkedBO, aPnContext);
    }
    else
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info(sbDetails.toString());
      }
    }
    return null;
  }


  /**
   * get error information of message.
   *
   * @param  aBOM
   *
   * @return list of error information of message
   */
  protected List<Pair<String, String>> getErrorInformation(BOM aBOM)
  {
    return (List<Pair<String, String>>) aBOM.getFieldByPnName(IBOBasePDMRequestDef.ARG_ERROR_INFORMATION).getAsObject();
  }


  /**
   * get message information .
   *
   * @param  aBOM
   *
   * @return message information
   */
  protected StringBuilder getMessageInformation(BOM aBOM)
  {
    return (StringBuilder) aBOM.getFieldByPnName(IBOBasePDMRequestDef.ARG_MESSAGE_INFORMATION).getAsObject();
  }


  /**
   * get severity of log message.
   *
   * @param  aBOM
   *
   * @return severity of log message
   */
  protected EventLogTypeSeverity getSeverity(BOM aBOM)
  {
    return (EventLogTypeSeverity) aBOM.getFieldByPnName(IBOBasePDMRequestDef.ARG_SEVERITY_OF_LOG).getAsObject();
  }


  /**
   * get translated display text of the request type
   *
   * @param  aPnContext
   *
   * @return the translated display text of the request type
   *
   * @throws PnErrorListException
   */
  protected String getTranslatedDisplayText(PnContext aPnContext) throws PnErrorListException
  {
    ITranslator translator = aPnContext.getCache(ServerTranslatorCache.class).getTranslator(aPnContext);
    ITranslatable key = this.boPDMRequest.getReferenceFieldByPnName(IBOBasePDMRequestDef.PN_REQUESTTYPE_REF).getLocalizableDisplayValue();
    key.localize(translator);
    return key.getDisplayText();
  }


  /**
   * the message returned from the database agent, contains literal error codes (PDMS_xxxx) and
   * possible message variables. Before logging these errors, these codes must be translated and
   * variables included in the message.
   *
   * @param  aMessage              complete message returned from the database agent
   * @param  aErrorInformationList the error information, including the message variables per error
   * @param  aPnContext
   *
   * @return the message in which all the error codes are replaced by the corresponding
   *         ttranslations
   *
   * @throws PnErrorListException
   */
  private String replaceErrorCodesWithTranslations(StringBuilder aMessage, List<Pair<String, String>> aErrorInformationList, PnContext aPnContext) throws PnErrorListException
  {
    final String pattern = ErrorNumberPDMP5Module.PREFIX + "_";
    StringBuilder summary = new StringBuilder();

    String regExp = pattern + "[0-9]{4}";
    String message = aMessage.substring(0);

    int index = message.indexOf(pattern);
    while (index > -1)
    {
      String toReplace = message.substring(index, index + 9);
      if (toReplace.matches(regExp))
      {
        MessageType messageType = ErrorNumberPDMP5Module.getMessageTypeByCode(toReplace);
        ITranslator translator = aPnContext.getCache(ServerTranslatorCache.class).getTranslator(aPnContext);
        PnMessage msg = aPnContext.getPnErrorManager().createMessage(messageType);
        if (aErrorInformationList != null)
        {
          for (Pair<String, String> errorInformation : aErrorInformationList)
          {
            if (toReplace.equals(errorInformation.getLeftHandSide()))
            {
              msg.addStringArgument(errorInformation.getRightHandSide());
            }
          }
        }
        msg.localize(translator);
        String translatedMessage = "***** PDM: " + msg.getDisplayText();
        if (summary.length() == 0)
        {
          summary.append("\n********** Start of PDM Error message(s) **********\n*****\n");
        }
        summary.append(translatedMessage + "\n");

        message = message.replaceAll(toReplace, translatedMessage);
      }
      index = message.indexOf(pattern);
    }
    if (summary.length() > 0)
    {
      summary.append("*****\n*********** End of PDM Error message(s) ***********\n\n\n");
    }

    return summary + message;
  }
}
