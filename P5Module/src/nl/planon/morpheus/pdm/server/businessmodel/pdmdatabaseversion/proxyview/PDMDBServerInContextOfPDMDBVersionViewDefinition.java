// Planon Enterprise Edition Source file: PDMDBServerInContextOfPDMDBVersionViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;

/**
 * PDMDBServerInContextOfPDMDBVersionViewDefinition
 */
public class PDMDBServerInContextOfPDMDBVersionViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDBServerJoin;
  private PVJoinDef pdmDBVersionpdmDBServerJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDBVersionInContextOfPDMDBServer object.
   */
  public PDMDBServerInContextOfPDMDBVersionViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createJoinDefs(aPnContext);
    this.pdmDBVersionpdmDBServerJoin = addInnerJoinDef(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, BOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
    this.pdmDBServerJoin = this.pdmDBVersionpdmDBServerJoin.addInnerJoinDef(BOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(final PnContext aPnContext) throws PnErrorListException
  {
    super.createStepSearchDefs(aPnContext);
    addStepSearchFieldDef(this.pdmDBServerJoin, IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
  }
}
