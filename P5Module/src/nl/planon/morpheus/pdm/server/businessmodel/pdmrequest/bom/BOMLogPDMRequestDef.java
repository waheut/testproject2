// Planon Enterprise Edition Source file: BOMLogPDMRequestDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

/**
 * BOMLogPDMRequestDef
 */
public class BOMLogPDMRequestDef extends BaseBOMLogPDMRequestDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMLogPDMRequestDef object.
   *
   * @param aBODef
   * @param aPnBOMName
   */
  public BOMLogPDMRequestDef(BODefinition aBODef, String aPnBOMName)
  {
    super(aBODef, aPnBOMName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject getLinkedBO(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    return this.boPDMRequest;
  }


  /**
   * {@inheritDoc}
   *
   * @throws PnErrorListException
   */
  @Override protected String getSource(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    BusinessObject boPDMDatabase = this.boPDMRequest.getReferenceFieldByPnName(IBOBasePDMRequestDef.PN_DATABASE_REF).getReferenceBO();
    return boPDMDatabase.getDisplayValue(aPnContext) + " - " + getTranslatedDisplayText(aPnContext);
  }
}
