// Planon Enterprise Edition Source file: PDMFileTypeLookupViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * PDMFileTypeLookupViewDefinition
 */
public class PDMFileTypeLookupViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinDbTypeFileType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMFileTypeLookupViewDefinition object.
   */
  public PDMFileTypeLookupViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_FILE_TYPE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDbTypeFileType = addLeftJoinDef(BOTypePDMP5Module.PDM_DB_TYPE_FILE_TYPE, IBOPDMDBTypePDMFileTypeDef.PN_FILE_TYPE_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.joinDbTypeFileType.addSearchFieldDef(IBOPDMDBTypePDMFileTypeDef.PN_DATABASE_TYPE_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(new PVExpressionNodeDef(getMasterJoinDef(), BOPDMFileTypeDef.PN_CODE, SCOperatorType.NOT_EQUAL, new BaseStringValue(PDMFileType.DATAPUMP_AND_EXP.getCode())));
  }
}
