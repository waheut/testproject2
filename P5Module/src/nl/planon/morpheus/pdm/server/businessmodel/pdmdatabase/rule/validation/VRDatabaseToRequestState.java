// Planon Enterprise Edition Source file: VRDatabaseToRequestState.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.statemachine.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequestexecutestate.*;

/**
 * validation rule on state change Database, which is dependent on state last added Request.
 */
public class VRDatabaseToRequestState extends ValidationRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Validation rule: The database cannot be set to Initial or Available when the last added request
   * is in state Initial or InProgress.
   *
   * @param  aBO        bo PDMDatabase after change
   * @param  aOldBO     bo PDMDatabase before change
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    State state = aBO.getSystemState();
    State oldState = aOldBO.getSystemState();

    if (!state.equals(oldState))
    {
      PVRuntimeDefinition pvDef = new PVRuntimeDefinition(BOTypePDMP5Module.BASE_PDM_REQUEST);
      PVJoinDef joinRequestSystemState = pvDef.addInnerJoinDef(IBOBasePDMRequestDef.PN_REF_BOSTATE);
      pvDef.addEqualWhereDef(IBOBasePDMRequestDef.PN_DATABASE_REF, aBO.getPrimaryKeyValue());
      pvDef.addSortDef(IBOBasePDMRequestDef.PN_SYSINSERTDATETIME, false); // newest first
      joinRequestSystemState.addSelectFieldDef(BOPDMRequestExecuteStateDef.PN_PNNAME);
      pvDef.setResultLimit(1);

      ProxyListValue plvPDMRequest = aPnContext.createByPVDefinition(pvDef);

      if (!plvPDMRequest.isEmpty())
      {
        int pKPDMRequest = plvPDMRequest.getColumnNumberByAliasName(BOPDMRequestExecuteStateDef.PN_PNNAME);
        String requestStateName = plvPDMRequest.getFirstProxy().getProxyItem(pKPDMRequest).getAsString();

        String dbStateName = state.getPnName();
        String displayValueDbState = state.getLocalizableKey();
        String displayValueRequestState = aPnContext.getBODefinition(BOTypePDMP5Module.BASE_PDM_REQUEST).getStateControllerDef().getSystemStateByPnName(requestStateName).getLocalizableKey();
        // when Database state change to Initial or Available and the request state is InProgress or Initial, then he refuses the action
        if ((IBOPDMDatabaseDef.STATE_INITIAL.equals(dbStateName) || (IBOPDMDatabaseDef.STATE_AVAILABLE.equals(dbStateName))) //
          && (IBOBasePDMRequestDef.STATE_IN_PROGRESS.equals(requestStateName) || IBOBasePDMRequestDef.STATE_INITIAL.equals(requestStateName)))
        {
          errorCreate(ErrorNumberPDMP5Module.EC_DATABASE_STATE_IS_NOT_ALLOWED, displayValueDbState, displayValueRequestState, aPnContext);
        }
        //change to state busy is always possible.
      }
    }
  }


  /**
   * throw an error.
   *
   * @param  aMessageType  error message
   * @param  aDbState      display value of database state
   * @param  aRequestState display value of request state
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  private void errorCreate(MessageType aMessageType, String aDbState, String aRequestState, PnContext aPnContext) throws PnErrorListException
  {
    PnMessage msg = aPnContext.getPnErrorManager().createMessage(aMessageType);
    msg.addLocalizebleKeyArgument(aDbState);
    msg.addLocalizebleKeyArgument(aRequestState);
    aPnContext.getPnErrorManager().addFatal(msg);
  }
}
