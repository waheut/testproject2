// Planon Enterprise Edition Source file: BOPDMRequestExecuteStateDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequestexecutestate;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.state.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * BOPDMRequestExecuteStateDef
 */
public class BOPDMRequestExecuteStateDef extends TemplateStateDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestExecuteStateDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMRequestExecuteStateDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected BODefFilter doCreatePermanentWhere(PnContext aPnContext) throws PnErrorListException
  {
    BODefFilter result = new BODefFilter(this, BOBaseBOStateDef.PN_BUSINESSOBJECT_REF, SCOperatorType.IN);

    BODefinition pdmRequest = getBODefinition(BOTypePDMP5Module.BASE_PDM_REQUEST);
    List<BODefinition> subBODefinitions = pdmRequest.getSubBOList();
    for (BODefinition boDef : subBODefinitions)
    {
      result.addBaseValue(boDef.getPrimaryKeyInDB());
    }
    result.addSCNode(getSystemStatesSCNode());
    return result;
  }
}
