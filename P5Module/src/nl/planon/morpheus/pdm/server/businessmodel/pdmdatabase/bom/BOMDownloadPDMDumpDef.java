// Planon Enterprise Edition Source file: BOMDownloadPDMDumpDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.bom;

import nl.planon.aphrodite.businessobjectsmethod.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;

import nl.planon.util.pnlogging.*;

/**
 * BOMDownloadPDMDumpDef
 */
public class BOMDownloadPDMDumpDef extends BOMDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(BOMDownloadPDMDumpDef.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMDownloadPDMDumpDef object.
   *
   * @param aBODef     BusinessObject definition;
   * @param aBOMPnName Planon name of BOM, this name has to be unique for all BOM defs within a
   *                   BODefinition.
   */
  public BOMDownloadPDMDumpDef(BODefinition aBODef, String aBOMPnName)
  {
    super(aBODef, aBOMPnName, BOMTypeAphrodite.GENERIC);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject execute(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    BusinessObject result = null;

    if (aBOM.getBOValue() != null)
    {
      result = BODefinition.convertBOValueToBO(aBOM.getBOValue(), aPnContext);
    }

    return result;
  }
}
