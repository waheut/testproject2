// Planon Enterprise Edition Source file: PDMRequestViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * PDMRequestViewDefinition
 */
public class PDMRequestViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestViewDefinition object.
   */
  public PDMRequestViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_REQUEST);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createFieldDefs(aPnContext);
    addSelectFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY);
  }


  /**
   * @see PVDefinition#createSearchDefs()
   */
  @Override protected void createSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSearchFieldDef(BOBasePDMRequestDef.PN_DATABASE_REF);
  }
}
