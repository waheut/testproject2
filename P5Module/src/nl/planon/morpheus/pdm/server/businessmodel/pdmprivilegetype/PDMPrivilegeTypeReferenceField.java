// Planon Enterprise Edition Source file: PDMPrivilegeTypeReferenceField.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.pdm.core.common.*;

/**
 * PDMPrivilegeTypeReferenceField
 */
public class PDMPrivilegeTypeReferenceField extends SystemCodeReferenceField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMPrivilegeTypeReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMPrivilegeTypeReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public String getCodeValue() throws PnErrorListException
  {
    return (String) super.getCodeValue();
  }


  /**
   * Get PDM privilege type according to value of code
   *
   * @return PDM privilege type
   *
   * @throws PnErrorListException
   */
  public PDMPrivilegeType getPDMDatabaseType() throws PnErrorListException
  {
    return PDMPrivilegeType.getPDMPrivilegeTypeByCode(getCodeValue());
  }


  /**
   * sets specified type
   *
   * @param  aType type
   *
   * @throws PnErrorListException
   */
  public void setPDMPrivilegeType(PDMPrivilegeType aType) throws PnErrorListException
  {
    setCodeValue(aType.getCode());
  }
}
