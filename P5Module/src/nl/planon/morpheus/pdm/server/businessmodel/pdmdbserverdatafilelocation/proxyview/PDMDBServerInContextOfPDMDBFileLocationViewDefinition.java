// Planon Enterprise Edition Source file: PDMDBServerInContextOfPDMDBFileLocationViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.proxyview;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.*;

/**
 * PDMDBServerInContextOfPDMDBFileLocationViewDefinition
 */
public class PDMDBServerInContextOfPDMDBFileLocationViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDBServerFileLocationJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDBVersionInContextOfPDMDBServer object.
   */
  public PDMDBServerInContextOfPDMDBFileLocationViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDBServerFileLocationJoin.addSelectFieldDef(IBOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL);
    this.pdmDBServerFileLocationJoin.addSelectFieldDef(IBOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_NETWORK);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createJoinDefs(aPnContext);
    this.pdmDBServerFileLocationJoin = addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY, BOTypePDMP5Module.PDM_DBSERVER_DATAFILELOCATION, BOPDMDBServerDataFileLocationDef.PN_DATABASESERVER_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(final PnContext aPnContext) throws PnErrorListException
  {
    super.createStepSearchDefs(aPnContext);
    addStepSearchFieldDef(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
  }
}
