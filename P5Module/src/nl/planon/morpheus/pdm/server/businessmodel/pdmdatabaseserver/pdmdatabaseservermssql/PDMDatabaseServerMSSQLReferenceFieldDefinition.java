// Planon Enterprise Edition Source file: PDMDatabaseServerMSSQLReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseServerMSSQLReferenceFieldDefinition
 */
public class PDMDatabaseServerMSSQLReferenceFieldDefinition extends ReferenceIntegerFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseServerMSSQLReferenceFieldDefinition object.
   *
   * @param aFreeFieldConversionInfo
   */
  public PDMDatabaseServerMSSQLReferenceFieldDefinition(FreeFieldConversionInfo aFreeFieldConversionInfo)
  {
    this(aFreeFieldConversionInfo.getBODefinition(), aFreeFieldConversionInfo.getPnName());
    setDB(aFreeFieldConversionInfo.getDBName(), aFreeFieldConversionInfo.getDBType());
  }


  /**
   * Creates a new PDMDatabaseServerMSSQLReferenceFieldDefinition object.
   *
   * <p>Default constructor to retrieve an instance of the field definition. Use
   * FieldTypeManager.constructFieldDefinition to construct a field definition</p>
   *
   * @param aBODefinition BusinessObject definition
   * @param aBOType       BOType
   */
  public PDMDatabaseServerMSSQLReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMDatabaseServerMSSQLReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   */
  public PDMDatabaseServerMSSQLReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName)
  {
    super(aBODefinition, aPlanonName, BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL);
    setPlanonFieldType(FieldTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL);
  }


  /**
   * Creates a new PDMDatabaseServerMSSQLReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   * @param aDBName       the name of the database field.
   */
  public PDMDatabaseServerMSSQLReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName, String aDBName)
  {
    this(aBODefinition, aPlanonName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public boolean isFreeFieldConversionAllowed()
  {
    return true;
  }


  /**
   * Create a PDMDatabaseServerMSSQLReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return SystemCodeReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMDatabaseServerMSSQLReferenceField(aBO, this);
  }
}
