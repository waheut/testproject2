// Planon Enterprise Edition Source file: PDMFileTypeReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMFileTypeReferenceFieldDefinition
 */
public class PDMFileTypeReferenceFieldDefinition extends SystemCodeReferenceFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMFileTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aBOType
   */
  public PDMFileTypeReferenceFieldDefinition(final BODefinition aBODefinition, final IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMFileTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   */
  public PDMFileTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName)
  {
    super(aBODefinition, aPnName, BOTypePDMP5Module.PDM_DATABASE_FILE_TYPE, FieldTypePDMP5Module.PDM_DATABASE_FILE_TYPE);
  }


  /**
   * Creates a new PDMFileTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   * @param aDBName
   */
  public PDMFileTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName, final String aDBName)
  {
    this(aBODefinition, aPnName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Create a PDMFileTypeReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return SystemCodeReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMFileTypeReferenceField(aBO, this);
  }
}
