// Planon Enterprise Edition Source file: PDMDatabaseServerMSSQLReferenceField.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

/**
 * PDMDatabaseServerMSSQLReferenceField
 */
public class PDMDatabaseServerMSSQLReferenceField extends ReferenceIntegerField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseServerMSSQLReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMDatabaseServerMSSQLReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }
}
