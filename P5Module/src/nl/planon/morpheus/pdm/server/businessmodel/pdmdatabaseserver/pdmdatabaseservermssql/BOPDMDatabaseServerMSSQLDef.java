// Planon Enterprise Edition Source file: BOPDMDatabaseServerMSSQLDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * BOPDMDatabaseServerMSSQLDef
 */
public class BOPDMDatabaseServerMSSQLDef extends BOBasePDMDatabaseServerDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PN_CODE_PAGE = PN_CODE_PAGE__NOT_IN_ALL_SUBTYPES;
  public static final String PN_SORT_ORDER = PN_SORT_ORDER__NOT_IN_ALL_SUBTYPES;

  // list of fields that hold specific settings of the database server
  protected static final String[] ORACLE_DATABASE_SERVER_SETTING_FIELDNAMES =
  {
    PN_CODE_PAGE, //
    PN_SORT_ORDER
  };

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerMSSQLDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDatabaseServerMSSQLDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public Set<String> getDatabaseServerSettingFieldNames()
  {
    Set<String> result = super.getDatabaseServerSettingFieldNames();

    result.add(PN_CODE_PAGE);
    result.add(PN_SORT_ORDER);

    return result;
  }


  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDatabaseServerMSSQL(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateFieldDefs(PnContext aPnContext)
  {
    removeFieldDefByPnName(PN_TEMPORARY_TABLESPACENAME__NOT_IN_ALL_SUBTYPES);
    removeFieldDefByPnName(PN_CONNECT_TNS_NAME__NOT_IN_ALL_SUBTYPES);
    removeFieldDefByPnName(PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES);
    removeFieldDefByPnName(PN_DATABASEVERSION_REF);
    removeFieldDefByPnName(PN_EXPORT_LOCATION__NOT_IN_ALL_SUBTYPES);
    removeFieldDefByPnName(PN_IMPORT_LOCATION__NOT_IN_ALL_SUBTYPES);

    PDMFileTypeReferenceFieldDefinition pdmFileTypeRefFieldDef = (PDMFileTypeReferenceFieldDefinition) getReferenceFieldDefByPnName(PN_FILE_TYPE_REF);
    pdmFileTypeRefFieldDef.setFieldLookupDefinition(PVTypePDMP5Module.PDMFILETYPEMSSQL);

    PDMDatabaseVersionMSSQLReferenceFieldDefinition fieldDef = new PDMDatabaseVersionMSSQLReferenceFieldDefinition(this, PN_DATABASEVERSION_REF, DB_DATABASEVERSION_REF);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNInContextPVType()
  {
    return PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_MSSQL_IN_CONTEXT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNPVType()
  {
    return PVTypePDMP5Module.PDMDBVERSION_MTON_PDMDBSERVER_MSSQL;
  }
}
