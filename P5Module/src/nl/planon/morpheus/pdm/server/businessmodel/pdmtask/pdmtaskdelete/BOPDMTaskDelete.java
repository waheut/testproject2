// Planon Enterprise Edition Source file: BOPDMTaskDelete.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskdelete;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * BOPDMTaskDelete
 */
public class BOPDMTaskDelete extends BOBasePDMTask
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskDelete object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  protected BOPDMTaskDelete(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
