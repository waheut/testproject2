// Planon Enterprise Edition Source file: VRTaskToRequestState.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.statemachine.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequestexecutestate.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * validation rule on state change Task, which is dependent on state linked Request.
 */
public class VRTaskToRequestState extends ValidationRule<BOBasePDMTask>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Validation rule task state change. rules: The task cannot be set to Ok or Notok, if the linked
   * request is still in state Initial
   *
   * @param  aBO        bo pdmtask after change
   * @param  aOldBO     bo pdmtask before change
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMTask aBO, BOBasePDMTask aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    State state = aBO.getSystemState();
    State oldState = aOldBO.getSystemState();

    if (!state.equals(oldState))
    {
      PVRuntimeDefinition pvDef = new PVRuntimeDefinition(BOTypePDMP5Module.BASE_PDM_TASK);
      PVJoinDef pdmRequest = pvDef.addInnerJoinDef(IBOBasePDMTaskDef.PN_PDMREQUEST_REF);
      PVJoinDef pdmrequestState = pdmRequest.addInnerJoinDef(IBOBasePDMRequestDef.PN_REF_BOSTATE);
      pvDef.addEqualWhereDef(IBOBasePDMTaskDef.PN_PRIMARYKEY, aBO.getPrimaryKeyValue());
      pdmrequestState.addSelectFieldDef(BOPDMRequestExecuteStateDef.PN_PNNAME);

      pvDef.setResultLimit(1);

      ProxyListValue plvPDMRequest = aPnContext.createByPVDefinition(pvDef);

      if (!plvPDMRequest.isEmpty())
      {
        int pkPDMRequest = plvPDMRequest.getColumnNumberByAliasName(BOPDMRequestExecuteStateDef.PN_PNNAME);
        String requestStateName = plvPDMRequest.getFirstProxy().getProxyItem(pkPDMRequest).getAsString();

        String taskStateName = state.getPnName();
        if ((BOBasePDMTaskDef.STATE_NOK.equals(taskStateName) || BOBasePDMTaskDef.STATE_OK.equals(taskStateName)) //
          && (IBOBasePDMRequestDef.STATE_INITIAL.equals(requestStateName)))
        {
          String displayValueRequestState = aPnContext.getBODefinition(BOTypePDMP5Module.BASE_PDM_REQUEST).getStateControllerDef().getSystemStateByPnName(requestStateName).getLocalizableKey();
          errorCreate(ErrorNumberPDMP5Module.EC_TASK_STATE_IS_NOT_ALLOWED, state.getLocalizableKey(), displayValueRequestState, aPnContext);
        }
      }
    }
  }


  /**
   * throw an error
   *
   * @param  aMessageType  error message
   * @param  aTaskState    display value of task state
   * @param  aRequestState display value of request state
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  private void errorCreate(MessageType aMessageType, String aTaskState, String aRequestState, PnContext aPnContext) throws PnErrorListException
  {
    PnMessage msg = aPnContext.getPnErrorManager().createMessage(aMessageType);
    msg.addLocalizebleKeyArgument(aTaskState);
    msg.addLocalizebleKeyArgument(aRequestState);
    aPnContext.getPnErrorManager().addFatal(msg);
  }
}
