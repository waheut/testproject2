// Planon Enterprise Edition Source file: VRPDMLocationEqualToPDMLocationDBServer.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * Validation rule to check if the PDM location filled in matches the PDM location of the linked
 * database server
 */
public class VRPDMLocationEqualToPDMLocationDBServer extends ValidationRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * The filled-in PDM Location must match the PDM location of the filled-in database server.
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    ReferenceField pdmLocationRefFieldDatabase = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_PDMLOCATION_REF);
    ReferenceField pdmDatabaseServerRefField = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    if (!(pdmDatabaseServerRefField.isEmpty() || pdmLocationRefFieldDatabase.isEmpty()))
    {
      BusinessObject databaseServer = pdmDatabaseServerRefField.getReferenceBO();
      ReferenceField pdmLocationRefFieldDBServer = databaseServer.getReferenceFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF);

      if (!pdmLocationRefFieldDatabase.getBaseValue().equals(pdmLocationRefFieldDBServer.getBaseValue()))
      {
        PnErrorManager manager = aPnContext.getPnErrorManager();
        PnMessage message = manager.createMessage(ErrorNumberPDMP5Module.EC_LOCATION_DOES_NOT_MATCH_LOCATION_DBSERVER);
        message.addStringArgument(pdmLocationRefFieldDatabase.getLocalizableDisplayValue());
        message.addStringArgument(pdmLocationRefFieldDBServer.getLocalizableDisplayValue());
        message.addStringArgument(pdmDatabaseServerRefField.getLocalizableDisplayValue());
        message.addFieldNameArgument(pdmDatabaseServerRefField);

        manager.addFatal(message);
      }
    }
  }
}
