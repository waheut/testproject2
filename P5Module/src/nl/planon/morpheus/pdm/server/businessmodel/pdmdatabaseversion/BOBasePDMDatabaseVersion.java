// Planon Enterprise Edition Source file: BOBasePDMDatabaseVersion.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.pdm.core.common.*;

/**
 * BOBasePDMDatabaseVersion
 */
public class BOBasePDMDatabaseVersion extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOBasePDMDatabaseVersion object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOBasePDMDatabaseVersion(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get pdm database type of db version
   *
   * @return pdm database type
   */
  public PDMDatabaseType getPDMDatabaseType()
  {
    return null;
  }
}
