// Planon Enterprise Edition Source file: PopupPDMDBVersionMSSQLViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

/**
 * PDMDBVersionMSSQLViewDefinition
 */
public class PopupPDMDBVersionMSSQLViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDBVersionMSSQLViewDefinition object.
   */
  public PopupPDMDBVersionMSSQLViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    IBaseValue pkBODefPDMDbVersion = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL).getPrimaryKeyInDB();
    addWhereDef(BOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL, pkBODefPDMDbVersion);
  }
}
