// Planon Enterprise Edition Source file: BOPDMDatabaseServerMSSQL.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMDatabaseServerMSSQL
 */
public class BOPDMDatabaseServerMSSQL extends BOBasePDMDatabaseServer
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String PREFIX_CONNECTION_URL = "jdbc:sqlserver://";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServerMSSQL object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMDatabaseServerMSSQL(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets connection URL
   *
   * @return String
   *
   * @throws PnErrorListException
   */
  @Override public String getConnectionURL() throws PnErrorListException
  {
    String hostName = getFieldByPnName(BOPDMDatabaseServerMSSQLDef.PN_HOST_NAME).getAsString();
    String dbInstanceName = getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASE_INSTANCENAME).getAsString();

    StringBuilder builder = new StringBuilder();
    builder.append(PREFIX_CONNECTION_URL).append(hostName);
    if (dbInstanceName != null)
    {
      builder.append("\\").append(dbInstanceName);
    }

    return builder.toString();
  }


  /**
   * PDM Database Type of databaseserver
   *
   * @return PDM Database Type MSSQL
   */
  @Override public PDMDatabaseType getPDMDatabaseType()
  {
    return PDMDatabaseType.MSSQL;
  }


  /**
   * This method sets the default field values. For PDM Database servers of type MSSQL the file type
   * must be initialized to MSSQL_DUMP.
   *
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected final void doSetDefaultValueInFields(PnContext aPnContext) throws PnErrorListException
  {
    getFieldByPnName(BOBasePDMDatabaseServerDef.PN_FILE_TYPE_REF).initAsBaseValue(BOPDMFileTypeDef.getPKValue(PDMFileType.MSSQL_DUMP.getCode(), aPnContext));
  }
}
