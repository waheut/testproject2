// Planon Enterprise Edition Source file: PDMDbServerPDMDbVersionMSSQLMtoNViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseservermssql.proxyview;

import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDbServerPDMDbVersionMSSQLMtoNViewDefinition
 */
public class PDMDbServerPDMDbVersionMSSQLMtoNViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDbServerPDMDbVersionMSSQLMtoNViewDefinition object.
   */
  public PDMDbServerPDMDbVersionMSSQLMtoNViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL);
  }
}
