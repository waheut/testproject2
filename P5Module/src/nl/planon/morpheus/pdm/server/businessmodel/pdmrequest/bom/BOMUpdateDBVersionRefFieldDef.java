// Planon Enterprise Edition Source file: BOMUpdateDBVersionRefFieldDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.bom;

import nl.planon.aphrodite.businessobjectsmethod.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.businessobjectsmethod.bomfield.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * BOM to update the (read-only) field database version reference of the request
 */
public class BOMUpdateDBVersionRefFieldDef extends BOMDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String ARG_BOM_DBVERSION = IBOBasePDMRequestDef.PN_DATABASEVERSION_REF;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMUpdateDBVersionRefFieldDef object.
   *
   * @param aBODef
   * @param aPnBOMName
   */
  public BOMUpdateDBVersionRefFieldDef(BODefinition aBODef, String aPnBOMName)
  {
    super(aBODef, aPnBOMName, BOMTypeAphrodite.GENERIC);
    setSystem(true);
  }

  //~ Methods --------------------------------------------------------------------------------------

  // /**
  // * {@inheritDoc}
  // */
  // @Override protected BOM doCreateBOM(BusinessObject aBO, PnContext
  // aPnContext)
  // {
  // return new BOMUpdateDBVersionRefField();
  // }

  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateBOMFieldDefs()
  {
    ReferenceFieldDefinition pdmDBVersionFieldDefinition = getBODefinition(BOTypePDMP5Module.BASE_PDM_REQUEST).getReferenceFieldDefByPnName(IBOBasePDMRequestDef.PN_DATABASEVERSION_REF);
    addBOMFieldDef(new BOMBOReferenceFieldDef(this, ARG_BOM_DBVERSION, pdmDBVersionFieldDefinition));
  }


  /**
   * BOM to update the read-only field database version of bo pdm request
   *
   * @param  aBOM
   * @param  aPnContext
   *
   * @return updated request bo
   *
   * @throws PnErrorListException
   */
  @Override protected BusinessObject execute(BOM aBOM, PnContext aPnContext) throws PnErrorListException
  {
    BOBasePDMRequest boPDMRequest = (BOBasePDMRequest) convertBOValueToBO(aBOM.getBOValue(), aPnContext);

    boPDMRequest.getFieldByPnName(IBOBasePDMRequestDef.PN_DATABASEVERSION_REF).setAsBaseValue(aBOM.getFieldByPnName(BOMUpdateDBVersionRefFieldDef.ARG_BOM_DBVERSION).getBaseValue());
    boPDMRequest = (BOBasePDMRequest) boPDMRequest.saveAndRead(aPnContext);

    return boPDMRequest;
  }
}
