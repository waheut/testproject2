// Planon Enterprise Edition Source file: BOPDMTaskImport.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.pdmtaskimport;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

/**
 * BOPDMTaskImport
 */
public class BOPDMTaskImport extends BOBasePDMTask
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMTaskImport object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  protected BOPDMTaskImport(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
