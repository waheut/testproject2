// Planon Enterprise Edition Source file: BOBasePDMRequestDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.common.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;
import nl.planon.hades.statemachine.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.bom.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.rule.validation.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequestexecutestate.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;

import nl.planon.pdm.core.common.*;

/**
 * The definition for the business object Base PDMRequest.
 */
public class BOBasePDMRequestDef extends BODefinition implements IBOBasePDMRequestDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // System state id's
  private static final String ID_INITIAL = "I";
  private static final String ID_IN_PROGRESS = "IP";
  private static final String ID_OK = "OK";
  private static final String ID_NOK = "NOK";

  // Database fields
  private static final String DB_DATABASE_REF = "FK_PDMDATABASE";
  private static final String DB_ACCOUNT_REQUESTOR_REF = "FK_ACCOUNT_REQUESTOR";
  private static final String DB_DATABASEVERSION_REF = "FK_PDMDATABASEVERSION";
  private static final String DB_REQUESTTYPE_REF = "FK_PLC_PDMREQUESTTYPE";
  private static final String DB_FK_BOSTATE = DbFieldName.FK_BOSTATE;

  private static final String TBL_NAME = "PLN_PDMREQUEST_QUEUE";

  public static final String DISPLAY_FORMAT = getDisplayFormat(PN_REQUESTTYPE_REF, PN_SYSINSERTDATETIME);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOBasePDMRequestDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType, TBL_NAME);
    setDefaultReferenceDisplayValue(DISPLAY_FORMAT);
    setConfigurable();
    setEventLogAware();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void doCreateBaseType()
  {
    setSystemSubTypeFieldDef(PN_REQUESTTYPE_REF);
  }


  /**
   * @see BOPDMRequestTypeCodeDef#getPDMRequestTypePrimaryKeyByCode
   */
  public IBaseValue getPDMRequestTypePrimaryKeyByCode(PnContext aPnContext, PDMRequestType aPDMRequestType, boolean aAllowNull) throws PnErrorListException
  {
    IBaseValue result = BOPDMRequestTypeDef.getPDMRequestTypePrimaryKeyByCode(aPnContext, aPDMRequestType.getCode());
    assert aAllowNull || (result != null) : "requesttype primarykey not found for code: " + aPDMRequestType.getCode();
    return result;
  }


  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.BASE_PDM_REQUEST;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createBOMDefs(PnContext aPnContext) throws PnErrorListException
  {
    addBOMDef(new BOMReadDef(this, PN_BOM_READ));
    addBOMDef(new BOMSaveDef(this, PN_BOM_SAVE));

    // system BOM's
    addBOMDef(new BOMUpdateDBVersionRefFieldDef(this, PN_BOM_UPDATE_DBVERSION_FIELD));
    addBOMDef(new BOMLogPDMRequestDef(this, PN_BOM_LOG_PDMREQUEST));
    addBOMDef(new BOMLogPDMRequestToDbServer(this, PN_BOM_LOG_PDMDBSERVER));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext, IBOType aBOType, boolean aIsSystemDefinition) throws PnErrorListException
  {
    FieldDefinition fieldDef;

    fieldDef = new SysChangeDateTimeFieldDefinition(this);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysInsertDateTimeFieldDefinition(this);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysChangeAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    fieldDef.setPlanonStandardCopy(false);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new SysInsertAccountReferenceFieldDefinition(this);
    fieldDef.setAndFreezePlanonReadOnly(true);
    fieldDef.setPlanonStandardCopy(false);
    addFieldDef(fieldDef);

    /* ---------------------------------------------------------------------- */
    ReferenceFieldDefinition refFieldDef = new PDMDatabaseReferenceFieldDefinition(this, PN_DATABASE_REF, DB_DATABASE_REF);
    refFieldDef.setAssociationActionType(AssociationActionType.DELETE);
    refFieldDef.setAndFreezePlanonReadOnly(true);
    refFieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(refFieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new AccountReferenceFieldDefinition(this, PN_ACCOUNT_REQUESTOR_REF, DB_ACCOUNT_REQUESTOR_REF);
    fieldDef.setAndFreezePlanonReadOnly(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new BasePDMDatabaseVersionReferenceFieldDefinition(this, PN_DATABASEVERSION_REF, DB_DATABASEVERSION_REF);
    fieldDef.setAndFreezePlanonReadOnly(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new PDMRequestTypeReferenceFieldDefinition(this, PN_REQUESTTYPE_REF, DB_REQUESTTYPE_REF);
    fieldDef.setAndFreezePlanonMandatory(true);
    addFieldDef(fieldDef);
    /* ---------------------------------------------------------------------- */
    fieldDef = new PDMRequestExecuteStateReferenceFieldDefinition(this, PN_REF_BOSTATE, DB_FK_BOSTATE);
    fieldDef.setAndFreezePlanonMandatory(true);
    fieldDef.setAndFreezePlanonReadOnly(true);
    addFieldDef(fieldDef);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOBasePDMRequest(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected final void doCreateRules()
  {
    doCreateActionRules();
    doCreateValidationRules();
    doCreateWarningRules();
    doCreateStateChangeRules();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateSystemStates()
  {
    StateControllerDef stateControllerDef = new StateControllerDef(this);
    this.setStateControllerDef(stateControllerDef);
    stateControllerDef.setSystemStateFieldDef(getFieldDefByPnName(IBOBasePDMRequestDef.PN_REF_BOSTATE));

    // States definition
    final SystemState initialState = stateControllerDef.addSystemState(ID_INITIAL, STATE_INITIAL);
    final SystemState inProgressState = stateControllerDef.addSystemState(ID_IN_PROGRESS, STATE_IN_PROGRESS);
    final SystemState okState = stateControllerDef.addSystemState(ID_OK, STATE_OK);
    final SystemState nokState = stateControllerDef.addSystemState(ID_NOK, STATE_NOK);

    // state changes
    stateControllerDef.addSystemStateChange(initialState, inProgressState);
    stateControllerDef.addSystemStateChange(inProgressState, okState);
    stateControllerDef.addSystemStateChange(inProgressState, nokState);

    // Set (default) initial system state
    stateControllerDef.addInitialSystemState(initialState);
    stateControllerDef.setDefaultInitialSystemState(initialState);
  }


  /**
   * validations rules for Sub BOTypes
   */
  protected void doCreateValidationRulesOfSub()
  {
    // override this in subs
  }


  /**
   * create action rules that apply to pdm requests
   */
  private void doCreateActionRules()
  {
  }


  /**
   * specify which state change rules do apply
   */
  private void doCreateStateChangeRules()
  {
  }


  /**
   * create validation rules that apply to pdm requests
   */
  private void doCreateValidationRules()
  {
    VRRequestToTaskState vrRequestToTaskState = new VRRequestToTaskState();
    addRuleValidateBeforeDBUpdate(vrRequestToTaskState);

    doCreateValidationRulesOfSub();
  }


  /**
   * create warning rules that apply to pdm requests
   */
  private void doCreateWarningRules()
  {
  }
}
