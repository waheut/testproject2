// Planon Enterprise Edition Source file: FCClearDbServerOfDifferentDbType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.fieldchange;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

import nl.planon.pdm.core.common.*;

/**
 * on change of field database type the field database server needs to be cleared if it is linked to
 * a database version of a different database type
 */
public class FCClearDbServerOfDifferentDbType extends FieldChangeAction<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * If field DatabaseTypeRef changes the field DatabaseServerRef is cleared if it is linked to a
   * DatabaseVersion that has a different DatabaseType
   *
   * @param  aFCContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void execute(FieldChangeActionContext<BOPDMDatabase> aFCContext, PnContext aPnContext) throws PnErrorListException
  {
    BOPDMDatabase boPDMDatabase = aFCContext.getBusinessObject();

    ReferenceField pdmDatabaseServerRefField = boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    if (!pdmDatabaseServerRefField.isEmpty())
    {
      PDMDatabaseType serverDatabaseType = boPDMDatabase.getDatabaseTypeOfServer(aPnContext);
      PDMDatabaseType databaseType = boPDMDatabase.getPDMDatabaseType();
      // PCIS 193288: if field database type is cleared, there is no need to clear
      // database server reference field
      if ((databaseType != null) && (serverDatabaseType != null) && (!databaseType.equals(serverDatabaseType)))
      {
        pdmDatabaseServerRefField.clear();
      }
    }
  }
}
