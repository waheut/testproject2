// Planon Enterprise Edition Source file: PDMPrivilegeTypeReferenceFieldDefinition.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMPrivilegeTypeReferenceFieldDefinition
 */
public class PDMPrivilegeTypeReferenceFieldDefinition extends SystemCodeReferenceFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMPrivilegeTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aBOType
   */
  public PDMPrivilegeTypeReferenceFieldDefinition(final BODefinition aBODefinition, final IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMPrivilegeTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   */
  public PDMPrivilegeTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName)
  {
    super(aBODefinition, aPnName, BOTypePDMP5Module.PDM_DATABASE_PRIVILEGE_TYPE, FieldTypePDMP5Module.PDM_DATABASE_PRIVILEGE_TYPE);
  }


  /**
   * Creates a new PDMPrivilegeTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   * @param aDBName
   */
  public PDMPrivilegeTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName, final String aDBName)
  {
    this(aBODefinition, aPnName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Create a PDMPrivilegeTypeReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return SystemCodeReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMPrivilegeTypeReferenceField(aBO, this);
  }
}
