// Planon Enterprise Edition Source file: VRReadOnlyFieldsForNonInitialDatabase.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * VRReadOnlyFieldsForNonInitialDatabase
 */
public class VRReadOnlyFieldsForNonInitialDatabase extends ValidationRule<BOPDMDatabase>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType MESSAGE_TYPE = ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_NON_INITIAL_DATABASE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * If the state of the database is beyond state Initial, it is not possible to change the fields
   * that hold specific settings for the database (like DatabaseServerRef, DatabaseTypeRef,
   * FileTypeRef, ObjectOwnerInDump, AccessCode, PDMLocationRef, TableSpaceNameInDump, Code).
   *
   * @param  aBO        BOPDMDatabaseServer
   * @param  aOldBO     BOPDMDatabaseServer
   * @param  aPnContext aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    boolean databaseInInitialState = aBO.getCurrentState().getPnName().equals(IBOPDMDatabaseDef.STATE_INITIAL);

    if (!databaseInInitialState)
    {
      String[] databaseSettingFieldNames = BOPDMDatabaseDef.getDatabaseSettingFieldNames();
      for (String fieldName : databaseSettingFieldNames)
      {
        Field field = aBO.getFieldByPnName(fieldName);
        if (field.isChanged())
        {
          PnMessage message = aPnContext.getPnErrorManager().createMessage(MESSAGE_TYPE);
          aPnContext.getPnErrorManager().add(message);
        }
      }
    }
  }
}
