// Planon Enterprise Edition Source file: PDMFileTypeReferenceField.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.pdm.core.common.*;

/**
 * PDMFileTypeReferenceField
 */
public class PDMFileTypeReferenceField extends SystemCodeReferenceField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMFileTypeReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMFileTypeReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public String getCodeValue() throws PnErrorListException
  {
    return (String) super.getCodeValue();
  }


  /**
   * get PDM Database Type that corresponds to the filetype
   *
   * @return PDM Database Type
   *
   * @throws PnErrorListException
   */
  public PDMDatabaseType getPDMDatabaseType() throws PnErrorListException
  {
    PDMDatabaseType result = null;
    PDMFileType pdmFileType = getPDMFileType();
    if (pdmFileType != null)
    {
      if (pdmFileType.equals(PDMFileType.MSSQL_DUMP))
      {
        result = PDMDatabaseType.MSSQL;
      }
      else if ((pdmFileType.equals(PDMFileType.DATAPUMP)) || (pdmFileType.equals(PDMFileType.ORA_EXP)))
      {
        result = PDMDatabaseType.ORACLE;
      }
    }
    return result;
  }


  /**
   * get PDM File Type according to code value
   *
   * @return PDM File Type
   *
   * @throws PnErrorListException
   */
  public PDMFileType getPDMFileType() throws PnErrorListException
  {
    return PDMFileType.getPDMFileTypeByCode(getCodeValue());
  }


  /**
   * sets specified type
   *
   * @param  aType type
   *
   * @throws PnErrorListException
   */
  public void setPDMFileType(PDMFileType aType) throws PnErrorListException
  {
    setCodeValue(aType.getCode());
  }
}
