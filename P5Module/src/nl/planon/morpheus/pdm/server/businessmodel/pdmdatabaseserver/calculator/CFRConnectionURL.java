// Planon Enterprise Edition Source file: CFRConnectionURL.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.calculator;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.calculation.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * calculates content of connection url
 */
public class CFRConnectionURL extends BaseFieldValueCalculator<BOBasePDMDatabaseServer>
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CFRConnectionURL object.
   *
   * @param aFieldDefinition
   */
  public CFRConnectionURL(FieldDefinition aFieldDefinition)
  {
    super(aFieldDefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * calculate connection URL
   *
   * @param  aBO
   * @param  aPnContext
   *
   * @return string containing connection url
   *
   * @throws PnErrorListException
   */
  @Override protected IBaseValue calculateFieldValue(BOBasePDMDatabaseServer aBO, PnContext aPnContext) throws PnErrorListException
  {
    return new BaseStringValue(aBO.getConnectionURL());
  }
}
