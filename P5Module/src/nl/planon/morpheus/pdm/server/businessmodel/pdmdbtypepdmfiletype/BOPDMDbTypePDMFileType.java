// Planon Enterprise Edition Source file: BOPDMDbTypePDMFileType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdbtypepdmfiletype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

/**
 * BOPDMDbTypePDMFileType
 */
public class BOPDMDbTypePDMFileType extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDbTypePDMFileType object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMDbTypePDMFileType(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
