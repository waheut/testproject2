// Planon Enterprise Edition Source file: BRDatabaseExported.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.bom;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * BRDatabaseExported
 */
public class BRDatabaseExported extends BOMRule<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * The BOMs 'Download dumpfile' is only available when:<br>
   * - The database is exported<br>
   *
   * @param  aBO
   * @param  aPnContext
   *
   * @return true if the BOM should be visible, false when the BOM should be invisible
   *
   * @throws PnErrorListException
   */
  @Override protected boolean execute(BOPDMDatabase aBO, PnContext aPnContext) throws PnErrorListException
  {
    return (aBO.isExported() && !aBO.isBusy());
  }
}
