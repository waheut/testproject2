// Planon Enterprise Edition Source file: BOBasePDMDatabaseServer.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver;

import java.util.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

import nl.planon.pdm.core.common.*;

/**
 * base class for e.g. mssql/Oracle PDMDatabaseServer
 */
public abstract class BOBasePDMDatabaseServer extends BusinessObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseServer object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOBasePDMDatabaseServer(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets connection URL
   *
   * @return String
   *
   * @throws PnErrorListException
   */
  public abstract String getConnectionURL() throws PnErrorListException;


  /**
   * Gets the all pdm databases linked to this pdm database server
   *
   * @param  aPnContext
   *
   * @return List with all linked pdm databases.
   *
   * @throws PnErrorListException
   */
  public List<IProxyValue> getLinkedPDMDatabases(PnContext aPnContext) throws PnErrorListException
  {
    PVRuntimeDefinition pvPDMDatabasesByPDMDatabaseServer = new PVRuntimeDefinition(BOTypePDMP5Module.PDM_DATABASE);
    PVJoinDef masterJoin = pvPDMDatabasesByPDMDatabaseServer.getMasterJoinDef();
    masterJoin.addSelectFieldDef(BOPDMDatabaseDef.PN_PDMLOCATION_REF);
    masterJoin.addWhereDef(BOPDMDatabaseDef.PN_DATABASESERVER_REF, SCOperatorType.EQUAL, getPrimaryKeyValue());

    IProxyListValue plvPDMDatabases = aPnContext.createByPVDefinition(pvPDMDatabasesByPDMDatabaseServer);
    return plvPDMDatabases.getProxyValues();
  }


  /**
   * PDM Database Type of databaseserver
   *
   * @return PDM Database Type of database server
   */
  public PDMDatabaseType getPDMDatabaseType()
  {
    return null;
  }


  /**
   * {@inheritDoc}
   *
   * <p>Disables several fields that hold specific settings for the database server when the
   * database server is active (IsActive field is true).</p>
   */
  @Override protected void doCreateBOValue(BOValue aBOValue, PnContext aPnContext) throws PnErrorListException
  {
    if (getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE).getAsBoolean().equals(Boolean.TRUE))
    {
      for (String pnName : ((BOBasePDMDatabaseServerDef) getBODefinition()).getDatabaseServerSettingFieldNames())
      {
        aBOValue.disableFieldByPnName(pnName);
      }
    }
  }
}
