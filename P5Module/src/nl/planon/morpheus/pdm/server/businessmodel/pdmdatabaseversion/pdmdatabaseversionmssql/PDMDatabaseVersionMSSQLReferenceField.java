// Planon Enterprise Edition Source file: PDMDatabaseVersionMSSQLReferenceField.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

/**
 * PDMDatabaseVersionMSSQLReferenceField
 */
public class PDMDatabaseVersionMSSQLReferenceField extends ReferenceIntegerField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseVersionMSSQLReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMDatabaseVersionMSSQLReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }
}
