// Planon Enterprise Edition Source file: PDMDatabaseServerOracleReferenceField.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

/**
 * PDMDatabaseServerOracleReferenceField
 */
public class PDMDatabaseServerOracleReferenceField extends ReferenceIntegerField
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseServerOracleReferenceField object.
   *
   * @param aBO
   * @param aFD
   */
  public PDMDatabaseServerOracleReferenceField(BusinessObject aBO, FieldDefinition aFD)
  {
    super(aBO, aFD);
  }
}
