// Planon Enterprise Edition Source file: PopupDBServerByDBTypeViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PopupDBServerByDBTypeViewDefinition
 */
public class PopupDBServerByDBTypeViewDefinition extends PVDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef databaseVersionJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PopupDBServerByDBTypeViewDefinition object.
   */
  public PopupDBServerByDBTypeViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.databaseVersionJoin = addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(new PVExpressionNodeDef(this.databaseVersionJoin, IBOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected final void doBeforeCreateSQLStatement(ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateSQLStatement(aProxyListContext, aPnContext);

    BusinessObject contextBO = aProxyListContext.getBO();
    if (contextBO != null)
    {
      String fieldName = IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF;
      if (contextBO.hasFieldByPnName(fieldName))
      {
        Field field = contextBO.getFieldByPnName(fieldName);
        if (!field.getBaseValue().isEmpty())
        {
          SearchCriteria searchCriteria = getPermanentSearchCriteria(aPnContext);

          ISCOperator operator = searchCriteria.getOperator(IBOBasePDMDatabaseVersionDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL);
          operator.addBaseValue(field.getBaseValue());
          getSearchCriteria().addSearchCriteriaWithAndNode(searchCriteria);
        }
      }
    }
  }
}
