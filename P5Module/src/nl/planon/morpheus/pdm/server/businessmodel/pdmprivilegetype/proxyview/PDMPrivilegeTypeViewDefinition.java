// Planon Enterprise Edition Source file: PDMPrivilegeTypeViewDefinition.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmprivilegetype.*;

/**
 * PDMPrivilegeTypeViewDefinition
 */
public class PDMPrivilegeTypeViewDefinition extends SystemCodeViewDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  IBaseValue[] includeValues;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMPrivilegeTypeViewDefinition object.
   */
  public PDMPrivilegeTypeViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_PRIVILEGE_TYPE);
  }


  /**
   * Creates a new PDMPrivilegeTypeViewDefinition object.
   *
   * @param aIncludeValues values that will be included in the grouping list
   */
  public PDMPrivilegeTypeViewDefinition(IBaseValue... aIncludeValues)
  {
    super(BOTypePDMP5Module.PDM_DATABASE_PRIVILEGE_TYPE);

    this.includeValues = aIncludeValues;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createWhereDefs(aPnContext);

    if (this.includeValues != null)
    {
      addWhereDef(BOPDMPrivilegeTypeDef.PN_CODE, SCOperatorType.IN, this.includeValues);
    }
  }
}
