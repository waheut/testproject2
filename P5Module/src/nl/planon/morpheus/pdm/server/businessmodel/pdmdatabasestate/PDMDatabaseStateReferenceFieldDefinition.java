// Planon Enterprise Edition Source file: PDMDatabaseStateReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasestate;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.state.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseStateReferenceFieldDefinition
 */
public class PDMDatabaseStateReferenceFieldDefinition extends BaseBOStateReferenceFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aBOType
   */
  public PDMDatabaseStateReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMDatabaseStateReferenceFieldDefinition object.
   *
   * @param aBODefinition BusinessObject definition.
   * @param aPnName       Planon name of field.
   */
  public PDMDatabaseStateReferenceFieldDefinition(BODefinition aBODefinition, String aPnName)
  {
    super(aBODefinition, aPnName, BOTypePDMP5Module.PDM_DATABASE_STATE, FieldTypePDMP5Module.PDM_DATABASE_STATE);
  }


  /**
   * Creates a new PDMDatabaseStateReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   * @param aDBName
   */
  public PDMDatabaseStateReferenceFieldDefinition(BODefinition aBODefinition, String aPnName, String aDBName)
  {
    this(aBODefinition, aPnName);
    setDB(aDBName);
  }
}
