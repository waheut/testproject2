// Planon Enterprise Edition Source file: ARCreateNextPDMTask.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmtask.rule.action;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.*;

import nl.planon.pdm.core.common.*;

/**
 * ARCreateNextPDMTask
 */
public class ARCreateNextPDMTask extends ActionRule<BOBasePDMTask>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * A request to reload an initial dump or an exported dump file consists of two tasks:
   *
   * <ul>
   *   <li>- delete the original database</li>
   *   <li>- import the initial or exported dump</li>
   * </ul>
   *
   * <p>In case the status of a pdm delete task is set to ok and the linked request is of type
   * reload initial or reload export, then a new pdm task must be created to import the dumpfile.
   * </p>
   *
   * @param  aBO        BOBasePDMTask
   * @param  aOldBO     BOBasePDMTask
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMTask aBO, BOBasePDMTask aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    if (PDMTaskType.DELETE.getCode().equals(aBO.getSystemSubTypeField().getAsString()))
    {
      BusinessObject pdmRequest = aBO.getReferenceFieldByPnName(IBOBasePDMTaskDef.PN_PDMREQUEST_REF).getReferenceBO();
      IBOType boTypePDMRequest = pdmRequest.getBOType();
      if ((BOTypePDMP5Module.PDM_REQUEST_RELOAD_INITIAL.equals(boTypePDMRequest)) //
        || (BOTypePDMP5Module.PDM_REQUEST_RELOAD_EXPORT.equals(boTypePDMRequest)))
      {
        BOBasePDMTask boPDMTask = (BOBasePDMTask) aPnContext.createBO(BOTypePDMP5Module.PDM_TASK_IMPORT);
        boPDMTask.getReferenceFieldByPnName(IBOBasePDMTaskDef.PN_PDMREQUEST_REF).setAsBaseValue(pdmRequest.getPrimaryKeyValue());
        boPDMTask.saveAndRead(aPnContext);
      }
    }
  }
}
