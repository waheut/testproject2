// Planon Enterprise Edition Source file: BOPDMDatabaseVersionOracleDef.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

/**
 * BOPDMDatabaseVersionOracleDef
 */
public class BOPDMDatabaseVersionOracleDef extends BOBasePDMDatabaseVersionDef
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMDatabaseVersionOracleDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDatabaseVersionOracleDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
    setParentBOType(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
    setConfigurable();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public FieldType getReferenceFieldType()
  {
    return FieldTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDatabaseVersionOracle(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNInContextPVType()
  {
    return PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_ORACLE_IN_CONTEXT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected IPVType getMtoNPVType()
  {
    return PVTypePDMP5Module.PDMDBSERVER_MTON_PDMDBVERSION_ORACLE;
  }
}
