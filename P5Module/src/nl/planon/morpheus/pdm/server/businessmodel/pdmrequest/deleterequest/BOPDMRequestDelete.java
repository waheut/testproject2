// Planon Enterprise Edition Source file: BOPDMRequestDelete.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.deleterequest;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.*;

/**
 * BOPDMRequestDelete
 */
public class BOPDMRequestDelete extends BOBasePDMRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMRequestDelete object.
   *
   * @param  aBODefinition
   *
   * @throws PnErrorListException
   */
  public BOPDMRequestDelete(BODefinition aBODefinition) throws PnErrorListException
  {
    super(aBODefinition);
  }
}
