// Planon Enterprise Edition Source file: BasePDMDatabaseVersionReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseVersionReferenceFieldDefinition
 */
public class BasePDMDatabaseVersionReferenceFieldDefinition extends ReferenceIntegerFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseVersionReferenceFieldDefinition object.
   *
   * <p>Default constructor to retrieve an instance of the field definition. Use
   * FieldTypeManager.constructFieldDefinition to construct a field definition</p>
   *
   * @param aBODefinition BusinessObject definition
   * @param aBOType       BOType
   */
  public BasePDMDatabaseVersionReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMDatabaseVersionReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   */
  public BasePDMDatabaseVersionReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName)
  {
    super(aBODefinition, aPlanonName, BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
    setPlanonFieldType(FieldTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
  }


  /**
   * Creates a new PDMDatabaseVersionReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   * @param aDBName       the name of the database field.
   */
  public BasePDMDatabaseVersionReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName, String aDBName)
  {
    this(aBODefinition, aPlanonName);
    setDB(aDBName);
  }
}
