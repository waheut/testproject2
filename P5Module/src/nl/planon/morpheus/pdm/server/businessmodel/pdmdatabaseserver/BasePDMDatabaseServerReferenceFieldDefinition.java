// Planon Enterprise Edition Source file: BasePDMDatabaseServerReferenceFieldDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseServerReferenceFieldDefinition
 */
public class BasePDMDatabaseServerReferenceFieldDefinition extends ReferenceIntegerFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BasePDMDatabaseServerReferenceFieldDefinition object.
   *
   * @param aFreeFieldConversionInfo
   */
  public BasePDMDatabaseServerReferenceFieldDefinition(FreeFieldConversionInfo aFreeFieldConversionInfo)
  {
    this(aFreeFieldConversionInfo.getBODefinition(), aFreeFieldConversionInfo.getPnName());
    setDB(aFreeFieldConversionInfo.getDBName(), aFreeFieldConversionInfo.getDBType());
  }


  /**
   * Creates a new PDMDatabaseServerReferenceFieldDefinition object.
   *
   * <p>Default constructor to retrieve an instance of the field definition. Use
   * FieldTypeManager.constructFieldDefinition to construct a field definition</p>
   *
   * @param aBODefinition BusinessObject definition
   * @param aBOType       BOType
   */
  public BasePDMDatabaseServerReferenceFieldDefinition(BODefinition aBODefinition, IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMDatabaseServerReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   */
  public BasePDMDatabaseServerReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName)
  {
    super(aBODefinition, aPlanonName, BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
    setPlanonFieldType(FieldTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }


  /**
   * Creates a new PDMDatabaseServerReferenceFieldDefinition object.
   *
   * @param aBODefinition the business object definition.
   * @param aPlanonName   a "unique" name for the field.
   * @param aDBName       the name of the database field.
   */
  public BasePDMDatabaseServerReferenceFieldDefinition(BODefinition aBODefinition, String aPlanonName, String aDBName)
  {
    this(aBODefinition, aPlanonName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public boolean isFreeFieldConversionAllowed()
  {
    return true;
  }
}
