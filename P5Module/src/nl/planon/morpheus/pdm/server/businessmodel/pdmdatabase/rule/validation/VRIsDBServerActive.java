// Planon Enterprise Edition Source file: VRIsDBServerActive.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * the filled in database type must be equal to the database type of the database version that is
 * linked to the filled in database server
 */
public class VRIsDBServerActive extends ValidationRule<BOPDMDatabase>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType ERROR_MESSAGE = ErrorNumberPDMP5Module.EC_IS_DB_SERVER_ACTIVE;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * It is not possible to fill in a database type that is not equal to the database type of the
   * database version that is linked to the filled in database server
   *
   * @param  aBO        BOPDMDatabase
   * @param  aOldBO     BOPDMDatabase
   * @param  aPnContext PnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOPDMDatabase aBO, BOPDMDatabase aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    ReferenceField pdmDatabaseServerRefField = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    if (!pdmDatabaseServerRefField.isEmpty())
    {
      IBaseValue fieldValueOfReferencedBO = pdmDatabaseServerRefField.getFieldValueOfReferencedBO(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE);
      boolean booleanValue = fieldValueOfReferencedBO.getAsBoolean().booleanValue();
      if (!booleanValue)
      {
        PnErrorManager manager = aPnContext.getPnErrorManager();
        PnMessage message = manager.createMessage(ERROR_MESSAGE);
        IBaseValue fieldValueOfReferencedBO2 = aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getFieldValueOfReferencedBO(IBOBasePDMDatabaseServerDef.PN_CODE);
        message.addStringArgument(aBO.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getFieldValueOfReferencedBO(IBOBasePDMDatabaseServerDef.PN_CODE));
        manager.add(message);
      }
    }
  }
}
