// Planon Enterprise Edition Source file: VRReadOnlyFieldsForActiveDbServer.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.rule.validation;

import java.util.*;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * Validation rule to check if fields that hold specific settings for the database server are not
 * changed, in case the database server is active (IsActive = "T").
 */
public class VRReadOnlyFieldsForActiveDbServer extends ValidationRule<BOBasePDMDatabaseServer>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType MESSAGE_TYPE = ErrorNumberPDMP5Module.EC_CANNOT_APPLY_CHANGES_TO_ACTIVE_DBSERVER;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * If the database server is active (IsActive = "T"), it is not possible to change the fields that
   * hold specific settings for the database server (like AdminUser, AdminPassword, HostName,
   * PortNumber, DatabaseInstanceName, PDMLocationRef and TemporaryTableSpaceName).
   *
   * @param  aBO        BOPDMDatabaseServer
   * @param  aOldBO     BOPDMDatabaseServer
   * @param  aPnContext aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMDatabaseServer aBO, BOBasePDMDatabaseServer aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    boolean dbServerIsActive = aBO.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE).getAsBoolean().booleanValue();
    if (dbServerIsActive)
    {
      Set<String> databaseServerSettingFieldNames = ((BOBasePDMDatabaseServerDef) aBO.getBODefinition()).getDatabaseServerSettingFieldNames();
      for (String fieldName : databaseServerSettingFieldNames)
      {
        Field field = aBO.getFieldByPnName(fieldName);
        if (field.isChanged())
        {
          PnMessage message = aPnContext.getPnErrorManager().createMessage(MESSAGE_TYPE);
          aPnContext.getPnErrorManager().add(message);
        }
      }
    }
  }
}
