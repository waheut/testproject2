// Planon Enterprise Edition Source file: VRCannotInactivateDbServerIfBusyDbPresent.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.rule.validation;

import nl.planon.aphrodite.businessobject.rule.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.statemachine.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * Validation rule to prevent setting field IsActive of database server to false while there are pdm
 * databases in state busy linked to it.
 */
public class VRCannotInactivateDbServerIfBusyDbPresent extends ValidationRule<BOBasePDMDatabaseServer>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final MessageType MESSAGE_TYPE = ErrorNumberPDMP5Module.EC_CANNOT_INACTIVATE_DBSERVER_WITH_BUSY_DB;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * If the database server has pdm databases in state busy linked to it, it is not possible to set
   * the field IsActive for this database server to false.
   *
   * @param  aBO        BOPDMDatabaseServer
   * @param  aOldBO     BOPDMDatabaseServer
   * @param  aPnContext aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void doExecute(BOBasePDMDatabaseServer aBO, BOBasePDMDatabaseServer aOldBO, PnContext aPnContext) throws PnErrorListException
  {
    Field isActive = aBO.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE);
    Field pdmLocation = aBO.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF);

    if ((pdmLocation.isChanged()) || (isActive.isChanged() && isActive.getAsBoolean().equals(Boolean.FALSE)))
    {
      if (pdmDatabasesInStatusBusyPresent(aBO.getPrimaryKeyValue(), aPnContext))
      {
        PnMessage message = aPnContext.getPnErrorManager().createMessage(MESSAGE_TYPE);
        aPnContext.getPnErrorManager().add(message);
      }
    }
  }


  /**
   * check if there are pdm databases in state busy linked to current database server
   *
   * @param  aPrimaryKeyValue primary key value of current database server
   * @param  aPnContext
   *
   * @return true if there are pdm databases in state busy present, false otherwise
   *
   * @throws PnErrorListException
   */
  private boolean pdmDatabasesInStatusBusyPresent(IBaseValue aPrimaryKeyValue, PnContext aPnContext) throws PnErrorListException
  {
    BODefinition pdmDatabase = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE);
    SystemState stateBusy = pdmDatabase.getStateControllerDef().getSystemStateByPnName(IBOPDMDatabaseDef.STATE_BUSY);

    PVRuntimeDefinition pvPDMDatabases = new PVRuntimeDefinition(BOTypePDMP5Module.PDM_DATABASE);

    pvPDMDatabases.addEqualWhereDef(IBOPDMDatabaseDef.PN_DATABASESERVER_REF, aPrimaryKeyValue);
    pvPDMDatabases.addWhereDef(IBOPDMDatabaseDef.PN_REF_BOSTATE, SCOperatorType.EQUAL, stateBusy.getPrimaryKeyInDB(aPnContext));

    ProxyListValue plvPDMDatabases = aPnContext.createByPVDefinition(pvPDMDatabases);

    // result set not empty means there are pdm databases in state busy linked
    return !plvPDMDatabases.isEmpty();
  }
}
