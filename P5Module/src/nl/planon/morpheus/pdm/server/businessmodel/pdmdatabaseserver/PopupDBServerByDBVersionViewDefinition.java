// Planon Enterprise Edition Source file: PopupDBServerByDBVersionViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.definition.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionmssql.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.pdmdatabaseversionoracle.*;

/**
 * PopupDBServerByDBVersionViewDefinition
 */
public class PopupDBServerByDBVersionViewDefinition extends PVDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PopupDBServerByDBVersionViewDefinition object.
   */
  public PopupDBServerByDBVersionViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /*
   * (non-Javadoc)
   * @see
   * nl.planon.hades.proxyview.PVDefinition#createFieldDefs(nl.planon.hades.
   * beans.PnContext)
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(BOBasePDMDatabaseServerDef.PN_CODE);
    addSelectFieldDef(BOBasePDMDatabaseServerDef.PN_NAME);
  }


  /*
   * (non-Javadoc)
   * @see
   * nl.planon.hades.proxyview.PVBaseDef#createWhereDefs(nl.planon.hades.beans
   * .PnContext)
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    addWhereDef(new PVExpressionNodeDef(getMasterJoinDef(), BOBasePDMDatabaseServerDef.PN_BUSINESSOBJECTDEFINITION_REF));
  }


  /*
   * (non-Javadoc)
   * @see
   * nl.planon.hades.proxyview.PVBaseDef#doBeforeCreateSQLStatement(nl.planon
   * .hades.proxyview.ProxyListContext, nl.planon.hades.beans.PnContext)
   */
  @Override protected void doBeforeCreateSQLStatement(ProxyListContext aProxyListContext, PnContext aPnContext) throws PnErrorListException
  {
    super.doBeforeCreateSQLStatement(aProxyListContext, aPnContext);

    BusinessObject contextBO = aProxyListContext.getBO();
    if (contextBO != null)
    {
      IBaseValue pkBODefinition = null;
      ReferenceField dbVersionField = contextBO.getReferenceFieldByPnName(BOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
      if (!dbVersionField.isEmpty())
      {
        if (dbVersionField instanceof PDMDatabaseVersionOracleReferenceField)
        {
          pkBODefinition = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE).getPrimaryKeyInDB();
        }
        else if (dbVersionField instanceof PDMDatabaseVersionMSSQLReferenceField)
        {
          pkBODefinition = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL).getPrimaryKeyInDB();
        }

        SearchCriteria sc = getPermanentSearchCriteria(aPnContext);
        ISCOperator operator = sc.getOperator(BOBasePDMDatabaseServerDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL);
        operator.addBaseValue(pkBODefinition);
      }
    }
  }
}
