// Planon Enterprise Edition Source file: PDMDbServerPDMDbVersionOracleMtoNInContextViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.pdmdatabaseserveroracle.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;

/**
 * PDMDbServerPDMDbVersionOracleMtoNInContextViewDefinition
 */
public class PDMDbServerPDMDbVersionOracleMtoNInContextViewDefinition extends PDMDbServerPDMDbVersionOracleMtoNViewDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef compatibilityMatrixJoin;

  private PVJoinDef databaseVersionJoin;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createJoinDefs(aPnContext);

    this.compatibilityMatrixJoin = addInnerJoinDef(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX, IBOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF);
    this.databaseVersionJoin = this.compatibilityMatrixJoin.addInnerJoinDef(IBOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.databaseVersionJoin, IBOBasePDMDatabaseVersionDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    IBaseValue pkBODefPDMDbVersion = aPnContext.getBODefinition(BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE).getPrimaryKeyInDB();
    addWhereDef(BOBasePDMDatabaseServerDef.PN_BUSINESSOBJECTDEFINITION_REF, SCOperatorType.EQUAL, pkBODefPDMDbVersion);
  }
}
