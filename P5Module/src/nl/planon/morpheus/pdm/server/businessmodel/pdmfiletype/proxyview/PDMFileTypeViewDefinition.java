// Planon Enterprise Edition Source file: PDMFileTypeViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.proxyview;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

/**
 * PDMFileTypeViewDefinition
 */
public class PDMFileTypeViewDefinition extends SystemCodeViewDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  IBaseValue[] includeValues;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMFileTypeViewDefinition object.
   */
  public PDMFileTypeViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE_FILE_TYPE);
  }


  /**
   * Creates a new PDMFileTypeViewDefinition object.
   *
   * @param aIncludeValues values that will be included in the grouping list
   */
  public PDMFileTypeViewDefinition(IBaseValue... aIncludeValues)
  {
    super(BOTypePDMP5Module.PDM_DATABASE_FILE_TYPE);

    this.includeValues = aIncludeValues;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createWhereDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createWhereDefs(aPnContext);

    if (this.includeValues != null)
    {
      addWhereDef(BOPDMFileTypeDef.PN_CODE, SCOperatorType.IN, this.includeValues);
    }
  }
}
