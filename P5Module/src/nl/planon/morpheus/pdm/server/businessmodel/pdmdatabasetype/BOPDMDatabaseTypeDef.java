// Planon Enterprise Edition Source file: BOPDMDatabaseTypeDef.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.businessmodelcache.*;
import nl.planon.hades.exception.*;

import nl.planon.pdm.core.common.*;

/**
 * BOPDMDatabaseTypeDef
 */
public class BOPDMDatabaseTypeDef extends BOSystemCodeDef
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String SUBTYPE = "PDMDATABASETYPE";

  public static final String DATABASETYPE_MSSQL = PDMDatabaseType.MSSQL.getCode();
  public static final String DATABASETYPE_ORACLE = PDMDatabaseType.ORACLE.getCode();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOPDMFileTypeDef object.
   *
   * @param aCache
   * @param aBOType
   */
  public BOPDMDatabaseTypeDef(IBODefinitionCache aCache, IBOType aBOType)
  {
    super(aCache, aBOType);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Returns the primary key of the given PDM Database Type code.
   *
   * @param  aCode
   * @param  aPnContext
   *
   * @return IBaseValue
   *
   * @throws PnErrorListException
   */
  public static IBaseValue getPKValue(Object aCode, PnContext aPnContext) throws PnErrorListException
  {
    assert aCode != null : "Invalid argument: code cannot be null!";
    return BOSystemCodeDef.getPKValue(aCode, new BaseStringValue(SUBTYPE), aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected BusinessObject doCreateBusinessObject() throws PnErrorListException
  {
    return new BOPDMDatabaseType(this);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doCreateSubType(PnContext aPnContext)
  {
    setParentBOType(BOTypeHades.SYSTEMCODE);
    setSystemSubTypeFieldValue(new BaseStringValue(SUBTYPE));
  }
}
