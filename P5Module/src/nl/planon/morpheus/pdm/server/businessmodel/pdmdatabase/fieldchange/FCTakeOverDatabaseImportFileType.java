// Planon Enterprise Edition Source file: FCTakeOverDatabaseImportFileType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.fieldchange;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.field.*;
import nl.planon.hades.field.definition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.*;

/**
 * FCTakeOverDatabaseImportFileType
 */
public class FCTakeOverDatabaseImportFileType extends FieldChangeAction<BOPDMDatabase>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * if field ImportFiletype filling, ExportFileType get the same
   *
   * @param  aFCContext
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  @Override protected void execute(FieldChangeActionContext<BOPDMDatabase> aFCContext, PnContext aPnContext) throws PnErrorListException
  {
    BOPDMDatabase boPDMDatabase = aFCContext.getBusinessObject();

    ReferenceField pdmDatabaseServerRefField = boPDMDatabase.getReferenceFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);

    if (!pdmDatabaseServerRefField.isEmpty())
    {
      BaseValue pdmFileTypeCode = (BaseValue) boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF).getBaseValue();
      boPDMDatabase.getFieldByPnName(IBOPDMDatabaseDef.PN_EXPORT_FILE_TYPE).setAsBaseValue(pdmFileTypeCode);
    }
  }
}
