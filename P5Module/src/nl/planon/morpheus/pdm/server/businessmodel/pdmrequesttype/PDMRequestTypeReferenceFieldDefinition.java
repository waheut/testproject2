// Planon Enterprise Edition Source file: PDMRequestTypeReferenceFieldDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype;

import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.field.*;

import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMRequestTypeReferenceFieldDefinition
 */
public class PDMRequestTypeReferenceFieldDefinition extends SystemCodeReferenceFieldDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aBOType
   */
  public PDMRequestTypeReferenceFieldDefinition(final BODefinition aBODefinition, final IBOType aBOType)
  {
    this(aBODefinition, DUMMY_PNNAME);
  }


  /**
   * Creates a new PDMRequestTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   */
  public PDMRequestTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName)
  {
    super(aBODefinition, aPnName, BOTypePDMP5Module.PDM_REQUEST_TYPE, FieldTypePDMP5Module.PDM_REQUEST_TYPE);
  }


  /**
   * Creates a new PDMRequestTypeReferenceFieldDefinition object.
   *
   * @param aBODefinition
   * @param aPnName
   * @param aDBName
   */
  public PDMRequestTypeReferenceFieldDefinition(final BODefinition aBODefinition, final String aPnName, final String aDBName)
  {
    this(aBODefinition, aPnName);
    setDB(aDBName);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Create a PDMRequestTypeReferenceField
   *
   * @param  aBO BusinessObject.
   *
   * @return PDMRequestTypeReferenceField
   */
  @Override protected Field doCreateField(BusinessObject aBO)
  {
    return new PDMRequestTypeReferenceField(aBO, this);
  }
}
