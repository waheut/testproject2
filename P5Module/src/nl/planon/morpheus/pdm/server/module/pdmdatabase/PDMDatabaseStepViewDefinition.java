// Planon Enterprise Edition Source file: PDMDatabaseStepViewDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmdatabase;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.state.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * DatabaseVersionStepViewDefinition
 */
public class PDMDatabaseStepViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDatabaseServerJoin;

  private PVJoinDef pdmDatabaseStateJoin;
  private PVJoinDef pdmDatabaseVersionJoin;
  private PVJoinDef pdmLocationJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseStepViewDefinition object.
   */
  public PDMDatabaseStepViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DATABASE);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDatabaseStateJoin.addSelectFieldDef(BOBaseBOStateDef.PN_PNNAME_ICON);
    addFieldDef(new PVSelectFieldDef(IBOPDMDatabaseDef.PN_NAME));
    addFieldDef(new PVSelectFieldDef(IBOPDMDatabaseDef.PN_DESCRIPTION));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDatabaseStateJoin = addInnerJoinDef(IBOPDMDatabaseDef.PN_REF_BOSTATE);
    this.pdmDatabaseServerJoin = addInnerJoinDef(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
    this.pdmDatabaseVersionJoin = this.pdmDatabaseServerJoin.addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);
    this.pdmLocationJoin = addInnerJoinDef(IBOPDMDatabaseDef.PN_PDMLOCATION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.pdmLocationJoin, IBOPDMLocationDef.PN_PRIMARYKEY);
    addStepSearchFieldDef(this.pdmDatabaseServerJoin, IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
    addStepSearchFieldDef(this.pdmDatabaseVersionJoin, IBOBasePDMDatabaseVersionDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   *
   * <p>TaArts: when PBL <b>13648, OSGI: handle cell renderers in a bundle</b> is implemented, then
   * this method should be removed! Also the createFieldDefs method can then be deleted. For now it
   * is not possible to have the state icon displayed in the BO list.</p>
   */
  @Override protected boolean doStateColumnNeeded()
  {
    return false;
  }
}
