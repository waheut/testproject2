// Planon Enterprise Edition Source file: PDMDatabaseServerStepViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmdatabaseserver;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * DatabaseServerStepViewDefinition
 */
public class PDMDatabaseServerStepViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef databaseVersionJoin;
  private PVJoinDef pdmLocationJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DatabaseServerStepViewDefinition object.
   */
  public PDMDatabaseServerStepViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_IS_ACTIVE);
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_CODE);
    addSelectFieldDef(IBOBasePDMDatabaseServerDef.PN_NAME);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.databaseVersionJoin = addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);
    this.pdmLocationJoin = addInnerJoinDef(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.databaseVersionJoin, IBOBasePDMDatabaseVersionDef.PN_PRIMARYKEY);
    addStepSearchFieldDef(this.pdmLocationJoin, IBOPDMLocationDef.PN_PRIMARYKEY);
  }
}
