// Planon Enterprise Edition Source file: PDMLocationStepViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmlocation;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMLocationStepViewDefinition
 */
public class PDMLocationStepViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMLocationStepViewDefinition object.
   */
  public PDMLocationStepViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_LOCATION);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOPDMLocationDef.PN_CODE);
    addSelectFieldDef(IBOPDMLocationDef.PN_NAME);
  }
}
