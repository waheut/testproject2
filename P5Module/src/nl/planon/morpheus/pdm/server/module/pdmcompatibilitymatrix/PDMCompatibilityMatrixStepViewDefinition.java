// Planon Enterprise Edition Source file: PDMCompatibilityMatrixStepViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmcompatibilitymatrix;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmcompatibilitymatrix.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseversion.*;

/**
 * PDMCompatibilityMatrixStepViewDefinition
 */
public class PDMCompatibilityMatrixStepViewDefinition extends PVStepDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PDMDATABASESERVER_NAME = "PDMDatabaseServerName";
  public static final String PDMDATABASEVERSION_CODE = "PDMDatabaseVersionCode";

  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDatabaseServerJoin;
  private PVJoinDef pdmDatabaseVersionJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DatabaseServerStepViewDefinition object.
   */
  public PDMCompatibilityMatrixStepViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_COMPATIBILITY_MATRIX);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDatabaseServerJoin.addFieldDef(new PVSelectFieldDef(BOBasePDMDatabaseServerDef.PN_NAME, PDMDATABASESERVER_NAME));
    this.pdmDatabaseVersionJoin.addFieldDef(new PVSelectFieldDef(BOBasePDMDatabaseVersionDef.PN_CODE, PDMDATABASEVERSION_CODE));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    // standard joins
    this.pdmDatabaseServerJoin = addJoinDef(new PVJoinDef(BOPDMCompatibilityMatrixDef.PN_DATABASESERVER_REF, PVJoinDef.INNER_JOIN));
    this.pdmDatabaseVersionJoin = addJoinDef(new PVJoinDef(BOPDMCompatibilityMatrixDef.PN_DATABASEVERSION_REF, PVJoinDef.INNER_JOIN));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.pdmDatabaseVersionJoin, IBOBasePDMDatabaseVersionDef.PN_PRIMARYKEY);
    addStepSearchFieldDef(this.pdmDatabaseServerJoin, IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
  }
}
