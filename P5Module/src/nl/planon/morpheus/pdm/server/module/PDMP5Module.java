// Planon Enterprise Edition Source file: PDMP5Module.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module;

import nl.planon.athena.module.*;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.module.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvtypeloader.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.module.pdmdbserverlog.*;
import nl.planon.morpheus.pdm.server.module.pdmrequestlog.*;

/**
 * PDMP5ModuleProcessModule
 */
public class PDMP5Module extends DynamicSysModule
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String PVTYPE_REQUESTLOG_NAME = "EL_REQUESTLOG";
  private static final String PVTYPE_DBSERVERLOG_NAME = "EL_DBSERVERLOG";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMP5ModuleProcessModule object.
   *
   * @param  aPnContext
   *
   * @throws PnErrorListException
   */
  public PDMP5Module(PnContext aPnContext) throws PnErrorListException
  {
    super(aPnContext);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createEntryDefs(PnContext aPnContext) throws PnErrorListException
  {
    addEntry(createEntryPDMP5Module(aPnContext));
  }


  /**
   * Create module entry with its levels, steps and drill down behavior
   *
   * @param  aPnContext current context
   *
   * @return The created entry
   *
   * @throws PnErrorListException
   */
  private SysEntry createEntryPDMP5Module(PnContext aPnContext) throws PnErrorListException
  {
    // ENTRY PDM Administrator
    SysEntry entryPDMP5Module = new SysEntry(ModulePDMP5Module.PDM_ADMIN_MODULE_ENTRY);
    entryPDMP5Module.setConfigurable(true);

    // LEVEL 'PDM Database Versions' and its steps 'Database Versions' and 'PDM Locations'
    final SysDrillDownLevel levelPDMDatabaseVersions = new SysDrillDownLevel(entryPDMP5Module, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_LEVEL);
    final SysDrillDownStep stepDatabaseVersions = new SysDrillDownStep(levelPDMDatabaseVersions, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_VERSIONS_STEP, BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION, PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_DATABASE_VERSIONS, aPnContext);
    final SysDrillDownStep stepPDMLocations = new SysDrillDownStep(levelPDMDatabaseVersions, ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_LOCATIONS_STEP, BOTypePDMP5Module.PDM_LOCATION, PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_PDM_LOCATIONS, aPnContext);

    // LEVEL 'PDM Database Servers' and its step 'Database Servers'
    final SysDrillDownLevel levelPDMDatabaseServers = new SysDrillDownLevel(entryPDMP5Module, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_SERVERS_LEVEL);
    final SysDrillDownStep stepDatabaseServers = new SysDrillDownStep(levelPDMDatabaseServers, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASE_SERVERS_STEP, BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_DATABASE_SERVERS, aPnContext);

    // LEVEL 'PDM Databases' and its steps 'PDM Databases', 'PDM Compatibility Matrix', 'PDM DbServer Logs' and 'Datafile locations of db server'
    final SysDrillDownLevel levelPDMDatabases = new SysDrillDownLevel(entryPDMP5Module, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_LEVEL);
    final SysDrillDownStep stepPDMDatabases = new SysDrillDownStep(levelPDMDatabases, ModulePDMP5Module.PDM_ADMIN_MODULE_DATABASES_STEP, BOTypePDMP5Module.PDM_DATABASE, PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_PDM_DATABASES, aPnContext);
    final SysDrillDownStep stepDbServerDataFileLoc = new SysDrillDownStep(levelPDMDatabases, ModulePDMP5Module.PDM_ADMIN_MODULE_DBSERVER_DATAFILELOC_STEP, BOTypePDMP5Module.PDM_DBSERVER_DATAFILELOCATION, PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_DBSERVER_DATAFILELOC, aPnContext);
    final SysDrillDownStep stepDbServerLogs = new SysDrillDownStep(levelPDMDatabases, ModulePDMP5Module.PDM_ADMIN_MODULE_DBSERVER_LOG_STEP, BOTypeHades.LINKED_EVENT_LOG, getPVTypeForDbServerLogs(aPnContext), aPnContext);

    // LEVEL 'PDM Requests' and its step 'PDM Requests'
    final SysDrillDownLevel levelPDMRequests = new SysDrillDownLevel(entryPDMP5Module, ModulePDMP5Module.PDM_ADMIN_MODULE_REQUESTS_LEVEL);
    final SysDrillDownStep stepPDMRequests = new SysDrillDownStep(levelPDMRequests, ModulePDMP5Module.PDM_ADMIN_MODULE_REQUESTS_STEP, BOTypePDMP5Module.BASE_PDM_REQUEST, PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_REQUESTS, aPnContext);

    // LEVEL 'PDM Tasks' and its steps 'PDM Tasks' and 'PDM Request Logs'
    final SysDrillDownLevel levelPDMTasks = new SysDrillDownLevel(entryPDMP5Module, ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_LEVEL);
    final SysDrillDownStep stepPDMTasks = new SysDrillDownStep(levelPDMTasks, ModulePDMP5Module.PDM_ADMIN_MODULE_PDM_TASKS_STEP, BOTypePDMP5Module.BASE_PDM_TASK, PVTypePDMP5Module.PN_STEP_PDM_ADMIN_MODULE_TASKS, aPnContext);
    final SysDrillDownStep stepRequestLogs = new SysDrillDownStep(levelPDMTasks, ModulePDMP5Module.PDM_ADMIN_MODULE_REQUEST_LOG_STEP, BOTypeHades.LINKED_EVENT_LOG, getPVTypeForRequestLogs(aPnContext), aPnContext);

    stepDatabaseVersions.setMultiSelect(true);
    stepPDMLocations.setMultiSelect(true);
    stepDatabaseServers.setMultiSelect(true);
    stepPDMDatabases.setMultiSelect(true);
    stepPDMRequests.setMultiSelect(true);
    stepPDMTasks.setMultiSelect(true);

    // drilldown behaviour
    stepDatabaseVersions.addDrillDownPath(stepDatabaseServers);
    stepDatabaseVersions.addDrillDownPath(stepPDMDatabases);

    stepPDMLocations.addDrillDownPath(stepDatabaseServers);
    stepPDMLocations.addDrillDownPath(stepPDMDatabases);

    stepDatabaseServers.addDrillDownPath(stepPDMDatabases);
    stepDatabaseServers.addDrillDownPath(stepDbServerDataFileLoc);
    stepDatabaseServers.addDrillDownPath(stepDbServerLogs);

    stepPDMDatabases.addDrillDownPath(stepPDMRequests);
    stepPDMDatabases.addDrillDownPath(stepPDMTasks);
    stepPDMDatabases.addDrillDownPath(stepRequestLogs);

    stepPDMRequests.addDrillDownPath(stepPDMTasks);
    stepPDMRequests.addDrillDownPath(stepRequestLogs);

    stepDbServerDataFileLoc.endDrillDownPath();
    stepDbServerLogs.endDrillDownPath();
    stepPDMTasks.endDrillDownPath();
    stepRequestLogs.endDrillDownPath();

    return entryPDMP5Module;
  }


  /**
   * gets eventlog PV type to access subjects
   *
   * @param  aPvTypeName          name of proxyview to register
   * @param  aSubjectBOType       BOtype to link the event log to
   * @param  aViewDefinitionClass ViewDefinition class to register
   * @param  aPnContext           Planon Context
   *
   * @return IPVType requested PVType
   *
   * @throws PnErrorListException failed to access cache
   */
  private IPVType getPVTypeEventLog(String aPvTypeName, IBOType aSubjectBOType, Class<? extends PVBaseDef> aViewDefinitionClass, PnContext aPnContext) throws PnErrorListException
  {
    PVTypeManagerCache cache = aPnContext.getCache(PVTypeManagerCache.class);

    IPVTypeLoader loader = cache.getPVTypeLoader(aPvTypeName);

    IDynamicBODefinition eventLogBODefinition = (IDynamicBODefinition) aPnContext.getBODefinition(BOTypeHades.LINKED_EVENT_LOG);
    IBOType intermediateBOType = eventLogBODefinition.getIntermediateBOType(aSubjectBOType, aPnContext);

    Class[] argumentTypes = {IBOType.class, IBOType.class, IBOType.class};
    Object[] arguments = {BOTypeHades.LINKED_EVENT_LOG, aSubjectBOType, intermediateBOType};
    loader = cache.register(aPvTypeName, aViewDefinitionClass, argumentTypes, arguments, aPnContext);

    return loader.getPVType();
  }


  /**
   * Gets the PV type for the  DbServerLogs
   *
   * @param  aPnContext Planon Context
   *
   * @return IPVType
   *
   * @throws PnErrorListException failed to access cache
   */
  private IPVType getPVTypeForDbServerLogs(PnContext aPnContext) throws PnErrorListException
  {
    String pvTypeName = PVTYPE_DBSERVERLOG_NAME;
    IBOType subjectBOType = BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER;
    Class<? extends PVBaseDef> viewDefinitionClass = PDMDbServerLogInContextOfStepViewDefinition.class;

    return getPVTypeEventLog(pvTypeName, subjectBOType, viewDefinitionClass, aPnContext);
  }


  /**
   * Gets the PV type for the  RequestLogs
   *
   * @param  aPnContext Planon Context
   *
   * @return IPVType
   *
   * @throws PnErrorListException failed to access cache
   */
  private IPVType getPVTypeForRequestLogs(PnContext aPnContext) throws PnErrorListException
  {
    String pvTypeName = PVTYPE_REQUESTLOG_NAME;
    IBOType subjectBOType = BOTypePDMP5Module.BASE_PDM_REQUEST;
    Class<? extends PVBaseDef> viewDefinitionClass = PDMRequestLogsInContextOfStepViewDefinition.class;

    return getPVTypeEventLog(pvTypeName, subjectBOType, viewDefinitionClass, aPnContext);
  }
}
