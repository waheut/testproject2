// Planon Enterprise Edition Source file: PDMTaskStepViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmtask;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.state.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMTaskStepViewDefinition
 */
public class PDMTaskStepViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDatabaseJoin;

  private PVJoinDef pdmRequestJoin;
  private PVJoinDef pdmTaskState;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTaskStepViewDefinition object.
   */
  public PDMTaskStepViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_TASK);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmTaskState.addSelectFieldDef(BOBaseBOStateDef.PN_PNNAME_ICON);
    addFieldDef(new PVSystemVisibleSelectFieldDef(IBOBasePDMTaskDef.PN_SYSINSERTDATETIME));
    addFieldDef(new PVSystemVisibleSelectFieldDef(IBOBasePDMTaskDef.PN_PRIMARYKEY));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmRequestJoin = addInnerJoinDef(IBOBasePDMTaskDef.PN_PDMREQUEST_REF);
    this.pdmDatabaseJoin = this.pdmRequestJoin.addInnerJoinDef(IBOBasePDMRequestDef.PN_DATABASE_REF);
    this.pdmTaskState = addInnerJoinDef(IBOBasePDMTaskDef.PN_REF_BOSTATE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.pdmRequestJoin, IBOBasePDMRequestDef.PN_PRIMARYKEY);
    addStepSearchFieldDef(this.pdmDatabaseJoin, IBOPDMDatabaseDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   *
   * <p>TA: when PBL <b>13648, OSGI: handle cell renderers in a bundle</b> is implemented, then this
   * method should be removed! Also the manually added icon field in the createFieldDefs method can
   * then be deleted. For now it is not possible to have the state icon displayed in the BO list.
   * </p>
   */
  @Override protected boolean doStateColumnNeeded()
  {
    return false;
  }
}
