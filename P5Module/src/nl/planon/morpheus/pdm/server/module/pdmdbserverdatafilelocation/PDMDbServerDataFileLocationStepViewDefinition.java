// Planon Enterprise Edition Source file: PDMDbServerDataFileLocationStepViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmdbserverdatafilelocation;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.*;

/**
 * PDMDbServerDataFileLocationStepViewDefinition
 */
public class PDMDbServerDataFileLocationStepViewDefinition extends PVStepDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PDMDATABASESERVER_NAME = "PDMDatabaseServerName";

  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDatabaseServerJoin;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDbServerDataFileLocationStepViewDefinition object.
   */
  public PDMDbServerDataFileLocationStepViewDefinition()
  {
    super(BOTypePDMP5Module.PDM_DBSERVER_DATAFILELOCATION);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDatabaseServerJoin.addFieldDef(new PVSelectFieldDef(BOBasePDMDatabaseServerDef.PN_NAME, PDMDATABASESERVER_NAME));
    addFieldDef(new PVSelectFieldDef(BOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    // standard joins
    this.pdmDatabaseServerJoin = addJoinDef(new PVJoinDef(BOPDMDBServerDataFileLocationDef.PN_DATABASESERVER_REF, PVJoinDef.INNER_JOIN));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.pdmDatabaseServerJoin, IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY);
  }
}
