// Planon Enterprise Edition Source file: PDMRequestStepViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmrequest;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.state.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequesttype.*;

/**
 * PDMRequestStepViewDefinition
 */
public class PDMRequestStepViewDefinition extends PVStepDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef pdmDatabaseJoin;
  private PVJoinDef pdmRequestState;
  private PVJoinDef pdmRequestType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseStepViewDefinition object.
   */
  public PDMRequestStepViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_REQUEST);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmRequestState.addSelectFieldDef(BOBaseBOStateDef.PN_PNNAME_ICON);
    this.pdmRequestType.addFieldDef(new PVSelectFieldDef(BOPDMRequestTypeDef.PN_PNNAME_TRANSKEY));
    addFieldDef(new PVSystemVisibleSelectFieldDef(IBOBasePDMRequestDef.PN_SYSINSERTDATETIME));
    addFieldDef(new PVSystemVisibleSelectFieldDef(IBOBasePDMRequestDef.PN_PRIMARYKEY));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    this.pdmDatabaseJoin = addInnerJoinDef(IBOBasePDMRequestDef.PN_DATABASE_REF);
    this.pdmRequestType = addInnerJoinDef(IBOBasePDMRequestDef.PN_REQUESTTYPE_REF);
    this.pdmRequestState = addInnerJoinDef(IBOBasePDMRequestDef.PN_REF_BOSTATE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createStepSearchDefs(PnContext aPnContext) throws PnErrorListException
  {
    addStepSearchFieldDef(this.pdmDatabaseJoin, IBOPDMDatabaseDef.PN_PRIMARYKEY);
    addStepSearchFieldDef(this.pdmRequestType, IBOBasePDMRequestDef.PN_PRIMARYKEY);
  }


  /**
   * {@inheritDoc}
   *
   * <p>TA: when PBL <b>13648, OSGI: handle cell renderers in a bundle</b> is implemented, then this
   * method should be removed! Also the manually added icon field in the createFieldDefs method can
   * then be deleted. For now it is not possible to have the state icon displayed in the BO list.
   * </p>
   */
  @Override protected boolean doStateColumnNeeded()
  {
    return false;
  }
}
