// Planon Enterprise Edition Source file: PDMDatabaseVersionStepViewDefinition.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmdatabaseversion;

import nl.planon.hades.beans.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

/**
 * PDMDatabaseVersionStepViewDefinition
 */
public class PDMDatabaseVersionStepViewDefinition extends PVStepDefinition
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DatabaseServerStepViewDefinition object.
   */
  public PDMDatabaseVersionStepViewDefinition()
  {
    super(BOTypePDMP5Module.BASE_PDM_DATABASE_VERSION);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(IBOBasePDMDatabaseVersionDef.PN_CODE);
    addSelectFieldDef(IBOBasePDMDatabaseVersionDef.PN_NAME);
  }
}
