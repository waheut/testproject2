// Planon Enterprise Edition Source file: PDMDbServerLogInContextOfStepViewDefinition.java
// Copyright Planon 1997-2015. All Rights Reserved.
package nl.planon.morpheus.pdm.server.module.pdmdbserverlog;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.account.*;
import nl.planon.hades.businessmodel.eventlog.linked.*;
import nl.planon.hades.dynamicboreference.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.proxyview.pvfielddefinition.*;

/**
 * PDMDbServerLogInContextOfStepViewDefinition
 */
public class PDMDbServerLogInContextOfStepViewDefinition extends DynamicBOReferenceStepViewDefinition
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PVJoinDef joinAccount;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDbServerLogInContextOfStepViewDefinition object.
   *
   * @param aMainBOType
   * @param aDecendBOType
   * @param aConnectionBOType
   */
  public PDMDbServerLogInContextOfStepViewDefinition(IBOType aMainBOType, IBOType aDecendBOType, IBOType aConnectionBOType)
  {
    super(aMainBOType, aDecendBOType, aConnectionBOType);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected void createFieldDefs(PnContext aPnContext) throws PnErrorListException
  {
    addSelectFieldDef(BOLinkedEventLogDef.PN_LOGTYPE);
    addSelectFieldDef(BOLinkedEventLogDef.PN_SOURCE);
    addFieldDef(new PVSystemVisibleSelectFieldDef(BOLinkedEventLogDef.PN_SYSINSERTDATETIME));
    this.joinAccount.addFieldDef(new PVSelectFieldDef(BOAccountDef.PN_ACCOUNTNAME));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void createJoinDefs(PnContext aPnContext) throws PnErrorListException
  {
    super.createJoinDefs(aPnContext);
    this.joinAccount = addJoinDef(new PVJoinDef(BOLinkedEventLogDef.PN_SYSACCOUNT_REF, PVJoinDef.LEFT_OUTER_JOIN));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doAddAdditionJoinDefs(PVJoinDef aDecendJoin, PnContext aPnContext)
  {
    super.doAddAdditionJoinDefs(aDecendJoin, aPnContext);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void doAddAdditionStepSearchDefs(PVJoinDef aDescendJoin, String aPrimKeyFieldPnName)
  {
    super.doAddAdditionStepSearchDefs(aDescendJoin, aPrimKeyFieldPnName);
  }
}
