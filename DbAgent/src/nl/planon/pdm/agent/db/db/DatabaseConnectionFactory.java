// Planon Enterprise Edition Source file: DatabaseConnectionFactory.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db.db;

import oracle.net.ns.*;

import java.net.*;
import java.sql.*;
import java.util.*;

import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;
import nl.planon.pdm.core.datamessage.oracle.*;

/**
 * DatabaseConnectionFactory
 */
public class DatabaseConnectionFactory
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * create a connection with a database server
   *
   * @return active database connection or null if connection failed
   *
   * @throws SQLException
   * @throws RuntimeException
   */
  public static Connection getConnection() throws SQLException
  {
    // -Djdbc.connect.url=jdbc:oracle:thin:@NL-DEVS04:1521:DV2102
    // -Djdbc.user=TESTER -Djdbc.password=TESTER
    try
    {
      Class.forName("oracle.jdbc.driver.OracleDriver");
    }
    catch (ClassNotFoundException ex)
    {
      throw new RuntimeException("Oracle class not found", ex);
    }

    String connectionURL = System.getProperty("jdbc.connect.url");
    String username = System.getProperty("jdbc.user");
    String password = System.getProperty("jdbc.password");

    Properties pty = new java.util.Properties();
    pty.put("user", username);
    pty.put("password", password);
    // pty.put("internal_logon", "sysdba");

    return DriverManager.getConnection(connectionURL, pty);
  }


  /**
   * create a connection to an Oracle database server
   *
   * @param  aDatabaseServerInformation contains the information needed to create a database
   *                                    connection
   *
   * @return active database connection or null if connection failed
   *
   * @throws SQLException
   */
  public static Connection getConnection(DatabaseServerInformation aDatabaseServerInformation) throws SQLException
  {
    if (aDatabaseServerInformation instanceof OracleDatabaseServerInformation)
    {
      return getConnection((OracleDatabaseServerInformation) aDatabaseServerInformation);
    }
    if (aDatabaseServerInformation instanceof MSSQLDatabaseServerInformation)
    {
      return getConnection((MSSQLDatabaseServerInformation) aDatabaseServerInformation);
    }
    return null;
  }


  /**
   * create a connection to an MSSQL database server
   *
   * @param  aDatabaseServerInformation contains the information needed to create a database
   *                                    connection
   *
   * @return active database connection or null if connection failed
   *
   * @throws SQLException
   * @throws RuntimeException
   */
  private static Connection getConnection(MSSQLDatabaseServerInformation aDatabaseServerInformation) throws SQLException
  {
    try
    {
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    }
    catch (ClassNotFoundException ex)
    {
      throw new RuntimeException("MSSQL class not found", ex);
    }
    Connection connection = null;

    String connectionURL = aDatabaseServerInformation.getConnectServerName();
    String inst = aDatabaseServerInformation.getDatabaseInstanceName();
    String instancename = "";
    if (inst != null)
    {
      instancename = ";instanceName=" + inst;
    }

    // =========================================================
    Properties pty = new java.util.Properties();
    pty.put("user", aDatabaseServerInformation.getAdminUser());
    pty.put("password", aDatabaseServerInformation.getAdminPassword());
    // pty.put("internal_logon", "sysdba");
    String reason = "unknown error";
    try
    {
      connection = DriverManager.getConnection(connectionURL + instancename, pty);
    }
    catch (SQLRecoverableException ex)
    {
      Throwable cause = ex.getCause();
      if ((cause != null) && (cause instanceof NetException))
      {
        Throwable deepercause = cause.getCause();

        if ((deepercause != null) && (deepercause instanceof ConnectException))
        {
          reason = "Possible cause: WRONG PORTNUMBER";
        }
        else if ((deepercause != null) && (deepercause instanceof UnknownHostException))
        {
          reason = "Possible cause: WRONG HOSTNAME";
        }
      }
      throw new SQLException(reason);
    }
    catch (SQLException ex)
    {
      Throwable cause = ex.getCause();
      if ((cause != null) && (cause instanceof NetException))
      {
        reason = "Possible cause: WRONG DATABASE INSTANCE NAME";
      }
      else if (cause == null)
      {
        reason = "Possible cause: WRONG USERNAME/PASSWORD";
      }
      ex.printStackTrace();
      throw new SQLException(reason);
    }
    return connection;
  }


  /**
   * create a connection to an Oracle database server
   *
   * @param  aDatabaseServerInformation contains the information needed to create a database
   *                                    connection
   *
   * @return active database connection or null if connection failed
   *
   * @throws SQLException
   * @throws RuntimeException
   */
  private static Connection getConnection(OracleDatabaseServerInformation aDatabaseServerInformation) throws SQLException
  {
    // -Djdbc.connect.url=jdbc:oracle:thin:@NL-DEVS04:1521:DV2102
    // -Djdbc.user=TESTER -Djdbc.password=TESTER
    try
    {
      Class.forName("oracle.jdbc.driver.OracleDriver");
    }
    catch (ClassNotFoundException ex)
    {
      throw new RuntimeException("Oracle class not found", ex);
    }
    Connection connection = null;

    String connectionURL = "jdbc:oracle:thin:@" + aDatabaseServerInformation.getHostName() + ":" + aDatabaseServerInformation.getPortNumber() + ":" + aDatabaseServerInformation.getDatabaseInstanceName();

    Properties pty = new java.util.Properties();
    pty.put("user", aDatabaseServerInformation.getAdminUser());
    pty.put("password", aDatabaseServerInformation.getAdminPassword());
    // pty.put("internal_logon", "sysdba");
    String reason = "unknown error";
    connection = DriverManager.getConnection(connectionURL, pty);
    return connection;
  }
}
