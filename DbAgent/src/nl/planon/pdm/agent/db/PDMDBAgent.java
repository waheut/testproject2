// Planon Enterprise Edition Source file: PDMDBAgent.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.agent.db;

import jade.content.*;
import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.proto.*;
import jade.util.leap.*;

import org.apache.commons.httpclient.*;

import java.io.*;

import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.response.*;
import nl.planon.pdm.core.ontology.*;

import nl.planon.util.pnlogging.*;
import nl.planon.util.webdav.*;

/**
 * implements behavior of DB agent
 */
public class PDMDBAgent extends AbstractPDMAgent
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String NOT_AVAILABLE = "<not available>";
  private static final String LOGFILE_POSTFIX = "_import";

  private static final PnLogger LOG = PnLogger.getLogger(PDMDBAgent.class, PnLogCategory.DEFAULT);

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected PDMAgentType getAgentType()
  {
    return PDMAgentType.DATABASE_AGENT;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected Behaviour getBehaviour(MessageTemplate aTemplate)
  {
    MessageTemplate messageTemplate = MessageTemplate.and(MessageTemplate.MatchLanguage(PDMOntology.CODEC.getName()), MessageTemplate.MatchOntology(PDMOntology.ONTOLOGY.getName()));
    return new AchieveREResponder(this, messageTemplate)
      {
        private ACLMessage reply;
        private AbstractPDMAgent pdmAgent = (AbstractPDMAgent) this.myAgent;
        private String dumpFileDir = null;
        private String dumpFileName = null;

        private String logOutputToWebDAV(String aPostFix) throws IOException
        {
          String nameOfDumpFile = "unknown.out";
          String newExtension = ".log";
          if (this.dumpFileName != null)
          {
            File dumpFile = new File(this.dumpFileName);
            nameOfDumpFile = dumpFile.getName();
          }

          int indexDot = nameOfDumpFile.lastIndexOf('.');
          String startName = ((indexDot < 0) ? nameOfDumpFile : nameOfDumpFile.substring(0, indexDot));

          String logFileName = startName + ((aPostFix != null) ? aPostFix : "") + newExtension;

          PnWebDAVResource directory = new PnWebDAVResource(new HttpURL(this.dumpFileDir));
          boolean result = directory.putMethod(directory.getPath() + "/" + logFileName, getPDMFeedbackHandler().getReportBuilder().toString(), null);
          if (!result)
          {
            System.err.println("Error while uploading file " + this.dumpFileDir + "/" + logFileName + ": " + directory.getStatusCode() + " - " + directory.getStatusMessage());
            return null;
          }
          return logFileName;
        }


        /**
         * handle exception . there is no guaranty the task state is updated afterwards because the
         * pk of the task might not be part of the response
         *
         * @param  aPDMexception
         * @param  aException
         * @param  aPerformative e.g. ACLMessage.FAILURE
         *
         * @return
         */
        private ACLMessage handleExceptionReply(PDMException aPDMexception, Throwable aException, int aPerformative)
        {
          try
          {
            String errorCode;
            if (aPDMexception != null)
            {
              errorCode = aPDMexception.getErrorDefinition().getCode();
              if (LOG.isErrorEnabled())
              {
                String description = aPDMexception.getDescription();
                if (aPDMexception.equals(aException))
                {
                  LOG.error(description);
                }
                else
                {
                  LOG.error(description, aException); // only stacktrace if not a pdm exception
                }
              }
            }
            else
            {
              errorCode = aException.toString();
            }

            AbstractPDMAgent pdmAgent = (AbstractPDMAgent) this.myAgent;
            Concept concept = null;
            try
            {
              Action action = (Action) pdmAgent.getContentManager().extractContent(this.reply);
              concept = action.getAction();
            }
            catch (CodecException ex)
            {
              // Apparently response via ontology does not work. Now try via plain content
              this.reply.setContent(errorCode);
            }
            catch (OntologyException ex)
            {
              // Apparently response via ontology does not work. Now try via plain content
              this.reply.setContent(errorCode);
            }

            IExceptionInformation exceptionInformation;
            if (concept instanceof AbstractResponse)
            {
              exceptionInformation = ((AbstractResponse) concept).getExceptionInformation();
            }
            else
            {
              exceptionInformation = new ExceptionInformation();
              concept = (Concept) exceptionInformation;
            }

            List errorInfoList = new ArrayList();
            for (PDMException pdmException : pdmAgent.getPDMFeedbackHandler().getPdmErrors())
            {
              ErrorInformation errorInformation = new ErrorInformation();
              errorInformation.setErrorCode(pdmException.getErrorDefinition().getCode());
              errorInformation.setIsErrorForAdministrator(pdmException.isErrorForAdministrator());
              java.util.List<String> msgVarList = pdmException.getMessageVariables();
              if (!msgVarList.isEmpty())
              {
                List errMsgVariables = new ArrayList();
                for (String msgVar : msgVarList)
                {
                  errMsgVariables.add(msgVar);
                }
                errorInformation.setMessageVariables(errMsgVariables);
              }
              errorInfoList.add(errorInformation);
            }
            try
            {
              pdmAgent.getPDMFeedbackHandler().reportErrors();
              if (this.dumpFileDir != null) // only if webdav configuration is passed to db agent
              {
                String logFileName = logOutputToWebDAV(LOGFILE_POSTFIX);
                exceptionInformation.setLogFileDir(this.dumpFileDir);
                exceptionInformation.setLogFileName(logFileName);
              }
              exceptionInformation.setErrorInformation(errorInfoList);
            }
            catch (IOException ex)
            {
              // logfile via WebDav not possible.
              exceptionInformation.setLogFileDir(NOT_AVAILABLE);
              exceptionInformation.setLogFileName(NOT_AVAILABLE);
              exceptionInformation.setErrorInformation(errorInfoList);
            }

            try
            {
              pdmAgent.getContentManager().fillContent(this.reply, new Action(pdmAgent.getAID(), concept));
            }
            catch (CodecException ex)
            {
              // Apparently response via ontology does not work. Now try via plain content
              this.reply.setContent(errorCode);
            }
            catch (OntologyException ex)
            {
              // Apparently response via ontology does not work. Now try via plain content
              this.reply.setContent(errorCode);
            }

            this.reply.setPerformative(aPerformative);
            return this.reply;
          }
          catch (Throwable ex)
          {
            // try to inform controller about failure ....
            LOG.error("Error occurred: ", ex);
            try
            {
              this.pdmAgent.getContentManager().fillContent(this.reply, new Action(this.pdmAgent.getAID(), new ExceptionInformation()));
            }
            catch (Exception ex2)
            {
              LOG.error("Error handling failed, additional error occurred: ", ex2);
              // do nothing
            }
            this.reply.setPerformative(ACLMessage.FAILURE);
            return this.reply;
          }
        }


        @Override protected ACLMessage handleRequest(ACLMessage aRequest)
        {
          getPDMFeedbackHandler().clear();
          try
          {
            this.reply = aRequest.createReply();
            this.reply.setEncoding("UTF-8");

            try
            {
              // enable next lines to test the situation the db agent fails without returning primarykeys of request/state/task
//              if (1 == 1)
//              {
//                throw getPDMFeedbackHandler().addException(ErrorDefinition.DATABASE_SERVER_VERSION_NOT_MATCH, new CodecException("Thrown to test case where dbagent cannnot return primary keys"), "1", "2", "CodecException");
//              }

              ContentElement contentElement = this.pdmAgent.getContentManager().extractContent(aRequest);

              Action action = (Action) contentElement;
              Concept concept = action.getAction();

              Class<? extends AbstractDBAgentAction> classDBAgentAction = getDBAgentActionClassForAction(action);
              if (classDBAgentAction != null)
              {
                if (concept instanceof AbstractRequest)
                {
                  AbstractRequest abstractRequest = (AbstractRequest) concept;
                  String versionNumber = abstractRequest.getVersionNumber();
                  String versionNumberAgent = PDMOntology.versionNumber;
                  boolean versionMatches = versionNumberAgent.equals(versionNumber);
                  if (!versionMatches)
                  {
                    refuse(concept, "Version " + versionNumber + " of the controller agent does not match the version " + versionNumberAgent + " of the database agent, task not executed (" + concept.getClass().getName() + ").");
                  }
                  else
                  {
                    if (concept instanceof WebDAVRequest)
                    {
                      WebDAVRequest request = (WebDAVRequest) concept;
                      this.dumpFileDir = request.getDumpFileDir();
                      this.dumpFileName = request.getDumpFileName();
                    }
                    // I understand this message; agreed to handle this.
                    this.reply.setPerformative(ACLMessage.AGREE);
                  }
                }
              }
              else
              {
                refuse(concept, "NO action registered for request " + concept.getClass().getName());
              }
            }
            catch (CodecException ex)
            {
              throw getPDMFeedbackHandler().addException(ErrorDefinition.MESSAGE_CONTENT_CANNOT_BE_HANDLED, ex, "CodecException");
            }
            catch (OntologyException ex)
            {
              throw getPDMFeedbackHandler().addException(ErrorDefinition.MESSAGE_CONTENT_CANNOT_BE_HANDLED, ex, "OntologyException");
            }
            return this.reply;
          }
          catch (PDMException ex)
          {
            return handleExceptionReply(ex, ex, ACLMessage.FAILURE);
          }
          catch (Throwable ex)
          {
            return handleExceptionReply(null, ex, ACLMessage.FAILURE);
          }
        }


        /**
         * refuse current request for reason <code>aReason</code>
         *
         * @param  aConcept pending request
         * @param  aReason  reason of failure
         *
         * @throws CodecException
         * @throws OntologyException
         */
        private void refuse(Concept aConcept, String aReason) throws CodecException, OntologyException
        {
          RefuseResponse response = new RefuseResponse();
          response.setRefuseReason(aReason);

          if (aConcept instanceof AbstractRequest)
          {
            AbstractRequest abstractRequest = (AbstractRequest) aConcept;
            response.fillMandatoryResponseData(abstractRequest);
            response.setRequestTypeCode(abstractRequest.getRequestTypeCode());
          }
          Action actionRefuse = new Action(this.pdmAgent.getAID(), response);
          this.pdmAgent.getContentManager().fillContent(this.reply, actionRefuse);
          this.reply.setPerformative(ACLMessage.REFUSE);
        }


        @Override protected ACLMessage prepareResultNotification(ACLMessage aRequestMSG, ACLMessage aPreviousResponse)
        {
          try
          {
            this.reply = aRequestMSG.createReply();
            this.reply.setEncoding("UTF-8");

            Action action = null;

            /*
             * Step 1: Decypher message and find the Action.
             */
            try
            {
              action = (Action) this.pdmAgent.getContentManager().extractContent(aRequestMSG);
            }
            catch (CodecException ex)
            {
              throw getPDMFeedbackHandler().addException(ErrorDefinition.MESSAGE_CONTENT_CANNOT_BE_HANDLED, ex, "CodecException");
            }
            catch (OntologyException ex)
            {
              throw getPDMFeedbackHandler().addException(ErrorDefinition.MESSAGE_CONTENT_CANNOT_BE_HANDLED, ex, "OntologyException");
            }

            /*
             * Step 2: Find the DBAgentAction class to match the requested action.
             */
            Concept concept = action.getAction();
            Class<? extends AbstractDBAgentAction> classDBAgentAction = getDBAgentActionClassForAction(action);
            if (classDBAgentAction == null)
            {
              throw getPDMFeedbackHandler().addException(ErrorDefinition.UNKNOWN_MESSAGE_CONTENT, null, concept.toString());
            }

            /*
             * Step 3: Instantiate the DBAgentAction class.
             */
            AbstractDBAgentAction pdmDBAgentAction = getDBAgentAction(classDBAgentAction);

            AbstractResponse responseData = pdmDBAgentAction.getResponseData();
            if (concept instanceof AbstractRequest)
            {
              AbstractRequest abstractRequest = (AbstractRequest) concept;
              responseData.setPrimaryKeyDatabase(abstractRequest.getPrimaryKeyDatabase());
              responseData.setPrimaryKeyRequest(abstractRequest.getPrimaryKeyRequest());
              responseData.setPrimaryKeyTask(abstractRequest.getPrimaryKeyTask());
              responseData.setRequestTypeCode(abstractRequest.getRequestTypeCode());
              responseData.setPrivilegesTypeCode(abstractRequest.getPrivilegesTypeCode());
            }

            /*
             * Step 4: Execute the DBAgentAction.execute() (do the real work!).
             */
            try
            {
              if (LOG.isInfoEnabled())
              {
                LOG.info("DB Agent: Starting DBAgent action: " + classDBAgentAction.getName());
              }
              this.reply = pdmDBAgentAction.execute(aRequestMSG, concept);
              this.reply.setEncoding("UTF-8");
            }
            finally
            {
              if (LOG.isInfoEnabled())
              {
                LOG.info("DB Agent: DBAgent action: " + classDBAgentAction.getName() + " finished. Returning result.");
              }

              pdmDBAgentAction.fillResponseData();

              /*
               * Step 6: Write log file to webdav.
               */
              boolean actionExecutedSuccessfully = (this.reply.getPerformative() == ACLMessage.INFORM);
              if (actionExecutedSuccessfully)
              {
                pdmDBAgentAction.responseFilesToWebDAV((AbstractRequest) concept);
              }
              else
              {
                // If for some reason, the action hasnt been completed, then set the response details correctly to process further.
                AbstractRequest aRequest = (AbstractRequest) concept;
                responseData.setLogFileDir(aRequest.getDumpFileDir());
                responseData.setLogFileName(NOT_AVAILABLE);
              }

              /*
               * Step 5: Create reply for Controller Agent.
               */
              try
              {
                if ((responseData != null) && (this.reply != null))
                {
                  Action actionOp = new Action(this.pdmAgent.getAID(), responseData);
                  this.pdmAgent.getContentManager().fillContent(this.reply, actionOp);
                }
              }
              catch (CodecException ex)
              {
                if (LOG.isErrorEnabled())
                {
                  LOG.error("Error filling message content (CodecException): " + classDBAgentAction.getName(), ex);
                }
                throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_FILLING_RESPONSE_MESSAGE, ex, "CodecException", classDBAgentAction.getName());
              }
              catch (OntologyException ex)
              {
                if (LOG.isErrorEnabled())
                {
                  LOG.error("Error filling message content (OntologyException): " + classDBAgentAction.getName(), ex);
                }
                throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_FILLING_RESPONSE_MESSAGE, ex, "OntologyException", classDBAgentAction.getName());
              }
            }
          }
          catch (PDMException ex)
          {
            return handleExceptionReply(ex, ex, ACLMessage.FAILURE);
          }
          catch (Throwable ex) // catch runtime exceptions, asserts, etc
          {
            PDMException pdmException = getPDMFeedbackHandler().addException(ErrorDefinition.INTERNAL_ERROR, ex);
            return handleExceptionReply(pdmException, ex, ACLMessage.FAILURE);
          }
          return this.reply;
        }


        private AbstractDBAgentAction getDBAgentAction(Class<? extends AbstractDBAgentAction> aClassDBAgentAction) throws PDMException
        {
          AbstractDBAgentAction action = null;
          try
          {
            action = aClassDBAgentAction.newInstance();
            action.setAgent(this.pdmAgent);
          }
          catch (InstantiationException ex)
          {
            throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_INSTANTIATION_AGENT_ACTION, ex, "InstantiationException", aClassDBAgentAction.getName());
          }
          catch (IllegalAccessException ex)
          {
            throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_INSTANTIATION_AGENT_ACTION, ex, "IllegalAccessException", aClassDBAgentAction.getName());
          }
          return action;
        }


        private Class<? extends AbstractDBAgentAction> getDBAgentActionClassForAction(Action aRequestAction)
        {
          return DBAgentActionRegistry.getInstance().get(aRequestAction.getAction().getClass().getName() + "");
        }
      };
  }
}
