// Planon Enterprise Edition Source file: AbstractPDMAgent.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db;

import jade.core.behaviours.*;
import jade.domain.*;
import jade.lang.acl.*;

import java.util.regex.*;

import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.ontology.*;

import nl.planon.util.pnlogging.*;

/**
 * This class provides an abstract base for all PDM 3 client agents.
 */
public abstract class AbstractPDMAgent extends PDMAgent
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(AbstractPDMAgent.class, PnLogCategory.DEFAULT);

  private static final String REPLY_OK = "Ok";
  private static final String REPLY_NOK = "NotOk";

  //~ Instance Variables ---------------------------------------------------------------------------

  private PDMDatabaseType dbType = PDMDatabaseType.getPDMDatabaseTypeByName(PDMAgentProperties.DEFAULT_DB_TYPE);
  private PDMFeedbackHandler pdmFeedbackHandler = PDMFeedbackHandler.getInstance();
  private PDMFileType fileType = PDMAgentProperties.DEFAULT_FILE_TYPE;

  // set initial default values; can be overwritten via arguments at agent
  // startup...
  private String dbServerName = PDMAgentProperties.DEFAULT_DB_SERVER;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AbstractPDMAgent object.
   */
  public AbstractPDMAgent()
  {
    super();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets the name of the database server on which this agent runs on
   *
   * @return the code of the database server on which this agent runs on
   */
  public String getDbServerName()
  {
    return this.dbServerName;
  }


  /**
   * gets the database type that the database server, on which this agent runs on, supports
   *
   * @return the database type that the database server, on which this agent runs on, supports
   */
  public PDMDatabaseType getDbType()
  {
    return this.dbType;
  }


  /**
   * gets the file type that the database server, on which this agent runs on, supports. F.e. Mssql
   * dump or Oracle Datapump
   *
   * @return the file type that the database server, on which this agent runs on, supports
   */
  public PDMFileType getFileType()
  {
    return this.fileType;
  }


  /**
   * gets the error handler of this agent
   *
   * @return the error handler of this agent
   */
  public PDMFeedbackHandler getPDMFeedbackHandler()
  {
    return this.pdmFeedbackHandler;
  }


  /**
   * sets the name of the database server on which this agent runs on
   *
   * @param aDbServerName name of the database server on which this agent runs on
   */
  public void setDbServerName(String aDbServerName)
  {
    this.dbServerName = aDbServerName;
  }


  /**
   * sets the database type that the database server, on which this agent runs on, supports
   *
   * @param aDbType PDMDatabaseType
   */
  public void setDbType(PDMDatabaseType aDbType)
  {
    this.dbType = aDbType;
  }


  /**
   * sets the file type that the database server, on which this agent runs on, supports. F.e. MSSQL
   * dump or Oracle Datapump
   *
   * @param aFileType PDMFileType
   */
  public void setFileType(PDMFileType aFileType)
  {
    this.fileType = aFileType;
  }


  /**
   * get type of agent
   *
   * @return type of agent
   */
  protected abstract PDMAgentType getAgentType();


  /**
   * gets behaviour of specified template
   *
   * @param  aTemplate MessageTemplate to get Behaviour of
   *
   * @return Behaviour
   */
  protected abstract Behaviour getBehaviour(MessageTemplate aTemplate);


  /**
   * setup the agent
   */
  @Override protected void setup()
  {
    super.setup();
    parseArguments();
    getContentManager().registerLanguage(PDMOntology.CODEC);
    getContentManager().registerOntology(PDMOntology.ONTOLOGY);

    MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST), MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

    PDMFileType fileType = getFileType();
    PDMFileType[] fileTypes;
    if (PDMFileType.DATAPUMP_AND_EXP.equals(fileType))
    {
      fileTypes = new PDMFileType[2];
      fileTypes[0] = PDMFileType.DATAPUMP;
      fileTypes[1] = PDMFileType.ORA_EXP;
    }
    else
    {
      fileTypes = new PDMFileType[1];
      fileTypes[0] = fileType;
    }
    JadeUtilities.registerAgent(this, getAgentType(), getDbType(), getDbServerName(), fileTypes);

    addBehaviour(getBehaviour(template));
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void takeDown()
  {
    if (LOG.isInfoEnabled())
    {
      LOG.info("DB Agent taken down (" + getAgentType().toString() + ").");
    }
    super.takeDown();
    JadeUtilities.deregisterAgent(this, getAgentType());
  }


  /**
   * Parse the arguments that can be specified when starting an agent.
   *
   * <ul>
   *   <li>db server code of database server as it is known in the Planon database</li>
   *   <li>db type database type (MSSQL, Oracle) that is supported on the database server the agent
   *     runs on</li>
   *   <li>file type type of dumpfile (MSSQL dump, Oracle datapump) that is supported on the
   *     database server the agent runs on</li>
   * </ul>
   */
  private void parseArguments()
  {
    Pattern dbTypePattern = Pattern.compile(PDMAgentProperties.PROP_DB_TYPE + "=([\\w\\-]+)");

    Pattern dbServerPattern = Pattern.compile(PDMAgentProperties.PROP_DB_SERVER + "=([\\w\\-]+)");

    Pattern fileTypePattern = Pattern.compile(PDMAgentProperties.PROP_FILE_TYPE + "=([\\w\\-]+)");

    Object[] args = getArguments();
    if ((args != null) && (args.length > 0))
    {
      for (int argumentIndex = 0; argumentIndex < args.length; argumentIndex++)
      {
        String argument = (String) args[argumentIndex];
        Matcher matcher = dbTypePattern.matcher(argument);
        if (matcher.find())
        {
          setDbType(PDMDatabaseType.getPDMDatabaseTypeByCode(matcher.group(1)));
        }
        matcher = dbServerPattern.matcher(argument);
        if (matcher.find())
        {
          setDbServerName(matcher.group(1));
        }
        matcher = fileTypePattern.matcher(argument);
        if (matcher.find())
        {
          setFileType(PDMFileType.getPDMFileTypeByCode(matcher.group(1)));
        }
      }
    }
  }
}
