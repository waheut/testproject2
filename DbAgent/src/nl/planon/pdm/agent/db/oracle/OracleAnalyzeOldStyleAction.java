// Planon Enterprise Edition Source file: OracleAnalyzeOldStyleAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import jade.content.*;
import jade.lang.acl.*;
import jade.util.leap.*;

import java.io.*;
import java.sql.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * analyzes an old style oracle db, OracleAnalyzeOldStyleAction detectes schemas
 */
public class OracleAnalyzeOldStyleAction extends OracleDBAgentAction<AnalyzeResponse>
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an oracle import Old style.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Download dump from webdav location to oracle shared directory</li>
   *   <li>Analyse dump on the Oracle DB server. This will result in an SQL script that contains the
   *     structure of the dump</li>
   *   <li>Retrieve the tablespace(s) and schema(s) from the table structure.</li>
   *   <li>Import the OldStyle dump file into the DB</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an OracleDataPumpRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    ACLMessage reply = aInput.createReply();
    OracleOldStyleAnalyzeRequest request = (OracleOldStyleAnalyzeRequest) aConcept;

    AnalyzeResponse response = getResponseData();
    response.fillMandatoryResponseData(request);
    Integer pkRequest = request.getPrimaryKeyRequest();
    try
    {
      IDatabaseDumpInformation dumpInformationRequest = request.getDumpInformation();

      String remoteDumpFileName = request.getDumpFileName();

      File localDumpFile = null;
      Connection dbConnection = null;
      //passing on the location
      Integer pdmLocation = request.getDatabaseLocation();
      response.setDatabaseLocation(pdmLocation);
      try
      {
        localDumpFile = retrieveFileFromWebDAV(remoteDumpFileName, getOracleSharedDirectory());
        dbConnection = getDBConnection(request.getDatabaseServerInformation());
        // if its true, its a oracle oldstyle dump
        if (isOldStyleDumpFileContainingPlanonDB(localDumpFile))
        {
          DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
          dumpInformation.setVersionNumber(getDumpDatabaseVersion(localDumpFile));
          reply.setPerformative(ACLMessage.INFORM);
        }
        else
        {
          reply.setPerformative(ACLMessage.FAILURE);
          throw getPDMFeedbackHandler().addException(ErrorDefinition.NOT_ORACLE_DMP_DUMPFILE, null);
        }
        response.setPrimaryKeyRequest(pkRequest);
      }
      catch (IOException ex)
      {
        //  reply.setPerformative(ACLMessage.FAILURE); // Action failed
        throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_IMPORTING_DUMPFILE_OLDSTYLE, ex);
      }
      finally
      {
        // PCIS 193113 / PCIS 193436:
        // Original dumpfile as well as analyze file and log file on database server (if created) must be deleted after import.
        deleteLocalFile(localDumpFile);

        if (dbConnection != null)
        {
          closeConnection(dbConnection);
        }
      }
    }
    catch (PDMException ex)
    {
      response.setPrimaryKeyRequest(pkRequest);
      // if its a admin error, the analyze is finish corectly and the import can start.
      if (ex.isErrorForAdministrator())
      {
        reply.setPerformative(ACLMessage.INFORM); // Action failed
      }
      else
      {
        reply.setPerformative(ACLMessage.FAILURE); // Action failed
      }
      throw ex;
    }

    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
    AbstractPDMAgent agent = getAgent();
    AnalyzeResponse response = getResponseData();
    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
    response.setDatabaseServerName(agent.getDbServerName());
    dumpInformation.setDatabaseType(agent.getDbType());
    dumpInformation.setFileType(PDMFileType.ORA_EXP);
    List list = new ArrayList(); // this is not a java.util.List because there is
                                 // no ontology for that (also no generics here)
    list.add(PDMFileType.ORA_EXP);
    response.setTriedFileType(list);
  }
  
  /**
   * creates new instance of AnalyzeResponse
   *
   * @return AnalyzeResponse
   */
  @Override protected AnalyzeResponse createResponse()
  {
    return new AnalyzeResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }
}
