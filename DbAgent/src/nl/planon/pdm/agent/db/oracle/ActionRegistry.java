// Planon Enterprise Edition Source file: ActionRegistry.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import nl.planon.pdm.core.datamessage.request.mssql.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.agent.db.mssql.*;

/**
 * You can register your Oracle DB Agent actions here.
 *
 * <p>The registry is in fact a Map that can be queried. Key is the Request class as the agent will
 * receive from the Controller Agent. Value is the 'action' that is to be performed. In the action,
 * the real work happens.</p>
 */
public class ActionRegistry extends DBAgentActionRegistry
{
  //~ Instance initializers ------------------------------------------------------------------------

  {
    // Register request and action for analyzing, importing and exporting Oracle datapump dumps
    this.register(OracleDataPumpAnalyzeRequest.class.getName(), OracleAnalyzeDataPumpAction.class);
    this.register(OracleDataPumpImportRequest.class.getName(), OracleImportDataPumpAction.class);
    this.register(OracleDataPumpExportRequest.class.getName(), OracleExportDataPumpAction.class);

    // Register request and action for analyzing, importing and exporting Oracle old style dumps
    this.register(OracleOldStyleAnalyzeRequest.class.getName(), OracleAnalyzeOldStyleAction.class);
    this.register(OracleOldStyleImportRequest.class.getName(), OracleImportOldStyleAction.class);
    this.register(OracleOldStyleExportRequest.class.getName(), OracleExportOldStyleAction.class);

    // Register request and action for deleting Oracle datapump/old style dumps
    this.register(OracleDeleteRequest.class.getName(), OracleDeleteAction.class);

    // Register request and action for analyzing, importing, exporting and deleting MSSQL dumps
    this.register(MSSQLAnalyzeRequest.class.getName(), MSSQLAnalyzeAction.class);
    this.register(MSSQLImportRequest.class.getName(), MSSQLImportAction.class);
    this.register(MSSQLExportRequest.class.getName(), MSSQLExportAction.class);
    this.register(MSSQLDeleteRequest.class.getName(), MSSQLDeleteAction.class);
  }
}
