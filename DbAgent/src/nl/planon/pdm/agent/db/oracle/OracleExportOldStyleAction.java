// Planon Enterprise Edition Source file: OracleExportOldStyleAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * OracleExportOldStyleAction
 */
public class OracleExportOldStyleAction extends OracleDBAgentAction<ExportResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String LOGFILE_POSTFIX = "_export";
  private static final String DUMPFILE_EXTENSION = ".dmp";

  private static final String ANALYZE_UNICODE = //
    "Select Count(*) " //-- 1: (N)CLOB exists; 0: (N)CLOB exists does not exists
    + " From DUAL " //
    + " Where exists (Select 'x'" //
    + "               from user_tab_columns" //
    + "               Where data_type in ('CLOB', 'NCLOB')" //
    + "              )";

  //~ Instance Variables ---------------------------------------------------------------------------

  private Integer pkPDMDatabase;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an export of oracle Old style.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Start exp.exe</li>
   *   <li>Export the DB to a datapump dump file</li>
   *   <li>Upload dumpfile and logfile from oracle shared directory to webdav location</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an OracleDataPumpRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    ExportResponse response = getResponseData();

    ACLMessage reply = aInput.createReply();

    OracleOldStyleExportRequest abstractRequest = (OracleOldStyleExportRequest) aConcept;
    response.fillMandatoryResponseData(abstractRequest);
    this.pkPDMDatabase = abstractRequest.getPrimaryKeyDatabase();

    String expLocation = abstractRequest.getDatabaseServerInformation().getExpLocation();
    String user = abstractRequest.getSchemaName();
    String password = abstractRequest.getDatabasePassword();
    String dbInstanceName = abstractRequest.getDatabaseServerInformation().getDatabaseInstanceName();

    String dumpFileName = createNameExportedDump(abstractRequest);
    String logfile = abstractRequest.getPrimaryKeyDatabase() + "_" + abstractRequest.getPrimaryKeyRequest() + "_" + abstractRequest.getPrimaryKeyTask() + getLogfilePostfix() + ".log";
    String dumpfileShareDir = getOracleSharedDirectory().toString();
    Scanner scan = null;
    try
    {
      if ((expLocation == null) || (expLocation.isEmpty()))
      {
        response.setDumpFileDir("No dumpfile available");
        response.setDumpFileName("No dumpfile available");
        throw getPDMFeedbackHandler().addException(ErrorDefinition.EXPORT_LOCATION_NOT_SPECIFIED, null);
      }

      //create executable argument
      String[] exportCommand = {expLocation, user + "/" + password + "@" + dbInstanceName + " file=" + dumpfileShareDir + "/" + dumpFileName + " log=" + dumpfileShareDir + "/" + logfile};
      getPDMFeedbackHandler().report("\tExport command: " + exportCommand[0] + " " + exportCommand[1]);
      if (!SKIP_DB_ACTIONS)
      {
        ProcessBuilder builder = new ProcessBuilder(exportCommand);

        builder.redirectErrorStream(true);

        Process process = builder.start();
        InputStream processInput = process.getInputStream();

        int taskTimeOut = getTaskTimeOut(abstractRequest);

        // create and start a timeout thread
        TimeOutThread timeout = new TimeOutThread(taskTimeOut, process);
        timeout.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(processInput));
        scan = new Scanner(reader);

        response.setDumpFileDir(dumpfileShareDir);
        response.setDumpFileName(dumpFileName);

        while (scan.hasNext())
        {
          String input = scan.nextLine();

          getPDMFeedbackHandler().report("  * Export step");
          String line = reader.readLine();
          // loop for write every output line
          while (line != null)
          {
            getPDMFeedbackHandler().report(line);
            line = reader.readLine();
          }
          // task is done
          if (line == null)
          {
            // stop timeout becouse action is successfully
            timeout.cancel();
            getPDMFeedbackHandler().report("    --> success, logfile: '" + logfile + "'!");
            reply.setPerformative(ACLMessage.INFORM);
            break;
          }
        }
        int exitCode = process.exitValue();
        if (exitCode != 0)
        {
          // if timeout = true, timout error
          if (timeout.isTimedOut())
          {
            //  getPDMFeedbackHandler().report("    --> failed with exitcode " + exitCode + ", logfile: '" + logfile + "'!");
            throw getPDMFeedbackHandler().addException(ErrorDefinition.TIMEOUT_ERROR_ON_EXECUTING_DBTASK, null);
          }
          else
          {
            getPDMFeedbackHandler().report("    --> failed with exitcode " + exitCode + ", logfile: '" + logfile + "'!");
            throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, null);
          }
        }
        deleteLocalFile(new File(getOracleSharedDirectory(), logfile));
      }
      else
      {
        String exportedDumpFileName = "ExportedDump.dmp";
        try
        {
          new File(getOracleSharedDirectory(), exportedDumpFileName).createNewFile();
        }
        catch (IOException ex)
        {
          throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, null);
        }
        response.setDumpFileName(exportedDumpFileName);
        getPDMFeedbackHandler().report(FEEDBACK_SIMULATING);
        reply.setPerformative(ACLMessage.INFORM);
      }
    }
    catch (IOException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE);
      throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, ex);
    }
    catch (PDMException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE);
      throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, ex.isErrorForAdministrator(), ex);
    }
    finally
    {
      if (scan != null)
      {
        scan.close();
      }
    }
    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aRequest DOCUMENT ME!
   *
   * @throws PDMException DOCUMENT ME!
   */
  @Override public void responseFilesToWebDAV(AbstractRequest aRequest) throws PDMException
  {
    ExportResponse response = getResponseData();

    String originalDumpFile = aRequest.getDumpFileName();
    String resultDumpFile = createNameExportedDump(aRequest);

    File localFile = new File(getOracleSharedDirectory(), resultDumpFile);
    if (localFile != null)
    {
      sendFileToWebDAVLocation(localFile, aRequest.getDumpFileDir(), resultDumpFile);
      deleteLocalFile(localFile);
    }

    response.setDumpFileDir(aRequest.getDumpFileDir());
    response.setDumpFileName(resultDumpFile);

    if (aRequest.getDumpFileName() != null)
    {
      deleteLocalFile(new File(getOracleSharedDirectory(), response.getDumpFileName()));
    }

    super.responseFilesToWebDAV(aRequest);
  }


  /**
   * creates new instance of ExportResponse
   *
   * @return ExportResponse
   */
  @Override protected ExportResponse createResponse()
  {
    return new ExportResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getDumpFileExtension()
  {
    return DUMPFILE_EXTENSION;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }


  /**
   * Instruct the DB to analyze the dump file and create an SQL file with the structure.
   *
   * @param  aConnection The DB connection
   *
   * @return The SQL file that contains the SQL commands to set up the structure of the DB.
   *
   * @throws PDMException When bad things happen.
   */
  private boolean isUnicode(Connection aConnection) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Analyse unicode step");

    if (!SKIP_DB_ACTIONS)
    {
      try
      {
        pdmFeedbackHandler.report("\tSQL: '" + ANALYZE_UNICODE + "'");
        statement = aConnection.prepareStatement(ANALYZE_UNICODE);
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next())
        {
          result = (resultSet.getInt(1) > 0);
        }
      }
      catch (SQLException ex)
      {
        exception = pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE, ex);
        throw exception;
      }
      finally
      {
        closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE);
      }
    }
    pdmFeedbackHandler.report(FEEDBACK_SUCCESS + "db contains " + (result ? "" : "no ") + "unicode columns");
    return result;
  }
}
