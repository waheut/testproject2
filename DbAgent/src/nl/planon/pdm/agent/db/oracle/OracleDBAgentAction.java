// Planon Enterprise Edition Source file: OracleDBAgentAction.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * Provides an abstract base class for DBAgent actions that have Oracle interaction.
 *
 * <p>When creating an oracle DBAgent action, this is the class to extend from. It contains a few
 * helper methods to make life a little easier for your day-to-day use of oracle :) .</p>
 */
public abstract class OracleDBAgentAction<RESPONSE extends AbstractResponse> extends AbstractDBAgentAction<RESPONSE>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String COLUMN_MACHINE = "MACHINE";
  private static final String TOKEN_DATAFILE = "$DATAFILE$";
  private static final String TOKEN_PASSWORD = "$PASSWORD$";
  private static final String TOKEN_TABLESPACENAME = "$TBLSPC$";
  private static final String TOKEN_USERNAME = "$USER$";
  private static final String TABLE_ACT = "TABLE \"ACT\"";
  protected static final String EXPORT_V = "EXPORT:V";

  protected static final String SUFFIX_DATABASE = "dbf";
  protected static final String SCHEMA_NAME_SEPARATOR = ",";
  protected static final String TABLESPACE_NAME_SEPARATOR = ",";

  protected static final Set<String> SYSTEM_SCHEMA_NAMES = new HashSet<String>();

  static
  {
    SYSTEM_SCHEMA_NAMES.add("SYS");
    SYSTEM_SCHEMA_NAMES.add("SYSTEM");
  }

  private static final String[] CREATE_TABLESPACE_AND_USER_SQL = new String[]
  { //
    "CREATE BIGFILE TABLESPACE " + TOKEN_TABLESPACENAME + " DATAFILE '" + TOKEN_DATAFILE + "' SIZE 300000K REUSE AUTOEXTEND ON NEXT 100000K", //
    "CREATE USER " + TOKEN_USERNAME + " IDENTIFIED BY " + TOKEN_PASSWORD + " DEFAULT TABLESPACE " + TOKEN_TABLESPACENAME + " QUOTA UNLIMITED ON " + TOKEN_TABLESPACENAME + " TEMPORARY TABLESPACE TEMP", //
    "GRANT SELECT ON SYS.V_$SESSION TO " + TOKEN_USERNAME // These rights differ from the above rights. This is applicable only to PDM3 databases.
  };

  private static final String[] STANDARD_TYPE_PRIVILIGES = new String[]
  { //
    "GRANT ALTER SESSION TO " + TOKEN_USERNAME, //
    "GRANT CREATE SESSION TO " + TOKEN_USERNAME, //
  };

  private static final String[] UPGRADE_TYPE_PRIVILEGES = new String[]
  { //
    "GRANT CREATE PROCEDURE TO " + TOKEN_USERNAME, //
    "GRANT CREATE TABLE TO " + TOKEN_USERNAME, //
    "GRANT CREATE TRIGGER TO " + TOKEN_USERNAME, //
    "GRANT CREATE VIEW TO " + TOKEN_USERNAME, //
    "GRANT QUERY REWRITE TO " + TOKEN_USERNAME, //
    "GRANT CREATE SEQUENCE TO " + TOKEN_USERNAME, //
  }; // Additional to these, STANDARD_TYPE_PRIVILIGES should also be part of this.

  private static final String[] ADMIN_TYPE_PRIVILEGES = new String[]
  { //
    "GRANT CREATE CLUSTER TO " + TOKEN_USERNAME, //
    "GRANT CREATE DATABASE LINK TO " + TOKEN_USERNAME, //
    "GRANT CREATE SYNONYM TO " + TOKEN_USERNAME, //
  }; // Additional to these, UPGRADE_TYPE_PRIVILEGES and STANDARD_TYPE_PRIVILIGES should also be part of this.

  // The file location property of the oracle shared directory.
  private static final String ORACLE_DIRECTORY_PROPERTY = "OracleSharedDirectory";

  // This is the default identifier for the shared oracle data directory.
  private static final String ORACLE_SHARED_DIR_IDENTIFIER_DEFAULT = "DATA_PUMP_DIR";
  // Use -DOracleSharedDirectoryIdentifier=MyID to set the identifier.
  private static final String ORACLE_SHARED_DIR_IDENTIFIER_PROPERTY = "OracleSharedDirectoryIdentifier";

  private static final String SQL_TO_DROP_USER = "DROP USER " + TOKEN_USERNAME + " CASCADE"; // drop user and all objects in the user's schema
  private static final String SQL_TO_DROP_TABLESPACE = "DROP TABLESPACE " + TOKEN_TABLESPACENAME + " INCLUDING CONTENTS AND DATAFILES"; // drop tablespace and all associated operating system datafiles
  private static final String SQL_GET_CONNECTED_USERS = "select MACHINE"
  + " FROM  v$session"
  + " WHERE  username = '" + TOKEN_USERNAME + "'"
  + " ORDER BY status DESC"
  + " , last_call_et DESC";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleDBAgentAction object.
   */
  public OracleDBAgentAction()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * create schema and table space
   *
   * @param  aDBConnection     db connection to use
   * @param  aTablespace       table space to use
   * @param  aUserName         user name to use
   * @param  aPassword         password of user
   * @param  aDatafile         db Datafile!
   * @param  aPdmPrivilegeType
   *
   * @throws PDMException When bad things happen.
   */
  protected void createSchemaAndTablespace(Connection aDBConnection, String aTablespace, String aUserName, String aPassword, File aDatafile, PDMPrivilegeType aPdmPrivilegeType) throws PDMException
  {
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Create schema and tablespace step");

    if (!SKIP_DB_ACTIONS)
    {
      assert aDatafile != null : "datafile must be specified";
      String dataFileName = aDatafile.getAbsolutePath().replaceAll(escape("\\"), escape("\\\\"));
      int sqlCounter = 0;
      boolean tableSpaceCreated = false;
      boolean tableSchemaCreated = false;

      String[] concatenatedSQL = getConcatenatedSQLStatement(aPdmPrivilegeType);
      for (String createSQLToExecute : concatenatedSQL)
      {
        sqlCounter++;
        // PCIS 195768: before replacing, the special characters (for example '$') in the replacement string must be escaped
        String createSQLToExecuteWithParams = createSQLToExecute.replaceAll(escape(TOKEN_TABLESPACENAME), escape(aTablespace));
        createSQLToExecuteWithParams = createSQLToExecuteWithParams.replaceAll(escape(TOKEN_USERNAME), escape(aUserName));
        createSQLToExecuteWithParams = createSQLToExecuteWithParams.replaceAll(escape(TOKEN_PASSWORD), escape(aPassword));
        createSQLToExecuteWithParams = createSQLToExecuteWithParams.replaceAll(escape(TOKEN_DATAFILE), escape(dataFileName));
        pdmFeedbackHandler.report("\tSQL: '" + createSQLToExecuteWithParams + "'");
        PreparedStatement pstmtTablespace;
        try
        {
          pstmtTablespace = aDBConnection.prepareStatement(createSQLToExecuteWithParams);
          pstmtTablespace.execute();
          switch (sqlCounter)
          {
            case 1 :

              tableSpaceCreated = true;
              break;
            case 2 :

              tableSchemaCreated = true;
              break;
            default :
              break;
          }
        }
        catch (SQLException ex)
        {
          // PCIS 207241: if the exception is raised after the schema and tablespace is created, then these have to be deleted again.
          // Previously, this was done in the calling methods but in some cases, it is not cleaning up the created table spaces thus lying on the database server as orphans.
          // Hence, solving the issue: JIRA: TEC-634
          if (tableSchemaCreated)
          {
            dropUser(aDBConnection, aUserName);
          }
          if (tableSpaceCreated)
          {
            dropTableSpace(aDBConnection, aTablespace);
          }
          throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE, ex);
        }
      }
    }
    else
    {
      pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
    }
    pdmFeedbackHandler.report(FEEDBACK_SUCCESS);
  }


  /**
   * drop schema and table space including contents
   *
   * @param  aDBConnection   db connection to use
   * @param  aSchemaName     name of schema to delete
   * @param  aTableSpaceName name of table space to delete
   *
   * @throws PDMException
   */
  protected void dropSchemaAndTablespace(Connection aDBConnection, String aSchemaName, String aTableSpaceName) throws PDMException
  {
    dropUser(aDBConnection, aSchemaName);
    dropTableSpace(aDBConnection, aTableSpaceName);
  }


  /**
   * drop table space including contents
   *
   * @param  aDBConnection   db connection to use
   * @param  aTableSpaceName name of table space to delete
   *
   * @throws PDMException
   */
  protected void dropTableSpace(Connection aDBConnection, String aTableSpaceName) throws PDMException
  {
    getPDMFeedbackHandler().report("  * Drop tablespace step");

    String createSQLToExecuteWithParams = SQL_TO_DROP_TABLESPACE.replaceAll(escape(TOKEN_TABLESPACENAME), escape(aTableSpaceName));
    getPDMFeedbackHandler().report("\tSQL: '" + createSQLToExecuteWithParams + "'");
    if (!SKIP_DB_ACTIONS)
    {
      PreparedStatement pstmtDropTableSpace;
      try
      {
        pstmtDropTableSpace = aDBConnection.prepareStatement(createSQLToExecuteWithParams);
        pstmtDropTableSpace.execute();
      }
      catch (SQLException ex)
      {
        throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      }
      getPDMFeedbackHandler().report(FEEDBACK_SUCCESS);
    }
    else
    {
      getPDMFeedbackHandler().report(FEEDBACK_SIMULATING);
    }
  }


  /**
   * drop user cascading
   *
   * @param  aDBConnection db connection to use
   * @param  aUserName     name of user to delete
   *
   * @throws PDMException
   */
  protected void dropUser(Connection aDBConnection, String aUserName) throws PDMException
  {
    getPDMFeedbackHandler().report("  * Drop user step");

    String createSQLToExecuteWithParams = SQL_TO_DROP_USER.replaceAll(escape(TOKEN_USERNAME), escape(aUserName));
    getPDMFeedbackHandler().report("\tSQL: '" + createSQLToExecuteWithParams + "'");
    if (!SKIP_DB_ACTIONS)
    {
      PreparedStatement pstmtDropUser;
      try
      {
        pstmtDropUser = aDBConnection.prepareStatement(createSQLToExecuteWithParams);
        pstmtDropUser.execute();
      }
      catch (SQLException ex)
      {
        connectedUsers(aDBConnection, aUserName);
        throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      }
      getPDMFeedbackHandler().report(FEEDBACK_SUCCESS);
    }
    else
    {
      getPDMFeedbackHandler().report(FEEDBACK_SIMULATING);
    }
  }


  /**
   * get the dump version.
   *
   * @param  aDumpFile dump file
   *
   * @return database version 2 decimal behind the comma
   *
   * @throws IOException
   */
  protected String getDumpDatabaseVersion(File aDumpFile) throws IOException
  {
    String databaseVersion = null;
    BufferedReader bf = new BufferedReader(new FileReader(aDumpFile));
    try
    {
      String line;
      int lineCount = 0;
      while ((line = bf.readLine()) != null)
      {
        // Increment the count and find the index of the word
        lineCount++;
        int indexfound = line.indexOf(EXPORT_V);
        if (indexfound > -1)
        {
          indexfound += EXPORT_V.length();

          databaseVersion = line.substring(indexfound);

          // version xx.xx.xx.xx.xx
          databaseVersion = formatTenDigitVersionNumber(databaseVersion);

          getPDMFeedbackHandler().report("Dump version number : " + databaseVersion);
        }
        if (lineCount >= 2)
        {
          break;
        }
      }
    }
    finally
    {
      bf.close();
    }

    return databaseVersion;
  }


  /**
   * Returns a File object that points to the shared oracle data directory.
   *
   * <p>The oracle data directory can be passed in the system property, named as specified in the
   * constant ORACLE_DIRECTORY_PROPERTY.</p>
   *
   * <p>Example:</p>
   *
   * <pre>
     -DOracleSharedDirectory=C:\oraclePE\directory
   * </pre>
   *
   * <p>If the property is not set, the directory does not exist, or the file does not denote a
   * directory, a RuntimeException will be thrown.</p>
   *
   * @return A file object pointing to the directory that is used to share files with Oracle.
   *
   * @throws RuntimeException When the user has made a configureation mistake.
   */
  protected File getOracleSharedDirectory()
  {
    String oracleSharedDirectory = System.getProperty(ORACLE_DIRECTORY_PROPERTY);
    if ((oracleSharedDirectory == null) || (oracleSharedDirectory.trim().length() == 0))
    {
      throw new RuntimeException("Oracle data share directory should be passed with -D" + ORACLE_DIRECTORY_PROPERTY);
    }
    File result = new File(oracleSharedDirectory);
    if (!result.exists() || !result.isDirectory())
    {
      throw new RuntimeException("Oracle shared directory passed in -D" + ORACLE_DIRECTORY_PROPERTY + " with value '" + oracleSharedDirectory + "' should exist as a directory");
    }
    return result;
  }


  /**
   * get identifier that holds Shared directory of Oracle via system properties. Use default value
   * if not defined.
   *
   * @return identifier that holds Shared directory of Oracle
   */
  protected String getOracleSharedDirectoryIdentifier()
  {
    return System.getProperty(ORACLE_SHARED_DIR_IDENTIFIER_PROPERTY, ORACLE_SHARED_DIR_IDENTIFIER_DEFAULT);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getTablePrefix(String aDatabaseSchemaName)
  {
    return aDatabaseSchemaName;
  }


  /**
   * search if its a oldstyle dump
   *
   * @param  aDumpfile
   *
   * @return true if its a oldstyle dump, false otherwise
   *
   * @throws IOException
   */
  protected boolean isOldStyleDumpFileContainingPlanonDB(File aDumpfile) throws IOException
  {
    BufferedReader bf = new BufferedReader(new FileReader(aDumpfile));
    // Start a line count and declare a string to hold our current line.
    boolean exportV = false;
    boolean tableAct = false;
    try
    {
      int linecount = 0;
      String line;
      // Loop through the first line for searching for EXPORT:V
      while ((line = bf.readLine()) != null)
      {
        // Increment the count and find the index of the word
        linecount++;
        exportV |= line.indexOf(EXPORT_V) > -1;
        tableAct |= line.indexOf(TABLE_ACT) > -1;

        if (tableAct && exportV)
        {
          break;
        }
      }
    }
    finally
    {
      // Close the file after done searching
      bf.close();
    }

    // If greater than -1, means we found the word
    if (exportV && tableAct)
    {
      return true;
    }
    else if (!tableAct)
    {
      getPDMFeedbackHandler().report("Probably not a Planon database");
      return false;
    }
    else
    {
      return false;
    }
  }


  /**
   * Concatenate the string arrays.
   *
   * @param  aFirstSQL first SQL obviously to create schema and tablespace
   * @param  aRestSQLs rest  Next SQLs to grant the privileges based on the type. U-being Upgrade
   *                   type privileges and S-being Standard type privileges
   *
   * @return
   */
  private <T> T[] concatenateSQL(T[] aFirstSQL, T[]... aRestSQLs)
  {
    int totalLength = aFirstSQL.length;
    for (T[] array : aRestSQLs)
    {
      totalLength += array.length;
    }
    T[] result = Arrays.copyOf(aFirstSQL, totalLength);
    int offset = aFirstSQL.length;
    for (T[] array : aRestSQLs)
    {
      System.arraycopy(array, 0, result, offset, array.length);
      offset += array.length;
    }

    return result;
  }


  /**
   * executes a select statement to determine if the user exists in the database
   *
   * @param  aConnection database connection
   * @param  aUserName   name of user to look for
   *
   * @return true if user exists in database, false otherwise
   *
   * @throws PDMException
   */
  private boolean connectedUsers(Connection aConnection, String aUserName) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;

    // get connecting users.
    String sql = SQL_GET_CONNECTED_USERS.replaceAll(escape(TOKEN_USERNAME), escape(aUserName));
    try
    {
      statement = aConnection.prepareStatement(sql);
      ResultSet resultSet = statement.executeQuery();

      if (resultSet.next())
      {
        getPDMFeedbackHandler().report("Users still connected:");
        do
        {
          getPDMFeedbackHandler().report("-----> " + resultSet.getString(COLUMN_MACHINE));
          result = true;
        }
        while (resultSet.next());
      }
    }
    catch (SQLException ex)
    {
      exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
    }

    return result;
  }


  /**
   * escapes string to be used in replaceAll
   *
   * @param  aValue value
   *
   * @return escaped string
   */
  private String escape(String aValue)
  {
    return Matcher.quoteReplacement(aValue);
  }


  /**
   * Gets the SQL string array
   *
   * @param  aPDMPrivilegeType Type of the privilege
   *
   * @return String array of SQL statements which are based on the privilege.
   */
  private String[] getConcatenatedSQLStatement(PDMPrivilegeType aPDMPrivilegeType)
  {
    String[] result = null;
    switch (aPDMPrivilegeType.getCode())
    {
      case "A" :

        result = concatenateSQL(CREATE_TABLESPACE_AND_USER_SQL, STANDARD_TYPE_PRIVILIGES, UPGRADE_TYPE_PRIVILEGES, ADMIN_TYPE_PRIVILEGES);
        break;

      case "U" :

        result = concatenateSQL(CREATE_TABLESPACE_AND_USER_SQL, STANDARD_TYPE_PRIVILIGES, UPGRADE_TYPE_PRIVILEGES);
        break;

      case "S" :

        result = concatenateSQL(CREATE_TABLESPACE_AND_USER_SQL, STANDARD_TYPE_PRIVILIGES);
        break;

      default :

        assert false : "Privilege Type: " + aPDMPrivilegeType.getCode() + " never exists!!";
        break;
    }
    return result;
  }
}
