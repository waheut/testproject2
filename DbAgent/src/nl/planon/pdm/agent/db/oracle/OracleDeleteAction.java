// Planon Enterprise Edition Source file: OracleDeleteAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.sql.*;
import java.util.regex.*;

import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * OracleDeleteAction
 */
public class OracleDeleteAction extends OracleDBAgentAction<DeleteResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String LOGFILE_POSTFIX = "_delete";

  private static final String SQL_CHECK_EXISTANCE_USERNAME = "SELECT USERNAME FROM DBA_USERS WHERE USERNAME = (?)";
  private static final String SQL_CHECK_IF_USER_CONNECTED = "SELECT COUNT(*) FROM sys.v_$session WHERE USERNAME = (?)";
  private static final String SQL_CHECK_EXISTANCE_TABLESPACENAME = "SELECT TABLESPACE_NAME FROM DBA_TABLESPACES WHERE TABLESPACE_NAME = (?)";

  private static final String SQL_TO_DROP_USER = "DROP USER $USER$ CASCADE"; // drop user and all objects in the user's schema
  private static final String SQL_TO_DROP_TABLESPACE = "DROP TABLESPACE $TBLSPC$ INCLUDING CONTENTS AND DATAFILES"; // drop tablespace and all associated operating system datafiles

  private static final String SQL_GET_CONNECTED_USERS = "select MACHINE"
  + " from  v$session"
  + " where  username = '$USERNAME$'"
  + " order by status desc"
  + " , last_call_et desc";

  //~ Instance Variables ---------------------------------------------------------------------------

  private final DeleteResponse deleteResponse = new DeleteResponse();

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to delete an oracle database.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Remove database user and remove the user's objects</li>
   *   <li>Remove table space from the database</li>
   *   <li>Remove all files related to this specific database</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an OracleDeleteRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    ACLMessage reply = aInput.createReply();
    try
    {
      OracleDeleteRequest request = (OracleDeleteRequest) aConcept;
      this.deleteResponse.fillMandatoryResponseData(request);
      String userName = request.getSchemaName();
      String tableSpaceName = request.getTableSpaceName();

      // Strip absolute path, we need the file name later.
      File localDumpFile = null;
      String logFileName = null;
      Connection dbConnection = null;
      try
      {
        getPDMFeedbackHandler().report("Starting deletion of database with username: '" + userName + "' and tablespace '" + tableSpaceName + "'...");

        // Now, the real work starts
        dbConnection = getDBConnection(request.getDatabaseServerInformation());
        if (!SKIP_DB_ACTIONS)
        {
          if (!userConnected(dbConnection, userName))
          {
            if (userExists(dbConnection, userName))
            {
              dropUser(dbConnection, userName);
            }
            if (tableSpaceExists(dbConnection, tableSpaceName))
            {
              dropTableSpace(dbConnection, tableSpaceName);
            }
            getPDMFeedbackHandler().report("Database: " + userName + " deleted succesfully!");
          }
          else
          {
            throw getPDMFeedbackHandler().addException(ErrorDefinition.USER_STILL_CONNECTED, null, userName);
          }
        }
      }
      finally
      {
        // PCIS 193113 / PCIS 193436:
        // Original dumpfile as well as analyze file and log file on database
        // server (if created) must be deleted after import.
        deleteLocalFile(localDumpFile);
        if (logFileName != null)
        {
          deleteLocalFile(new File(getOracleSharedDirectory(), logFileName));
        }

        if (dbConnection != null)
        {
          closeConnection(dbConnection);
        }
      }
      reply.setPerformative(ACLMessage.INFORM); // Action successfully executed
    }
    catch (PDMException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE); // Action failed
      throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DELETING_DATABASE, ex.isErrorForAdministrator(), ex);
    }
    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
  }

  /**
   * creates delete response
   *
   * @return DeleteResponse
   */
  @Override protected DeleteResponse createResponse()
  {
    return new DeleteResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }


  /**
   * executes a select statement to determine if the tablespace exists in the database
   *
   * @param  aConnection database connection
   * @param  aTableSpace name of tablespace to look for
   *
   * @return true if tablespace exists in database, false otherwise
   *
   * @throws PDMException
   */
  private boolean tableSpaceExists(Connection aConnection, String aTableSpace) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;
    try
    {
      statement = aConnection.prepareStatement(SQL_CHECK_EXISTANCE_TABLESPACENAME, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
      statement.setString(1, aTableSpace);
      ResultSet resultSet = statement.executeQuery();

      result = resultSet.next();
    }
    catch (SQLException ex)
    {
      exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
    }

    return result;
  }


  /**
   * executes a select statement to determine if the user exists in the database
   *
   * @param  aConnection database connection
   * @param  aUserName   name of user to look for
   *
   * @return true if user exists in database, false otherwise
   *
   * @throws PDMException
   */
  private boolean userConnected(Connection aConnection, String aUserName) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;

    // get connecting users.
    String createSQLToExecuteWithParams = SQL_GET_CONNECTED_USERS.replaceAll("\\$USERNAME\\$", Matcher.quoteReplacement(aUserName));
    try
    {
      statement = aConnection.prepareStatement(createSQLToExecuteWithParams);
      ResultSet resultSet = statement.executeQuery();
      if (resultSet.next())
      {
        getPDMFeedbackHandler().report("Users still connected:");
        do
        {
          getPDMFeedbackHandler().report("-----> " + resultSet.getString("MACHINE"));
          result = true;
        }
        while (resultSet.next());
      }
    }
    catch (SQLException ex)
    {
      exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
    }
    return result;
  }


  /**
   * executes a select statement to determine if the user exists in the database
   *
   * @param  aConnection database connection
   * @param  aUserName   name of user to look for
   *
   * @return true if user exists in database, false otherwise
   *
   * @throws PDMException
   */
  private boolean userExists(Connection aConnection, String aUserName) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;
    try
    {
      statement = aConnection.prepareStatement(SQL_CHECK_EXISTANCE_USERNAME, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
      statement.setString(1, aUserName);
      ResultSet resultSet = statement.executeQuery();

      result = resultSet.next();
    }
    catch (SQLException ex)
    {
      exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
    }
    return result;
  }
}
