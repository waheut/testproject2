// Planon Enterprise Edition Source file: OracleImportDataPumpAction.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * Provides an implementation for the oracle datapump import.
 *
 * <p>This DBAgentAction implements the Oracle Datapump Import Action. It will be able to receive an
 * OracleDataPumpRequest message, with a few values set. It imports a dumpfile into a DB</p>
 */
public class OracleImportDataPumpAction extends OracleDBAgentAction<ImportResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String ANALYSE_STATEMENT = //
    "{call PLN_DataPumpInterface.PDM_DATAPUMP_DUMPFILEINFO(" //
    + "p_DumpFileName => ?, " //
    + "p_Directory => ?, " //
    + "p_FullAnalyse => ?, " //   0 limited (default) , 1= extended, see enum DataPumpAnalyze
    + "p_SQLFileName => ?, " //
    + "p_LogFileName => ?, " //
    + "p_ExitCode => ?)}";
  private static final String PROCEDURE_NAME_ANALYSE_DUMPFILE = "PLN_DataPumpInterface.AnalyzeDumpFile";
  private static final String ANALYZE_DUMPFILE = //
    "{call " + PROCEDURE_NAME_ANALYSE_DUMPFILE + "(" //
    + "p_Directory => ?, " //
    + "p_DumpFileName => ?, " //
    + "p_FileType => ?, " //
    + "p_DatabaseVersion => ?)}";

  private static final String IMPORT_DUMPFILE_HEAD = //
    "{call PLN_DataPumpInterface.PDM_DATAPUMP_IMPORT(" //
    + "p_DumpFileName => ?, " //
    + "p_Directory => ?, " //
    + "p_OldSchemaName => ?, " //
    + "p_NewSchemaName => ?, " //
    + "p_OldTableSpaceName => ?, " //
    + "p_NewTableSpaceName => ?, " //
    + "p_LogFileName => ?, " //
    + "p_ExitCode => ?, " //
    + "p_TerminateOnError => ?";
  private static final String IMPORT_DUMPFILE_TIMEOUT = ", p_TimeoutPeriod => ?";
  private static final String IMPORT_DUMPFILE_TAIL = ")}";

  private static final String ANALYZE_UNICODE = //
    "Select Count(*) " //-- 1: (N)CLOB exists; 0: (N)CLOB exists does not exists
    + " From DUAL " //
    + " Where exists (Select 'x'" //
    + "               from user_tab_columns" //
    + "               Where data_type in ('CLOB', 'NCLOB')" //
    + "              )";

  // PCIS 207696: a list of schema names and tablespace names that need to be ignored.
  private static final Set<String> SYSTEM_TABLESPACE_NAMES = new HashSet<String>();
  private static final Set<String> SYSTEM_TABLESPACE_NAMES_PREFIXES = new HashSet<String>();

  static
  {
    SYSTEM_TABLESPACE_NAMES.add("SYSTEM");
    SYSTEM_TABLESPACE_NAMES.add("SYSAUX");

    SYSTEM_TABLESPACE_NAMES_PREFIXES.add("UNDO");
    SYSTEM_TABLESPACE_NAMES_PREFIXES.add("TEMP");
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an oracle import data pump.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Download dump from webdav location to oracle shared directory</li>
   *   <li>Analyse dump on the Oracle DB server. This will result in an SQL script that contains the
   *     structure of the dump</li>
   *   <li>Retrieve the tablespace(s) and schema(s) from the table structure.</li>
   *   <li>Import the datapump dump file into the DB</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an OracleDataPumpRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    boolean schemaAndTablespaceCreated = false;
    boolean databaseImported = false;

    ImportResponse response = getResponseData();
    ACLMessage reply = aInput.createReply();
    try
    {
      OracleDataPumpImportRequest request = (OracleDataPumpImportRequest) aConcept;
      IDatabaseDumpInformation dumpInformationRequest = request.getDumpInformation();
      response.fillMandatoryResponseData(request);
      String remoteDumpFileName = request.getDumpFileName();
      String databasePassword = request.getDatabasePassword();
      boolean noImport = request.getNoImport();
      String targetSchema = request.getSchemaName();
      PDMPrivilegeType pdmPrivilegeType = request.getPDMPrivilegeType();
      String targetTablespace = request.getTableSpaceName();
      String dumpSchemaName = dumpInformationRequest.getSchemaName();
      String dumpTableSpaces = dumpInformationRequest.getTableSpaceNames();
      File datafileLocation = getDataFileLocationWithMaxFreeSpace(request.getDatabaseServerInformation().getDataFileLocations()); // PCIS 193113: set datafilelocation for creating tablespace

      // Strip absolute path, we need the file name later.
      String dumpFileName = new File(remoteDumpFileName).getName();
      File localDumpFile = null;
      String logFileName = null;
      Connection dbConnection = null;
      try
      {
        getPDMFeedbackHandler().report("Starting import of: '" + remoteDumpFileName + "' (file name '" + dumpFileName + "')...");

        // Now, the real work starts

        localDumpFile = retrieveFileFromWebDAV(remoteDumpFileName, getOracleSharedDirectory());
        dbConnection = getDBConnection(request.getDatabaseServerInformation());

        if ((dumpSchemaName == null) || (dumpTableSpaces == null))
        {
          // Figure out what the old schema name and tablespace are, by analyzing the dump.
          AnalysisResult analysis = analyseDB(localDumpFile, dbConnection);
          dumpTableSpaces = analysis.getTableSpaceNames(TABLESPACE_NAME_SEPARATOR);
          dumpSchemaName = analysis.getSchemaNames(SCHEMA_NAME_SEPARATOR);
        }
        // do not fill the schemaname and tablespace name in case the request is a reload export
        // (so only in case the request is a new import or a reload initial).
        if (!PDMRequestType.RELOAD_EXPORT.equals(request.getPDMRequestType()))
        {
          DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
          dumpInformation.setSchemaName(dumpSchemaName);
          dumpInformation.setTableSpaceNames(dumpTableSpaces);
        }

        // PCIS 205844: if multiple schema names found in dump, stop importing, report back that the user first
        // has to choose the right schema name
        if (dumpSchemaName.contains(SCHEMA_NAME_SEPARATOR))
        {
          throw getPDMFeedbackHandler().addException(ErrorDefinition.MULTIPLE_SCHEMAS_FOUND_IN_DUMP, null);
        }
        String dataFileName = changeFileExtension(dumpFileName, SUFFIX_DATABASE, "");

        createSchemaAndTablespace(dbConnection, targetTablespace, targetSchema, databasePassword, new File(datafileLocation, dataFileName), pdmPrivilegeType);
        schemaAndTablespaceCreated = true;
        // if no import is true. create only schema and tablespace, skip the import.
        if (noImport)
        {
          getPDMFeedbackHandler().report("Load of dump " + localDumpFile.getAbsolutePath() + " skipped as specified!");
        }
        else
        {
          databaseImported = importDumpOnDB(dbConnection, localDumpFile, getOracleSharedDirectoryIdentifier(), request, dumpSchemaName, dumpTableSpaces);
          setPlanonDBVersionDetailsTODumpInfo(response, dbConnection, targetSchema);
          getPDMFeedbackHandler().report("Dump " + localDumpFile.getAbsolutePath() + " loaded succesfully!");
        }
      }
      finally
      {
        // PCIS 207241: if the exception is raised after the schema and tablespace is created, then these have to be deleted again.
        // Also TEC-2714, PDM3: reloading exported database don't drop user and tablespace if it fails to load in oracle
        if (schemaAndTablespaceCreated && !databaseImported && !noImport)
        {
          getPDMFeedbackHandler().report("The schema and the tablespace were created successfully but the db import is not succesful.");
          getPDMFeedbackHandler().report("Hence, dropping the schema and tablespaces...");
          dropSchemaAndTablespace(dbConnection, request.getSchemaName(), targetTablespace);
          getPDMFeedbackHandler().report("The schema and tablespaces were dropped successfully.");
        }
        // PCIS 193113 / PCIS 193436:
        // Original dumpfile as well as analyze file and log file on database server (if created) must be deleted after import.
        deleteLocalFile(localDumpFile);

        if (dbConnection != null)
        {
          closeConnection(dbConnection);
        }
      }
      reply.setPerformative(ACLMessage.INFORM); // Action successfully executed
    }
    catch (PDMException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE); // Action failed
      throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_IMPORTING_DUMPFILE, ex.isErrorForAdministrator(), ex);
    }
    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
    AbstractPDMAgent agent = getAgent();
    ImportResponse response = getResponseData();

    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
    assert (PDMFileType.DATAPUMP.equals(agent.getFileType()) || PDMFileType.DATAPUMP_AND_EXP.equals(agent.getFileType())) : "File type must contain oracle data dump";
    response.setDatabaseServerName(agent.getDbServerName());
    dumpInformation.setDatabaseType(agent.getDbType());
    dumpInformation.setFileType(PDMFileType.DATAPUMP);
  }


  /**
   * creates new instance of ImportResponse
   *
   * @return ImportResponse
   */
  @Override protected ImportResponse createResponse()
  {
    return new ImportResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }


  /**
   * analyse db and get results
   *
   * @param  aLocalDumpFile local dump file
   * @param  aDbConnection  db connection
   *
   * @return AnalysisResult
   *
   * @throws PDMException
   */
  private AnalysisResult analyseDB(File aLocalDumpFile, Connection aDbConnection) throws PDMException
  {
    AnalysisResult analysis;
    List<File> filesToDelete = new ArrayList<File>(); // because log is easier to read if cleanup is at end...
    try
    {
      String sharedDirectory = getOracleSharedDirectoryIdentifier();

      File sqlFile = constructSQLFileWithAnalyseDumpOnDB(aDbConnection, aLocalDumpFile, sharedDirectory, DataPumpAnalyze.LIMITED);
      filesToDelete.add(sqlFile);
      analysis = parseAnalyseSQL(sqlFile);
      if (!analysis.isSchemaFound() || !analysis.isTableSpacesFound())
      {
        // second try, but now extended analysis!
        sqlFile = constructSQLFileWithAnalyseDumpOnDB(aDbConnection, aLocalDumpFile, sharedDirectory, DataPumpAnalyze.EXTENDED);
        filesToDelete.add(sqlFile);
        analysis = parseAnalyseSQL(sqlFile);
      }
      PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
      if (!analysis.isTableSpacesFound())
      {
        throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_PARSING_NO_TABLESPACE_FOUND, null);
      }
      if (!analysis.isSchemaFound())
      {
        throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_PARSING_NO_SCHEMA_FOUND, null);
      }
    }
    finally
    {
      for (File file : filesToDelete)
      {
        deleteLocalFile(file);
      }
    }
    return analysis;
  }


  /**
   * Instruct the DB to analyze the dump file and create an SQL file with the structure.
   *
   * @param  aDBConnection    The DB connection
   * @param  aLocalDumpFile   The dump file in the shared oracle directory.
   * @param  aDumpDirectory   The name of the directory, as oracle knows it.
   * @param  aDataPumpAnalyze indicates whether limited (quick) or extended (slower) analysis has to
   *                          be done
   *
   * @return The SQL file that contains the SQL commands to set up the structure of the DB.
   *
   * @throws PDMException When bad things happen.
   */
  private File constructSQLFileWithAnalyseDumpOnDB(Connection aDBConnection, File aLocalDumpFile, String aDumpDirectory, DataPumpAnalyze aDataPumpAnalyze) throws PDMException
  {
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Analyse step");

    String sqlFileName = null;
    String logFileName = null;
    String databaseVersion = null;
    if (!SKIP_DB_ACTIONS)
    {
      try
      {
        pdmFeedbackHandler.report("\tSQL: '" + ANALYSE_STATEMENT + "'");
        pdmFeedbackHandler.report("\t\tInput parameters: ");
        pdmFeedbackHandler.report("\t\t\t 1: " + aLocalDumpFile.getName());
        pdmFeedbackHandler.report("\t\t\t 2: " + aDumpDirectory);
        pdmFeedbackHandler.report("\t\t\t 3: " + aDataPumpAnalyze.name());
        CallableStatement cstmt = aDBConnection.prepareCall(ANALYSE_STATEMENT);
        cstmt.setString(1, aLocalDumpFile.getName());
        cstmt.setString(2, aDumpDirectory);
        cstmt.setInt(3, aDataPumpAnalyze.getReturnValue());
        cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
        cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
        cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
        cstmt.execute();
        sqlFileName = cstmt.getString(4);
        logFileName = cstmt.getString(5);
        // if exitcode of procedure is not equal to 0 an error has occurred
        int exitCode = cstmt.getInt(6);

        // it is possible that the dump version too low for the database server. then an error code 2 is returned.
        if (exitCode == 5)
        {
          try
          {
            String directory = aLocalDumpFile.getParent();
            String name = aLocalDumpFile.getName();
            CallableStatement cstmt1 = aDBConnection.prepareCall(ANALYZE_DUMPFILE);
            cstmt.setString(1, aDumpDirectory);
            cstmt1.setString(2, name);
            cstmt1.registerOutParameter(3, java.sql.Types.VARCHAR);
            cstmt1.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt1.execute();
            String fileType = cstmt1.getString(3);
            databaseVersion = cstmt1.getString(4);

            throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, null);
          }
          // refuse the exception. because if it going good, you can conclude that the version number isn't supported
          // by the db server. if its going wrong its something else. don't know what.
          // the exception =  Caused by: java.sql.SQLException: ORA-06550
          // PLS-00306: wrong number or types of arguments in call to 'ANALYZEDUMPFILE'
          catch (SQLException e)
          {
            databaseVersion = "Version error, database version probably too low to analyze the version number.";
          }
        }
        if (exitCode != 0)
        {
          pdmFeedbackHandler.report("    --> Stored procedure " + PROCEDURE_NAME_ANALYSE_DUMPFILE + "() failed with exitcode " + exitCode + "!\n\tlogfile: " + logFileName + "\n\tsqlfile: " + sqlFileName);
          pdmFeedbackHandler.report("    --> Contact DB team to manually check the dump version, it is possible that the database server don't support the version of the dump: " + databaseVersion);
          throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, null);
        }
      }
      catch (SQLException ex)
      {
        throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, ex);
      }
    }
    else
    {
      pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
      sqlFileName = "AnalyzeDumpfile_4.sql";
      logFileName = "import.log";
    }
    pdmFeedbackHandler.report(FEEDBACK_SUCCESS + "\n\tlogfile: " + logFileName + "\n\tsqlfile: " + sqlFileName);

    if (logFileName != null)
    {
      pdmFeedbackHandler.report(new File(getOracleSharedDirectory(), logFileName));
      deleteLocalFile(new File(getOracleSharedDirectory(), logFileName));
    }
    else
    {
      pdmFeedbackHandler.report("\tNo logfile to output");
    }

    return new File(getOracleSharedDirectory(), sqlFileName);
  }


  /**
   * Perform the actual import on the DB server.
   *
   * @param  aDbConnection      The DB connection
   * @param  aDumpFile          The dump file, as it exists in the shared oracle directory.
   * @param  aDumpDirectory     The name of the shared oracle directory, as Oracle knows it.
   * @param  aRequest           The request which contains specific information for importing a
   *                            dumpfile
   * @param  aSourceSchema      The schema name as specified in the oracle dump file. This is found
   *                            out by the analyse step.
   * @param  aSourceTableSpaces The table space(s) as specified in the oracle dump file. This is
   *                            found out by the analyse step.
   *
   * @return name of log file created by the import datapump procedure of the database
   *
   * @throws PDMException When bad things happen.
   */
  private boolean importDumpOnDB(Connection aDbConnection, File aDumpFile, String aDumpDirectory, OracleDataPumpImportRequest aRequest, String aSourceSchema, String aSourceTableSpaces) throws PDMException
  {
    boolean importResult = false;

    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Import step");

    String logFile = null;
    try
    {
      try
      {
        int taskTimeOut = aRequest.getTaskTimeOut();

        String importStatement = IMPORT_DUMPFILE_HEAD;

        if (taskTimeOut != 0)
        {
          importStatement += IMPORT_DUMPFILE_TIMEOUT;
        }
        importStatement += IMPORT_DUMPFILE_TAIL;

        pdmFeedbackHandler.report("\tSQL: '" + importStatement + "'");
        pdmFeedbackHandler.report("\t\tInput parameters: ");
        pdmFeedbackHandler.report("\t\t\t 1: " + aDumpFile.getName());
        pdmFeedbackHandler.report("\t\t\t 2: " + aDumpDirectory);

        pdmFeedbackHandler.report("\t\t\t 3: " + aSourceSchema);
        pdmFeedbackHandler.report("\t\t\t 4: " + aRequest.getSchemaName());
        pdmFeedbackHandler.report("\t\t\t 5: " + aSourceTableSpaces);
        pdmFeedbackHandler.report("\t\t\t 6: " + aRequest.getTableSpaceName());
        pdmFeedbackHandler.report("\t\t\t 9: " + (aRequest.getStopOnFailure() ? "1" : "0"));
        if (taskTimeOut != 0)
        {
          pdmFeedbackHandler.report("\t\t\t 10: " + taskTimeOut);
        }

        if (!SKIP_DB_ACTIONS)
        {
          CallableStatement cstmt = aDbConnection.prepareCall(importStatement);
          cstmt.setString(1, aDumpFile.getName());
          cstmt.setString(2, aDumpDirectory);
          cstmt.setString(3, aSourceSchema);
          cstmt.setString(4, aRequest.getSchemaName());
          cstmt.setString(5, aSourceTableSpaces);
          cstmt.setString(6, aRequest.getTableSpaceName());
          cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);
          cstmt.registerOutParameter(8, java.sql.Types.INTEGER);
          cstmt.setInt(9, (aRequest.getStopOnFailure() ? 1 : 0));
          if (taskTimeOut != 0)
          {
            cstmt.setInt(10, aRequest.getTaskTimeOut());
          }
          cstmt.execute();
          logFile = cstmt.getString(7);
          // if exitcode of procedure is not equal to 0 an error has occurred
          int exitCode = cstmt.getInt(8);
          switch (exitCode)
          {
            case 0 : // everything went well
            {
              importResult = true;
              pdmFeedbackHandler.report(FEEDBACK_SUCCESS);
              break;
            }
            case 3 : // the timeout has expired, the import has failed.
            {
              importResult = false;
              pdmFeedbackHandler.report("    --> failed with exitcode " + exitCode + "!");
              throw pdmFeedbackHandler.addException(ErrorDefinition.TIMEOUT_ERROR_ON_EXECUTING_DBTASK, null);
            }
            default : // an error occurred while importing.
            {
              importResult = false;
              pdmFeedbackHandler.report("    --> failed with exitcode " + exitCode + "!");
              throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE, null);
            }
          }
        }
        else
        {
          importResult = true;
          pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
        }
      }
      finally
      {
        if (logFile != null)
        {
          pdmFeedbackHandler.report(new File(getOracleSharedDirectory(), logFile));
          deleteLocalFile(new File(getOracleSharedDirectory(), logFile));
        }
      }
    }
    catch (SQLException ex)
    {
      throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE, ex);
    }

    return importResult;
  }


  /**
   * Check if tablespace name is a system like
   *
   * @param  aTableSpaceName tablespace name to check
   *
   * @return true if tablespace name is system specific, false otherwise
   */
  private boolean isSystemLikeTableSpaceName(String aTableSpaceName)
  {
    String tableSpaceName = aTableSpaceName.toUpperCase();
    for (String systemLikeTableSpace : SYSTEM_TABLESPACE_NAMES_PREFIXES)
    {
      if (tableSpaceName.startsWith(systemLikeTableSpace))
      {
        return true;
      }
    }
    return false;
  }


  /**
   * Check if schema name is a system specific
   *
   * @param  aSchemaName schema name to check
   *
   * @return true if schema name is system specific, false otherwise
   */
  private boolean isSystemSpecificSchemaName(String aSchemaName)
  {
    return SYSTEM_SCHEMA_NAMES.contains(aSchemaName);
  }


  /**
   * Check if tablespace name is a system specific
   *
   * @param  aTableSpaceName tablespace name to check
   *
   * @return true if tablespace name is system specific, false otherwise
   */
  private boolean isSystemSpecificTableSpaceName(String aTableSpaceName)
  {
    return SYSTEM_TABLESPACE_NAMES.contains(aTableSpaceName);
  }


  /**
   * Instruct the DB to analyze the dump file and create an SQL file with the structure.
   *
   * @param  aConnection The DB connection
   *
   * @return The SQL file that contains the SQL commands to set up the structure of the DB.
   *
   * @throws PDMException When bad things happen.
   */
  private boolean isUnicode(Connection aConnection) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Analyse unicode step");

    if (!SKIP_DB_ACTIONS)
    {
      try
      {
        pdmFeedbackHandler.report("\tSQL: '" + ANALYZE_UNICODE + "'");
        PreparedStatement pstmt = aConnection.prepareStatement(ANALYZE_UNICODE);
        ResultSet resultSet = pstmt.executeQuery();
        if (resultSet.next())
        {
          result = (resultSet.getInt(1) > 0);
        }
      }
      catch (SQLException ex)
      {
        exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE, ex);
        throw exception;
      }
      finally
      {
        closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE);
      }
    }
    pdmFeedbackHandler.report(FEEDBACK_SUCCESS + "db contains " + (result ? "" : "no ") + "unicode columns");
    return result;
  }


  /**
   * Parses the SQL file that contains the SQL structure of the DB, in order to find out what the
   * table space(s) and schema(s) names in the datapump file are.
   *
   * @param  aSQLFile the SQL file containing the analysis
   *
   * @return AnalysisResult.
   *
   * @throws PDMException
   */
  private AnalysisResult parseAnalyseSQL(File aSQLFile) throws PDMException
  {
    Set<String> tableSpacesList = new HashSet();
    Set<String> schemasList = new HashSet();
    Set<String> alternateSchemaList = new HashSet();
    List<String> lines = new ArrayList<String>();

    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Parse SQL step");
    if (!SKIP_DB_ACTIONS)
    {
      FileReader reader = null;
      try
      {
        reader = new FileReader(aSQLFile);
      }
      catch (FileNotFoundException ex)
      {
        throw pdmFeedbackHandler.addException(ErrorDefinition.FILE_TO_PARSE_NOT_FOUND, ex);
      }

      Pattern schemanamePattern = Pattern.compile("\\WCREATE USER \"(\\w+)\" IDENTIFIED BY VALUES ");
      // PCIS 203610: Find all possible tablespaces, except the temporary ones.
      Pattern tempTablespacePattern = Pattern.compile("\\WTEMPORARY TABLESPACE \"(\\w+)\"");
      Pattern tablespacePattern = Pattern.compile("\\WTABLESPACE \"(\\w+)\"");
      Pattern alternateSchemaPattern = Pattern.compile("^-- CONNECT (\\w+)");

      BufferedReader br = new BufferedReader(reader);

      final int limitedLines = 200;
      try
      {
        try
        {
          String currentRecord;
          int lineNr = 0;
          while ((currentRecord = br.readLine()) != null)
          {
            lineNr++;
            if (lineNr < limitedLines)
            {
              lines.add(currentRecord);
            }
            else if (lineNr == limitedLines)
            {
              lines.add("...");
            }
            Matcher schemanameMatcher = schemanamePattern.matcher(currentRecord);
            Matcher tablespaceMatcher = tablespacePattern.matcher(currentRecord);
            Matcher tempTablespaceMatcher = tempTablespacePattern.matcher(currentRecord);
            Matcher alternateTablespaceMatcher = alternateSchemaPattern.matcher(currentRecord);
            while (schemanameMatcher.find())
            {
              String userSchema = schemanameMatcher.group(1);
              // keep a list of schema names to prevent duplicates and
              // a list of system-specific schema names that need to be ignored (PCIS 207696)
              if (!isSystemSpecificSchemaName(userSchema))
              {
                schemasList.add(userSchema);
              }
            }
            while (tablespaceMatcher.find())
            {
              // exclude temporary tablespaces
              if (!tempTablespaceMatcher.find())
              {
                String tableSpace = tablespaceMatcher.group(1);
                // keep list of tablespace names to prevent duplicates and
                // a list of system-specific tablespace names that need to be ignored (PCIS 207696) and
                // a list of system-alike tablespace names that need to be ignored (PCIS 209637)
                if (!isSystemSpecificTableSpaceName(tableSpace) //
                  && !isSystemLikeTableSpaceName(tableSpace))
                {
                  tableSpacesList.add(tableSpace);
                }
              }
            }
            if (alternateTablespaceMatcher.find())
            {
              String tableSpace = alternateTablespaceMatcher.group(1);
              if (!isSystemSpecificTableSpaceName(tableSpace))
              {
                alternateSchemaList.add(tableSpace);
              }
            }
          }
        }
        finally
        {
          br.close();
        }
      }
      catch (IOException ex)
      {
        throw pdmFeedbackHandler.addException(ErrorDefinition.IO_ERROR_READING_ANALYZE_FILE, ex);
      }
    }
    else
    {
      pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
      tableSpacesList.add("SYSTEM");
      schemasList.add("SYM_SCHEMA");
    }
    if (schemasList.isEmpty())
    {
      if (!alternateSchemaList.isEmpty())
      {
        pdmFeedbackHandler.report("\tadding schema's found by \"--Connect: <schema name>\": " + PDMDbAgentUtilities.convertSeparatedList(alternateSchemaList, SCHEMA_NAME_SEPARATOR));
      }
      schemasList.addAll(alternateSchemaList);
    }

    AnalysisResult analysisResult = new AnalysisResult(tableSpacesList, schemasList);

    if (tableSpacesList.isEmpty() || schemasList.isEmpty())
    {
      pdmFeedbackHandler.report(FEEDBACK_FAILURE + "\n\tfound tableSpaces: " + PDMDbAgentUtilities.convertSeparatedList(tableSpacesList, TABLESPACE_NAME_SEPARATOR) //
        + "\n\tfound schemas: " + PDMDbAgentUtilities.convertSeparatedList(schemasList, SCHEMA_NAME_SEPARATOR) //
        + "\n" + aSQLFile + ":\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" + PDMDbAgentUtilities.convertSeparatedList(lines, "\n") //
        + ":\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
    }
    else
    {
      pdmFeedbackHandler.report(FEEDBACK_SUCCESS + "\n\ttableSpaces: " + analysisResult.getTableSpaceNames(TABLESPACE_NAME_SEPARATOR) //
        + "\n\tschemas: " + analysisResult.getSchemaNames(SCHEMA_NAME_SEPARATOR));
    }

    return analysisResult;
  }
}
