// Planon Enterprise Edition Source file: OracleExportDataPumpAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.nio.file.*;
import java.sql.*;

import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * Provides a very simple oracle export.
 *
 * <p>This class is already in place because i know we're going to need it in the future, and i
 * already have part of the code.</p>
 */
public class OracleExportDataPumpAction extends OracleDBAgentAction<ExportResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String LOGFILE_POSTFIX = "_export";
  private static final String DUMPFILE_EXTENSION = ".dpd";

  //~ Instance Variables ---------------------------------------------------------------------------

  private Integer pkPDMDatabase;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an export of oracle data pump.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Export the DB to a datapump dump file</li>
   *   <li>Close DB connection</li>
   *   <li>Upload dumpfile and logfile from oracle shared directory to webdav location</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an OracleDataPumpRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    ACLMessage reply = aInput.createReply();
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    try
    {
      String logFileName = null;
      try
      {
        ExportResponse response = getResponseData();
        OracleDataPumpExportRequest request = (OracleDataPumpExportRequest) aConcept;
        response.fillMandatoryResponseData(request);
        response.setDumpFileDir("n.a.");
        response.setDumpFileName("n.a.");
        this.pkPDMDatabase = request.getPrimaryKeyDatabase();
        pdmFeedbackHandler.report("  * Export step");
        Connection dbConnection = null;

        try
        {
          // Now, the real work starts
          dbConnection = getDBConnection(request.getDatabaseServerInformation());

          int stopOnFailure = (request.getStopOnFailure() ? 1 : 0);
          // The timeout parameter is optional. If it is not set by the user it can be ignored.
          int taskTimeOut = request.getTaskTimeOut();
          String exportStatement = //
            "{call PLN_DataPumpInterface.PDM_DATAPUMP_EXPORT( " //
            + "  p_SchemaName        => ?" //
            + ", p_Directory         => ?" //
            + ", p_DumpFileName      => ?" //
            + ", p_LogFileName       => ?" //
            + ", p_ExitCode          => ?" //
            + ", p_TerminateOnError  => ?";
          if (taskTimeOut != 0)
          {
            exportStatement += ", p_TimeoutPeriod     => ?";
          }
          exportStatement += ")}";

          pdmFeedbackHandler.report("\tSQL: '" + exportStatement + "'");
          pdmFeedbackHandler.report("\t\tInput parameters: ");
          pdmFeedbackHandler.report("\t\t\t 1: " + request.getSchemaName());
          pdmFeedbackHandler.report("\t\t\t 2: " + getOracleSharedDirectoryIdentifier());
          pdmFeedbackHandler.report("\t\t\t 6: " + stopOnFailure);
          if (taskTimeOut != 0)
          {
            pdmFeedbackHandler.report("\t\t\t 7: " + taskTimeOut);
          }

          if (!SKIP_DB_ACTIONS)
          {
            CallableStatement cstmt = dbConnection.prepareCall(exportStatement);
            cstmt.setString(1, request.getSchemaName());
            cstmt.setString(2, getOracleSharedDirectoryIdentifier());
            cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
            cstmt.setInt(6, stopOnFailure);
            if (taskTimeOut != 0)
            {
              cstmt.setInt(7, taskTimeOut);
            }

            cstmt.execute();

            Path result = getOracleSharedDirectory().toPath().resolve(createNameExportedDump(request));
            Files.move(getOracleSharedDirectory().toPath().resolve(cstmt.getString(3)), result);
            response.setDumpFileName(result.toFile().getName());

            logFileName = cstmt.getString(4);
            // if exitcode of procedure is not equal to 0 an error has occurred
            int exitCode = cstmt.getInt(5);
            switch (exitCode)
            {
              case 0 : // everything went well
                break;
              case 3 : // the timeout has expired, the export has failed.
              {
                pdmFeedbackHandler.report("    --> failed with exitcode " + exitCode + ", logfile: '" + logFileName + "'!");
                throw pdmFeedbackHandler.addException(ErrorDefinition.TIMEOUT_ERROR_ON_EXECUTING_DBTASK, null);
              }
              default : // an error occurred while exporting.
              {
                pdmFeedbackHandler.report("    --> failed with exitcode " + exitCode + ", logfile: '" + logFileName + "'!");
                throw pdmFeedbackHandler.addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, null);
              }
            }
          }
          else
          {
            String exportedDumpFileName = "ExportedDump" + DUMPFILE_EXTENSION;
            try
            {
              new File(getOracleSharedDirectory(), exportedDumpFileName).createNewFile();
            }
            catch (IOException ex)
            {
              throw pdmFeedbackHandler.addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, null);
            }
            response.setDumpFileName(exportedDumpFileName);
            logFileName = "Simulated.log";
            pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
          }
        }
        finally
        {
          if (dbConnection != null)
          {
            dbConnection.close();
          }
        }
      }
      catch (SQLException | IOException ex)
      {
        throw pdmFeedbackHandler.addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, ex);
      }
      pdmFeedbackHandler.report("    --> success, logfile: '" + logFileName + "'!");

      pdmFeedbackHandler.report(new File(getOracleSharedDirectory(), logFileName));
      deleteLocalFile(new File(getOracleSharedDirectory(), logFileName));

      reply.setPerformative(ACLMessage.INFORM);
      reply.setContent(PDMAgentProperties.MESSAGE_PDMREQUEST_OK);
      reply.addUserDefinedParameter(PDMAgentProperties.PARAM_MESSAGE_TYPE, PDMAgentProperties.MESSAGE_PDMREQUEST_RESULT);
    }
    catch (PDMException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE); // Action failed
      throw pdmFeedbackHandler.addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, ex.isErrorForAdministrator(), ex);
    }
    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
  }


  /**
   * {@inheritDoc}
   */
  @Override public void responseFilesToWebDAV(AbstractRequest aRequest) throws PDMException
  {
    ExportResponse response = getResponseData();

    String originalDumpFile = aRequest.getDumpFileName();
    String resultDumpFile = createNameExportedDump(aRequest);

    File localFile = new File(getOracleSharedDirectory(), resultDumpFile);
    if (localFile != null)
    {
      sendFileToWebDAVLocation(localFile, aRequest.getDumpFileDir(), resultDumpFile);
      deleteLocalFile(localFile);
    }

    response.setDumpFileDir(aRequest.getDumpFileDir());
    response.setDumpFileName(resultDumpFile);

    if (aRequest.getDumpFileName() != null)
    {
      deleteLocalFile(new File(getOracleSharedDirectory(), response.getDumpFileName()));
    }

    super.responseFilesToWebDAV(aRequest);
  }


  /**
   * creates new instance of ExportResponse
   *
   * @return ExportResponse
   */
  @Override protected ExportResponse createResponse()
  {
    return new ExportResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getDumpFileExtension()
  {
    return DUMPFILE_EXTENSION;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }
}
