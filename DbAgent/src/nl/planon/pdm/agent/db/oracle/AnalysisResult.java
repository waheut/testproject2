// Planon Enterprise Edition Source file: AnalysisResult.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import java.util.*;

import nl.planon.pdm.agent.db.*;

/**
 * Analysis Result of oracle dump file
 */
class AnalysisResult
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private final Set<String> schemaNames;
  private final Set<String> tableSpaceNames;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AnalysisResult object.
   *
   * @param aTableSpaceNames comma separarated list of table spaces
   * @param aSchemaNames     name of schema
   */
  public AnalysisResult(Set<String> aTableSpaceNames, Set<String> aSchemaNames)
  {
    assert aTableSpaceNames != null : "table space names must be specified";
    assert aSchemaNames != null : "schema names must be specified";
    this.tableSpaceNames = aTableSpaceNames;
    this.schemaNames = aSchemaNames;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * is Schema(s) Found
   *
   * @return boolean
   */
  public boolean isSchemaFound()
  {
    return !this.schemaNames.isEmpty();
  }


  /**
   * is TableSpaces(s) Found
   *
   * @return boolean
   */
  public boolean isTableSpacesFound()
  {
    return !this.tableSpaceNames.isEmpty();
  }


  /**
   * gets separated list of schema names
   *
   * @param  aSep separator
   *
   * @return String
   */
  String getSchemaNames(String aSep)
  {
    return PDMDbAgentUtilities.convertSeparatedList(this.schemaNames, aSep);
  }


  /**
   * gets separated list of table space names
   *
   * @param  aSep separator
   *
   * @return String
   */
  String getTableSpaceNames(String aSep)
  {
    return PDMDbAgentUtilities.convertSeparatedList(this.tableSpaceNames, aSep);
  }
}
