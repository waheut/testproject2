// Planon Enterprise Edition Source file: OracleImportOldStyleAction.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.oracle.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * OracleImportOldStyleAction, also collect schemas used by dump
 */
public class OracleImportOldStyleAction extends OracleDBAgentAction<ImportResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String DOUBLE_QUOTE_CHAR = "\"";
  private static final String DOT_CHAR = ".";
  private static final String SPACE_CHAR = " ";
  private static final String CONNECT = "CONNECT ";
  private static final String CONNECT_PARAMETER_START = "(";
  private static final String CREATE_USER = "CREATE USER ";
  private static final String RUSERS = "RUSERS";
  private static final String ARG_BUFFER = "BUFFER=1000000";
  private static final String SUFFIX_DMP = "dmp";

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an oracle import Old style.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Download dump from webdav location to oracle shared directory</li>
   *   <li>Analyse dump on the Oracle DB server. This will result in an SQL script that contains the
   *     structure of the dump</li>
   *   <li>Retrieve the tablespace(s) and schema(s) from the table structure.</li>
   *   <li>Import the OldStyle dump file into the DB</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an OracleDataPumpRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException When bad things happen.
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    boolean schemaAndTablespaceCreated = false;
    boolean databaseImported = false;

    ImportResponse response = getResponseData();
    ACLMessage reply = aInput.createReply();
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    try
    {
      OracleOldStyleImportRequest request = (OracleOldStyleImportRequest) aConcept;
      OracleDatabaseServerInformation dbServerInfo = request.getDatabaseServerInformation();
      response.fillMandatoryResponseData(request);
      String adminUser = dbServerInfo.getAdminUser();
      String adminPassword = dbServerInfo.getAdminPassword();
      String impLocation = dbServerInfo.getImpLocation();
      String dbInstanceName = dbServerInfo.getDatabaseInstanceName();
      String remoteDumpFileName = request.getDumpFileName();
      boolean noImport = request.getNoImport();
      PDMPrivilegeType pdmPrivilegeType = request.getPDMPrivilegeType();
      String databasePassword = request.getDatabasePassword();
      String toUser = request.getSchemaName();
      String dumpfileShareDir = getOracleSharedDirectory().toString();
      File datafileLocation = getDataFileLocationWithMaxFreeSpace(dbServerInfo.getDataFileLocations()); // PCIS 193113: set datafilelocation for creating tablespace

      String dumpSchemaName = request.getDumpInformation().getSchemaName();

      String logfile = request.getPrimaryKeyDatabase() + "_" + request.getPrimaryKeyRequest() + "_" + request.getPrimaryKeyTask() + "_" + getLogfilePostfix() + ".log";
      // Strip absolute path, we need the file name later.
      String dumpFileName = new File(remoteDumpFileName).getName();

      Connection dbConnection = null;

      String tablespace = PDM_ + toUser;
      File localDumpFile = retrieveFileFromWebDAV(remoteDumpFileName, getOracleSharedDirectory());

      localDumpFile = correctSuffix(localDumpFile, SUFFIX_DMP); // the import will fail when the dump doesn't have an extension.

      Scanner scan = null;
      try
      {
        if ((impLocation == null) || (impLocation.isEmpty()))
        {
          throw pdmFeedbackHandler.addException(ErrorDefinition.IMPORT_LOCATION_NOT_SPECIFIED, null);
        }
        if (!isOldStyleDumpFileContainingPlanonDB(localDumpFile))
        {
          // dump file not properly readable, not an old style dump.
          throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, null);
        }

        String dumpDatabaseVersion = getDumpDatabaseVersion(localDumpFile);

        // if version don't equal with the supported version of the database, throw a error.
        boolean isSupported = dbServerInfo.isVersionSupported(dumpDatabaseVersion);
        if (!isSupported)
        {
          throw pdmFeedbackHandler.addException(ErrorDefinition.DUMP_VERSION_NOT_SUPPORT, null, dumpDatabaseVersion);
        }

        if (dumpSchemaName == null)
        {
          // collect all schema names, user has to pick one before import can be done
          Set<String> schemaNames = getSchemas(localDumpFile);
          if (schemaNames.isEmpty())
          {
            throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_PARSING_NO_SCHEMA_FOUND, null);
          }

          dumpSchemaName = PDMDbAgentUtilities.convertSeparatedList(schemaNames, SCHEMA_NAME_SEPARATOR);
        }

        // do not fill the schemaname and tablespace name in case the request is a reload export
        // (so only in case the request is a new import or a reload initial).
        if (!PDMRequestType.RELOAD_EXPORT.equals(request.getPDMRequestType()))
        {
          DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
          dumpInformation.setSchemaName(dumpSchemaName);
          //dumpInformation.setTableSpaceNames(dumpTableSpaceNames);
          // PCIS 205844: if multiple schema names found in dump, stop importing, report back that the user first
          // has to choose the right schema name
          if (dumpSchemaName.contains(SCHEMA_NAME_SEPARATOR))
          {
            throw pdmFeedbackHandler.addException(ErrorDefinition.MULTIPLE_SCHEMAS_FOUND_IN_DUMP, null);
          }
        }
        //create first a databaseconnection.
        dbConnection = getDBConnection(dbServerInfo);
        //create tablespace and schema
        String dataFileName = changeFileExtension(dumpFileName, SUFFIX_DATABASE, "");
        createSchemaAndTablespace(dbConnection, tablespace, toUser, databasePassword, new File(datafileLocation, dataFileName), pdmPrivilegeType);
        schemaAndTablespaceCreated = true;
        if (!noImport)
        {
          //create executable argument
          String ignoreFlag = (!request.getStopOnFailure()) ? "Y" : "N";
          String importCommandTail = "@" + dbInstanceName + " file=" + localDumpFile + " fromuser=" + dumpSchemaName + " touser=" + toUser + " log=" + dumpfileShareDir + "\\" + logfile + " ROWS=Y " + ARG_BUFFER + " IGNORE=" + ignoreFlag + " GRANTS=N";
          String[] importCommand =
          { //
            impLocation, //
            adminUser + "/" + adminPassword + importCommandTail //
          };

          pdmFeedbackHandler.report(importCommand[0] + SPACE_CHAR + "***uid***/***pwd***" + importCommandTail);

          if (!SKIP_DB_ACTIONS)
          {
            ProcessBuilder builder = new ProcessBuilder(importCommand);
            builder.redirectErrorStream(true);

            Process process = builder.start();
            InputStream processInput = process.getInputStream();

            int taskTimeOut = getTaskTimeOut(request);

            // create and start a timeout thread
            TimeOutThread timeout = new TimeOutThread(taskTimeOut, process);
            timeout.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(processInput));
            scan = new Scanner(reader);

            while (scan.hasNext())
            {
              pdmFeedbackHandler.report("  * Import step");
              String line = reader.readLine();
              // loop for write every output line
              while (line != null)
              {
                pdmFeedbackHandler.report(line);
                line = reader.readLine();
              }
              // task is done
              if (line == null)
              {
                timeout.cancel();

                int exitCode = process.exitValue();
                process.destroy();
                if (timeout.isTimedOut())
                {
                  throw pdmFeedbackHandler.addException(ErrorDefinition.TIMEOUT_ERROR_ON_EXECUTING_DBTASK, null);
                }
                if (exitCode == 0)
                {
                  databaseImported = true;
                  pdmFeedbackHandler.report(FEEDBACK_SUCCESS + " logfile: '" + logfile + "'");
                  break;
                }
                else if (!request.getStopOnFailure())
                {
                  databaseImported = true;
                  pdmFeedbackHandler.report("    --> failed with exitcode " + exitCode + ", logfile: '" + logfile + "'!\n    --> BUT treated as successfull because stopOnFailure=false!");
                  break;
                }
                else
                {
                  pdmFeedbackHandler.report("    --> failed with exitcode " + exitCode + ", logfile: '" + logfile + "'!");
                  throw pdmFeedbackHandler.addException(ErrorDefinition.ERROR_ON_IMPORTING_DUMPFILE_OLDSTYLE, null);
                }
              }
            }
            if (databaseImported)
            {
              setPlanonDBVersionDetailsTODumpInfo(response, dbConnection, toUser);
            }
          }
          else
          {
            pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
          }
        }
        reply.setPerformative(ACLMessage.INFORM); // Action successfully executed
      }
      catch (IOException ex)
      {
        //  reply.setPerformative(ACLMessage.FAILURE); // Action failed
        throw pdmFeedbackHandler.addException(ErrorDefinition.ERROR_ON_IMPORTING_DUMPFILE_OLDSTYLE, ex);
      }
      finally
      {
        // PCIS 207241: if the exception is raised after the schema and tablespace is created, then these have to be deleted again.
        // Also TEC-2714, PDM3: reloading exported database don't drop user and tablespace if it fails to load in oracle
        if (schemaAndTablespaceCreated && !databaseImported && !noImport)
        {
          getPDMFeedbackHandler().report("The schema and the tablespace were created successfully but the db import is not succesful.");
          getPDMFeedbackHandler().report("Hence, dropping the schema and tablespaces...");
          dropSchemaAndTablespace(dbConnection, request.getSchemaName(), tablespace);
          getPDMFeedbackHandler().report("The schema and tablespaces were dropped successfully.");
        }
        // PCIS 193113 / PCIS 193436:
        // Original dumpfile as well as analyze file and log file on database server (if created) must be deleted after import.
        deleteLocalFile(localDumpFile);

        if (scan != null)
        {
          scan.close();
        }

        if (dbConnection != null)
        {
          closeConnection(dbConnection);
        }
      }
    }
    catch (PDMException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE); // Action failed
      throw ex;
    }

    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
    AbstractPDMAgent agent = getAgent();
    ImportResponse response = getResponseData();

    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
    response.setDatabaseServerName(agent.getDbServerName());
    dumpInformation.setDatabaseType(agent.getDbType());
    dumpInformation.setFileType(PDMFileType.DATAPUMP);
  }


  /**
   * creates new instance of ImportResponse
   *
   * @return ImportResponse
   */
  @Override protected ImportResponse createResponse()
  {
    return new ImportResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }


  /**
   * append suffix if specified dumpfile has no suffix
   *
   * @param  aDumpFile source dump file
   * @param  aSuffix   suffix to apply
   *
   * @return aDumpFile with suffix
   */
  private File correctSuffix(File aDumpFile, String aSuffix)
  {
    String name = aDumpFile.getName();
    int lastIndexOf = name.lastIndexOf(DOT_CHAR);
    if (lastIndexOf <= 0)
    {
      File newFile = new File(aDumpFile.getParentFile(), name + DOT_CHAR + aSuffix);
      if (aDumpFile.renameTo(newFile))
      {
        return newFile;
      }
    }
    return aDumpFile;
  }


  /**
   * scan the dumpfile for schemas. first it tries to find it at the second line.<br>
   * Second it collects them from CREATE USER statements.<br>
   * third, if previous steps do not result in a schema name, all connected schema names are
   * returned.
   *
   * <p>i've seen dumps with system schema name on third line instead of second line. However it did
   * contain the correct schema name at the connect statement</p>
   *
   * @param  aDumpfile Dumpfile
   *
   * @return Set with schemas, can be empty
   *
   * @throws IOException
   */
  private Set<String> getSchemas(File aDumpfile) throws IOException
  {
    Set<String> schemas = new TreeSet<String>();
    Set<String> connects = new TreeSet<String>();

    BufferedReader bf = new BufferedReader(new FileReader(aDumpfile));
    try
    {
      String singleSchema = null;

      // Start a line count and declare a string to hold our current line.
      int linecount = 0;
      String line;

      // Loop through the first line for searching for EXPORT:V
      while ((line = bf.readLine()) != null)
      {
        linecount++;
        if (linecount == 2)
        {
          if (line.contains(EXPORT_V)) // should normally be on line 1
          {
            linecount--; // line 1 is split over two lines, correction required
          }
        }
        if (linecount == 2)
        {
          singleSchema = line.substring(1);
        }
        if (linecount == 3)
        {
          if (line.equalsIgnoreCase(RUSERS))
          {
            schemas.add(singleSchema);
          }
          else
          {
            singleSchema = null;
          }
        }
        if (line.startsWith(CREATE_USER))
        {
          String[] lineWords = line.split(SPACE_CHAR);
          String schema = lineWords[2];
          if (schema.startsWith(DOUBLE_QUOTE_CHAR)) // ignore internal users without qoutes like COGNOS
          {
            schema = schema.substring(1, schema.length() - 1); // remove quotes
            schemas.add(schema);
          }
        }

        if (line.startsWith(CONNECT))
        {
          String[] lineWords = line.split(SPACE_CHAR);
          String connect = lineWords[1];
          if (!connect.startsWith(CONNECT_PARAMETER_START))
          {
            connects.add(connect);
          }
        }
      }
    }
    finally
    {
      // Close the file after done searching
      bf.close();
    }

    connects.removeAll(SYSTEM_SCHEMA_NAMES);
    schemas.removeAll(SYSTEM_SCHEMA_NAMES);
    if (!connects.isEmpty())
    {
      return connects; // preference to schemas which are connected...
    }
    return schemas;
  }
}
