// Planon Enterprise Edition Source file: DataPumpAnalyze.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

/**
 * indicates whether a limited (standard) or extended analyze has to be done
 *
 * @version $Revision$
 */
public enum DataPumpAnalyze
{
  //~ Enum constants -------------------------------------------------------------------------------

  LIMITED(0), EXTENDED(1);

  //~ Instance Variables ---------------------------------------------------------------------------

  private int returnValue;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DataPumpAnalyze object.
   *
   * @param aReturnValue
   */
  private DataPumpAnalyze(int aReturnValue)
  {
    this.returnValue = aReturnValue;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets return value
   *
   * @return 0,1
   */
  public int getReturnValue()
  {
    return this.returnValue;
  }
}
