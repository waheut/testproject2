// Planon Enterprise Edition Source file: OracleAnalyzeDataPumpAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.oracle;

import jade.content.*;
import jade.lang.acl.*;
import jade.util.leap.*;

import java.io.*;
import java.sql.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * OracleAnalyzeDataPumpAction
 */
public class OracleAnalyzeDataPumpAction extends OracleDBAgentAction<AnalyzeResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String ANALYZE_DUMPFILE = //
    "{call PLN_DataPumpInterface.AnalyzeDumpFile("
    + "p_Directory => ?, " //
    + "p_DumpFileName => ?, " //
    + "p_FileType => ?, " //
    + "p_DatabaseVersion => ?)}";

  //~ Instance Variables ---------------------------------------------------------------------------

  private final List fileType = new ArrayList(); // jade list of PDMFileType

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    ACLMessage reply = aInput.createReply();
    AnalyzeResponse response = getResponseData();
    OracleDataPumpAnalyzeRequest request = (OracleDataPumpAnalyzeRequest) aConcept;
    response.fillMandatoryResponseData(request);
    Integer pkRequest = request.getPrimaryKeyRequest();
    try
    {
      IDatabaseDumpInformation dumpInformationRequest = request.getDumpInformation();

      String remoteDumpFileName = request.getDumpFileName();
      File datafileLocation = getDataFileLocationWithMaxFreeSpace(request.getDatabaseServerInformation().getDataFileLocations()); // PCIS 193113: set datafilelocation for creating tablespace
      Integer pdmLocation = request.getDatabaseLocation();
      response.setDatabaseLocation(pdmLocation);

      String dumpFileName = new File(remoteDumpFileName).getName();
      File localDumpFile = null;
      Connection dbConnection = null;

      try
      {
        localDumpFile = retrieveFileFromWebDAV(remoteDumpFileName, getOracleSharedDirectory());
        dbConnection = getDBConnection(request.getDatabaseServerInformation());

        analyseDumpOnDB(dbConnection, localDumpFile, getOracleSharedDirectoryIdentifier());

        reply.setPerformative(ACLMessage.INFORM); // Action successfully executed
      }
      catch (SQLException ex)
      {
        response.setPrimaryKeyRequest(pkRequest);

        reply.setPerformative(ACLMessage.FAILURE); // Action failed
        throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, ex);
      }
      finally
      {
        // PCIS 193113 / PCIS 193436:
        // Original dumpfile as well as analyze file and log file on database server (if created) must be deleted after import.
        deleteLocalFile(localDumpFile);

        if (dbConnection != null)
        {
          closeConnection(dbConnection);
        }
      }
    }
    catch (PDMException ex)
    {
      response.setPrimaryKeyRequest(pkRequest);
      // if its a admin error, the analyze is finish correctly and the import can start.
      if (ex.isErrorForAdministrator())
      {
        reply.setPerformative(ACLMessage.INFORM); // Action finish correctly
      }
      else
      {
        reply.setPerformative(ACLMessage.FAILURE); // Action failed
      }
      throw ex;
    }

    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
    AbstractPDMAgent agent = getAgent();
    AnalyzeResponse response = getResponseData();
    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
    PDMFileType.DATAPUMP.equals(agent.getFileType());
    response.setDatabaseServerName(agent.getDbServerName());
    dumpInformation.setDatabaseType(agent.getDbType());
    dumpInformation.setFileType(PDMFileType.DATAPUMP);
    if (!this.fileType.isEmpty())
    {
      response.setTriedFileType(this.fileType);
    }
    else
    {
      // if a exception occurred before the analyze be used. set Datapump as triedFileType.
      List list = new ArrayList(); // this is not a java.util.List because there is
                                   // no ontology for that (also no generics here)
      list.add(PDMFileType.DATAPUMP);
      response.setTriedFileType(list);
    }
  }


  /**
   * creates new instance of AnalyzeResponse
   *
   * @return AnalyzeResponse
   */
  @Override protected AnalyzeResponse createResponse()
  {
    return new AnalyzeResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return "";
  }


  /**
   * Instruct the DB to analyze the dump file and create an SQL file with the structure.
   *
   * @param  aDBConnection  The DB connection
   * @param  aLocalDumpFile The dump file in the shared oracle directory.
   * @param  aDumpDirectory The name of the directory, as oracle knows it.
   *
   * @throws PDMException When bad things happen.
   * @throws SQLException
   */
  private void analyseDumpOnDB(Connection aDBConnection, File aLocalDumpFile, String aDumpDirectory) throws PDMException, SQLException
  {
    String name = aLocalDumpFile.getName();
    AnalyzeResponse response = getResponseData();
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Analyse step");

    pdmFeedbackHandler.report("\tSQL: '" + ANALYZE_DUMPFILE + "'");
    pdmFeedbackHandler.report("\t\tInput parameters: ");
    pdmFeedbackHandler.report("\t\t\t 1: " + aDumpDirectory);
    pdmFeedbackHandler.report("\t\t\t 2: " + name);

    String fileType = null;
    String databaseVersion = null;
    if (!SKIP_DB_ACTIONS)
    {
      CallableStatement cstmt = aDBConnection.prepareCall(ANALYZE_DUMPFILE);
      cstmt.setString(1, aDumpDirectory);
      cstmt.setString(2, name);
      cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
      cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
      cstmt.execute();
      fileType = cstmt.getString(3);
      databaseVersion = cstmt.getString(4);
    }
    else
    {
      fileType = "1";
      databaseVersion = "11.2.0.1.0";
    }
    // filetype can be null.
    // 1 = datapump
    // 2 = oldstyle
    // null or else = mssql
    if (fileType != null)
    {
      int filetype = Integer.valueOf(fileType).intValue();
      switch (filetype)
      {
        // 1 = datapump
        // 2 = oldstyle
        // 3 = something else
        case 1 :
          this.fileType.add(PDMFileType.DATAPUMP);

          // version xx.xx.xx.xx.xx
          databaseVersion = formatTenDigitVersionNumber(databaseVersion);

          DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
          dumpInformation.setVersionNumber(databaseVersion);
          // exception for failing the
          break;
        default :
          throw pdmFeedbackHandler.addException(ErrorDefinition.NOT_ORACLE_DATAPUMPFILE, null);
      }
    }
    else
    {
      this.fileType.add(PDMFileType.DATAPUMP);
      this.fileType.add(PDMFileType.ORA_EXP);
      throw pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, null);
    }
  }
}
