// Planon Enterprise Edition Source file: DBAgentActionRegistry.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.agent.db;

import java.util.*;

import nl.planon.pdm.agent.db.oracle.*;

/**
 * ActionRegistry
 */
public class DBAgentActionRegistry
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final DBAgentActionRegistry INSTANCE = new ActionRegistry();

  //~ Instance Variables ---------------------------------------------------------------------------

  private final Map<String, Class<? extends AbstractDBAgentAction>> registry = new HashMap<String, Class<? extends AbstractDBAgentAction>>();

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get instance of db action registry
   *
   * @return instance of db action registry
   */
  public static DBAgentActionRegistry getInstance()
  {
    // This is the place where later, an MSSQL implementation can be added.
    return INSTANCE;
  }


  /**
   * get database agent action linked to the given action name
   *
   * @param  aActionName name of database action
   *
   * @return database agent action class
   */
  public Class<? extends AbstractDBAgentAction> get(String aActionName)
  {
    return this.registry.get(aActionName);
  }


  /**
   * Register a database action with its DBAgentAction Class.
   *
   * @param aActionName name of database action
   * @param aAction     database agent action class
   */
  protected void register(String aActionName, Class<? extends AbstractDBAgentAction> aAction)
  {
    assert !this.registry.containsKey(aActionName) : "Registry can only hold one registration for an action";
    this.registry.put(aActionName, aAction);
  }
}
