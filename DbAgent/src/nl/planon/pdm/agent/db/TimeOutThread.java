// Planon Enterprise Edition Source file: TimeOutThread.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db;

/**
 * Thread to kill process after delay in seconds
 */
public class TimeOutThread extends Thread
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private boolean cancel = false;
  private boolean timedout = false;
  private int timeOut;
  private Process process;
  private String ex = null;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Destroys a process after the timeout in seconds.
   *
   * @param aTimeOutInSeconds time after which process has to be stopped
   * @param aProcess          process
   */
  public TimeOutThread(int aTimeOutInSeconds, Process aProcess)
  {
    this.timeOut = aTimeOutInSeconds;
    this.process = aProcess;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Cancel the timeout thread
   */
  public void cancel()
  {
    this.cancel = true;
  }


  /**
   * check if thread is timed out
   *
   * @return true if thread is timed out, otherwise false
   */
  public boolean isTimedOut()
  {
    return this.timedout;
  }


  /**
   * Start the Timeout Thread
   */
  @Override public void run()
  {
    try
    {
      Thread.sleep(this.timeOut * 1000);
      if (!this.cancel)
      {
        this.process.destroy();
        this.timedout = true;
      }
    }
    catch (InterruptedException ex)
    {
    }
  }
}
