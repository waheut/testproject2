// Planon Enterprise Edition Source file: MSSQLDeleteAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.mssql;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.sql.*;

import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * MSSQLDeleteAction
 */
public class MSSQLDeleteAction extends MSSQLDBAgentAction<DeleteResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String LOGFILE_POSTFIX = "_delete";

  private static final String SQL_CHECK_EXISTANCE_USERNAME = "select * from master..syslogins where loginname = (?)";

  private static final String SQL_CHECK_IF_USER_CONNECTED = "SELECT (login_name), COUNT(session_id) as session_count FROM sys.dm_exec_sessions where login_name = (?) GROUP BY login_name";

  private static final String SQL_CHECK_EXISTANCE_TABLESPACENAME = "select * from sys.databases where name = (?)";

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to delete an MSSQL database.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Remove database user and remove the user's objects</li>
   *   <li>Remove table space from the database</li>
   *   <li>Remove all files related to this specific database</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an MSSQLDeleteRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    ACLMessage reply = aInput.createReply();
    try
    {
      MSSQLDeleteRequest request = (MSSQLDeleteRequest) aConcept;
      getResponseData().fillMandatoryResponseData(request);
      String userName = request.getSchemaName();
      String tableSpaceName = request.getTableSpaceName();

      // Strip absolute path, we need the file name later.
      File localDumpFile = null;
      Connection dbConnection = null;
      try
      {
        getPDMFeedbackHandler().report("Starting deletion of database with username: '" + userName + "' and tablespace '" + tableSpaceName + "'...");

        // Now, the real work starts
        dbConnection = getDBConnection(request.getDatabaseServerInformation());

        if (!userConnected(dbConnection, userName))
        {
          if (databaseExists(dbConnection, tableSpaceName))
          {
            dropDatabase(dbConnection, tableSpaceName);
          }
          if (userExists(dbConnection, userName))
          {
            dropUser(dbConnection, userName);
          }
          getPDMFeedbackHandler().report("Database: " + userName + " deleted succesfully!");
        }
        else
        {
          throw getPDMFeedbackHandler().addException(ErrorDefinition.USER_STILL_CONNECTED, null, userName);
        }
      }
      finally
      {
        // PCIS 193113 / PCIS 193436:
        // Original dumpfile as well as analyze file and log file on database
        // server (if created) must be deleted after import.
        deleteLocalFile(localDumpFile);

        if (dbConnection != null)
        {
          closeConnection(dbConnection);
        }
      }
      reply.setPerformative(ACLMessage.INFORM); // Action successfully executed
    }
    catch (PDMException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE); // Action failed
      throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DELETING_DATABASE, ex.isErrorForAdministrator(), ex);
    }
    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
  }


  /**
   * creates delete response
   *
   * @return DeleteResponse
   */
  @Override protected DeleteResponse createResponse()
  {
    return new DeleteResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }


  /**
   * executes a select statement to determine if the database exists
   *
   * @param  aConnection database connection
   * @param  aTableSpace name of tablespace to look for
   *
   * @return true if tablespace exists in database, false otherwise
   *
   * @throws PDMException
   */
  private boolean databaseExists(Connection aConnection, String aTableSpace) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("Check if database exists.");
    pdmFeedbackHandler.report("\tSQL: '" + SQL_CHECK_EXISTANCE_TABLESPACENAME.replace("(?)", aTableSpace) + "'");

    try
    {
      if (!SKIP_DB_ACTIONS)
      {
        statement = aConnection.prepareStatement(SQL_CHECK_EXISTANCE_TABLESPACENAME, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        statement.setString(1, aTableSpace);
        ResultSet resultSet = statement.executeQuery();

        result = resultSet.next();
      }
      else
      {
        pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
        result = true;
      }
    }
    catch (SQLException ex)
    {
      exception = pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
    }
    return result;
  }


  /**
   * executes a select statement to determine if the user exists in the database
   *
   * @param  aConnection database connection
   * @param  aUserName   name of user to look for
   *
   * @return true if user exists in database, false otherwise
   *
   * @throws PDMException
   */
  private boolean userConnected(Connection aConnection, String aUserName) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("Check if user is connected.");
    pdmFeedbackHandler.report("\tSQL: '" + SQL_CHECK_IF_USER_CONNECTED.replace("(?)", aUserName) + "'");

    try
    {
      if (!SKIP_DB_ACTIONS)
      {
        statement = aConnection.prepareStatement(SQL_CHECK_IF_USER_CONNECTED);
        statement.setString(1, aUserName);
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next())
        {
          result = (resultSet.getInt(2) > 0);
        }
      }
      else
      {
        pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
      }
    }
    catch (SQLException ex)
    {
      exception = pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
    }
    return result;
  }


  /**
   * executes a select statement to determine if the user exists in the database
   *
   * @param  aConnection database connection
   * @param  aUserName   name of user to look for
   *
   * @return true if user exists in database, false otherwise
   *
   * @throws PDMException
   */
  private boolean userExists(Connection aConnection, String aUserName) throws PDMException
  {
    boolean result = false;
    PDMException exception = null;
    PreparedStatement statement = null;

    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("Check if user exists.");
    pdmFeedbackHandler.report("\tSQL: '" + SQL_CHECK_EXISTANCE_USERNAME.replace("(?)", aUserName) + "'");
    try
    {
      if (!SKIP_DB_ACTIONS)
      {
        statement = aConnection.prepareStatement(SQL_CHECK_EXISTANCE_USERNAME, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        statement.setString(1, aUserName);
        ResultSet resultSet = statement.executeQuery();

        result = resultSet.next();
      }
      else
      {
        pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
        result = true;
      }
    }
    catch (SQLException ex)
    {
      exception = pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
    }
    return result;
  }
}
