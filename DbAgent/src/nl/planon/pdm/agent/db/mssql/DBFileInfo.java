// Planon Enterprise Edition Source file: DBFileInfo.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db.mssql;

/**
 * contains information about database files like name of dump file and log file
 */
public class DBFileInfo
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private String dumpFileName;
  private String logFileName;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DBFileInfo object.
   *
   * @param aDumpFileName logical name (base) of dump file name
   * @param aLogFileName  logical name (base) of log file name
   */
  public DBFileInfo(String aDumpFileName, String aLogFileName)
  {
    this.dumpFileName = aDumpFileName;
    this.logFileName = aLogFileName;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets dump file name
   *
   * @return String containing path
   */
  public String getDumpFileName()
  {
    return this.dumpFileName;
  }


  /**
   * gets log file name
   *
   * @return String containing path
   */
  public String getLogFileName()
  {
    return this.logFileName;
  }
}
