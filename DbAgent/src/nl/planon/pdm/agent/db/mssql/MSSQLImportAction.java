// Planon Enterprise Edition Source file: MSSQLImportAction.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.agent.db.mssql;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.sql.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * MSSQLImportAction
 */
public class MSSQLImportAction extends MSSQLDBAgentAction<ImportResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String SUFFIX_LDF = "ldf";
  private static final String SUFFIX_MDF = "mdf";
  private static final String ANALYZE_LOG = "L"; // represent xxxx.ldf
  private static final String ANALYZE_DUMP = "D"; // represents xxxx.mdf

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an MSSQL import.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Download dump from webdav location to MSSQL shared directory</li>
   *   <li>Analyse dump on the MSSQL DB server. This will result in an SQL script that contains the
   *     structure of the dump</li>
   *   <li>Retrieve the tablespace(s) and schema(s) from the table structure.</li>
   *   <li>Import the datapump dump file into the DB</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  a MSSQLImportRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    boolean schemaAndTablespaceCreated = false;
    boolean databaseImported = false;

    ACLMessage reply = aInput.createReply();
    PDMFeedbackHandler feedbackHandler = getPDMFeedbackHandler();
    ImportResponse response = getResponseData();
    try
    {
      MSSQLImportRequest request = (MSSQLImportRequest) aConcept;
      response.fillMandatoryResponseData(request);
      IDatabaseDumpInformation dumpInformationRequest = request.getDumpInformation();

      String remoteDumpFileName = request.getDumpFileName();
      String databasePassword = request.getDatabasePassword();
      String userName = request.getSchemaName();
      String targetTablespace = request.getTableSpaceName();
      String dumpSchemaName = dumpInformationRequest.getSchemaName();
      String dumpTableSpaces = dumpInformationRequest.getTableSpaceNames();
      MSSQLDatabaseServerInformation databaseServerInformation = request.getDatabaseServerInformation();
      File datafileLocation = getDataFileLocationWithMaxFreeSpace(databaseServerInformation.getDataFileLocations()); // PCIS 193113: set datafilelocation for creating tablespace
      boolean noImport = request.getNoImport();
      PDMPrivilegeType pdmPrivilegeType = request.getPDMPrivilegeType();

      // Strip absolute path, we need the file name later.
      String dumpFileName = new File(remoteDumpFileName).getName();
      File localDumpFile = null;
      Connection dbConnection = null;
      try
      {
        feedbackHandler.report("Starting import of: '" + remoteDumpFileName + "' (file name '" + dumpFileName + "')...");

        // Now, the real work starts
        localDumpFile = retrieveFileFromWebDAV(remoteDumpFileName, getMSSQLSharedDirectory());

        dbConnection = getDBConnection(databaseServerInformation);

        String dumpDatabaseVersion = getVersionNumber(dbConnection, localDumpFile);
        // if version don't equal with the supported version of the database, throw a error.
        boolean isSupported = databaseServerInformation.isVersionSupported(dumpDatabaseVersion);
        if (!isSupported)
        {
          throw feedbackHandler.addException(ErrorDefinition.DUMP_VERSION_NOT_SUPPORT, null, dumpDatabaseVersion);
        }

        String databaseName = PDM_ + request.getSchemaName();
        createDatabase(dbConnection, databaseName, userName, datafileLocation);
        switch (pdmPrivilegeType.getCode())
        {
          case "A" :
          case "U" :

            createUser(dbConnection, databaseName, userName, databasePassword);
            break;

          case "S" :

            createLoginAndUserWithStandardPrivileges(dbConnection, databaseName, userName, databasePassword);
            break;

          default :

            assert false : "Privilege Type: " + pdmPrivilegeType.getCode() + " never exists!!";
            break;
        }
        schemaAndTablespaceCreated = true;

        if ((dumpSchemaName == null) || (dumpTableSpaces == null))
        {
          // Figure out what the old schema name and tablespace are, by analyzing the dump.
          DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
          dumpInformation.setVersionNumber(dumpDatabaseVersion);
        }

        // do not fill the schemaname and tablespace name in case the request is a reload export
        // (so only in case the request is a new import or a reload initial).
        if (!PDMRequestType.RELOAD_EXPORT.equals(request.getPDMRequestType()))
        {
          DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
          dumpInformation.setSchemaName(getDatabaseUser());
          dumpInformation.setTableSpaceNames(getDatabaseName());
        }

        // noImport =true : create only new schema and tablespace
        if (noImport)
        {
          feedbackHandler.report("Load of dump " + localDumpFile.getAbsolutePath() + " skipped as specified!");
        }
        else
        {
          // analyze the Logical name of the dump
          DBFileInfo dbFileInfo = analyzeFileListOnlyDump(dbConnection, localDumpFile);
          String logicalName = dbFileInfo.getDumpFileName();
          String logName = dbFileInfo.getLogFileName();

          // import dump
          importDumpOnDB(dbConnection, localDumpFile, logName, databaseName, logicalName, datafileLocation.getAbsolutePath());
          databaseImported = true;
        }

        if ("U".equals(pdmPrivilegeType.getCode())) // Only link if the selected privilege type is: U
        {
          linkDBToUser(dbConnection, databaseName, userName);
        }
        if (!noImport)
        {
          setPlanonDBVersionDetailsTODumpInfo(response, dbConnection, databaseName);
        }
        feedbackHandler.report("Dump of: " + localDumpFile.getAbsolutePath() + " loaded succesfully!");
      }
      finally
      {
        if (dbConnection != null)
        {
          // PCIS 207241: if the exception is raised after the schema and tablespace is created, then these have to be deleted again.
          // Also TEC-2714, PDM3: reloading exported database don't drop user and tablespace if it fails to load in oracle
          if (schemaAndTablespaceCreated && !databaseImported && !noImport)
          {
            getPDMFeedbackHandler().report("The schema and the tablespace were created successfully but the db import is not succesful.");
            getPDMFeedbackHandler().report("Hence, dropping the schema and tablespaces...");
            dropUser(dbConnection, userName);
            dropDatabase(dbConnection, targetTablespace);
            getPDMFeedbackHandler().report("The schema and tablespaces were dropped successfully.");
          }
          // PCIS 193113 / PCIS 193436:
          // Original dumpfile as well as analyze file and log file on database server (if created) must be deleted after import.
          deleteLocalFile(localDumpFile);

          closeConnection(dbConnection);
        }
      }
      reply.setPerformative(ACLMessage.INFORM); // Action successfully executed
    }
    catch (PDMException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE); // Action failed
      throw ex;
    }
    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
    AbstractPDMAgent agent = getAgent();
    assert PDMFileType.MSSQL_DUMP.equals(agent.getFileType()) : "agent FileType(" + agent.getFileType() + ") must be equal to " + PDMFileType.MSSQL_DUMP;
    getResponseData().setDatabaseServerName(agent.getDbServerName());
    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) getResponseData().getDumpInformation();
    dumpInformation.setDatabaseType(agent.getDbType());
    dumpInformation.setFileType(PDMFileType.MSSQL_DUMP);
  }


  /**
   * creates new instance of ImportResponse
   *
   * @return ImportResponse
   */
  @Override protected ImportResponse createResponse()
  {
    return new ImportResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }


  /**
   * analyze the logicalnames and types of the dump
   *
   * @param  aDBConnection DBConnection
   * @param  aDatafile     DumpFile
   *
   * @return DBFileInfo, contains null logical names if db actions are skipped
   *
   * @throws PDMException error on analyzing dump file
   */
  private DBFileInfo analyzeFileListOnlyDump(Connection aDBConnection, File aDatafile) throws PDMException
  {
    PDMException exception = null;
    Statement statement = null;
    try
    {
      String logFileName = null;
      String dumpFileName = null;

      // analyze the Logical name

      if (!SKIP_DB_ACTIONS)
      {
        String absolutePath = aDatafile.getAbsolutePath();
        String queryAnalyze = "Restore FILELISTONLY from disk= '" + absolutePath + "'";
        statement = aDBConnection.createStatement();
        ResultSet resultSet = statement.executeQuery(queryAnalyze);
        ResultSetMetaData rsmd = resultSet.getMetaData();
        int colums = rsmd.getColumnCount();
        while (resultSet.next())
        {
          String type = resultSet.getString("Type");
          String logicalName = resultSet.getString("LogicalName");
          assert ANALYZE_DUMP.equals(type) || ANALYZE_LOG.equals(type);
          if (ANALYZE_DUMP.equals(type))
          {
            assert dumpFileName == null : "set dump file name only once";
            dumpFileName = logicalName;
          }
          if (ANALYZE_LOG.equals(type))
          {
            assert logFileName == null : "set log file name only once";
            logFileName = logicalName;
          }
        }
      }
      DBFileInfo result = new DBFileInfo(dumpFileName, logFileName);
      return result;
    }
    catch (SQLException ex)
    {
      exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, ex, (String) null);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE);
    }
  }


  /**
   * create database
   *
   * @param  aDBConnection db connection to use
   * @param  aDatabaseName name of database
   * @param  aUserName     user name to use
   * @param  aDatafile     db Datafile!
   *
   * @throws PDMException When bad things happen.
   */
  private void createDatabase(Connection aDBConnection, String aDatabaseName, String aUserName, File aDatafile) throws PDMException
  {
    assert aDatafile != null : "DataFile can't be null";
    assert aUserName != null : "aUserName can't be null";

    // create Database
    String queryCreateDatabase = "USE master ; CREATE DATABASE [" + aDatabaseName + "]"
    + " ON  ( NAME = " + aUserName + "_dat,"
    + " FILENAME = '" + aDatafile.getPath() + "\\" + aUserName + "." + SUFFIX_MDF + "')"
    + " LOG ON ( NAME = " + aUserName + "_log,"
    + " FILENAME = '" + aDatafile.getPath() + "\\" + aUserName + "." + SUFFIX_LDF + "')"
    + " ALTER DATABASE [" + PDM_ + aUserName + "] SET AUTO_SHRINK ON";

    executeSQL("Create db", queryCreateDatabase, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);
  }


  /**
   * Create Login, User and grants various privileges the user. This will actually give the minimal
   * rights to run our Planon application.
   *
   * @param  aDBConnection db connection to use
   * @param  aDatabaseName name of database
   * @param  aUserName     user name to use
   * @param  aPassword     password of user
   *
   * @throws PDMException When bad things happen.
   *
   * @see    http://planonwiki.planon-fm.com/development/index.php?title=Sqlserver_permissions
   */
  private void createLoginAndUserWithStandardPrivileges(Connection aDBConnection, String aDatabaseName, String aUserName, String aPassword) throws PDMException
  {
    assert aUserName != null : "aUserName can't be null";

    String queryCreateLoginName = "CREATE LOGIN [" + aUserName + "] WITH PASSWORD = '" + aPassword + "'," //
    + " DEFAULT_DATABASE = [" + aDatabaseName + "]," //
    + " DEFAULT_LANGUAGE=[us_english]," //
    + " CHECK_EXPIRATION=OFF," //
    + " CHECK_POLICY=OFF" //
    + " ALTER LOGIN [" + aUserName + "] ENABLE";

    executeSQL("Create Login", queryCreateLoginName, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);

    String queryGrantViewServerState = "USE master; GRANT VIEW SERVER STATE TO " + aUserName;
    executeSQL("Grant View Server State", queryGrantViewServerState, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);

    String queryCreateUser = "USE [" + aDatabaseName + "]; CREATE USER  " + aUserName + "_USER FOR LOGIN " + aUserName;
    executeSQL("Create User", queryCreateUser, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);

    String queryGrantConnect = "USE [" + aDatabaseName + "]; GRANT CONNECT TO " + aUserName + "_USER";
    executeSQL("Grant Connect", queryGrantConnect, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);

    String queryGrantSelect = "USE [" + aDatabaseName + "]; GRANT SELECT,INSERT,UPDATE,DELETE TO " + aUserName + "_USER";
    executeSQL("Grant Select", queryGrantSelect, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);

    String queryGrantExecute = "USE [" + aDatabaseName + "]; GRANT EXECUTE TO " + aUserName + "_USER";
    executeSQL("Grant Execute", queryGrantExecute, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);
  }


  /**
   * create user (with Upgrade privielges.)
   *
   * @param  aDBConnection db connection to use
   * @param  aDatabaseName name of database
   * @param  aUserName     user name to use
   * @param  aPassword     password of user
   *
   * @throws PDMException When bad things happen.
   *
   * @see    http://planonwiki.planon-fm.com/development/index.php?title=Sqlserver_permissions
   */
  private void createUser(Connection aDBConnection, String aDatabaseName, String aUserName, String aPassword) throws PDMException
  {
    assert aUserName != null : "aUserName can't be null";

    String queryCreateLoginName = "CREATE LOGIN [" + aUserName + "] WITH PASSWORD = '" + aPassword + "'," //
    + " DEFAULT_DATABASE = [" + aDatabaseName + "]," //
    + " DEFAULT_LANGUAGE=[us_english]," //
    + " CHECK_EXPIRATION=OFF," //
    + " CHECK_POLICY=OFF" //
    + " ALTER LOGIN [" + aUserName + "] ENABLE";

    executeSQL("Create user", queryCreateLoginName, aDBConnection, ErrorDefinition.SQL_ERROR_CREATING_TABLESPACE);
  }


  /**
   * Perform the actual import on the DB server.
   *
   * @param  aDbConnection     The DB connection
   * @param  aDumpFile         The dump file, as it exists in the shared oracle directory.
   * @param  aLogicalNameLog   Logical name of the log file from the dump file.
   * @param  aDatabaseName     name of data base
   * @param  aLogicalNameDB    logical database name from the dump file. out by the analyse step.
   * @param  aDatafileLocation Database Location on the database server
   *
   * @throws PDMException When bad things happen.
   */
  private void importDumpOnDB(Connection aDbConnection, File aDumpFile, String aLogicalNameLog, String aDatabaseName, String aLogicalNameDB, String aDatafileLocation) throws PDMException
  {
    String dumpName = aDumpFile.getName();
    int d = dumpName.indexOf(".");
    if (d > 0)
    {
      dumpName = dumpName.substring(0, d);
    }

    String importDsDump = "USE master" //
    + " RESTORE DATABASE [" + aDatabaseName + "]" //
    + " FROM DISK = " + "'" + aDumpFile.toString() + "'" //
    + " WITH " //
    + " MOVE '" + aLogicalNameDB + "' TO '" + aDatafileLocation + "\\" + dumpName + "." + SUFFIX_MDF + "'," //
    + " MOVE '" + aLogicalNameLog + "' TO '" + aDatafileLocation + "\\" + dumpName + "_LOG" + "." + SUFFIX_LDF + "',"
    + " REPLACE";

    executeSQL("Import step", importDsDump, aDbConnection, ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE);
  }


  /**
   * Link the database to owner
   *
   * @param  aDbConnection The DB connection
   * @param  aDatabaseName the name of the database to change the owner for
   * @param  aUserName     The user name to link
   *
   * @throws PDMException When bad things happen.
   */
  private void linkDBToUser(Connection aDbConnection, String aDatabaseName, String aUserName) throws PDMException
  {
    String sql = "USE [" + aDatabaseName + "];" //
    + " exec sp_changedbowner [" + aUserName + "];";

    executeSQL("Link db to owner step", sql, aDbConnection, ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE);
  }
}
