// Planon Enterprise Edition Source file: MSSQLDBAgentAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.mssql;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * Provides an abstract base class for DBAgent actions that have MSSQL interaction.
 *
 * <p>When creating a DBAgent action, this is the class to extend from. It contains a few helper
 * methods to make life a little easier for your day-to-day use.</p>
 */
public abstract class MSSQLDBAgentAction<RESPONSE extends AbstractResponse> extends AbstractDBAgentAction<RESPONSE>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String MSSQL_VERSION_2008_R2 = "2008 R2";
  private static final String MSSQL_VERSION_2008 = "2008";
  private static final String MSSQL_VERSION_2005 = "2005";
  private static final String MSSQL_VERSION_2000 = "2000";
  private static final String MSSQL_VERSION_7_0 = "7.0";
  private static final Map<Integer, String> DB_VERSIONS;

  static
  {
    DB_VERSIONS = new HashMap<Integer, String>();
    DB_VERSIONS.put(Integer.valueOf(515), MSSQL_VERSION_7_0);
    DB_VERSIONS.put(Integer.valueOf(639), MSSQL_VERSION_2000);
    DB_VERSIONS.put(Integer.valueOf(611), MSSQL_VERSION_2005);
    DB_VERSIONS.put(Integer.valueOf(612), MSSQL_VERSION_2005);
    DB_VERSIONS.put(Integer.valueOf(655), MSSQL_VERSION_2008);
    DB_VERSIONS.put(Integer.valueOf(661), MSSQL_VERSION_2008_R2);
  }

  private static final String HDR_SOFTWARE_VERSION_MINOR = "SoftwareVersionMinor";
  private static final String HDR_SOFTWARE_VERSION_MAJOR = "SoftwareVersionMajor";
  private static final String HDR_USER_NAME = "UserName";
  private static final String HDR_DATABASE_NAME = "DatabaseName";

// The file location property of the MSSQL shared directory.
  private static final String MSSQL_DIRECTORY_PROPERTY = "MSSQLSharedDirectory";

// This is the default identifier for the shared MSSQL data directory.
  private static final String MSSQL_SHARED_DIR_IDENTIFIER_DEFAULT = "MSSQL";
// Use -DMSSQLSharedDirectoryIdentifier=MyID to set the identifier.
  private static final String MSSQL_SHARED_DIR_IDENTIFIER_PROPERTY = "MSSQLSharedDirectoryIdentifier";
  private static final String SQL_TO_DROP_USER = "DROP LOGIN $USER$"; // drop user and all objects in the user's schema
  private static final String SQL_TO_DROP_DATABASE = "DROP DATABASE $TBLSPC$"; // drop tablespace and all associated operating system datafiles
  private static final String HOSTNAME = "hostname";
  private static final String SQL_GET_CONNECTED_USERS = " select sysdatabases.name dbname,"
  + " sysusers.name username,"
  + " sysprocesses.loginame,"
  + " sysprocesses." + HOSTNAME + ","
  + " sysprocesses.program_name,"
  + " sysprocesses.login_time"
  + " from master.dbo.sysprocesses sysprocesses,"
  + " master.dbo.sysdatabases sysdatabases,"
  + " master.dbo.sysusers sysusers"
  + " where sysdatabases.dbid = sysprocesses.dbid"
  + " and sysusers.uid = sysprocesses.uid"
  + " and sysdatabases.name ='$DATABASENAME$'";
  private static final String SQL_ANALYZE = "Restore Headeronly from disk= '$ABSOLUTE_PATH$'";

  //~ Instance Variables ---------------------------------------------------------------------------

  private String databaseName = null;
  private String databaseUser = null;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * drop database including contents
   *
   * @param  aDBConnection db connection to use
   * @param  aDatabaseName name of database to delete
   *
   * @throws PDMException
   */
  protected void dropDatabase(Connection aDBConnection, String aDatabaseName) throws PDMException
  {
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    pdmFeedbackHandler.report("  * Drop database step");
    PDMException exception = null;
    PreparedStatement statement = null;

    if (!SKIP_DB_ACTIONS)
    {
      try
      {
        String sqlStatement = SQL_TO_DROP_DATABASE.replaceAll("\\$TBLSPC\\$", Matcher.quoteReplacement(aDatabaseName));
        PreparedStatement pstmtDropTableSpace = aDBConnection.prepareStatement(sqlStatement);
        pstmtDropTableSpace.execute();
      }
      catch (SQLException ex)
      {
        // get connecting users.
        String sqlStatement = SQL_GET_CONNECTED_USERS.replaceAll("\\$DATABASENAME\\$", Matcher.quoteReplacement(aDatabaseName));
        try
        {
          statement = aDBConnection.prepareStatement(sqlStatement);
          ResultSet rs = statement.executeQuery();
          if (rs.next())
          {
            pdmFeedbackHandler.report("Users still connected:");
            do
            {
              pdmFeedbackHandler.report("-----> " + rs.getString(HOSTNAME));
            }
            while (rs.next());
          }
        }
        catch (SQLException e)
        {
          exception = pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
          throw exception;
        }
        exception = pdmFeedbackHandler.addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
        throw exception;
      }
      finally
      {
        closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_DROPPING_DATABASE);
      }
      pdmFeedbackHandler.report(FEEDBACK_SUCCESS);
    }
    else
    {
      pdmFeedbackHandler.report(FEEDBACK_SIMULATING);
    }
  }


  /**
   * drop user cascading
   *
   * @param  aDBConnection db connection to use
   * @param  aUserName     name of user to delete
   *
   * @throws PDMException
   */
  protected void dropUser(Connection aDBConnection, String aUserName) throws PDMException
  {
    getPDMFeedbackHandler().report("  * Drop user step");

    String sqlStatement = SQL_TO_DROP_USER.replaceAll("\\$USER\\$", Matcher.quoteReplacement(aUserName));
    getPDMFeedbackHandler().report("\tSQL: '" + sqlStatement + "'");
    if (!SKIP_DB_ACTIONS)
    {
      try
      {
        PreparedStatement pstmtDropUser = aDBConnection.prepareStatement(sqlStatement);
        pstmtDropUser.execute();
      }
      catch (SQLException ex)
      {
        throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_DROPPING_DATABASE, ex);
      }
      getPDMFeedbackHandler().report(FEEDBACK_SUCCESS);
    }
    else
    {
      getPDMFeedbackHandler().report(FEEDBACK_SIMULATING);
    }
  }


  /**
   * get the database name from the MSSQl database dump
   *
   * @return
   */
  protected String getDatabaseName()
  {
    return this.databaseName;
  }


  /**
   * Get the database user from a MSSQL dump
   *
   * @return Database user
   */
  protected String getDatabaseUser()
  {
    return this.databaseUser;
  }


  /**
   * Returns a File object that points to the shared MSSQL data directory.
   *
   * <p>The MSSQL data directory can be passed in the system property, named as specified in the
   * constant MSSQL_DIRECTORY_PROPERTY.</p>
   *
   * <p>Example:</p>
   *
   * <pre>
    -DMSSQLSharedDirectory=C:\oraclePE\directory
  * </pre>
   *
   * <p>If the property is not set, the directory does not exist, or the file does not denote a
   * directory, a RuntimeException will be thrown.</p>
   *
   * @return A file object pointing to the directory that is used to share files with MSSQL.
   *
   * @throws RuntimeException When the user has made a configureation mistake.
   */
  protected File getMSSQLSharedDirectory()
  {
    String aMSSQLSharedDirectory = System.getProperty(MSSQL_DIRECTORY_PROPERTY);
    if ((aMSSQLSharedDirectory == null) || (aMSSQLSharedDirectory.trim().length() == 0))
    {
      throw new RuntimeException("MSSQL data share directory should be passed with -D" + MSSQL_DIRECTORY_PROPERTY);
    }
    File result = new File(aMSSQLSharedDirectory);
    if (!result.exists() || !result.isDirectory())
    {
      throw new RuntimeException("MSSQL shared directory passed in -D" + MSSQL_DIRECTORY_PROPERTY + " with value '" + aMSSQLSharedDirectory + "' should exist as a directory");
    }
    return result;
  }


  /**
   * get identifier that holds Shared directory of Oracle via system properties. Use default value
   * if not defined.
   *
   * @return identifier that holds Shared directory of MSSQL
   */
  protected String getOracleSharedDirectoryIdentifier()
  {
    return System.getProperty(MSSQL_SHARED_DIR_IDENTIFIER_PROPERTY, MSSQL_SHARED_DIR_IDENTIFIER_DEFAULT);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getTablePrefix(String aDatabaseSchemaName)
  {
    return aDatabaseSchemaName.concat(".dbo");
  }


  /**
   * analyse db and get results, check if the database server version is higher or equal with the
   * dumpversion. when it isn't, throw a exception.
   *
   * <p>setDatabaseName and setdatabaseUser</p>
   *
   * @param  aDBConnection DBconnection
   * @param  aDatafile     dump file for analyze
   *
   * @return String
   *
   * @throws PDMException
   */
  protected String getVersionNumber(Connection aDBConnection, File aDatafile) throws PDMException
  {
    String absolutePath = aDatafile.getAbsolutePath().replaceAll("\\\\", "/");
    String versionNumber = null;
    PDMException exception = null;
    Statement statement = null;
    // analyze dump query

    String sqlStatement = SQL_ANALYZE.replaceAll("\\$ABSOLUTE_PATH\\$", absolutePath);
    try
    {
      statement = aDBConnection.createStatement();
      ResultSet resultSet = statement.executeQuery(sqlStatement);
      while (resultSet.next())
      {
        setDatabaseName(resultSet.getString(HDR_DATABASE_NAME));
        setDatabaseUser(resultSet.getString(HDR_USER_NAME));
        String versionNumberMajor = resultSet.getString(HDR_SOFTWARE_VERSION_MAJOR);
        String versionNumberMinor = resultSet.getString(HDR_SOFTWARE_VERSION_MINOR);
        if (versionNumberMajor == null)
        {
          throw getPDMFeedbackHandler().addException(ErrorDefinition.CORRUPTED_DUMP_FILE, null);
        }
        versionNumberMajor = (versionNumberMajor.length() == 1) ? (versionNumberMajor = "0" + versionNumberMajor) : versionNumberMajor;
        versionNumber = versionNumberMajor + versionNumberMinor;
      }
      // version xx.xx.xx.xx.xx
      versionNumber = formatTenDigitVersionNumber(versionNumber);

      getPDMFeedbackHandler().report("Dump version number : " + versionNumber);
    }
    catch (SQLException ex)
    {
      if (ex.getMessage().contains("The media family on device") && ex.getMessage().contains("is incorrectly formed. SQL Server cannot process this media family."))
      {
        exception = getPDMFeedbackHandler().addException(ErrorDefinition.NOT_MSSQL_DUMPFILE, null);
        throw exception;
      }

      exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_ANALYZING_DB_DUMPFILE);
    }
    return versionNumber;
  }


  /**
   * set database Name
   *
   * @param aDatabaseName
   */
  protected void setDatabaseName(String aDatabaseName)
  {
    this.databaseName = aDatabaseName;
  }


  /**
   * Set database user
   *
   * @param aDatabaseUser
   */
  protected void setDatabaseUser(String aDatabaseUser)
  {
    this.databaseUser = aDatabaseUser;
  }


  /**
   * convert the database version number as returned by the database to a more meaningful version
   * number of MSSQL
   *
   * @param  aDatabaseversion Verion number.
   *
   * @return Database version. null when it does not match.
   */
  private String getDatabaseTranslation(int aDatabaseversion)
  {
    return DB_VERSIONS.get(Integer.valueOf(aDatabaseversion));
  }
}
