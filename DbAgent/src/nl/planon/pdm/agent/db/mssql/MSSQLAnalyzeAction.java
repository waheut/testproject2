// Planon Enterprise Edition Source file: MSSQLAnalyzeAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.mssql;

import jade.content.*;
import jade.lang.acl.*;
import jade.util.leap.*;

import java.io.*;
import java.sql.*;

import nl.planon.pdm.agent.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * analyzes mssql database (does not import)
 */
public class MSSQLAnalyzeAction extends MSSQLDBAgentAction<AnalyzeResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  protected static final String LOGFILE_POSTFIX = "_analyze";

  private static String databaseName = null;
  private static String databaseUser = null;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an MSSQL import.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Download dump from webdav location to MSSQL shared directory</li>
   *   <li>Analyse dump on the DB server. This will result in an SQL script that contains the
   *     structure of the dump</li>
   *   <li>Retrieve the tablespace(s) and schema(s) from the table structure.</li>
   *   <li>Import the dump file into the DB</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  a MSSQLAnalyzeRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    boolean schemaAndTablespaceCreated = false;
    boolean databaseImported = false;

    ACLMessage reply = aInput.createReply();
    reply.setEncoding("UTF-8");

    AnalyzeResponse response = getResponseData();
    try
    {
      MSSQLAnalyzeRequest request = (MSSQLAnalyzeRequest) aConcept;
      response.fillMandatoryResponseData(request);

      Integer pdmLocation = request.getDatabaseLocation();
      response.setDatabaseLocation(pdmLocation);

      // Strip absolute path, we need the file name later.
      String remoteDumpFileName = request.getDumpFileName();
      String dumpFileName = new File(remoteDumpFileName).getName();
      File localDumpFile = null;

      Connection dbConnection = null;
      try
      {
        getPDMFeedbackHandler().report("Starting analyze of: '" + remoteDumpFileName + "' (file name '" + dumpFileName + "')...");

        // Now, the real work starts

        localDumpFile = retrieveFileFromWebDAV(remoteDumpFileName, getMSSQLSharedDirectory());

        dbConnection = getDBConnection(request.getDatabaseServerInformation());

        // Figure out what the old schema name and tablespace are, by analyzing the dump.
        DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
        dumpInformation.setVersionNumber(getVersionNumber(dbConnection, localDumpFile));

        getPDMFeedbackHandler().report("Dump " + localDumpFile.getAbsolutePath() + " analyzed succesfully!");
      }
      finally
      {
        // PCIS 193113 / PCIS 193436:
        // Original dumpfile as well as analyze file and log file on database server (if created) must be deleted after import.
        deleteLocalFile(localDumpFile);
        if (dbConnection != null)
        {
          closeConnection(dbConnection);
        }
      }
      reply.setPerformative(ACLMessage.INFORM); // Action successfully executed
    }
    catch (PDMException ex)
    {
      // if its a admin error, the analyze is finish correctly and the import can start.
      if (ex.isErrorForAdministrator())
      {
        reply.setPerformative(ACLMessage.INFORM); // Action finish correctly
      }
      else
      {
        reply.setPerformative(ACLMessage.FAILURE); // Action failed
      }

      throw ex;
    }
    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
    AbstractPDMAgent agent = getAgent();
    AnalyzeResponse response = getResponseData();
    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) response.getDumpInformation();
    assert PDMFileType.MSSQL_DUMP.equals(agent.getFileType());
    dumpInformation.setDatabaseType(agent.getDbType());
    dumpInformation.setFileType(PDMFileType.MSSQL_DUMP);
    List list = new ArrayList(); // this is not a java.util.List because there is \\
                                 // no ontology for that (also no generics here)
    list.add(PDMFileType.MSSQL_DUMP);
    response.setTriedFileType(list);
  }

  /**
   * creates new instance of AnalyzeResponse
   *
   * @return AnalyzeResponse
   */
  @Override protected AnalyzeResponse createResponse()
  {
    return new AnalyzeResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }
}
