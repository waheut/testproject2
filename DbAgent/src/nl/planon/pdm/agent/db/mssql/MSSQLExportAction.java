// Planon Enterprise Edition Source file: MSSQLExportAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db.mssql;

import jade.content.*;
import jade.lang.acl.*;

import java.io.*;
import java.sql.*;
import java.util.regex.*;

import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;
import nl.planon.pdm.core.datamessage.response.*;

/**
 * MSSQLExportAction
 */
public class MSSQLExportAction extends MSSQLDBAgentAction<ExportResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String MSSQL_EXPRESS = "EXPRESS";
  private static final String DOT = "\\.";
  private static final String LOGFILE_POSTFIX = "_export";
  private static final String DUMPFILE_EXTENSION = ".bak";
  private static final String BACKUP_DATABASE = "BACKUP DATABASE $DATABASE$ TO DISK = $LOCATION$";
  private static final String COMPRESSED_BACKUP = " WITH COMPRESSION";
  private static final String SQL_GET_VERSION_DATABASE = "SELECT " //
  + " CONVERT(varchar(100), SERVERPROPERTY('ProductVersion')), " //
  + " CONVERT(varchar(200), SERVERPROPERTY ('edition'))";

  //~ Instance Variables ---------------------------------------------------------------------------

  private Integer pkPDMDatabase;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * <p>Perform all actions needed to perform an oracle import MSSQL.</p>
   *
   * <p>Actions performed:</p>
   *
   * <ol>
   *   <li>Open DB connection</li>
   *   <li>Download dump from webdav location to oracle shared directory</li>
   *   <li>Import the MSSQL dump file into the DB</li>
   * </ol>
   *
   * @param  aInput   The request message
   * @param  aConcept The concept derived from the request, this is added for convenience. Should be
   *                  an MSSQLDataPumpRequest.
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  @Override public ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException
  {
    ACLMessage reply = aInput.createReply();

    MSSQLExportRequest abstractRequest = (MSSQLExportRequest) aConcept;
    ExportResponse response = getResponseData();
    response.fillMandatoryResponseData(abstractRequest);
    String user = abstractRequest.getSchemaName();
    this.pkPDMDatabase = abstractRequest.getPrimaryKeyDatabase();

    int timeOutInSeconds = getTaskTimeOut(abstractRequest);
    String dumpFileName = createNameExportedDump(abstractRequest);
    String dumpfileShareDir = getMSSQLSharedDirectory().toString();
    Connection dbConnection = getDBConnection(abstractRequest.getDatabaseServerInformation());
    boolean commpressBackup = isCompressionSupported(dbConnection);
    try
    {
      getPDMFeedbackHandler().report("  * Export step " + (commpressBackup ? "compressed " : "") + "(max " + timeOutInSeconds + " seconds)");
      String sql = BACKUP_DATABASE.replaceAll("\\$DATABASE\\$", Matcher.quoteReplacement(PDM_ + user));
      if (commpressBackup)
      {
        sql += COMPRESSED_BACKUP;
      }

      sql = sql.replaceAll("\\$LOCATION\\$", Matcher.quoteReplacement("'" + dumpfileShareDir + "/" + dumpFileName + "'"));
      getPDMFeedbackHandler().report("\tExport command: '" + sql + "'");
      if (!SKIP_DB_ACTIONS)
      {
        PreparedStatement pstmt = dbConnection.prepareStatement(sql);

        pstmt.setQueryTimeout(timeOutInSeconds);
        pstmt.execute();
      }
      else
      {
        try
        {
          new File(dumpfileShareDir, dumpFileName).createNewFile();
        }
        catch (IOException ex)
        {
          throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, null);
        }
        getPDMFeedbackHandler().report(FEEDBACK_SIMULATING);
      }
      reply.setPerformative(ACLMessage.INFORM); // Action succeeded
    }
    catch (SQLException ex)
    {
      reply.setPerformative(ACLMessage.FAILURE); // Action failed
      throw getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, ex);
    }
    finally
    {
      response.setDumpFileDir(dumpfileShareDir);
      response.setDumpFileName(dumpFileName);
    }

    return reply;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void fillResponseData()
  {
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aRequest DOCUMENT ME!
   *
   * @throws PDMException DOCUMENT ME!
   */
  @Override public void responseFilesToWebDAV(AbstractRequest aRequest) throws PDMException
  {
    ExportResponse response = getResponseData();

    String originalDumpFile = aRequest.getDumpFileName();
    String resultDumpFile = createNameExportedDump(aRequest);

    File localFile = new File(getMSSQLSharedDirectory(), resultDumpFile);
    if (localFile != null)
    {
      sendFileToWebDAVLocation(localFile, aRequest.getDumpFileDir(), resultDumpFile);
      deleteLocalFile(localFile);
    }

    response.setDumpFileDir(aRequest.getDumpFileDir());
    response.setDumpFileName(resultDumpFile);

    if (aRequest.getDumpFileName() != null)
    {
      deleteLocalFile(new File(getMSSQLSharedDirectory(), response.getDumpFileName()));
    }

    super.responseFilesToWebDAV(aRequest);
  }


  /**
   * creates new instance of ExportResponse
   *
   * @return ExportResponse
   */
  @Override protected ExportResponse createResponse()
  {
    return new ExportResponse();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getDumpFileExtension()
  {
    return DUMPFILE_EXTENSION;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected String getLogfilePostfix()
  {
    return LOGFILE_POSTFIX;
  }


  /**
   * gets the server version string, eg, 10.50.XXXXX, second value in result contains edition
   *
   * @param  aDBConnection DBconnection
   *
   * @return String
   *
   * @throws PDMException
   */
  protected String[] getServerVersion(Connection aDBConnection) throws PDMException
  {
    String versionNumber = null;
    String edition = null;
    PDMException exception = null;
    Statement statement = null;
    try
    {
      statement = aDBConnection.createStatement();
      ResultSet rs = statement.executeQuery(SQL_GET_VERSION_DATABASE);
      rs.next();
      versionNumber = rs.getString(1); // there must be data, otherwise crash..
      edition = rs.getString(2); // there must be data, otherwise crash..
      String[] result = new String[2];
      result[0] = versionNumber;
      result[1] = edition;
      return result;
    }
    catch (SQLException ex)
    {
      exception = getPDMFeedbackHandler().addException(ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE, ex);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.ERROR_ON_EXPORTING_DUMPFILE);
    }
  }


  /**
   * checks if database server supported compressed exports
   *
   * @param  aDbConnection
   *
   * @return 9, 10, 11, ...
   *
   * @throws PDMException
   */
  private boolean isCompressionSupported(Connection aDbConnection) throws PDMException
  {
    String[] versionInfo = getServerVersion(aDbConnection);
    String version = versionInfo[0];
    String edition = versionInfo[1];
    getPDMFeedbackHandler().report("  * DB server version " + version + ", edition " + edition);

    String majorString = version.split(DOT)[0];
    int majorNr = Integer.valueOf(majorString).intValue();
    if (majorNr <= 9)
    {
      return false;
    }
    if (edition == null)
    {
      return false;
    }
    return !edition.toUpperCase().contains(MSSQL_EXPRESS);
  }
}
