// Planon Enterprise Edition Source file: PDMDbAgentUtilities.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.db;

import java.util.*;

/**
 * PDMDbAgentUtilities
 */
public class PDMDbAgentUtilities
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDbAgentUtilities object.
   */
  private PDMDbAgentUtilities()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * converts list to string each item separated by aSeparator
   *
   * @param  aList      list of items
   * @param  aSeparator separator to use between items
   *
   * @return separated list
   */
  public static String convertSeparatedList(List<String> aList, String aSeparator)
  {
    StringBuilder builder = new StringBuilder();
    for (String print : aList)
    {
      boolean first = builder.length() == 0;
      if (!first)
      {
        builder.append(aSeparator);
      }
      builder.append(print);
    }
    return builder.toString();
  }


  /**
   * converts set to string each item separated by aSeparator
   *
   * @param  aSet       set of items
   * @param  aSeparator separator to use between items
   *
   * @return separated list
   */
  public static String convertSeparatedList(Set<String> aSet, String aSeparator)
  {
    StringBuilder builder = new StringBuilder();
    for (String print : aSet)
    {
      boolean first = builder.length() == 0;
      if (!first)
      {
        builder.append(aSeparator);
      }
      builder.append(print);
    }
    return builder.toString();
  }
}
