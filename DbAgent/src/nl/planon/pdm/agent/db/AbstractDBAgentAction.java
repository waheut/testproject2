// Planon Enterprise Edition Source file: AbstractDBAgentAction.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.db;

import jade.content.*;
import jade.lang.acl.*;

import org.apache.commons.httpclient.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.*;

import nl.planon.pdm.agent.db.db.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.error.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.response.*;

import nl.planon.util.webdav.*;

/**
 * AbstractDBAgentAction
 *
 * @version $Revision$
 */
public abstract class AbstractDBAgentAction<RESPONSE extends AbstractResponse>
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final int HTTP_404 = 404;
  public static final String FEEDBACK_SIMULATING = "    ***(SIMULATING)*** ";
  public static final String FEEDBACK_SUCCESS = "    --> success!";
  public static final String FEEDBACK_FAILURE = "    --> failure!";
  protected static final String LOGFILE_POSTFIX = "_import";
  protected static final String PDM_ = "PDM_";

  private static final String SUFFIX_LOG = "log";
  public static final int DEFAULT_TIMEOUT = 14400; // 4 hours  needed by Imp and exp 4 * 60 * 60
  // For testing purposes these can be set to true.
  private static final String SKIP_DB_ACTIONS_PROPERTY = "SkipDbActions";

  protected static final boolean SKIP_DB_ACTIONS = Boolean.getBoolean(SKIP_DB_ACTIONS_PROPERTY);

  //~ Instance Variables ---------------------------------------------------------------------------

  private AbstractPDMAgent pdmAgent;
  private RESPONSE response;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AbstractDBAgentAction object.
   */
  public AbstractDBAgentAction()
  {
    this.response = createResponse();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Implement the execute action for your special action
   *
   * @param  aInput   The input message
   * @param  aConcept The concept message
   *
   * @return The reply message.
   *
   * @throws PDMException
   */
  public abstract ACLMessage execute(ACLMessage aInput, Concept aConcept) throws PDMException;


  /**
   * Fill data for response to controller agent.
   */
  public abstract void fillResponseData();


  /**
   * gets agent that instantiated this db action
   *
   * @return AbstractPDMAgent
   */
  public AbstractPDMAgent getAgent()
  {
    return this.pdmAgent;
  }


  /**
   * Returns the response data object for this type of DBAgentAction.
   *
   * <p>This has a relation with the result of the execute, however, the response data will also be
   * used in case of failure (exception). When an exception occurs in the execute, it is not
   * possible to also pass the response object, so therefore this method is added.</p>
   *
   * @return The instance of the response data. Should always be the same instance of an instance of
   *         DBAgentAction
   */
  public RESPONSE getResponseData()
  {
    return this.response;
  }


  /**
   * method to copy the files, delivered by the action, to a webdav location and storing its names
   * in the response message
   *
   * @param  aRequest aWebDAVRequest the request linked to the action
   *
   * @throws PDMException
   */
  public void responseFilesToWebDAV(AbstractRequest aRequest) throws PDMException
  {
    String logFile = aRequest.getPrimaryKeyDatabase() + getLogfilePostfix() + ".log";
    logOutputToWebDAV(getPDMFeedbackHandler().getReportBuilder(), aRequest.getDumpFileDir(), logFile);

    this.response.setLogFileDir(aRequest.getDumpFileDir());
    this.response.setLogFileName(logFile);
  }


  /**
   * set agent that instantiated this db action
   *
   * @param aAgent agent that instantiated this db action
   */
  public void setAgent(AbstractPDMAgent aAgent)
  {
    this.pdmAgent = aAgent;
  }


  /**
   * creates new reponse data object
   *
   * @return RESPONSE
   */
  protected abstract RESPONSE createResponse();


  /**
   * Get the postfix to be used for logfiles for this DBAgentAction.
   *
   * <p>Should return something like "_import" for import actions, and "_export" for export actions.
   * </p>
   *
   * @return A non-null string.
   */
  protected abstract String getLogfilePostfix();


  /**
   * Returns the table prefix based on the database type.
   *
   * <ul>
   *   <li>If it is Oracle, then the table prefix would be: schemaname</li>
   *   <li>If it is MSSQL, then it would be: schemaname.dbo</li>
   * </ul>
   *
   * @param  aSchemaName
   *
   * @return table prefix
   */
  protected abstract String getTablePrefix(String aSchemaName);


  /**
   * Utility method to help determine a filename based on the name of the original file, including
   * adjusting the extension.
   *
   * @param  aOriginalFileName name of original file
   * @param  aNewExtension     the new extension
   * @param  aPostfix          string that is postfixed to the new name
   *
   * @return file name with the new extension.
   */
  protected String changeFileExtension(String aOriginalFileName, String aNewExtension, String aPostfix)
  {
    assert aPostfix != null : "postfix may be empty but must be specified";
    int indexDot = aOriginalFileName.lastIndexOf('.');
    String startName = ((indexDot < 0) ? aOriginalFileName : aOriginalFileName.substring(0, indexDot));
    return startName + aPostfix + '.' + aNewExtension;
  }


  /**
   * close db connection
   *
   * @param  aDbConnection db connection to use
   *
   * @throws PDMException failed to close connection
   */
  protected void closeConnection(Connection aDbConnection) throws PDMException
  {
    try
    {
      aDbConnection.close();
    }
    catch (SQLException ex)
    {
      throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_CLOSING_CONNECTION_DB_SERVER, ex);
    }
  }


  /**
   * close statement and underlying resultset if active
   *
   * @param  aStatement       statement to close, can be null
   * @param  aException       pending exception, null if none
   * @param  aErrorDefinition error to raise if close fails and no other exception is pending
   *
   * @throws PDMException
   */
  protected void closeStatement(Statement aStatement, PDMException aException, ErrorDefinition aErrorDefinition) throws PDMException
  {
    if (aStatement != null)
    {
      try
      {
        aStatement.close();
      }
      catch (SQLException ex)
      {
        if (aException != null)
        {
          ex.printStackTrace();
        }
        else
        {
          throw getPDMFeedbackHandler().addException(aErrorDefinition, ex);
        }
      }
    }
  }


  /**
   * create name of dumpfile that is created when exporting
   *
   * @param  aRequest aWebDAVRequest the request linked to the action
   *
   * @return name of dumpfile that will be used when exporting/importing a database
   */

  protected final String createNameExportedDump(AbstractRequest aRequest)
  {
    return aRequest.getPrimaryKeyDatabase() + getLogfilePostfix() + getDumpFileExtension();
  }


  /**
   * Remove file from disk
   *
   * @param  aLocalFile the local file to be deleted
   *
   * @throws PDMException
   */
  protected void deleteLocalFile(File aLocalFile) throws PDMException
  {
    if (aLocalFile != null)
    {
      PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
      String absolutePath = aLocalFile.getAbsolutePath();
      pdmFeedbackHandler.report("Going to check existance " + absolutePath + " in order to delete it ... ");
      if (aLocalFile.exists())
      {
        pdmFeedbackHandler.report("Going to delete  " + absolutePath + " ...");
        if (aLocalFile.delete())
        {
          pdmFeedbackHandler.report("Successfully deleted file on database server: " + absolutePath);
        }
        else
        {
          throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_DELETING_FILE_ON_DBSERVER, true, null, absolutePath);
        }
      }
    }
  }


  /**
   * execute sql script
   *
   * @param  aAction          description of current action
   * @param  aSql             aDatabaseName the name of the database to change the owner for
   * @param  aDbConnection    The DB connection
   * @param  aErrorDefinition error definition to raise on failure
   *
   * @throws PDMException When bad things happen.
   */
  protected void executeSQL(String aAction, String aSql, Connection aDbConnection, ErrorDefinition aErrorDefinition) throws PDMException
  {
    PDMFeedbackHandler feedbackHandler = getPDMFeedbackHandler();
    feedbackHandler.report("  * " + aAction);

    feedbackHandler.report("\tSQL command: '" + aSql + "'");

    try
    {
      if (!SKIP_DB_ACTIONS)
      {
        Statement statement = aDbConnection.createStatement();
        statement.setQueryTimeout(DEFAULT_TIMEOUT);
        statement.execute(aSql);
      }
      else
      {
        feedbackHandler.report(FEEDBACK_SIMULATING);
      }
    }
    catch (SQLException ex)
    {
      throw feedbackHandler.addException(aErrorDefinition, ex);
    }
  }


  /**
   * converts specified raw version number in a formatted version number
   *
   * @param  aRawVersionNumber any string with up to 10 digits and optionally dots
   *
   * @return ww.xx.yy.zz
   */
  protected String formatTenDigitVersionNumber(String aRawVersionNumber)
  {
    String versionNumber = aRawVersionNumber.replace(".", ""); // replace to : because its not possible to split on a dot

    versionNumber = versionNumber.replace(".", "") + "0000000000";

    // add dots again
    StringBuilder builder = new StringBuilder();
    builder.append(versionNumber.substring(0, 2)).append('.');
    builder.append(versionNumber.substring(2, 4)).append('.');
    builder.append(versionNumber.substring(4, 6)).append('.');
    builder.append(versionNumber.substring(6, 8)).append('.');
    builder.append(versionNumber.substring(8, 10));
    versionNumber = builder.toString();
    return versionNumber;
  }


  /**
   * from a list of possible data file locations, select the one that has the most remaining usable
   * disk space left.
   *
   * @param  aDatafileLocations a list of possible data file locations
   *
   * @return File location, or null if no locations specified
   */
  protected File getDataFileLocationWithMaxFreeSpace(jade.util.leap.List aDatafileLocations)
  {
    File locationWithMostSpaceLeft = null;
    if (aDatafileLocations != null)
    {
      long mostUsableDiskSpaceLeft = 0L;
      // normal for-loop not possible because its not a java.util.List!
      for (Iterator locations = aDatafileLocations.iterator(); locations.hasNext();)
      {
        DatafileLocation datafileLocation = (DatafileLocation) locations.next();
        File location = new File(datafileLocation.getLocation());
        long usableDiskSpace = location.getUsableSpace();

        if (usableDiskSpace > mostUsableDiskSpaceLeft)
        {
          locationWithMostSpaceLeft = location;
          mostUsableDiskSpaceLeft = usableDiskSpace;
        }
      }
    }
    return locationWithMostSpaceLeft;
  }


  /**
   * Gets a DB connection.
   *
   * <p>Will always create a new one. Please close this connection yourself once you're done.</p>
   *
   * @param  aDatabaseServerInformation holds relevant information of database server to setup
   *                                    connection
   *
   * @return a Connection to the DB.
   *
   * @throws PDMException On problems.
   */
  protected Connection getDBConnection(DatabaseServerInformation aDatabaseServerInformation) throws PDMException
  {
    Connection dbConnection = null;
    getPDMFeedbackHandler().report("  * Create DB connection");
    try
    {
      dbConnection = DatabaseConnectionFactory.getConnection(aDatabaseServerInformation);
    }
    catch (SQLException ex)
    {
      String errorInfo = ex.getLocalizedMessage();
      if (errorInfo.endsWith("\n"))
      {
        errorInfo = errorInfo.substring(0, errorInfo.length() - 1);
      }
      throw getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_CONNECTING_TO_DB_SERVER, ex, errorInfo);
    }
    getPDMFeedbackHandler().report(FEEDBACK_SUCCESS);
    return dbConnection;
  }


  /**
   * Get the dump file extension for this DBAgentAction.
   *
   * <p>Should return something like ".bak" for mssql actions, and ".dpd" or ".dmp" for export
   * actions.</p>
   *
   * @return A non-null string.
   */
  protected String getDumpFileExtension()
  {
    // override in the sub-classes
    return "";
  }


  /**
   * get error handler linked to agent that instantiated this db action
   *
   * @return error handler linked to agent that instantiated this db action
   */
  protected PDMFeedbackHandler getPDMFeedbackHandler()
  {
    return getAgent().getPDMFeedbackHandler();
  }


  /**
   * Gets the Planon version details like what is the latest EEMetadata, and about the latest DB
   * version and also latest FNMetadata versions.
   *
   * @param  aDBConnection The DB connection
   * @param  aDatabaseName
   *
   * @return String object with planon version details
   *
   * @throws PDMException
   */
  protected String getPlanonVersionsDetailsFromTheDatabase(Connection aDBConnection, String aDatabaseName) throws PDMException
  {
    PDMException exception = null;
    Statement statement = null;
    String result = "";
    try
    {
      String queryVersions = " SELECT PV.CODE" //
      + " 	,V.SUITE_RELEASE_NUMBER" //
      + " 	,V.SUITE_SERVICEPACK_NUMBER" //
      + " 	,PV.PRODUCT_SERVICEPACK_NUMBER" //
      + " 	,PV.PRODUCT_HOTFIX_NUMBER" //
      + " 	,PV.BUILD_ID" //
      + " 	,PV.STATE" //
      + " FROM $USER$.PLN_VERSION V" //
      + " INNER JOIN $USER$.PLN_PRODUCTVERSION PV ON (PV.FK_VERSION = V.SYSCODE)" //
      + " 	AND PV.SYSCODE IN (" //
      + " 		SELECT MAX(Z.SYSCODE)" //
      + " 		FROM $USER$.PLN_PRODUCTVERSION Z" //
      + " 		WHERE Z.CODE IN (" //
      + " 				'EEMETADATA'" //
      + " 				,'FNMETADATA'" //
      + " 				,'DATABASE'" //
      + " 				)" //
      + " 		GROUP BY Z.CODE" //
      + " 		)";

      queryVersions = queryVersions.replaceAll("\\$USER\\$", Matcher.quoteReplacement(getTablePrefix(aDatabaseName)));
      statement = aDBConnection.createStatement();
      ResultSet resultSet = statement.executeQuery(queryVersions);
      ResultSetMetaData rsmd = resultSet.getMetaData();
      int colums = rsmd.getColumnCount();
      StringBuilder sbVersionDatabase = new StringBuilder();
      StringBuilder sbVersionEEMetadata = new StringBuilder();
      StringBuilder sbVersionFNMetadata = new StringBuilder();

      while (resultSet.next())
      {
        String productCode = resultSet.getString("CODE");
        String suiteReleaseNumber = resultSet.getString("SUITE_RELEASE_NUMBER");
        String suiteServicePackNumber = resultSet.getString("SUITE_SERVICEPACK_NUMBER");
        String productServicePackNumber = resultSet.getString("PRODUCT_SERVICEPACK_NUMBER");
        String productHotfixNumber = resultSet.getString("PRODUCT_HOTFIX_NUMBER");
        String buildID = resultSet.getString("BUILD_ID");
        String state = resultSet.getString("STATE");

        StringBuilder sbVersion = buildVersionString(suiteReleaseNumber, suiteServicePackNumber, productServicePackNumber, productHotfixNumber);

        switch (productCode)
        {
          case "DATABASE" :
            sbVersionDatabase = getSpecificPlanonDetailVersionInfo("Database Version:", sbVersion, buildID);
            break;
          case "EEMETADATA" :
            sbVersionEEMetadata = getSpecificPlanonDetailVersionInfo("EE Metadata Version: ", sbVersion, buildID);
            break;
          case "FNMETADATA" :
            sbVersionFNMetadata = getSpecificPlanonDetailVersionInfo("FN Metadata Version: ", sbVersion, buildID);
            break;
          default :
            break;
        }
      }
      return sbVersionEEMetadata.toString() + sbVersionDatabase.toString() + sbVersionFNMetadata.toString();
    }
    catch (SQLException ex)
    {
      exception = getPDMFeedbackHandler().addException(ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE, ex, (String) null);
      throw exception;
    }
    finally
    {
      closeStatement(statement, exception, ErrorDefinition.SQL_ERROR_IMPORTING_DB_DUMPFILE);
    }
  }


  /**
   * gets Task TimeOut in seconds, returns DEFAULT_TIMEOUT if not set
   *
   * @param  aAbstractRequest AbstractRequest to get initial time out for
   *
   * @return > 0
   */
  protected int getTaskTimeOut(AbstractRequest aAbstractRequest)
  {
    int timeOut = aAbstractRequest.getTaskTimeOut();
    if (timeOut <= 0)
    {
      timeOut = DEFAULT_TIMEOUT;
    }
    return timeOut;
  }


  /**
   * copies contents of a string builder to a file on a WebDAV location.
   *
   * @param  aOutputData          string builder of which the contents must be sent to a file on the
   *                              webdav location
   * @param  aRemoteDumpDirectory complete path to webdav location.
   * @param  aRemoteFileName      name of file on the webdav location
   *
   * @throws PDMException On WebDAV errors
   */
  protected void logOutputToWebDAV(StringBuilder aOutputData, String aRemoteDumpDirectory, String aRemoteFileName) throws PDMException
  {
    try
    {
      PnWebDAVResource directory = new PnWebDAVResource(new HttpURL(aRemoteDumpDirectory));
      if (!directory.putMethod(directory.getPath() + "/" + aRemoteFileName, aOutputData.toString(), null))
      {
        throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_UPLOADING_TO_WEBDAV_LOCATION, true, null, aRemoteDumpDirectory + aRemoteFileName, Integer.toString(directory.getStatusCode()), directory.getStatusMessage());
      }
    }
    catch (URIException ex)
    {
      throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_LOGGING_TO_WEBDAV_LOCATION, true, ex);
    }
    catch (HttpException ex)
    {
      throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_LOGGING_TO_WEBDAV_LOCATION, true, ex);
    }
    catch (IOException ex)
    {
      throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_LOGGING_TO_WEBDAV_LOCATION, true, ex);
    }
  }


  /**
   * Retrieves a dump file from a webdav location and places it in the shared oracle directory.
   *
   * <p>The file is downloaded from the webdav location and placed inside the local directory.</p>
   *
   * @param  aWebDAVFileLocation this should point to a valid dump on a webdav server. It is
   *                             expected to be URL format.
   * @param  aToDirectory        The local directory to dump the file on.
   *
   * @return The locally copied file.
   *
   * @throws PDMException
   */
  protected File retrieveFileFromWebDAV(String aWebDAVFileLocation, File aToDirectory) throws PDMException
  {
    File localFile = null;
    PDMFeedbackHandler pdmFeedbackHandler = getPDMFeedbackHandler();
    try
    {
      pdmFeedbackHandler.report("  * Retrieve dump step");
      HttpURL httpURL = new HttpURL(aWebDAVFileLocation);
      PnWebDAVResource resource = new PnWebDAVResource(httpURL);
      if (resource != null)
      {
        localFile = new File(aToDirectory, resource.getName());
        if (!resource.getMethod(localFile))
        {
          pdmFeedbackHandler.report("  !!! Error: " + resource.getStatusCode() + " - " + resource.getStatusMessage());
          localFile = null;
        }
      }

      pdmFeedbackHandler.report(FEEDBACK_SUCCESS + "\n\tRemote file: " + aWebDAVFileLocation + "\n\tLocal file: " + localFile);
    }
    catch (PnWebDavException ex)
    {
      int errorCode = ex.getDavException().getErrorCode();
      if (errorCode == HTTP_404)
      {
        throw pdmFeedbackHandler.addException(ErrorDefinition.FILE_NOT_FOUND, null, aWebDAVFileLocation);
      }

      throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_RETRIEVAL_DUMPFILE, ex, "PnWebDavException");
    }
    catch (HttpException ex)
    {
      throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_RETRIEVAL_DUMPFILE, ex, "HttpException");
    }
    catch (IOException ex)
    {
      throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_RETRIEVAL_DUMPFILE, ex, "IOException");
    }
    catch (Exception ex)
    {
      throw pdmFeedbackHandler.addException(ErrorDefinition.FAILED_RETRIEVAL_DUMPFILE, ex, "General Exception");
    }
    return localFile;
  }


  /**
   * copies a local file to a WebDAV location.
   *
   * @param  aLocalFile           local file that must be sent to the webdav location
   * @param  aRemoteDumpDirectory complete path to webdav location.
   * @param  aRemoteFileName      name of file when it is sent to the webdav location
   *
   * @throws PDMException On WebDAV errors
   */
  protected void sendFileToWebDAVLocation(File aLocalFile, String aRemoteDumpDirectory, String aRemoteFileName) throws PDMException
  {
    try
    {
      PnWebDAVResource directory = new PnWebDAVResource(new HttpURL(aRemoteDumpDirectory));
      boolean result = directory.putMethod(directory.getPath() + "/" + aRemoteFileName, aLocalFile);
      if (!result)
      {
        throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_UPLOADING_TO_WEBDAV_LOCATION, null, aRemoteDumpDirectory + aRemoteFileName, Integer.toString(directory.getStatusCode()), directory.getStatusMessage());
      }
    }
    catch (URIException ex)
    {
      throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_LOGGING_TO_WEBDAV_LOCATION, ex);
    }
    catch (HttpException ex)
    {
      throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_LOGGING_TO_WEBDAV_LOCATION, ex);
    }
    catch (IOException ex)
    {
      throw getPDMFeedbackHandler().addException(ErrorDefinition.FAILED_LOGGING_TO_WEBDAV_LOCATION, ex);
    }
  }


  /**
   * Sets the planon db version details to DatabaseDumpInformation.
   *
   * @param  aResponse     ImportResponse
   * @param  aDBConnection The DB Connection
   * @param  aDatabaseName
   *
   * @throws PDMException
   */
  protected void setPlanonDBVersionDetailsTODumpInfo(ImportResponse aResponse, Connection aDBConnection, String aDatabaseName) throws PDMException
  {
    String planonVersionInfoFromDB = getPlanonVersionsDetailsFromTheDatabase(aDBConnection, aDatabaseName);
    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) aResponse.getDumpInformation();
    dumpInformation.setPlanonDBVersionDetails(planonVersionInfoFromDB);
  }


  /**
   * Builds the version info
   *
   * @param  aSuiteReleaseNumber       Suite Release Number
   * @param  aSuiteServicePackNumber   Suite Service Pack Number
   * @param  aProductServicePackNumber Product Service Pack Number
   * @param  aProductHotfixNumber      Product Hotfix Number
   *
   * @return Version as string
   */
  private StringBuilder buildVersionString(String aSuiteReleaseNumber, String aSuiteServicePackNumber, String aProductServicePackNumber, String aProductHotfixNumber)
  {
    String versionSeparator = ".";
    StringBuilder sbVersion = new StringBuilder();
    sbVersion.append(aSuiteReleaseNumber);
    sbVersion.append(versionSeparator);
    sbVersion.append(aSuiteServicePackNumber);
    sbVersion.append(versionSeparator);
    sbVersion.append(aProductServicePackNumber);
    sbVersion.append(versionSeparator);
    sbVersion.append(aProductHotfixNumber);
    return sbVersion;
  }


  /**
   * Build and returns the Planon version info
   *
   * @param  aPlanonDetailVersion Planon details like: DB, EEmetadata or FNMetadata
   * @param  aVersion             version information
   * @param  aBuildID             build id
   *
   * @return specific planon version detail
   */
  private StringBuilder getSpecificPlanonDetailVersionInfo(String aPlanonDetailVersion, StringBuilder aVersion, String aBuildID)
  {
    return new StringBuilder().append(aPlanonDetailVersion).append(aVersion).append(" (Build_ID:").append(aBuildID).append(")\n");
  }
}
