set JAVA_HOME=C:\PDM3\PlanonEE201012\Server\jdk-1.6.0_21
set DBAGENT=Oracle10Agent:nl.planon.pdm.agent.db.PDMDBAgent(db_type=1,db_server=XE)
set SharedDirectory=C:\TEMP
set SharedDirectoryIdentifier=DATA_PUMP_DIR
set HostID=192.168.248.16
set PortJade=8888

%JAVA_HOME%\bin\java -classpath DbAgent.jar;PDMP5Module_server.jar;PDMCore_server.jar;libs\log4j-1.2.15.jar;libs\pnlogging.jar;libs\ojdbc6.jar;libs\pnwebdavutil.jar;libs\jade.jar;libs\jackrabbit-webdav-1.5.0.jar;libs\commons-httpclient-3.1.jar;libs\commons-codec-1.3.jar;libs\commons-logging-1.1.1.jar;libs\slf4j-log4j12-1.5.0.jar;libs\slf4j-api-1.5.0.jar;libs\jackrabbit-jcr-commons-1.5.0.jar -DOracleSharedDirectory=%SharedDirectory% -DOracleSharedDirectoryIdentifier=%SharedDirectoryIdentifier% -DLiveLog=true jade.Boot -container -host %HostID% -gui -agents %DBAGENT% -port %PortJade%