  Create Or Replace Package PLN_DataPumpInterface
  Is
    -- Integer to identify version. Please increase by 1 after EVERY change in this package.
    c_VersionID     Constant Integer := 51;

    -- Integer to set the default number of seconds before terminating a import or export job.
    c_TimeoutPeriod Constant Integer := 14400;

    -- Integer to set the default number of seconds to sleep in order to give the OS time to handle file traffic.
    c_SleepDelay Constant Integer := 5;

    /******************************/
    /* Metadata Analyse procedure */
    /******************************/
    Procedure PDM_DATAPUMP_DUMPFILEINFO( 
                                 p_DumpFileName     In  Varchar2
                               , p_Directory        In  Varchar2
                               , p_SQLFileName      Out Varchar2
                               , p_LogFileName      Out Varchar2
                               , p_ExitCode         Out Integer
                               , p_FullAnalyse      In  Integer Default 0
                               , p_SleepAfterFinish In  Integer Default c_SleepDelay
                               );


    /******************************/
    /* Import procedure           */
    /******************************/
    Procedure PDM_DATAPUMP_IMPORT( p_DumpFileName      In  Varchar2
                         , p_Directory         In  Varchar2
                         , p_OldSchemaName     In  Varchar2
                         , p_NewSchemaName     In  Varchar2
                         , p_OldTableSpaceName In  Varchar2 -- Comma separated list of tablespace names. All are remapped to the single target.
                         , p_NewTableSpaceName In  Varchar2
                         , p_LogFileName       Out Varchar2
                         , p_ExitCode          Out Integer
                         , p_TerminateOnError  In Integer := 1
                         , p_TimeoutPeriod     In Integer := c_TimeoutPeriod -- By default terminate after 4 hours if not ready before.
                         , p_AddLogTimestamps  In Integer := 0 -- If true (!= 0; timestamps are added to the log in minute intervals.)
                         , p_ParallelLevel     In Integer := 4 -- This parameter adjusts the degree of parallelism within a job.
                       );


    /******************************/
    /* Export procedure           */
    /******************************/
    Procedure PDM_DATAPUMP_EXPORT( p_SchemaName        In  Varchar2
                         , p_Directory         In  Varchar2
                         , p_DumpFileName      Out Varchar2
                         , p_LogFileName       Out Varchar2
                         , p_ExitCode          Out Integer
                         , p_TerminateOnError  In Integer  := 1
                         , p_TimeoutPeriod     In Integer  := c_TimeoutPeriod -- By default terminate after 4 hours if not ready before.
                         , p_AddLogTimestamps  In Integer  := 0 -- If true (!= 0; timestamps are added to the log in minute intervals.)
                         , p_Version           In Varchar2 := 'COMPATIBLE'
                         , p_ParallelLevel     In Integer  := 4 -- This parameter adjusts the degree of parallelism within a job.
                        );
                        
    /****************************************************/
    /* Overload: Kill a job based on job owner and name */
    /****************************************************/
     Procedure killJob( p_owner   In Varchar2
                      , p_jobName In Varchar2
                      );

    /****************************************************/
    /* Overload: Kill a job based on job handle         */
    /****************************************************/
    Procedure killJob( p_handle  In Number);

    /****************************************/
    /* Analyse dumpfile procedure           */
    /****************************************/
    Procedure AnalyzeDumpFile( p_Directory       In Varchar2
                             , p_DumpFileName    In Varchar2
                             , p_FileType        Out Varchar2
                             , p_DatabaseVersion Out Varchar2
                             );


    /****************************************/
    /* Get methods                          */
    /****************************************/

    Function getTypeOfDump Return Varchar2;

    Function getDatabaseVersion Return Varchar2;

    Function getGUID Return Varchar2;

    Function getExportJobName Return Varchar2;

    Function getExportServerOS Return Varchar2;

    Function getExportCharacterset Return Varchar2;

    Function getExportCreateDate Return Varchar2;

    Function getExportServerBlockSize Return Varchar2;

    Function getPackageVersion Return Integer;

    /***********************************/
    /* Getters for file types          */
    /***********************************/
    Function getFILE_TYPE_DATAPUMP Return Varchar2;
    Function getFILE_OLD_EXPORT Return Varchar2;
    
    Procedure REINIT_SPATIALINDEXES( p_Owner NVarchar2, p_LogDetailLevel Integer := 0);
End;
/
























































































  Create Or Replace Package Body PLN_DataPumpInterface
  Is
    /**************************************************/
    /* Globals to hold analyzed dump file properties. */
    /**************************************************/
    g_TypeOfDump               Integer;
    g_FILE_VERSION             Varchar2(2048);
    g_DATABASE_VERSION         Varchar2(2048);
    g_GUID                     Varchar2(2048);
    g_EXPORT_JOB_NAME          Varchar2(2048);
    g_EXPORT_SERVER_OS         Varchar2(2048);
    g_EXPORT_CHARACTERSET      Varchar2(2048);
    g_EXPORT_CREATEDATE        Varchar2(2048);
    g_EXPORT_SERVER_BLOCKSIZE  Varchar2(2048);

    /*****************************************************************/
    /* Error exception list                                          */
    /* Errors listed here will never cause termination of operations */
    /*****************************************************************/
    Type ErrorException Is Record(ErrorID Varchar2(9), Description Varchar2(255));
    Type ErrorExceptionVarray Is Varray(10) Of ErrorException;
    ErrorExceptionList ErrorExceptionVarray;
    l_Counter         Integer := 0; -- used in the exclusion list
    
    /***********************************/
    /* Exception for Time out handling */
    /***********************************/
    l_MessageString Varchar2(500); 
    Timeout_Encountered EXCEPTION;
    PRAGMA EXCEPTION_INIT(Timeout_Encountered, -20999);
  
    FileDoesNotExists EXCEPTION;
    PRAGMA EXCEPTION_INIT(FileDoesNotExists, -20998);

    FileVersionMismatch EXCEPTION;
    PRAGMA EXCEPTION_INIT(FileVersionMismatch, -20997);

    ExportVersionMismatch EXCEPTION;
    PRAGMA EXCEPTION_INIT(ExportVersionMismatch, -20996);

    JobInitialisationError EXCEPTION;
    PRAGMA EXCEPTION_INIT(JobInitialisationError, -20995);

    /***********************************/
    /* Dump file type constants        */
    /***********************************/
    FILE_TYPE_DATAPUMP  CONSTANT INTEGER := 1;
    FILE_OLD_EXPORT     CONSTANT INTEGER := 2;

    /***************************************/
    /* Private Database version detector   */
    /***************************************/

    /*****************************************************/
    /* Private variables for sessions settings handling  */
    /*****************************************************/
    l_timeout BINARY_INTEGER;

    /******************************/
    /* Private string tokeniser   */
    /******************************/
    Function get_token(
      the_list  varchar2,
      the_index integer,
      delim     varchar2 := ','
    )
      return    varchar2
    Is
      start_pos integer;
      end_pos   integer;
    Begin
      If (the_index = 1) then
        start_pos := 1;
      Else
        start_pos := Instr(the_list, delim, 1, the_index - 1);
        If (start_pos = 0) Then
          Return Null;
        Else
          start_pos := start_pos + Length(delim);
        End If;
      End If;

      end_pos := Instr(the_list, delim, start_pos, 1);

      If (end_pos = 0) Then
        Return Substr(the_list, start_pos);
      Else
        Return Substr(the_list, start_pos, end_pos - start_pos);
      End If;
    End get_token;

    /***********************************/
    /* Private log file writer         */
    /***********************************/
    Procedure WriteToLog( p_Handle  Number
                        , p_Message Varchar2
                        ) Is
      Job_does_not_exists Exception;
      PRAGMA EXCEPTION_INIT(Job_does_not_exists, -31626);    
    Begin
      dbms_datapump.log_entry(handle => p_Handle, message => p_Message, log_file_only => 1);
    Exception
      When Job_does_not_exists Then
      Null;
    End;

    /***********************************/
    /* Private Version info log writer */
    /***********************************/
    Procedure VersionLogWriter( p_Handle Number) Is
    Begin
      WriteToLog(p_Handle, 'Package version running is: '||To_char(c_VersionID)||'.');
      WriteToLog(p_Handle, 'Job was started at: '||To_Char(SYSDATE, 'DD-MM-YYYY HH24:MI:SS'));
      WriteToLog(p_Handle, 'Job handle is: '||To_Char(p_handle));
    End;

    /***************************************************/
    /* Private File Version checker                    */
    /* File version must be <= database server version */
    /***************************************************/
    Procedure fileVersionChecker( p_Directory        In Varchar2
                                , p_DumpFileName     In Varchar2
                                , p_DatabaseVersion  Out Integer
                                , p_FileVersion      Out Integer
                                ) Is
      l_version             Varchar2(2);
      l_release             Varchar2(2);
      l_fileVersion         Varchar2(255);
    l_dummy1              Varchar2(255);
    Begin
      -- Query the database server version
      l_version         := lpad(To_char(DBMS_DB_VERSION.VERSION), 2, '0');
      l_release         := lpad(To_char(DBMS_DB_VERSION.RELEASE), 2, '0');

      dbms_output.put_line(l_version||'.'||l_release);
      p_DatabaseVersion := To_Number(l_version||l_release);    -- numeric representation of DB server version.

      PLN_DataPumpInterface.AnalyzeDumpFile( p_Directory       => p_Directory
                                           , p_DumpFileName    => p_DumpFileName
                             , p_FileType        => l_dummy1
                             , p_DatabaseVersion => l_fileVersion
                                           );
      l_fileVersion := PLN_DataPumpInterface.getDatabaseVersion;
      p_FileVersion := To_Number(Substr(l_fileVersion, 1, 2)||Substr(l_fileVersion, 4, 2));
    End;

    /***************************************************/
    /* Private File exists checker                     */
    /***************************************************/
    Function fileExists( p_Directory     Varchar2
                       , p_DumpFileName  Varchar2) Return Boolean Is
      v_bfile BFILE;
    Begin
      v_bfile := Bfilename(p_Directory, p_DumpFileName);
      If (dbms_lob.fileexists(v_bfile)=1) Then
        Return True;
      Else
        Return False;
      End If;
    End;

    /****************************************************/
    /* Private Get the number of seconds since midnight */
    /****************************************************/
    Function getSecondsSinceMidnight( p_Date Date) Return Integer Is
    Begin
      Return((p_Date - trunc(p_Date)) * (24*60*60));
    End;

    /****************************************************/
    /* Private Add a number of seconds to a date        */
    /****************************************************/
    Function addSecondsToDate( p_Date Date, p_noOfSeconds Integer) Return Date Is
    Begin
      Return(p_Date + (p_noOfSeconds / (24*60*60)));
    End;

    /***************************************************/
    /* Private Table exists checker                    */
    /* For each job a table is created with a name     */
    /* identical to the job name. This check can thus  */
    /* be used as a check whether the job exists.      */
    /***************************************************/
    Function tableExists( p_Tablename In Varchar2) Return Boolean Is
      l_counter Integer;
    Begin
      Select Count(*)
      Into l_counter
      From ALL_TABLES
      Where TABLE_NAME = p_Tablename;

      If (l_counter = 0) Then
        Return False;
      Else
        Return True;
      End If;
    End;

    /*************************************************************************/
    /*                                                                       */
    /* Private DB version checker                                            */
    /* This method validates the optional version flag of the export method. */
    /*************************************************************************/
    Function validateDBVersion(p_Version In Varchar2) Return Boolean Is
      l_version       Varchar2(2);
      l_release       Varchar2(1);
      l_VersionNumber Number(3, 1);
      l_ActualVersion Number(3, 1);

      ParameterNumberError EXCEPTION;
      PRAGMA EXCEPTION_INIT(ParameterNumberError, -06502);
    
      -- List of supported versions. If a new Oracle version is introduced this list must be re-evaluated.
      Version102 CONSTANT Number := 10.2;
      Version111 CONSTANT Number := 11.1;
      Version112 CONSTANT Number := 11.2;
      Version121 CONSTANT Number := 12.1;
      Version122 CONSTANT Number := 12.2;
    Begin
      /* Validation of p_version is both with the actual database version and with
           the supported versions. If the version required is equal to the actual server 
       version it should be encoded as 'COMPATIBLE'. If anything else the version
       requested should be a supported version lower than the actual database version.
       Currently supported version are: '10.2', '11.1' and '11.2'. 
       In theory '10.1' should be possible too, but this version is not supported by 
       Planon and hence of no interest.
      */
      l_version       := dbms_db_version.version;
      l_release       := dbms_db_version.release;
      l_ActualVersion := To_Number(l_version||'.'||l_release);
      If (p_Version = 'COMPATIBLE') Then
        Return(True);
      Else
        l_VersionNumber := To_Number(p_Version);
        If (l_VersionNumber >= l_ActualVersion) Then
          -- Requested version is equal or higher than the actual version. Only lower versions are valid.
              Return(False);
        End If;
        If (l_VersionNumber = Version102) Then
          Return(True);
        ElsIf (l_VersionNumber = Version111) Then
          Return(True);
        ElsIf (l_VersionNumber = Version112) Then
          Return(True);
        ElsIf (l_VersionNumber = Version121) Then
          Return(True);
        ElsIf (l_VersionNumber = Version122) Then
          Return(True);
        Else   
          Return(False);
        End If;  
      End If;
      Exception
      When ParameterNumberError Then
        Return(False);
    End;

    Function errorOnExclusionList(p_ErrorText In Varchar2) Return Boolean Is
      l_index   Integer := 0;
      l_exclude Boolean := False;
    Begin
      If (LTrim(RTrim(p_ErrorText)) Is Null) Then
        Return(True);         
      ElsIf (ErrorExceptionList.LAST > 0) Then
        For l_index in 1..ErrorExceptionList.LAST Loop
          If (Instr(p_ErrorText, ErrorExceptionList(l_index).ErrorID) > 0) Then
            Return(True);         
          End If;
        End Loop;
      End If;
      Return(False);
    End;

    Procedure printExclusionListToLog(p_Handle Number) Is
      l_index   Integer := 0;
    Begin
      If (ErrorExceptionList.LAST > 0) Then
        WriteToLog( p_Handle  => p_Handle
                  , p_Message => '==========================================================================================================================================================='
                  );
        WriteToLog( p_Handle  => p_Handle
                  , p_Message => 'The following error codes are defined as non-fatal errors and will NOT result in a job fail, not even when the parameter p_TerminateOnError is set to true.'
                  );
        WriteToLog( p_Handle  => p_Handle
                  , p_Message => '==========================================================================================================================================================='
                  );
        For l_index in 1..ErrorExceptionList.LAST Loop
          WriteToLog( p_Handle  => p_Handle
                    , p_Message => ErrorExceptionList(l_index).ErrorID||'; '||ErrorExceptionList(l_index).Description
                    );
        End Loop;
        WriteToLog( p_Handle  => p_Handle
                  , p_Message => '==========================================================================================================================================================='
                  );
      End If;
    End;


    /**********************************************************************/
    /* Sleep procedure                                                    */
    /* Tricked like this as to avoid use of the DBMS_LOCK.SLEEP routine   */
    /* as execution rights on DBMS_LOCK are handed out by SYS.            */
    /* Amazon Oracle database as a service server do not offer SYS access */
    /* so we cannot use DBMS_LOCK.                                        */
    /* And yes, it is terrible code, but it fixes the issue.              */
    /**********************************************************************/
    Procedure SLEEP(p_Seconds Integer) Is
      l_BeginTime Date;
    Begin
      l_BeginTime := SYSDATE;
      Loop
        Exit When (l_BeginTime + (p_Seconds * (1 / 86400))) <= SYSDATE; 
      End Loop;
    End;

    /********************************************************************************************************************************************/
    /* Private: Re-init all invalid spatial indexes. This procedure can be called from the import procedure if a specific error is encountered. */
    /********************************************************************************************************************************************/
    Procedure REINIT_SPATIALINDEXES( p_Owner          NVarchar2
                                   , p_LogDetailLevel Integer := 0 -- 0 = no log; 1 = minimal log; 2 = verbose log 
                                   ) Is
      Cursor c_Invalid_Indexes Is
        Select i.TABLE_NAME   TABLE_NAME
             , i.INDEX_NAME   INDEX_NAME
             , ic.COLUMN_NAME COLUMN_NAME
        From ALL_INDEXES i Inner Join ALL_IND_COLUMNS ic On (i.INDEX_NAME = ic.INDEX_NAME And i.index_type like '%DOMAIN%' And i.OWNER = p_Owner And ic.INDEX_OWNER = i.OWNER);

      l_Counter        Integer;
      l_SPCall_prefix  Varchar2(2000);  
      l_SPCall_postfix Varchar2(2000) := '); End;';  
      l_Command        Varchar2(2000);  
      l_LogText        Varchar2(2000);  
      l_TABLE_NAME     Varchar2(128);
      l_INDEX_NAME     Varchar2(128);
      l_COLUMN_NAME    Varchar2(128);

    Begin
      l_LogText := 'Spatial Index correction: Invalid spatial indexes found. Attempting to re-create.';
      If (p_LogDetailLevel >= 1) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
      -- Create executor stored procedure 
      l_Command := 'Create Or Replace Procedure '||p_Owner||'.PLN_EXECUTE_SQL(p_SQL NVarchar2) Is Begin Execute Immediate p_SQL; End;';
      l_LogText := 'Spatial Index correction: About to create helper procedure '||p_Owner||'.PLN_EXECUTE_SQL() with command: '||l_Command;
      If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
      Execute Immediate l_Command;
      l_LogText := 'Spatial Index correction: helper procedure created.';
      If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

      l_SPCall_prefix := 'Begin '||p_Owner||'.'||'PLN_EXECUTE_SQL(p_SQL => ';

      For r_row In c_Invalid_Indexes Loop
        l_TABLE_NAME  := r_row.TABLE_NAME;
        l_INDEX_NAME  := r_row.INDEX_NAME;
        l_COLUMN_NAME := r_row.COLUMN_NAME;
      
        l_LogText := 'Spatial Index correction: About to re-create index '||r_row.INDEX_NAME||' on '||p_Owner||'.'||r_row.TABLE_NAME||'('||r_row.COLUMN_NAME||')';
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
      
        l_LogText := 'Spatial Index correction: Register index '||r_row.INDEX_NAME||' for '||p_Owner||'.'||r_row.TABLE_NAME||'('||r_row.COLUMN_NAME||') into MDSYS.SDO_GEOM_METADATA_TABLE.';
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

        Delete MDSYS.SDO_GEOM_METADATA_TABLE
        Where SDO_OWNER       = 'TEC7876'
          And SDO_TABLE_NAME  = 'OBJALG'
          And SDO_COLUMN_NAME = 'GPSLOCATION';

        Insert Into MDSYS.SDO_GEOM_METADATA_TABLE(SDO_OWNER, SDO_TABLE_NAME, SDO_COLUMN_NAME, SDO_DIMINFO, SDO_SRID) 
        Values('TEC7876', 'OBJALG', 'GPSLOCATION', SDO_DIM_ARRAY(SDO_DIM_ELEMENT('X', -180, 180, 0.005), SDO_DIM_ELEMENT('Y', -180, 180, 0.005)), 4326);
        
        l_LogText := 'Spatial Index correction: Index registered.';
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
        
        -- Now drop and recreate the index. For one reason or another a rebuild of the index will not do.
        -- Drop the index
        l_LogText := 'Spatial Index correction: About to Drop Index: '||r_row.INDEX_NAME;
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

        l_Command := l_SPCall_prefix||' '' Drop Index '||r_row.INDEX_NAME||' '' '||l_SPCall_postfix;
        l_LogText := 'Spatial Index correction: About to Drop Index: '||r_row.INDEX_NAME||' using command: '||l_Command;
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
        Execute Immediate l_Command;

        l_LogText := 'Spatial Index correction: Index dropped.';
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

        -- Create the index
        l_LogText := 'Spatial Index correction: About to create index '||r_row.INDEX_NAME||' for '||p_Owner||'.'||r_row.TABLE_NAME||'('||r_row.COLUMN_NAME||').';
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
        l_Command := l_SPCall_prefix||''' Create Index '||r_row.INDEX_NAME||' On '||p_Owner||'.'||r_row.TABLE_NAME||'('||r_row.COLUMN_NAME||') INDEXTYPE IS "MDSYS"."SPATIAL_INDEX" '''||l_SPCall_postfix;
        l_LogText := 'Spatial Index correction: About to execute: '||l_Command;
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
        l_LogText := 'Spatial Index correction: Index created.';
        If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
      End Loop;

      -- Clean up executor stored procedure 
      l_Command := 'Drop Procedure '||p_Owner||'.PLN_EXECUTE_SQL';
      l_LogText := 'Spatial Index correction: About to execute: '||l_Command;
      If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
      Execute Immediate l_Command;

      l_LogText := 'Spatial Index correction: Helper prodedure cleaned up.';
      If (p_LogDetailLevel >= 2) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

      l_LogText := 'All invalid spatial index(es) have been successfully registered and re-created. Previous errors listed in this log regarding spatial indexes can be ignored.';
      If (p_LogDetailLevel >= 0) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
    Exception  
      When Others Then
        dbms_output.put_line(SQLERRM);
        dbms_output.put_line(Dbms_utility.Format_error_backtrace);

        l_LogText := '...Spatial Index correction: An error was encountered while handling index: '||l_INDEX_NAME||' on '||p_Owner||'.'||l_TABLE_NAME||'('||l_COLUMN_NAME||'). ';
        If (p_LogDetailLevel >= 1) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

        l_LogText := '...Attempted to execute command: '||l_Command;
        If (p_LogDetailLevel >= 1) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

        l_LogText := '...Resulted in error: '||SQLERRM;
        If (p_LogDetailLevel >= 1) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;

        l_LogText := '...Error back trace: '||Dbms_utility.Format_error_backtrace;
        If (p_LogDetailLevel >= 1) Then DBMS_OUTPUT.PUT_LINE(l_LogText); End If;
    End;

    /******************************/
    /* Metadata Analyse procedure */
    /******************************/
    Procedure
      PDM_DATAPUMP_DUMPFILEINFO( p_DumpFileName     In  Varchar2
                               , p_Directory        In  Varchar2
                               , p_SQLFileName      Out Varchar2
                               , p_LogFileName      Out Varchar2
                               , p_ExitCode         Out Integer
                               , p_FullAnalyse      In  Integer Default 0
                               , p_SleepAfterFinish In  Integer Default c_SleepDelay
                               ) Is
      -- Import from Oracle directory DATA_PUMP_DIR
      handle    NUMBER;          -- Job handle
      jobName   Varchar2(30);    -- Unique job name
      jobNumber Integer;         -- Unique number to generate job name
      jobState  Varchar2(2000);  -- Resulting state of the job

      -- Exception handling
      l_ind           Integer := 0;
      l_sts           ku$_Status;
      l_le            ku$_LogEntry;
      fileExistsCode  Integer;
      l_DBVersion     Integer;
      l_FileVersion   Integer;

      -- Monitoring code chage
      ind           NUMBER;        -- Loop index
      percent_done  NUMBER;        -- Percentage of job complete
      job_state     VARCHAR2(30);  -- To keep track of job state
      le            ku$_LogEntry;  -- For WIP and error messages
      js            ku$_JobStatus; -- The job status from get_status
      jd            ku$_JobDesc;   -- The job description from get_status
      sts           ku$_Status;    -- The status object returned by get_status

      -- File exists check
      fileExistsFlag Boolean;
    BEGIN
      -- Check whether dump file exists. If not raise an error.
      fileExistsFlag := fileExists( p_Directory    => p_Directory
                                  , p_DumpFileName => p_DumpFileName);
      If (Not fileExistsFlag) Then
        -- At this time there is no dump file yet. So enter the p_ExitCode and raise an error.
        p_ExitCode := 1;
        Raise FileDoesNotExists;
      End If;

      -- Check whether the file to check is compatible with the database server we are working on.
      -- If not a error state is returned.
      fileVersionChecker( p_Directory        => p_Directory
                        , p_DumpFileName     => p_DumpFileName
                        , p_DatabaseVersion  => l_DBVersion
                        , p_FileVersion      => l_FileVersion
                        );
      If (l_DBVersion < l_FileVersion) Then
        p_ExitCode := 1;
        Raise FileVersionMismatch;
      End If;

      -- Get unique job number
      Select PDM_JOB_SEQ.NEXTVAL
      Into jobNumber
      From dual;

      -- Assemble job name
      jobName := 'PDM30JOB_'||TO_CHAR(jobNumber);

      p_SQLFileName := 'AnalyzeDumpfile_'||TO_CHAR(jobNumber)||'.sql';
      -- Declare job as a schema import; get job handle
      handle := Dbms_DataPump.Open(operation => 'SQL_FILE',
                                   job_mode  => 'FULL',
                                   job_name  => jobName,
                                   version   => 'COMPATIBLE'
                                  );

      -- Assign no log file to job as it is not created anyway.

      -- Assign dump file to job
      Dbms_DataPump.Add_File(handle  => handle,
                             filename  => p_DumpFileName,
                             directory => p_Directory,
                             filetype  => DBMS_DATAPUMP.KU$_FILE_TYPE_DUMP_FILE);


      -- Metadata file (inconsistent with export; that is you cannot produce both an export file and a sql file in one job)
      Dbms_DataPump.Add_File(handle  => handle,
                           filename  => p_SQLFileName,
                           directory => p_Directory,
                           filetype  => DBMS_DATAPUMP.KU$_FILE_TYPE_SQL_FILE);


      If (p_FullAnalyse = 0) Then
        -- Exclude the actual data from the 'SQL file', this should work much quicker
        Dbms_DataPump.METADATA_FILTER( handle      => handle,
                                       name        => 'NAME_EXPR',
                                       value       => 'IN (''NOT_EXISTING_TABLE_TO_EXCLUDE_ALL'')',
                                       Object_Type => 'TABLE'
                                     );
      End If;

      -- Start the job
      Dbms_DataPump.Start_Job(handle);

      percent_done := 0;
      job_state    := 'UNDEFINED';
      While (job_state != 'COMPLETED') And (job_state != 'STOPPED') Loop
        dbms_datapump.get_status(handle,
           dbms_datapump.ku$_status_job_error +
           dbms_datapump.ku$_status_job_status +
           dbms_datapump.ku$_status_wip,-1,job_state,sts);
        js := sts.job_status;

        -- If any work-in-progress (WIP) or error messages were received for the job,
        -- display them.
        If (bitand(sts.mask,dbms_datapump.ku$_status_wip) != 0) Then
          le := sts.wip;
        Elsif (bitand(sts.mask,dbms_datapump.ku$_status_job_error) != 0) Then
          le := sts.error;
        Else
          le := Null;
        end if;
        if le is not null
        then
          ind := le.FIRST;
          while ind is not null loop
            dbms_output.put_line(le(ind).LogText);
            ind := le.NEXT(ind);
          end loop;
        end if;
      End Loop;

      dbms_output.put_line('Job has completed');
      dbms_output.put_line('Final job state = ' || job_state);

      -- Free the job resources
      Dbms_datapump.detach(handle);
      p_ExitCode := 0;

    -- Add a delay. On fast servers we think this prevents problems with reading the .sql file produced.
    SLEEP(p_Seconds => p_SleepAfterFinish);
    
    Exception
      When FileVersionMismatch Then
        l_MessageString := SQLERRM;
        dbms_output.put_line(l_MessageString);
    
        l_MessageString := Dbms_utility.Format_error_backtrace;
        dbms_output.put_line(l_MessageString);

        l_MessageString := 'File '||p_DumpFileName||' version: '||l_FileVersion||' is newer then the database version: '||l_DBVersion;
        dbms_output.put_line(l_MessageString);

        p_ExitCode := 5;
        If (tableExists(jobName)) Then
          killJob( p_Handle => handle);
        End If;
      When FileDoesNotExists Then
        -- No WriteToLog as the logfile does probably not exists at this time
        l_MessageString := SQLERRM;
        dbms_output.put_line(l_MessageString);
    
        l_MessageString := Dbms_utility.Format_error_backtrace;
        dbms_output.put_line(l_MessageString);

        l_MessageString := 'File '||p_DumpFileName||' does not exists in directory: '||p_Directory||'.';
        dbms_output.put_line(l_MessageString);
        p_ExitCode := 4;
      When Timeout_Encountered Then
        l_MessageString := SQLERRM;
        dbms_output.put_line(l_MessageString);
    
        l_MessageString := Dbms_utility.Format_error_backtrace;
        dbms_output.put_line(l_MessageString);

        l_MessageString := 'Timeout encountered! Job will be terminated.';
        dbms_output.put_line(l_MessageString);

        p_ExitCode := 3;
        If (tableExists(jobName)) Then
          killJob( p_Handle => handle);
        End If;
      When Others Then
        dbms_output.put_line(SQLERRM);
        dbms_output.put_line(Dbms_utility.Format_error_backtrace);
        p_ExitCode := 2;
        If (tableExists(jobName)) Then
          killJob( p_Handle => handle);
        End If;
    END;


    /******************************/
    /* Import procedure           */
    /******************************/
    Procedure
      PDM_DATAPUMP_IMPORT( p_DumpFileName      In  Varchar2
                         , p_Directory         In  Varchar2
                         , p_OldSchemaName     In  Varchar2
                         , p_NewSchemaName     In  Varchar2
                         , p_OldTableSpaceName In  Varchar2 -- Comma separated list of tablespace names. All are remapped to the single target.
                         , p_NewTableSpaceName In  Varchar2
                         , p_LogFileName       Out Varchar2
                         , p_ExitCode          Out Integer
                         , p_TerminateOnError  In Integer := 1
                         , p_TimeoutPeriod     In Integer := c_TimeoutPeriod -- By default terminate after 4 hours if not ready before.
                         , p_AddLogTimestamps  In Integer := 0 -- If true (!= 0; timestamps are added to the log in minute intervals.)
                         , p_ParallelLevel     In Integer := 4 -- This parameter adjusts the degree of parallelism within a job.
                       ) Is

      -- Import from Oracle directory DATA_PUMP_DIR
      handle               NUMBER;          -- Job handle
      jobName              Varchar2(30);    -- Unique job name
      jobNumber            Integer;         -- Unique number to generate job name
      jobState             Varchar2(2000);  -- Resulting state of the job
      l_Counter            Integer := 0;

      -- Exception handling
      l_ind             Integer := 0;
      l_sts             ku$_Status;
      l_le              ku$_LogEntry;
      l_DBVersion       Integer;
      l_FileVersion     Integer;
      l_ErrorCountTotal Integer := 0;
      l_ErrorCountFatal Integer := 0;
      l_LogText         Varchar2(2000);  
     
      ind             NUMBER;        -- Loop index
      percent_done    NUMBER;        -- Percentage of job complete
      job_state       VARCHAR2(30);  -- To keep track of job state
      job_state_prev  VARCHAR2(30);  -- Previous job state
      le              ku$_LogEntry;  -- For WIP and error messages
      js              ku$_JobStatus; -- The job status from get_status
      js_prev         ku$_JobStatus; -- The previous job status from get_status
      jd              ku$_JobDesc;   -- The job description from get_status
      sts             ku$_Status;    -- The status object returned by get_status

      -- Performance
      l_ParallelLevel Integer;

      -- File exists check
      fileExistsFlag Boolean;

      -- Oracle schemas and tablespaces must be defined upper case
      l_OldSchemaName     Varchar2(255);
      l_NewSchemaName     Varchar2(255);
      l_OldTableSpaceName Varchar2(255);
      l_NewTableSpaceName Varchar2(255);

      -- DateTime variable to be used for the timeout mechanism.
      l_BeginOfJobTime Date := SYSDATE;

      -- String to print timestamp progress info 
      l_TimestampString     Varchar2(50);
      l_prevTimestampString Varchar2(50);
  BEGIN
    -- Oracle schemas and tablespaces must be defined upper case
      l_OldSchemaName     := Upper(p_OldSchemaName);
      l_NewSchemaName     := Upper(p_NewSchemaName);
      l_OldTableSpaceName := Upper(p_OldTableSpaceName);
      l_NewTableSpaceName := Upper(p_NewTableSpaceName);

      -- Check whether dump file exists. If not raise an error.
      fileExistsFlag := fileExists( p_Directory    => p_Directory
                                  , p_DumpFileName => p_DumpFileName);
      If (Not fileExistsFlag) Then
        p_ExitCode := 1;
        Raise FileDoesNotExists;
      End If;

      -- Check whether the file to check is compatible with the database server we are working on.
      -- If not a error state is returned.
      fileVersionChecker( p_Directory        => p_Directory
                        , p_DumpFileName     => p_DumpFileName
                        , p_DatabaseVersion  => l_DBVersion
                        , p_FileVersion      => l_FileVersion
                        );
      If (l_DBVersion < l_FileVersion) Then
        p_ExitCode := 1;
        Raise FileVersionMismatch;
      End If;

      -- Get unique job number
      Select PDM_JOB_SEQ.NEXTVAL
      Into jobNumber
      From dual;

      -- Assemble job name
      jobName := 'PDM30JOB_'||TO_CHAR(jobNumber);

      p_LogFileName := 'impdp_'||l_NewSchemaName||'_'||TO_CHAR(jobNumber)||'.log';

      -- Declare job as a schema import; get job handle
      handle := 0;
      
      handle := Dbms_DataPump.Open(operation => 'IMPORT',
                                   job_mode  => 'SCHEMA',
                                   job_name  => jobName,
                                   version   => 'COMPATIBLE'
                                  );
      If (handle = 0) Then
        p_ExitCode := 1;
        Raise JobInitialisationError;
      End If;
                                  
      -- Assign log file to job
      Dbms_DataPump.Add_File(handle    => handle,
                             filename  => p_LogFileName,
                             directory => p_Directory,
                             filetype  => DBMS_DATAPUMP.KU$_FILE_TYPE_LOG_FILE);

      -- Publish version info in the log
      VERSIONLOGWRITER(handle);

      -- Assign dump file to job
      Dbms_DataPump.Add_File(handle    => handle,
                             filename  => p_DumpFileName,
                             directory => p_Directory,
                             filetype  => DBMS_DATAPUMP.KU$_FILE_TYPE_DUMP_FILE);


      -- Remap user
      DBMS_DATAPUMP.METADATA_REMAP(handle      => handle,
                                   name        => 'REMAP_SCHEMA',
                                   old_value   => l_OldSchemaName,
                                   value       => l_NewSchemaName,
                                   object_type => null -- Default affects all objects
                                  );

      /******************************************************************************************************
      If a single tablespace is given a call to PLN_DataPumpInterface.get_token(p_OldTableSpaceName, 1, ',')
      will result in Null. In that case use the whole of p_OldTableSpaceName as source tablespace.
      Else if the call to PLN_DataPumpInterface.get_token(p_OldTableSpaceName, 1, ',') results in tokens
      loop trough it with ever increasing index value until result is Null.

      Each token found is a source tablespace. These are all remapped to the single new one specified
      in paramter p_NewTableSpaceName.
      *******************************************************************************************************/
      l_OldTableSpaceName := Upper(get_token(p_OldTableSpaceName, 1, ','));
      If (l_OldTableSpaceName Is Null) Then
        l_OldTableSpaceName := Upper(p_OldTableSpaceName);
        -- Only a single source tablespace is given. p_OldTableSpaceName contains the source tablespace.
        -- Remap tablespace
        dbms_output.put_line('Remapping source tablespace: '||l_OldTableSpaceName||' to target tablespace: '||l_NewTableSpaceName);
        DBMS_DATAPUMP.METADATA_REMAP(handle      => handle,
                                     name        => 'REMAP_TABLESPACE',
                                     old_value   => l_OldTableSpaceName,
                                     value       => l_NewTableSpaceName,
                                     object_type => null -- Default affects all objects
                                     );
      Else
        -- l_OldTableSpaceName Is Not Null. That implies a comma seperated list of source tablespaces. Loop trough them and remap them one by one.

        l_Counter := 1; -- First time is 1.
        While (l_OldTableSpaceName Is Not Null) Loop
          dbms_output.put_line('Remapping source tablespace: '||l_OldTableSpaceName||' to target tablespace: '||p_NewTableSpaceName);
          DBMS_DATAPUMP.METADATA_REMAP(handle      => handle,
                                       name        => 'REMAP_TABLESPACE',
                                       old_value   => l_OldTableSpaceName,
                                       value       => l_NewTableSpaceName,
                                       object_type => null -- Default affects all objects
                                       );
          l_Counter := l_Counter + 1;
          l_OldTableSpaceName := Upper(get_token(p_OldTableSpaceName, l_Counter, ','));
        End Loop;
      End If;

      -- Prevent user being created by data pump as it is managed by PDM itself 
      -- If this metadata filter is discarded the users in the dumpfile are created in the target database by datapump based on the user info in the dump file.
      DBMS_DATAPUMP.METADATA_FILTER(handle      => handle,
                                    name        => 'EXCLUDE_PATH_EXPR',
                                    value       => 'IN (''USER'')',
                                    object_path => null
                                   );

      -- Prevent user being created by data pump as it is managed by PDM itself
      -- If this metadata filter is discarded the users in the dumpfile are created in the target database by datapump based on the user info in the dump file.
      DBMS_DATAPUMP.METADATA_FILTER(handle      => handle,
                                    name        => 'SCHEMA_EXPR',
                                    value       => 'IN ('''||l_OldSchemaName||''')',
                                    object_path => null
                                   );
                                   
       -- ignore tables which are oraphened tables which are like MDXT_%$_BKTS and MDXT_%$_MBR.
       -- this  MDXT_%$_BKTS and MDXT_%$_MBR tables are causing import to fail. The fix is to ignore them. Jira call: TEC-1895
       dbms_datapump.metadata_filter(handle      => handle,
                                     name        =>'NAME_EXPR',
                                     value        =>'NOT LIKE ''%$_BKTS''',                                  
                                    object_type  => 'TABLE' 
                                    );
                                    
      dbms_datapump.metadata_filter(handle      => handle,
                                    name        =>'NAME_EXPR',
                                    value        =>'NOT LIKE ''%$_MBR''',                                  
                                    object_type  => 'TABLE' 
                                    );                            

      DBMS_DATAPUMP.METADATA_FILTER(handle      => handle,
                                    name        =>'EXCLUDE_PATH_EXPR',
                                    value       =>'IN (''STATISTICS'')'
                                   );

      l_ParallelLevel := p_ParallelLevel;
      If (p_ParallelLevel < 1) Then l_ParallelLevel := 1; End If;
      dbms_datapump.set_parallel(handle, l_ParallelLevel);

      -- Start the job
      Dbms_DataPump.Start_Job(handle);

      -- Print the error exclusion list to the log file.
      printExclusionListToLog(p_Handle => handle);


      -- Hook up with the job and wait for it to finish;
      -- If the next line is not executed the session will hand off the data pump job and not wait for it to finish.
      -- Control will return to caller while the job is still running and might continue to run for an undefined period of time.
      percent_done := 0;
      job_state := 'UNDEFINED';

      While (job_state != 'COMPLETED') And (job_state != 'STOPPED') Loop
        dbms_datapump.get_status(handle,
           dbms_datapump.ku$_status_job_error +
           dbms_datapump.ku$_status_job_status +
           dbms_datapump.ku$_status_wip,-1, job_state, sts);
        js := sts.job_status;
        job_state_prev := job_state;

        If (job_state != job_state_prev) Then
          WriteToLog(handle, 'Job state: '||job_state||' Previous state: '||job_state_prev);
        End If;

        -- Based on the setting of parameter p_AddLogTimestamps; add debug timestamps to the log
        If (p_AddLogTimestamps != 0) Then
          l_prevTimestampString := l_TimestampString;
          If (p_AddLogTimestamps between 1 and 10) Then
            l_TimestampString     := To_char(SYSDATE, 'yyyy-mm-dd hh24:mi');
          ElsIf  (p_AddLogTimestamps > 10) Then
            l_TimestampString     := To_char(SYSDATE, 'yyyy-mm-dd hh24:mi:ss');
          End If;
          If ((l_prevTimestampString != l_TimestampString) And (tableExists(jobName))) Then
            WriteToLog(handle, l_TimestampString);
          End If;  
        End If;

        -- If any error messages were received for the job write them to the log and end the job.
        If (bitand(sts.mask,dbms_datapump.ku$_status_job_error) != 0) Then
          le := sts.error;
          -- Error; Write the error info to the log and terminate the job.
          ind := le.FIRST;
          While (ind Is Not Null) Loop
            l_ErrorCountTotal := l_ErrorCountTotal + 1;
            If (Not errorOnExclusionList(p_ErrorText => le(ind).LogText)) Then
              -- Only increase error count if the error is not on the exclusion list.
              l_ErrorCountFatal := l_ErrorCountFatal + 1;

              -- Do not write excluded errors to the log in order to avoid repeating lists of log entries.             
              dbms_output.put_line(le(ind).LogText);
            End If;
            ind := le.NEXT(ind);
          End Loop;

          If (p_TerminateOnError != 0 And l_ErrorCountFatal > 0) Then -- If p_TerminateOnError = True (default) terminate on error. If False continue.
            p_ExitCode := 1;
            If (tableExists(jobName)) Then
              killJob( p_Handle => handle);
            End If;
            Return;
           End If;
        End If;  -- Error

        -- Timeout mechanism. If the timeout period is reached an error is raised. This error can be catched by the 
        -- exception clause including handling logging and cleanup of the job. 
        -- The reaching of the timeout is always fatal, regardless of the value of parameter p_TerminateOnError.
        If (addSecondsToDate(l_BeginOfJobTime, p_TimeoutPeriod) < SYSDATE) Then
          Raise Timeout_Encountered; 
        End If;
      End Loop; -- While

      -- Check and correct spatial indexes
      l_Counter := 0;
      DBMS_OUTPUT.PUT_LINE('Spatial index status report: All spatial indexes and their status in loaded database: ');
      For r_row In (Select index_name
                         , index_type
                         , status
                         , domidx_status
                         , domidx_opstatus 
                    From ALL_INDEXES 
                    Where OWNER = l_NewSchemaName
                      And index_type like '%DOMAIN%' 
                      And (domidx_status <> 'VALID' or domidx_opstatus <> 'VALID')
                    ) Loop 
        l_LogText := 'SPATIAL INDEX CHECK: Index name: '||r_row.INDEX_NAME||' Status: '||r_row.STATUS||' DOMIDX_STATUS: '||r_row.DOMIDX_STATUS||' DOMIDX_OPSTATUS: '||r_row.DOMIDX_OPSTATUS;
        DBMS_OUTPUT.PUT_LINE(l_LogText);
        l_Counter := l_Counter + 1;
      End Loop;                    
      If (l_Counter > 0) Then
        REINIT_SPATIALINDEXES( p_Owner => l_NewSchemaName, p_LogDetailLevel => 2);
      End If;

      DBMS_STATS.GATHER_SCHEMA_STATS(
        OWNNAME              =>  l_NewSchemaName,
        CASCADE              =>  TRUE,
        ESTIMATE_PERCENT     =>  100,
        DEGREE               =>  NULL,
        METHOD_OPT           =>  'FOR ALL COLUMNS SIZE AUTO'
      );

      -- Free the job resources
      Dbms_datapump.detach(handle);
      p_ExitCode := 0;

    Exception
      When JobInitialisationError Then
        l_MessageString := SQLERRM;
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);
    
        l_MessageString := Dbms_utility.Format_error_backtrace;
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);

        l_MessageString := 'Error during initialisation of datapump job. Job not created!';
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);

        p_ExitCode := 6;
      When FileVersionMismatch Then
        l_MessageString := SQLERRM;
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);
    
        l_MessageString := Dbms_utility.Format_error_backtrace;
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);

        l_MessageString := 'File '||p_DumpFileName||' version: '||l_FileVersion||' is newer then the database version: '||l_DBVersion;
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);

        p_ExitCode := 5;
        If (tableExists(jobName)) Then
          killJob( p_Handle => handle);
        End If;
      When FileDoesNotExists Then
        l_MessageString := SQLERRM;
        dbms_output.put_line(l_MessageString);
    
        l_MessageString := Dbms_utility.Format_error_backtrace;
        dbms_output.put_line(l_MessageString);

        l_MessageString := 'File '||p_DumpFileName||' does not exists in directory: '||p_Directory||'.';
        dbms_output.put_line(l_MessageString);
        p_ExitCode := 4;
      When Timeout_Encountered Then
        l_MessageString := SQLERRM;
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);
    
        l_MessageString := Dbms_utility.Format_error_backtrace;
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);

        l_MessageString := 'Timeout encountered! Job will be terminated.';
        dbms_output.put_line(l_MessageString);
--        WriteToLog(handle, l_MessageString);

        p_ExitCode := 3;
        If (tableExists(jobName)) Then
          killJob( p_Handle => handle);
        End If;
      When Others Then
        dbms_output.put_line(SQLERRM);
        dbms_output.put_line(Dbms_utility.Format_error_backtrace);
        p_ExitCode := 2;
        If (tableExists(jobName)) Then
          killJob( p_Handle => handle);
        End If;
    END;

    /******************************/
    /* Export procedure           */
    /******************************/
    Procedure
      PDM_DATAPUMP_EXPORT( p_SchemaName        In  Varchar2
                         , p_Directory         In  Varchar2
                         , p_DumpFileName      Out Varchar2
                         , p_LogFileName       Out Varchar2
                         , p_ExitCode          Out Integer
                         , p_TerminateOnError  In Integer  := 1
                         , p_TimeoutPeriod     In Integer  := c_TimeoutPeriod -- By default terminate after 4 hours if not ready before.
                         , p_AddLogTimestamps  In Integer  := 0 -- If true (!= 0; timestamps are added to the log in minute intervals.)
                         , p_Version           In Varchar2 := 'COMPATIBLE'
                         , p_ParallelLevel     In Integer  := 4 -- This parameter adjusts the degree of parallelism within a job.
                        ) Is
      -- Export current schema to Oracle directory DATA_PUMP_DIR
      handle     NUMBER;
      jobName    Varchar2(30);
      jobNumber  Integer;
      jobState   Varchar2(2000);

      -- Exception handling
      l_ind integer := 0;
      l_sts ku$_Status;
      l_le  ku$_LogEntry;
      l_ErrorCountTotal Integer := 0;
      l_ErrorCountFatal Integer := 0;

      -- Performance
      l_ParallelLevel Integer;

      -- Local
      l_SchemaName Varchar2(50);

      ind           NUMBER;        -- Loop index
      percent_done  NUMBER;        -- Percentage of job complete
      job_state     VARCHAR2(30);  -- To keep track of job state
      le            ku$_LogEntry;  -- For WIP and error messages
      js            ku$_JobStatus; -- The job status from get_status
      jd            ku$_JobDesc;   -- The job description from get_status
      sts           ku$_Status;    -- The status object returned by get_status

      -- DateTime variable to be used for the timeout mechanism.
      l_BeginOfJobTime Date := SYSDATE;

      -- String to print timestamp progress info 
    l_TimestampString     Varchar2(50);
    l_prevTimestampString Varchar2(50);
  BEGIN
      -- Get unique job number
      Select PDM_JOB_SEQ.NEXTVAL
      Into jobNumber
      From dual;

      -- Assemble job name
      jobName := 'ORADPJOB_'||TO_CHAR(jobNumber);
      p_DumpFileName := 'expdp_'||l_SchemaName||'_'||TO_CHAR(jobNumber)||'.dpd';
      p_LogFileName  := 'expdp_'||l_SchemaName||'_'||TO_CHAR(jobNumber)||'.log';

    -- Validate version
    If Not (validateDBVersion(p_Version => p_Version)) Then
      Raise ExportVersionMismatch;
    End If;
    
      -- Declare job as a schema export; Get job handle
      handle := Dbms_DataPump.Open(operation => 'EXPORT',
                                   job_mode  => 'SCHEMA',
                                   job_name  => jobName,
                                   version   => p_Version
                                  );

      -- Assign log file to job
      Dbms_DataPump.Add_File(handle    => handle,
                             filename  => p_LogFileName,
                             directory => p_Directory,
                             filetype  => DBMS_DATAPUMP.KU$_FILE_TYPE_LOG_FILE
                            );

      -- Publish version info in the log
      VersionLogWriter(handle);

      -- Assign dump file to job
      Dbms_DataPump.Add_File(handle    => handle,
                             filename  => p_DumpFileName,
                             directory => p_Directory,
                             filetype  => DBMS_DATAPUMP.KU$_FILE_TYPE_DUMP_FILE
                            );


    --  l_SchemaName := '''IN ('''''||Upper(p_SchemaName)||''''')''';
      l_SchemaName := ''''||Upper(p_SchemaName)||'''';
      -- Filter user schem
      DBMS_DATAPUMP.METADATA_FILTER(handle      => handle,
                                    name        => 'SCHEMA_LIST',
                                    value       => l_SchemaName,
                                    object_path => null
                                  );
                                   
       -- ignore tables which are oraphened tables which are like MDXT_%$_BKTS and MDXT_%$_MBR.
       -- this  MDXT_%$_BKTS and MDXT_%$_MBR tables are causing import to fail. The fix is to ignore them. Jira call: TEC-1895
       dbms_datapump.metadata_filter(handle      => handle,
                                     name        =>'NAME_EXPR',
                                     value        =>'NOT LIKE ''%$_BKTS''',                                  
                                    object_type  => 'TABLE' 
                                    );
                                    
        dbms_datapump.metadata_filter(handle      => handle,
                                     name        =>'NAME_EXPR',
                                     value        =>'NOT LIKE ''%$_MBR''',                                  
                                    object_type  => 'TABLE' 
                                    );                            

      l_ParallelLevel := p_ParallelLevel;
      If (p_ParallelLevel < 1) Then l_ParallelLevel := 1; End If;
      dbms_datapump.set_parallel(handle, l_ParallelLevel);

      -- Start the job
      Dbms_DataPump.Start_Job(handle);

      -- Print the error exclusion list to the log file.
      printExclusionListToLog(p_Handle => handle);

      percent_done := 0;
      job_state    := 'UNDEFINED';
      While (job_state != 'COMPLETED') And (job_state != 'STOPPED') Loop
        dbms_datapump.get_status(handle,
           dbms_datapump.ku$_status_job_error +
           dbms_datapump.ku$_status_job_status +
           dbms_datapump.ku$_status_wip,-1,job_state,sts);
        js := sts.job_status;

        -- Based on the setting of parameter p_AddLogTimestamps; add debug timestamps to the log
        If (p_AddLogTimestamps != 0) Then
          l_prevTimestampString := l_TimestampString;
           If (p_AddLogTimestamps between 1 and 10) Then
            l_TimestampString     := To_char(SYSDATE, 'yyyy-mm-dd hh24:mi');
          ElsIf  (p_AddLogTimestamps > 10) Then
            l_TimestampString     := To_char(SYSDATE, 'yyyy-mm-dd hh24:mi:ss');
          End If;
          If ((l_prevTimestampString != l_TimestampString) And (tableExists(jobName))) Then
            WriteToLog(handle, l_TimestampString);
          End If;  
        End If;

        -- If any error messages were received for the job write them to the log and end the job.
        If (bitand(sts.mask,dbms_datapump.ku$_status_job_error) != 0) Then
          le := sts.error;
        Else
          le := Null;
        End If;

        if (le is not null) then
          -- Error; Write the error info to the log and terminate the job.
          ind := le.FIRST;
          while (ind is not null) loop
            l_ErrorCountTotal := l_ErrorCountTotal + 1;
            If (Not errorOnExclusionList(p_ErrorText => le(ind).LogText)) Then
              -- Only increase error count if the error is not on the exclusion list.
              l_ErrorCountFatal := l_ErrorCountFatal + 1;

              -- Do not write excluded errors to the log in order to avoid repeating lists of log entries.             
              WriteToLog(handle, le(ind).LogText);
            End If;
            ind := le.NEXT(ind);
          end loop;

          If (p_TerminateOnError != 0 And l_ErrorCountFatal > 0) Then -- If p_TerminateOnError = True (default) terminate on error. If False continue.
            p_ExitCode := 1;
            If (tableExists(jobName)) Then
              killJob( p_Handle => handle);
            End If;
            Return;
          End If;
        End If;

        -- Timeout mechanism. If the timeout period is reached an error is raised. This error can be catched by the 
        -- exception clause including handling logging and cleanup of the job. 
        -- The reaching of the timeout is always fatal, regardless of the value of parameter p_TerminateOnError.
        If (addSecondsToDate(l_BeginOfJobTime, p_TimeoutPeriod) < SYSDATE) Then
          Raise Timeout_Encountered; 
        End If;
      End Loop; --While

      -- Free the resources
      Dbms_datapump.detach(handle);
      p_ExitCode := 0;

    Exception
      When Timeout_Encountered Then
        p_ExitCode := 3;
        WriteToLog(handle, 'Timeout reached. Job has ran for more than '||To_Char(p_TimeoutPeriod)||' seconds and was not finished. It will now be terminated. The job has failed.');
        If (tableExists(jobName)) Then
          killJob( p_Handle => handle);
        End If;
      When ExportVersionMismatch Then
          p_ExitCode := 4;
--        WriteToLog(handle, 'Version parameter error. Requested version '||p_Version||' is not valid. Export aborted.');
      When Others Then
        dbms_output.put_line(SQLERRM);
        dbms_output.put_line(Dbms_utility.Format_error_backtrace);
        p_ExitCode := 2;
        If (tableExists(jobName)) Then
          WriteToLog(handle, SQLERRM);
          WriteToLog(handle, Dbms_utility.Format_error_backtrace);
          killJob( p_Handle => handle);
        End If;
    END;



    /****************************************/
    /* Analyse dumpfile procedure           */
    /****************************************/
    Procedure AnalyzeDumpFile( p_Directory       In Varchar2
                             , p_DumpFileName    In Varchar2
                             , p_FileType        Out Varchar2
                             , p_DatabaseVersion Out Varchar2
                             ) Is
      l_InfoTable  sys.ku$_dumpfile_info;
      l_TypeString Varchar2(1000);

      INFO_FILE_VERSION             CONSTANT INTEGER := 1;
      INFO_DATABASE_VERSION         CONSTANT INTEGER := 2;
      INFO_GUID                     CONSTANT INTEGER := 5;
      INFO_EXPORT_JOB_NAME          CONSTANT INTEGER := 8;
      INFO_EXPORT_SERVER_OS         CONSTANT INTEGER := 9;
      INFO_EXPORT_CHARACTERSET      CONSTANT INTEGER := 11;
      INFO_EXPORT_CREATEDATE        CONSTANT INTEGER := 12;
      INFO_EXPORT_SERVER_BLOCKSIZE  CONSTANT INTEGER := 13;

    Begin
      DBMS_DATAPUMP.GET_DUMPFILE_INFO(
       p_DumpFileName,
       p_Directory,
       l_InfoTable,
       g_TypeOfDump);

      If (g_TypeOfDump = FILE_TYPE_DATAPUMP) Then
        l_TypeString               := 'Oracle DataPump dump type';
        g_FILE_VERSION             := l_InfoTable(INFO_FILE_VERSION).value;
        g_DATABASE_VERSION         := l_InfoTable(INFO_DATABASE_VERSION).value;
        g_GUID                     := l_InfoTable(INFO_GUID).value;
        g_EXPORT_JOB_NAME          := l_InfoTable(INFO_EXPORT_JOB_NAME).value;
        g_EXPORT_SERVER_OS         := l_InfoTable(INFO_EXPORT_SERVER_OS).value;
        g_EXPORT_CHARACTERSET      := l_InfoTable(INFO_EXPORT_CHARACTERSET).value;
        g_EXPORT_CREATEDATE        := l_InfoTable(INFO_EXPORT_CREATEDATE).value;
        g_EXPORT_SERVER_BLOCKSIZE  := l_InfoTable(INFO_EXPORT_SERVER_BLOCKSIZE).value;

        dbms_output.put_line('The file is a: '||l_TypeString);
        dbms_output.put_line('FILE_VERSION           Value: '||l_InfoTable(INFO_FILE_VERSION).value);
        dbms_output.put_line('Database version       Value: '||l_InfoTable(INFO_DATABASE_VERSION).value);
        dbms_output.put_line('Export server OS       Value: '||l_InfoTable(INFO_EXPORT_SERVER_OS).value);
        dbms_output.put_line('Export server charset  Value: '||l_InfoTable(INFO_EXPORT_CHARACTERSET).value);
        dbms_output.put_line('Export create date     Value: '||l_InfoTable(INFO_EXPORT_CREATEDATE).value);
        dbms_output.put_line('Export server block size  Value: '||l_InfoTable(INFO_EXPORT_SERVER_BLOCKSIZE).value);
      ElsIf (g_TypeOfDump = FILE_OLD_EXPORT) Then
        l_TypeString               := 'Oracle original dump type';
        g_FILE_VERSION             := l_InfoTable(INFO_FILE_VERSION).value;
        g_DATABASE_VERSION         := l_InfoTable(INFO_DATABASE_VERSION).value;
        g_GUID                     := Null;
        g_EXPORT_JOB_NAME          := Null;
        g_EXPORT_SERVER_OS         := Null;
        g_EXPORT_CHARACTERSET      := Null;
        g_EXPORT_CREATEDATE        := Null;
        g_EXPORT_SERVER_BLOCKSIZE  := Null;
        dbms_output.put_line('The file is a: '||l_TypeString);
        dbms_output.put_line('FILE_VERSION        Value: '||l_InfoTable(INFO_FILE_VERSION).value);
      Else -- g_TypeOfDump = FILE_TYPE_UNKNOWN
        l_TypeString := 'Unknown type of file / Not a known Oracle dump file type';
        g_FILE_VERSION             := Null;
        g_DATABASE_VERSION         := Null;
        g_GUID                     := Null;
        g_EXPORT_JOB_NAME          := Null;
        g_EXPORT_SERVER_OS         := Null;
        g_EXPORT_CHARACTERSET      := Null;
        g_EXPORT_CREATEDATE        := Null;
        g_EXPORT_SERVER_BLOCKSIZE  := Null;
        dbms_output.put_line('The file is a: '||l_TypeString);
      End If;
      p_FileType        := g_TypeOfDump;
      p_DatabaseVersion := g_DATABASE_VERSION;
    End;

    Function getTypeOfDump Return Varchar2 Is
    Begin
      Return(g_TypeOfDump);
    End;

    Function getDatabaseVersion Return Varchar2 Is
    Begin
      Return(g_DATABASE_VERSION);
    End;

    Function getGUID Return Varchar2 Is
    Begin
      Return(g_GUID);
    End;

    Function getExportJobName Return Varchar2 Is
    Begin
      Return(g_EXPORT_JOB_NAME);
    End;

    Function getExportServerOS Return Varchar2 Is
    Begin
      Return(g_EXPORT_SERVER_OS);
    End;

    Function getExportCharacterset Return Varchar2 Is
    Begin
      Return(g_EXPORT_CHARACTERSET);
    End;

    Function getExportCreateDate Return Varchar2 Is
    Begin
      Return(g_EXPORT_CREATEDATE);
    End;

    Function getExportServerBlockSize Return Varchar2 Is
    Begin
      Return(g_EXPORT_SERVER_BLOCKSIZE);
    End;

    Procedure killJob( p_owner   In Varchar2
                     , p_jobName In Varchar2
                     ) Is
      handle Number;

      Job_Not_Exists EXCEPTION;
      PRAGMA EXCEPTION_INIT(Job_Not_Exists, -31626);
    Begin
      handle := DBMS_DATAPUMP.ATTACH( job_owner => p_owner
                                    , job_name  => p_jobName
                                    );

      DBMS_DATAPUMP.STOP_JOB( handle      => handle
                            , immediate   => 1 -- non-zero; implies terminate immediately.
                            , keep_master => 0 -- Jobs master table must be dropped; job is not restarteble.
                            , delay       => 0 -- Terminate immediate, do not wait.
                            );
    Exception
      When Job_Not_Exists Then
        --Job does not exist. Is okay, we want to drop it in the first place.
        Null;
      When Others Then
        Raise;
    End;

    Procedure killJob( p_handle  In Number) Is
      Job_Not_Exists EXCEPTION;
      PRAGMA EXCEPTION_INIT(Job_Not_Exists, -31626);
    Begin
      DBMS_DATAPUMP.STOP_JOB( handle      => p_handle
                            , immediate   => 1 -- non-zero; implies terminate immediately.
                            , keep_master => 0 -- Jobs master table must be dropped; job is not restarteble.
                            , delay       => 0 -- Terminate immediate, do not wait.
                            );
    Exception
      When Job_Not_Exists Then
        --Job does not exist. Is okay, we want to drop it in the first place.
        Null;
      When Others Then
        Raise;
    End;

    Function getPackageVersion Return Integer Is
    Begin
      Return(c_VersionID);
    End;

    /***********************************/
    /* Getters for file types          */
    /***********************************/
    Function getFILE_TYPE_DATAPUMP Return Varchar2 Is
    Begin
      Return(FILE_TYPE_DATAPUMP);
    End;
  
    Function getFILE_OLD_EXPORT Return Varchar2 Is
    Begin
      Return(FILE_OLD_EXPORT);
    End;

  Begin
    /*****************************************************************/
    /* Initialisation of error exception list                        */
    /* Errors listed here will never cause termination of operations */
    /*****************************************************************/
    /* Initialise exception list */
    ErrorExceptionList := ErrorExceptionVarray();

    l_Counter := l_Counter + 1;
    ErrorExceptionList.EXTEND;
    ErrorExceptionList(l_Counter).ErrorId  := 'ORA-39083'; ErrorExceptionList(l_Counter).Description := '{Object} failed to create';

    l_Counter := l_Counter + 1;
    ErrorExceptionList.EXTEND;
    ErrorExceptionList(l_Counter).ErrorId  := 'ORA-06512'; ErrorExceptionList(l_Counter).Description := 'General error message';

    l_Counter := l_Counter + 1;
    ErrorExceptionList.EXTEND;
    ErrorExceptionList(l_Counter).ErrorId  := 'ORA-01917'; ErrorExceptionList(l_Counter).Description := 'user or role does not exist';

    l_Counter := l_Counter + 1;
    ErrorExceptionList.EXTEND;
    ErrorExceptionList(l_Counter).ErrorId  := 'ORA-01918'; ErrorExceptionList(l_Counter).Description := 'user does not exist';

    l_Counter := l_Counter + 1;
    ErrorExceptionList.EXTEND;
    ErrorExceptionList(l_Counter).ErrorId  := 'ORA-01919'; ErrorExceptionList(l_Counter).Description := 'role does not exist';

    l_Counter := l_Counter + 1;
    ErrorExceptionList.EXTEND;
    ErrorExceptionList(l_Counter).ErrorId  := 'ORA-31684'; ErrorExceptionList(l_Counter).Description := 'Object type {Object type}:"{Object name}" already exists';

    l_Counter := l_Counter + 1;
    ErrorExceptionList.EXTEND;
    ErrorExceptionList(l_Counter).ErrorId  := 'ORA-06502'; ErrorExceptionList(l_Counter).Description := 'PL/SQL: numeric or value error: character string buffer too small';

    /**/
    /*****************************************************************/
    /* Check the value of the resumable timeout session setting.     */
    /* Resumable timeouts lead to 'hang' situations in PDM3.         */
    /* If active disable it.                                         */
    /*****************************************************************/
    l_timeout := DBMS_RESUMABLE.GET_TIMEOUT();
    If (l_timeout > 0) Then
      DBMS_RESUMABLE.SET_TIMEOUT(timeout => 1); -- In Oracle 19 0 is now allowed anymore.
    End If;
  End; -- Package body
/
