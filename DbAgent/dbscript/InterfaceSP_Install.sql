-- Run this part of the script as system
drop directory data_pump_dir;

 -- replace path if required
create directory data_pump_dir as 'D:\oracle\Admin\DV3102\dpdump';

-- everyone may read and write
grant read, write on directory data_pump_dir to public; 

-- Sequence for job name generation
Create sequence PDM_JOB_SEQ 
start with 1
increment by 1
/

-- Run this part of the script as sys as it does hand out permissions to system
grant execute on DBMS_RESUMABLE to system;
Grant INSERT,DELETE on "MDSYS"."SDO_GEOM_METADATA_TABLE" to SYSTEM;

-- For analyzing other schemas for statistics
grant ANALYZE ANY DICTIONARY to system;
grant ANALYZE ANY to system;
Grant SELECT ANY TABLE to system;
