echo off

if [%1]==[] goto usage

rem set the classpath to the DbAgent
set JAVA_CLASSPATH=z:\PDM3\DbAgent\jars\DbAgent.jar
rem add classpath for hades (if needed)
set JAVA_CLASSPATH=%JAVA_CLASSPATH%;z:\PDM3\DbAgent\jars\Hades.jar;z:\Planon5\lib\Util\pnlogging.jar;z:\Planon5\lib\Log4j\log4j-1.2.15.jar;z:\Planon5\lib\jboss\jboss4.2.1.GA.jar
rem add classpath for pdm server admin (if needed)
set JAVA_CLASSPATH=%JAVA_CLASSPATH%;z:\Planon5\jars\nl.planon.module.pdmp5module.server.jar
rem add classpath to third party libraries (a.o. jade)
set JAVA_CLASSPATH=%JAVA_CLASSPATH%;z:\PDM3\lib\jade\jade.jar

rem set JAVA_HOME
set JAVA_HOME=z:\Planon5\JDK\jdk1.6.0_11\bin

rem set VM arguments
set JNDI_FILE=z:\Planon5\jndi.properties
set JAAS_CONFIG_FILE=Z:\Planon5\auth.conf
set VM_ARG=-Djava.security.auth.login.config=%JAAS_CONFIG_FILE% -Xmx128m -DJNDIFILE=%JNDI_FILE%

rem set program arguments
set AGENT_NAME=%1
set AGENT_CLASS=nl.planon.pdm.agent.db.DbAgent
set JADE_SERVER_HOST=localhost
set JADE_SERVER_PORT=8888
set PROGRAM_ARG=-container -host %JADE_SERVER_HOST% -port %JADE_SERVER_PORT% -agents %AGENT_NAME%:%AGENT_CLASS%

echo Starting agent: %AGENT_NAME%
echo java_home=%JAVA_HOME%
echo classpath=%JAVA_CLASSPATH%
echo vm_arg=%VM_ARG%
echo program_arg=%PROGRAM_ARG%


echo on

java %VM_ARG% -classpath "%JAVA_CLASSPATH%" jade.Boot %PROGRAM_ARG%



:usage
echo usage: run agent-name

:end