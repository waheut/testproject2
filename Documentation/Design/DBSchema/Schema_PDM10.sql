/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all tables'                            */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_DATABASE' */

Create Table PDM_DATABASE
(
  SYSCODE                        Number(10,0)   Not Null,
  ACCESSCODE                     VarChar2(25)   Not Null,
  DATABASE_STATUS                VarChar2(15)   Default 'N/A' Not Null,
  DESCRIPTION                    VarChar2(255)  Not Null,
  DUMPFILE                       VarChar2(255)  Not Null,
  EXPIRATIONDATE                 Date           Not Null,
  FK_MSSQLDBCLIENT_FOR_IMPORT    Number(10,0)   Null,
  FK_MSSQLDBSERVER               Number(10,0)   Null,
  FK_MSSQLDBSFILELOCATION        Number(10,0)   Null,
  FK_MSSQLDUMPVERSION            Number(10,0)   Null,
  FK_ORACLEDBCLIENT_FOR_IMPORT   Number(10,0)   Null,
  FK_ORACLEDBSERVER              Number(10,0)   Null,
  FK_ORACLEDBSFILELOCATION       Number(10,0)   Null,
  FK_ORACLEDUMPVERSION           Number(10,0)   Null,
  FK_USER_DATABASEOWNER          Number(10,0)   Not Null,
  NAME                           VarChar2(25)   Not Null,
  OBJECT_OWNER_IN_DUMP           VarChar2(30)   Null,
  OPTION_NOT_CREATE_DBUSER       VarChar2(1)    Default 'F' Not Null,
  OPTION_NO_IMPORT               VarChar2(1)    Default 'F' Not Null,
  OPTION_PARTIAL_IMPORT          VarChar2(1)    Default 'F' Not Null,
  PARTIAL_IMPORT_TABLE_LIST      VarChar2(255)  Null,
  SIZE_DATABASE                  Number(16,2)   Null,
  SIZE_DUMPFILE                  Number(16,2)   Null,
  SPACE_IN_USE                   VarChar2(1)    Default 'F' Not Null,
  TYPE                           VarChar2(10)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_DATABASEREQUEST' */

Create Table PDM_DATABASEREQUEST
(
  SYSCODE                        Number(10,0)   Not Null,
  EXECUTE_DATETIME               Date           Null,
  EXECUTE_DURATION               Number(10,0)   Null,
  EXECUTE_FUNCTION               VarChar2(15)   Not Null,
  EXECUTE_LOGFILE                VarChar2(255)  Null,
  EXECUTE_STATUS                 VarChar2(15)   Default 'REQUEST' Not Null,
  FK_DATABASE                    Number(10,0)   Not Null,
  FK_USER_REQUESTOR              Number(10,0)   Not Null,
  OPTION_NOT_CREATE_DBUSER       VarChar2(1)    Default 'F' Not Null,
  OPTION_NO_IMPORT               VarChar2(1)    Default 'F' Not Null,
  OPTION_PARTIAL_IMPORT          VarChar2(1)    Default 'F' Not Null,
  PARTIAL_IMPORT_TABLE_LIST      VarChar2(255)  Null,
  REQUEST_DATETIME               Date           Default SYSDATE Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_EMAILQUEUE' */

Create Table PDM_EMAILQUEUE
(
  SYSCODE                        Number(10,0)   Not Null,
  SMTP_ATTACHMENTFILE            VarChar2(255)  Null,
  SMTP_BODY1                     VarChar2(255)  Not Null,
  SMTP_BODY2                     VarChar2(255)  Null,
  SMTP_REPLY_TO                  VarChar2(255)  Not Null,
  SMTP_SUBJECT                   VarChar2(255)  Not Null,
  SMTP_TO                        VarChar2(255)  Not Null,
  SMTP_TO_BCC                    VarChar2(255)  Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_HORIZONTALCONVERTINFO' */

Create Table PDM_HORIZONTALCONVERTINFO
(
  SYSCODE                        Number(10,0)   Not Null,
  CREATE_OTHERS_SCRIPT           VarChar2(255)  Not Null,
  CREATE_TABLES_SCRIPT           VarChar2(255)  Not Null,
  PLANONDB_BUILD                 Number(10,0)   Not Null,
  PLANONDB_VERSION               Number(10,0)   Not Null,
  SYBASE_BASIC_DATABASEFILE      VarChar2(255)  Not Null,
  VSS_GET_SCRIPTS_STRING         VarChar2(255)  Not Null,
  VSS_LIBDIRECTORY               VarChar2(255)  Not Null,
  VSS_PROJECT                    VarChar2(255)  Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_IPPORTLIST' */

Create Table PDM_IPPORTLIST
(
  SYSCODE                        Number(10,0)   Not Null,
  IPPORTNUMBER                   Number(10,0)   Not Null,
  IS_FREE                        VarChar2(1)    Default 'T' Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_METRICS' */

Create Table PDM_METRICS
(
  SYSCODE                        Number(10,0)   Not Null,
  DATABASE_STATUS                VarChar2(15)   Not Null,
  EXECUTE_DURATION               Number(10,0)   Null,
  EXECUTE_FUNCTION               VarChar2(15)   Not Null,
  METRIC_CREATEDATE              Date           Default SYSDATE Not Null,
  REF_DATABASE                   Number(10,0)   Not Null,
  SIZE_DATABASE                  Number(16,2)   Null,
  SIZE_DUMPFILE                  Number(16,2)   Null,
  TYPE                           VarChar2(10)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_MSSQLDBCLIENT' */

Create Table PDM_MSSQLDBCLIENT
(
  SYSCODE                        Number(10,0)   Not Null,
  BINDIRECTORY                   VarChar2(255)  Not Null,
  ISQL_PROGRAMNAME               VarChar2(50)   Not Null,
  NAME                           VarChar2(50)   Not Null,
  VERSION                        VarChar2(15)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_MSSQLDBSERVER' */

Create Table PDM_MSSQLDBSERVER
(
  SYSCODE                        Number(10,0)   Not Null,
  ADMIN_PASS                     VarChar2(30)   Not Null,
  ADMIN_USER                     VarChar2(30)   Not Null,
  BDE_ALIAS                      VarChar2(50)   Not Null,
  CODE_PAGE                      Number(5,0)    Not Null,
  CONNECT_SERVER_NAME            VarChar2(100)  Not Null,
  DUMPFILE_LOCATION              VarChar2(205)  Not Null,
  DUMPFILE_LOCATION_LOCALPREFIX  VarChar2(50)   Not Null,
  DUMPFILE_LOCATION_UNCPREFIX    VarChar2(50)   Not Null,
  LOGFILE_LOCATION               VarChar2(255)  Not Null,
  NAME                           VarChar2(20)   Not Null,
  SORT_ORDER                     Number(5,0)    Not Null,
  SPACE_QUOTA                    Number(16,2)   Not Null,
  UNICODE_COMPARISONSTYLE        Number(8,0)    Not Null,
  UNICODE_LOCALEID               Number(8,0)    Not Null,
  VERSION                        VarChar2(15)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_MSSQLDBSFILELOCATION' */

Create Table PDM_MSSQLDBSFILELOCATION
(
  SYSCODE                        Number(10,0)   Not Null,
  DATAFILE_LOCATION              VarChar2(205)  Not Null,
  DATAFILE_LOCATION_LOCALPREFIX  VarChar2(50)   Not Null,
  DATAFILE_LOCATION_UNCPREFIX    VarChar2(50)   Not Null,
  FK_MSSQLDBSERVER               Number(10,0)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_MSSQLDUMPVERSION' */

Create Table PDM_MSSQLDUMPVERSION
(
  SYSCODE                        Number(10,0)   Not Null,
  FK_MSSQLDBCLIENT               Number(10,0)   Not Null,
  FK_MSSQLDBSERVER               Number(10,0)   Not Null,
  VERSION                        VarChar2(15)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_ORACLEDBCLIENT' */

Create Table PDM_ORACLEDBCLIENT
(
  SYSCODE                        Number(10,0)   Not Null,
  BINDIRECTORY                   VarChar2(255)  Not Null,
  EXPORT_PROGRAMNAME             VarChar2(50)   Not Null,
  IMPORT_PROGRAMNAME             VarChar2(50)   Not Null,
  NAME                           VarChar2(50)   Not Null,
  SQLPLUS_PROGRAMNAME            VarChar2(50)   Not Null,
  VERSION                        VarChar2(15)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_ORACLEDBSERVER' */

Create Table PDM_ORACLEDBSERVER
(
  SYSCODE                        Number(10,0)   Not Null,
  ADMIN_PASS                     VarChar2(30)   Not Null,
  ADMIN_USER                     VarChar2(30)   Not Null,
  BDE_ALIAS                      VarChar2(50)   Not Null,
  CONNECT_TNS_NAME               VarChar2(100)  Not Null,
  DEFAULT_TABLESPACENAME         VarChar2(30)   Not Null,
  DUMPFILE_LOCATION              VarChar2(255)  Not Null,
  IS_TABLESPACE_PER_USER_SERVER  VarChar2(1)    Default 'T' Not Null,
  LOGFILE_LOCATION               VarChar2(255)  Not Null,
  NAME                           VarChar2(20)   Not Null,
  SPACE_QUOTA                    Number(16,2)   Not Null,
  TEMPORARY_TABLESPACENAME       VarChar2(30)   Not Null,
  VERSION                        VarChar2(15)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_ORACLEDBSFILELOCATION' */

Create Table PDM_ORACLEDBSFILELOCATION
(
  SYSCODE                        Number(10,0)   Not Null,
  DATAFILE_LOCATION              VarChar2(205)  Not Null,
  DATAFILE_LOCATION_LOCALPREFIX  VarChar2(50)   Not Null,
  DATAFILE_LOCATION_UNCPREFIX    VarChar2(50)   Not Null,
  FK_ORACLEDBSERVER              Number(10,0)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_ORACLEDUMPVERSION' */

Create Table PDM_ORACLEDUMPVERSION
(
  SYSCODE                        Number(10,0)   Not Null,
  FK_ORACLEDBCLIENT              Number(10,0)   Not Null,
  FK_ORACLEDBSERVER              Number(10,0)   Not Null,
  VERSION                        VarChar2(15)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_SERVERCHARACTERISTIC' */

Create Table PDM_SERVERCHARACTERISTIC
(
  NAME                           VarChar2(20)   Not Null,
  CLEANUP_TMPDIRECTORY           VarChar2(1)    Not Null,
  FK_MSSQL7_ALTERNATIVEDBSERVER  Number(10,0)   Null,
  FK_MSSQLDBCLIENT_DEFAULT       Number(10,0)   Null,
  FK_MSSQLDBSERVER_DEFAULT       Number(10,0)   Null,
  KEEP_REQUESTS                  Number(10,0)   Default 50 Not Null,
  ORACLEDBS_TS_AUTOEXTENT_QUOTA  Number(16,2)   Null,
  SMTP_FROM_ADDRESS              VarChar2(100)  Not Null,
  SMTP_FROM_NAME                 VarChar2(50)   Not Null,
  SMTP_HOST                      VarChar2(100)  Not Null,
  SMTP_REPLY_TO                  VarChar2(50)   Not Null,
  SMTP_TO_BCC                    VarChar2(255)  Null,
  SMTP_USERID                    VarChar2(50)   Not Null,
  SYBASE_BINDIRECTORY            VarChar2(255)  Not Null,
  SYBASE_OUTDIRECTORY            VarChar2(255)  Not Null,
  VSS_BINDIRECTORY               VarChar2(255)  Not Null,
  VSS_CONNECT                    VarChar2(50)   Not Null
)
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table PDM_USER' */

Create Table PDM_USER
(
  SYSCODE                        Number(10,0)   Not Null,
  EMAIL                          VarChar2(255)  Not Null,
  LOGINNAME                      VarChar2(50)   Not Null,
  MAY_ADAPT_EXPORT_PARAMETERS    VarChar2(1)    Default 'F' Not Null,
  MAY_ADAPT_IMPORT_PARAMETERS    VarChar2(1)    Default 'F' Not Null,
  MAY_DELETE_FOREIGN_DATABASE    VarChar2(1)    Default 'F' Not Null,
  MAY_EXPORT_DATABASE            VarChar2(1)    Default 'F' Not Null,
  MAY_OVERRULE_EXPIRATION        VarChar2(1)    Default 'F' Not Null,
  MAY_OVERRULE_QUOTA             VarChar2(1)    Default 'F' Not Null,
  MAY_SET_HORIZONTAL_CONVERSION  VarChar2(1)    Default 'F' Not Null,
  MAY_SET_IMPORT_OPTIONS         VarChar2(1)    Default 'F' Not Null,
  MAY_TRACE_APPLICATION          VarChar2(1)    Default 'F' Not Null,
  NAME                           VarChar2(50)   Not Null,
  SPACE_QUOTA                    Number(16,2)   Not Null
)
/

/*---------------------------------------------------------------------------*/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all primary keys'                      */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_DATABASE.PLN_DTE_PK' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_DATABASEREQUEST.PLN_DRT_PK' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_EMAILQUEUE.PLN_EQE_PK' */

Alter Table
  PDM_EMAILQUEUE
Add
  Constraint
    PLN_EQE_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_HORIZONTALCONVERTINFO.PLN_HCO_PK' */

Alter Table
  PDM_HORIZONTALCONVERTINFO
Add
  Constraint
    PLN_HCO_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_IPPORTLIST.PLN_IPT_PK' */

Alter Table
  PDM_IPPORTLIST
Add
  Constraint
    PLN_IPT_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_METRICS.PDM_METRICS_PK' */

Alter Table
  PDM_METRICS
Add
  Constraint
    PDM_METRICS_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_MSSQLDBCLIENT.PLN_MCT_PK' */

Alter Table
  PDM_MSSQLDBCLIENT
Add
  Constraint
    PLN_MCT_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_MSSQLDBSERVER.PLN_MDR_PK' */

Alter Table
  PDM_MSSQLDBSERVER
Add
  Constraint
    PLN_MDR_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_MSSQLDBSFILELOCATION.PLN_MDN_PK' */

Alter Table
  PDM_MSSQLDBSFILELOCATION
Add
  Constraint
    PLN_MDN_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_MSSQLDUMPVERSION.PLN_MDO_PK' */

Alter Table
  PDM_MSSQLDUMPVERSION
Add
  Constraint
    PLN_MDO_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_ORACLEDBCLIENT.PLN_OCT_PK' */

Alter Table
  PDM_ORACLEDBCLIENT
Add
  Constraint
    PLN_OCT_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_ORACLEDBSERVER.PLN_ODR_PK' */

Alter Table
  PDM_ORACLEDBSERVER
Add
  Constraint
    PLN_ODR_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_ORACLEDBSFILELOCATION.PLN_ODN_PK' */

Alter Table
  PDM_ORACLEDBSFILELOCATION
Add
  Constraint
    PLN_ODN_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_ORACLEDUMPVERSION.PLN_ODO_PK' */

Alter Table
  PDM_ORACLEDUMPVERSION
Add
  Constraint
    PLN_ODO_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_SERVERCHARACTERISTIC.PLN_SCC_PK' */

Alter Table
  PDM_SERVERCHARACTERISTIC
Add
  Constraint
    PLN_SCC_PK
  Primary Key
  (
    NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create primary key PDM_USER.PLN_USR_PK' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_PK
  Primary Key
  (
    SYSCODE
  )
/

/*---------------------------------------------------------------------------*/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all unique keys'                       */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_DATABASE.PLN_DTE_UK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_UK01
  Unique
  (
    NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_HORIZONTALCONVERTINFO.PLN_HCO_UK01' */

Alter Table
  PDM_HORIZONTALCONVERTINFO
Add
  Constraint
    PLN_HCO_UK01
  Unique
  (
    PLANONDB_VERSION,
    PLANONDB_BUILD
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_IPPORTLIST.PLN_IPT_UK01' */

Alter Table
  PDM_IPPORTLIST
Add
  Constraint
    PLN_IPT_UK01
  Unique
  (
    IPPORTNUMBER
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_MSSQLDBCLIENT.PLN_MCT_UK02' */

Alter Table
  PDM_MSSQLDBCLIENT
Add
  Constraint
    PLN_MCT_UK02
  Unique
  (
    NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_MSSQLDBSERVER.PLN_MDR_UK01' */

Alter Table
  PDM_MSSQLDBSERVER
Add
  Constraint
    PLN_MDR_UK01
  Unique
  (
    CONNECT_SERVER_NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_MSSQLDBSERVER.PLN_MDR_UK02' */

Alter Table
  PDM_MSSQLDBSERVER
Add
  Constraint
    PLN_MDR_UK02
  Unique
  (
    NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_MSSQLDBSFILELOCATION.PLN_MDN_UK01' */

Alter Table
  PDM_MSSQLDBSFILELOCATION
Add
  Constraint
    PLN_MDN_UK01
  Unique
  (
    DATAFILE_LOCATION,
    DATAFILE_LOCATION_UNCPREFIX,
    DATAFILE_LOCATION_LOCALPREFIX
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_MSSQLDUMPVERSION.PLN_MDO_UK01' */

Alter Table
  PDM_MSSQLDUMPVERSION
Add
  Constraint
    PLN_MDO_UK01
  Unique
  (
    VERSION
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_ORACLEDBCLIENT.PLN_OCT_UK02' */

Alter Table
  PDM_ORACLEDBCLIENT
Add
  Constraint
    PLN_OCT_UK02
  Unique
  (
    NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_ORACLEDBSERVER.PLN_ODR_UK01' */

Alter Table
  PDM_ORACLEDBSERVER
Add
  Constraint
    PLN_ODR_UK01
  Unique
  (
    CONNECT_TNS_NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_ORACLEDBSERVER.PLN_ODR_UK02' */

Alter Table
  PDM_ORACLEDBSERVER
Add
  Constraint
    PLN_ODR_UK02
  Unique
  (
    NAME
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_ORACLEDBSFILELOCATION.PLN_ODN_UK01' */

Alter Table
  PDM_ORACLEDBSFILELOCATION
Add
  Constraint
    PLN_ODN_UK01
  Unique
  (
    DATAFILE_LOCATION,
    DATAFILE_LOCATION_UNCPREFIX,
    DATAFILE_LOCATION_LOCALPREFIX
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_ORACLEDUMPVERSION.PLN_ODO_UK01' */

Alter Table
  PDM_ORACLEDUMPVERSION
Add
  Constraint
    PLN_ODO_UK01
  Unique
  (
    VERSION
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create unique key PDM_USER.PLN_USR_UK01' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_UK01
  Unique
  (
    NAME
  )
/

/*---------------------------------------------------------------------------*/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all foreign keys'                      */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_MCT_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_MCT_FK01
  Foreign Key
  (
    FK_MSSQLDBCLIENT_FOR_IMPORT
  )
  References
    PDM_MSSQLDBCLIENT
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_MDN_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_MDN_FK01
  Foreign Key
  (
    FK_MSSQLDBSFILELOCATION
  )
  References
    PDM_MSSQLDBSFILELOCATION
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_MDO_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_MDO_FK01
  Foreign Key
  (
    FK_MSSQLDUMPVERSION
  )
  References
    PDM_MSSQLDUMPVERSION
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_MDR_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_MDR_FK01
  Foreign Key
  (
    FK_MSSQLDBSERVER
  )
  References
    PDM_MSSQLDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_OCT_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_OCT_FK01
  Foreign Key
  (
    FK_ORACLEDBCLIENT_FOR_IMPORT
  )
  References
    PDM_ORACLEDBCLIENT
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_ODN_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_ODN_FK01
  Foreign Key
  (
    FK_ORACLEDBSFILELOCATION
  )
  References
    PDM_ORACLEDBSFILELOCATION
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_ODO_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_ODO_FK01
  Foreign Key
  (
    FK_ORACLEDUMPVERSION
  )
  References
    PDM_ORACLEDUMPVERSION
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_ODR_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_ODR_FK01
  Foreign Key
  (
    FK_ORACLEDBSERVER
  )
  References
    PDM_ORACLEDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASE.PLN_DTE_USR_FK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_USR_FK01
  Foreign Key
  (
    FK_USER_DATABASEOWNER
  )
  References
    PDM_USER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASEREQUEST.PLN_DRT_DTE_FK01' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_DTE_FK01
  Foreign Key
  (
    FK_DATABASE
  )
  References
    PDM_DATABASE
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_DATABASEREQUEST.PLN_DRT_USR_FK01' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_USR_FK01
  Foreign Key
  (
    FK_USER_REQUESTOR
  )
  References
    PDM_USER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_MSSQLDBSFILELOCATION.PLN_MDN_MDR_FK01' */

Alter Table
  PDM_MSSQLDBSFILELOCATION
Add
  Constraint
    PLN_MDN_MDR_FK01
  Foreign Key
  (
    FK_MSSQLDBSERVER
  )
  References
    PDM_MSSQLDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_MSSQLDUMPVERSION.PLN_MDO_MCT_FK01' */

Alter Table
  PDM_MSSQLDUMPVERSION
Add
  Constraint
    PLN_MDO_MCT_FK01
  Foreign Key
  (
    FK_MSSQLDBCLIENT
  )
  References
    PDM_MSSQLDBCLIENT
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_MSSQLDUMPVERSION.PLN_MDO_MDR_FK01' */

Alter Table
  PDM_MSSQLDUMPVERSION
Add
  Constraint
    PLN_MDO_MDR_FK01
  Foreign Key
  (
    FK_MSSQLDBSERVER
  )
  References
    PDM_MSSQLDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_ORACLEDBSFILELOCATION.PLN_ODN_ODR_FK01' */

Alter Table
  PDM_ORACLEDBSFILELOCATION
Add
  Constraint
    PLN_ODN_ODR_FK01
  Foreign Key
  (
    FK_ORACLEDBSERVER
  )
  References
    PDM_ORACLEDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_ORACLEDUMPVERSION.PLN_ODO_OCT_FK01' */

Alter Table
  PDM_ORACLEDUMPVERSION
Add
  Constraint
    PLN_ODO_OCT_FK01
  Foreign Key
  (
    FK_ORACLEDBCLIENT
  )
  References
    PDM_ORACLEDBCLIENT
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_ORACLEDUMPVERSION.PLN_ODO_ODR_FK01' */

Alter Table
  PDM_ORACLEDUMPVERSION
Add
  Constraint
    PLN_ODO_ODR_FK01
  Foreign Key
  (
    FK_ORACLEDBSERVER
  )
  References
    PDM_ORACLEDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_SERVERCHARACTERISTIC.PLN_SCC_MCT_FK01' */

Alter Table
  PDM_SERVERCHARACTERISTIC
Add
  Constraint
    PLN_SCC_MCT_FK01
  Foreign Key
  (
    FK_MSSQLDBCLIENT_DEFAULT
  )
  References
    PDM_MSSQLDBCLIENT
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_SERVERCHARACTERISTIC.PLN_SCC_MDR_FK01' */

Alter Table
  PDM_SERVERCHARACTERISTIC
Add
  Constraint
    PLN_SCC_MDR_FK01
  Foreign Key
  (
    FK_MSSQLDBSERVER_DEFAULT
  )
  References
    PDM_MSSQLDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create foreign key PDM_SERVERCHARACTERISTIC.PLN_SCC_MDR_FK02' */

Alter Table
  PDM_SERVERCHARACTERISTIC
Add
  Constraint
    PLN_SCC_MDR_FK02
  Foreign Key
  (
    FK_MSSQL7_ALTERNATIVEDBSERVER
  )
  References
    PDM_MSSQLDBSERVER
    (
      SYSCODE
    )
/

/*---------------------------------------------------------------------------*/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all indexes'                           */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_01' */

Create Index
  PLN_DTE_01
On
  PDM_DATABASE
  (
    FK_USER_DATABASEOWNER 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_02' */

Create Index
  PLN_DTE_02
On
  PDM_DATABASE
  (
    FK_ORACLEDUMPVERSION 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_03' */

Create Index
  PLN_DTE_03
On
  PDM_DATABASE
  (
    FK_ORACLEDBSERVER 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_04' */

Create Index
  PLN_DTE_04
On
  PDM_DATABASE
  (
    FK_MSSQLDBSERVER 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_05' */

Create Index
  PLN_DTE_05
On
  PDM_DATABASE
  (
    FK_ORACLEDBCLIENT_FOR_IMPORT 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_06' */

Create Index
  PLN_DTE_06
On
  PDM_DATABASE
  (
    EXPIRATIONDATE 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_07' */

Create Index
  PLN_DTE_07
On
  PDM_DATABASE
  (
    DATABASE_STATUS 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_08' */

Create Index
  PLN_DTE_08
On
  PDM_DATABASE
  (
    FK_ORACLEDBSFILELOCATION 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_09' */

Create Index
  PLN_DTE_09
On
  PDM_DATABASE
  (
    FK_MSSQLDBSFILELOCATION 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_10' */

Create Index
  PLN_DTE_10
On
  PDM_DATABASE
  (
    FK_MSSQLDUMPVERSION 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASE.PLN_DTE_11' */

Create Index
  PLN_DTE_11
On
  PDM_DATABASE
  (
    FK_MSSQLDBCLIENT_FOR_IMPORT 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASEREQUEST.PLN_DRT_01' */

Create Index
  PLN_DRT_01
On
  PDM_DATABASEREQUEST
  (
    FK_USER_REQUESTOR 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_DATABASEREQUEST.PLN_DRT_02' */

Create Index
  PLN_DRT_02
On
  PDM_DATABASEREQUEST
  (
    FK_DATABASE 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_MSSQLDBCLIENT.PLN_MCT_01' */

Create Index
  PLN_MCT_01
On
  PDM_MSSQLDBCLIENT
  (
    VERSION 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_MSSQLDBSFILELOCATION.PLN_MDN_01' */

Create Index
  PLN_MDN_01
On
  PDM_MSSQLDBSFILELOCATION
  (
    FK_MSSQLDBSERVER 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_MSSQLDUMPVERSION.PLN_MDO_01' */

Create Index
  PLN_MDO_01
On
  PDM_MSSQLDUMPVERSION
  (
    FK_MSSQLDBSERVER 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_MSSQLDUMPVERSION.PLN_MDO_02' */

Create Index
  PLN_MDO_02
On
  PDM_MSSQLDUMPVERSION
  (
    FK_MSSQLDBCLIENT 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_ORACLEDBCLIENT.PLN_OCT_01' */

Create Index
  PLN_OCT_01
On
  PDM_ORACLEDBCLIENT
  (
    VERSION 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_ORACLEDBSFILELOCATION.PLN_ODN_01' */

Create Index
  PLN_ODN_01
On
  PDM_ORACLEDBSFILELOCATION
  (
    FK_ORACLEDBSERVER 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_ORACLEDUMPVERSION.PLN_ODO_01' */

Create Index
  PLN_ODO_01
On
  PDM_ORACLEDUMPVERSION
  (
    FK_ORACLEDBSERVER 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_ORACLEDUMPVERSION.PLN_ODO_02' */

Create Index
  PLN_ODO_02
On
  PDM_ORACLEDUMPVERSION
  (
    FK_ORACLEDBCLIENT 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_SERVERCHARACTERISTIC.PLN_SCC_01' */

Create Index
  PLN_SCC_01
On
  PDM_SERVERCHARACTERISTIC
  (
    FK_MSSQLDBCLIENT_DEFAULT 
  )
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create index PDM_USER.PLN_USR_01' */

Create Index
  PLN_USR_01
On
  PDM_USER
  (
    LOGINNAME 
  )
/

/*---------------------------------------------------------------------------*/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all checks'                            */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASE.PLN_DTE_CK02' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK02
  Check
    ((FK_ORACLEDBSERVER Is Null And FK_MSSQLDBSERVER Is Not Null And TYPE ='MSSQL') Or (FK_ORACLEDBSERVER Is Not Null And FK_MSSQLDBSERVER Is Null And TYPE ='ORACLE') Or (FK_ORACLEDBSERVER Is Null And FK_MSSQLDBSERVER Is Null))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASE.PLN_DTE_CK08' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK08
  Check
    ((OPTION_PARTIAL_IMPORT ='F' And PARTIAL_IMPORT_TABLE_LIST Is Null) Or (OPTION_PARTIAL_IMPORT ='T' And PARTIAL_IMPORT_TABLE_LIST Is Not Null))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASE.PLN_DTE_CK09' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK09
  Check
    ((TYPE ='ORACLE' And Upper(NAME) Not In ('SYS', 'SYSTEM')) Or (TYPE ='MSSQL' And Upper(NAME) <> 'SA'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASE.PLN_DTE_CK10' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK10
  Check
    ((FK_ORACLEDBSFILELOCATION Is Not Null And TYPE ='ORACLE') Or (FK_MSSQLDBSFILELOCATION Is Not Null And TYPE ='MSSQL') Or (FK_ORACLEDBSFILELOCATION Is Null And FK_MSSQLDBSFILELOCATION Is Null))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASE.PLN_DTE_CK11' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK11
  Check
    ((FK_ORACLEDUMPVERSION Is Not Null And TYPE ='ORACLE') Or (FK_MSSQLDUMPVERSION Is Not Null And TYPE ='MSSQL') Or (FK_ORACLEDUMPVERSION Is Null And FK_MSSQLDUMPVERSION Is Null))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASE.PLN_DTE_CK12' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK12
  Check
    ((FK_ORACLEDBCLIENT_FOR_IMPORT Is Not Null And TYPE ='ORACLE') Or (FK_MSSQLDBCLIENT_FOR_IMPORT Is Not Null And TYPE ='MSSQL') Or (FK_ORACLEDBCLIENT_FOR_IMPORT Is Null And FK_MSSQLDBCLIENT_FOR_IMPORT Is Null))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASE.DATABASE_STATUS.PLN_DTE_CK03' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK03
  Check
    (DATABASE_STATUS In ('N/A', 'IN_USE', 'OK', 'NOK'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASE.OPTION_NOT_CREATE_DBUSER.PLN_DTE_CK05' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK05
  Check
    (OPTION_NOT_CREATE_DBUSER In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASE.OPTION_NO_IMPORT.PLN_DTE_CK06' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK06
  Check
    (OPTION_NO_IMPORT In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASE.OPTION_PARTIAL_IMPORT.PLN_DTE_CK07' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK07
  Check
    (OPTION_PARTIAL_IMPORT In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASE.SPACE_IN_USE.PLN_DTE_CK04' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK04
  Check
    (SPACE_IN_USE In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASE.TYPE.PLN_DTE_CK01' */

Alter Table
  PDM_DATABASE
Add
  Constraint
    PLN_DTE_CK01
  Check
    (TYPE In ('ORACLE', 'MSSQL'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASEREQUEST.PLN_DRT_CK03' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK03
  Check
    (EXECUTE_DATETIME Is Not Null Or EXECUTE_STATUS ='REQUEST')
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASEREQUEST.PLN_DRT_CK04' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK04
  Check
    (EXECUTE_LOGFILE Is Not Null Or EXECUTE_STATUS ='REQUEST')
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create table check PDM_DATABASEREQUEST.PLN_DRT_CK09' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK09
  Check
    ((OPTION_PARTIAL_IMPORT ='F' And PARTIAL_IMPORT_TABLE_LIST Is Null) Or (OPTION_PARTIAL_IMPORT ='T' And PARTIAL_IMPORT_TABLE_LIST Is Not Null))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASEREQUEST.EXECUTE_FUNCTION.PLN_DRT_CK01' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK01
  Check
    (EXECUTE_FUNCTION In ('IMPORT', 'DELETE', 'RELOAD', 'CONVERT', 'IMPORT AND CONVERT', 'EXPORT', 'SNAPSHOT'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASEREQUEST.EXECUTE_STATUS.PLN_DRT_CK02' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK02
  Check
    (EXECUTE_STATUS In ('REQUEST', 'PREPARE', 'IMPORTING', 'DELETING', 'CONVERTING', 'FINISHED', 'EXPORTING', 'SNAPSHOTTING'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASEREQUEST.OPTION_NOT_CREATE_DBUSER.PLN_DRT_CK06' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK06
  Check
    (OPTION_NOT_CREATE_DBUSER In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASEREQUEST.OPTION_NO_IMPORT.PLN_DRT_CK07' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK07
  Check
    (OPTION_NO_IMPORT In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_DATABASEREQUEST.OPTION_PARTIAL_IMPORT.PLN_DRT_CK08' */

Alter Table
  PDM_DATABASEREQUEST
Add
  Constraint
    PLN_DRT_CK08
  Check
    (OPTION_PARTIAL_IMPORT In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_IPPORTLIST.IS_FREE.PLN_IPT_CK01' */

Alter Table
  PDM_IPPORTLIST
Add
  Constraint
    PLN_IPT_CK01
  Check
    (IS_FREE In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_ORACLEDBSERVER.IS_TABLESPACE_PER_USER_SERVER.PLN_ODR_CK01' */

Alter Table
  PDM_ORACLEDBSERVER
Add
  Constraint
    PLN_ODR_CK01
  Check
    (IS_TABLESPACE_PER_USER_SERVER In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_SERVERCHARACTERISTIC.CLEANUP_TMPDIRECTORY.PLN_SCC_CK01' */

Alter Table
  PDM_SERVERCHARACTERISTIC
Add
  Constraint
    PLN_SCC_CK01
  Check
    (CLEANUP_TMPDIRECTORY In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_ADAPT_EXPORT_PARAMETERS.PLN_USR_CK09' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK09
  Check
    (MAY_ADAPT_EXPORT_PARAMETERS In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_ADAPT_IMPORT_PARAMETERS.PLN_USR_CK01' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK01
  Check
    (MAY_ADAPT_IMPORT_PARAMETERS In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_DELETE_FOREIGN_DATABASE.PLN_USR_CK02' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK02
  Check
    (MAY_DELETE_FOREIGN_DATABASE In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_EXPORT_DATABASE.PLN_USR_CK08' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK08
  Check
    (MAY_EXPORT_DATABASE In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_OVERRULE_EXPIRATION.PLN_USR_CK04' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK04
  Check
    (MAY_OVERRULE_EXPIRATION In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_OVERRULE_QUOTA.PLN_USR_CK03' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK03
  Check
    (MAY_OVERRULE_QUOTA In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_SET_HORIZONTAL_CONVERSION.PLN_USR_CK06' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK06
  Check
    (MAY_SET_HORIZONTAL_CONVERSION In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_SET_IMPORT_OPTIONS.PLN_USR_CK05' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK05
  Check
    (MAY_SET_IMPORT_OPTIONS In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create column check PDM_USER.MAY_TRACE_APPLICATION.PLN_USR_CK07' */

Alter Table
  PDM_USER
Add
  Constraint
    PLN_USR_CK07
  Check
    (MAY_TRACE_APPLICATION In ('T', 'F'))
/

/*---------------------------------------------------------------------------*/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all triggers'                          */
/*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*~!~Step, Nr=0001, IgnoreError=No, DBMR=xxx */
/*~!~Step, Desc = 'Create trigger PDM_DATABASEREQUEST.PDM_BRU_COLLECTMETRICS' */

  Create Or Replace Trigger
    PDM_BRU_COLLECTMETRICS
  Before
    Update
  Of
    EXECUTE_STATUS
  On
    PDM_DATABASEREQUEST
  Referencing
    New As MyNew
    Old As MyOld
  For Each Row
  Begin
    If ((:MyOld.EXECUTE_STATUS != 'FINISHED') And (:MyNew.EXECUTE_STATUS = 'FINISHED')) Then
      Insert Into
        PDM_METRICS(
          SYSCODE,
          TYPE,
          DATABASE_STATUS,
          SIZE_DATABASE,
          SIZE_DUMPFILE,
          METRIC_CREATEDATE,
          EXECUTE_FUNCTION,
          EXECUTE_DURATION,
          REF_DATABASE
        )
        Select
          PDM_SEQ_METRICS.NEXTVAL,
          db.TYPE,
          db.DATABASE_STATUS,
          db.SIZE_DATABASE,
          db.SIZE_DUMPFILE,
          Sysdate,
          :MyNew.EXECUTE_FUNCTION,
          :MyNew.EXECUTE_DURATION,
          :MyNew.FK_DATABASE
        From
          PDM_DATABASE db
        Where
          SYSCODE = :MyNew.FK_DATABASE;
    End If;
  End;
/

/*---------------------------------------------------------------------------*/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all methods'                           */
/*****************************************************************************/



/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all views'                             */
/*****************************************************************************/

/*****************************************************************************/
/*~!~Section, Nr=0001, Desc = 'Create all sequences'                         */
/*****************************************************************************/

CREATE SEQUENCE PDM_SEQ_METRICS
  START WITH 116249
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
/

CREATE SEQUENCE PDM_SYSCODE_VALUE
  START WITH 143008
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
/

