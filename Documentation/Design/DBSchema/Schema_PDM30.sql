/**********************/
/* Create all tables  */
/**********************/

Create Table PDM_DATABASE
(
  SYSCODE                        Integer        Not Null,
  ACCESSCODE                     VarChar2(25)   Not Null,
  DATABASE_STATUS                VarChar2(15)   Default 'N/A' Not Null,
  DESCRIPTION                    VarChar2(255)  Null,
  DUMPFILE                       VarChar2(255)  Not Null,
  FK_DATABASESERVER              Integer        Not Null,
  FK_USER_DATABASEOWNER          Integer        Not Null,
  NAME                           VarChar2(25)   Not Null,
  OBJECT_OWNER_IN_DUMP           VarChar2(30)   Null,
  OPTION_NO_IMPORT               VarChar2(1)    Default 'F' Not Null,
  SIZE_DATABASE                  Number(16,2)   Null,
  SIZE_DUMPFILE                  Number(16,2)   Null,
  DATABASE_TYPE                  VarChar2(10)   Not Null
)
/


Create Table PDM_USER
(
  SYSCODE                        Integer        Not Null,
  NAME                           VarChar2(50)   Not Null,
  LOGINNAME                      VarChar2(50)   Not Null,
  EMAIL                          VarChar2(255)  Not Null,
  SPACE_QUOTA                    Number(16,2)   Not Null,
  MAY_SET_IMPORT_OPTIONS         VarChar2(1)    Default 'F' Not Null,
  MAY_ADAPT_EXPORT_PARAMETERS    VarChar2(1)    Default 'T' Not Null,
  MAY_ADAPT_IMPORT_PARAMETERS    VarChar2(1)    Default 'T' Not Null,
  MAY_DELETE_FOREIGN_DATABASE    VarChar2(1)    Default 'F' Not Null
)
/


Create Table PDM_DATABASESERVER
(
  SYSCODE                        Integer        Not Null,
  NAME                           VarChar2(20)   Not Null,
  FK_DATABASEVERSION             Integer        Not Null,
  ADMIN_USER                     VarChar2(30)   Not Null,
  ADMIN_PASS                     VarChar2(30)   Not Null,
  DUMPFILE_LOCATION              VarChar2(255)  Not Null, -- Dump files on the file server
  LOGFILE_LOCATION               VarChar2(255)  Not Null, -- Location of log files; Do we still use cental locations or a individual approach?
  DUMPFILE_LOCATION_LOCALPREFIX  VarChar2(50)   Not Null,
  DUMPFILE_LOCATION_UNCPREFIX    VarChar2(50)   Not Null,
  CODE_PAGE                      Number(5,0)        Null,
  CONNECT_SERVER_NAME            VarChar2(100)      Null,
  SORT_ORDER                     Number(5,0)        Null,
  UNICODE_COMPARISONSTYLE        Number(8,0)        Null,
  UNICODE_LOCALEID               Number(8,0)        Null,
  CONNECT_TNS_NAME               VarChar2(30)       Null,
  TEMPORARY_TABLESPACENAME       VarChar2(30)       Null
)
/

Create Table PDM_DBSERVER_DATAFILELOCATION
(
  SYSCODE                        Integer        Not Null,
  FK_DATABASESERVER              Integer        Not Null,
  FILE_LOCATION_LOCAL            VarChar2(250)  Not Null,
  FILE_LOCATION_NETWORK          VarChar2(250)  Not Null
)
/

Create Table PDM_DBSERVER_CLIENT
(
  SYSCODE                        Integer        Not Null,
  FK_DATABASESERVER              Integer        Not Null,
  CLIENT_LOCATION_LOCAL          VarChar2(250)  Not Null,
  CLIENT_LOCATION_NETWORK        VarChar2(250)  Not Null,
  MS_BINDIRECTORY                VarChar2(255)  Not Null,
  MS_ISQL_PROGRAMNAME            VarChar2(50)   Not Null
)

Create Table PDM_REQUESTTYPE
(
  SYSCODE                        Integer        Not Null,
  TYPE                           VarChar2(10)   Not Null,
  DESCRIPTION                    VarChar2(100)  Not Null
)

Insert Into PDM_REQUESTTYPE(SYSCODE, TYPE, DESCRIPTION) Values(1, 'IMPORT', 'Initial import of new database.');
Insert Into PDM_REQUESTTYPE(SYSCODE, TYPE, DESCRIPTION) Values(2, 'RELOAD', 'Reload of an existing database.');
Insert Into PDM_REQUESTTYPE(SYSCODE, TYPE, DESCRIPTION) Values(3, 'DELETE', 'Removal of an existing database.');
Insert Into PDM_REQUESTTYPE(SYSCODE, TYPE, DESCRIPTION) Values(4, 'NEW IMPORT', 'Overwrite an existing database using a new dump file as input.');
Insert Into PDM_REQUESTTYPE(SYSCODE, TYPE, DESCRIPTION) Values(5, 'SNAPSHOT', 'Define the current situation as the poin of return in case of a reload.');
Commit;

Create Table PDM_REQUEST_QUEQUE
(
  SYSCODE                        Integer        Not Null,
  FK_DATABASE                    Integer        Not Null,
  FK_USER_REQUESTOR              Integer        Not Null,
  FK_DATABASEVERSION             Integer        Not Null,
  FK_REQUESTTYPE                 Integer        Not Null,
  REFERENCE_NUMBER               Integer        Not Null,
  REFERENCE_TIMESTAMP            Date           Not Null,
  EXECUTE_STATUS                 VarChar2(50)   Not Null
)

Create Table PDM_SERVER_EXECUTOR
(
  SYSCODE                        Integer        Not Null,
  FK_DATABASESERVER              Integer        Not Null,
  SERVER_EXECUTOR_PROCESS        VarChar2(50)   Not Null  -- path and name of SEP 
)

-- Table PDM_DATABASEVERSION defines the known database versions. 
Create Table PDM_DATABASEVERSION
(
  SYSCODE                        Integer        Not Null,
  DATABASETYPE                   VarChar2(50)   Not Null, -- Oracle / MsSql
  FILETYPE                       VarChar2(50)   Not Null, -- Oracle exp / data pump
  DATABASEVERSION                VarChar2(50)   Not Null -- e.g. 11.1, 10
)


-- Table PDM_COMPATIBILITYMATRIX defines the compatibility between database servers and the versions of dump files.
-- Only explicitly defined combinations are supported.
Create Table PDM_COMPATIBILITYMATRIX 
(
  SYSCODE                        Integer   Not Null,
  FK_DATABASEVERSION             Integer   Not Null,
  FK_DATABASESERVER              Integer   Not Null
)

Create Table PDM_REQUESTLOG 
(
  SYSCODE                        Integer        Not Null,
  FK_DATABASE                    Integer        Not Null,
  FK_USER_REQUESTOR              Integer        Not Null,
  FK_DATABASEVERSION             Integer        Not Null,
  FK_REQUESTTYPE                 Integer        Not Null,
  REFERENCE_NUMBER               Integer        Not Null,
  REFERENCE_TIMESTAMP            Date           Not Null,
  RESULT_EXIT_CODE               Integer        Not Null,
  RESULT_COMMENT                 VarChar2(2000) Null
)

Create Table PDM_REQUESTLOGQUEQUE 
(
  SYSCODE                        Integer        Not Null,
  FK_DATABASE                    Integer        Not Null,
  FK_USER_REQUESTOR              Integer        Not Null,
  FK_DATABASEVERSION             Integer        Not Null,
  FK_REQUESTTYPE                 Integer        Not Null,
  REFERENCE_NUMBER               Integer        Not Null,
  REFERENCE_TIMESTAMP            Date           Not Null,
  RESULT_EXIT_CODE               Integer        Not Null,
  RESULT_COMMENT                 VarChar2(2000) Null
)

/************************/
/* Create all squences  */
/************************/

CREATE SEQUENCE PDM_SYSCODE_VALUE
  START WITH 1
  MAXVALUE 9999999999
  MINVALUE 1
  NOCYCLE
/

/************************/
/* Create all PK's      */
/************************/

Alter Table PDM_DATABASE 
Add Constraint DATABASE_PK
Primary Key(SYSCODE)
/

Alter Table PDM_USER 
Add Constraint USER_PK
Primary Key(SYSCODE)
/

Alter Table PDM_DATABASESERVER 
Add Constraint DATABASESERVER_PK 
Primary Key(SYSCODE)
/

Alter Table PDM_DBSERVER_DATAFILELOCATION 
Add Constraint DBSERVER_DATAFILELOCATION_PK
Primary Key(SYSCODE)
/

Alter Table PDM_DBSERVER_CLIENT 
Add Constraint DBSERVER_CLIENT_PK
Primary Key(SYSCODE)
/

Alter Table PDM_REQUESTTYPE 
Add Constraint REQUESTTYPE_PK
Primary Key(SYSCODE)
/

Alter Table PDM_REQUEST_QUEQUE 
Add Constraint REQUEST_QUEQUE_PK
Primary Key(SYSCODE)
/

Alter Table PDM_REQUESTLOG
Add Constraint REQUESTLOG_PK
Primary Key(SYSCODE)
/

Alter Table PDM_DATABASEVERSION 
Add Constraint DATABASEVERSION_PK
Primary Key(SYSCODE)
/

Alter Table PDM_COMPATIBILITYMATRIX  
Add Constraint COMPATIBILITYMATRIX_PK
Primary Key(SYSCODE)
/


Alter Table PDM_REQUESTLOGQUEQUE  
Add Constraint REQUESTLOGQUEQUE_PK
Primary Key(SYSCODE)
/

Alter Table PDM_SERVER_EXECUTOR 
Add Constraint SERVER_EXECUTOR_PK 
Primary Key(SYSCODE)
/


/************************/
/* Create all FK's      */
/************************/

Alter Table PDM_DATABASE
Add constraint DATABASE_FK01
Foreign Key (FK_DATABASESERVER)
References PDM_DATABASESERVER(SYSCODE)
/

Alter Table PDM_DATABASE
Add constraint DATABASE_FK03
Foreign Key (FK_USER_DATABASEOWNER)
References PDM_USER(SYSCODE)
/

Alter Table PDM_DBSERVER_DATAFILELOCATION
Add constraint DBSERVER_DATAFILELOCATION_FK01
Foreign Key (FK_DATABASESERVER)
References PDM_DATABASESERVER(SYSCODE)
/

Alter Table PDM_DBSERVER_CLIENT
Add constraint DBSERVER_CLIENT_FK01
Foreign Key (FK_DATABASESERVER)
References PDM_DATABASESERVER(SYSCODE)
/

Alter Table PDM_REQUEST_QUEQUE
Add constraint REQUEST_QUEQUE_FK01
Foreign Key (FK_DATABASE)
References PDM_DATABASE(SYSCODE)
/

Alter Table PDM_REQUEST_QUEQUE
Add constraint REQUEST_QUEQUE_FK02
Foreign Key (FK_USER_REQUESTOR)
References PDM_USER(SYSCODE)
/

Alter Table PDM_REQUEST_QUEQUE
Add constraint REQUEST_QUEQUE_FK03
Foreign Key (FK_DATABASEVERSION)
References PDM_DATABASEVERSION(SYSCODE)
/

Alter Table PDM_REQUEST_QUEQUE
Add constraint REQUEST_QUEQUE_FK04
Foreign Key (FK_REQUESTTYPE)
References PDM_REQUESTTYPE(SYSCODE)
/

Alter Table PDM_SERVER_EXECUTOR
Add constraint SERVER_EXECUTOR_FK01
Foreign Key (FK_DATABASESERVER)
References PDM_DATABASESERVER(SYSCODE)
/

Alter Table PDM_COMPATIBILITYMATRIX 
Add constraint DATABASEVERSION_FK01
Foreign Key (FK_DATABASEVERSION)
References PDM_DATABASEVERSION(SYSCODE)
/

Alter Table PDM_COMPATIBILITYMATRIX 
Add constraint DATABASEVERSION_FK02
Foreign Key (FK_DATABASESERVER)
References PDM_DATABASESERVER(SYSCODE)
/

Alter Table PDM_REQUESTLOG 
Add constraint REQUESTLOG_FK01
Foreign Key (FK_DATABASE)
References PDM_DATABASE(SYSCODE)
/

Alter Table PDM_REQUESTLOG 
Add constraint REQUESTLOG_FK02
Foreign Key (FK_USER_REQUESTOR)
References PDM_USER(SYSCODE)
/

Alter Table PDM_REQUESTLOG 
Add constraint REQUESTLOG_FK03
Foreign Key (FK_DATABASEVERSION)
References PDM_DATABASEVERSION(SYSCODE)
/

Alter Table PDM_REQUESTLOG 
Add constraint REQUESTLOG_FK04
Foreign Key (FK_REQUESTTYPE)
References PDM_REQUESTTYPE(SYSCODE)
/

Alter Table PDM_REQUESTLOGQUEQUE 
Add constraint REQUESTLOGQUEQUE_FK01
Foreign Key (FK_DATABASE)
References PDM_DATABASE(SYSCODE)
/

Alter Table PDM_REQUESTLOGQUEQUE 
Add constraint REQUESTLOGQUEQUE_FK02
Foreign Key (FK_USER_REQUESTOR)
References PDM_USER(SYSCODE)
/

Alter Table PDM_REQUESTLOGQUEQUE 
Add constraint REQUESTLOGQUEQUE_FK03
Foreign Key (FK_DATABASEVERSION)
References PDM_DATABASEVERSION(SYSCODE)
/

Alter Table PDM_REQUESTLOGQUEQUE 
Add constraint REQUESTLOGQUEQUE_FK04
Foreign Key (FK_REQUESTTYPE)
References PDM_REQUESTTYPE(SYSCODE)
/

Alter Table PDM_DATABASESERVER
Add constraint DATABASESERVER_FK01
Foreign Key (FK_DATABASEVERSION)
References PDM_DATABASEVERSION(SYSCODE)
/

/************************/
/* Create all CK's      */
/************************/

Alter Table PDM_REQUEST_QUEQUE
Add constraint REQUEST_QUEQUE_CK01
CHECK(EXECUTE_STATUS In ('PENDING0', 'EXECUTING', 'COMPLETED'))
/

Alter Table PDM_DATABASEVERSION
Add Constraint DATABASEVERSION_CK01
Check(DATABASETYPE in ('ORACLE', 'MSSQL'))
/

Alter Table PDM_DATABASEVERSION
Add Constraint DATABASEVERSION_CK02
Check(FILETYPE in ('MSSQL DUMP', 'ORACLE EXP', 'ORACLE DATAPUMP'))
/

Alter Table PDM_DATABASESERVER
Add Constraint DATABASESERVER_CK01
Check ((
  CODE_PAGE                 Is Not Null And
  CONNECT_SERVER_NAME       Is Not Null And
  SORT_ORDER                Is Not Null And
  UNICODE_COMPARISONSTYLE   Is Not Null And
  UNICODE_LOCALEID          Is Not Null And
  TEMPORARY_TABLESPACENAME  Is Null And
  CONNECT_TNS_NAME          Is Null) 
  Or (
  CODE_PAGE                 Is Null And
  CONNECT_SERVER_NAME       Is Null And
  SORT_ORDER                Is Null And
  UNICODE_COMPARISONSTYLE   Is Null And
  UNICODE_LOCALEID          Is Null And
  TEMPORARY_TABLESPACENAME  Is Not Null And 
  CONNECT_TNS_NAME          Is Not Null) 
)
/
  
  

