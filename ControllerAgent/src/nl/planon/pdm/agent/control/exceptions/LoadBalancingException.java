// Planon Enterprise Edition Source file: LoadBalancingException.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control.exceptions;

/**
 * LoadBalancingException
 */
public class LoadBalancingException extends Exception
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new LoadBalancingException object.
   *
   * @param aMessage DOCUMENT ME!
   */
  public LoadBalancingException(String aMessage)
  {
    super(aMessage);
  }


  /**
   * Creates a new LoadBalancingException object.
   *
   * @param aMessage DOCUMENT ME!
   * @param aCause   DOCUMENT ME!
   */
  public LoadBalancingException(String aMessage, Throwable aCause)
  {
    super(aMessage, aCause);
  }
}
