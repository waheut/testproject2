// Planon Enterprise Edition Source file: PDMAgentLoadReporter.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control;

import jade.core.behaviours.*;

import java.util.concurrent.*;

import nl.planon.util.pnlogging.*;

/**
 * PDMAgentLoadPrinter
 */
public class PDMAgentLoadReporter extends TickerBehaviour
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(PDMAgentLoadReporter.class, PnLogCategory.DEFAULT);

  //~ Instance Variables ---------------------------------------------------------------------------

  private final PDMControllerAgent aAgent;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMAgentLoadPrinter object.
   *
   * @param aAgent DOCUMENT ME!
   */
  public PDMAgentLoadReporter(PDMControllerAgent aAgent)
  {
    super(aAgent, TimeUnit.MINUTES.toMillis(1));
    this.aAgent = aAgent;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * DOCUMENT ME!
   */
  @Override protected void onTick()
  {
    try
    {
      this.aAgent.getLoadBalancingStrategy().printStatusOfAgents();
    }
    catch (Throwable t)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to print the status of the agents. See stacktrace for more information.", t);
      }
    }
  }
}
