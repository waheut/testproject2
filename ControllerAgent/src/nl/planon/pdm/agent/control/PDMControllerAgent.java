// Planon Enterprise Edition Source file: PDMControllerAgent.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control;

import jade.content.*;
import jade.content.lang.*;
import jade.content.lang.Codec.*;
import jade.content.lang.sl.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;
import jade.core.*;
import jade.lang.acl.*;

import java.util.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.util.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.agent.control.exceptions.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.client.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.ontology.*;

import nl.planon.util.pnlogging.*;

/**
 * PDMControllerAgent
 *
 * <p>Jade agent that is responsible for administering and issuing the PDM tasks and requests. It
 * contains a behavior, that is executed repetitively to retrieve all initial tasks and issues them
 * to the appropriate agent for executing. It also is responsible for setting the PDM tasks in the
 * correct status.</p>
 */
public class PDMControllerAgent extends PDMAgent
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(PDMControllerAgent.class, PnLogCategory.DEFAULT);

  public static final Codec CODEC = new SLCodec();
  public static final Ontology ONTOLOGY = PDMOntology.getInstance();

  //~ Instance Variables ---------------------------------------------------------------------------

  private final DistributeLoadStrategy loadBalancingStrategy;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMControllerAgent object.
   */
  public PDMControllerAgent()
  {
    this.loadBalancingStrategy = new DistributeLoadStrategy(this);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * This method will write information of a PDM Database Server to Event Log.
   *
   * @param  aPrimaryKeyPDMRequest primary key of pdm request to which the log belongs
   * @param  aLogMessage           the log message
   * @param  aSeverity             severity of log message (info, warning or error)
   * @param  aErrors               Information of all errors generated by the request (that is, the
   *                               error code and its possible variables)
   *
   * @throws AuthorizationException BOMNotFoundException
   * @throws PnErrorListException
   */
  public static void addLogForPDMDbServer(IBaseValue aPrimaryKeyPDMRequest, StringBuilder aLogMessage, EventLogTypeSeverity aSeverity, List<Pair<String, String>> aErrors) throws AuthorizationException, PnErrorListException
  {
    IFOValue boPDMBO = getBOValueByPrimaryKey(BOTypePDMP5Module.BASE_PDM_REQUEST, aPrimaryKeyPDMRequest);

    final BOM bomLogPDMDbServer = (BOM) boPDMBO.getBOMListManager().getBOMByPnNameEx(IBOBasePDMRequestDef.PN_BOM_LOG_PDMDBSERVER);
    bomLogPDMDbServer.setBOValue(boPDMBO);
    bomLogPDMDbServer.getFieldByPnName(IBOBasePDMRequestDef.ARG_MESSAGE_INFORMATION).setAsObject(aLogMessage);
    bomLogPDMDbServer.getFieldByPnName(IBOBasePDMRequestDef.ARG_SEVERITY_OF_LOG).setAsObject(aSeverity);
    if (aErrors != null)
    {
      bomLogPDMDbServer.getFieldByPnName(IBOBasePDMRequestDef.ARG_ERROR_INFORMATION).setAsObject(aErrors);
    }

    BOMResourceLocator.getInstance().execute(bomLogPDMDbServer);
  }


  /**
   * This method will write information of a PDM Request to Event Log.
   *
   * @param  aPrimaryKeyPDMRequest primary key of pdm request to which the log belongs
   * @param  aLogMessage           the log message
   * @param  aSeverity             severity of log message (info, warning or error)
   * @param  aErrors               Information of all errors generated by the request (f.e. code,
   *                               variables)
   *
   * @throws AuthorizationException BOMNotFoundException
   * @throws PnErrorListException
   */
  public static void addLogForPDMRequest(IBaseValue aPrimaryKeyPDMRequest, StringBuilder aLogMessage, EventLogTypeSeverity aSeverity, List<Pair<String, String>> aErrors) throws AuthorizationException, PnErrorListException
  {
    IFOValue boPDMBO = getBOValueByPrimaryKey(BOTypePDMP5Module.BASE_PDM_REQUEST, aPrimaryKeyPDMRequest);

    final BOM bomLogPDMRequest = (BOM) boPDMBO.getBOMListManager().getBOMByPnNameEx(IBOBasePDMRequestDef.PN_BOM_LOG_PDMREQUEST);
    bomLogPDMRequest.setBOValue(boPDMBO);
    bomLogPDMRequest.getFieldByPnName(IBOBasePDMRequestDef.ARG_MESSAGE_INFORMATION).setAsObject(aLogMessage);
    bomLogPDMRequest.getFieldByPnName(IBOBasePDMRequestDef.ARG_SEVERITY_OF_LOG).setAsObject(aSeverity);
    if (aErrors != null)
    {
      bomLogPDMRequest.getFieldByPnName(IBOBasePDMRequestDef.ARG_ERROR_INFORMATION).setAsObject(aErrors);
    }
    BOMResourceLocator.getInstance().execute(bomLogPDMRequest);
  }


  /**
   * method to perform a state change of the PDM BO's PDM database, PDM request and PDM task, cause
   * most of the time they are done simultaneously.
   *
   * @param  aPkPDMTask       primary key of PDMTask that needs state change
   * @param  aToTaskState     pnname of state the pdm task must be set in to
   * @param  aPkPDMRequest    primary key of PDMRequest that needs state change
   * @param  aToRequestState  pnname of state the pdm request must be set in to
   * @param  aPkPDMDatabase   primary key of PDMDatabase that needs state change
   * @param  aToDatabaseState pnname of state the pdm database must be set in to
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static void setNextStatePDMBOs(final IBaseValue aPkPDMTask, final PDMTaskState aToTaskState, final IBaseValue aPkPDMRequest, final PDMRequestState aToRequestState, final IBaseValue aPkPDMDatabase, final PDMDatabaseState aToDatabaseState) throws PnErrorListException, AuthorizationException
  {
    if (aPkPDMTask != null)
    {
      setTaskState(aPkPDMTask, aToTaskState);
    }
    if (aPkPDMRequest != null)
    {
      setNextStatePDMBO(BOTypePDMP5Module.BASE_PDM_REQUEST, aPkPDMRequest, aToRequestState.getPnName());
    }
    if (aPkPDMDatabase != null)
    {
      setNextStatePDMBO(BOTypePDMP5Module.PDM_DATABASE, aPkPDMDatabase, aToDatabaseState.getPnName());
    }
  }


  /**
   * sets state of
   *
   * @param  aPkTask    primary key of task to set state for
   * @param  aTaskState DOCUMENT ME!
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public static void setTaskState(IBaseValue aPkTask, PDMTaskState aTaskState) throws PnErrorListException, AuthorizationException
  {
    setNextStatePDMBO(BOTypePDMP5Module.BASE_PDM_TASK, aPkTask, aTaskState.getPnName());
  }


  /**
   * have the task executed on the selected agent by assigning the task to the agent and send a
   * request to that agent
   *
   * @param  aPDMTaskREInitiator the behaviour that initiates the request to have it executed on a
   *                             database agent
   * @param  aPKPDMTask          primary key PDM Task
   * @param  aRequest            the data needed for the request to be executed
   * @param  aMsgPDMRequest      the message that contains the request to be executed
   * @param  aPDMAgent           agent to which the task will be assigned
   *
   * @throws PnErrorListException
   * @throws CodecException
   * @throws OntologyException
   */
  public void executeTaskOnPDMAgent(AbstractPDMAchieveREInitiator aPDMTaskREInitiator, IBaseValue aPKPDMTask, AbstractRequest aRequest, ACLMessage aMsgPDMRequest, AID aPDMAgent) throws PnErrorListException, CodecException, OntologyException
  {
    try
    {
      this.loadBalancingStrategy.assignTaskToAgent(aPKPDMTask, aPDMAgent);
    }
    catch (LoadBalancingException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to assign task: '" + aPKPDMTask.getAsString() + "', to PMDAgent: '" + aPDMAgent.getName() + "'.", e);
      }
      // do not even try to continue.
      return;
    }

    try
    {
      this.getContentManager().fillContent(aMsgPDMRequest, new Action(aPDMAgent, aRequest));
      aPDMTaskREInitiator.setAgent(this);
      this.addBehaviour(aPDMTaskREInitiator);
    }
    catch (Throwable t) // we have to catch everything or risk a mismatch in the loadbalancer.
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to actually assgin task: '" + aPKPDMTask.getAsString() + "'. Will remove it from the loadbalancer.", t);
      }

      try
      {
        this.loadBalancingStrategy.unassignTaskToAgent(aPKPDMTask, aPDMAgent);
      }
      catch (LoadBalancingException ex)
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to unassign task: '" + aPKPDMTask.getAsString() + "', from PMDAgent: '" + aPDMAgent.getName() + "' while it was assigned before and actually assigning it to be executed failed.", ex);
        }
      }
    }
  }


  /**
   * get Agent Queue
   *
   * @param  aAgentName name of agent to get queue for
   *
   * @return List of pks of requests
   */
  public List<IBaseValue> getAgentQueue(AID aAgentName)
  {
    try
    {
      return this.loadBalancingStrategy.getAgentQueue(aAgentName);
    }
    catch (LoadBalancingException ex)
    {
      if (LOG.isWarnEnabled())
      {
        LOG.warn("Failed to get the agent queue for PDMAgent: '" + aAgentName.getName() + "'. Returning empty list for now.");
      }
      return new ArrayList<>();
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aDatabaseServerCode DOCUMENT ME!
   * @param  aFileType           DOCUMENT ME!
   * @param  aDatabaseType       DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public AID getAgentToExecuteTask(String aDatabaseServerCode, PDMFileType aFileType, PDMDatabaseType aDatabaseType)
  {
    try
    {
      return this.loadBalancingStrategy.getAgentToExecuteTask(aDatabaseServerCode, aFileType, aDatabaseType);
    }
    catch (LoadBalancingException e)
    {
      if (LOG.isWarnEnabled())
      {
        LOG.warn("Failed to get an Agent to execute task for (databaseServerCode: '" + aDatabaseServerCode + "', fileType: '" + aFileType + "', databaseType: '" + aDatabaseType + "'). Will continue but without an agent to execute this taks.", e);
      }
      return null;
    }
  }


  /**
   * provide agent with a type to make it easier searching for it in the JADE Directory Facilitator
   *
   * @return agent type
   */
  public PDMAgentType getAgentType()
  {
    return PDMAgentType.CONTROLLER_AGENT;
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public DistributeLoadStrategy getLoadBalancingStrategy()
  {
    return this.loadBalancingStrategy;
  }


  /**
   * Update the load balancer.
   *
   * @return A list of deleted agents and the tasks that were still assigned to that agent.
   *
   * @throws PnErrorListException
   * @throws BOMNotFoundException
   */
  public Map<AID, java.util.List<IBaseValue>> updateAgents() throws PnErrorListException, BOMNotFoundException
  {
    return this.loadBalancingStrategy.updateAgents();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected void setup()
  {
    super.setup();
    ContentManager contentManager = getContentManager();
    contentManager.registerLanguage(PDMControllerAgent.CODEC);
    contentManager.registerOntology(PDMControllerAgent.ONTOLOGY);

    // Register agent if it is not already registered.
    if (!JadeUtilities.isAgentRegistered(this, this.getAgentType()))
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Started PDM Controller Agent :" + getAID().getName());
      }
      JadeUtilities.registerAgent(this, PDMAgentType.CONTROLLER_AGENT);
    }

    addBehaviour(new PDMTaskRetriever(this));
    addBehaviour(new PDMAgentLoadReporter(this));
  }


  /**
   * get BOValue By Primarykey
   *
   * @param  aBOType
   * @param  aPk
   *
   * @return IFOValue
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private static IFOValue getBOValueByPrimaryKey(final IBOType aBOType, final IBaseValue aPk) throws PnErrorListException, AuthorizationException
  {
    return PDMUtilities.getInstance().getBOValueByPrimaryKey(aBOType, aPk);
  }


  /**
   * Helper method to determine if the given statechange-BOM has the same <em>to-</em>state as the
   * given to-state.
   *
   * @param  aStateChange           the statechange BOM to investigate;
   * @param  aSearchedToStatePnName the to-state PnName.
   *
   * @return <code>true</code> if the given to-state PnName matches the to-state of the given
   *         statechange BOM, <code>false</code> otherwise.
   */
  private static boolean isRequestedStateChange(final BOMStateChange aStateChange, final String aSearchedToStatePnName)
  {
    final String toStatePnName = aStateChange.getToStatePnName();
    return toStatePnName.equals(aSearchedToStatePnName);
  }


  /**
   * set next status of pdm bo
   *
   * @param  aBOType        Type of PDM BO of which the status has to be changed
   * @param  aPk            primary key of PDM BO of which the status has to be changed
   * @param  aToStatePnName pnname of state the PDM BO must be set in to
   *
   * @throws PnErrorListException   on state change failure
   * @throws AuthorizationException
   */
  private static void setNextStatePDMBO(final IBOType aBOType, final IBaseValue aPk, final String aToStatePnName) throws PnErrorListException, AuthorizationException
  {
    IFOValue boPDMBO = getBOValueByPrimaryKey(aBOType, aPk);

    IBOMListManager bomListManager = boPDMBO.getBOMListManager();

    for (IBOM bom : bomListManager.getBOMs())
    {
      if (bom instanceof BOMStateChange)
      {
        final BOMStateChange stateChangeBOM = (BOMStateChange) bom;
        if (isRequestedStateChange(stateChangeBOM, aToStatePnName))
        {
          BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();
          boPDMBO = bomResourceLocator.execute(stateChangeBOM);
          IBOM saveBOM = boPDMBO.getBOMListManager().getSaveBOMEx();
          boPDMBO = bomResourceLocator.execute(saveBOM);
          break;
        }
      }
    }
  }
}
