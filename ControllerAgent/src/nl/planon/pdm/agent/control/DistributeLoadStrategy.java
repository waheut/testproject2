// Planon Enterprise Edition Source file: DistributeLoadStrategy.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control;

import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;

import java.time.*;
import java.time.format.*;
import java.util.*;
import java.util.Map.*;
import java.util.concurrent.*;
import java.util.stream.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabaseserver.bom.*;

import nl.planon.pdm.agent.*;
import nl.planon.pdm.agent.control.exceptions.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;

import nl.planon.util.pnlogging.*;

/**
 * Very simple load distribution, based on agent queue length. Will pick agent with shortest queue.
 *
 * <p>Queue length is registered in an internal structure.</p>
 */
public class DistributeLoadStrategy implements AgentLoadBalancingStrategy
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(DistributeLoadStrategy.class, PnLogCategory.DEFAULT);

  //~ Classes --------------------------------------------------------------------------------------

  /**
   * AgentStatus
   */
  private class AgentStatus
  {
    List<AgentWorkItem> assignedWork;
    ZonedDateTime lastAlive;
    ZonedDateTime lastTaskAssigned;
    ZonedDateTime lastTaskUnassigned;
    ZonedDateTime registeredOn;

    /**
     * Creates a new AgentStatus object.
     *
     * @param lastAlive DOCUMENT ME!
     */
    public AgentStatus(ZonedDateTime lastAlive)
    {
      this.lastAlive = lastAlive;
      this.registeredOn = lastAlive;
      this.assignedWork = new LinkedList<>();
    }
  }

  /**
   * AgentWorkItem
   */
  private class AgentWorkItem
  {
    IBaseValue workItem;
    ZonedDateTime createdAt;

    /**
     * Creates a new AgentWorkItem object.
     *
     * @param aWorkItem  DOCUMENT ME!
     * @param aCreatedAt DOCUMENT ME!
     */
    public AgentWorkItem(IBaseValue aWorkItem, ZonedDateTime aCreatedAt)
    {
      this.workItem = aWorkItem;
      this.createdAt = aCreatedAt;
    }
  }

  //~ Instance Variables ---------------------------------------------------------------------------

  private final Map<AID, AgentStatus> statusPerAgent = new ConcurrentHashMap<>();

  private final PDMControllerAgent agent;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DistributeLoadStrategy object.
   *
   * @param aAgent aPdmControllerAgent
   */
  public DistributeLoadStrategy(PDMControllerAgent aAgent)
  {
    this.agent = aAgent;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   *
   * @throws Exception
   */
  @Override public void assignTaskToAgent(IBaseValue aPkPDMTask, AID aPdmAgent) throws LoadBalancingException
  {
    AgentStatus status = this.statusPerAgent.get(aPdmAgent);
    if (status != null)
    {
      synchronized (status)
      {
        ZonedDateTime now = ZonedDateTime.now();
        status.lastTaskAssigned = now;
        status.assignedWork.add(new AgentWorkItem(aPkPDMTask, now));
      }
    }
    else
    {
      throw new LoadBalancingException("Failed to assign a task to a PDMAgent, because there is no status registered for the agent.");
    }
  }


  /**
   * Get agent queue by agent name Agent will be lazily initialized if not yet in agent register
   *
   * @param  aAgent the AID of the agent
   *
   * @return the queue, a list of TASK syscodes.
   *
   * @throws LoadBalancingException RuntimeException DOCUMENT ME!
   */
  public List<IBaseValue> getAgentQueue(AID aAgent) throws LoadBalancingException
  {
    AgentStatus status = this.statusPerAgent.get(aAgent);
    if (status != null)
    {
      synchronized (status)
      {
        return Collections.unmodifiableList(status.assignedWork.stream().map(entry ->
                entry.workItem).collect(Collectors.toList()));
      }
    }
    else
    {
      throw new LoadBalancingException("Failed to load the AgentQueue because the status is nolonger available.");
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aDatabaseServerCode DOCUMENT ME!
   * @param  aFileType           DOCUMENT ME!
   * @param  aDatabaseType       DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   *
   * @throws LoadBalancingException DOCUMENT ME!
   */
  @Override public AID getAgentToExecuteTask(String aDatabaseServerCode, PDMFileType aFileType, PDMDatabaseType aDatabaseType) throws LoadBalancingException
  {
    // all known agents
    AMSAgentDescription[] agentDescriptions = new AMSAgentDescription[] {};
    try
    {
      SearchConstraints c = new SearchConstraints();
      c.setMaxResults(new Long(-1));
      agentDescriptions = AMSService.search(this.agent, new AMSAgentDescription(), c);
    }
    catch (FIPAException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to use the AMSService to find all active agents. This results in our implementation to think that there are no agents active.", e);
      }
    }

    // all known agents for the specified task.
    List<DFAgentDescription> agentsForTask = new LinkedList(Arrays.asList(PDMAgentUtilities.getAvailableAgents(this.agent, aDatabaseServerCode, aFileType, aDatabaseType)));

    // refetch all active agents to make sure that we get only assign it to a "alive" agent.
    Set<AID> activeAgents = new HashSet<>();
    for (AMSAgentDescription agent : agentDescriptions)
    {
      if ("active".equals(agent.getState()))
      {
        activeAgents.add(agent.getName());
      }
    }

    // Remove the agents that we do not know yet. In a previous iteration we updated the workList again, but this makes handling dead agents very difficult.
    Iterator<DFAgentDescription> agentsForTaskIterator = agentsForTask.iterator();
    while (agentsForTaskIterator.hasNext())
    {
      DFAgentDescription agentDescription = agentsForTaskIterator.next();
      AID agent = agentDescription.getName();

      if (!activeAgents.contains(agent))
      {
        // when agents die the are still in the DFService but not in the AMSAgentDescription. Continue with this knowledge.
        agentsForTaskIterator.remove();
        continue;
      }

      if (!this.statusPerAgent.keySet().contains(agent))
      {
        agentsForTaskIterator.remove();

        if (LOG.isWarnEnabled())
        {
          LOG.warn("Found PDM agent: '" + agent.getName() + "' for executing PDMTask (databaseServerCode: '" + aDatabaseServerCode + "', fileType: '" + aFileType + "', databaseType: '" + aDatabaseType + "'). But this agent is unknown to the loadbalancer. Will be ignored for now.");
        }
      }
    }

    // select the best agent to run the task.
    List<AID> potentialWinners = new ArrayList<>();
    int winnerCount = -1;
    for (DFAgentDescription agentDescription : agentsForTask)
    {
      AID agent = agentDescription.getName();
      AgentStatus status = this.statusPerAgent.get(agent);
      if (status == null)
      {
        if (LOG.isWarnEnabled())
        {
          LOG.warn("Found PDM agent: '" + agent.getName() + "' for executing PDMTask (databaseServerCode: '" + aDatabaseServerCode + "', fileType: '" + aFileType + "', databaseType: '" + aDatabaseType + "'). This PDM agent does not have a status. Which means it should have been removed previously. But was not. Most likely a programming mistake. Investigate! Ignoring it for now.");
        }
        continue;
      }
      synchronized (status)
      {
        List<IBaseValue> assignedWork = status.assignedWork.stream().map(entry -> entry.workItem).collect(Collectors.toList());
        if ((winnerCount == -1) || (assignedWork.size() < winnerCount))
        {
          potentialWinners.clear();
          potentialWinners.add(agent);
          winnerCount = assignedWork.size();
        }
        else if (assignedWork.size() == winnerCount)
        {
          potentialWinners.add(agent);
        }
      }
    }

    if (potentialWinners.isEmpty())
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("No PDM Agent found to execute PDMTask (databaseServerCode: '" + aDatabaseServerCode + "', fileType: '" + aFileType + "', databaseType: '" + aDatabaseType + "').");
      }

      return null;
    }

    // try to make sure that not every time the same agent is picked.
    Collections.shuffle(potentialWinners);

    AID winner = potentialWinners.get(0);
    if (LOG.isInfoEnabled())
    {
      LOG.info("Selected PDM Agent: '" + winner.getName() + "' to execute PDMTask (databaseServerCode: '" + aDatabaseServerCode + "', fileType: '" + aFileType + "', databaseType: '" + aDatabaseType + "').");
    }

    return winner;
  }


  /**
   * DOCUMENT ME!
   */
  public void printStatusOfAgents()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Current PDM load distribution:");

    if (!this.statusPerAgent.isEmpty())
    {
      int agentsSize = this.statusPerAgent.size();
      for (Entry<AID, AgentStatus> entry : this.statusPerAgent.entrySet())
      {
        sb.append(System.lineSeparator());

        AID agent = entry.getKey();
        AgentStatus status = entry.getValue();
        List<AgentWorkItem> work = status.assignedWork;

        sb.append(agent.getName()).append(" - ");
        sb.append("registeredAt: '").append(formatInCurrentTimezone(status.registeredOn)).append("' ");
        sb.append("lastAlive: '").append(formatInCurrentTimezone(status.lastAlive)).append("' ");

        sb.append("work: [");
        int scheduledWorkSize = work.size();
        for (AgentWorkItem item : work)
        {
          sb.append("(workItem: '").append(item.workItem.getAsString()).append("', ").append("createdAt: '" + formatInCurrentTimezone(item.createdAt)).append("')");
          scheduledWorkSize -= 1;
          if (scheduledWorkSize != 0)
          {
            sb.append(", ");
          }
        }
        sb.append("], ");
        sb.append("registeredOn: '").append(formatInCurrentTimezone(status.registeredOn)).append("' ");

        sb.append("lastTaskAssigned: '");
        if (status.lastTaskAssigned != null)
        {
          sb.append(formatInCurrentTimezone(status.lastTaskAssigned));
        }
        else
        {
          sb.append("null");
        }
        sb.append("' ");

        sb.append("lastTaskUnassigned: '");
        if (status.lastTaskUnassigned != null)
        {
          sb.append(formatInCurrentTimezone(status.lastTaskUnassigned));
        }
        else
        {
          sb.append("null");
        }
        sb.append("';");
      }
    }

    if (LOG.isInfoEnabled())
    {
      LOG.info(sb.toString());
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override public void unassignTaskToAgent(IBaseValue aPkTask, AID aPdmAgent) throws LoadBalancingException
  {
    AgentStatus status = this.statusPerAgent.get(aPdmAgent);
    if (status != null)
    {
      synchronized (status)
      {
        ZonedDateTime now = ZonedDateTime.now();
        status.lastTaskUnassigned = now;
        Iterator<AgentWorkItem> it = status.assignedWork.iterator();
        while (it.hasNext())
        {
          AgentWorkItem agent = it.next();

          if (agent.workItem.equals(aPkTask))
          {
            it.remove();
            if (LOG.isInfoEnabled())
            {
              LOG.info("Removed workItem: '" + aPkTask.getAsString() + "' from PDMAgent: '" + aPdmAgent.getName() + "' execution took: '" + Duration.between(agent.createdAt, now).toString() + "'.");
            }
          }
        }
      }
    }
    else
    {
      throw new LoadBalancingException("Failed to unassign a task to a PDMAgent because the status does nolonger exist.");
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   *
   * @throws PnErrorListException
   * @throws BOMNotFoundException
   */
  public Map<AID, List<IBaseValue>> updateAgents() throws PnErrorListException, BOMNotFoundException
  {
    ZonedDateTime scanExecutedOn = ZonedDateTime.now();

    // all known agents
    AMSAgentDescription[] agentDescriptions = new AMSAgentDescription[] {};
    try
    {
      SearchConstraints c = new SearchConstraints();
      c.setMaxResults(new Long(-1));
      agentDescriptions = AMSService.search(this.agent, new AMSAgentDescription(), c);
    }
    catch (FIPAException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to use the AMSService to find all active agents. This results in our implementation to think that there are no agents active.", e);
      }
    }

    // all known database agents.
    DFAgentDescription[] pdmAgentDescriptions = new DFAgentDescription[] {};
    try
    {
      String[][] aPropertyValues = null;
      ServiceDescription serviceDescription = JadeUtilities.fillServiceDescription(PDMAgentType.DATABASE_AGENT.getCode(), null, aPropertyValues);
      pdmAgentDescriptions = JadeUtilities.findAgents(this.agent, serviceDescription);
    }
    catch (Exception e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to use the DFService to find all active PDM agents. This results in our implementation to think that there are no PDM agents active.", e);
      }
    }

    // put all active agents in the agents list.
    java.util.List<AID> agents = new java.util.ArrayList<>(agentDescriptions.length);
    Arrays.asList(agentDescriptions).forEach(element ->
      {
      if (AMSAgentDescription.ACTIVE.equals(element.getState()))
      {
        agents.add(element.getName());
      }
    });

    // put all pdm agents that are active in the pdmagents list.
    java.util.List<AID> pdmAgents = new java.util.ArrayList<>(agentDescriptions.length);
    Arrays.asList(pdmAgentDescriptions).forEach(element ->
      {
      if (agents.contains(element.getName()))
      {
        pdmAgents.add(element.getName());
      }
    });

    // a list of the original queued work. Alive agents will be removed.
    Map<AID, AgentStatus> deletedAgents = new HashMap<>(this.statusPerAgent);

    boolean hasChanges = false;

    for (AID aliveAgent : pdmAgents)
    {
      if (deletedAgents.get(aliveAgent) != null)
      {
        // is still alive. do nothing.
        deletedAgents.remove(aliveAgent);

        // update the last alive property.
        AgentStatus status = this.statusPerAgent.get(aliveAgent);
        if (status != null)
        {
          synchronized (status)
          {
            status.lastAlive = scanExecutedOn;
          }
        }
        else
        {
          //todo log and continue;
          if (LOG.isWarnEnabled())
          {
            LOG.warn("While updating the LoadBalancer with the Jade truth, we found a PDMAgent that was in our internal registry but nolonger has a status (most likely removed). Investigate!");
          }
        }
      }
      else
      {
        // is a new agent. Register it.
        this.statusPerAgent.put(aliveAgent, new AgentStatus(scanExecutedOn));

        hasChanges = true;

        if (LOG.isInfoEnabled())
        {
          LOG.info("Found and registered a new PDMAgent: '" + aliveAgent.getName() + "', scanExecutedOn: '" + formatInCurrentTimezone(scanExecutedOn) + "'.");
        }
      }
    }

    Map<AID, List<IBaseValue>> openWorkItemsPerDeletedAgent = new HashMap<>();

    StringBuilder sb = new StringBuilder();

    int amountOfDeletedAgents = deletedAgents.size();

    for (Map.Entry<AID, AgentStatus> entry : deletedAgents.entrySet())
    {
      hasChanges = true;

      AID agent = entry.getKey();
      AgentStatus status = entry.getValue();

      // delete the actual agent from the internal registration.
      this.statusPerAgent.remove(agent);

      List<AgentWorkItem> scheduledWork = status.assignedWork;
      int scheduledWorkSize = scheduledWork.size();

      // the list with all open work. (will be filled in the logging loop).
      List<IBaseValue> openWork = new ArrayList<>(scheduledWorkSize);

      sb.append("PDMAgent: '").append(agent.getName()).append("' has died.").append(System.lineSeparator());
      sb.append("workItems: [");
      for (AgentWorkItem item : scheduledWork)
      {
        openWork.add(item.workItem);

        sb.append("(workItem: '").append(item.workItem.getAsString()).append("', ").append("createdAt: '" + formatInCurrentTimezone(item.createdAt)).append("')");
        scheduledWorkSize -= 1;
        if (scheduledWorkSize != 0)
        {
          sb.append(", ");
        }
      }
      sb.append("];");

      openWorkItemsPerDeletedAgent.put(agent, openWork);

      amountOfDeletedAgents -= 1;
      if (amountOfDeletedAgents != 0)
      {
        sb.append(System.lineSeparator());
      }
    }

    if (LOG.isInfoEnabled() && (sb.length() > 0))
    {
      LOG.info(sb.toString());
      //Send an email with the above information. Refer: TEC-2765
      sendEmailAboutDiedAgents(System.lineSeparator() + sb.toString());
    }
    if (hasChanges)
    {
      printStatusOfAgents();
    }

    return openWorkItemsPerDeletedAgent;
  }


  /**
   * Formats a zoneddatetime into a given pattern: "yyyy-MM-dd'T'HH:mm:ss'Z'" and returns the same
   * instance in the current timezone
   *
   * @param  aZonedDateTime the time of the agents status
   *                        (registered/lastlive/lasttaskassigned,etc..)
   *
   * @return ZonedDateTime based on this date-time with the requested zone; in this case CET
   */
  private String formatInCurrentTimezone(ZonedDateTime aZonedDateTime)
  {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
    return format.format(aZonedDateTime.withZoneSameInstant(ZoneId.of(TimeZone.getDefault().getID())));
  }


  /**
   * Sends an email about the status of died agents.
   *
   * @param  aEmailText The text to be send via email
   *
   * @throws PnErrorListException
   * @throws BOMNotFoundException
   */
  private void sendEmailAboutDiedAgents(String aEmailText) throws PnErrorListException, BOMNotFoundException
  {
    String currentTime = formatInCurrentTimezone(ZonedDateTime.now());

    IBOMListManager activeBOMs = BOMResourceLocator.getInstance().getBusinessObjectMethods(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER);
    IBOM bomSendMail = activeBOMs.getBOMByPnNameEx(BOBasePDMDatabaseServerDef.PN_BOM_SEND_MAIL);

    StringBuilder htmlText = new StringBuilder();
    htmlText.append("<html>");
    htmlText.append("<body>");
    htmlText.append("The following are the agents died as of " + currentTime);
    htmlText.append("<b><pre>" + aEmailText + "</pre></b>");
    htmlText.append("</body>");
    htmlText.append("</html>");

    bomSendMail.getFieldByPnName(BOMSendMailDef.ARG_EMAIL_SUBJECT).setAsObject("List of PDM died-agents as of " + currentTime);
    bomSendMail.getFieldByPnName(BOMSendMailDef.ARG_EMAIL_CONTENT).setAsObject(htmlText.toString());
    //Execute SendMail bom
    BOMResourceLocator.getInstance().execute(bomSendMail);
  }
}
