// Planon Enterprise Edition Source file: DeleteResponseHandler.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.deleting.responsehandler;

import jade.content.*;
import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.lang.acl.*;

import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.response.*;

import nl.planon.util.pnlogging.*;

/**
 * DeleteResponseHandler
 */
public class DeleteResponseHandler extends AbstractResponseHandler
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(DeleteResponseHandler.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DeleteResponseHandler object.
   *
   * @param  aAgent           agent which receives the response message
   * @param  aResponseMessage the response message that must be handled
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public DeleteResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    super(aAgent, aResponseMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   *
   * <p>in case of a delete request the state after success is OK, otherwise it is a reload request
   * (either initial or export) and the state must stay IN PROGRESS because of an import task that
   * must be carried out after the delete</p>
   */
  public PDMRequestState getRequestStateAfterSuccess()
  {
    if (PDMRequestType.DELETE.equals(getRequestType()))
    {
      return PDMRequestState.OK;
    }
    else
    {
      return PDMRequestState.IN_PROGRESS;
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override public void handleFailureResponse(ACLMessage aFailure) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    StringBuilder feedbackMessage = new StringBuilder("Received failure response from ").append(aFailure.getSender()).append("\n\n");

    PDMTaskState taskState;
    PDMRequestState requestState;
    PDMDatabaseState databaseState;

    // if one of the errors are not for the admin means that the request is not succesfully finish.
    if (isErrorForAdminOnly(aFailure))
    {
      // An error for administrator means that the request is succesfully fulfilled, but there are some minor things
      // we don't want to bother the user with. These things will be logged in the event log of the database server
      // at which the request was executed.
      // task to status 'Ok', both request and database to their 'success' status (dependent to the type of request)
      taskState = PDMTaskState.OK;
      requestState = getRequestStateAfterSuccess();
      databaseState = getDatabaseStateAfterSuccess();
    }
    else
    {
      // PCIS 201467: Before logging the failure, set statuses to correct value:
      // task to status 'NOk', both request and database to their 'failed' status (dependent to the type of request)
      taskState = PDMTaskState.NOT_OK;
      requestState = REQUEST_STATE_AFTER_FAILURE;
      databaseState = PDMDatabaseState.AVAILABLE;
    }

    try
    {
      Concept concept = getMessageContent(aFailure);
      if (concept instanceof ExceptionInformation)
      {
        IExceptionInformation exceptionInformation = (IExceptionInformation) concept;
        handleExceptionInformation(exceptionInformation, feedbackMessage);
      }

      DeleteResponse deleteResponse = getResponse(aFailure);
      try
      {
        if (deleteResponse != null)
        {
          IExceptionInformation exceptionInformation = deleteResponse.getExceptionInformation();
          handleExceptionInformation(exceptionInformation, feedbackMessage);
        }
        feedbackMessage.append("\n\n").append(aFailure.getContent());
        // Log the failure message to the event logging of the PDM Request
      }
      finally
      {
      	if(deleteResponse != null) {
        removeLogFiles(deleteResponse.getLogFileDir(), deleteResponse.getLogFileName());
      }
      }
    }
    catch (PnErrorListException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }
    catch (AuthorizationException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }
    catch (Exception ex)
    {
      feedbackMessage.append("\n: Unable to interpret the response message correctly: ").append(aFailure.toString());
      feedbackMessage.append("\n: Stacktrace: ").append(getStackTrace(ex));
    }

    addEventLogs(aFailure, feedbackMessage);
    handleResponse(aFailure, databaseState, requestState, taskState);
  }


  /**
   * {@inheritDoc}
   */
  @Override public void handleInformResponse(ACLMessage aInform) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    StringBuilder feedbackMessage = new StringBuilder("Request succeeded: Received succesful response from ").append(aInform.getSender());

    PDMDatabaseState databaseState;
    PDMRequestState requestState;
    PDMTaskState taskState;

    try
    {
      DeleteResponse deleteResponse = getResponse(aInform);

      try
      {
        // Log the success message to the event logging of the PDM Request (PCIS 209105: if available)
        if ((deleteResponse != null) && (deleteResponse.getLogFileDir() != null) && (deleteResponse.getLogFileName() != null))
        {
          feedbackMessage.append("\nResponse of request for deleting database. Logfile ").append(deleteResponse.getLogFileName()).append(" can be found in ").append(deleteResponse.getLogFileDir());
          feedbackMessage.append("\n\n").append(deleteResponse.getContentOfLogFile());

          databaseState = getDatabaseStateAfterSuccess();
          requestState = getRequestStateAfterSuccess();
          taskState = getTaskStateAfterSuccess();
        }
        else
        {
          databaseState = PDMDatabaseState.INITIAL;
          requestState = PDMRequestState.NOT_OK;
          taskState = PDMTaskState.NOT_OK;
        }
      }
      finally
      {
      	if(deleteResponse != null) {
        removeLogFiles(deleteResponse.getLogFileDir(), deleteResponse.getLogFileName());
      	}
      }
    }
    catch (PnErrorListException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (AuthorizationException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (Exception ex)
    {
      feedbackMessage.append("\n: Unable to interpret the response message correctly:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }

    PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), feedbackMessage, EventLogTypeSeverity.INFORMATION, null);
    handleResponse(aInform, databaseState, requestState, taskState);
  }


  /**
   * handle Refuse Response
   *
   * @param  aRefuse ACLMessage
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  @Override public void handleRefuseResponse(ACLMessage aRefuse) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    handleRefuseResponse(aRefuse, PDMDatabaseState.AVAILABLE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected DeleteResponse getResponse(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    Concept concept = getMessageContent(aMessage);
    if (concept instanceof DeleteResponse)
    {
      return (DeleteResponse) concept;
    }
    else
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Received incorrect DeleteResponse message (" + ((concept == null) ? "null" : concept.getClass().getSimpleName()) + ")");
      }
      return null;
    }
  }


  /**
   * {@inheritDoc}
   *
   * <p>in case of a delete request the state after success is INITIAL, otherwise it is a reload
   * request (either initial or export) and the state must stay BUSY because of an import task that
   * must be carried out after the delete</p>
   */
  private PDMDatabaseState getDatabaseStateAfterSuccess()
  {
    if (PDMRequestType.DELETE.equals(getRequestType()))
    {
      return PDMDatabaseState.INITIAL;
    }
    else
    {
      return PDMDatabaseState.BUSY;
    }
  }
}
