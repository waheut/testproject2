// Planon Enterprise Edition Source file: OracleDataPumpExportRequestInitiator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.exporting.requestinitiator;

import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.core.*;
import jade.lang.acl.*;

import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.agent.control.behaviour.exporting.responsehandler.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;

import nl.planon.util.pnlogging.*;

/**
 * OracleDataPumpExportRequestInitiator
 */
public class OracleDataPumpExportRequestInitiator extends AbstractPDMAchieveREInitiator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(OracleDataPumpExportRequestInitiator.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleDataPumpExportRequestInitiator object.
   *
   * @param aAgent      agent to which the behavior belongs
   * @param aACLMessage request message to be sent
   */
  public OracleDataPumpExportRequestInitiator(Agent aAgent, ACLMessage aACLMessage)
  {
    super(aAgent, aACLMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public AbstractRequest createRequest()
  {
    return new OracleDataPumpExportRequest();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected AbstractResponseHandler createResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    return new ExportResponseHandler(aAgent, aResponseMessage);
  }
}
