// Planon Enterprise Edition Source file: AnalyzeOracleOldStyleResponseHandler.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.analyzing.responsehandler;

import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.lang.acl.*;

import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.core.common.*;

/**
 * AnalyzeOracleOldStyleImportResponseHandler
 */
public class AnalyzeOracleOldStyleResponseHandler extends AnalyzeResponseHandler
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AnalyzeOracleOldStyleImportResponseHandler object.
   *
   * @param  aAgent           agent which receives the response message
   * @param  aResponseMessage the response message that must be handled
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public AnalyzeOracleOldStyleResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    super(aAgent, aResponseMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected PDMDatabaseType getPDMDatabaseType()
  {
    return PDMDatabaseType.ORACLE;
  }


  /**
   * gets array of allowed file types
   *
   * @return array of allowed file types
   */
  @Override protected PDMFileType[] getPDMFileTypes()
  {
    return new PDMFileType[] {PDMFileType.DATAPUMP, PDMFileType.DATAPUMP_AND_EXP};
  }
}
