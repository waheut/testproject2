// Planon Enterprise Edition Source file: ExportResponseHandler.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.exporting.responsehandler;

import jade.content.*;
import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.lang.acl.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.response.*;

import nl.planon.util.pnlogging.*;

/**
 * ExportResponseHandler
 */
public class ExportResponseHandler extends AbstractResponseHandler
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(ExportResponseHandler.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ExportResponseHandler object.
   *
   * @param  aAgent           agent which receives the response message
   * @param  aResponseMessage the response message that must be handled
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public ExportResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    super(aAgent, aResponseMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void handleFailureResponse(ACLMessage aFailure) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    StringBuilder feedbackMessage = new StringBuilder("Received failure response from ").append(aFailure.getSender()).append("\n\n");

    PDMDatabaseState databaseState;
    PDMRequestState requestState;
    PDMTaskState taskState;

    // if one of the errors are not for the admin means that the request is not succesfully finish.
    if (isErrorForAdminOnly(aFailure))
    {
      // An error for administrator means that the request is succesfully fulfilled, but there are some minor things
      // we don't want to bother the user with. These things will be logged in the event log of the database server
      // at which the request was executed.
      // task to status 'Ok', both request and database to their 'success' status (dependent to the type of request)
      databaseState = PDMDatabaseState.AVAILABLE;
      requestState = getRequestStateAfterSuccess();
      taskState = PDMTaskState.OK;
    }
    else
    {
      // PCIS 201467: Before logging the failure, set statuses to correct value:
      // task to status 'NOk', both request and database to their 'failed' status (dependent to the type of request)
      databaseState = PDMDatabaseState.AVAILABLE;
      requestState = REQUEST_STATE_AFTER_FAILURE;
      taskState = PDMTaskState.NOT_OK;
    }

    try
    {
      // Check if there is only exception information (that is without any request information) send back from database agent
      Concept concept = getMessageContent(aFailure);
      if (concept instanceof ExceptionInformation)
      {
        IExceptionInformation exceptionInformation = (IExceptionInformation) concept;
        handleExceptionInformation(exceptionInformation, feedbackMessage);
      }

      ExportResponse exportResponse = getResponse(aFailure);
      try
      {
        if (exportResponse != null)
        {
          IExceptionInformation exceptionInformation = exportResponse.getExceptionInformation();
          handleExceptionInformation(exceptionInformation, feedbackMessage);
          updateDatabaseInformation(exportResponse.getLogFileDir(), exportResponse.getDumpFileName());

          feedbackMessage.append("\nExported dump file ").append(exportResponse.getDumpFileName()).append(" can be found at ").append(exportResponse.getLogFileDir());
        }
        feedbackMessage.append("\n\n").append(aFailure.getContent());
        // Log the failure message to the event logging of the PDM Request
      }
      finally
      {
      	if(exportResponse != null) {
        removeLogFiles(exportResponse.getLogFileDir(), exportResponse.getLogFileName());
      	}
      }
    }
    catch (PnErrorListException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }
    catch (AuthorizationException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }
    catch (Exception ex)
    {
      feedbackMessage.append("\n: Unable to interpret the response message correctly: ").append(aFailure.toString());
      feedbackMessage.append("\n: Stacktrace: ").append(getStackTrace(ex));
    }

    addEventLogs(aFailure, feedbackMessage);
    handleResponse(aFailure, databaseState, requestState, taskState);
  }


  /**
   * {@inheritDoc}
   */
  @Override public void handleInformResponse(ACLMessage aInform) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    StringBuilder feedbackMessage = new StringBuilder("Request succeeded: Received succesful response from ").append(aInform.getSender());

    PDMDatabaseState databaseState;
    PDMRequestState requestState;
    PDMTaskState taskState;

    try
    {
      ExportResponse exportResponse = getResponse(aInform);

      try
      {
        // Log the success message to the event logging of the PDM Request (PCIS 209105: if available)
        if ((exportResponse != null) && (exportResponse.getLogFileDir() != null) && (exportResponse.getLogFileName() != null))
        {
          updateDatabaseInformation(exportResponse.getLogFileDir(), exportResponse.getDumpFileName());
          feedbackMessage.append("\nResponse of request for exporting dump. Logfile ").append(exportResponse.getLogFileName()).append(" can be found in ").append(exportResponse.getLogFileDir());
          feedbackMessage.append("\n\n").append(exportResponse.getContentOfLogFile());

          databaseState = PDMDatabaseState.AVAILABLE;
          requestState = getRequestStateAfterSuccess();
          taskState = getTaskStateAfterSuccess();
        }
        else
        {
          databaseState = PDMDatabaseState.INITIAL;
          requestState = PDMRequestState.NOT_OK;
          taskState = PDMTaskState.NOT_OK;
        }
      }
      finally
      {
      	if(exportResponse != null) {
        removeLogFiles(exportResponse.getLogFileDir(), exportResponse.getLogFileName());
      	}
      }
    }
    catch (PnErrorListException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (AuthorizationException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (Exception ex)
    {
      feedbackMessage.append("\n: Unable to interpret the response message correctly:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }

    PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), feedbackMessage, EventLogTypeSeverity.INFORMATION, null);
    handleResponse(aInform, databaseState, requestState, taskState);
  }


  /**
   * handle Refuse Response
   *
   * @param  aRefuse ACLMessage
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  @Override public void handleRefuseResponse(ACLMessage aRefuse) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    handleRefuseResponse(aRefuse, PDMDatabaseState.AVAILABLE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected ExportResponse getResponse(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    Concept concept = getMessageContent(aMessage);
    if (concept instanceof ExportResponse)
    {
      return (ExportResponse) concept;
    }
    else
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Received incorrect ExportResponse message");
      }
      return null;
    }
  }


  /**
   * status of request when it is fullfilled succesfully
   *
   * @return status of request when it is fullfilled succesfully.
   */
  private PDMRequestState getRequestStateAfterSuccess()
  {
    return PDMRequestState.OK;
  }


  /**
   * Updates BO PDMDatabase with information obtained after exporting database.
   *
   * @param  aExportedDumpFileDir  directory in which the exported dumpfile can be found
   * @param  aExportedDumpFileName name of dumpfile exported database
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private void updateDatabaseInformation(String aExportedDumpFileDir, String aExportedDumpFileName) throws PnErrorListException, AuthorizationException
  {
    if ((aExportedDumpFileDir != null) && (aExportedDumpFileName != null))
    {
      IFOValue pdmBODatabase = getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE, getPkPDMDatabase());

      IFOFieldValue fieldExportFile = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE);

      assert fieldExportFile.isEmpty() || !aExportedDumpFileName.isEmpty() : " Exported DumpFile Name may not be thrown away by new name";

      fieldExportFile.setAsBaseValue(new BaseStringValue(aExportedDumpFileDir + aExportedDumpFileName));

      IBOM saveDatabaseBOM = pdmBODatabase.getBOMListManager().getSaveBOMEx();

      BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();
      bomResourceLocator.execute(saveDatabaseBOM);
    }
  }
}
