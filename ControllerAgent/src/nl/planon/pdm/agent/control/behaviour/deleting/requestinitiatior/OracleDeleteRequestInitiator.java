// Planon Enterprise Edition Source file: OracleDeleteRequestInitiator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.deleting.requestinitiatior;

import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.core.*;
import jade.lang.acl.*;

import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.agent.control.behaviour.deleting.responsehandler.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;

import nl.planon.util.pnlogging.*;

/**
 * OracleDeleteRequestInitiator
 */
public class OracleDeleteRequestInitiator extends AbstractPDMAchieveREInitiator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(OracleDeleteRequestInitiator.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleDeleteRequestInitiator object.
   *
   * @param aAgent Agent to which this behavior belongs
   * @param aMsg   Message to be used in this behavior
   */
  public OracleDeleteRequestInitiator(Agent aAgent, ACLMessage aMsg)
  {
    super(aAgent, aMsg);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public AbstractRequest createRequest()
  {
    return new OracleDeleteRequest();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected AbstractResponseHandler createResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    return new DeleteResponseHandler(aAgent, aResponseMessage);
  }
}
