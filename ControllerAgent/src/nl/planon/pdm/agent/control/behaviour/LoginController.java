// Planon Enterprise Edition Source file: LoginController.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour;

import javax.security.auth.login.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.core.common.*;

import nl.planon.util.pnlogging.*;

/**
 * keeps tracks of who is logged in.
 */
public class LoginController
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(LoginController.class, PnLogCategory.DEFAULT);
  private static final String PRODUCT_KEY = PDMConstants.PRODUCT_KEY;
  private static final String DEFAULT_USER_NAME = "SYS";
  private static LoginController instance;

  //~ Instance Variables ---------------------------------------------------------------------------

  private String activeUser = null;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new LoginController object.
   */
  private LoginController()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get a instance of this class
   *
   * @return LoginController
   */
  public static LoginController getInstance()
  {
    if (instance == null)
    {
      instance = new LoginController();
    }
    return instance;
  }


  /**
   * login (USER_NAME, PASSWORD)
   *
   * @return false if failed, true if logged in
   */
  public boolean login()
  {
    return login(DEFAULT_USER_NAME);
  }


  /**
   * login as specified user
   *
   * @param  aUserName user to login, password is not required
   *
   * @return true if logged in
   */
  public boolean login(String aUserName)
  {
    assert aUserName != null : "username must be specified for login";

    if (aUserName.equals(this.activeUser))
    {
      return true;
    }
    this.activeUser = null;
    try
    {
      PDMLogin login = new PDMLogin();
      login.login(PRODUCT_KEY, aUserName, null);
    }
    catch (LoginException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to login '" + aUserName + "'");
      }
      return false;
    }
    catch (Throwable e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to login", e);
      }
      return false;
    }

    this.activeUser = aUserName;
    return true;
  }
}
