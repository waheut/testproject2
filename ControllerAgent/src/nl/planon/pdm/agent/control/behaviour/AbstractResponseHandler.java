// Planon Enterprise Edition Source file: AbstractResponseHandler.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour;

import jade.content.*;
import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;
import jade.core.*;
import jade.lang.acl.*;

import org.apache.commons.httpclient.*;

import java.io.*;
import java.util.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxyview.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.util.*;

import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview.*;
import nl.planon.morpheus.pdm.server.logging.requestlogging.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.exceptions.*;
import nl.planon.pdm.core.client.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.response.*;

import nl.planon.util.pnlogging.*;
import nl.planon.util.webdav.*;

/**
 * BaseResponseHandler
 */
public abstract class AbstractResponseHandler
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  protected static final PDMRequestState REQUEST_STATE_AFTER_FAILURE = PDMRequestState.NOT_OK;

  private static final PnLogger LOG = PnLogger.getLogger(AbstractResponseHandler.class, PnLogCategory.DEFAULT);

  //~ Enums ----------------------------------------------------------------------------------------

  /**
   * TypeOfErrors
   *
   * @version $Revision$
   */
  public enum TypeOfErrors
  {
    NO_ERRORS, ERRORS_FOR_USER_ONLY, ERRORS_FOR_ADMIN_ONLY, ERRORS_FOR_BOTH_ADMIN_AND_USER
  }

  //~ Instance Variables ---------------------------------------------------------------------------

  private ACLMessage responseMessage;
  private IBaseValue primaryKeyDatabase;
  private IBaseValue primaryKeyRequest;
  private IBaseValue primaryKeyTask;
  private PDMControllerAgent receivingAgent;
  private PDMRequestType requestType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AbstractResponseHandler object.
   *
   * @param  aAgent           agent which received the response message
   * @param  aResponseMessage the response message that must be handled
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public AbstractResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    setAgent(aAgent);
    setResponseMessage(aResponseMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Set the database, request and task in the correct state as a result of an unsuccesfully
   * executed request
   *
   * @param  aFailure the response message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public abstract void handleFailureResponse(ACLMessage aFailure) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException;


  /**
   * Set the database, request and task in the correct state as a result of a succesfully executed
   * request
   *
   * @param  aInform the response message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public abstract void handleInformResponse(ACLMessage aInform) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException;


  /**
   * handles Refuse Response
   *
   * @param  aRefuse
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public abstract void handleRefuseResponse(ACLMessage aRefuse) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException;


  /**
   * get requesttype that initiated the action
   *
   * @return requesttype that initiated the action
   */
  public PDMRequestType getRequestType()
  {
    return this.requestType;
  }


  /**
   * status of request when it is fullfilled succesfully
   *
   * @return status of request when it is fullfilled succesfully.
   */
  public PDMTaskState getTaskStateAfterSuccess()
  {
    return PDMTaskState.OK;
  }


  /**
   * Handle the agree message that is received
   *
   * @param aAgree the agree message
   */
  public void handleAgreeResponse(ACLMessage aAgree)
  {
    if (LOG.isInfoEnabled())
    {
      LOG.info("The requested task is agreed to be performed");
    }
  }


  /**
   * Set the database, request and task in the specified state request
   *
   * @param  aInform    the response message
   * @param  aTaskState DOCUMENT ME!
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void handleInformResponse(ACLMessage aInform, PDMTaskState aTaskState) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    IBaseValue task = this.primaryKeyTask;
    AID agent = aInform.getSender();
    try
    {
      this.receivingAgent.getLoadBalancingStrategy().unassignTaskToAgent(task, agent);
    }
    catch (LoadBalancingException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to unassign task: '" + task.getAsString() + "' from agent: '" + agent.getName() + "'.", e);
      }
    }
    PDMControllerAgent.setTaskState(getPkTask(), aTaskState);
  }


  /**
   * Set the database, request and task in the specified state request
   *
   * @param  aMessage       the message
   * @param  aDatabaseState database state to set
   * @param  aRequestState  request state to set
   * @param  aTaskState     task state to set
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void handleResponse(ACLMessage aMessage, PDMDatabaseState aDatabaseState, PDMRequestState aRequestState, PDMTaskState aTaskState) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    try
    {
      this.receivingAgent.getLoadBalancingStrategy().unassignTaskToAgent(this.primaryKeyTask, aMessage.getSender());
    }
    catch (Exception e)
    {
      if (LOG.isWarnEnabled())
      {
        LOG.warn("Failed to unassign task: '" + this.primaryKeyTask + "' from PDMAgent: '" + aMessage.getSender() + "'.", e);
      }
    }

    PDMControllerAgent.setNextStatePDMBOs( //
      getPkTask(), aTaskState, //
      getPkPDMRequest(), aRequestState, //
      getPkPDMDatabase(), aDatabaseState);
  }


  /**
   * Dependent on the performative set in the response message, handle it accordingly. If Controller
   * agent is restarted it might still receive pending messages from main container. Those handled
   * here.
   *
   * @param  aResponseMessage the received response message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void handleResponseMessage(ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    switch (aResponseMessage.getPerformative())
    {
      case ACLMessage.INFORM :
      {
        handleInformResponse(aResponseMessage);
        break;
      }
      case ACLMessage.FAILURE :
      {
        handleFailureResponse(aResponseMessage);
        break;
      }
      case ACLMessage.AGREE :
      {
        handleAgreeResponse(aResponseMessage);
        break;
      }
      case ACLMessage.REFUSE :
      {
        handleRefuseResponse(aResponseMessage);
        break;
      }
    }
  }


  /**
   * set the response message that must be handled
   *
   * @param  aResponseMessage the response message that must be handled
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void setResponseMessage(ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    this.responseMessage = aResponseMessage;
    Concept concept = getMessageContent(aResponseMessage);
    if (concept instanceof AbstractResponse)
    {
      AbstractResponse abstractResponse = (AbstractResponse) concept;
      this.primaryKeyTask = new BaseIntegerValue(abstractResponse.getPrimaryKeyTask());
      this.primaryKeyDatabase = new BaseIntegerValue(abstractResponse.getPrimaryKeyDatabase());
      this.primaryKeyRequest = new BaseIntegerValue(abstractResponse.getPrimaryKeyRequest());
      this.requestType = PDMRequestType.getPDMRequestType(abstractResponse.getRequestTypeCode());
    }
    else if (LOG.isErrorEnabled())
    {
      LOG.error("setResponseMessage failed: cannot assign pk of task, database and request because no AbstractResponse found");
    }
  }


  /**
   * gets response content from message
   *
   * @param  aMessage message to get response from
   *
   * @return null or response content
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected abstract AbstractResponse getResponse(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException;


  /**
   * this method analyzes the type of errors in failure message and depending on the outcome add
   * event logs to the request or database server:
   *
   * <ul>
   *   <li>NO_ERRORS                      no logs to add</li>
   *   <li>ERRORS_FOR_USER_ONLY           add an error log to the request</li>
   *   <li>ERRORS_FOR_ADMIN_ONLY          add an error log to the database server on which the
   *     request has failed and a success log to the request</li>
   *   <li>ERRORS_FOR_BOTH_ADMIN_AND_USER add an error log to the database server on which the
   *     request has failed and an error log to the request</li>
   * </ul>
   *
   * @param  aFailure         the failure message
   * @param  aFeedbackMessage the feedback message containing the text to be logged
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws OntologyException
   * @throws CodecException
   */
  protected void addEventLogs(ACLMessage aFailure, StringBuilder aFeedbackMessage) throws PnErrorListException, AuthorizationException, OntologyException, CodecException
  {
    if (getPkPDMRequest() == null)
    {
      // this situation occurs when db agent fails and it cannot determine pk's of database/request/task and it cannot send  normal message
      if (LOG.isErrorEnabled())
      {
        LOG.error("The primary key of request is unknown so cannot store logging!");
      }
      List<IBaseValue> queue = getAgent().getAgentQueue(aFailure.getSender());
      if (queue.size() > 0)
      {
        ISearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDM_TASK_PENDING);
        IBaseValue pkTask = queue.get(0);
        sc.getOperator(IPDMPendingTaskViewDefinition.ALIAS_PK_PDMTASK, SCOperatorType.IN).addBaseValue(pkTask);
        IProxyListValue plv = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(sc);
        List<IProxyValue> proxyValues = plv.getProxyValues();
        int ixDatabase = plv.getColumnNumberByAliasName(IPDMPendingTaskViewDefinition.ALIAS_PK_PDMDATABASE);
        int ixDatabaseServer = plv.getColumnNumberByAliasName(IPDMPendingTaskViewDefinition.ALIAS_PK_PDMDATABASESERVER);
        int ixTask = plv.getColumnNumberByAliasName(IPDMPendingTaskViewDefinition.ALIAS_PK_PDMTASK);
        int ixRequest = plv.getColumnNumberByAliasName(IPDMPendingTaskViewDefinition.ALIAS_PK_PDMREQUEST);
        int ixRequestTypeCode = plv.getColumnNumberByAliasName(IPDMPendingTaskViewDefinition.ALIAS_REQUESTTYPE_CODE);
        if (!proxyValues.isEmpty())
        {
          IProxyValue pv = proxyValues.get(0);
          IBaseValue pkDatabase = pv.getProxyItem(ixDatabase).getBaseValue();
          IBaseValue pkDatabaseServer = pv.getProxyItem(ixDatabaseServer).getBaseValue();
          IBaseValue pkRequest = pv.getProxyItem(ixRequest).getBaseValue();
          PDMRequestType pdmRequestType = PDMRequestType.getPDMRequestType(pv.getProxyItem(ixRequestTypeCode).getAsString());

          PDMControllerAgent.setNextStatePDMBOs( //
            pkTask, PDMTaskState.NOT_OK, //
            pkRequest, PDMRequestState.NOT_OK, //
            pkDatabase, pdmRequestType.getPreviousDatabaseState());

          StringBuilder text = new StringBuilder();
          text.append("Servere db agent ERROR did occur. Task is canceled and state of request is reverted. See database server log for more information.");
          PDMControllerAgent.addLogForPDMRequest(pkRequest, text, EventLogTypeSeverity.ERROR, null);
          text = new StringBuilder();
          text.append("Servere db agent ERROR did occur. As a result of this a message was send without primary keys of db/request/task.");
          text.append("Due to that it was not possible to revert the state of the pending task in a normal way.");
          text.append("The first pending task for agent " + getAgent().getAID() + " was set to " + PDMTaskState.NOT_OK);
          text.append("reverted bo's: pk task  = " + pkTask + ", pk request = " + pkRequest + ", pk Database = " + pkDatabase);
          for (int i = 1; i < queue.size(); i++)
          {
            text.append("next pending request = " + queue.get(i));
          }
          if (LOG.isErrorEnabled())
          {
            LOG.error(text);
          }

          PDMControllerAgent.addLogForPDMDbServer(pkDatabaseServer, text, EventLogTypeSeverity.ERROR, null);

          try
          {
            this.receivingAgent.getLoadBalancingStrategy().unassignTaskToAgent(pkTask, aFailure.getSender());
          }
          catch (LoadBalancingException ex)
          {
            if (LOG.isWarnEnabled())
            {
              LOG.warn("Failed to unassign task: '" + pkTask + "' from PDMAgent: '" + aFailure.getSender() + "'.", ex);
            }
          }
        }
      }

      return;
    }

    // Get the error information out of the message. Since it is not possible to pass the ErrorInformation itself as a BOM parameter
    // the information is stored in a list of pairs of strings, in which the pairs consist of the error code and a message variable.
    List<Pair<String, String>> errorInfo = getErrorInformationFromMessage(aFailure);

    switch (getTypeOfErrorsFromMessage(aFailure))
    {
      case ERRORS_FOR_USER_ONLY :
      {
        // Log the failure message to the event logging of the PDM Request
        PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), aFeedbackMessage, EventLogTypeSeverity.ERROR, errorInfo);
        break;
      }
      case ERRORS_FOR_ADMIN_ONLY :
      {
        // Log the failure message to the event logging of the Database server involved
        PDMControllerAgent.addLogForPDMDbServer(getPkPDMRequest(), aFeedbackMessage, EventLogTypeSeverity.ERROR, errorInfo);
        // Log a success message to the event logging of the PDM Request. The user can use the database without any problem.
        PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), new StringBuilder("Request succeeded: Received succesful response from ").append(aFailure.getSender()), EventLogTypeSeverity.INFORMATION, errorInfo);
        break;
      }
      case ERRORS_FOR_BOTH_ADMIN_AND_USER :
      {
        // Log the failure message to the event logging of the Database server involved
        PDMControllerAgent.addLogForPDMDbServer(getPkPDMRequest(), aFeedbackMessage, EventLogTypeSeverity.ERROR, errorInfo);
        // Log the failure message to the event logging of the PDM Request
        PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), aFeedbackMessage, EventLogTypeSeverity.ERROR, errorInfo);
        break;
      }
      case NO_ERRORS :
      {
        break;
      }
    }
  }


  /**
   * gets agent
   *
   * @return PDMControllerAgent
   */
  protected PDMControllerAgent getAgent()
  {
    return this.receivingAgent;
  }


  /**
   * Retrieve the BO value according to the BOType and primary key that are passed as parameters
   *
   * @param  aBOType     Type of BO for which the BOValue must be retrieved
   * @param  aPrimaryKey primary key of BO of which the BOValue must be retrieved
   *
   * @return IFOValue
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected IFOValue getBOValueByPrimaryKey(IBOType aBOType, IBaseValue aPrimaryKey) throws PnErrorListException, AuthorizationException
  {
    return PDMUtilities.getInstance().getBOValueByPrimaryKey(aBOType, aPrimaryKey);
  }


  /**
   * get content of log file on file server with WebDAV
   *
   * @param  aLogFileDir  directory location of log file
   * @param  aLogFileName name of log file
   *
   * @return content of log file
   *
   * @throws IOException
   */
  protected String getContentOfLogFile(String aLogFileDir, String aLogFileName) throws IOException
  {
    StringBuilder logFileContent = new StringBuilder("Content of logfile: ");
    try
    {
      PnWebDAVResource resource = new PnWebDAVResource(new HttpURL(aLogFileDir + aLogFileName));
      if ((resource != null) && (resource.exists()))
      {
        BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getMethodData()));
        String logFileLine;
        try
        {
          while ((logFileLine = reader.readLine()) != null)
          {
            logFileContent.append(logFileLine).append(PDMRequestLogger.NEW_LINE);
          }
        }
        finally
        {
          reader.close();
        }
      }
    }
    catch (IOException ex)
    {
      logFileContent.append("No logfile found.");
    }
    catch (Exception ex)
    {
      logFileContent.append("Error retrieving logfile.");
    }
    return logFileContent.toString();
  }


  /**
   * get content of message by concept in an ontology
   *
   * @param  aMessage the message to extract the content from
   *
   * @return the message content
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   */
  protected Concept getMessageContent(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException
  {
    if (LOG.isInfoEnabled())
    {
      LOG.info("Content of message: " + aMessage.toString());
    }
    Action action = (Action) getAgent().getContentManager().extractContent(aMessage);
    return action.getAction();
  }


  /**
   * Get Primary key of BO according to its code that is passed as parameter via the proxy view of
   * which the type is passed as parameter.
   *
   * @param  aPVType             PVType to use in order to retrieve the primary key
   * @param  aExpressionNodeName Field name to which the searchcriteria applies
   * @param  aCode               Code of BO of which the primary key must be retrieved
   *
   * @return Primary Key
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected IBaseValue getPKbyCode(IPVType aPVType, String aExpressionNodeName, String aCode) throws PnErrorListException, AuthorizationException
  {
    ISearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(aPVType);
    sc.getOperatorEx(aExpressionNodeName, SCOperatorType.EQUAL).addValue(aCode);
    IProxyListValue plvDbData = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(sc);

    List<IProxyValue> dbDataList = plvDbData.getProxyValues();
    int colPrimaryKey = plvDbData.getColumnNumberByAliasName(BODefinition.PN_PRIMARYKEY);
    IProxyValue proxyValue = dbDataList.get(0);

    return proxyValue.getProxyItem(colPrimaryKey).getBaseValue();
  }


  /**
   * Extract the primary key of the corresponding database from the response message and return it
   * as BaseValue
   *
   * @return primary key of the corresponding database
   */
  protected IBaseValue getPkPDMDatabase()
  {
    return this.primaryKeyDatabase;
  }


  /**
   * Extract the primary key of the corresponding request from the response message and return it as
   * BaseValue
   *
   * @return primary key of the corresponding request
   */
  protected IBaseValue getPkPDMRequest()
  {
    return this.primaryKeyRequest;
  }


  /**
   * Extract the primary key of the corresponding task from the response message and return it as
   * BaseValue
   *
   * @return primary key of the corresponding task
   */
  protected IBaseValue getPkTask()
  {
    return this.primaryKeyTask;
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aException DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  protected String getStackTrace(Throwable aException)
  {
    return PDMUtilities.getInstance().getStackTrace(aException);
  }


  /**
   * loop through exceptions in the failure message and determine whether the exceptions are meant
   * for either users or administrators or both
   *
   * @param  aFailure the message which contains the information of the exception
   *
   * @return <ul>
   *           <li>NO_ERRORS                  if message does not contain any exception at all</li>
   *           <li>USER_ERRORS_ONLY           if message only contains exceptions meant for user
   *           </li>
   *           <li>ADMIN_ERRORS_ONLY          if message only contains exceptions meant for
   *             administrator</li>
   *           <li>BOTH_ADMIN_AND_USER_ERRORS if message contains exception for both user and
   *             administrator</li>
   *         </ul>
   *
   * @throws CodecException
   * @throws OntologyException
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  protected TypeOfErrors getTypeOfErrorsFromMessage(ACLMessage aFailure) throws CodecException, OntologyException, AuthorizationException, PnErrorListException
  {
    TypeOfErrors result = TypeOfErrors.NO_ERRORS;
    IExceptionInformation exceptionInformation = getExceptionInformation(aFailure);

    if (exceptionInformation != null)
    {
      jade.util.leap.List errorInformationList = exceptionInformation.getErrorInformation();
      if (errorInformationList != null)
      {
        for (Iterator errorInfoIterator = errorInformationList.iterator(); errorInfoIterator.hasNext();)
        {
          ErrorInformation errorInfo = (ErrorInformation) errorInfoIterator.next();
          if (errorInfo.getIsErrorForAdministrator())
          {
            if (result == TypeOfErrors.NO_ERRORS)
            {
              result = TypeOfErrors.ERRORS_FOR_ADMIN_ONLY;
            }
            else if (result == TypeOfErrors.ERRORS_FOR_USER_ONLY)
            {
              return TypeOfErrors.ERRORS_FOR_BOTH_ADMIN_AND_USER;
            }
          }
          else
          {
            if (result == TypeOfErrors.NO_ERRORS)
            {
              result = TypeOfErrors.ERRORS_FOR_USER_ONLY;
            }
            else if (result == TypeOfErrors.ERRORS_FOR_ADMIN_ONLY)
            {
              return TypeOfErrors.ERRORS_FOR_BOTH_ADMIN_AND_USER;
            }
          }
        }
      }
    }
    return result;
  }


  /**
   * handle exception information by putting its content into the feedback message
   *
   * @param  aExceptionInformation the exception information
   * @param  aFeedbackMessage      the feedback message
   *
   * @throws IOException
   */
  protected void handleExceptionInformation(IExceptionInformation aExceptionInformation, StringBuilder aFeedbackMessage) throws IOException
  {
    String logFileDir = aExceptionInformation.getLogFileDir();
    String logFileName = aExceptionInformation.getLogFileName();
    if ((logFileDir != null) && (logFileName != null))
    {
      aFeedbackMessage.append("Response of request. Logfile ").append(logFileName).append(" can be found in ").append(logFileDir).append("\n");
      aFeedbackMessage.append(getContentOfLogFile(logFileDir, logFileName));
      removeLogFiles(logFileDir, logFileName);
    }
  }


  /**
   * handle the received refuse message
   *
   * @param  aRefuse  refusal message
   * @param  aDBState state to set db in
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected void handleRefuseResponse(ACLMessage aRefuse, PDMDatabaseState aDBState) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    StringBuilder builder = new StringBuilder();
    builder.append("Received refuse response from ").append(getAgent().getAID()) //
           .append(": Task put back to status initial for another try.");

    String message = aRefuse.getContent();
    if ((message != null) && !message.isEmpty())
    {
      // Log the refusal message to the event logging of the PDM Request
      builder.append("\n").append(message);
    }

    PDMControllerAgent.addLogForPDMRequest(this.getPkPDMRequest(), builder, EventLogTypeSeverity.WARNING, null);
    handleResponse(aRefuse, aDBState, REQUEST_STATE_AFTER_FAILURE, PDMTaskState.NOT_OK);
  }


  /**
   * returns true if errors for aMessage should only be send to administrator and not to user
   *
   * @param  aMessage message to inspect
   *
   * @return boolean
   *
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected boolean isErrorForAdminOnly(ACLMessage aMessage) throws CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    return TypeOfErrors.ERRORS_FOR_ADMIN_ONLY.equals(getTypeOfErrorsFromMessage(aMessage));
  }


  /**
   * DOCUMENT ME!
   *
   * @param aLogFileDir  DOCUMENT ME!
   * @param aLogFileName DOCUMENT ME!
   */
  protected void removeLogFiles(String aLogFileDir, String aLogFileName)
  {
  	if(aLogFileDir == null || aLogFileName == null) {
  		return;
  	}
  	
  	String logFile = aLogFileDir + aLogFileName;
  	if(LOG.isInfoEnabled()) {
  		LOG.info("Will try to remove log file: '" + logFile + "'.");
  	}
    try
    {
      new PnWebDAVResource(new HttpURL(logFile)).deleteMethod();
    }
    catch (IOException ex)
    {
    	if(LOG.isErrorEnabled()) {
    		LOG.error("Failed to remove log file: '" + logFile + "' See stacktrace for more information.", ex);
    	}
    }
  }


  /**
   * get the error information, including message variables, out of the message.
   *
   * @param  aMessage the message
   *
   * @return the error information
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private List<Pair<String, String>> getErrorInformationFromMessage(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    List<Pair<String, String>> result = new ArrayList<Pair<String, String>>();
    IExceptionInformation exceptionInformation = getExceptionInformation(aMessage);

    if (exceptionInformation != null)
    {
      jade.util.leap.List errorInformationList = exceptionInformation.getErrorInformation();
      if (errorInformationList != null)
      {
        for (Iterator errorInfoIterator = errorInformationList.iterator(); errorInfoIterator.hasNext();)
        {
          ErrorInformation errorInfo = (ErrorInformation) errorInfoIterator.next();
          jade.util.leap.List messageVariables = errorInfo.getMessageVariables();
          if (messageVariables != null)
          {
            for (Iterator msgVarIterator = messageVariables.iterator(); msgVarIterator.hasNext();)
            {
              String msgVar = (String) msgVarIterator.next();
              result.add(new Pair(errorInfo.getErrorCode(), msgVar));
            }
          }
        }
      }
    }
    return result;
  }


  /**
   * get the exception information out of the message.
   *
   * @param  aMessage the message
   *
   * @return the exception information
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private IExceptionInformation getExceptionInformation(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    IExceptionInformation exceptionInformation;

    Concept concept = getMessageContent(aMessage);
    if (concept instanceof ExceptionInformation)
    {
      exceptionInformation = (IExceptionInformation) concept;
    }
    else
    {
      AbstractResponse response = getResponse(aMessage);
      exceptionInformation = response.getExceptionInformation();
    }
    return exceptionInformation;
  }


  /**
   * Set the agent that receives the response that must be handled
   *
   * @param aAgent the agent that receives the response message
   */
  private void setAgent(PDMControllerAgent aAgent)
  {
    this.receivingAgent = aAgent;
  }
}
