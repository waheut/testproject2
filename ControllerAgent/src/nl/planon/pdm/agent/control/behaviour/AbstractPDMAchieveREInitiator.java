// Planon Enterprise Edition Source file: AbstractPDMAchieveREInitiator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour;

import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.core.*;
import jade.lang.acl.*;
import jade.proto.*;

import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.core.datamessage.request.base.*;

import nl.planon.util.pnlogging.*;

/**
 * base class for all AchieveREInitiator for PDM.
 *
 * <p>The AchieveREInitiator class is a single homogeneous and effective implementation of all the
 * FIPA-Request-like interaction protocols defined by FIPA, that is all those protocols where the
 * initiator sends a single message (i.e. it performs a single communicative act) within the scope
 * of an interaction protocol in order to verify if the RE (Rational Effect) of the communicative
 * act has been achieved or not.</p>
 */
public abstract class AbstractPDMAchieveREInitiator extends AchieveREInitiator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(AbstractPDMAchieveREInitiator.class, PnLogCategory.DEFAULT);

  //~ Instance Variables ---------------------------------------------------------------------------

  private AbstractResponseHandler responseHandler;
  private final PDMControllerAgent ownerAgent = (PDMControllerAgent) this.myAgent;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * create base class for all AchieveREInitiator for PDM
   *
   * @param aAgent Agent to which this behavior belongs
   * @param aMsg   Message to be used in this behavior
   */
  public AbstractPDMAchieveREInitiator(Agent aAgent, ACLMessage aMsg)
  {
    super(aAgent, aMsg);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * create request
   *
   * @return request.
   */
  public abstract AbstractRequest createRequest();


  /**
   * Create a handler for the response of the request that is initiated by this behaviour
   *
   * @param  aAgent           the agent that received the response message
   * @param  aResponseMessage the message to handle
   *
   * @return the response handler
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected abstract AbstractResponseHandler createResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException;


  /**
   * handle the received agree message
   *
   * @param  aAgree agree message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected void doHandleAgree(ACLMessage aAgree) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
  }


  /**
   * This method is called every time a failure message is received.
   *
   * @param  aFailure received failure message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected final void doHandleFailure(ACLMessage aFailure) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    this.responseHandler = createResponseHandler(this.ownerAgent, aFailure);
    this.responseHandler.handleFailureResponse(aFailure);
  }


  /**
   * This method is called every time a inform message is received.
   *
   * @param  aInform received inform message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected void doHandleInform(ACLMessage aInform) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    this.responseHandler = createResponseHandler(this.ownerAgent, aInform);
    this.responseHandler.handleInformResponse(aInform);
  }


  /**
   * This method is called every time a refuse message is received.
   *
   * @param  aRefuse received refuse message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  protected void doHandleRefuse(ACLMessage aRefuse) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    this.responseHandler = createResponseHandler(this.ownerAgent, aRefuse);
    this.responseHandler.handleRefuseResponse(aRefuse);
  }


  /**
   * handle the received agree message
   *
   * @param aAgree agree message
   */
  @Override protected final void handleAgree(ACLMessage aAgree)
  {
    // The requested task is agreed to be performed
    if (LOG.isInfoEnabled())
    {
      LOG.info("The requested task is agreed to be performed");
    }
    try
    {
      doHandleAgree(aAgree);
    }
    catch (Exception ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("The requested task is agreed, but failed to handle it afterwards", ex);
      }
    }
  }


  /**
   * This method is called every time a failure message is received.
   *
   * @param aFailure received failure message
   */
  @Override protected final void handleFailure(ACLMessage aFailure)
  {
    // The requested task has failed to perform
    if (LOG.isInfoEnabled())
    {
      LOG.info("The requested task has failed to perform");
    }
    try
    {
      doHandleFailure(aFailure);
    }
    catch (Exception ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("The requested task has failed to perform", ex);
      }
    }
  }


  /**
   * This method is called every time a inform message is received.
   *
   * @param aInform received inform message
   */
  @Override protected final void handleInform(ACLMessage aInform)
  {
    super.handleInform(aInform);
    // The requested task is successfully performed
    if (LOG.isInfoEnabled())
    {
      LOG.info("The requested task is successfully performed");
    }
    try
    {
      doHandleInform(aInform);
    }
    catch (Exception ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("The requested task has succeeded but afterwards failed to handle the inform", ex);
      }
    }
  }


  /**
   * This method is called every time a <code>not-understood</code> message is received, which is
   * not out-of-sequence according to the protocol rules. This default implementation does nothing;
   * programmers might wish to override the method in case they need to react to this event.
   *
   * @param aMessage notUnderstood the received not-understood message
   */
  @Override protected final void handleNotUnderstood(ACLMessage aMessage)
  {
    // The requested task is not understood: use sniffer to debug
    if (LOG.isErrorEnabled())
    {
      LOG.error("The requested task is not recognized! " + aMessage);
    }
  }


  /**
   * This method is called every time a <code>out-of-sequence</code> message is received. This
   * default implementation does nothing; programmers might wish to override the method in case they
   * need to react to this event.
   *
   * @param aMessage the received out-of-sequence message
   */
  @Override protected final void handleOutOfSequence(ACLMessage aMessage)
  {
    // The requested task is out of sequence: use sniffer to debug
    if (LOG.isErrorEnabled())
    {
      LOG.error("The requested task is out of sequence! " + aMessage);
    }
  }


  /**
   * This method is called every time a refuse message is received.
   *
   * @param aRefuse received refuse message
   */
  @Override protected final void handleRefuse(ACLMessage aRefuse)
  {
    // The requested task is refused to be performed
    if (LOG.isInfoEnabled())
    {
      LOG.info("The requested task is refused to be performed");
    }
    try
    {
      doHandleRefuse(aRefuse);
    }
    catch (Exception ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("The requested task is refused, and failed to handle it afterwards", ex);
      }
    }
  }
}
