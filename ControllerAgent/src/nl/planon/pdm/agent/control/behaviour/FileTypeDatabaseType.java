// Planon Enterprise Edition Source file: FileTypeDatabaseType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour;

import nl.planon.pdm.core.common.*;

/**
 * combined storage for FileType and DatabaseType
 */
public class FileTypeDatabaseType
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PDMDatabaseType databaseType;
  private PDMFileType fileType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new FileTypeDatabaseType object.
   *
   * @param aFileType
   * @param aDatabaseType
   */
  public FileTypeDatabaseType(PDMFileType aFileType, PDMDatabaseType aDatabaseType)
  {
    this.fileType = aFileType;
    this.databaseType = aDatabaseType;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMDatabaseType
   *
   * @return PDMDatabaseType
   */
  public PDMDatabaseType getDatabaseType()
  {
    return this.databaseType;
  }


  /**
   * gets PDMFileType
   *
   * @return PDMFileType
   */
  public PDMFileType getFileType()
  {
    return this.fileType;
  }
}
