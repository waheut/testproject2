// Planon Enterprise Edition Source file: MSSQLExportRequestInitiator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.exporting.requestinitiator;

import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.core.*;
import jade.lang.acl.*;

import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.agent.control.behaviour.exporting.responsehandler.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;

import nl.planon.util.pnlogging.*;

/**
 * MSSQLExportrequestinitiator
 */
public class MSSQLExportRequestInitiator extends AbstractPDMAchieveREInitiator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(OracleOldStyleExportRequestinitiator.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLExportrequestinitiator object.
   *
   * @param aAgent      agent to which the behavior belongs
   * @param aACLMessage request message to be sent
   */
  public MSSQLExportRequestInitiator(Agent aAgent, ACLMessage aACLMessage)
  {
    super(aAgent, aACLMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public AbstractRequest createRequest()
  {
    return new MSSQLExportRequest();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected AbstractResponseHandler createResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    return new ExportResponseHandler(aAgent, aResponseMessage);
  }
}
