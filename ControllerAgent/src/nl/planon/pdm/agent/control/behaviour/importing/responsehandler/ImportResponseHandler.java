// Planon Enterprise Edition Source file: ImportResponseHandler.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.importing.responsehandler;

import jade.content.*;
import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.lang.acl.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.bom.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.response.*;

import nl.planon.util.pnlogging.*;

/**
 * ImportResponseHandler
 */
public class ImportResponseHandler extends AbstractResponseHandler
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PDMRequestState REQUEST_STATE_ON_SUCCESS = PDMRequestState.OK;
  private static final PDMDatabaseState DB_STATE_ON_SUCCESS = PDMDatabaseState.AVAILABLE;
  private static final PDMDatabaseState DB_STATE_AFTER_FAILURE = PDMDatabaseState.INITIAL;
  private static final PnLogger LOG = PnLogger.getLogger(ImportResponseHandler.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ImportResponseHandler object.
   *
   * @param  aAgent           agent which receives the response message
   * @param  aResponseMessage the response message that must be handled
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public ImportResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    super(aAgent, aResponseMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void handleFailureResponse(ACLMessage aFailure) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    String databaseServerName = "";
    StringBuilder feedbackMessage = new StringBuilder("Received failure response from ").append(aFailure.getSender()).append("\n\n");

    PDMDatabaseState databaseState;
    PDMRequestState requestState;
    PDMTaskState taskState;

    // if one of the errors are not for the admin means that the request is not succesfully finish.
    if (isErrorForAdminOnly(aFailure))
    {
      // An error for administrator means that the request is succesfully fulfilled, but there are some minor things
      // we don't want to bother the user with. These things will be logged in the event log of the database server
      // at which the request was executed.
      // task to status 'Ok', both request and database to their 'success' status (dependent to the type of request)
      databaseState = DB_STATE_ON_SUCCESS;
      requestState = REQUEST_STATE_ON_SUCCESS;
      taskState = PDMTaskState.OK;
    }
    else
    {
      // PCIS 201467: Before logging the failure, set statuses to correct value:
      // task to status 'NOk', both request and database to their 'failed' status (dependent to the type of request)
      databaseState = DB_STATE_AFTER_FAILURE;
      requestState = REQUEST_STATE_AFTER_FAILURE;
      taskState = PDMTaskState.NOT_OK;
    }

    try
    {
      // Check if there is only exception information (that is without any request information) send back from database agent
      Concept concept = getMessageContent(aFailure);
      if (concept instanceof ExceptionInformation)
      {
        IExceptionInformation exceptionInformation = (IExceptionInformation) concept;
        handleExceptionInformation(exceptionInformation, feedbackMessage);
      }

      ImportResponse importResponse = getResponse(aFailure);
      if (importResponse != null)
      {
        try
        {
          IExceptionInformation exceptionInformation = importResponse.getExceptionInformation();
          handleExceptionInformation(exceptionInformation, feedbackMessage);
          updateDatabaseInformation(importResponse);
          IDatabaseDumpInformation dumpInformation = importResponse.getDumpInformation();

          feedbackMessage.append("\nSchema name ").append(dumpInformation.getSchemaName()).append(" Tablespace name ").append(dumpInformation.getTableSpaceNames());
          feedbackMessage.append("\n").append(getContentOfLogFile(importResponse.getLogFileDir(), importResponse.getLogFileName()));
        }
        finally
        {
          removeLogFiles(importResponse.getLogFileDir(), importResponse.getLogFileName());
        }
      }
      feedbackMessage.append("\n\n").append(aFailure.getContent());
    }
    catch (PnErrorListException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }
    catch (AuthorizationException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }
    catch (Exception ex)
    {
      feedbackMessage.append("\n: Unable to interpret the response message correctly: ").append(aFailure.toString());
      feedbackMessage.append("\n: Stacktrace: ").append(getStackTrace(ex));
    }

    addEventLogs(aFailure, feedbackMessage);
    handleResponse(aFailure, databaseState, requestState, taskState);
  }


  /**
   * {@inheritDoc}
   */
  @Override public void handleInformResponse(ACLMessage aInform) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    StringBuilder feedbackMessage = new StringBuilder("Request succeeded: Received succesful response from ").append(aInform.getSender());

    PDMDatabaseState databaseState;
    PDMRequestState requestState;
    PDMTaskState taskState;

    try
    {
      ImportResponse importResponse = getResponse(aInform);

      try
      {
        // Log the success message to the event logging of the PDM Request (PCIS 209105: if available)
        if ((importResponse != null) && (importResponse.getLogFileDir() != null) && (importResponse.getLogFileName() != null))
        {
          updateDatabaseInformation(importResponse);
          feedbackMessage.append("\nResponse of request for importing dump. Logfile ").append(importResponse.getLogFileName()).append(" can be found in ").append(importResponse.getLogFileDir());
          feedbackMessage.append("\n\n").append(importResponse.getContentOfLogFile());

          databaseState = DB_STATE_ON_SUCCESS;
          requestState = REQUEST_STATE_ON_SUCCESS;
          taskState = getTaskStateAfterSuccess();
        }
        else
        {
          databaseState = PDMDatabaseState.INITIAL;
          requestState = PDMRequestState.NOT_OK;
          taskState = PDMTaskState.NOT_OK;
        }
      }
      finally
      {
        removeLogFiles(importResponse.getLogFileDir(), importResponse.getLogFileName());
      }
    }
    catch (PnErrorListException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (AuthorizationException ex)
    {
      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (Exception ex)
    {
      feedbackMessage.append("\n: Unable to interpret the response message correctly:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }

    PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), feedbackMessage, EventLogTypeSeverity.INFORMATION, null);
    handleResponse(aInform, databaseState, requestState, taskState);
  }


  /**
   * handle the received refuse message
   *
   * @param  aRefuse refusal message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  @Override public void handleRefuseResponse(ACLMessage aRefuse) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    handleRefuseResponse(aRefuse, DB_STATE_AFTER_FAILURE);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected ImportResponse getResponse(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    Concept concept = getMessageContent(aMessage);
    if (concept instanceof ImportResponse)
    {
      return (ImportResponse) concept;
    }
    else
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Received incorrect ImportResponse message");
      }
      return null;
    }
  }


  /**
   * Updates BO PDMDatabase with information obtained after analysis of dumpfile.
   *
   * @param  aImportInformation dump information retrieved from DB Agent after analyzing and
   *                            importing an oracle datapump dumpfile
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private void updateDatabaseInformation(IDatabaseImportInformation aImportInformation) throws PnErrorListException, AuthorizationException
  {
    IDatabaseDumpInformation dumpInformation = aImportInformation.getDumpInformation();
    String dumpSchemaName = dumpInformation.getSchemaName();
    String dumpTableSpaceNames = dumpInformation.getTableSpaceNames();
    if ((dumpSchemaName != null) || (dumpTableSpaceNames != null))
    {
      IFOValue pdmBODatabase = getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE, getPkPDMDatabase());

      String databaseServerName = aImportInformation.getDatabaseServerName();
      PDMDatabaseType databaseType = dumpInformation.getDatabaseType();

      IFOFieldValue fieldOwnerInDump = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_OBJECT_OWNER_IN_DUMP);
      IFOFieldValue fieldDumpTableSpaceNames = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_TABLESPACENAMES_IN_DUMP);
      IFOFieldValue fieldDatabaseType = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF);
      IFOFieldValue fieldDatabaseServer = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
      IFOFieldValue fieldPlanonDBInfo = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PLANON_DB_DETAILS);

      assert fieldOwnerInDump.isEmpty() || !dumpSchemaName.isEmpty() : " DumpSchemaName may not be thrown away by new DumpInformation";
      assert fieldDumpTableSpaceNames.isEmpty() || !dumpSchemaName.isEmpty() : " DumpTableSpaceName may not be thrown away by new DumpInformation";
      assert fieldDatabaseType.isEmpty() || (databaseType != null) : " databaseType may not be thrown away by new DumpInformation";
      assert fieldDatabaseServer.isEmpty() || (databaseServerName != null) : " databaseServerName may not be thrown away by new DumpInformation";

      // only in case of a new import or a reload initial, the object owner in dump and tablespace names in dump must be updated.
      if (!PDMRequestType.RELOAD_EXPORT.equals(getRequestType()))
      {
        fieldOwnerInDump.setAsObject(dumpSchemaName);
        fieldDumpTableSpaceNames.setAsObject(dumpTableSpaceNames);
      }
      fieldPlanonDBInfo.setAsObject(dumpInformation.getPlanonDBVersionDetails());

      IBaseValue pkPDMDatabaseServer = getPKbyCode(PVTypePDMP5Module.PDMDBSERVER, IBOBasePDMDatabaseServerDef.PN_CODE, databaseServerName);
      fieldDatabaseServer.setAsBaseValue(pkPDMDatabaseServer);

      IBOM saveDatabaseBOM = pdmBODatabase.getBOMListManager().getSaveBOMEx();

      BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();
      bomResourceLocator.execute(saveDatabaseBOM);

      // PCIS 190806: PDM Request field DatabaseVersionRef should be filled
      // with same value as corresponding field of PDMDatabaseServer
      // that is linked to the same PDMDatabase as the request is.
      if (pkPDMDatabaseServer != null)
      {
        updateRequestInformation(pkPDMDatabaseServer);
      }
    }
  }


  /**
   * update field databaseversionref of the pdm request with the version that corresponds to the
   * database server of which the primary key is passed as parameter.
   *
   * @param  aPkPDMDatabaseServer primary key of database server
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private void updateRequestInformation(IBaseValue aPkPDMDatabaseServer) throws PnErrorListException, AuthorizationException
  {
    BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();

    IFOValue pdmBORequest = getBOValueByPrimaryKey(BOTypePDMP5Module.BASE_PDM_REQUEST, getPkPDMRequest());
    IFOValue pdmBODatabaseServer = getBOValueByPrimaryKey(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, aPkPDMDatabaseServer);
    IFOFieldValue pdmDatabaseVersionRefFieldValue = pdmBODatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);

    IBOMListManager bomListManager = pdmBORequest.getBOMListManager();
    final IBOM updateFieldBOM = bomListManager.getBOMByPnNameEx(IBOBasePDMRequestDef.PN_BOM_UPDATE_DBVERSION_FIELD);
    updateFieldBOM.getFieldByPnName(BOMUpdateDBVersionRefFieldDef.ARG_BOM_DBVERSION).setAsBaseValue(pdmDatabaseVersionRefFieldValue.getBaseValue());
    bomResourceLocator.execute(updateFieldBOM);
  }
}
