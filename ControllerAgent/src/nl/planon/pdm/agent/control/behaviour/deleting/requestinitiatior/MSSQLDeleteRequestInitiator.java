// Planon Enterprise Edition Source file: MSSQLDeleteRequestInitiator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.deleting.requestinitiatior;

import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.core.*;
import jade.lang.acl.*;

import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.agent.control.behaviour.deleting.responsehandler.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;

import nl.planon.util.pnlogging.*;

/**
 * MSSQLDeleteRequestInitiator
 */
public class MSSQLDeleteRequestInitiator extends AbstractPDMAchieveREInitiator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(OracleDeleteRequestInitiator.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLDeleteRequestInitiator object.
   *
   * @param aAgent      agent to which the behavior belongs
   * @param aACLMessage request message to be sent
   */
  public MSSQLDeleteRequestInitiator(Agent aAgent, ACLMessage aACLMessage)
  {
    super(aAgent, aACLMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public AbstractRequest createRequest()
  {
    return new MSSQLDeleteRequest();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected AbstractResponseHandler createResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    return new DeleteResponseHandler(aAgent, aResponseMessage);
  }
}
