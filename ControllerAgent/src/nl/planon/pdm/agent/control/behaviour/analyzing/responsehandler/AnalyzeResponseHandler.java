// Planon Enterprise Edition Source file: AnalyzeResponseHandler.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.analyzing.responsehandler;

import jade.content.*;
import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.lang.acl.*;
import jade.util.leap.*;

import java.util.Iterator;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.businessmodel.systemcode.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.bom.*;

import nl.planon.pdm.agent.*;
import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.response.*;

import nl.planon.util.pnlogging.*;

/**
 * AnalyzeResponseHandler, deals with analysis result and does create import task if possible
 */
public abstract class AnalyzeResponseHandler extends AbstractResponseHandler
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final BaseIntegerValue EMPTY_BASE_VALUE = new BaseIntegerValue();
  private static final PnLogger LOG = PnLogger.getLogger(AnalyzeResponseHandler.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AnalyzeResponseHandler object.
   *
   * @param  aAgent           agent which receives the response message
   * @param  aResponseMessage the response message that must be handled
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public AnalyzeResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    super(aAgent, aResponseMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void handleFailureResponse(ACLMessage aFailure) throws UngroundedException, CodecException, OntologyException, AuthorizationException, PnErrorListException
  {
    StringBuilder feedbackMessage = new StringBuilder("Received failure response from ").append(aFailure.getSender()).append("\n\n");

    PDMDatabaseState databaseState;
    PDMRequestState requestState;
    PDMTaskState taskState;

    boolean analysisTaskPending = false;
    try
    {
      // Check if there is only exception information (that is without any request information) send back from database agent
      Concept concept = getMessageContent(aFailure);
      if (concept instanceof ExceptionInformation)
      {
        IExceptionInformation exceptionInformation = (IExceptionInformation) concept;
        handleExceptionInformation(exceptionInformation, feedbackMessage);
      }

      AnalyzeResponse importResponse = getResponse(aFailure);
      if (importResponse != null)
      {
        try
        {
          Integer primaryKeyRequest = importResponse.getPrimaryKeyRequest();
          List fileType = importResponse.getTriedFileType();
          Iterator in = fileType.iterator();
          StringBuilder sb = new StringBuilder();
          while (in.hasNext())
          {
            PDMFileType pdmFileTypeByCode = PDMFileType.getPDMFileTypeByCode(in.next().toString());
            sb.append("\n").append(pdmFileTypeByCode).append(" Tried to analyze");
            addTriedFileTypeToDB(primaryKeyRequest, pdmFileTypeByCode);
          }
          sb.append("\n");

          PDMDatabaseType databaseType = importResponse.getDumpInformation().getDatabaseType();
          Integer primaryDatabase = importResponse.getPrimaryKeyDatabase();
          if (isAnalyzeImport(primaryDatabase, primaryKeyRequest, sb))
          {
            createNewAnalyzeTask(primaryKeyRequest);

            databaseState = PDMDatabaseState.BUSY;
            requestState = PDMRequestState.IN_PROGRESS;
            taskState = PDMTaskState.OK;

            sb.append("\n").append("New analyze task created");
            analysisTaskPending = true;
          }
          else
          {
            databaseState = PDMDatabaseState.INITIAL;
            requestState = PDMRequestState.NOT_OK;
            taskState = PDMTaskState.NOT_OK;
          }
          IExceptionInformation exceptionInformation = importResponse.getExceptionInformation();
          handleExceptionInformation(exceptionInformation, feedbackMessage);

          feedbackMessage.append("\n").append(sb);
          feedbackMessage.append("\n").append(getContentOfLogFile(importResponse.getLogFileDir(), importResponse.getLogFileName()));
        }
        finally
        {
          removeLogFiles(importResponse.getLogFileDir(), importResponse.getLogFileName());
        }
      }
      else
      {
        databaseState = PDMDatabaseState.INITIAL;
        requestState = PDMRequestState.NOT_OK;
        taskState = PDMTaskState.NOT_OK;

        String agentName = aFailure.getSender().getName();
        System.out.println("TODO: revert state of request handled by this agent! " + agentName);
      }
      feedbackMessage.append("\n\n").append(aFailure.getContent());
    }
    catch (Exception ex)
    {
      feedbackMessage.append("\n: Unable to interpret the response message correctly: ").append(aFailure.toString());
      feedbackMessage.append("\n: Stacktrace: ").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    try
    {
      // if one of the errors are not for the admin means that the request is not succesfully finish.
      if (isErrorForAdminOnly(aFailure) && analysisTaskPending)
      {
        // An error for administrator means that the request is succesfully fulfilled, but there are some minor things
        // we don't want to bother the user with. These things will be logged in the event log of the database server
        // at which the request was executed.
        // task to status 'Ok', both request and database to their 'success' status (dependent to the type of request)
        taskState = PDMTaskState.OK;
        requestState = PDMRequestState.IN_PROGRESS;
        databaseState = PDMDatabaseState.BUSY;
      }
    }
    catch (PnErrorListException ex)
    {
      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;

      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }
    catch (AuthorizationException ex)
    {
      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;

      feedbackMessage.append("\n: Failed to update PDMDatabase and PDMRequest in database: ").append(getStackTrace(ex));
    }

    addEventLogs(aFailure, feedbackMessage);
    super.handleResponse(aFailure, databaseState, requestState, taskState);
  }


  /**
   * {@inheritDoc}
   */
  @Override public void handleInformResponse(ACLMessage aInform) throws UngroundedException, CodecException, OntologyException, BOMNotFoundException, PnErrorListException, AuthorizationException
  {
    boolean serverFound = false;
    StringBuilder msgBuilder = new StringBuilder("Request succeeded: Received succesful response from ").append(aInform.getSender());

    PDMDatabaseState databaseState;
    PDMRequestState requestState;
    PDMTaskState taskState;

    try
    {
      AnalyzeResponse importResponse = getResponse(aInform);

      // Log the success message to the event logging of the PDM Request (PCIS 209105: if available)
      if ((importResponse != null) && (importResponse.getLogFileDir() != null) && (importResponse.getLogFileName() != null))
      {
        try
        {
          StringBuilder infoLog = new StringBuilder();
          try
          {
            Integer location = importResponse.getDatabaseLocation();
            String dumpVersion = importResponse.getDumpInformation().getVersionNumber();

            // get the database regarding to the dbtype. take the server with the least related database
            IFOValue pdmBODatabase = getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE, getPkPDMDatabase());
            IBaseValue pkDatabaseServer = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getBaseValue();
            String databaseOwner = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_ACCOUNT_DATABASEOWNER_REF).getLookupValue().getAsString();
            if (pkDatabaseServer.isEmpty())
            {
              pkDatabaseServer = computeDatabaseServer(databaseOwner, location, dumpVersion, infoLog);
              serverFound = !pkDatabaseServer.isEmpty();
            }
            else
            {
              serverFound = true;
              infoLog.append("Server already specified: " + pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getLookupValue().getAsString());
            }
            updateDatabaseInformation(pkDatabaseServer.getAsInteger(), importResponse);
          }
          catch (Exception ex)
          {
            msgBuilder.append("\n: Failed to determine database server code:\n\n");
            msgBuilder.append("Details:\n").append(infoLog).append("\n");
            msgBuilder.append(getStackTrace(ex));

            serverFound = false;
          }
          if (!serverFound)
          {
            msgBuilder.append("\nNo database server found to Import dump with version number: " + importResponse.getDumpInformation().getVersionNumber() + ". Contact the PDM guys for more information");

            databaseState = PDMDatabaseState.INITIAL;
            requestState = PDMRequestState.NOT_OK;
            taskState = PDMTaskState.NOT_OK;
          }
          else
          {
            // create import task.
            createNewImportTask(importResponse.getPrimaryKeyRequest());
            msgBuilder.append("\nResponse of request for importing dump. Logfile ").append(importResponse.getLogFileName()).append(" can be found in ").append(importResponse.getLogFileDir());
            msgBuilder.append("\n\n").append(importResponse.getContentOfLogFile());
            msgBuilder.append("\n\nDetails:\n").append(infoLog).append("\n");

            databaseState = PDMDatabaseState.BUSY;
            requestState = PDMRequestState.IN_PROGRESS;
            taskState = PDMTaskState.OK;
          }
        }
        finally
        {
          removeLogFiles(importResponse.getLogFileDir(), importResponse.getLogFileName());
        }
      }
      else
      {
        databaseState = PDMDatabaseState.INITIAL;
        requestState = PDMRequestState.NOT_OK;
        taskState = PDMTaskState.NOT_OK;
      }
    }
    catch (AuthorizationException ex)
    {
      msgBuilder.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (PnErrorListException ex)
    {
      msgBuilder.append("\n: Failed to update PDMDatabase and PDMRequest in database:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }
    catch (Exception ex)
    {
      msgBuilder.append("\n: Unable to interpret the response message correctly:\n").append(getStackTrace(ex));

      databaseState = PDMDatabaseState.INITIAL;
      requestState = PDMRequestState.NOT_OK;
      taskState = PDMTaskState.NOT_OK;
    }

    if (serverFound)
    {
      PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), msgBuilder, EventLogTypeSeverity.INFORMATION, null);
    }
    else
    {
      PDMControllerAgent.addLogForPDMRequest(getPkPDMRequest(), msgBuilder, EventLogTypeSeverity.ERROR, null);
    }

    super.handleResponse(aInform, databaseState, requestState, taskState);
  }


  /**
   * handle the received refuse message
   *
   * @param  aRefuse refusal message
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  @Override public void handleRefuseResponse(ACLMessage aRefuse) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    handleRefuseResponse(aRefuse, PDMDatabaseState.INITIAL);
  }


  /**
   * get the database type that corresponds to this response handler
   *
   * @return the database type that corresponds to this response handler
   */
  protected abstract PDMDatabaseType getPDMDatabaseType();


  /**
   * gets array of supported file types
   *
   * @return array of file types
   */
  protected abstract PDMFileType[] getPDMFileTypes();


  /**
   * Create a new analyze task.
   *
   * @param  aPKDMRequest Primary key of the Request
   *
   * @throws AuthorizationException PnErrorListException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   */
  protected void createNewAnalyzeTask(Integer aPKDMRequest) throws AuthorizationException, BOMNotFoundException, PnErrorListException
  {
    IBOType boType = BOTypePDMP5Module.PDM_TASK_ANALYZE_IMPORT;
    final IBOMListManager bomListManager = BOMResourceLocator.getInstance().getActiveBusinessObjectMethods(boType);
    IBOM bom = bomListManager.getBOMByPnNameEx(IBOBasePDMTaskDef.PN_BOM_ADD);
    IBOValue result = BOMResourceLocator.getInstance().execute(bom);
    result.getFieldByPnNameEx(IBOBasePDMTaskDef.PN_PDMREQUEST_REF).setAsObject(aPKDMRequest);

    final IBOMListManager bomListManager1 = result.getBOMListManager();
    final IBOM saveBOM = bomListManager1.getSaveBOMEx();
    BOMResourceLocator.getInstance().execute(saveBOM);
  }


  /**
   * Create a new analyze task.
   *
   * @param  aPKDMRequest Primary key of the Request
   *
   * @throws AuthorizationException PnErrorListException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   */
  protected void createNewImportTask(Integer aPKDMRequest) throws AuthorizationException, BOMNotFoundException, PnErrorListException
  {
    IBOType boType = BOTypePDMP5Module.PDM_TASK_IMPORT;
    final IBOMListManager bomListManager = BOMResourceLocator.getInstance().getActiveBusinessObjectMethods(boType);
    IBOM bom = bomListManager.getBOMByPnNameEx(IBOBasePDMTaskDef.PN_BOM_ADD);
    IBOValue result = BOMResourceLocator.getInstance().execute(bom);
    result.getFieldByPnNameEx(IBOBasePDMTaskDef.PN_PDMREQUEST_REF).setAsObject(aPKDMRequest);

    final IBOMListManager bomListManager1 = result.getBOMListManager();
    final IBOM saveBOM = bomListManager1.getSaveBOMEx();
    BOMResourceLocator.getInstance().execute(saveBOM);
  }


  /**
   * {@inheritDoc}
   */
  @Override protected AnalyzeResponse getResponse(ACLMessage aMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    Concept concept = getMessageContent(aMessage);
    if (concept instanceof AnalyzeResponse)
    {
      return (AnalyzeResponse) concept;
    }
    else
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Received incorrect AnalyzeResponse message");
      }
      return null;
    }
  }


  /**
   * Add tried Database file type to the database.
   *
   * @param  aPKRequest   aRefuse
   * @param  aPDMFileType
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private void addTriedFileTypeToDB(Integer aPKRequest, PDMFileType aPDMFileType) throws PnErrorListException, AuthorizationException
  {
    IBaseValue pkFileType = getPKbyCode(PVTypePDMP5Module.PDMFILETYPE, IBOPDMDatabaseTypeDef.PN_CODE, aPDMFileType.getCode());
    ISearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMANALYZE);

    ISCOperator opFileType = sc.getOperatorEx(IBOPDMAnalyzeDef.PN_FILE_TYPE_REF, SCOperatorType.EQUAL);
    opFileType.addValue(pkFileType.getAsInteger());

    ISCOperator opRequest = sc.getOperatorEx(IBOPDMAnalyzeDef.PN_PDMREQUEST_REF, SCOperatorType.EQUAL);
    opRequest.addValue(aPKRequest);

    IProxyListValue plvAnalyze = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(sc);
    if (plvAnalyze.isEmpty())
    {
      IBOMListManager bomListManager = BOMResourceLocator.getInstance().getBusinessObjectMethods(BOTypePDMP5Module.PDM_ANALYZE);
      IBOM bomByPnNameEx = bomListManager.getBOMByPnNameEx(PnBOMName.PN_BOM_ADD);
      IBOValue result = BOMResourceLocator.getInstance().execute(bomByPnNameEx);

      result.getFieldByPnNameEx(IBOPDMAnalyzeDef.PN_FILE_TYPE_REF).setAsBaseValue(pkFileType);
      result.getFieldByPnNameEx(IBOPDMAnalyzeDef.PN_PDMREQUEST_REF).setAsObject(aPKRequest);

      IBOMListManager bomListManagerSave = result.getBOMListManager();
      IBOM saveBOM = bomListManagerSave.getSaveBOMEx();
      BOMResourceLocator.getInstance().execute(saveBOM);
    }
  }


  /**
   * search for a database server which can handle databasetype and filetypes.
   *
   * @param  aDatabaseOwner user name of database owner
   * @param  aLocation      server must be able to work on this location
   * @param  aDumpVersion   server must support this version
   * @param  aInfoLog       optional: log detailed info about process
   *
   * @return primary key of database server to use, or empty base value if no data
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private IBaseValue computeDatabaseServer(String aDatabaseOwner, Integer aLocation, String aDumpVersion, StringBuilder aInfoLog) throws PnErrorListException, AuthorizationException
  {
    // get the database regarding to the dbtype. take the server with the least related database
    ISearchCriteria scDatabaseServerCount = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDM_DATABASE_SERVER_SEARCH);
    ISCOperator opFileTypeCode = scDatabaseServerCount.getOperatorEx(IBOPDMFileTypeDef.PN_CODE, SCOperatorType.IN);
    ISCOperator opLocation = scDatabaseServerCount.getOperatorEx(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF, SCOperatorType.IN);
    ISCOperator opVersion = scDatabaseServerCount.getOperatorEx(IBOBasePDMDatabaseVersionDef.PN_CODE, SCOperatorType.EQUAL);
    for (PDMFileType fileType : getPDMFileTypes())
    {
      opFileTypeCode.addValue(fileType.getCode());
    }
    opLocation.addValue(aLocation);
    opVersion.addValue(aDumpVersion);
    LoginController loginController = LoginController.getInstance();
    loginController.login(aDatabaseOwner);
    IProxyListValue plv = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scDatabaseServerCount);
    loginController.login();

    // server info contains primary key
    DatabaseServerInfo serverInfo = PDMAgentUtilities.getDatabaseServerInfo(plv, getAgent(), getPDMDatabaseType(), aInfoLog);
    if (serverInfo != null)
    {
      return serverInfo.getPrimarykey().getBaseValue();
    }
    return EMPTY_BASE_VALUE;
  }


  /**
   * return true new task has to be created because there are still format to be tried
   *
   * @param  aPrimaryKeyDatabase primary key of present database
   * @param  aPrimaryKeyRequest  primary key of present request
   * @param  aMsgs               error message
   *
   * @return boolean  true = continue analysis
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  private boolean isAnalyzeImport(Integer aPrimaryKeyDatabase, Integer aPrimaryKeyRequest, StringBuilder aMsgs) throws AuthorizationException, PnErrorListException
  {
    ISearchCriteria scDatabase = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMDATABASE);
    scDatabase.getOperatorEx(IBOPDMDatabaseDef.PN_PRIMARYKEY, SCOperatorType.EQUAL).addValue(aPrimaryKeyDatabase);
    IProxyListValue plvDatabase = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scDatabase);

    if (plvDatabase.isEmpty())
    {
      return false; // database not found!
    }

    // befor creating a new task check if the filetype is empty, otherwise the import failt
    int ixDBFileType = plvDatabase.getColumnNumberByAliasName(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
    int ixDBType = plvDatabase.getColumnNumberByAliasName(BOSystemCodeDef.PN_CODE);
    IProxyItem proxyItemDBType = plvDatabase.getFirstProxy().getProxyItem(ixDBType);
    PDMDatabaseType pdmDatabaseTypeByCode = PDMDatabaseType.getPDMDatabaseTypeByCode(proxyItemDBType.getAsString());
    if (!plvDatabase.getFirstProxy().getProxyItem(ixDBFileType).isEmpty())
    {
      return false;
    }

    java.util.List<PDMFileType> types = new java.util.ArrayList<PDMFileType>();
    types.addAll(PDMAgentUtilities.getAnalyzedFileType(aPrimaryKeyRequest));

    // don't create a new analyze task when all the filetypes of the dbtype already tried.
    if (PDMDatabaseType.ORACLE.equals(pdmDatabaseTypeByCode) && (types.contains(PDMFileType.DATAPUMP) && types.contains(PDMFileType.ORA_EXP)))
    {
      aMsgs.append("Analyze step failed, probably an unrecognized, non-Oracle Dump");
      return false;
    }
    else if (PDMDatabaseType.MSSQL.equals(pdmDatabaseTypeByCode) && types.contains(PDMFileType.MSSQL_DUMP))
    {
      aMsgs.append("Analyze step failed, probably an unrecognized, non-MSSQL Dump");
      return false;
    }
    else if (types.contains(PDMFileType.DATAPUMP) && types.contains(PDMFileType.ORA_EXP) && types.contains(PDMFileType.MSSQL_DUMP))
    {
      aMsgs.append("Analyze step failed, probably an unrecognized Dump");
      return false;
    }
    return true;
  }


  /**
   * Updates BO PDMDatabase with information obtained after analysis of dumpfile (FileType database
   * Type and Database Server).
   *
   * @param  aPrimaryKeyDatabaseServer aImportInformation dump information retrieved from DB Agent
   *                                   after analyzing and importing an oracle datapump dumpfile
   * @param  aImportResponse
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private void updateDatabaseInformation(Integer aPrimaryKeyDatabaseServer, AnalyzeResponse aImportResponse) throws PnErrorListException, AuthorizationException
  {
    IFOValue pdmBODatabase = getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE, getPkPDMDatabase());

    IDatabaseDumpInformation dumpInformation = aImportResponse.getDumpInformation();
    PDMFileType fileType = dumpInformation.getFileType();

    IFOFieldValue fieldDatabaseType = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF);
    IFOFieldValue fieldFileType = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_FILE_TYPE_REF);
    IFOFieldValue fieldExportFileType = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_EXPORT_FILE_TYPE);
    IFOFieldValue fieldDatabaseServer = pdmBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);

    assert fieldDatabaseType.isEmpty() || (getPDMDatabaseType() != null) : " databaseType may not be thrown away by new DumpInformation";
    assert fieldFileType.isEmpty() || (fileType != null) : " fileType may not be thrown away by new DumpInformation";

    fieldDatabaseType.setAsBaseValue(getPKbyCode(PVTypePDMP5Module.PDMDATABASETYPE, IBOPDMDatabaseTypeDef.PN_CODE, getPDMDatabaseType().getCode()));
    IBaseValue pkFileType = getPKbyCode(PVTypePDMP5Module.PDMFILETYPE, IBOPDMFileTypeDef.PN_CODE, fileType.getCode());
    fieldFileType.setAsBaseValue(pkFileType);
    fieldExportFileType.setAsBaseValue(pkFileType);

    fieldDatabaseServer.setAsBaseValue(new BaseIntegerValue(aPrimaryKeyDatabaseServer));

    IBOM saveDatabaseBOM = pdmBODatabase.getBOMListManager().getSaveBOMEx();

    BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();
    bomResourceLocator.execute(saveDatabaseBOM);
  }


  /**
   * update field databaseVersionRef of the pdm request with the version that corresponds to the
   * database server of which the primary key is passed as parameter.
   *
   * @param  aPkPDMDatabaseServer primary key of database server
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private void updateRequestInformation(IBaseValue aPkPDMDatabaseServer) throws PnErrorListException, AuthorizationException
  {
    BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();

    IFOValue pdmBORequest = getBOValueByPrimaryKey(BOTypePDMP5Module.BASE_PDM_REQUEST, getPkPDMRequest());
    IFOValue pdmBODatabaseServer = getBOValueByPrimaryKey(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, aPkPDMDatabaseServer);
    IFOFieldValue pdmDatabaseVersionRefFieldValue = pdmBODatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF);

    IBOMListManager bomListManager = pdmBORequest.getBOMListManager();
    final IBOM updateFieldBOM = bomListManager.getBOMByPnNameEx(IBOBasePDMRequestDef.PN_BOM_UPDATE_DBVERSION_FIELD);
    updateFieldBOM.getFieldByPnName(BOMUpdateDBVersionRefFieldDef.ARG_BOM_DBVERSION).setAsBaseValue(pdmDatabaseVersionRefFieldValue.getBaseValue());
    bomResourceLocator.execute(updateFieldBOM);
    IBOM saveRequestBOM = bomListManager.getSaveBOMEx();
    bomResourceLocator.execute(saveRequestBOM);
  }
}
