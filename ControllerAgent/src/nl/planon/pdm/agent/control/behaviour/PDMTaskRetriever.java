// Planon Enterprise Edition Source file: PDMTaskRetriever.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour;

import jade.content.*;
import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.domain.*;
import jade.lang.acl.*;
import jade.util.leap.ArrayList;
import jade.util.leap.List;

import java.io.*;
import java.text.*;
import java.util.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.businessobject.system.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.client.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.client.resourcelocators.cache.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.localize.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdbserverdatafilelocation.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmtask.proxyview.*;

import nl.planon.pdm.agent.*;
import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.analyzing.requestinitiator.*;
import nl.planon.pdm.agent.control.behaviour.analyzing.responsehandler.*;
import nl.planon.pdm.agent.control.behaviour.deleting.requestinitiatior.*;
import nl.planon.pdm.agent.control.behaviour.deleting.responsehandler.*;
import nl.planon.pdm.agent.control.behaviour.exporting.requestinitiator.*;
import nl.planon.pdm.agent.control.behaviour.exporting.responsehandler.*;
import nl.planon.pdm.agent.control.behaviour.importing.requestinitiator.*;
import nl.planon.pdm.agent.control.behaviour.importing.responsehandler.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.client.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;
import nl.planon.pdm.core.datamessage.oracle.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.response.*;
import nl.planon.pdm.core.ontology.*;

import nl.planon.util.pnlogging.*;

/**
 * behavior that handles all tasks in initial state
 */
public class PDMTaskRetriever extends TickerBehaviour
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String EXTENSION_DPD = PDMConstants.EXTENSION_DPD;
  private static final String EXTENSION_DMP = PDMConstants.EXTENSION_DMP;
  private static final String EXTENSION_BAK = PDMConstants.EXTENSION_BAK;

  private static final PnLogger LOG = PnLogger.getLogger(PDMTaskRetriever.class, PnLogCategory.DEFAULT);

  private static final String VMARG_PDM_AGENT_TIME = "SchedulePDMControllerAgent";

  // time to indicate when the repetitive behavior must be executed again
  private static final long DEFAULT_TICKER_TIME = 30000L; //miliseconds

  //~ Instance Variables ---------------------------------------------------------------------------

  private final PDMControllerAgent ownerAgent;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTaskRetriever object.
   *
   * @param aAgent Agent to which the PDMTaskRetriever behavior belongs
   */
  public PDMTaskRetriever(Agent aAgent)
  {
    super(aAgent, getTickerTime() / 2L);

    this.ownerAgent = (PDMControllerAgent) aAgent;

    if (LOG.isInfoEnabled())
    {
      LOG.info("Ticker behaviour created");
      LOG.info("\t- the check for tasks runs every " + (getTickerTime() / 1000L) + " seconds");
    }
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Get the vmargument pdmAgentTime (set as seconds). default = 30sec.
   *
   * @return milliseconds (default 30000 msec.)
   */
  public static long getTickerTime()
  {
    String pdmAgentTimeString = System.getProperty(VMARG_PDM_AGENT_TIME); // in seconds
    if (pdmAgentTimeString != null)
    {
      long result = Long.parseLong(pdmAgentTimeString);
      result = result * 1000;
      return result;
    }
    return DEFAULT_TICKER_TIME;
  }


  /**
   * This method handles all messages that are pending, for example the responses that arrived
   * during a restart of the application server.
   *
   * <p>It first checks if there is a pending message, then it creates an appropriate response
   * handler (depending on the type of response) and hands over the message to this handler to
   * process it.</p>
   *
   * <p>If the correct responsehandler cannot be determined, the method will continue with the
   * following pending message, after it has written an errormessage to the server log</p>
   *
   * @throws UngroundedException
   * @throws CodecException
   * @throws OntologyException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void handlePendingResponseMessages() throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    ACLMessage responseMessage = this.ownerAgent.receive();
    while (responseMessage != null)
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Handling a pending message...");
      }

      try
      {
        int performative = responseMessage.getPerformative();
        if ((performative == ACLMessage.INFORM) || (performative == ACLMessage.FAILURE) || (performative == ACLMessage.REFUSE))
        {
          AbstractResponseHandler responseHandler = createResponseHandler(this.ownerAgent, responseMessage);
          if (responseHandler != null)
          {
            responseHandler.handleResponseMessage(responseMessage);
          }
        }
      }
      catch (Exception ex)
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("The pending message could not be handled: ");
          LOG.info(ex);
        }
      }
      responseMessage = this.ownerAgent.receive();
    }
  }


  /**
   * gets stack trace as a string
   *
   * @param  aException
   *
   * @return String
   */
  protected String getStackTrace(Throwable aException)
  {
    return PDMUtilities.getInstance().getStackTrace(aException);
  }


  /**
   * This method is invoked periodically with the period defined in the constructor. At every tick:
   *
   * <ul>
   *   <li>a check is done if agents, that are busy executing a request, are still alive. If not,
   *     the request is set to failed.</li>
   *   <li>a check is done if there are pending response messages that need to be handled, f.e.
   *     after a restart of the controller agent</li>
   *   <li>a check is done if there are initial tasks waiting to be picked up. If so, they are
   *     issued to the right database agent for executing.</li>
   * </ul>
   */
  @Override protected void onTick()
  {
    if (LoginController.getInstance().login())
    {
      try
      {
        loadTranslations();
        checkAlivenessBusyDBAgents();
        handlePendingResponseMessages();

        retrievePDMTasks();
      }
      catch (Throwable ex)
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Error when handling pending messages: ", ex);
        }
      }
    }
  }


  /**
   * get database Type and Filetype that not tried. if All the formats are tried, null is returned
   *
   * @param  aDatabaseType   database type
   * @param  aTriedFileTypes list of tried file types
   *
   * @return FileTypeDatabaseType or null if none found
   */
  private static FileTypeDatabaseType getDatabaseTypeAndFileType(PDMDatabaseType aDatabaseType, java.util.List<PDMFileType> aTriedFileTypes)
  {
    boolean isDatapumpAnalyzed = aTriedFileTypes.contains(PDMFileType.DATAPUMP);
    boolean isOldstyleAnalyzed = aTriedFileTypes.contains(PDMFileType.ORA_EXP);
    boolean isMssqlAnalyzed = aTriedFileTypes.contains(PDMFileType.MSSQL_DUMP);
    // get one of the not tried formats
    if (!isDatapumpAnalyzed && (PDMDatabaseType.ORACLE.equals(aDatabaseType) || (aDatabaseType == null)))
    {
      return new FileTypeDatabaseType(PDMFileType.DATAPUMP, PDMDatabaseType.ORACLE);
    }
    else if (!isOldstyleAnalyzed && (PDMDatabaseType.ORACLE.equals(aDatabaseType) || (aDatabaseType == null)))
    {
      return new FileTypeDatabaseType(PDMFileType.ORA_EXP, PDMDatabaseType.ORACLE);
    }
    else if (!isMssqlAnalyzed && (PDMDatabaseType.MSSQL.equals(aDatabaseType) || (aDatabaseType == null)))
    {
      return new FileTypeDatabaseType(PDMFileType.MSSQL_DUMP, PDMDatabaseType.MSSQL);
    }
    return null;
  }


  /**
   * Get the file type related to the extension, and search for a database server with the highest
   * Version and least buzy
   *
   * @param  aDatabaseType PDMDatabaseType
   * @param  aDumpFileName aPlvDatabase PVTypePDMP5Module.PDMDATABASE
   *
   * @return next file type and db type to try, or null if no extension found
   */
  private static FileTypeDatabaseType getDatabaseTypeAndFileTypeRelatedToExtension(PDMDatabaseType aDatabaseType, String aDumpFileName)
  {
    String extension = getFileExtension(aDumpFileName);
    if (extension == null)
    {
      return null;
    }
    else
    {
      PDMFileType fileType = getFileTypeByExtension(aDatabaseType, extension); // replace null value by type found in filename
      PDMDatabaseType dbType;
      switch (fileType)
      {
        case DATAPUMP :
          dbType = PDMDatabaseType.ORACLE;
          break;
        case ORA_EXP :
          dbType = PDMDatabaseType.ORACLE;
          break;
        case MSSQL_DUMP :
          dbType = PDMDatabaseType.MSSQL;
          break;
        default :
          assert false : "Database Type: " + fileType.getCode() + " should not occur";
          return null;
      }
      return new FileTypeDatabaseType(fileType, dbType);
    }
  }


  /**
   * the first time: look to the extension and try to analyze. second time: take one of the
   * remainder formats
   *
   * @param  aPKPDMDatabase can't be null
   * @param  aFileType      PDMFileType
   * @param  aDatabaseType  PDMDatabaseType
   * @param  aPkPDMRequest  can't be null
   *
   * @return FileType, DatabaseType or null if failed
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private static FileTypeDatabaseType getDBTypeAndFileType(IBaseValue aPKPDMDatabase, PDMFileType aFileType, PDMDatabaseType aDatabaseType, IBaseValue aPkPDMRequest) throws PnErrorListException, AuthorizationException
  {
    assert (aPKPDMDatabase != null) && !aPKPDMDatabase.isEmpty() : "pk database is required";
    assert !aPkPDMRequest.isEmpty() : "pk request is required";
    // create a sc for getting the Dump file.
    if (aFileType == null)
    {
      ISearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMREQUEST_DUMPFILE);
      sc.getOperatorEx(IBOBasePDMRequestDef.PN_PRIMARYKEY, SCOperatorType.EQUAL).addBaseValue(aPkPDMRequest);
      IProxyListValue plvDatabase = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(sc);

      java.util.List<PDMFileType> triedFileTypes = PDMAgentUtilities.getAnalyzedFileType(aPkPDMRequest.getAsInteger());

      if (triedFileTypes.isEmpty())
      {
        if (!plvDatabase.isEmpty())
        {
          int columnNumberByAliasName = plvDatabase.getColumnNumberByAliasName(IBOPDMDatabaseDef.PN_DUMPFILE);
          IProxyItem proxyItem = plvDatabase.getFirstProxy().getProxyItem(columnNumberByAliasName);
          String dumpFileName = proxyItem.getAsString();
          // get the extension of the dump. if its one of this, start with the import on that kind of server.

          FileTypeDatabaseType ftdt = getDatabaseTypeAndFileTypeRelatedToExtension(aDatabaseType, dumpFileName);
          if (ftdt == null)
          {
            // if the file doesn't have any extension. start with next type
            ftdt = new FileTypeDatabaseType(PDMFileType.MSSQL_DUMP, PDMDatabaseType.MSSQL);
          }
          return ftdt;
        }
      }
      else
      {
        // analyze based on file extension failed, try other options
        return getDatabaseTypeAndFileType(aDatabaseType, triedFileTypes);
      }
    }
    else
    {
      // file type is specified by user..
      if (aDatabaseType == null)
      {
        // determine database type related to specified file type
        switch (aFileType)
        {
          case MSSQL_DUMP :
            aDatabaseType = PDMDatabaseType.MSSQL;
            break;
          case ORA_EXP :
          case DATAPUMP :
          case DATAPUMP_AND_EXP :
            aDatabaseType = PDMDatabaseType.ORACLE;
            break;
        }
      }
      return new FileTypeDatabaseType(aFileType, aDatabaseType);
    }
    return null;
  }


  /**
   * gets extension of specified file name
   *
   * @param  aFileName to get extension from
   *
   * @return extension or null if not available
   */
  private static String getFileExtension(String aFileName)
  {
    int lastIndexOf = aFileName.lastIndexOf(".");
    if (lastIndexOf < 0)
    {
      return null;
    }
    else
    {
      return aFileName.substring(lastIndexOf + 1);
    }
  }


  /**
   * search filetype by extension within database type.<br>
   * default : Datapump
   *
   * @param  aDBType    database type to limit file type !
   * @param  aExtension aCode
   *
   * @return fileType, or null if extension does not match specified database type
   */
  private static PDMFileType getFileTypeByExtension(PDMDatabaseType aDBType, String aExtension)
  {
    PDMFileType value = null;
    if (aDBType == null)
    {
      value = EXTENSION_DPD.equals(aExtension) ? PDMFileType.DATAPUMP : value;
      value = EXTENSION_DMP.equals(aExtension) ? PDMFileType.ORA_EXP : value;
      value = EXTENSION_BAK.equals(aExtension) ? PDMFileType.MSSQL_DUMP : value;
    }
    else
    {
      switch (aDBType)
      {
        case ORACLE :
          value = EXTENSION_DPD.equals(aExtension) ? PDMFileType.DATAPUMP : value;
          value = EXTENSION_DMP.equals(aExtension) ? PDMFileType.ORA_EXP : value;
          break;
        case MSSQL :
          value = EXTENSION_BAK.equals(aExtension) ? PDMFileType.MSSQL_DUMP : value;
          break;
      }
    }
    // if its not one of the extensions. start with Datapump
    if (value == null)
    {
      value = PDMFileType.DATAPUMP;
    }
    return value;
  }


  /**
   * method to check if busy database agents are still alive while executing a request. First the
   * agent load balancing strategy is checked for agents with pending tasks. Then per found agent a
   * check is done if it is still registered to the Jade framework. If not, then the request will be
   * finished as failure.
   *
   * @throws PnErrorListException
   * @throws AuthorizationException BOMNotFoundException
   */
  private void checkAlivenessBusyDBAgents() throws PnErrorListException, AuthorizationException
  {
    Map<AID, java.util.List<IBaseValue>> deletedTasksPerAgent = this.ownerAgent.updateAgents();

    StringBuilder sb = new StringBuilder();
    for (Map.Entry<AID, java.util.List<IBaseValue>> entry : deletedTasksPerAgent.entrySet())
    {
      for (IBaseValue primaryKeyUnhandledPDMTask : entry.getValue())
      {
        ISearchCriteria scPDMTasks = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMTASK_REQUEST_DATABASE);
        scPDMTasks.getOperator(IBOBasePDMTaskDef.PN_PRIMARYKEY, SCOperatorType.EQUAL).addBaseValue(primaryKeyUnhandledPDMTask);
        IProxyListValue plvPDMTasks = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scPDMTasks);

        int columnIndexPKRequest = plvPDMTasks.getColumnNumberByAliasName(IBOBasePDMRequestDef.PN_PRIMARYKEY);
        int columnIndexPKDatabase = plvPDMTasks.getColumnNumberByAliasName(IBOBasePDMRequestDef.PN_DATABASE_REF);
        int columnIndexTaskTypeCode = plvPDMTasks.getColumnNumberByAliasName(IBOBasePDMTaskDef.PN_SYSSYSTEMTYPE);

        for (IProxyValue item : plvPDMTasks.getProxyValues()) //this will always be one!
        {
          IBaseValue pkPDMRequest = item.getProxyItem(columnIndexPKRequest).getBaseValue();
          IBaseValue pkPDMDatabase = item.getProxyItem(columnIndexPKDatabase).getBaseValue();
          PDMTaskType pdmTaskType = PDMTaskType.getPDMTaskType(item.getProxyItem(columnIndexTaskTypeCode).getAsString());
          // in case of a delete or export task the database must be set back to status available, otherwise (analyze or import) to status initial.
          PDMDatabaseState databaseState = ((PDMTaskType.DELETE.equals(pdmTaskType)) || (PDMTaskType.EXPORT.equals(pdmTaskType))) ? PDMDatabaseState.AVAILABLE : PDMDatabaseState.INITIAL;

          handleException(getTranslatedMessage(ErrorNumberPDMP5Module.EC_UNDETERMINED_REQUEST_STATE), databaseState, primaryKeyUnhandledPDMTask, pkPDMRequest, pkPDMDatabase);

          sb.append("PDMAgent: '" + entry.getKey().getName() + "' died. Deleting (pdmDatabase: '" + pkPDMDatabase + "', pdmRequest: '" + pkPDMRequest + "', pdmTask: '" + primaryKeyUnhandledPDMTask + "');");
          sb.append(System.lineSeparator());
        }
      }
    }

    if (LOG.isInfoEnabled() && (sb.length() > 0))
    {
      String message = sb.toString();
      message = message.substring(0, message.length() - System.lineSeparator().length());
      LOG.info(message);
    }
  }


  /**
   * converts items in the ProxyValue to a map, in order to have them easily send via Jade messages
   *
   * @param  aProxyValue     for retrieving the field values
   * @param  aProxyListValue for retrieving the header values
   *
   * @return map of field header values and their corresponding field values
   */
  private Map<String, Object> convertProxyValueToMap(IProxyValue aProxyValue, IProxyListValue aProxyListValue)
  {
    Map<String, Object> result = new HashMap();
    for (IProxyHeaderValue proxyHeaderValue : aProxyListValue.getProxyHeaderValues())
    {
      String aliasName = proxyHeaderValue.getAliasName();
      result.put(aliasName, aProxyValue.getProxyItem(aProxyValue.getColumnNumberByAliasName(aliasName)).getAsObject());
    }
    return result;
  }


  /**
   * create a response handler for the received response
   *
   * @param  aAgent   the agent that has received the response message
   * @param  aMessage the received response message
   *
   * @return an appropriate handler to handle the reponse or null if it cannot be determined.
   */
  private AbstractResponseHandler createResponseHandler(PDMControllerAgent aAgent, ACLMessage aMessage)
  {
    try
    {
      Action action = (Action) aAgent.getContentManager().extractContent(aMessage);
      Concept concept = action.getAction();

      if (concept instanceof DeleteResponse)
      {
        return new DeleteResponseHandler(aAgent, aMessage);
      }
      else if (concept instanceof ExportResponse)
      {
        return new ExportResponseHandler(aAgent, aMessage);
      }
      else if (concept instanceof ImportResponse)
      {
        return new ImportResponseHandler(aAgent, aMessage);
      }
      else if (concept instanceof AnalyzeResponse)
      {
        AnalyzeResponse realResponse = ((AnalyzeResponse) concept);
        switch (realResponse.getDumpInformation().getFileType())
        {
          case DATAPUMP :
            return new AnalyzeOracleDatapumpResponseHandler(aAgent, aMessage);
          case DATAPUMP_AND_EXP :
            return new AnalyzeOracleDatapumpResponseHandler(aAgent, aMessage);
          case MSSQL_DUMP :
            return new AnalyzeMSSQLResponseHandler(aAgent, aMessage);
          case ORA_EXP :
            return new AnalyzeOracleOldStyleResponseHandler(aAgent, aMessage);
          default :
            return null;
        }
      }
      else
      {
        return null;
      }
    }
    catch (CodecException | OntologyException ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to create a Response Handler. See stacktrace for more infomration.", ex);
      }
      return null;
    }
    catch (PnErrorListException | AuthorizationException ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to create a Response Handler. See stacktrace for more infomration.", ex);
      }
      return null;
    }
  }


  /**
   * search for a database server which can handle databasetype and file type on given location
   *
   * @param  aDatabaseOwnerName Database owner name
   * @param  aFileType          filetype to search server for
   * @param  aDatabaseType      database type to search server for
   * @param  aPkLocation        location to search server for
   * @param  aLog               optional, will contain detailed information about search process ,
   *                            can be null
   *
   * @return DatabaseServerInfo, or null if failed
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  private DatabaseServerInfo getDatabaseServerInfo(String aDatabaseOwnerName, PDMFileType aFileType, PDMDatabaseType aDatabaseType, IBaseValue aPkLocation, StringBuilder aLog) throws PnErrorListException, AuthorizationException
  {
    assert aDatabaseType != null : "databaseType null not allowed";
    assert aFileType != null : "databaseFileType null not allowed";

    // get the database regarding to the dbtype. take the server with the least related database

    String subType = aDatabaseType.equals(PDMDatabaseType.ORACLE) ? BOTypePDMP5Module.PDM_DATABASE_VERSION_ORACLE.getPnName() : BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL.getPnName();
    // query get max version number and related databases. get the highest version with the least database

    ISearchCriteria scDatabaseServer = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDM_SEARCH_FOR_HIGHEST_DB_SERVER_VERSION);
    scDatabaseServer.getOperator(BOSystemBODefinitionDef.PN_PNNAME, SCOperatorType.EQUAL).addValue(subType);
    scDatabaseServer.getOperator(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF, SCOperatorType.EQUAL).addBaseValue(aPkLocation);
    ISCOperator opFileTypeCode = scDatabaseServer.getOperator(IBOPDMFileTypeDef.PN_CODE, SCOperatorType.IN);
    // add DandO when its a oracle database.
    if (PDMDatabaseType.ORACLE.equals(aDatabaseType))
    {
      opFileTypeCode.addValue(PDMFileType.DATAPUMP_AND_EXP.getCode());
    }
    opFileTypeCode.addValue(aFileType.getCode());

    LoginController loginController = LoginController.getInstance();
    loginController.login(aDatabaseOwnerName);
    IProxyListValue plvDatabaseCount = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scDatabaseServer);
    loginController.login();

    return PDMAgentUtilities.getDatabaseServerInfo(plvDatabaseCount, this.ownerAgent, aDatabaseType, aLog);
  }


  /**
   * retrieve the datafile locations of a pdm database server via a proxy view and put them in a
   * list
   *
   * @param  aPkPDMDbServer primary key of PDM database server to get the file locations from
   *
   * @return datafile locations, can be empty list on failure
   *
   * @throws PnErrorListException
   */
  private List getDataFileLocationsByPrimaryKey(IBaseValue aPkPDMDbServer) throws PnErrorListException
  {
    List dataFileLocations = new ArrayList(); // this is not a java.util.List because there is \\
                                              // no ontology for that (also no generics here)

    ISearchCriteria scDataFileLocations = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.DATAFILELOCATIONS_OF_DBSERVER);
    scDataFileLocations.getOperator(DatafileLocationsOfDBServerViewDefinition.PN_DATABASESERVER_REF, SCOperatorType.EQUAL).addBaseValue(aPkPDMDbServer);
    IProxyListValue plvDataFileLocations = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scDataFileLocations);

    int columnIndexLocation = plvDataFileLocations.getColumnNumberByAliasName(IBOPDMDBServerDataFileLocationDef.PN_FILE_LOCATION_LOCAL);

    for (IProxyValue item : plvDataFileLocations.getProxyValues())
    {
      DatafileLocation datafileLocation = new DatafileLocation();
      datafileLocation.setLocation(item.getProxyItem(columnIndexLocation).getAsString());
      dataFileLocations.add(datafileLocation);
    }
    return dataFileLocations;
  }


  /**
   * get database server information out of database
   *
   * @param  aPkPDMDbServer primary key of pdm database server of which the information must be
   *                        retrieved
   * @param  aMessages      will contain error messages if applicable
   *
   * @return information of the selected oracle database server, or null on error
   */
  private MSSQLDatabaseServerInformation getMSSQLDatabaseServerInformation(IBaseValue aPkPDMDbServer, StringBuilder aMessages)
  {
    try
    {
      IFOValue boPDMDatabaseServer = PDMUtilities.getInstance().getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE_SERVER_MSSQL, aPkPDMDbServer);

      String cryptedPassword = boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS).getAsString();

      MSSQLDatabaseServerInformation databaseServerInformation = new MSSQLDatabaseServerInformation();
      databaseServerInformation.setSupportedVersions(getSupportedVersions(boPDMDatabaseServer));
      databaseServerInformation.setPrimaryKeyServer(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY).getAsInteger());
      databaseServerInformation.setAdminUser(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_USER).getAsString());
      databaseServerInformation.setAdminPassword(new nl.planon.hades.authentication.PasswordHasher().decryptString(cryptedPassword));
      databaseServerInformation.setHostName(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_HOST_NAME).getAsString());
      databaseServerInformation.setCodePage(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_CODE_PAGE__NOT_IN_ALL_SUBTYPES).getAsString());
      databaseServerInformation.setDatabaseInstanceName(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASE_INSTANCENAME).getAsString());
      databaseServerInformation.setSortOrder(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_SORT_ORDER__NOT_IN_ALL_SUBTYPES).getAsString());
      databaseServerInformation.setDataFileLocations(getDataFileLocationsByPrimaryKey(aPkPDMDbServer));

      // get pk db version, get databaseVersion BO and ask for version number.
      IBaseValue pkVersionNumber = boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASEVERSION_REF).getBaseValue();
      IFOValue boPDMDatabaseVersion = PDMUtilities.getInstance().getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE_VERSION_MSSQL, pkVersionNumber);
      String stringVersionNumber = boPDMDatabaseVersion.getFieldByPnName(IBOBasePDMDatabaseVersionDef.PN_CODE).getAsString();
      databaseServerInformation.setVersionNumber(stringVersionNumber);
      return databaseServerInformation;
    }
    catch (AuthorizationException ex)
    {
      logStackTrace(ex, aMessages);
    }
    catch (PnErrorListException ex)
    {
      logStackTrace(ex, aMessages);
    }
    return null;
  }


  /**
   * get database server information out of database
   *
   * @param  aPkPDMDbServer primary key of pdm database server of which the information must be
   *                        retrieved
   * @param  aMessages      will contain error messages if applicable
   *
   * @return information of the selected oracle database server, or null on error
   */
  private OracleDatabaseServerInformation getOracleDatabaseServerInformation(IBaseValue aPkPDMDbServer, StringBuilder aMessages)
  {
    try
    {
      IFOValue boPDMDatabaseServer = PDMUtilities.getInstance().getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE, aPkPDMDbServer);

      String cryptedPassword = boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_PASS).getAsString();

      OracleDatabaseServerInformation databaseServerInformation = new OracleDatabaseServerInformation();
      databaseServerInformation.setSupportedVersions(getSupportedVersions(boPDMDatabaseServer));
      databaseServerInformation.setPrimaryKeyServer(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY).getAsInteger());
      databaseServerInformation.setAdminUser(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_ADMIN_USER).getAsString());
      databaseServerInformation.setAdminPassword(new nl.planon.hades.authentication.PasswordHasher().decryptString(cryptedPassword));
      databaseServerInformation.setHostName(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_HOST_NAME).getAsString());
      databaseServerInformation.setPortNumber(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES).getAsInteger());
      databaseServerInformation.setDatabaseInstanceName(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_DATABASE_INSTANCENAME).getAsString());
      databaseServerInformation.setTemporaryTableSpaceName(boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_TEMPORARY_TABLESPACENAME__NOT_IN_ALL_SUBTYPES).getAsString());
      databaseServerInformation.setDataFileLocations(getDataFileLocationsByPrimaryKey(aPkPDMDbServer));
      // PCIS 224906: location of import and export executable is only applicable for Oracle Old Style and therefore not always mandatory.
      IFOFieldValue impLocation = boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_IMPORT_LOCATION__NOT_IN_ALL_SUBTYPES);
      IFOFieldValue expLocation = boPDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_EXPORT_LOCATION__NOT_IN_ALL_SUBTYPES);
      if (!impLocation.isEmpty())
      {
        databaseServerInformation.setImpLocation(impLocation.getAsString());
      }
      if (!expLocation.isEmpty())
      {
        databaseServerInformation.setExpLocation(expLocation.getAsString());
      }

      return databaseServerInformation;
    }
    catch (AuthorizationException ex)
    {
      logStackTrace(ex, aMessages);
    }
    catch (PnErrorListException ex)
    {
      logStackTrace(ex, aMessages);
    }
    return null;
  }


  /**
   * Get the previous database state
   *
   * @param  aPDMRequestType PDMRequestType
   *
   * @return String State
   */
  private PDMDatabaseState getPreviousDatabaseState(PDMRequestType aPDMRequestType)
  {
    return aPDMRequestType.getPreviousDatabaseState();
  }


  /**
   * DOCUMENT ME!
   *
   * @param  pdmTaskType   DOCUMENT ME!
   * @param  fileType      DOCUMENT ME!
   * @param  msgPDMRequest DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  private AbstractPDMAchieveREInitiator getRequestInitiatorForTaskTypeAndFileType(PDMTaskType pdmTaskType, PDMFileType fileType, ACLMessage msgPDMRequest)
  {
    AbstractPDMAchieveREInitiator requestInitiator;
    switch (pdmTaskType)
    {
      case IMPORT_ANALYZE :
        switch (fileType)
        {
          case MSSQL_DUMP :
            requestInitiator = new AnalyzeMSSQLRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case DATAPUMP :
            requestInitiator = new AnalyzeOracleDatapumpRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case ORA_EXP :
            requestInitiator = new AnalyzeOracleOldStyleRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          default :
            requestInitiator = null;
            break;
        }
        break;
      case IMPORT :
        switch (fileType)
        {
          case MSSQL_DUMP :
            requestInitiator = new MSSQLImportRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case DATAPUMP :
            requestInitiator = new OracleDataPumpImportRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case ORA_EXP :
            requestInitiator = new OracleOldStyleImportRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          default :
            requestInitiator = null;
            break;
        }
        break;
      case EXPORT :
        switch (fileType)
        {
          case MSSQL_DUMP :
            requestInitiator = new MSSQLExportRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case DATAPUMP :
            requestInitiator = new OracleDataPumpExportRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case ORA_EXP :
            requestInitiator = new OracleOldStyleExportRequestinitiator(this.ownerAgent, msgPDMRequest);
            break;
          default :
            requestInitiator = null;
            break;
        }
        break;
      case DELETE :
        switch (fileType)
        {
          case MSSQL_DUMP :
            requestInitiator = new MSSQLDeleteRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case DATAPUMP :
            requestInitiator = new OracleDeleteRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          case ORA_EXP :
            requestInitiator = new OracleDeleteRequestInitiator(this.ownerAgent, msgPDMRequest);
            break;
          default :
            requestInitiator = null;
            break;
        }
        break;
      default :
        requestInitiator = null;
        break;
    }
    return requestInitiator;
  }


  /**
   * get a list of the supported versions of a database server
   *
   * @param  aIFOValuePDMDatabaseServer IFOValue
   *
   * @return List of supported versions
   *
   * @throws PnErrorListException
   * @throws AuthorizationException FieldNotFoundException
   */
  private List getSupportedVersions(IFOValue aIFOValuePDMDatabaseServer) throws PnErrorListException, AuthorizationException
  {
    List list = new ArrayList();

    Integer pkDBServer = aIFOValuePDMDatabaseServer.getFieldByPnName(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY).getAsInteger();
    ISearchCriteria scAnalyze = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDM_SUPPORTED_VERSIONS_FROM_ONE_SERVER);
    scAnalyze.getOperatorEx(IBOBasePDMDatabaseServerDef.PN_PRIMARYKEY, SCOperatorType.EQUAL).addValue(pkDBServer);
    IProxyListValue plvAnalyze = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scAnalyze);
    for (IProxyValue view : plvAnalyze.getProxyValues())
    {
      list.add(view.getProxyItem(0).getAsString());
    }
    return list;
  }


  /**
   * gets translated message for specified error code
   *
   * @param  aMessageType MessageType to translate
   * @param  aArgs        arguments of message type
   *
   * @return String
   */
  private String getTranslatedMessage(IMessageType aMessageType, Object... aArgs)
  {
    //  String va = Translator.getString(aMessageType.getLocalizebleKey());

    return getTranslatedMessage(aMessageType.getLocalizebleKey(), aArgs);
  }


  /**
   * gets translated message
   *
   * @param  aTransKey key of message to be translated
   * @param  aArgs     arguments of message type
   *
   * @return String
   */
  private String getTranslatedMessage(String aTransKey, Object... aArgs)
  {
    ITranslator translator = SessionHades.get().getTranslator();
    String formatMessage = translator.getString(aTransKey);
    String result = MessageFormat.format(formatMessage, aArgs);
    return result;
  }


  /**
   * handle exception that occurred when retrieving and issuing tasks.<br>
   * - report message in request log<br>
   * - put database, request and task in corresponding state
   *
   * @param aExceptionText text of exception to report in request log
   * @param aDatabaseState aBaseStatusDatabase aRequest       request where the error occurred;
   *                       determines next state of pdm database
   * @param aPkPDMTask     primary key of pdm task for performing state change
   * @param aPkPDMRequest  primary key of pdm request for performing state change
   * @param aPkPDMDatabase primary key of pdm database for performing state change
   */
  private void handleException(String aExceptionText, PDMDatabaseState aDatabaseState, IBaseValue aPkPDMTask, IBaseValue aPkPDMRequest, IBaseValue aPkPDMDatabase)
  {
    handleException(new StringBuilder(aExceptionText), aDatabaseState, aPkPDMTask, aPkPDMRequest, aPkPDMDatabase);
  }


  /**
   * handle exception that occurred when retrieving and issuing tasks.<br>
   * - report message in request log<br>
   * - put database, request and task in corresponding state
   *
   * @param aExceptionText text of exception to report in request log
   * @param aDatabaseState aBaseStatusDatabase aRequest       request where the error occurred;
   *                       determines next state of pdm database
   * @param aPkPDMTask     primary key of pdm task for performing state change
   * @param aPkPDMRequest  primary key of pdm request for performing state change
   * @param aPkPDMDatabase primary key of pdm database for performing state change
   */
  private void handleException(StringBuilder aExceptionText, PDMDatabaseState aDatabaseState, IBaseValue aPkPDMTask, IBaseValue aPkPDMRequest, IBaseValue aPkPDMDatabase)
  {
    int numberOfRetriesOnCacheRefresh = 2;
    boolean loggingDone = false;
    for (int i = 0; i < numberOfRetriesOnCacheRefresh; i++)
    {
      Exception e;
      try
      {
        if (!loggingDone)
        {
          // Log the failure message to the event logging of the PDM Request
          PDMControllerAgent.addLogForPDMRequest(aPkPDMRequest, aExceptionText, EventLogTypeSeverity.ERROR, null);
          loggingDone = true;
        }

        PDMControllerAgent.setNextStatePDMBOs( //
          aPkPDMTask, PDMTaskState.NOT_OK, //
          aPkPDMRequest, PDMRequestState.NOT_OK, //
          aPkPDMDatabase, ((aDatabaseState == null) ? PDMDatabaseState.INITIAL : aDatabaseState));
        break;
      }
      catch (AuthorizationException ex)
      {
        e = ex;
      }
      catch (PnErrorListException ex)
      {
        if (ex.getErrorHandler().hasCacheUpdateCountError())
        {
          // PCIS 189377 and PCIS 197198: in case the metadata is updated, the cache has to be refreshed.
          if (LOG.isErrorEnabled())
          {
            LOG.error("One or more business objects are being configured. The cache will be refreshed!", ex);
          }
          CacheManager.getInstance().clearAllInstances();
          if (LOG.isErrorEnabled())
          {
            LOG.error("Going to retry....");
          }
          continue;
        }
        e = ex;
      }
      if (LOG.isErrorEnabled())
      {
        StringBuilder builder = new StringBuilder();
        if (!loggingDone)
        {
          builder.append("failed to write following logging to request log:");
          builder.append(aExceptionText);
          builder.append("\n");
        }
        builder.append("Failed to set states to PDMTaskState.NOT_OK, PDMRequestState.NOT_OK");
        LOG.error(builder.toString(), e);
      }
      break;
    }
  }


  /**
   * If translations weren't loaded, here we try to load by calling FrameworkTranslator #
   * getFormattingUtilities().
   *
   * <p>Refer: TEC-2925</p>
   *
   * @see nl.planon.hades.client.localize.FrameworkTranslator.getFormattingUtilities()
   */
  private void loadTranslations()
  {
    try
    {
      SessionHades.get().getFormattingUtilities();
    }
    catch (PnErrorListException ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to load the translations. See stacktrace for more information.", ex);
      }
    }
  }


  /**
   * log stacktrace in string builder
   *
   * @param aEx       ex the exception to log
   * @param aMessages exception will be added to this builder
   */
  private void logStackTrace(Exception aEx, StringBuilder aMessages)
  {
    final Writer result = new StringWriter();
    final PrintWriter printWriter = new PrintWriter(result);
    aEx.printStackTrace(printWriter);

    aMessages.append(printWriter.toString());
  }


  /**
   * this method gets all pdm tasks that are in initial state, loops through them and dispatches it
   * to the correct agent, according to the kind of task, to execute it. It the sets the status of
   * this task to InProgress.
   */
  private void retrievePDMTasks()
  {
    if (LOG.isInfoEnabled())
    {
      LOG.info("Executing the RetrieveTasks method");
    }

    try
    {
      ISearchCriteria scPDMTasks = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMTASKS_IN_INITIAL_STATE);
      IProxyListValue plvPDMTasks = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scPDMTasks);

      if (!plvPDMTasks.isEmpty())
      {
        int ixPkPDMTask = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PK_PDMTASK);
        int ixPkPDMRequest = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PK_PDMREQUEST);
        int ixPkPDMDatabase = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PK_PDMDATABASE);
        int ixTaskSubType = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PDMTASK_SUBTYPE);
        int ixDbServerCode = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PDMDBSERVER_CODE);
        int ixLocation = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PK_PDMDATABASE_LOCATION);
        int ixExportFileTypeCode = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_EXPORTFILETYPE_CODE);
        int ixFileTypeCode = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PDMFILETYPE_CODE);
        int ixRequestTypeCode = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_REQUESTTYPE_CODE);
        int ixDatabaseTypeCode = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PDMDBTYPE_CODE);
        int ixPkDbServer = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PK_PDMDATABASESERVER);
        int ixDatabaseOwnerName = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_DATABASE_OWNER_NAME);
        int ixExportDatabase = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PDMDATABASE_PDMEXPORTFILE);
        int ixPrivilegeType = plvPDMTasks.getColumnNumberByAliasName(IPDMTasksInInitialStateViewDefinition.ALIAS_PDMDATABASE_PRIVILEGE_TYPE);

        for (IProxyValue item : plvPDMTasks.getProxyValues())
        {
          IBaseValue pkPDMTask = item.getProxyItem(ixPkPDMTask).getBaseValue();
          IBaseValue pkPDMRequest = item.getProxyItem(ixPkPDMRequest).getBaseValue();
          IBaseValue pkPDMDatabase = item.getProxyItem(ixPkPDMDatabase).getBaseValue();
          PDMRequestType pdmRequestType = PDMRequestType.getPDMRequestType(item.getProxyItem(ixRequestTypeCode).getAsString());

          IBaseValue pkPDMDatabaseServer = item.getProxyItem(ixPkDbServer).getBaseValue();
          PDMTaskType pdmTaskType = PDMTaskType.getPDMTaskType(item.getProxyItem(ixTaskSubType).getAsString());
          // get code of database server and database type filled in by user, in order to find the right dbagent to perform the request.

          String databaseOwnerName = item.getProxyItem(ixDatabaseOwnerName).getAsString();
          String databaseServerCode = item.getProxyItem(ixDbServerCode).getAsString();
          PDMDatabaseType databaseType = PDMDatabaseType.getPDMDatabaseTypeByCode(item.getProxyItem(ixDatabaseTypeCode).getAsString());
          PDMFileType fileType = PDMFileType.getPDMFileTypeByCode(item.getProxyItem(ixFileTypeCode).getAsString());

          PDMPrivilegeType privilegeType = PDMPrivilegeType.getPDMPrivilegeTypeByCode(item.getProxyItem(ixPrivilegeType).getAsString());

          IBaseValue pkDBServerLocation = item.getProxyItem(ixLocation).getBaseValue();
          // if no Exportfiletype is filled in, then importfiletype is the default.
          IProxyItem piExportFileType = item.getProxyItem(ixExportFileTypeCode);
          PDMFileType exportFileType = (piExportFileType.isEmpty() ? fileType : PDMFileType.getPDMFileTypeByCode(piExportFileType.getAsString()));

          PDMControllerAgent.setNextStatePDMBOs( //
            pkPDMTask, PDMTaskState.IN_PROGRESS, //
            pkPDMRequest, PDMRequestState.IN_PROGRESS, //
            null, null);
          try
          {
            // if its a export task, get the filetype from exportfile Field
            if (PDMTaskType.EXPORT.equals(pdmTaskType))
            {
              fileType = exportFileType;
            }
            if (PDMRequestType.RELOAD_EXPORT.equals(pdmRequestType))
            {
              // fix file type to used export file type (export file type might not match with exported db)
              String exportDatabase = item.getProxyItem(ixExportDatabase).getAsString();
              if (exportDatabase != null)
              {
                if (exportDatabase.endsWith(EXTENSION_DPD))
                {
                  fileType = PDMFileType.DATAPUMP;
                }
                else if (exportDatabase.endsWith(EXTENSION_DMP))
                {
                  fileType = PDMFileType.ORA_EXP;
                }
                else if (exportDatabase.endsWith(EXTENSION_BAK))
                {
                  fileType = PDMFileType.MSSQL_DUMP;
                }
              }
            }

            // if filetype isn't filled in. look to the extension and search for a database server with the highest version
            // if only databaseType is not filled derive databaseType from specified fileType
            if ((fileType == null) || (databaseType == null))
            {
              FileTypeDatabaseType fileTypeDatabaseType = getDBTypeAndFileType(pkPDMDatabase, fileType, databaseType, pkPDMRequest);
              if (fileTypeDatabaseType == null) // all possible filetypes are already tried: DO NOT create a new analyze task!
              {
                handleException(new StringBuilder("all possible file types are already tried"), //
                  getPreviousDatabaseState(pdmRequestType), pkPDMTask, pkPDMRequest, pkPDMDatabase);
                continue;
              }
              fileType = fileTypeDatabaseType.getFileType();
              databaseType = fileTypeDatabaseType.getDatabaseType();
            }

            if (databaseServerCode == null)
            {
              StringBuilder logInfo = new StringBuilder();
              DatabaseServerInfo info = getDatabaseServerInfo(databaseOwnerName, fileType, databaseType, pkDBServerLocation, logInfo);
              if (info == null)
              {
                StringBuilder msg = new StringBuilder("Analyze step did not finish because no database server is configured to handle Filetype ").append(fileType).append(" ");
                msg.append("The configured server(s) is not reachable.\n\n");
                msg.append("Details:\n").append(logInfo);
                handleException(msg, //
                  getPreviousDatabaseState(pdmRequestType), pkPDMTask, pkPDMRequest, pkPDMDatabase);
                continue;
              }
              else
              {
                // no logging content of logInfo will  be logged by following analysis task
              }
              databaseServerCode = info.getCode();
              pkPDMDatabaseServer = info.getPrimarykey();
            }

            // get an agent to fullfill the task
            AID pdmAgent = this.ownerAgent.getAgentToExecuteTask(databaseServerCode, fileType, databaseType);

            // if no agent found, request/task failed
            if (pdmAgent == null)
            {
              handleException(new StringBuilder(getTranslatedMessage(ErrorNumberPDMP5Module.EC_NO_PDMAGENT_FOUND)), //
                getPreviousDatabaseState(pdmRequestType), pkPDMTask, pkPDMRequest, pkPDMDatabase);
              continue;
            }

            ACLMessage msgPDMRequest = new ACLMessage(ACLMessage.REQUEST);
            msgPDMRequest.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
            msgPDMRequest.setLanguage(PDMOntology.CODEC.getName());
            msgPDMRequest.setOntology(PDMOntology.ONTOLOGY.getName());
            msgPDMRequest.addUserDefinedParameter(PDMAgentProperties.PARAM_MESSAGE_TYPE, PDMAgentProperties.MESSAGE_PDM_REQUEST);

            AbstractPDMAchieveREInitiator requestInitiator = getRequestInitiatorForTaskTypeAndFileType(pdmTaskType, fileType, msgPDMRequest);
            if ((requestInitiator == null))
            {
              handleException(new StringBuilder(ErrorNumberPDMP5Module.EC_INVALID_REQUEST.getLocalizebleKey()), null, pkPDMTask, pkPDMRequest, pkPDMDatabase);

              if (LOG.isErrorEnabled())
              {
                LOG.error("Failed to create a request initiator for pkPDMTask: '" + pkPDMTask + "', pkPDMRequest: '" + pkPDMRequest + "', pkPDMDatabase: '" + pkPDMDatabase + "'.");
              }

              // go to the next task.
              continue;
            }

            // try to get the correct request according to
            AbstractRequest request = requestInitiator.createRequest();
            request.setPDMRequestType(pdmRequestType);
            request.setPrimaryKeyDatabase(pkPDMDatabase.getAsInteger());
            request.setPrimaryKeyRequest(pkPDMRequest.getAsInteger());
            request.setPrimaryKeyTask(pkPDMTask.getAsInteger());
            request.setPrivilegesType(privilegeType);

            // set the agent that is found to fulfill the task as receiver for the request
            msgPDMRequest.addReceiver(pdmAgent);

            DatabaseServerInformation dbServerInfo = null;
            if (!pkPDMDatabaseServer.isEmpty())
            {
              StringBuilder messages = new StringBuilder();
              // if its MSSQL Database Request, get the MSSQL server information
              if (PDMFileType.MSSQL_DUMP.equals(fileType))
              {
                dbServerInfo = getMSSQLDatabaseServerInformation(pkPDMDatabaseServer, messages);
              }
              else
              {
                // if no filetype is filled in, then Oracle datapump is the default.
                dbServerInfo = getOracleDatabaseServerInformation(pkPDMDatabaseServer, messages);
              }
              if (dbServerInfo == null)
              {
                // a server was specified but not found
                handleException(messages, getPreviousDatabaseState(pdmRequestType), pkPDMTask, pkPDMRequest, pkPDMDatabase);
                continue;
              }
            }

            try
            {
              // PCIS 206720: for the request some values have to be filled and send to the DB Agent. In this request,
              // references to the framework (hades) cannot be included. The proxyvalue can thus not be passed as argument
              // to method filldata (see below). Therefore the fields and their values are added to a map, which is then
              // passed as argument.
              Map<String, Object> mapOfFields = convertProxyValueToMap(item, plvPDMTasks);
              request.fillData(mapOfFields, dbServerInfo);
              this.ownerAgent.executeTaskOnPDMAgent(requestInitiator, pkPDMTask, request, msgPDMRequest, pdmAgent);
            }
            catch (CodecException ex)
            {
              handleException(new StringBuilder(getTranslatedMessage(ErrorNumberPDMP5Module.EC_CODEC_EXCEPTION_IN_REQUEST)).append("\n\n").append(ex.getLocalizedMessage()), //
                request.getBaseStatusDatabase(), pkPDMTask, pkPDMRequest, pkPDMDatabase);
            }
            catch (OntologyException ex)
            {
              handleException(new StringBuilder(getTranslatedMessage(ErrorNumberPDMP5Module.EC_ONTOLOGY_EXCEPTION_IN_REQUEST)).append("\n\n").append(ex.getLocalizedMessage()), //
                request.getBaseStatusDatabase(), pkPDMTask, pkPDMRequest, pkPDMDatabase);
            }
          }
          catch (Throwable ex) //handle asserts/runtime exceptions, etc
          {
            StringBuilder msg = new StringBuilder(getTranslatedMessage(ErrorNumberPDMP5Module.EC_INTERNAL_ERROR));
            msg.append("\n\n").append(getStackTrace(ex));
            handleException(msg, getPreviousDatabaseState(pdmRequestType), pkPDMTask, pkPDMRequest, pkPDMDatabase);
          }
        }
      }
    }
    catch (PnErrorListException ex)
    {
      if (ex.getErrorHandler().hasCacheUpdateCountError())
      {
        // PCIS 189377 and PCIS 197198: in case the metadata is updated, the cache has to be refreshed.
        if (LOG.isErrorEnabled())
        {
          LOG.error("One or more business objects are being configured. The cache will be refreshed!", ex);
        }
        CacheManager.getInstance().clearAllInstances();
      }
      else
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("PnErrorListException occurred!", ex);
        }
      }
    }
    catch (AuthorizationException ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("AuthorizationException occurred!", ex);
      }
    }
  }
}
