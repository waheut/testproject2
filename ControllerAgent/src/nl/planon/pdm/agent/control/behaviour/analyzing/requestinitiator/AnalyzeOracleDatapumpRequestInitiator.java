// Planon Enterprise Edition Source file: AnalyzeOracleDatapumpRequestInitiator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour.analyzing.requestinitiator;

import jade.content.lang.Codec.*;
import jade.content.onto.*;
import jade.core.*;
import jade.lang.acl.*;

import nl.planon.hades.exception.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.agent.control.behaviour.analyzing.responsehandler.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;

import nl.planon.util.pnlogging.*;

/**
 * AnalyzeOracleDatapumpRequestInitiator
 */
public class AnalyzeOracleDatapumpRequestInitiator extends AbstractPDMAchieveREInitiator
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(AnalyzeOracleDatapumpRequestInitiator.class, PnLogCategory.DEFAULT);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new AnalyzeOracleDatapumpRequestInitiator object.
   *
   * @param aAgent      agent to which the behavior belongs
   * @param aACLMessage request message to be sent
   */
  public AnalyzeOracleDatapumpRequestInitiator(Agent aAgent, ACLMessage aACLMessage)
  {
    super(aAgent, aACLMessage);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public AbstractRequest createRequest()
  {
    return new OracleDataPumpAnalyzeRequest();
  }


  /**
   * {@inheritDoc}
   */
  @Override protected AbstractResponseHandler createResponseHandler(PDMControllerAgent aAgent, ACLMessage aResponseMessage) throws UngroundedException, CodecException, OntologyException, PnErrorListException, AuthorizationException
  {
    return new AnalyzeOracleDatapumpResponseHandler(aAgent, aResponseMessage);
  }
}
