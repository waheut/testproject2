// Planon Enterprise Edition Source file: DatabaseServerInfo.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.control.behaviour;

import nl.planon.hades.basevalue.*;

/**
 * contains information about DatabaseServer like primary key and code
 */
public class DatabaseServerInfo
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private IBaseValue pk;
  private String code;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DatabaseServerInfo object.
   *
   * @param aPrimarykey pk
   * @param aCode       code
   */
  public DatabaseServerInfo(IBaseValue aPrimarykey, String aCode)
  {
    this.pk = aPrimarykey;
    this.code = aCode;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets code of database server
   *
   * @return String
   */
  public String getCode()
  {
    return this.code;
  }


  /**
   * gets primary key
   *
   * @return IBaseValue
   */
  public IBaseValue getPrimarykey()
  {
    return this.pk;
  }
}
