// Planon Enterprise Edition Source file: PDMLogin.java
// Copyright Planon 1997-2014. All Rights Reserved.
package nl.planon.pdm.agent.control;

import javax.security.auth.login.*;

import nl.planon.hades.client.authentication.*;

/**
 * PDMLogin
 */
public class PDMLogin extends BaseServiceLogin
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // misusing Exchange configuration to save a lot of configuration work (also for PDM LE)
  public static final String PDM_CLIENT_JAAS_CONFIG = "ExchangeClientLoginConfiguration";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMLogin object.
   *
   * @throws LoginException
   */
  public PDMLogin() throws LoginException
  {
    super(PDM_CLIENT_JAAS_CONFIG);
  }
}
