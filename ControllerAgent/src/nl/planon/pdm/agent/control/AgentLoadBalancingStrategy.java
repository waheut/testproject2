// Planon Enterprise Edition Source file: AgentLoadBalancingStrategy.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.control;

import jade.core.*;

import nl.planon.hades.basevalue.*;

import nl.planon.pdm.agent.control.exceptions.*;
import nl.planon.pdm.core.common.*;

/**
 * Abstract class to provide with an interface for implementing a load balancing strategy.
 */
public interface AgentLoadBalancingStrategy
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Is called when a task is assigned to an agent.
   *
   * @param  aPkPDMTask the task
   * @param  aPdmAgent  the agent
   *
   * @throws LoadBalancingException When the implementation fails.
   */
  public void assignTaskToAgent(IBaseValue aPkPDMTask, AID aPdmAgent) throws LoadBalancingException;


  /**
   * This method is called to determine the best matching agent.
   *
   * @param  aDatabaseServerCode the database server code which the PDMAgent has to support.
   * @param  aFileType           the file type which the PDMAgent has to support.
   * @param  aDatabaseType       the database type code which the PDMAgent has to support.
   *
   * @return The best PDM Agent to use when handling a PDMRequest for the given parameters.
   *
   * @throws LoadBalancingException When the implementation fails.
   */
  public AID getAgentToExecuteTask(String aDatabaseServerCode, PDMFileType aFileType, PDMDatabaseType aDatabaseType) throws LoadBalancingException;


  /**
   * Is called when a task finishes on an agent (whether or not succesful does not matter).
   *
   * @param  aPkTask   the task
   * @param  aPdmAgent the agent.
   *
   * @throws LoadBalancingException When the implementation fails.
   */
  public void unassignTaskToAgent(IBaseValue aPkTask, AID aPdmAgent) throws LoadBalancingException;
}
