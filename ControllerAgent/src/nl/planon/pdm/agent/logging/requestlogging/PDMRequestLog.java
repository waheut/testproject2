// Planon Enterprise Edition Source file: PDMRequestLog.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.logging.requestlogging;

import nl.planon.hades.beans.*;
import nl.planon.hades.businessmodel.eventlog.*;
import nl.planon.hades.exception.*;

/**
 * Object that contains the information to log of a PDM Request
 */
public class PDMRequestLog extends BaseLogObject
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestLog object.
   *
   * @param  aLogMessage message to log
   * @param  aPnContext
   * @param  aArguments  arguments to message
   *
   * @throws PnErrorListException
   */
  public PDMRequestLog(String aLogMessage, PnContext aPnContext, Object... aArguments) throws PnErrorListException
  {
    super(aLogMessage, aPnContext, aArguments);
  }


  /**
   * Creates a new PDMRequestLog object.
   *
   * @param  aLogType    severity of log message (error, warning or information)
   * @param  aLogMessage message to log
   * @param  aPnContext
   * @param  aArguments  arguments to message
   *
   * @throws PnErrorListException
   */
  public PDMRequestLog(EventLogTypeSeverity aLogType, String aLogMessage, PnContext aPnContext, Object[] aArguments) throws PnErrorListException
  {
    super(aLogType, aLogMessage, aPnContext, aArguments);
  }


  /**
   * Creates a new PDMRequestLog object.
   *
   * @param  aLogMessage message to log
   * @param  aTranslate  if the data is coming then no need to translate, this parameter will define
   *                     if we need to translate this message or not
   * @param  aPnContext
   * @param  aArguments  arguments to message
   *
   * @throws PnErrorListException
   */
  public PDMRequestLog(String aLogMessage, boolean aTranslate, PnContext aPnContext, Object... aArguments) throws PnErrorListException
  {
    super(aLogMessage, aTranslate, aPnContext, aArguments);
  }


  /**
   * Creates a new PDMRequestLog object.
   *
   * @param  aLogMessage message to log
   * @param  aException  (can be null, if an error needs to be logged and not an exception)
   * @param  aPnContext
   * @param  aArguments  arguments to message
   *
   * @throws PnErrorListException
   */
  public PDMRequestLog(String aLogMessage, Throwable aException, PnContext aPnContext, Object... aArguments) throws PnErrorListException
  {
    super(aLogMessage, aException, aPnContext, aArguments);
  }


  /**
   * Creates a new PDMRequestLog object.
   *
   * @param  aLogType    severity of log message (error, warning or information)
   * @param  aLogMessage message to log
   * @param  aTranslate  boolean, true if message must/can be translated
   * @param  aPnContext
   * @param  aArguments  arguments to message
   *
   * @throws PnErrorListException
   */
  public PDMRequestLog(EventLogTypeSeverity aLogType, String aLogMessage, boolean aTranslate, PnContext aPnContext, Object... aArguments) throws PnErrorListException
  {
    super(aLogType, aLogMessage, aTranslate, aPnContext, aArguments);
  }
}
