// Planon Enterprise Edition Source file: PDMAgentUtilities.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent;

import jade.core.*;
import jade.domain.FIPAAgentManagement.*;

import java.sql.*;
import java.util.*;

import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabase.proxyview.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmfiletype.*;

import nl.planon.pdm.agent.control.behaviour.*;
import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;

import nl.planon.util.pnlogging.*;

/**
 * Utilities to be used on PDMAgents
 */
public class PDMAgentUtilities
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(PDMAgentUtilities.class, PnLogCategory.DEFAULT);

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets list of analyzed file types
   *
   * @param  aPkRequest request to get file types for
   *
   * @return list of analyzed file types for specified request
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  public static List<PDMFileType> getAnalyzedFileType(Integer aPkRequest) throws AuthorizationException, PnErrorListException
  {
    ISearchCriteria scAnalyze = SearchCriteriaFactoryResourceLocator.getInstance().getPVSearchCriteria(PVTypePDMP5Module.PDMANALYZE_BY_PKREQUEST);
    scAnalyze.getOperatorEx(IBOPDMAnalyzeDef.PN_PDMREQUEST_REF, SCOperatorType.EQUAL).addValue(aPkRequest);
    IProxyListValue plvAnalyze = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scAnalyze);
    int size = plvAnalyze.getProxyValues().size();
    int ixFileType = plvAnalyze.getColumnNumberByAliasName(BOPDMFileTypeDef.PN_CODE);

    List<PDMFileType> types = new ArrayList<PDMFileType>();
    for (IProxyValue pv : plvAnalyze.getProxyValues())
    {
      types.add(PDMFileType.getPDMFileTypeByCode(pv.getProxyItem(ixFileType).getAsString()));
    }
    return types;
  }


  /**
   * gets all available agents Descriptions
   *
   * @param  aAgent              DOCUMENT ME!
   * @param  aDatabaseServerCode
   * @param  aFileType
   * @param  aDatabaseType
   *
   * @return array of DFAgentDescription
   */
  public static DFAgentDescription[] getAvailableAgents(Agent aAgent, String aDatabaseServerCode, PDMFileType aFileType, PDMDatabaseType aDatabaseType)
  {
    String[][] aPropertyValues =
    {
      // format = {Property name, property value}
      {PDMAgentProperties.PROP_DB_SERVER, aDatabaseServerCode},
      {PDMAgentProperties.PROP_DB_TYPE, aDatabaseType.getCode()},
      {PDMAgentProperties.PROP_FILE_TYPE, aFileType.getCode()}
    };
    ServiceDescription serviceDescription = JadeUtilities.fillServiceDescription(PDMAgentType.DATABASE_AGENT.getCode(), null, aPropertyValues);
    DFAgentDescription[] agentsFound = JadeUtilities.findAgents(aAgent, serviceDescription);
    return agentsFound;
  }


  /**
   * gets server information for specified aginet and db type.
   *
   * @param  aPlv
   * @param  aAgent
   * @param  aDatabaseType
   * @param  aInfoLog
   *
   * @return DatabaseServerInfo
   */
  public static DatabaseServerInfo getDatabaseServerInfo(IProxyListValue aPlv, Agent aAgent, PDMDatabaseType aDatabaseType, StringBuilder aInfoLog)
  {
    java.util.List<String> triedServer = new java.util.ArrayList<String>();

    if (!aPlv.isEmpty())
    {
      int ixAdminUser = aPlv.getColumnNumberByAliasName(IPDMSearchDatabaseServer.ALIAS_ADMIN_USER);
      int ixAdminPass = aPlv.getColumnNumberByAliasName(IPDMSearchDatabaseServer.ALIAS_ADMIN_PASSWORD);
      int ixFileTypeCode = aPlv.getColumnNumberByAliasName(IPDMSearchDatabaseServer.ALIAS_FILE_TYPE_CODE);
      int ixServerCode = aPlv.getColumnNumberByAliasName(IPDMSearchDatabaseServer.ALIAS_SERVER_CODE);
      int ixVersionNumber = aPlv.getColumnNumberByAliasName(IPDMSearchDatabaseServer.ALIAS_VERSION_NUMBER);
      int ixConnectionURL = aPlv.getColumnNumberByAliasName(IPDMSearchDatabaseServer.ALIAS_CONNECTION_URL);
      int ixPk = aPlv.getColumnNumberByAliasName(IPDMSearchDatabaseServer.ALIAS_PRIMARYKEY); // because of aggregate
      for (IProxyValue item : aPlv.getProxyValues())
      {
        String adminUser = item.getProxyItem(ixAdminUser).getAsString();
        String adminPass = item.getProxyItem(ixAdminPass).getAsString();
        String decryptPass = new nl.planon.hades.authentication.PasswordHasher().decryptString(adminPass);
        String connectionURL = item.getProxyItem(ixConnectionURL).getAsString();
        PDMFileType fileType = PDMFileType.getPDMFileTypeByCode(item.getProxyItem(ixFileTypeCode).getAsString());
        // test first the connection to the database
        StringBuilder validInfo = new StringBuilder();
        boolean skip = !isValidDatabaseConnection(aDatabaseType, connectionURL, adminUser, decryptPass, validInfo);
        String serverCode = item.getProxyItem(ixServerCode).getAsString();
        String version = item.getProxyItem(ixVersionNumber).getAsString();
        triedServer.add(serverCode);
        if (skip)
        {
          aInfoLog.append("skipping server ").append(serverCode).append(", version ").append(version).append(", connection URL ").append(connectionURL).append(" because db connection is not available.\n");
          aInfoLog.append(validInfo);
          continue;
        }

        DFAgentDescription[] agentsFound = getAvailableAgents(aAgent, serverCode, fileType, aDatabaseType);
        if (PDMFileType.DATAPUMP_AND_EXP.equals(fileType))
        {
          if (agentsFound.length == 0)
          {
            agentsFound = getAvailableAgents(aAgent, serverCode, PDMFileType.ORA_EXP, aDatabaseType);
          }
          if (agentsFound.length == 0)
          {
            agentsFound = getAvailableAgents(aAgent, serverCode, PDMFileType.DATAPUMP, aDatabaseType);
          }
        }
        if (agentsFound.length == 0)
        {
          aInfoLog.append("Skipping server ").append(serverCode).append(", version ").append(version).append(", because no agents are available.\n");
          continue;
        }

        aInfoLog.append("Server ").append(serverCode).append(", version ").append(version).append(", is available for analysis.\n");
        return new DatabaseServerInfo(item.getProxyItem(ixPk).getBaseValue(), serverCode);
      }
      aInfoLog.append("all configured ").append(aDatabaseType.name()).append(" servers could not be used (tried servers ");
      for (String server : triedServer)
      {
        aInfoLog.append(server).append(", ");
      }
      aInfoLog.append(")\n");
    }
    return null;
  }


  /**
   * checks if specified url is a valid database connection
   *
   * @param  aDatabaseType  database type
   * @param  aConnectionURL Connection URL
   * @param  aUsername      User name
   * @param  aPassword      password
   * @param  aInfoLog       optional, contains additional info about progression
   *
   * @return boolean
   */
  private static boolean isValidDatabaseConnection(PDMDatabaseType aDatabaseType, String aConnectionURL, String aUsername, String aPassword, StringBuilder aInfoLog)
  {
    Properties pty = new java.util.Properties();
    pty.put("user", aUsername);
    pty.put("password", aPassword);
    try
    {
      DriverManager.getConnection(aConnectionURL, pty);
    }
    catch (SQLRecoverableException ex)
    {
      return false;
    }
    // database server maybe not available try a other one.
    catch (SQLException ex)
    {
      if (aInfoLog != null)
      {
        aInfoLog.append("Validation ").append(aDatabaseType.getName()).append(" connection failed [user=" + aUsername + "]: ").append(ex.getMessage()).append("\n");
      }

      if (LOG.isErrorEnabled())
      {
        LOG.error("isValidDatabaseConnection failed for " + aConnectionURL + " [user=" + aUsername + "]: ", ex);
      }
      return false;
    }
    return true;
  }
}
