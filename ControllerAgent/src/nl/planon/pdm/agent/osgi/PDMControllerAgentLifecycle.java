// Planon Enterprise Edition Source file: PDMControllerAgentLifecycle.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.agent.osgi;

import jade.core.*;
import jade.osgi.service.runtime.*;
import jade.wrapper.*;

import org.osgi.framework.*;

import nl.planon.morpheus.pdm.server.osgi.*;

import nl.planon.pdm.agent.control.*;
import nl.planon.pdm.core.*;

import nl.planon.util.pnlogging.*;

/**
 * activates PDMControllerAgent, requires bundle jadeOSGIBundle.jar to be deployed because this
 * activator communicates with it.
 */
public class PDMControllerAgentLifecycle implements IApplicationLifecycleListener
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PDMAgentType AGENT_TYPE = PDMAgentType.CONTROLLER_AGENT;
  private static final java.lang.Class AGENT_CLASS = PDMControllerAgent.class;

  private static final PnLogger LOG = PnLogger.getLogger(PDMControllerAgentLifecycle.class, PnLogCategory.DEFAULT);

  //~ Instance Variables ---------------------------------------------------------------------------

  private ServiceListener serviceListener;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * register agent AGENT_CLASS (if not already registered)
   *
   * @param  aContext BundleContext
   *
   * @throws RuntimeException InvalidSyntaxException
   */
  @Override public void activated(final BundleContext aContext)
  {
    if (LOG.isInfoEnabled())
    {
      LOG.info(getClass().getSimpleName() + ": activated() for agent '" + AGENT_TYPE.getCode() + "'");
    }

    this.serviceListener = new ServiceListener()
    {
      @Override public void serviceChanged(ServiceEvent aEvent)
      {
        ServiceReference sr = aEvent.getServiceReference();
        switch (aEvent.getType())
        {
          case ServiceEvent.REGISTERED :
          {
            if (LOG.isInfoEnabled())
            {
              LOG.info("going to register agent '" + AGENT_TYPE.getCode() + "'");
            }
            registerAgent(aContext, sr);
          }
          break;
          case ServiceEvent.UNREGISTERING :
          {
            if (LOG.isInfoEnabled())
            {
              LOG.info("going to unregister agent '" + AGENT_TYPE.getCode() + "'");
            }
            unregisterAgent(aContext, sr);
          }
          break;
        }
      }
    };

    try
    {
      ServiceReference serverReference = getServiceReference(aContext);
      if (this.serviceListener != null)
      {
        aContext.addServiceListener(this.serviceListener, getJadeRuntimeServiceClassFilter());
      }

      // jadeOSGIBundle.jar can already be started (or not): if so: trigger
      // here...
      if (serverReference != null)
      {
        this.serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, serverReference));
      }
    }
    catch (InvalidSyntaxException ex)
    {
      throw new RuntimeException(ex);
    }
  }


  /**
   * unregister agent
   *
   * @param  aContext BundleContext
   *
   * @throws RuntimeException wraps InvalidSyntaxException
   */
  @Override public void deActivated(final BundleContext aContext)
  {
    if (LOG.isInfoEnabled())
    {
      LOG.info(getClass().getSimpleName() + ": de-activated() for agent '" + AGENT_TYPE.getCode() + "'");
    }

    try
    {
      ServiceReference serverReference = getServiceReference(aContext);
      if (this.serviceListener != null)
      {
        aContext.removeServiceListener(this.serviceListener);
      }
      if (serverReference != null)
      {
        unregisterAgent(aContext, serverReference);
      }
    }
    catch (InvalidSyntaxException ex)
    {
      throw new RuntimeException(ex);
    }
  }


  /**
   * gets string containing jade filter on classed of JadeRuntimeService
   *
   * @return String
   */
  private String getJadeRuntimeServiceClassFilter()
  {
    String jrsName = JadeRuntimeService.class.getName();
    String filter = "(objectclass=" + jrsName + ")";
    return filter;
  }


  /**
   * gets reference to jade service (of which there only can be one)
   *
   * @param  aContext BundleContext
   *
   * @return ServiceReference
   *
   * @throws InvalidSyntaxException
   */
  private ServiceReference getServiceReference(final BundleContext aContext) throws InvalidSyntaxException
  {
    ServiceReference[] serverReferences = aContext.getServiceReferences((String) null, getJadeRuntimeServiceClassFilter());
    assert (serverReferences == null) || (serverReferences.length <= 1) : "can only be up to one osgi jade service.. (" + serverReferences.length + " found)";
    if ((serverReferences != null) && (serverReferences.length == 1))
    {
      return serverReferences[0];
    }
    return null;
  }


  /**
   * register agent (AGENT_TYPE, AGENT_CLASS)
   *
   * @param  aContext       BundleContext
   * @param  aJadeReference
   *
   * @throws RuntimeException
   */
  private void registerAgent(BundleContext aContext, final ServiceReference aJadeReference) throws RuntimeException
  {
    AgentController agentController = null;
    JadeRuntimeService jrs = (JadeRuntimeService) aContext.getService(aJadeReference);
    // In upgrade mode, it is failing. Working around this for temporary purpose.
    if (jrs == null)
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Could not register agent because it is not found");
      }
      return;
    }
    try
    {
      // Check if agent is not already available
      try
      {
        agentController = jrs.getAgent(AGENT_TYPE.getCode());
      }
      catch (ControllerException ex)
      {
        agentController = null;
      }

      // only start agent if it's not available yet
      if (agentController == null)
      {
        Agent agent = (Agent) AGENT_CLASS.newInstance();
        agentController = jrs.acceptNewAgent(AGENT_TYPE.getCode(), agent);
        agentController.start();
      }
    }
    catch (Exception ex)
    {
      throw new RuntimeException(ex);
    }
  }


  /**
   * unregister agent (AGENT_TYPE, AGENT_CLASS) if still registered
   *
   * @param  aContext       BundleContext
   * @param  aJadeReference
   *
   * @throws RuntimeException
   */
  private void unregisterAgent(BundleContext aContext, ServiceReference aJadeReference) throws RuntimeException
  {
    JadeRuntimeService jrs = (JadeRuntimeService) aContext.getService(aJadeReference);
    // In upgrade mode, it is failing. Working around this for temporary purpose.
    if (jrs == null)
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Could not unregister agent because it is not found");
      }
      return;
    }
    try
    {
      LOG.info("about to kill jrs");
      jrs.kill();
      LOG.info("jrs killed");
//      AgentController agentController = jrs.getAgent(AGENT_TYPE.getCode());
//      if (agentController != null)
//      {
//        LOG.info("kill the agent");
//        agentController.kill();
//      }
    }
    catch (StaleProxyException ex)
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Could not unregister agent because it already gone", ex);
      }
      // no need to take action...
    }
    catch (Exception ex)
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Could not unregister agent because of a runtime exception", ex);
      }
      throw new RuntimeException(ex);
    }
    finally
    {
      LOG.info("at end of unregistering agent");
    }
  }
}
