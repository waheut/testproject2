// Planon Enterprise Edition Source file: ControllerAgentActivator.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.agent.osgi;

import org.apache.felix.dm.*;
import org.osgi.framework.*;

import java.util.*;

import nl.planon.hades.*;

import nl.planon.morpheus.pdm.server.osgi.*;

/**
 * ControllerAgentActivator
 */
public class ControllerAgentActivator extends DependencyActivatorBase
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void destroy(BundleContext aContext, DependencyManager aManager)
  {
  }


  /**
   * {@inheritDoc}
   */
  @Override public void init(BundleContext aContext, DependencyManager aManager)
  {
  }


  /**
   * Registers the given IApplicationLifecycleListener object to listen for the appserver entering
   * {@code NORMAL_MODE}.
   *
   * @param aListener The listener to register, never {@code null}.
   *
   * @see   AppServerInit.AppServerRunMode#NORMAL_MODE
   */
  public void registerLifecycleListener(IApplicationLifecycleListener aListener)
  {
    assert aListener != null : "listener must be specified";
    DependencyManager manager = getDependencyManager();
    Properties props = new Properties();
    Component service = manager.createComponent();

    service.setInterface(IApplicationLifecycleListener.class.getName(), props);
    service.setImplementation(aListener);
    manager.add(service);
  }


  /**
   * {@inheritDoc}
   */
  @Override public void start(BundleContext aContext) throws Exception
  {
    super.start(aContext);
    registerLifecycleListener(new PDMControllerAgentLifecycle());
  }
}
