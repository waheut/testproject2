// Planon Enterprise Edition Source file: CommandLineArgumentsTest.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool;

import org.junit.*;

import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;

/**
 * CommandLineArgumentsTest
 */
public class CommandLineArgumentsTest
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException
   */
  @Test public void testAddImport() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"addImport", "UserName=pasmin", "Password=aa", "importDumpFile=D:\\db_mssql\\mssqlNOExtension_bak", "accesscode=plansql", "databasename=pietje", "AccountDatabaseOwnerRef=PASMIN", "PDMLocationRef=NL", "timeout=8000"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.ADDIMPORT, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
    Assert.assertEquals("D:\\db_mssql\\mssqlNOExtension_bak", arguments.getArgument("importDumpFile"));
    Assert.assertEquals("plansql", arguments.getArgument("accesscode"));
    Assert.assertEquals("pietje", arguments.getArgument("databasename"));
    Assert.assertEquals("PASMIN", arguments.getArgument("AccountDatabaseOwnerRef"));
    Assert.assertEquals("8000", arguments.getArgument("timeout"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test public void testCommandWithEqualsInArgument() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"REPORTDATABASEPROPERTIES", "UserName==pasmin"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.REPORTDATABASEPROPERTIES, arguments.getCommand());
    Assert.assertEquals("=pasmin", arguments.getArgument("UserName"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException
   */
  @Test public void testCreateDsFile() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"createDsFile", "UserName=pasmin", "Password=aa", "databasename=PIETJE", "PlanonDSFile=c:\\ds.xml"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.CREATEDSFILE, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
    Assert.assertEquals("PIETJE", arguments.getArgument("databasename"));
    Assert.assertEquals("c:\\ds.xml", arguments.getArgument("PlanonDSFile"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException
   */
  @Test public void testDelete() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"delete", "UserName=pasmin", "Password=aa", "databasename=PIETJE"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.DELETE, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
    Assert.assertEquals("PIETJE", arguments.getArgument("databasename"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test public void testExport() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"export", "UserName=pasmin", "Password=aa", "databasename=PIETJE", "exportdumpfile=c:\\exp.dmp"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.EXPORT, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
    Assert.assertEquals("PIETJE", arguments.getArgument("databasename"));
    Assert.assertEquals("c:\\exp.dmp", arguments.getArgument("exportdumpfile"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException
   */
  @Test public void testHelpCommand() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"help"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.HELP, arguments.getCommand());
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test public void testHelpWithCredentials() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"help", "UserName=pasmin", "Password=aa"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.HELP, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test public void testImport() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"Import", "UserName=pasmin", "Password=aa", "importDumpFile=D:\\db_mssql\\mssqlNOExtension_bak", "databasename=pietje"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.IMPORT, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
    Assert.assertEquals("D:\\db_mssql\\mssqlNOExtension_bak", arguments.getArgument("importDumpFile"));
    Assert.assertEquals("pietje", arguments.getArgument("databasename"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException
   */
  @Test public void testParseEmptyCommand() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(null);
    arguments.parse();

    Assert.assertEquals(PDMCommand.HELP, arguments.getCommand());
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException
   */
  @Test public void testParseNullValue() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {null});
    arguments.parse();

    Assert.assertEquals(PDMCommand.HELP, arguments.getCommand());
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test public void testReportDatabaseNames() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"REPORTDATABASENAMES", "UserName=pasmin", "Password=aa", "outputfile=C:\\db.txt"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.REPORTDATABASENAMES, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
    Assert.assertEquals("C:\\db.txt", arguments.getArgument("outputfile"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test public void testReportDatabaseProperties() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"REPORTDATABASEPROPERTIES", "UserName=pasmin", "Password=aa", "OutputPropertyFile=c:\\dbinfov10.txt", "databasename=PIETJE"});
    arguments.parse();

    Assert.assertEquals(PDMCommand.REPORTDATABASEPROPERTIES, arguments.getCommand());
    Assert.assertEquals("pasmin", arguments.getArgument("UserName"));
    Assert.assertEquals("aa", arguments.getArgument("Password"));
    Assert.assertEquals("c:\\dbinfov10.txt", arguments.getArgument("OutputPropertyFile"));
    Assert.assertEquals("PIETJE", arguments.getArgument("databasename"));
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test(expected = FailedToParseCommandlineException.class)
  public void testUnknownCommand() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"DOSTUFFBLA"});
    arguments.parse();
  }


  /**
   * DOCUMENT ME!
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  @Test(expected = FailedToParseCommandlineException.class)
  public void testUnknownCommandWithEquals() throws FailedToParseCommandlineException
  {
    CommandLineArguments arguments = new CommandLineArguments(new String[] {"DO=STUFFBLA"});
    arguments.parse();
  }
}
