// Planon Enterprise Edition Source file: BatchTool.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool;

import nl.planon.hades.client.*;
import nl.planon.hades.client.resourceinitiators.*;
import nl.planon.hades.client.resourcelocators.login.*;
import nl.planon.hades.exception.*;

import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;

import nl.planon.util.pnlogging.*;

/**
 * BatchTool
 */
public class BatchTool
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String PRODUCT_KEY = "PDM";

  private static final PnLogger LOG = PnLogger.getLogger(BatchTool.class, PnLogCategory.DEFAULT);

  private static final String MSG_CONNECTED = "Connected";
  private static final String MSG_INCORRECT_ARGUMENTS_SPECIFIED = "Incorrect arguments specified";
  private static final String MSG_LOGIN_FAILED = "Failed to log in; please check username and password.";
  private static final String MSG_LOGOUT_FAILED = "Failed to logout.";

  //~ Instance Variables ---------------------------------------------------------------------------

  private final CommandExecutorFactory executorFactory;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BatchTool object.
   *
   * @param executorFactory
   */
  public BatchTool(CommandExecutorFactory executorFactory)
  {
    this.executorFactory = executorFactory;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * execute specified command
   *
   * @param  aCommandLineArguments command line arguments
   *
   * @throws ProgramExecutionException DOCUMENT ME!
   */
  public void execute(CommandLineArguments aCommandLineArguments)
  {
    ICommandExecutor executer = this.executorFactory.build(aCommandLineArguments);
    PDMCommand command = aCommandLineArguments.getCommand();

    if (LOG.isInfoEnabled())
    {
      LOG.info("Starting: '" + command + "'.");
    }

    boolean loggedIn = false;
    try
    {
      loggedIn = login(aCommandLineArguments.getUserName(), aCommandLineArguments.getPassword());

      // if we are not executing the HELP command, or we are not logged in. Print the help page and quit the application.
      if ((command != PDMCommand.HELP) && !loggedIn)
      {
        HelpCommand help = new HelpCommand(aCommandLineArguments);
        help.printHelp(command);

        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to login. Can not continue. Please provide us with valid credentials.");
        }
        throw new ProgramExecutionException(ErrorCode.FAILURE);
      }

      // if we are executing the help command and logged in, or if we are logged in. update the command line arguments.
      if (((command == PDMCommand.HELP) && loggedIn) || loggedIn)
      {
        command.updateCommandLineItems();
      }

      if (!command.verifyArguments(aCommandLineArguments))
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to verify commandline arguments. The commandline arguments were: '" + String.join(",", aCommandLineArguments.getOriginalArguments()) + "'.");
        }
        throw new ProgramExecutionException(ErrorCode.FAILURE);
      }

      switch (command)
      {
        case HELP :
          HelpCommand help = new HelpCommand(aCommandLineArguments);
          help.printHelp(null);
          break;
        case ADDIMPORT :
          executer.addAndImportDB();
          break;
        case EXPORT :
          executer.exportDB();
          break;
        case DELETE :
          executer.deleteDB();
          break;
        case IMPORT :
          executer.importDB();
          break;
        case REPORTDATABASEPROPERTIES :
          executer.reportDBProperties();
          break;
        case REPORTDATABASENAMES :
          executer.reportDBNames();
          break;
        case CREATEDSFILE :
          executer.createPlanonDS(command);
          break;
      }
    }
    catch (CommandExecutionException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to execute the requested command. The commandline arguments were: '" + String.join(",", aCommandLineArguments.getOriginalArguments()) + "'. See the stacktrace for more information.", e);
      }
      throw new ProgramExecutionException(ErrorCode.FAILURE);
    }
    catch (PnErrorListException | BOMExecutionException | AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to execute the requested command. The commandline arguments were: '" + String.join(",", aCommandLineArguments.getOriginalArguments()) + "'. See the stacktrace for more information.", e);
      }
      throw new ProgramExecutionException(ErrorCode.FAILURE);
    }
    finally
    {
      if (loggedIn)
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("Logging out.");
        }
        logout();
      }
    }

    if (LOG.isInfoEnabled())
    {
      LOG.info("Finished command " + command.name());
    }
  }


  /**
   * try to login on application level. If an exception (other than wrong username or password) is
   * thrown, another attempt is done after a certain sleeping time. If, after a maximum number of
   * attempts the login still fails, the application fails.
   *
   * @param  aUserName Username
   * @param  aPassword password
   *
   * @return DOCUMENT ME!
   *
   * @throws ProgramExecutionException DOCUMENT ME!
   */
  private boolean login(String aUserName, String aPassword)
  {
    if ((aUserName == null) || (aPassword == null))
    {
      return false;
    }

    for (int i = 0; i < IPDMCommandLine.MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB; i++)
    {
      try
      {
        ClientSessionContext.getInstance().login(PRODUCT_KEY, aUserName, aPassword);
        SessionHades.get().setResourceInitiators( //
                      new TranslatorInitiator(), //
                      new UpdateCacheInitiator());
        LOG.info(MSG_CONNECTED);
        return true;
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        LOG.error(MSG_LOGIN_FAILED);
      }

      LOG.info("retry " + i);
      try
      {
        Thread.sleep(IPDMCommandLine.INTERVAL_BETWEEN_ATTEMPTS);
      }
      catch (InterruptedException e)
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Authentication failed, and the Thread.sleep has been interrupted.", e);
        }
      }
    }

    if (LOG.isErrorEnabled())
    {
      LOG.error("Failed to login because the overall process walked into a timeout. The values were: MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB: '" + IPDMCommandLine.MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB + "', INTERVAL_BETWEEN_ATTEMPTS: '" + IPDMCommandLine.INTERVAL_BETWEEN_ATTEMPTS + "'");
    }
    throw new ProgramExecutionException(ErrorCode.FAILURE);
  }


  /**
   * logout pdm user
   */
  private void logout()
  {
    try
    {
      ClientSessionContext.getInstance().logout();
      LOG.info("Logged out.");
    }
    catch (Exception ex)
    {
      LOG.error(MSG_LOGOUT_FAILED);
    }
  }
}
