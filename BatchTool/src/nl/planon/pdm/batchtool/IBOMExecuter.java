// Planon Enterprise Edition Source file: IBOMExecuter.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.valueobject.*;

import nl.planon.pdm.batchtool.exceptions.*;

/**
 * interface for executing the BOMs needed for the command line client
 */
public interface IBOMExecuter
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * try to add a database. In case of failure, another attempt will be done after some sleeping
   * time. Failures could occur due to for example: bo under construction, server down and webdav
   * down. If, after a maximum number of attempts the bom still fails, null is returned.
   *
   * @param  aBOType type of bo to add
   *
   * @return BOValue of added bo, null if bom failed
   *
   * @throws BOMExecutionException DOCUMENT ME!
   */
  IBOValue executeAddBomTimeBoxed(IBOType aBOType) throws BOMExecutionException;


  /**
   * Download the export dump of a Database. get Export directory from argumentParser
   *
   * @param  aBoDatabase IBOValue for Exporting
   * @param  aExportPath export dump file path
   *
   * @throws BOMExecutionException FieldNotFoundException
   */
  void executeDownloadExport(IBOValue aBoDatabase, String aExportPath) throws BOMExecutionException;


  /**
   * execute the savebom
   *
   * @param  aBOValue to save
   *
   * @return saved bo
   *
   * @throws BOMExecutionException DOCUMENT ME!
   */
  IBOValue executeSaveBom(IBOValue aBOValue) throws BOMExecutionException;


  /**
   * try to execute a BOM. In case of failure retry after some time with a maximum of retries.
   * Failures could occur due to for example: bo under construction, server down, webdav not
   * available, etc.
   *
   * @param  aBoDatabase          BO to which the BOM applies
   * @param  aPnBomImportDatabase aBOMName        the name of BOM to execute
   * @param  aStateInitial        aExpectedState  the state in which the BO is expected to be for
   *                              executing the BOM
   * @param  aBOMDescription      a description of the BOM
   *
   * @return true when BOM finished successfully, false if after 1 hour trying BOM still fails.
   *
   * @throws BOMExecutionException DOCUMENT ME!
   */
  boolean executeTimeBoxed(IBOValue aBoDatabase, String aPnBomImportDatabase, String aStateInitial, String aBOMDescription) throws BOMExecutionException;


  /**
   * try to upload a dumpfile. In case of failure retry after some time with a maximum number of
   * retries. Failures could occur due to, for example, webdav not available.
   *
   * @param  aBoDatabase     BO PDM Database of which the dumpfile must be uploaded
   * @param  aWebdavLocation location to where the dumpfile must be uploaded
   *
   * @return an up-to-date BO PDM Database after uploading the dump file
   *
   * @throws BOMExecutionException PnErrorListException
   */
  IBOValue executeUploadFileTimeBoxed(IBOValue aBoDatabase, String aWebdavLocation) throws BOMExecutionException;


  /**
   * DOCUMENT ME!
   *
   * @param  aBOM                DOCUMENT ME!
   * @param  aMethodArgumentList DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   *
   * @throws BOMExecutionException AuthorizationException DOCUMENT ME!
   */
  IBOValue internalExecuteBOM(IBOM aBOM, IBaseValue... aMethodArgumentList) throws BOMExecutionException;


  /**
   * cache refresh of a specific BO.
   *
   * @param  aBoDatabase aValue bo value for which a cache refresh is requested
   *
   * @return newly read BO
   *
   * @throws BOMExecutionException PnErrorListException
   */
  IBOValue read(IBOValue aBoDatabase) throws BOMExecutionException;


  /**
   * Wait for the database to be back to expected state, by repeatedly checking with a certain
   * interval the status of the database.
   *
   * @param  aPKPDMDatabase         Primary key of database of which the status to be checked
   * @param  aDatabaseExpectedState PDM Database expected state. For eg: Available, Initial
   *
   * @return true if request finished successfully, false if request finished with failure
   *
   * @throws BOMExecutionException PnErrorListException
   */
  boolean waitForDatabaseToBeInExpectedState(IBaseValue aPKPDMDatabase, String aDatabaseExpectedState) throws BOMExecutionException;


  /**
   * wait for the current request to finish, by repeatedly checking with a certain interval the
   * status of the request.
   *
   * @param  aPrimaryKeyValue aPKPDMDatabase primary key of database of which the request must be
   *                          checked
   * @param  aTimeOut         aI aTimeOut       time out in seconds. If timed out method will not
   *                          return
   *
   * @return true if request finished successfully, false if request finished with failure
   *
   * @throws BOMExecutionException PnErrorListException
   */
  boolean waitForRequestToFinish(IBaseValue aPrimaryKeyValue, int aTimeOut) throws BOMExecutionException;
}
