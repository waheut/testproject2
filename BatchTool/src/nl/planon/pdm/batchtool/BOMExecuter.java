// Planon Enterprise Edition Source file: BOMExecuter.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import nl.planon.ares.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.client.resourcelocators.cache.*;
import nl.planon.hades.errorhandling.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.pdm.batchtool.boinfo.*;
import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;

import nl.planon.util.pnlogging.*;

/**
 * class for executing the BOMs needed for the command line client
 */
public class BOMExecuter implements IBOMExecuter
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(BOMExecuter.class);
  private static final String WEBDAV_CONNECTION_FAIL = " cannot connect to the webdav server";

  //~ Instance Variables ---------------------------------------------------------------------------

  private final BOFinder boFinder;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMExecuter object.
   */
  public BOMExecuter()
  {
    this.boFinder = new BOFinder();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * try to add a database. In case of failure, another attempt will be done after some sleeping
   * time. Failures could occur due to for example: bo under construction, server down and webdav
   * down. If, after a maximum number of attempts the bom still fails, null is returned.
   *
   * @param  aBOType type of bo to add
   *
   * @return BOValue of added bo, null if bom failed
   *
   * @throws BOMExecutionException DOCUMENT ME!
   */
  @Override public IBOValue executeAddBomTimeBoxed(IBOType aBOType) throws BOMExecutionException
  {
    // Retrieve the BOM we've been asked for; throws an exception in case it doesn't exist...
    IBOM bom = null;
    int nrOfAttemptsToExecuteBOM = 0;
    boolean maxNrOfAttemptsReached;
    boolean doRefresh = false;
    do
    {
      maxNrOfAttemptsReached = (nrOfAttemptsToExecuteBOM == IPDMCommandLine.MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB);
      nrOfAttemptsToExecuteBOM++;
      try
      {
        if (doRefresh)
        {
          refresh();
          doRefresh = false;
        }
        bom = getBOM(aBOType, PnBOMName.PN_BOM_ADD);
        IBOValue result = BOMResourceLocator.getInstance().execute(bom);

        if (nrOfAttemptsToExecuteBOM >= 1)
        {
          // logging only when its at least 1 times going wrong
          LOG.info("Add bom found");
        }
        return result;
      }
      catch (PnErrorListException ex)
      {
        if (!ex.containsMessageType(ErrorNumberHades.EC_CACHEUPDATECOUNT) || maxNrOfAttemptsReached)
        {
          LOG.error(IPDMCommandLine.MSG_TIME_OUT_ERROR, ex);
        }
        if (nrOfAttemptsToExecuteBOM == 1)
        {
          LOG.error(aBOType.getPnName() + ".Add bom execution failed", ex);
        }
        doRefresh = true;
        try
        {
          Thread.sleep(IPDMCommandLine.INTERVAL_BETWEEN_ATTEMPTS);
        }
        catch (InterruptedException ex1)
        {
          if (LOG.isWarnEnabled())
          {
            LOG.warn("Failed to sleep. Will continue.", ex1);
          }
        }
      }
      // probably bo under construction
      catch (BOMNotFoundException ex)
      {
        if (maxNrOfAttemptsReached)
        {
          LOG.error(IPDMCommandLine.MSG_TIME_OUT_ERROR, ex);
        }
        if (nrOfAttemptsToExecuteBOM == 1)
        {
          LOG.error("Add bom not found, could be that the bo is under construction", ex);
        }
        doRefresh = true;
        try
        {
          Thread.sleep(IPDMCommandLine.INTERVAL_BETWEEN_ATTEMPTS);
        }
        catch (InterruptedException ex1)
        {
          if (LOG.isWarnEnabled())
          {
            LOG.warn("Failed to sleep. Will continue.", ex1);
          }
        }
      }
    }
    while (!maxNrOfAttemptsReached);
    LOG.error(IPDMCommandLine.MSG_TIME_OUT_ERROR);
    return null;
  }


  /**
   * Download the export dump of a Database. get Export directory from argumentParser
   *
   * @param  aBOPDMDatabase IBOValue for Exporting
   * @param  aExportPath    export dump file path
   *
   * @throws BOMExecutionException FieldNotFoundException
   */
  @Override public void executeDownloadExport(IBOValue aBOPDMDatabase, String aExportPath) throws BOMExecutionException
  {
    try
    {
      long start = System.currentTimeMillis();

      while (true)
      {
        IBOFieldValue exportFile = aBOPDMDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE);

        if (exportFile.isEmpty())
        {
          if ((System.currentTimeMillis() - start) > TimeUnit.SECONDS.toMillis(10))
          {
            throw new BOMExecutionException("The: 'IBOPDMDatabaseDef.PN_PDM_EXPORTFILE' field was empty. Thus unable to download the export.");
          }

          try
          {
            Thread.sleep(TimeUnit.SECONDS.toMillis(1));
          }
          catch (InterruptedException ex)
          {
          	if(LOG.isErrorEnabled()) {
          		LOG.error("Sleepting to wait till the 'IBOPDMDatabaseDef.PN_PDM_EXPORTFILE' field is filled was interrupted.", ex);
          	}
          }
        }
        else
        {
        	WebDAVUtilities.getInstance().downloadPDMDumpFile(aBOPDMDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE).getAsString(), aExportPath);
          break;
        }
      }
    }
    catch (FieldNotFoundException | IOException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to executeDownloadExport. See stracktrace for more information.", e);
      }
      throw new BOMExecutionException("Failed to executeDownloadExport. See stracktrace for more information.", e);
    }
  }


  /**
   * execute the savebom
   *
   * @param  aBOValue to save
   *
   * @return saved bo
   *
   * @throws BOMExecutionException DOCUMENT ME!
   */
  @Override public IBOValue executeSaveBom(IBOValue aBOValue) throws BOMExecutionException
  {
    try
    {
      final IBOMListManager bomListManager = aBOValue.getBOMListManager();
      final IBOM saveBOM = bomListManager.getSaveBOMEx();
      return BOMResourceLocator.getInstance().execute(saveBOM);
    }
    catch (PnErrorListException | AuthorizationException ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Error when trying to save BO. See stacktrace for more information.", ex);
      }
      throw new BOMExecutionException("Error when trying to save BO. See stacktrace for more information.", ex);
    }
  }


  /**
   * try to execute a BOM. In case of failure retry after some time with a maximum of retries.
   * Failures could occur due to for example: bo under construction, server down, webdav not
   * available, etc.
   *
   * @param  aBOPDMDatabase  BO to which the BOM applies
   * @param  aBOMName        the name of BOM to execute
   * @param  aExpectedState  the state in which the BO is expected to be for executing the BOM
   * @param  aBOMDescription a description of the BOM
   *
   * @return true when BOM finished successfully, false if after 1 hour trying BOM still fails.
   *
   * @throws BOMExecutionException DOCUMENT ME!
   */
  @Override public boolean executeTimeBoxed(final IBOValue aBOPDMDatabase, String aBOMName, String aExpectedState, String aBOMDescription) throws BOMExecutionException
  {
    try
    {
      // Check if BO database is in the expected state, otherwise the bom cannot be executed
      if (!aExpectedState.equals(aBOPDMDatabase.getStateName()))
      {
        throw new BOMExecutionException(aBOMDescription + " not available, not in state '" + aExpectedState + "'");
      }
      int nrOfAttemptsToExecuteBOM = 0;
      IBOValue boPDMDatabase = aBOPDMDatabase;
      boolean doRead = false;
      do
      {
        if (doRead)
        {
          LOG.info("going to refresh cache and read database again..");

          refresh();

          boPDMDatabase = read(aBOPDMDatabase);
          doRead = false;
        }
        if (boPDMDatabase.getBOMListManager().hasBOMByPnName(aBOMName))
        {
          BOM pdmBOM = (BOM) boPDMDatabase.getBOMListManager().getBOMByPnNameEx(aBOMName);
          try
          {
            executeIgnoreWarnings(pdmBOM);
            break; // bom executed successfully
          }
          catch (PnErrorListException ex)
          {
            IErrorHandler errorHandler = ex.getErrorHandler();

            // do not throw an error because bom is probably not available yet, instead wait a moment and give it another try.
            // an error is only logged the first time it fails
            if (nrOfAttemptsToExecuteBOM == 0)
            {
              LOG.error(aBOMDescription + " a non-fatal application error occurred", ex);
            }

            if (ex.containsMessageType(ErrorNumberHades.EC_CACHEUPDATECOUNT))
            {
              LOG.info("EC_CACHEUPDATECOUNT occurred");
              doRead = true;
            }
          }
        }
        else
        {
          doRead = true;
        }

        nrOfAttemptsToExecuteBOM++;
        if (nrOfAttemptsToExecuteBOM > IPDMCommandLine.MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB)
        {
          // after the maximum number of retries it still fails, hence, log a message and return negative result
          LOG.error(IPDMCommandLine.MSG_TIME_OUT_ERROR);
          return false;
        }
        try
        {
          Thread.sleep(IPDMCommandLine.INTERVAL_BETWEEN_ATTEMPTS);
        }
        catch (InterruptedException ex)
        {
          LOG.warn("Sleeping failed. Will continue.", ex);
        }
      }
      while (true); // loop is left by exception or successful scenario
      if (nrOfAttemptsToExecuteBOM >= 1)
      {
        LOG.info(aBOMDescription + " bom found");
      }
      return true;
    }
    catch (PnErrorListException | BOMNotFoundException e)
    {
      if (LOG.isWarnEnabled())
      {
        LOG.warn("Failed to execute in a time boxed manner. See stacktrace for more information.", e);
      }
      throw new BOMExecutionException("Failed to execute in a time boxed manner. See stacktrace for more information.", e);
    }
  }


  /**
   * try to upload a dumpfile. In case of failure retry after some time with a maximum number of
   * retries. Failures could occur due to, for example, webdav not available.
   *
   * @param  aPDMDatabase BO PDM Database of which the dumpfile must be uploaded
   * @param  aLocation    location to where the dumpfile must be uploaded
   *
   * @return an up-to-date BO PDM Database after uploading the dump file
   *
   * @throws BOMExecutionException PnErrorListException
   */
  @Override public IBOValue executeUploadFileTimeBoxed(IBOValue aPDMDatabase, String aLocation) throws BOMExecutionException
  {
    try
    {
      IBOValue result = null;
      int nrOfAttempts = 0;
      String pdmDatabaseCode = aPDMDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_NAME).getAsString();
      do
      {
        String uploadResult = WebDAVUtilities.getInstance().uploadPDMDumpFile(aPDMDatabase, aLocation);
        // null = good result
        if (uploadResult == null)
        {
          result = executeSaveBom(aPDMDatabase);
          if (result != null)
          {
            break;
          }
        }
        nrOfAttempts++;
        // log only the first time when its going wrong.
        if (nrOfAttempts == 1)
        {
          LOG.error(WEBDAV_CONNECTION_FAIL);
        }
        else
        {
          LOG.info(WEBDAV_CONNECTION_FAIL + " retry (" + nrOfAttempts + ")");
        }

        try
        {
          Thread.sleep(IPDMCommandLine.INTERVAL_BETWEEN_ATTEMPTS);
        }
        catch (InterruptedException e)
        {
          if (LOG.isWarnEnabled())
          {
            LOG.warn("Sleeping was interrupted. Will continue.");
          }
        }

        if (nrOfAttempts >= IPDMCommandLine.MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB)
        {
          // upload failed too many times now, delete the bo and let the application fail.
          LOG.error(uploadResult);
          LOG.error(IPDMCommandLine.MSG_TIME_OUT_ERROR);
          result = this.boFinder.getMatchingBOValue(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_NAME, pdmDatabaseCode);
          boolean deleteOk = executeTimeBoxed(result, IBOPDMDatabaseDef.PN_BOM_DELETE, IBOPDMDatabaseDef.STATE_INITIAL, "Delete");
          LOG.warn("Delete = " + deleteOk);

          if (LOG.isWarnEnabled())
          {
            LOG.warn("Failed to upload the database to webdav.");
          }
          throw new BOMExecutionException(WEBDAV_CONNECTION_FAIL);
        }
      }
      while (true); // loop is left by exception or successfull scenario

      if (nrOfAttempts >= 1)
      {
        LOG.info("reconnected to the webdav");
      }
      return result;
    }
    catch (PnErrorListException | AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to upload the database to a webdav location. See the stacktrace for more information.", e);
      }
      throw new BOMExecutionException("Failed to upload the database to a webdav location. See the stacktrace for more information.", e);
    }
  }


  /**
   * get the BOM corresponding to the specified botype and bomname
   *
   * @param  aBOType  BOType to which the bom belongs
   * @param  aBOMName name of BOM
   *
   * @return bom that's been asked for
   *
   * @throws PnErrorListException
   * @throws BOMNotFoundException
   */
  public IBOM getBOM(IBOType aBOType, String aBOMName) throws PnErrorListException, BOMNotFoundException
  {
    final IBOMListManager bomListManager = BOMResourceLocator.getInstance().getActiveBusinessObjectMethods(aBOType);
    return bomListManager.getBOMByPnNameEx(aBOMName);
  }


  /**
   * Helper method to execute a BOM for the given BOType.
   *
   * @param  aBOM                aBOType the BOType to execute the BOM on;
   * @param  aMethodArgumentList the (ordered) list of BOM-arguments, can be <code>null</code>.
   *
   * @return the resulting businessobject.
   *
   * @throws BOMExecutionException AuthorizationException in case of authorization problems;
   */
  @Override public IBOValue internalExecuteBOM(final IBOM aBOM, final IBaseValue... aMethodArgumentList) throws BOMExecutionException
  {
    try
    {
      final BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();

      final List<IBOFieldValue> actualMethodArgumentList = aBOM.getFieldValueList();

      // Use a rather naive approach to map each given method-argument to the ones expected: simply expect them in order...
      final int size = (aMethodArgumentList == null) ? 0 : Math.min(aMethodArgumentList.length, actualMethodArgumentList.size());

      for (int i = 0; i < size; i++)
      {
        final IBaseValue methodArgumentValue = aMethodArgumentList[i];
        final IBOFieldValue methodArgument = actualMethodArgumentList.get(i);
        methodArgument.setAsBaseValue(methodArgumentValue);
      }

      IBOValue result;
      boolean retry;

      // Execute the actual BOM, and handle warnings/confirmations appropriately...
      do
      {
        try
        {
          retry = false;

          result = bomResourceLocator.execute(aBOM);
        }
        catch (PnErrorListException exception)
        {
          // Not all is lost yet, maybe we've got warnings and/or confirmations to approve...
          retry = internalHandlePnErrorListException(aBOM, exception);
          result = null; // to keep compiler happy ;)

          // Hmm, no retry? Rethrow the original exception, as we should inform the caller that its call has failed...
          if (!retry)
          {
            throw exception;
          }
        }
      }
      while (retry == true);

      return result;
    }
    catch (PnErrorListException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to execute internal bom.");
      }
      throw new BOMExecutionException("Failed to execute internal bom. See stacktrace for more infomration.", e);
    }
  }


  /**
   * cache refresh of a specific BO.
   *
   * @param  aValue bo value for which a cache refresh is requested
   *
   * @return newly read BO
   *
   * @throws BOMExecutionException PnErrorListException
   */
  @Override public IBOValue read(IBOValue aValue) throws BOMExecutionException
  {
    try
    {
      IBOMListManager bomListManager = BOMResourceLocator.getInstance().getBusinessObjectMethods(aValue.getBOType());
      IBOMRead bomRead = bomListManager.getReadBOMEx();

      bomRead.setPrimaryKeyValue(aValue.getPrimaryKeyValue());
      final BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();
      aValue = bomResourceLocator.execute(bomRead);
      return aValue;
    }
    catch (PnErrorListException | BOMNotFoundException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to read the BOValue. See the stacktrace for more information.", e);
      }
      throw new BOMExecutionException("Failed to read the BOValue. See the stacktrace for more information.", e);
    }
  }


  /**
   * Wait for the database to be back to expected state, by repeatedly checking with a certain
   * interval the status of the database.
   *
   * @param  aPKPDMDatabase         Primary key of database of which the status to be checked
   * @param  aDatabaseExpectedState PDM Database expected state. For eg: Available, Initial
   *
   * @return true if request finished successfully, false if request finished with failure
   *
   * @throws BOMExecutionException PnErrorListException
   */
  @Override public boolean waitForDatabaseToBeInExpectedState(IBaseValue aPKPDMDatabase, String aDatabaseExpectedState) throws BOMExecutionException
  {
    try
    {
      int timeOut = 120; // Time-out for two minutes
      long start = System.nanoTime();
      long nanoTimeout = timeOut * 1000L * 1000L * 1000L;
      final IBOM bom = getBOM(BOTypePDMP5Module.PDM_DATABASE, PnBOMName.PN_BOM_READ);
      do
      {
        IBOValue boValuePDMDatabase = internalExecuteBOM(bom, aPKPDMDatabase);
        String pdmDatabaseState = boValuePDMDatabase.getStateName();
        if (aDatabaseExpectedState.equals(pdmDatabaseState))
        {
          return true;
        }

        if ((System.nanoTime() - start) > nanoTimeout)
        {
          LOG.error("Database does not return to " + aDatabaseExpectedState + " state in " + timeOut + " seconds. Not going to wait longer for it to finish (waited " + timeOut + " seconds)!");
          return false;
        }

        LOG.info("Waiting for database to return to " + aDatabaseExpectedState + " state..");
        try
        {
          Thread.sleep(TimeUnit.SECONDS.toMillis(5)); // with an interval 5 seconds
        }
        catch (InterruptedException ex)
        {
          if (LOG.isWarnEnabled())
          {
            LOG.warn("Failed to sleep, will continue");
          }
        }
      }
      while (true);
    }
    catch (PnErrorListException | AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to wait till the BO was in the expected state. See the stacktrace for more information.", e);
      }
      throw new BOMExecutionException("Failed to wait till the BO was in the expected state. See the stacktrace for more information.", e);
    }
  }


  /**
   * wait for the current request to finish, by repeatedly checking with a certain interval the
   * status of the request.
   *
   * @param  aPKPDMDatabase primary key of database of which the request must be checked
   * @param  aTimeOut       time out in seconds. If timed out method will not return
   *
   * @return true if request finished successfully, false if request finished with failure
   *
   * @throws BOMExecutionException PnErrorListException
   */
  @Override public boolean waitForRequestToFinish(IBaseValue aPKPDMDatabase, int aTimeOut) throws BOMExecutionException
  {
    try
    {
      long start = System.nanoTime();
      long nanoTimeout = aTimeOut * 1000L * 1000L * 1000L;
      IBaseValue pkPDMRequest = this.boFinder.getPrimaryKeyCurrentPDMRequest(aPKPDMDatabase);
      final IBOM bom = getBOM(BOTypePDMP5Module.BASE_PDM_REQUEST, PnBOMName.PN_BOM_READ);

      do
      {
        IBOValue boValuePDMRequest = internalExecuteBOM(bom, pkPDMRequest);
        String pdmRequestState = boValuePDMRequest.getStateName();
        if (IBOBasePDMRequestDef.STATE_OK.equals(pdmRequestState))
        {
          return true;
        }
        if (IBOBasePDMRequestDef.STATE_NOK.equals(pdmRequestState))
        {
          return false;
        }

        if ((System.nanoTime() - start) > nanoTimeout)
        {
          LOG.error("Request did not finish in " + aTimeOut + " seconds. Either db agent is not running, or it is very busy.. Not going to wait longer for it to finish (waited " + aTimeOut + " seconds)!");
          return false;
        }

        LOG.info("Waiting for request to be picked up and to be finished");
        try
        {
          Thread.sleep(IPDMCommandLine.INTERVAL_BETWEEN_ATTEMPTS);
        }
        catch (InterruptedException ex)
        {
          if (LOG.isWarnEnabled())
          {
            LOG.warn("Failed to sleep, will continue");
          }
        }
      }
      while (true);
    }
    catch (PnErrorListException | AuthorizationException e)
    {
      if (LOG.isWarnEnabled())
      {
        LOG.warn("Failed to wait for the request to finish, see the stacktrace for more information.", e);
      }
      throw new BOMExecutionException("Failed to wait for the request to finish, see the stacktrace for more information.", e);
    }
  }


  /**
   * executes bom ignoring warnings
   *
   * @param  aBOM bom to execute
   *
   * @throws PnErrorListException
   */
  private void executeIgnoreWarnings(BOM aBOM) throws PnErrorListException
  {
    do
    {
      try
      {
        BOMResourceLocator.getInstance().execute(aBOM);
        return;
      }
      catch (PnErrorListException ex)
      {
        IErrorHandler errorHandler = ex.getErrorHandler();
        if (errorHandler.isIgnoreWarnings())
        {
          throw ex; // some thing else
        }

        errorHandler.ignoreWarnings();
        aBOM.setErrorHandler(errorHandler);
      }
    }
    while (true);
  }


  /**
   * Helper method to approve all confirmations in the given error handler.
   *
   * @param aErrorHandler the errorhandler to approve, cannot be <code>null</code>.
   */
  private void internalApproveConfirmations(final IErrorHandler aErrorHandler)
  {
    final List<ErrorContextList> contextLists = aErrorHandler.getErrorContextLists();
    for (ErrorContextList contextList : contextLists)
    {
      for (PnMessage message : contextList.getMessages())
      {
        if (message instanceof ConfirmationMessage)
        {
          internalHandleConfirmation((ConfirmationMessage) message);
        }
      }
    }
  }


  /**
   * Helper method to approve a single confirmation.
   *
   * <p>For now, each given confirmation is simply answered positively, in the near future, this
   * might be changed to support more nifty confirmation tactics.</p>
   *
   * @param aMessage the confirmation message to approve, is never <code>null</code>.
   */
  private void internalHandleConfirmation(final ConfirmationMessage aMessage)
  {
    aMessage.setAnswer(ConfirmationResult.YES);
  }


  /**
   * Helper method to handle a PnErrorListException appropriately.
   *
   * @param  aBOM       the BOM we've just executed and which throwed the {@link
   *                    PnErrorListException}, cannot be <code>null</code>;
   * @param  aException the actual {@link PnErrorListException} that was thrown, cannot be <code>
   *                    null</code>.
   *
   * @return <code>true</code> if the calling method should retry the BOM once again, <code>
   *         false</code> to stuff it and rethrow the exception.
   */
  private boolean internalHandlePnErrorListException(final IBOM aBOM, final PnErrorListException aException)
  {
    // Hmm, check whether we've got unanswered warnings and/or confirmations...
    final IErrorHandler errorHandler = aException.getErrorHandler();
    final int errorCount = errorHandler.getNumberOfErrorMessages() + errorHandler.getNumberOfSystemMessages();
    // Hmm, errors mean than we can never get this right; abort immediately...
    if (errorCount > 0)
    {
      return false;
    }

    final int warningCount = errorHandler.getNumberOfWarningMessages();
    final int confirmationCount = errorHandler.getNumberOfConfirmationMessages();

    assert (errorCount == 0) && ((warningCount > 0) || (confirmationCount > 0)) : "Internal error: how did I get here?!";

    if (warningCount > 0)
    {
      // Handle warnings (which is easy, since we can simply tell the error handler to ignore them...
      errorHandler.ignoreWarnings();
    }

    if (confirmationCount > 0)
    {
      // Handle confirmations, which is not so easy, as we must answer them all explicitly...
      internalApproveConfirmations(errorHandler);
    }

    if (errorHandler.canReplyErrorhandler())
    {
      aBOM.setErrorHandler(errorHandler);
    }

    return true;
  }


  /**
   * refreshing caches
   *
   * @throws PnErrorListException
   */
  private void refresh() throws PnErrorListException
  {
    // Clear all client caches, but do not clear all state
    CacheManager.getInstance().clearAllInstances();

    ISessionAres session = SessionAres.get();
    session.clear();
  }
}
