// Planon Enterprise Edition Source file: Main.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;

import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;

import nl.planon.util.pnlogging.*;

/**
 * Main
 */
public class Main
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(Main.class, PnLogCategory.DEFAULT);
  private static final String DEFAULT_LOG_LEVEL = "INFO";

  //~ Instance Variables ---------------------------------------------------------------------------

  private CommandLineArguments commandLineArguments;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new Main object.
   */
  private Main()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Handle the specified command line parameters and execute the requested command (Add, Export,
   * Import, Delete, Read, etc.)
   *
   * @param aArgs the argument
   */
  public static void main(String[] aArgs)
  {
    try
    {
      Main main = new Main();
      main.execute(aArgs);
    }
    catch (ProgramExecutionException e)
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("Calling System.exit with exit code: '" + e.getErrorCode().getReturnValue() + "'.");
      }
      System.exit(e.getErrorCode().getReturnValue());
    }
  }


  /**
   * configure logging get the location and name of the log file via the passed program arguments
   * (this one is mandatory). The logfile and loglevel are passed to the PnLoggingUtils.
   *
   * <p>Two types of logging appenders are added. One is for ConsoleAppender and the other is
   * FileAppender. Incase 'LogFile' parameter is not used, then it falls back to ConsoleAppender
   * logging.</p>
   *
   * @param aCommandLineArguments
   */
  private void configureLogging(CommandLineArguments aCommandLineArguments)
  {
    boolean safeToLogToFile = true;
    String logFile = aCommandLineArguments.getArgument(IPDMCommandLine.CMDLN_PARAM_LOG_FILE);
    safeToLogToFile = (logFile != null) ? true : false;
    String logLevel = aCommandLineArguments.getArgument(IPDMCommandLine.CMDLN_PARAM_LOG_LEVEL, DEFAULT_LOG_LEVEL).toUpperCase();
    if (logFile != null)
    {
      Path path = Paths.get(logFile);
      try
      {
        Path parent = path.getParent();
        if (parent != null)
        {
          Files.createDirectories(parent);
        }

        File logFileCheck = path.toFile();
        if (logFileCheck.isDirectory())
        {
          if (LOG.isWarnEnabled())
          {
            LOG.warn("The 'LogFile' parameter value is a directory. Will continue, but will log to the console.");
          }
          safeToLogToFile = false;
        }
        if (!logFileCheck.isFile())
        {
          logFileCheck.createNewFile();
        }
      }
      catch (IOException ioE)
      {
        if (LOG.isWarnEnabled())
        {
          LOG.warn("Failed to create log file. Will continue, but log to the console.");
        }

        safeToLogToFile = false;
      }
    }
    String fileAppender = (safeToLogToFile) ? ", FileAppender" : "";
    Properties properties = new Properties();
    properties.put("log4j.rootLogger", logLevel + ", CA" + fileAppender);
    properties.put("log4j.logger.pnlog", logLevel + ", CA" + fileAppender);
    properties.put("log4j.additivity.pnlog", "false");

    properties.put("log4j.appender.CA", "org.apache.log4j.ConsoleAppender");
    properties.put("log4j.appender.CA.layout", "org.apache.log4j.PatternLayout");
    properties.put("log4j.appender.CA.layout.ConversionPattern", "%d [%t] %-5p %c - %m%n");
    if (safeToLogToFile)
    {
      properties.put("log4j.appender.CA.Threshold", "ERROR");
    }
    properties.put("log4j.appender.CA.Target", "System.err");

    if (safeToLogToFile)
    {
      properties.put("log4j.appender.FileAppender", "org.apache.log4j.FileAppender");
      properties.put("log4j.appender.FileAppender.File", logFile);
      properties.put("log4j.appender.FileAppender.layout", "org.apache.log4j.PatternLayout");
      properties.put("log4j.appender.FileAppender.layout.ConversionPattern", "%d [%t] %-5p %c - %m%n");
    }
    PnLoggingConfigurator.configure(properties);
  }


  /**
   * DOCUMENT ME!
   */
  private void configureStartupLogging()
  {
    URL stream = this.getClass().getClassLoader().getResource("pnlogging.properties");
    PnLoggingConfigurator.configure(stream);
  }


  /**
   * execute specified command
   *
   * @param  aArgs contains command line arguments
   *
   * @throws ProgramExecutionException DOCUMENT ME!
   */
  private void execute(String[] aArgs)
  {
    try
    {
      configureStartupLogging();

      this.commandLineArguments = new CommandLineArguments(aArgs);
      this.commandLineArguments.parse();

      configureLogging(this.commandLineArguments);

      CommandExecutorFactory commandExecutorFactory = new CommandExecutorFactory();

      BatchTool batchTool = new BatchTool(commandExecutorFactory);
      batchTool.execute(this.commandLineArguments);
    }
    catch (FailedToParseCommandlineException ex)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to parse the commandline commands and/or arguments. See the stacktrace for more information.", ex);
      }
      throw new ProgramExecutionException(ErrorCode.FAILURE);
    }
  }
}
