// Planon Enterprise Edition Source file: BOFinder.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.boinfo;

import java.util.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessmodel.eventlog.linked.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.proxy.comparator.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.util.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmrequest.proxyview.*;

import nl.planon.nyx.common.filter.*;
import nl.planon.nyx.server.codegenerator.exception.*;
import nl.planon.nyx.server.codegenerator.java.field.type.*;

import nl.planon.pdm.batchtool.*;
import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;

import nl.planon.util.pnlogging.*;

/**
 * BOFinder
 */
public class BOFinder
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(BOFinder.class);

  private static Map<String, SCOperatorType> operatorConversionMap = new HashMap<String, SCOperatorType>();

  static
  {
    operatorConversionMap.put(FilterOperator.EQUALS, SCOperatorType.EQUAL);
    operatorConversionMap.put(FilterOperator.LESS_EQUAL, SCOperatorType.LESS_EQUAL);
    operatorConversionMap.put(FilterOperator.GREATER_EQUAL, SCOperatorType.GREATER_EQUAL);
    operatorConversionMap.put(FilterOperator.STRICT_GREATER, SCOperatorType.GREATER);
    operatorConversionMap.put(FilterOperator.STRICT_LESS, SCOperatorType.LESS);
    operatorConversionMap.put(FilterOperator.STARTS_WITH, SCOperatorType.STARTINGWITH);
    operatorConversionMap.put(FilterOperator.ENDS_WITH, SCOperatorType.ENDINGWITH);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * read the bovalue of the pdm database with the specified name. If not found, the application
   * fails with exit code 2 and a proper error message.
   *
   * @param  aPDMDatabaseName database name to match
   *
   * @return bovalue of the pdm database with the matching name
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws BOMExecutionException
   * @throws ProgramExecutionException DOCUMENT ME!
   */
  public IBOValue getBOValuePDMDatabase(String aPDMDatabaseName) throws PnErrorListException, AuthorizationException, BOMExecutionException
  {
    LOG.info("Database name:  " + aPDMDatabaseName);
    IBOValue boPDMDatabase = getMatchingBOValue(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_NAME, aPDMDatabaseName);
    //check if PDM database exist
    if (boPDMDatabase == null)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("No database found with name '" + aPDMDatabaseName + "'");
      }
      throw new ProgramExecutionException(ErrorCode.FAILURE_DUPLICATE_DATABASE_NAME);
    }
    return boPDMDatabase;
  }


  /**
   * return all bo values of a specific bo type that comply to the field filter
   *
   * @param  aBOType      bo type of which all bo values must be returned
   * @param  aFieldFilter field filter to which the bo values must comply
   *
   * @return list of all bo values found
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   * @throws BOMExecutionException
   */
  public List<IBOValue> getBOValues(IBOType aBOType, FieldFilter[] aFieldFilter) throws AuthorizationException, PnErrorListException, BOMExecutionException
  {
    // get all primary keys of the bo's of type aBOType that comply to the field filter
    List<Integer> primaryKeys = getPKsOfMatchingBOs(aBOType, aFieldFilter);
    if (primaryKeys.size() == 0)
    {
      return null;
    }
    List<IBOValue> search = getBOValues(aBOType, primaryKeys);
    return search;
  }


  /**
   * get the eventlog corresponding to the latest executed request of the pdmdatabase, specified by
   * its code
   *
   * @param  aPKPDMDatabase the pdm database code
   *
   * @return the logmessage of the requestlog of the latest executed request
   *
   * @throws PnErrorListException
   * @throws FieldNotFoundException
   */
  public String getLog(IBaseValue aPKPDMDatabase) throws PnErrorListException, FieldNotFoundException
  {
    StringBuilder sb = new StringBuilder();
    IBaseValue pkCurrentPDMRequest = getPrimaryKeyCurrentPDMRequest(aPKPDMDatabase);

    IFOSearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().createPVAdvancedSearchCriteria(PVTypePDMP5Module.PDM_LATEST_EVENTLOGS);
    sc.getOperatorEx(LatestEventLogsForPDMDatabaseViewDefinition.PN_PDMDATABASE_PK, SCOperatorType.EQUAL).addBaseValue(aPKPDMDatabase);
    sc.getOperatorEx(LatestEventLogsForPDMDatabaseViewDefinition.PN_REQUEST_PK, SCOperatorType.EQUAL).addBaseValue(pkCurrentPDMRequest);
    IProxyListValue boValuesEventLogs = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(sc);

    for (IProxyValue eventLog : boValuesEventLogs.getProxyValues())
    {
      int columnNumberByAliasName = eventLog.getColumnNumberByAliasName(BOLinkedEventLogDef.PN_LOGMESSAGE);
      sb.append(System.lineSeparator());
      sb.append(eventLog.getProxyItem(columnNumberByAliasName).getAsString());
    }
    
    if(sb.length() == 0 && LOG.isInfoEnabled()) {
    	LOG.info("No event logs retrieved and hence not showing any.");
    }    
    
    return sb.toString();
  }


  /**
   * checks whether a value corresponding to each more fields possible!
   *
   * @param  aBOType     database!
   * @param  aFieldName  Field to compare!
   * @param  aFieldValue matched field with aField!
   *
   * @return List with matching IBOValues!
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   * @throws BOMExecutionException
   */
  public IBOValue getMatchingBOValue(IBOType aBOType, String aFieldName, String aFieldValue) throws AuthorizationException, PnErrorListException, BOMExecutionException
  {
    List<IBOValue> boValueList = getBOValues(aBOType, new FieldFilter[] {new FieldFilter(aFieldName, FilterOperator.EQUALS, aFieldValue)});
    if (boValueList != null)
    {
      for (IBOValue boValue : boValueList)
      {
        if (aFieldValue.equalsIgnoreCase(boValue.getFieldByPnNameEx(aFieldName).getAsString()))
        {
          return boValue;
        }
      }
    }
    return null;
  }


  /**
   * get the primary key of the last started request of the specified database
   *
   * @param  aPKPDMDatabase primary key of database
   *
   * @return the primary key of the last started request
   *
   * @throws FieldNotFoundException
   * @throws PnErrorListException
   */
  public IBaseValue getPrimaryKeyCurrentPDMRequest(IBaseValue aPKPDMDatabase) throws FieldNotFoundException, PnErrorListException
  {
    IFOSearchCriteria scPDMRequests = SearchCriteriaFactoryResourceLocator.getInstance().createPVAdvancedSearchCriteria(PVTypePDMP5Module.PDMREQUEST_INCONTEXT_DATABASE);
    scPDMRequests.getOperatorEx(IBOBasePDMRequestDef.PN_DATABASE_REF, SCOperatorType.EQUAL).addBaseValue(aPKPDMDatabase);

    IProxyListValue pdmRequestPLV = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(scPDMRequests);

    if (!pdmRequestPLV.getProxyValues().isEmpty())
    {
      int posPrimaryKey = pdmRequestPLV.getColumnNumberByAliasName(IBOBasePDMRequestDef.PN_PRIMARYKEY);

      // sort the proxy values in order to get the primary key of the latest added request.
      Comparator comparator = ProxyComparatorFactoryClient.getInstance().getComparator(pdmRequestPLV, SortColumnInfo.sortOn(SortDirection.DESCENDING, posPrimaryKey));
      pdmRequestPLV.sort(comparator);

      return pdmRequestPLV.getFirstProxy().getPrimaryKeyValue();
    }
    return new BaseIntegerValue(0);
  }


  /**
   * Helper method to convert the given filter operator to a search criteria operator.
   *
   * <p>This conversion is necessary since SCOperatorTypes should not be exposed to users of
   * webservices as-is.</p>
   *
   * @param  aFilterOperator the string representation of the filter operator to convert. See
   *                         #FilterOperator for the available operators.
   *
   * @return a SCOperatorType equivalent to the given filter operator.
   *
   * @throws RuntimeException in case of unhandled/unknown filter operators.
   */
  private SCOperatorType convertFilterOperatorToSCOperatorType(final String aFilterOperator)
  {
    SCOperatorType result = operatorConversionMap.get(aFilterOperator);
    if (result == null)
    {
      throw new RuntimeException("Internal error: unhandled FilterOperator: " + aFilterOperator);
    }
    return result;
  }


  /**
   * Helper method to convert the filter value of a given field filter to the correct Java type.
   *
   * @param  aFieldFilter  the field filter to convert its field value of;
   * @param  aBODefinition the BO-definition to get the corresponding BO-field from;
   *
   * @return the object representation for the field filter value, can be <code>null</code>.
   *
   * @throws PnErrorListException in case of business errors;
   * @throws InputParseException  in case the given field filter value could not be correctly be
   *                              converted to an object representation, can have various reasons.
   */
  private Object convertToCorrectFieldType(final FieldFilter aFieldFilter, final IBODefinitionValue aBODefinition) throws PnErrorListException, InputParseException
  {
    assert aFieldFilter != null : "Invalid argument: field filter cannot be null!";
    assert aBODefinition != null : "Invalid argument: BO definition cannot be null!";

    Object result = aFieldFilter.getFilterValue();

    if (result != null)
    {
      final String fieldPnName = aFieldFilter.getFieldName();

      // In case the user is throwing rubbish at us, we need to be fail-safe...
      final IFieldDefinitionValue fieldDef = aBODefinition.getFieldDefByPnName(fieldPnName);

      if (fieldDef != null)
      {
        final FieldTypeMappingRegistry fieldTypeMapping = FieldTypeMappingRegistry.getInstance();

        if (fieldTypeMapping.hasMappingFor(fieldDef))
        {
          result = fieldTypeMapping.valueOf(fieldDef, aFieldFilter.getFilterValue());
        }
      }
    }

    return result;
  }


  /**
   * Returns the list of all BOValues that are specified by the type and using the primary keys that
   * we passed.
   *
   * @param  aBOType Type of the BO for which we read the BOValues
   * @param  aPKs    The Primary keys of the BOs to be read for BOValue
   *
   * @return list of all bo values found
   *
   * @throws BOMExecutionException
   * @throws BOMNotFoundException
   * @throws PnErrorListException
   */
  private List<IBOValue> getBOValues(IBOType aBOType, List<Integer> aPKs) throws BOMExecutionException, BOMNotFoundException, PnErrorListException
  {
    // Get the read BOM
    BOMExecuter bomExecuter = new BOMExecuter();
    final IBOM bom = bomExecuter.getBOM(aBOType, PnBOMName.PN_BOM_READ);

    // for each primary key found read the corresponding BO value
    List<IBOValue> search = new ArrayList<IBOValue>();
    for (Integer primaryKey : aPKs)
    {
      search.add(bomExecuter.internalExecuteBOM(bom, new BaseIntegerValue(primaryKey)));
    }
    return search;
  }


  /**
   * get primary keys of bo's of type aRequestedBOType that comply to the field filters specified in
   * aFilterList
   *
   * @param  aRequestedBOType type of BO of which the primary keys must be retreived
   * @param  aFilterList      field filters to which the bo's must comply to
   *
   * @return a list of primary keys of the matching bo's
   *
   * @throws PnErrorListException
   * @throws FieldNotFoundException
   */
  private final List<Integer> getPKsOfMatchingBOs(final IBOType aRequestedBOType, final FieldFilter[] aFilterList) throws PnErrorListException, FieldNotFoundException
  {
    List<Integer> result = new ArrayList<Integer>();

    if (aFilterList != null)
    {
      final IBODefinitionValue boDefinition = DefinitionManagerResourceLocator.getInstance().getBODefinitionValue(aRequestedBOType);

      // Create a BO-filter search criteria; *warning* this is quite a costly operation!
      final ISearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().createBOFilterSearchCriteria(aRequestedBOType);

      // Fill the given filter options (= user search criteria)...
      for (final FieldFilter fieldFilter : aFilterList)
      {
        try
        {
          final SCOperatorType operatorType = convertFilterOperatorToSCOperatorType(fieldFilter.getOperator());
          final Object filterValue = convertToCorrectFieldType(fieldFilter, boDefinition);

          sc.getOperatorEx(fieldFilter.getFieldName(), operatorType).addValue(filterValue);
        }
        catch (InputParseException exception)
        {
        }
        catch (FieldNotFoundException exception)
        {
          throw new FieldNotFoundException(aRequestedBOType, fieldFilter.getFieldName());
        }
      }

      // Execute the query, and return the result as list of matching primary keys...
      final IProxyListValue plv = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(sc);
      final List<IProxyValue> proxyValues = plv.getProxyValues();
      final int resultSize = proxyValues.size();

      for (int i = 0; i < resultSize; i++)
      {
        result.add(proxyValues.get(i).getPrimaryKeyValue().getAsInteger());
      }
    }

    return result;
  }
}
