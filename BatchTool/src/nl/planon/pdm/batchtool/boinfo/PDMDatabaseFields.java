// Planon Enterprise Edition Source file: PDMDatabaseFields.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.boinfo;

import java.util.*;

import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;

import nl.planon.pdm.batchtool.*;
import nl.planon.pdm.batchtool.exceptions.*;

/**
 * PDMDatabaseFields
 */
public class PDMDatabaseFields
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private final Map<String, String> defaultFieldValuesMap = new HashMap<String, String>();
  private final Map<String, Boolean> fieldsMap = new HashMap<String, Boolean>();

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseFields object and, if there is a connection with the database, it also
   * per field sets the mandatory indicator and the default value.
   *
   * @param  aConnectedToDatabase true if there is a db connection, false otherwise
   *
   * @throws PnErrorListException
   * @throws AuthorizationException FieldNotFoundException
   * @throws BOMExecutionException
   */
  public PDMDatabaseFields(boolean aConnectedToDatabase) throws PnErrorListException, AuthorizationException, BOMExecutionException
  {
    if (aConnectedToDatabase)
    {
      this.setMandatoryFields();
      this.setDefaultFieldValues();
    }
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * return the fields with their default values
   *
   * @return Map<String,String>
   */
  public Map<String, String> getDefaultFieldValuesMap()
  {
    return this.defaultFieldValuesMap;
  }


  /**
   * return the fields with mandatory indicator
   *
   * @return Map<String, Boolean>
   */
  public Map<String, Boolean> getFieldsMap()
  {
    return this.fieldsMap;
  }


  /**
   * return a list of mandatory fields
   *
   * @return List<String>
   */
  public List<String> getMandatoryFields()
  {
    List<String> mandatoryFields = new ArrayList<String>();

    for (Map.Entry<String, Boolean> field : this.fieldsMap.entrySet())
    {
      if (Boolean.TRUE.equals(field.getValue()))
      {
        mandatoryFields.add(field.getKey());
      }
    }
    return mandatoryFields;
  }


  /**
   * Set the default Values for all fields of the PDM_Database.
   *
   * @throws FieldNotFoundException
   * @throws BOMExecutionException
   */
  public void setDefaultFieldValues() throws FieldNotFoundException, BOMExecutionException
  {
    IBOMExecuter bomExecuter = new BOMExecuter();
    IBOValue boPdmDatabase = bomExecuter.executeAddBomTimeBoxed(BOTypePDMP5Module.PDM_DATABASE);
    if (boPdmDatabase != null)
    {
      for (Map.Entry<String, Boolean> field : this.fieldsMap.entrySet())
      {
        String fieldName = field.getKey();
        String defaultValue = null;

        if ((IBOPDMDatabaseDef.PN_OPTION_NO_IMPORT.equals(fieldName)) || (IBOPDMDatabaseDef.PN_STOP_ON_FAILURE.equals(fieldName)))
        {
          defaultValue = Boolean.FALSE.toString();
        }
        else
        {
          defaultValue = boPdmDatabase.getFieldByPnNameEx(fieldName).getAsString();
        }

        if (defaultValue != null)
        {
          this.defaultFieldValuesMap.put(fieldName, defaultValue);
        }
      }
    }
  }


  /**
   * Collect the fields of BO PDMDatabase. Loop through all fields of BOPDMDatabase and put them in
   * a map with an indicator whether or not this field is mandatory. Read-only fields are skipped.
   *
   * @throws PnErrorListException
   */
  public void setMandatoryFields() throws PnErrorListException
  {
    IBODefinitionValue boDefPDMDatabase = DefinitionManagerResourceLocator.getInstance().getBODefinitionValue(BOTypePDMP5Module.PDM_DATABASE);
    Iterator<IFieldDefinitionValue> defIterator = boDefPDMDatabase.getFieldDefIterator();
    while (defIterator.hasNext())
    {
      IFieldDefinitionValue fieldDefinitionValue = defIterator.next();
      String pnName = fieldDefinitionValue.getPnName();
      if (IBOPDMDatabaseDef.PN_NAME.equals(pnName))
      {
        continue; //hard code with different name...
      }
      if (IBOPDMDatabaseDef.PN_DUMPFILE.equals(pnName))
      {
        continue; //hard code with different name...
      }
      if (IBOPDMDatabaseDef.PN_TASK_TIMEOUT.equals(pnName))
      {
        continue; //hard code with different name...
      }
      if (IBOPDMDatabaseDef.PN_PDM_EXPORTFILE.equals(pnName))
      {
        continue; //never required
      }
      if (IBOPDMDatabaseDef.PN_PDM_DUMPFILE.equals(pnName))
      {
        continue; //never required
      }
      if (IBOPDMDatabaseDef.PN_SIZE_DATABASE.equals(pnName))
      {
        continue; //never required
      }

      // location not mandatory in the batch, the location will be retrieved from the database server.
      if (IBOPDMDatabaseDef.PN_PDMLOCATION_REF.equals(pnName))
      {
        this.fieldsMap.put(pnName.toString(), Boolean.FALSE);
        continue;
      }

      // Skip the fields that are read-only
      if (fieldDefinitionValue.isReadOnly())
      {
        continue;
      }
      // add the fieldname to the list and whether or not the field is mandatory
      this.fieldsMap.put(pnName.toString(), new Boolean(fieldDefinitionValue.isMandatory()));
    }
  }
}
