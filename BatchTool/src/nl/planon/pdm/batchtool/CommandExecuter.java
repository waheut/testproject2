// Planon Enterprise Edition Source file: CommandExecuter.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.batchtool;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;
import nl.planon.hades.proxy.*;
import nl.planon.hades.searchcriteria.*;
import nl.planon.hades.searchcriteria.operator.*;
import nl.planon.hades.valueobject.*;

import nl.planon.morpheus.pdm.common.*;
import nl.planon.morpheus.pdm.common.businessmodel.*;
import nl.planon.morpheus.pdm.common.type.*;
import nl.planon.morpheus.pdm.server.businessmodel.pdmdatabasetype.*;

import nl.planon.pdm.batchtool.boinfo.*;
import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;
import nl.planon.pdm.core.client.*;
import nl.planon.pdm.core.common.*;

import nl.planon.util.pnlogging.*;

/**
 * all the executers for all batch commands.
 */
class CommandExecuter implements ICommandExecutor
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String FIELD_TYPE_BOOLEAN = "Boolean";
  private static final String FIELD_TYPE_STRING = "String";
  private static final String FIELD_TYPE_INTEGER = "Integer";
  private static final Object FIELD_TYPE_ACCOUNT = "Account";

  private static final PnLogger LOG = PnLogger.getLogger(CommandExecuter.class);
  private static final int PROCESS_TIME_IN_SECONDS = 20 * 60; // 20 minutes time

  //~ Instance Variables ---------------------------------------------------------------------------

  private final BOFinder boFinder;

  private final CommandLineArguments commandLineArguments;
  private final IBOMExecuter bomExecuter;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CommandExecuter object.
   *
   * @param aCommandLineArguments DOCUMENT ME!
   */
  public CommandExecuter(CommandLineArguments aCommandLineArguments)
  {
    this.commandLineArguments = aCommandLineArguments;
    this.boFinder = new BOFinder();
    this.bomExecuter = new BOMExecuter();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * This method first adds a Database, it fills in the values specified via the command line
   * arguments. Then it will try to save the database. If saving fails, because, for example, the bo
   * is under construction, it will repeatedly retry to save with a certain interval (specified by
   * IPDMCommandLine.SLEEP_TIME) until it succeeds or the number of retries (specified by
   * IPDMCommandLine.MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB) is exceeded. Same goes for the case in which
   * the webdav (for uploading the dumpfile to the fileserver) is not available.
   *
   * <p>if the AddOnly argument is set to false (is default), then it will be imported directly
   * after the add.</p>
   *
   * @throws CommandExecutionException DOCUMENT ME!
   */
  @Override public void addAndImportDB() throws CommandExecutionException
  {
    IBOValue boDatabase = null;
    try
    {
      String databaseName = this.commandLineArguments.getDatabaseName();
      int timeOut = this.commandLineArguments.getTimeOut(PDMCommand.ADDIMPORT);

      // check that PDM database name does not already exist
      checkUniquenessDatabaseName(databaseName);

      String databaseServerCode = this.commandLineArguments.getDatabaseServerCode();
      String locationLookupCode = getLookupLocationRef(databaseServerCode);
      String privilegeLookupCode = this.commandLineArguments.getPrivilegeTypeCode(PDMCommand.ADDIMPORT);

      String dumpFilePath = this.commandLineArguments.getImportDumpfile();
      File file = Paths.get(dumpFilePath).toFile();
      if (!file.isFile())
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to load the given dump file location: '" + dumpFilePath + "' because it does not exist.");
        }
        throw new CommandExecutionException(IBOPDMDatabaseDef.PN_DUMPFILE + ": " + dumpFilePath + ", " + IPDMCommandLine.DOES_NOT_EXIST);
      }

      LOG.info("going to create new database BO ..");
      boDatabase = this.bomExecuter.executeAddBomTimeBoxed(BOTypePDMP5Module.PDM_DATABASE);
      LOG.info("New database BO created (not saved yet)");

      //set fields
      setDatabaseFieldFromCommandLineArgumentAsString(boDatabase, IBOPDMDatabaseDef.PN_NAME, databaseName);
      setDatabaseFieldFromCommandLineArgumentAsString(boDatabase, IBOPDMDatabaseDef.PN_DUMPFILE, dumpFilePath);
      setDatabaseLookupField(boDatabase, IBOPDMDatabaseDef.PN_DATABASESERVER_REF, databaseServerCode);
      setDatabaseLookupField(boDatabase, IBOPDMDatabaseDef.PN_PDMLOCATION_REF, locationLookupCode);
      setDatabaseFieldAsInteger(boDatabase, IBOPDMDatabaseDef.PN_TASK_TIMEOUT, timeOut);
      setDatabaseLookupField(boDatabase, IBOPDMDatabaseDef.PN_PRIVILEGE_TYPE, privilegeLookupCode);

      for (CommandLineNameValueDefinition item : PDMCommand.ADDIMPORT.getBOItems())
      {
        String itemName = item.getName();
        if (this.commandLineArguments.isArgument(itemName))
        {
          if (IBOPDMDatabaseDef.PN_DATABASESERVER_REF.equals(itemName) //
            || IBOPDMDatabaseDef.PN_PDMLOCATION_REF.equals(itemName) //
            || IBOPDMDatabaseDef.PN_TASK_TIMEOUT.equals(itemName) //
            || IBOPDMDatabaseDef.PN_NAME.equals(itemName) //
            || IBOPDMDatabaseDef.PN_DUMPFILE.equals(itemName) //
            || IBOPDMDatabaseDef.PN_PRIVILEGE_TYPE.equals(itemName))
          {
            // ignore already set
            continue;
          }
          IBODefinitionValue boDefinitionValue = BODefinitionValueFactory.getInstance().getProvider().getBODefinitionValue(BOTypePDMP5Module.PDM_DATABASE);
          String fieldTypeName = boDefinitionValue.getFieldDefByPnName(itemName).getFieldType().getPnName();
          if (FIELD_TYPE_INTEGER.equals(fieldTypeName))
          {
            int value = this.commandLineArguments.getArgumentAsInt(itemName);
            setDatabaseFieldAsInteger(boDatabase, IBOPDMDatabaseDef.PN_NAME, value);
          }
          else if (FIELD_TYPE_STRING.equals(fieldTypeName))
          {
            setDatabaseFieldFromCommandLineArgumentAsString(boDatabase, itemName);
          }
          else if (FIELD_TYPE_BOOLEAN.equals(fieldTypeName))
          {
            setDatabaseFieldFromCommandLineArgumentAsBoolean(boDatabase, itemName);
          }
          else if (FIELD_TYPE_ACCOUNT.equals(fieldTypeName))
          {
            setDatabaseLookupFieldFromCommandLineArgument(boDatabase, itemName);
          }
          else if (FieldTypePDMP5Module.PDM_DATABASE_TYPE.getPnName().equals(fieldTypeName))
          {
            setDatabaseLookupFieldFromCommandLineArgument(boDatabase, itemName);
          }
          else if (FieldTypePDMP5Module.PDM_DATABASE_FILE_TYPE.getPnName().equals(fieldTypeName))
          {
            setDatabaseLookupFieldFromCommandLineArgument(boDatabase, itemName);
          }
          else
          {
            throw new CommandExecutionException("Field: '" + itemName + "' Unsupported field type: '" + fieldTypeName + "'.");
          }
        }
      }

      LOG.info("going to save created database BO ..");
      boDatabase = this.bomExecuter.executeSaveBom(boDatabase);
      LOG.info("New database BO save");

      // if db agents are free and running this request will be picked up
      IBOValue locationCode = this.boFinder.getMatchingBOValue(BOTypePDMP5Module.PDM_LOCATION, IBOPDMLocationDef.PN_CODE, locationLookupCode);
      String webdavLocation = locationCode.getFieldByPnNameEx(IBOPDMLocationDef.PN_LOCATION).getAsString();
      String dumpFileName = WebDAVUtilities.getInstance().generateDumpFileName(boDatabase);

      LOG.info(IBOPDMDatabaseDef.PN_PDM_DUMPFILE + ": " + webdavLocation + dumpFileName);
      boDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).setAsObject(webdavLocation + dumpFileName);

      LOG.info("going to upload to location " + webdavLocation + "  ..");
      boDatabase = this.bomExecuter.executeUploadFileTimeBoxed(boDatabase, webdavLocation);
      LOG.info("Database uploaded to " + webdavLocation);

      LOG.info("Using privileges type:  " + PDMPrivilegeType.getPDMPrivilegeTypeByCode(privilegeLookupCode).getName());
      boolean addOnly = this.commandLineArguments.getAddOnly();
      LOG.info("add without import: " + (addOnly ? "yes" : "no"));

      if (!addOnly)
      {
        LOG.info("going to import database named " + databaseName + "  ..");
        boolean importOk = this.bomExecuter.executeTimeBoxed(boDatabase, IBOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE, IBOPDMDatabaseDef.STATE_INITIAL, "Import");
        LOG.info("Import database " + (importOk ? "pending" : "failed"));
        if (!importOk && LOG.isErrorEnabled())
        {
          LOG.error("Failed to import the database. The BO never reached the initial state.");
        }

        LOG.info("going to wait max " + timeOut + " second for import to finish  ..");
        if (this.bomExecuter.waitForRequestToFinish(boDatabase.getPrimaryKeyValue(), timeOut + PROCESS_TIME_IN_SECONDS))
        {
          LOG.info("import succeeded, log = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
        }
        else
        {
          if (LOG.isErrorEnabled())
          {
            LOG.error("Failed to import database. The BO nver reached the OKE state.");
          }
          LOG.info("import failed, log = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
          boDatabase = this.bomExecuter.read(boDatabase); // get updated state

          LOG.info("going to delete (failed) database " + databaseName + " ..");
          boolean deleteOk = this.bomExecuter.executeTimeBoxed(boDatabase, IBOPDMDatabaseDef.PN_BOM_DELETE, IBOPDMDatabaseDef.STATE_INITIAL, "Delete");
          LOG.warn("database delete " + (deleteOk ? "succeeded" : "failed"));

          throw new CommandExecutionException("Could not import. See logging for more information.");
        }
      }
    }
    catch (DatabaseNameNotUniqueException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Unable to import the database. The database name is not unique.", e);
      }
      throw new CommandExecutionException("Failed to import database. See stacktrace for more information.", e);
    }
    catch (PnErrorListException | BOMExecutionException e)
    {
      if (boDatabase != null)
      {
        if (LOG.isInfoEnabled())
        {
          try
          {
            LOG.info("Failed to import database. See the stacktrace for the actual cause. We made a last effort to fetch the logging from the remote. This may not be complete! The logging was: " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()), e);
          }
          catch (FieldNotFoundException | PnErrorListException ex)
          {
            if (LOG.isErrorEnabled())
            {
              LOG.error("Failed to import the database. See the stacktrace for the actual cause. We made a last effort to fetch the logging from the remote. THIS FAILED!.", e);
            }
          }
        }
      }
      else
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("Failed to import the database. See the stacktrace for the actual cause. The bodatabase field has never been set. So we can not make a last effort to fetch logging.", e);
        }
      }

      throw new CommandExecutionException("Failed to import database. See the logging for more information, and the stacktrace for the actual cause.", e);
    }
    catch (AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to import database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Failed to import database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
    }
  }


  /**
   * Create a oracle/mssql Planon-ds.xml get arguments from Argumentsparser
   *
   * @param  aPDMCommand aInfo Planon Ds Oracle or Mssql
   *
   * @throws CommandExecutionException AuthorizationException
   */
  @Override public void createPlanonDS(PDMCommand aPDMCommand) throws CommandExecutionException
  {
    try
    {
      String databaseName = this.commandLineArguments.getDatabaseName();
      String dsFileName = this.commandLineArguments.getPlanonDSFileName();

      long start = System.currentTimeMillis();
      IBOValue boPDMDatabase = this.boFinder.getBOValuePDMDatabase(databaseName);
      String dataSource = boPDMDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_XML_DATASOURCE_EXAMPLE).getAsString();
      long end = System.currentTimeMillis();

      if (LOG.isInfoEnabled())
      {
        LOG.info("Retrieval of datasource string from database took " + ((end - start) / 1000f) + " seconds");
      }

      start = System.currentTimeMillis();

      Path file = Paths.get(dsFileName);
      Files.createDirectories(file.getParent());
      Files.write(file, dataSource.getBytes());

      end = System.currentTimeMillis();

      if (LOG.isInfoEnabled())
      {
        LOG.info("Writing into the file took " + ((end - start) / 1000f) + " seconds");
      }
    }
    catch (PnErrorListException | IOException | BOMExecutionException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to create a PlanonDS file. See the stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Failed to create a PlanonDS file. See the stacktrace for more information.", e);
    }
    catch (AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to create a PlanonDS file. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Failed to create a PlanonDS file. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
    }
  }


  /**
   * try to clean-up the database. if the clean-up finishes successfully then also the delete bom is
   * executed.
   *
   * @throws CommandExecutionException PnErrorListException
   */
  @Override public void deleteDB() throws CommandExecutionException
  {
    try
    {
      String databaseName = this.commandLineArguments.getDatabaseName();
      IBOValue boDatabase = this.boFinder.getBOValuePDMDatabase(databaseName);

      boolean cleanupPending = false;

      if (IBOPDMDatabaseDef.STATE_AVAILABLE.equals(boDatabase.getStateName()))
      {
        LOG.info("Going to drop tables of database...");
        cleanupPending = this.bomExecuter.executeTimeBoxed(boDatabase, IBOPDMDatabaseDef.PN_BOM_DELETE_DATABASE, IBOPDMDatabaseDef.STATE_AVAILABLE, "Clean-up");
        LOG.info("Database drop is " + (cleanupPending ? "pending..." : "Not ok"));
      }

      // if the clean-up is started we have to wait until it's finished
      if (cleanupPending)
      {
        final int timeOut = this.commandLineArguments.getTimeOut(PDMCommand.DELETE); // in seconds

        //thread timeout. temporarily timeout. pdm3 timout support only import and export on the moment!
        boolean timeoutOccured;
        try
        {
          final IBOValue boDatabaseToWaitFor = boDatabase;
          ExecutorService executor = Executors.newFixedThreadPool(1);
          Future<Boolean> timeout = executor.submit(new Callable<Boolean>()
            {
              @Override public Boolean call() throws Exception
              {
                try
                {
                  return Boolean.valueOf(CommandExecuter.this.bomExecuter.waitForRequestToFinish(boDatabaseToWaitFor.getPrimaryKeyValue(), timeOut));
                }
                catch (BOMExecutionException ex)
                {
                  if (LOG.isErrorEnabled())
                  {
                    LOG.error("Failed to wait till the request has finished. See stacktrace for more information.", ex);
                  }
                  return Boolean.FALSE;
                }
              }
            });

          executor.shutdown();
          boolean succesfulWait = executor.awaitTermination(timeOut, TimeUnit.SECONDS);
          executor.shutdownNow();

          if (succesfulWait)
          {
            timeoutOccured = !timeout.get().booleanValue();
          }
          else
          {
            timeoutOccured = false;
          }
        }
        catch (InterruptedException | ExecutionException e)
        {
          if (LOG.isInfoEnabled())
          {
            LOG.info("Failed to wait till the request finished. See stacktrace for more infomration.", e);
          }
          timeoutOccured = false;
        }

        if (!timeoutOccured)
        {
          if (this.bomExecuter.waitForRequestToFinish(boDatabase.getPrimaryKeyValue(), timeOut))
          {
            LOG.info("Pending cleanup has finished.");
            LOG.info("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
            boDatabase = this.boFinder.getMatchingBOValue(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_NAME, databaseName);
            // Adding two minutes wait time for the database to be returned to 'Initial' state.
            // Because only in initial state, it has the BomDelete.
            if (this.bomExecuter.waitForDatabaseToBeInExpectedState(boDatabase.getPrimaryKeyValue(), //
                IBOPDMDatabaseDef.STATE_INITIAL // Expected state
                ))
            {
              boDatabase = this.bomExecuter.read(boDatabase); // Read the bovalue again as there were state changes happened already.
              final IBOMListManager bomListManager = boDatabase.getBOMListManager();
              final IBOM deleteBOM = bomListManager.getDeleteBOMEx();

              cleanupFiles(boDatabase);

              LOG.info("Going to delete database BO ...");
              this.bomExecuter.internalExecuteBOM(deleteBOM);
              LOG.info("Delete = Ok");
            }
            else
            {
              if (LOG.isErrorEnabled())
              {
                LOG.error("Unable to find BomDelete on the database as it is not in initial state.");
              }
              throw new CommandExecutionException("Unable to find BomDelete on the database as it is not in initial state.");
            }
          }
          else
          {
            if (LOG.isErrorEnabled())
            {
              LOG.error("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
            }
            throw new CommandExecutionException("Failed to clean-up. See log for more information.");
          }
        }
        else
        {
          if (LOG.isErrorEnabled())
          {
            LOG.error("We failed to wait till the BOM moved to the OK state. This most likely means a timeout occured.");
            LOG.error("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
          }
          throw new CommandExecutionException("Could not clean-up. See logging for more information.");
        }
      }
      else
      {
        LOG.info("Going to delete database BO...");
        boolean deleted = this.bomExecuter.executeTimeBoxed(boDatabase, IBOPDMDatabaseDef.PN_BOM_DELETE, IBOPDMDatabaseDef.STATE_INITIAL, "Delete");
        LOG.info("Delete of BO =" + (deleted ? "Ok" : "Not ok"));
        if (deleted)
        {
          if (LOG.isInfoEnabled())
          {
            LOG.info("Succesfully deleted the database.");
          }
          cleanupFiles(boDatabase);
        }
        else
        {
          if (LOG.isErrorEnabled())
          {
            LOG.error("Failed to delete the database. This most likely means that a timout occured, and that the BOM never reached the INITIAL state.");
            LOG.error("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
          }
          throw new CommandExecutionException("Could not clean-up. See logging for more information.");
        }
      }
    }
    catch (PnErrorListException | CleanupException | BOMExecutionException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to delete the database. See the stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Unable to delete the database. See the stacktrace for more information.", e);
    }
    catch (AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to delete the database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Failed to delete the database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
    }
  }


  /**
   * Export a PDM Database. get his arguments from ArgumentParser
   *
   * @throws CommandExecutionException PnErrorListException
   */
  @Override public void exportDB() throws CommandExecutionException
  {
    try
    {
      String databaseName = this.commandLineArguments.getDatabaseName();
      int timeOut = this.commandLineArguments.getTimeOut(PDMCommand.EXPORT);
      String exportPath = this.commandLineArguments.getExportDumpFileName();
      String exportFileType = this.commandLineArguments.getExportFileType();

      Path exportFile = Paths.get(exportPath);
      Files.createDirectories(exportFile.getParent());

      // store time out and export file type in database BO
      IBOValue boDatabase = this.boFinder.getBOValuePDMDatabase(databaseName);
      setDatabaseFieldAsInteger(boDatabase, IBOPDMDatabaseDef.PN_TASK_TIMEOUT, timeOut);
      if (exportFileType != null)
      {
        setDatabaseLookupField(boDatabase, IBOPDMDatabaseDef.PN_EXPORT_FILE_TYPE, exportFileType);
      }

      boDatabase = this.bomExecuter.executeSaveBom(boDatabase);

      this.bomExecuter.executeTimeBoxed(boDatabase, IBOPDMDatabaseDef.PN_BOM_EXPORT_DATABASE, IBOPDMDatabaseDef.STATE_AVAILABLE, "Export");

      if (this.bomExecuter.waitForRequestToFinish(boDatabase.getPrimaryKeyValue(), timeOut + PROCESS_TIME_IN_SECONDS))
      {
        LOG.info("Database is exported successfully.");
        LOG.info("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));

        // Adding a one minute of wait time for the database to be returned to 'Available' state.
        // Because only in available state, it has the BomDownloadDumpfile.
        if (this.bomExecuter.waitForDatabaseToBeInExpectedState(boDatabase.getPrimaryKeyValue(), IBOPDMDatabaseDef.STATE_AVAILABLE))
        {
          boDatabase = this.bomExecuter.read(boDatabase); // Read the bovalue again as there were state changes happened already.

          // if export finish right, download Export is available
          this.bomExecuter.executeDownloadExport(boDatabase, exportPath);

          if (LOG.isInfoEnabled())
          {
            LOG.info("Exported dumpfile is downloaded successfully.");
          }
        }
        else
        {
          throw new CommandExecutionException("Download of the exported dump file is failed because the database is in busy state.");
        }
      }
      else
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to export the database. Most likely a timeout occured and the BO never reached the OK state.");
          LOG.error("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
        }
        throw new CommandExecutionException("Export failed.");
      }
    }
    catch (PnErrorListException | BOMExecutionException | IOException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to export the database. See the stacktrace for more information.", e);
      }

      throw new CommandExecutionException("Unable to export the database. See the stacktrace for more information.", e);
    }
    catch (AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to export the database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Failed to export the database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
    }
  }


  /**
   * Start a PDM Database import. Get his Arguments from ArgumentsParser
   *
   * @throws CommandExecutionException AuthorizationException
   */
  @Override public void importDB() throws CommandExecutionException
  {
    try
    {
      String databaseName = this.commandLineArguments.getDatabaseName();

      //set timeout value
      int timeOut = this.commandLineArguments.getTimeOut(PDMCommand.IMPORT);
      IBOValue boDatabase = this.boFinder.getBOValuePDMDatabase(databaseName);
      setDatabaseFieldAsInteger(boDatabase, IBOPDMDatabaseDef.PN_TASK_TIMEOUT, timeOut);
      boDatabase = this.bomExecuter.executeSaveBom(boDatabase);

      boolean result = this.bomExecuter.executeTimeBoxed(boDatabase, IBOPDMDatabaseDef.PN_BOM_IMPORT_DATABASE, IBOPDMDatabaseDef.STATE_INITIAL, "Import");
      if (!result && LOG.isWarnEnabled())
      {
        LOG.warn("Failed to move the BO to the initial state.");
      }

      if (LOG.isInfoEnabled())
      {
        LOG.info("Import = " + (result ? "Ok" : "Not ok"));
      }

      boolean statusAvailable = this.bomExecuter.waitForRequestToFinish(boDatabase.getPrimaryKeyValue(), timeOut + PROCESS_TIME_IN_SECONDS);

      if (statusAvailable)
      {
        if (LOG.isInfoEnabled())
        {
          LOG.info("Databsde is imported succesfully");
          LOG.info("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
        }
      }
      else
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to import the database. Most likely a timeout occured, and the BO never reached the OK state.");
          LOG.error("Getlog = " + this.boFinder.getLog(boDatabase.getPrimaryKeyValue()));
        }
        throw new CommandExecutionException("Could not import.");
      }
    }
    catch (PnErrorListException | BOMExecutionException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Unable to import the database. See the stacktrace for more information", e);
      }
      throw new CommandExecutionException("Unable to import the database. See the stacktrace for more information.", e);
    }
    catch (AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to import the database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Failed to import the database. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
    }
  }


  /**
   * Read all the PDM database names and print it. Optionally names are writen to OutputFileName
   *
   * @throws CommandExecutionException AuthorizationException
   */
  @Override public void reportDBNames() throws CommandExecutionException
  {
    try
    {
      IFOSearchCriteria sc = SearchCriteriaFactoryResourceLocator.getInstance().createBOFilterSearchCriteria(BOTypePDMP5Module.PDM_DATABASE);
      final IProxyListValue plv = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteria(sc);

      String outputFile = this.commandLineArguments.getOutputFileName();
      if (outputFile == null)
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to report the db names. The outputFile property is not set.");
        }
        throw new CommandExecutionException("Failed to report the db names, because the outputfile property is not set.");
      }

      try
      {
        Path path = Paths.get(outputFile);
        Files.createDirectories(path.getParent());

        try(BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8))
        {
          for (IProxyValue pdmDB : plv.getProxyValues())
          {
            String pnNameOfPDMDB = pdmDB.getProxyItem(pdmDB.getColumnNumberByAliasName(IBOPDMDatabaseDef.PN_NAME)).getAsString();
            System.out.println(IPDMCommandLine.INFO + "Name: " + pnNameOfPDMDB);
            writer.write(pnNameOfPDMDB);
            writer.newLine();
          }
        }
      }
      catch (IOException e)
      {
        if (LOG.isErrorEnabled())
        {
          LOG.error("Failed to report the database names. See the stacktrace for more information.", e);
        }
        throw new CommandExecutionException("Failed to report the database names. See the stacktrace for more information.", e);
      }
    }
    catch (PnErrorListException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Unable to report the database names. See the stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Unable to report the database names. See the stacktrace for more information.", e);
    }
  }


  /**
   * read Host name, database name, Port number and Databasetype values from a Specific PDM3 user!
   *
   * @throws CommandExecutionException AuthorizationException
   */
  @Override public void reportDBProperties() throws CommandExecutionException
  {
    try
    {
      String outputPropertyFile = this.commandLineArguments.getOutputPropertyFileName();

      Path file = Paths.get(outputPropertyFile);
      Files.createDirectories(file.getParent());

      String databaseName = this.commandLineArguments.getDatabaseName();
      IBOValue boDatabase = this.boFinder.getBOValuePDMDatabase(databaseName);

      IBaseValue pkDatabaseserver = boDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASESERVER_REF).getBaseValue();
      IBOValue boValuePdmDatabaseServer = getBOValueByPrimaryKey(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, pkDatabaseserver);
      IBaseValue pkDatabaseType = boDatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF).getBaseValue();
      IBOValue boValuePdmDatabaseType = getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_DATABASE_TYPE, pkDatabaseType);
      String code = boValuePdmDatabaseType.getFieldByPnNameEx(BOPDMDatabaseTypeDef.PN_CODE).getAsString();
      PDMDatabaseType pdmDatabaseType = PDMDatabaseType.getPDMDatabaseTypeByCode(code);
      String pdmDatabaseName = (pdmDatabaseType != null) ? pdmDatabaseType.getName() : "";

      // read the arguments and set it to a propertyfile
      Properties prop = new Properties();
      addProperty(prop, IBOPDMDatabaseDef.PN_NAME, databaseName);
      addProperty(prop, IBOPDMDatabaseDef.PN_ACCESSCODE, boDatabase);
      addProperty(prop, IBOPDMDatabaseDef.PN_DATABASE_TYPE_REF, pdmDatabaseName);
      addProperty(prop, IBOBasePDMDatabaseServerDef.PN_HOST_NAME, boValuePdmDatabaseServer);
      addProperty(prop, IBOPDMDatabaseDef.PN_DATABASESERVER_REF, boValuePdmDatabaseServer, IBOBasePDMDatabaseServerDef.PN_CODE);
      addProperty(prop, IBOBasePDMDatabaseServerDef.PN_DATABASE_INSTANCENAME, boValuePdmDatabaseServer);
      addProperty(prop, IBOBasePDMDatabaseServerDef.PN_CONNECTION_URL, boValuePdmDatabaseServer);
      if ((boValuePdmDatabaseServer != null) && BOTypePDMP5Module.PDM_DATABASE_SERVER_ORACLE.equals(boValuePdmDatabaseServer.getBOType()))
      {
        addProperty(prop, IBOBasePDMDatabaseServerDef.PN_PORT_NUMBER__NOT_IN_ALL_SUBTYPES, boValuePdmDatabaseServer);
      }

      //create a property file in the PROPERTY_FILE location
      prop.store(new FileOutputStream(outputPropertyFile), null);
      if (LOG.isInfoEnabled())
      {
        LOG.info("Reported the database properties of (" + databaseName + ") into the file: " + outputPropertyFile);
      }
    }
    catch (PnErrorListException | IOException | BOMExecutionException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Unable to report the database properties. See the stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Unable to report the database properties. See the stacktrace for more information.", e);
    }
    catch (AuthorizationException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Failed to report the database properties. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
      }
      throw new CommandExecutionException("Failed to report the database properties. Most likely the account is not authorized to execute actions. See stacktrace for more information.", e);
    }
  }


  /**
   * adds property
   *
   * @param  aProp     properties to put property in
   * @param  aPropName name to store value with
   * @param  aBOValue  value to store
   *
   * @throws AuthorizationException
   */
  private void addProperty(Properties aProp, String aPropName, IBOValue aBOValue) throws AuthorizationException
  {
    if ((aBOValue != null) && aBOValue.hasFieldByPnName(aPropName))
    {
      addProperty(aProp, aPropName, aBOValue.getFieldByPnNameEx(aPropName));
    }
    else
    {
      addProperty(aProp, aPropName, "");
    }
  }


  /**
   * adds property
   *
   * @param aProp     properties to put property in
   * @param aPropName name to store value with
   * @param aValue    value to store
   */
  private void addProperty(Properties aProp, String aPropName, String aValue)
  {
    aProp.setProperty(aPropName, aValue);
  }


  /**
   * adds property
   *
   * @param aProp     properties to put property in
   * @param aPropName name to store value with
   * @param aValue    value to store
   */
  private void addProperty(Properties aProp, String aPropName, IBOFieldValue aValue)
  {
    String value = "";
    if ((aValue != null) && !aValue.isEmpty())
    {
      value = aValue.getAsString();
    }

    addProperty(aProp, aPropName, value);
  }


  /**
   * adds property
   *
   * @param  aProp      properties to put property in
   * @param  aPropName  name to store value with
   * @param  aBOValue   value to store
   * @param  aFieldName pn name of field
   *
   * @throws AuthorizationException
   */
  private void addProperty(Properties aProp, String aPropName, IBOValue aBOValue, String aFieldName) throws AuthorizationException
  {
    if ((aBOValue != null) && aBOValue.hasFieldByPnName(aFieldName))
    {
      addProperty(aProp, aPropName, aBOValue.getFieldByPnNameEx(aFieldName));
    }
  }


  /**
   * Check if there already exists a database with the same code. If so, the application fails.
   *
   * @param  aPDMDatabaseName the database code to check
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   * @throws DatabaseNameNotUniqueException DOCUMENT ME!
   * @throws BOMExecutionException
   */
  private void checkUniquenessDatabaseName(String aPDMDatabaseName) throws AuthorizationException, PnErrorListException, DatabaseNameNotUniqueException, BOMExecutionException
  {
    if (this.boFinder.getMatchingBOValue(BOTypePDMP5Module.PDM_DATABASE, IBOPDMDatabaseDef.PN_NAME, aPDMDatabaseName) != null)
    {
      throw new DatabaseNameNotUniqueException(IBOPDMDatabaseDef.PN_NAME + "= " + aPDMDatabaseName + ", is not unique");
    }
  }


  /**
   * deletes all related files of specified database
   *
   * @param  aBODatabase database bo
   *
   * @throws AuthorizationException
   * @throws CleanupException       DOCUMENT ME!
   */
  private void cleanupFiles(IBOValue aBODatabase) throws AuthorizationException, CleanupException
  {
    LOG.info("Going to delete related files from file server...");

    WebDAVFileserverCleaner cleaner = new WebDAVFileserverCleaner();

    String dumpFilePath = aBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_DUMPFILE).getAsString();
    boolean dumpFileCleaned = cleaner.clean(dumpFilePath);

    String exportFilePath = aBODatabase.getFieldByPnNameEx(IBOPDMDatabaseDef.PN_PDM_EXPORTFILE).getAsString();
    boolean exportFileCleaned = cleaner.clean(exportFilePath);

    if (!dumpFileCleaned || !exportFileCleaned)
    {
      throw new CleanupException("Failed to delete the dumpFile or the exportFile form the server. dumpFile: (path: '" + dumpFilePath + "', deleted: '" + dumpFileCleaned + "'), exportFile: (path: '" + exportFilePath + "', deleted: '" + exportFileCleaned + "');");
    }

    if (LOG.isInfoEnabled())
    {
      LOG.info("Related files from file server deleted.");
    }
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aBOType     basePdmDatabaseServer DOCUMENT ME!
   * @param  aPrimarykey pkDatabaseserver      DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   *
   * @throws PnErrorListException   DOCUMENT ME!
   * @throws AuthorizationException DOCUMENT ME!
   */
  private IFOValue getBOValueByPrimaryKey(IBOType aBOType, IBaseValue aPrimarykey) throws PnErrorListException, AuthorizationException
  {
    return PDMUtilities.getInstance().getBOValueByPrimaryKey(aBOType, aPrimarykey);
  }


  /**
   * if the pdm location is not specified via the corresponding command line parameter, then the pdm
   * location of the database server is used (this database server is specified via a mandatory
   * command line parameter).
   *
   * @param  aDatabaseServerCode database server code
   *
   * @return DOCUMENT ME!
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   * @throws BOMExecutionException
   */
  private String getLookupLocationRef(String aDatabaseServerCode) throws PnErrorListException, AuthorizationException, BOMExecutionException
  {
    String code = this.commandLineArguments.getLocationCode();
    // only fill the location automatically, if the location is not specified via the corresponding command line parameter
    if ((code == null) && (aDatabaseServerCode != null))
    {
      // fill automatically the location, related to DatabaseServer.
      IBOValue databaseServer = this.boFinder.getMatchingBOValue(BOTypePDMP5Module.BASE_PDM_DATABASE_SERVER, IBOBasePDMDatabaseServerDef.PN_CODE, aDatabaseServerCode);
      IBaseValue pkPDMLocation = databaseServer.getFieldByPnNameEx(IBOBasePDMDatabaseServerDef.PN_PDMLOCATION_REF).getBaseValue();
      IBOValue locationValue = getBOValueByPrimaryKey(BOTypePDMP5Module.PDM_LOCATION, pkPDMLocation);

      code = locationValue.getFieldByPnNameEx(IBOPDMLocationDef.PN_CODE).getAsString();
    }
    return code;
  }


  /**
   * set a value, give a Planon name and set a result to Arguments parser. get the result from
   * argument parser and set in into the right value
   *
   * <p>boPdmDatabase must be filled!</p>
   *
   * @param  aDatabase DOCUMENT ME!
   * @param  aPnName
   * @param  aValue    aDefaultValue
   *
   * @throws PnErrorListException
   * @throws FieldNotFoundException
   */
  private void setDatabaseFieldAsInteger(IBOValue aDatabase, String aPnName, int aValue) throws PnErrorListException, FieldNotFoundException
  {
    LOG.info(aPnName + ": " + aValue);
    aDatabase.getFieldByPnNameEx(aPnName).setAsBaseValue(new BaseIntegerValue(aValue));
  }


  /**
   * set a value, give a Planon name and set a result to Arguments parser. get the result from
   * argument parser and set in into the right value
   *
   * @param  aDatabase DOCUMENT ME!
   * @param  aPnName
   *
   * @throws AuthorizationException
   * @throws PnErrorListException
   */
  private void setDatabaseFieldFromCommandLineArgumentAsBoolean(IBOValue aDatabase, String aPnName) throws AuthorizationException, PnErrorListException
  {
    if (this.commandLineArguments.isArgument(aPnName))
    {
      boolean parsedArgumentValue = this.commandLineArguments.getArgumentAsBoolean(aPnName);
      LOG.info(aPnName + ": " + parsedArgumentValue);
      aDatabase.getFieldByPnNameEx(aPnName).setAsBaseValue(BaseBooleanValue.valueOf(parsedArgumentValue));
    }
  }


  /**
   * set a value, give a Planon name and set a result to Arguments parser. get the result from
   * argument parser and set in into the right value
   *
   * <p>PnName must be the same as argumentsparser</p>
   *
   * @param  aDatabase DOCUMENT ME!
   * @param  aPnName   Planon Value name for import. get the value result from ArgumentsParser
   *
   * @throws PnErrorListException
   * @throws FieldNotFoundException
   */
  private void setDatabaseFieldFromCommandLineArgumentAsString(IBOValue aDatabase, String aPnName) throws PnErrorListException, FieldNotFoundException
  {
    String value = this.commandLineArguments.getArgument(aPnName);
    setDatabaseFieldFromCommandLineArgumentAsString(aDatabase, aPnName, value);
  }


  /**
   * set a value, give a Planon name and set a result to Arguments parser. get the result from
   * argument parser and set in into the right value
   *
   * <p>PnName must be the same as argumentsparser</p>
   *
   * @param  aDatabase DOCUMENT ME!
   * @param  aPnName   Planon Value name for import. get the value result from ArgumentsParser
   * @param  aValue    DOCUMENT ME!
   *
   * @throws PnErrorListException
   * @throws FieldNotFoundException
   */
  private void setDatabaseFieldFromCommandLineArgumentAsString(IBOValue aDatabase, String aPnName, String aValue) throws PnErrorListException, FieldNotFoundException
  {
    if (aValue != null)
    {
      LOG.info(aPnName + ": " + aValue);
      aDatabase.getFieldByPnNameEx(aPnName).setAsBaseValue(new BaseStringValue(aValue));
    }
  }


  /**
   * * sets value <code>aReferenceFieldName</code> in bo <code>aDatabase</code> to <code>
   * aValue</code>
   *
   * @param  aDatabase
   * @param  aReferenceFieldName
   * @param  aValue
   *
   * @throws FieldNotFoundException
   * @throws PnErrorListException
   * @throws CommandExecutionException DOCUMENT ME!
   */
  private void setDatabaseLookupField(IBOValue aDatabase, String aReferenceFieldName, String aValue) throws FieldNotFoundException, PnErrorListException, CommandExecutionException
  {
    if (aValue != null)
    {
      LOG.info(aReferenceFieldName + ": " + aValue);
      IBaseValue bvLookup = new BaseStringValue(aValue);

      BOFieldValue context = ValueFactory.createBOFieldValue((BaseValue) bvLookup);
      context.setParent(aDatabase);

      ILookupSearchCriteria searchCriteria = SearchCriteriaFactoryResourceLocator.getInstance().createBOLookupSearchCriteria(BOTypePDMP5Module.PDM_DATABASE, aReferenceFieldName);

      ISCOperator scOperator = searchCriteria.getLookupOperator();
      if (scOperator != null)
      {
        scOperator.addBaseValue(bvLookup);
      }

      IProxyListValue plv = ProxyListHandlerResourceLocator.getInstance().createBySearchCriteriaWithField(searchCriteria, BOTypePDMP5Module.PDM_DATABASE, context);

      if (plv.isEmpty())
      {
        //lookup not found
        throw new CommandExecutionException(aReferenceFieldName + ": " + aValue + " = invalid value");
      }
      IProxyValue proxyValue = plv.getFirstProxy();
      IBaseValue primaryKey = proxyValue.getPrimaryKeyValue();
      aDatabase.getFieldByPnNameEx(aReferenceFieldName).setAsBaseValue(primaryKey);
    }
  }


  /**
   * * sets value <code>aReferenceFieldName</code> in bo <code>aDatabase</code> to value of command
   * line parametyer <code>aReferenceFieldName</code>
   *
   * @param  aDatabase
   * @param  aReferenceFieldName
   *
   * @throws FieldNotFoundException
   * @throws PnErrorListException
   * @throws CommandExecutionException
   */
  private void setDatabaseLookupFieldFromCommandLineArgument(IBOValue aDatabase, String aReferenceFieldName) throws FieldNotFoundException, PnErrorListException, CommandExecutionException
  {
    String value = this.commandLineArguments.getArgument(aReferenceFieldName);
    setDatabaseLookupField(aDatabase, aReferenceFieldName, value);
  }
}
