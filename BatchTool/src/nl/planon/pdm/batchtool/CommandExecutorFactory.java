// Planon Enterprise Edition Source file: CommandExecutorFactory.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool;

/**
 * CommandExecutorFactory
 */
public class CommandExecutorFactory
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Builds a CommandExecutor.
   *
   * @param  aCommandLineArguments The command line arguments to build a CommandExecutor for.
   *
   * @return A command executor.
   */
  public ICommandExecutor build(CommandLineArguments aCommandLineArguments)
  {
    return new CommandExecuter(aCommandLineArguments);
  }
}
