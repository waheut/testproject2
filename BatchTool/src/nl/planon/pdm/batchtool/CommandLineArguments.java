// Planon Enterprise Edition Source file: CommandLineArguments.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.batchtool;

import java.util.*;

import nl.planon.hades.client.argumentparser.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;
import nl.planon.pdm.core.common.*;

/**
 * CommandLineArguments
 */
public class CommandLineArguments
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PDMCommand command;
  private String[] arguments;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CommandLineArguments object.
   *
   * @param arguments actual argument of main program
   */
  public CommandLineArguments(String[] arguments)
  {
    this.arguments = arguments;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets add only (false if db also has to be imported)
   *
   * @return boolean
   */
  public boolean getAddOnly()
  {
    return ArgumentParser.getAsBoolean(IPDMCommandLine.CMDLN_PARAM_ADD_ONLY, false);
  }


  /**
   * gets i-th argument
   *
   * @param  aIndex i-th argument, 0, 1, ...
   *
   * @return String
   */
  public final String getArgument(int aIndex)
  {
    return this.arguments[aIndex];
  }


  /**
   * gets argument with specified name
   *
   * @param  aName name of parameter
   *
   * @return String
   */
  public String getArgument(String aName)
  {
    return ArgumentParser.getAsString(aName);
  }


  /**
   * gets argument with specified name
   *
   * @param  aName    name of parameter
   * @param  aDefault default
   *
   * @return String
   */
  public String getArgument(String aName, String aDefault)
  {
    return ArgumentParser.getAsString(aName, aDefault);
  }


  /**
   * gets boolean argument with specified name
   *
   * @param  aName name of parameter
   *
   * @return boolean
   */
  public boolean getArgumentAsBoolean(String aName)
  {
    return ArgumentParser.getAsBoolean(aName);
  }


  /**
   * gets int value, value passed to commandline must be an integer
   *
   * @param  aName value to get
   *
   * @return int
   */
  public int getArgumentAsInt(String aName)
  {
    return ArgumentParser.getAsInt(aName);
  }


  /**
   * gets number of arguments
   *
   * @return int
   */
  public final int getArgumentCount()
  {
    return this.arguments.length;
  }


  /**
   * gets command
   *
   * @return PDMCommand, or null if failed
   */
  public final PDMCommand getCommand()
  {
    return this.command;
  }


  /**
   * gets database name
   *
   * @return String
   */
  public String getDatabaseName()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_DATABASE_NAME);
  }


  /**
   * gets database server code
   *
   * @return String
   */
  public String getDatabaseServerCode()
  {
    return ArgumentParser.getAsString(IBOPDMDatabaseDef.PN_DATABASESERVER_REF);
  }


  /**
   * gets export file name
   *
   * @return String
   */
  public String getExportDumpFileName()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_EXPORT_DUMP_FILE);
  }


  /**
   * gets export file type (D,O,M)
   *
   * @return String
   */
  public String getExportFileType()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_EXPORT_FILE_TYPE);
  }


  /**
   * gets import dump file
   *
   * @return String
   */
  public String getImportDumpfile()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_IMPORT_DUMP_FILE);
  }


  /**
   * gets location code specified on command line
   *
   * @return String
   */
  public String getLocationCode()
  {
    return ArgumentParser.getAsString(IBOPDMDatabaseDef.PN_PDMLOCATION_REF);
  }


  /**
   * Get the original arguments supplied to this programm.
   *
   * @return The arguments supplied to this programm.
   */
  public String[] getOriginalArguments()
  {
    return this.arguments;
  }


  /**
   * gets property file name
   *
   * @return String
   */
  public String getOutputFileName()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_OUTPUT_FILE);
  }


  /**
   * gets property file name
   *
   * @return String
   */
  public String getOutputPropertyFileName()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_OUTPUT_PROPERTY_FILE);
  }


  /**
   * gets password specified on command line
   *
   * @return String
   */
  public String getPassword()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_PASSWORD);
  }


  /**
   * gets planon ds file name
   *
   * @return String
   */
  public String getPlanonDSFileName()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_PLANON_DS_FILE);
  }


  /**
   * gets privilege type code
   *
   * @param  aCommand Command to get privilege type for; here in this case ADDIMPORT
   *
   * @return String
   */
  public String getPrivilegeTypeCode(PDMCommand aCommand)
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_PRIVILEGE_TYPE, PDMPrivilegeType.UPGRADE.getCode());
  }


  /**
   * gets time out in seconds
   *
   * @param  aCommand command to get time out for
   *
   * @return int
   */
  public int getTimeOut(PDMCommand aCommand)
  {
    int defaultValue = Integer.valueOf(aCommand.getDefault(IPDMCommandLine.CMDLN_PARAM_TIMEOUT)).intValue();
    return ArgumentParser.getAsInt(IPDMCommandLine.CMDLN_PARAM_TIMEOUT, defaultValue);
  }


  /**
   * gets user name specified on command line
   *
   * @return String
   */
  public String getUserName()
  {
    return ArgumentParser.getAsString(IPDMCommandLine.CMDLN_PARAM_USERNAME);
  }


  /**
   * checks if specified argument exists on command line
   *
   * @param  aName name of parameter
   *
   * @return boolean
   */
  public boolean isArgument(String aName)
  {
    return ArgumentParser.containsContent(aName);
  }


  /**
   * parse the arguments
   *
   * @throws FailedToParseCommandlineException DOCUMENT ME!
   */
  public void parse() throws FailedToParseCommandlineException
  {
    if ((this.arguments == null) || (this.arguments.length == 0) || (this.arguments[0] == null))
    {
      this.command = PDMCommand.HELP;
      return;
    }

    String cmdString = this.arguments[0];
    this.command = PDMCommand.getCommand(cmdString);
    if (this.command == null)
    {
      throw new FailedToParseCommandlineException("Unkown command: '" + cmdString + "' specified, use one of: '" + String.join(",", Arrays.toString(PDMCommand.values()) + "'."));
    }

    try
    { // parse remainder of arguments
      ArgumentParser.parseArguments(Arrays.copyOfRange(this.arguments, 1, this.arguments.length));
    }
    //catch when an argument exception occurs. Print an error message and pass the help command for a complete description of the tool
    catch (WrongArgumentException ex)
    {
      throw new FailedToParseCommandlineException("Wrong arguments provided. The arguments were: '" + String.join("'", this.arguments) + "'.", ex);
    }
  }
}
