// Planon Enterprise Edition Source file: PDMCommand.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.batchtool.commandinfo;

import java.util.*;

import nl.planon.hades.exception.*;

import nl.planon.morpheus.pdm.common.businessmodel.*;

import nl.planon.pdm.batchtool.*;
import nl.planon.pdm.batchtool.boinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;
import nl.planon.pdm.core.common.*;

/**
 * PDMCommand enum  Help, Export, Add, Delete, Read, Import, NewImport, ReadUsers, CreateDsFile
 *
 * @version $Revision$
 */
public enum PDMCommand
{
  //~ Enum constants -------------------------------------------------------------------------------

  HELP("show help of commands"), //
  EXPORT("exports a PDM database."), //
  ADDIMPORT("adds and import a database."), //
  DELETE("deletes and clean-ups a PDM database."), //
  REPORTDATABASEPROPERTIES("Creates a property file with DatabaseName, AccessCode, Name, DatabaseType, HostName and PortNumber of a specific PDM Database."), //
  IMPORT("imports an existing PDM database."), //
  REPORTDATABASENAMES("retrieves a list of all databases available."), //
  CREATEDSFILE("creates a planon-ds.xml file.");

  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final boolean MANDATORY = true;

  static
  {
    CommandLineNameValueDefinition itemOptionalLogLevel = new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_LEVEL);
    itemOptionalLogLevel.setValueDescription("<DEBUG,INFO,ERROR,...>");
    CommandLineNameValueDefinition itemAddOnly = new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_ADD_ONLY, "false");
    itemAddOnly.setValueDescription("<true,false>");

    CommandLineNameValueDefinition itemTimeOut = new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_TIMEOUT, "7200");
    itemTimeOut.setValueDescription("timeout in seconds " + itemTimeOut.getDefaultValue());

    CommandLineNameValueDefinition itemMandatoryDatabaseName = new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_DATABASE_NAME, MANDATORY);
    itemMandatoryDatabaseName.setValueDescription("<name of database>");

    CommandLineNameValueDefinition itemOptionalPrivilegeType = new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PRIVILEGE_TYPE, PDMPrivilegeType.UPGRADE.getCode());
    itemOptionalPrivilegeType.setValueDescription("Privilege Type <A> or <U> or <S>" + itemOptionalPrivilegeType.getDefaultValue());

    ADDIMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME, MANDATORY));
    ADDIMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD, MANDATORY));
    ADDIMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_IMPORT_DUMP_FILE, MANDATORY));
    ADDIMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_FILE));
    ADDIMPORT.addItem(itemMandatoryDatabaseName);
    ADDIMPORT.addItem(itemOptionalLogLevel);
    ADDIMPORT.addItem(itemAddOnly);
    ADDIMPORT.addItem(itemTimeOut);
//    ADDIMPORT.addItem(itemOptionalPrivilegeType);
    // other arguments (fields from database bo) will be added at runtime..

    EXPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME, MANDATORY));
    EXPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD, MANDATORY));
    EXPORT.addItem(itemMandatoryDatabaseName);
    EXPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_EXPORT_DUMP_FILE, MANDATORY));
    EXPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_FILE));
    CommandLineNameValueDefinition itemExportFileType = new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_EXPORT_FILE_TYPE);
    itemExportFileType.setValueDescription("<D,O,M> default as specified in Database");
    EXPORT.addItem(itemExportFileType);
    EXPORT.addItem(itemTimeOut);
    EXPORT.addItem(itemOptionalLogLevel);

    DELETE.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME, MANDATORY));
    DELETE.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD, MANDATORY));
    DELETE.addItem(itemMandatoryDatabaseName);
    DELETE.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_FILE));
    DELETE.addItem(itemTimeOut);
    DELETE.addItem(itemOptionalLogLevel);

    IMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME, MANDATORY));
    IMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD, MANDATORY));
    IMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_IMPORT_DUMP_FILE, MANDATORY));
    IMPORT.addItem(itemMandatoryDatabaseName);
    IMPORT.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_FILE));
    IMPORT.addItem(itemOptionalLogLevel);
    IMPORT.addItem(itemTimeOut);

    REPORTDATABASEPROPERTIES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME, MANDATORY));
    REPORTDATABASEPROPERTIES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD, MANDATORY));
    REPORTDATABASEPROPERTIES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_OUTPUT_PROPERTY_FILE, MANDATORY));
    REPORTDATABASEPROPERTIES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_FILE));
    REPORTDATABASEPROPERTIES.addItem(itemMandatoryDatabaseName);
    REPORTDATABASEPROPERTIES.addItem(itemOptionalLogLevel);

    CREATEDSFILE.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME, MANDATORY));
    CREATEDSFILE.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD, MANDATORY));
    CREATEDSFILE.addItem(itemMandatoryDatabaseName);
    CREATEDSFILE.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_FILE));
    CREATEDSFILE.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PLANON_DS_FILE, MANDATORY));
    CREATEDSFILE.addItem(itemOptionalLogLevel);

    REPORTDATABASENAMES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME, MANDATORY));
    REPORTDATABASENAMES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD, MANDATORY));
    REPORTDATABASENAMES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_OUTPUT_FILE));
    REPORTDATABASENAMES.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_LOG_FILE));
    REPORTDATABASENAMES.addItem(itemOptionalLogLevel);

    HELP.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_USERNAME));
    HELP.addItem(new CommandLineNameValueDefinition(IPDMCommandLine.CMDLN_PARAM_PASSWORD));
  }

  //~ Instance Variables ---------------------------------------------------------------------------

  private List<CommandLineNameValueDefinition> itemList = new ArrayList<CommandLineNameValueDefinition>();
  private Map<String, CommandLineNameValueDefinition> items = new LinkedHashMap<String, CommandLineNameValueDefinition>();
  private String description;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMCommand object.
   *
   * @param aDescription DOCUMENT ME!
   */
  private PDMCommand(String aDescription)
  {
    this.description = aDescription;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Retrieve the PDMCommand by id.
   *
   * @param  aName
   *
   * @return PDMCommand or null if unknown name
   */
  public static PDMCommand getCommand(String aName)
  {
    for (PDMCommand command : PDMCommand.values())
    {
      if (command.name().equalsIgnoreCase(aName))
      {
        return command;
      }
    }
    return null;
  }


  /**
   * add item to list of arguments of command
   *
   * @param aItem item to add
   */
  public void addItem(CommandLineNameValueDefinition aItem)
  {
    if (this.items.put(aItem.getName().toUpperCase(), aItem) != null)
    {
      System.out.print("Duplicate item " + aItem.getName() + " for  command " + name());
      System.exit(ErrorCode.FAILURE.getReturnValue());
    }
    this.itemList.add(aItem);
  }


  /**
   * gets list of items to be passed to bo
   *
   * @return List<CommandLineNameValueDefinition>
   */
  public List<CommandLineNameValueDefinition> getBOItems()
  {
    List<CommandLineNameValueDefinition> boItemList = new ArrayList<CommandLineNameValueDefinition>();
    for (CommandLineNameValueDefinition item : this.itemList)
    {
      if (item.isBOItem())
      {
        boItemList.add(item);
      }
    }

    return boItemList;
  }


  /**
   * gets default
   *
   * @param  aPnName argument to get default for
   *
   * @return String
   */
  public String getDefault(String aPnName)
  {
    return this.items.get(aPnName.toUpperCase()).getDefaultValue();
  }


  /**
   * gets description
   *
   * @return String
   */
  public String getDescription()
  {
    return this.description;
  }


  /**
   * gets list of all command arguments
   *
   * @return List
   */
  public List<CommandLineNameValueDefinition> getItems()
  {
    return this.itemList;
  }


  /**
   * updates command argument definitions with mandatory fields of Database BO
   *
   * @throws BOMExecutionException
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public void updateCommandLineItems() throws BOMExecutionException, PnErrorListException, AuthorizationException
  {
    PDMDatabaseFields pdmDatabaseFields = new PDMDatabaseFields(true);

    List<String> remainingFieldNames = new ArrayList();
    remainingFieldNames.addAll(pdmDatabaseFields.getFieldsMap().keySet());

    Map<String, String> defaultFieldValues = pdmDatabaseFields.getDefaultFieldValuesMap();
    List<String> optionals = new ArrayList<String>();
    for (String fieldName : pdmDatabaseFields.getMandatoryFields())
    {
      if (!defaultFieldValues.containsKey(fieldName))
      {
        CommandLineNameValueDefinition item = new CommandLineNameValueDefinition(fieldName, true);
        item.setValueDescription("\"..\"");
        item.setBOItem();
        PDMCommand.ADDIMPORT.addItem(item);
        remainingFieldNames.remove(fieldName);
      }
      else
      {
        optionals.add(fieldName);
      }
    }
    for (String fieldName : remainingFieldNames)
    {
      String defaultValue = defaultFieldValues.get(fieldName);
      CommandLineNameValueDefinition item;
      if (defaultValue != null)
      {
        item = new CommandLineNameValueDefinition(fieldName, defaultValue);
        if ("false".equals(defaultValue) || "true".equals(defaultValue))
        {
          item.setValueDescription("<true,false>");
        }
        else
        {
          item.setValueDescription(defaultValue);
        }
      }
      else
      {
        item = new CommandLineNameValueDefinition(fieldName);
      }
      item.setBOItem();

      PDMCommand.ADDIMPORT.addItem(item);
    }
  }


  /**
   * verifies arguments
   *
   * @param  aCommandLineArguments command line arguments to check
   *
   * @return false if not correct
   */
  public boolean verifyArguments(CommandLineArguments aCommandLineArguments)
  {
    boolean result = true;
    for (CommandLineNameValueDefinition def : getItems())
    {
      String itemName = def.getName();
      String value = aCommandLineArguments.getArgument(itemName);
      if (def.isMandatory() && (value == null) && (def.getDefaultValue() == null))
      {
        System.out.println(IPDMCommandLine.ERROR + itemName + " is missing but mandatory!");
        result = false;
      }
    }
    for (int i = 1; i < aCommandLineArguments.getArgumentCount(); i++)
    {
      String arg = aCommandLineArguments.getArgument(i);
      int offset = arg.indexOf("=");
      if (offset > 0)
      {
        String itemName = arg.substring(0, offset);
        if (!this.items.containsKey(itemName.toUpperCase()))
        {
          System.out.println(IPDMCommandLine.ERROR + "command line item " + itemName + " is unknown!");
          result = false;
        }
      }
    }

    if (ADDIMPORT.equals(this) && (aCommandLineArguments.getDatabaseServerCode() == null) && (aCommandLineArguments.getLocationCode() == null))
    {
      System.out.println(IPDMCommandLine.ERROR + "command line item " + IBOPDMDatabaseDef.PN_DATABASESERVER_REF + " or " + IBOPDMDatabaseDef.PN_PDMLOCATION_REF + " must be specified!");
      result = false;
    }
    return result;
  }
}
