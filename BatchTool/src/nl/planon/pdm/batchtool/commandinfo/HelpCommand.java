// Planon Enterprise Edition Source file: HelpCommand.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.commandinfo;

import nl.planon.morpheus.pdm.common.businessmodel.*;

import nl.planon.pdm.batchtool.*;

/**
 * helpCommand
 */
public class HelpCommand
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final String ENTER_PARAMETERS = "The following parameters can be entered by the user (the ones marked with * are mandatory):";
  private static final String TEXT_DEFAULT = "Default = ";
  private static final String SYNTAX_INFO = "Defaults are marked with " + CommandLineNameValueDefinition.DEFAULT_START_MARKER + ".." + CommandLineNameValueDefinition.DEFAULT_END_MARKER + ". Be sure you are logged in to get full specification of all items!";

  //~ Instance Variables ---------------------------------------------------------------------------

  private final CommandLineArguments commandLineArguments;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new HelpCommand object.
   *
   * @param aCommandLineArguments DOCUMENT ME!
   */
  public HelpCommand(CommandLineArguments aCommandLineArguments)
  {
    this.commandLineArguments = aCommandLineArguments;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get the fields that need to be filled in when you want to Read a pdm database
   */
  public void printCreateDBInfoPropertyFile()
  {
    printHeaderForCommand(PDMCommand.REPORTDATABASEPROPERTIES);
  }


  /**
   * Get a menu with Add, Delete, import, export, read, readusers, createdsfile with al the default
   * fields. The result can depend on whether is logged in
   *
   * @param aCmd DOCUMENT ME!
   */
  public void printHelp(PDMCommand aCmd)
  {
    if (PDMCommand.ADDIMPORT.equals(aCmd) || (aCmd == null))
    {
      printHeaderForCommand(PDMCommand.ADDIMPORT);
      System.out.println("command line item " + IBOPDMDatabaseDef.PN_DATABASESERVER_REF + " or " + IBOPDMDatabaseDef.PN_PDMLOCATION_REF + " must be specified!");
    }
    if (PDMCommand.DELETE.equals(aCmd) || (aCmd == null))
    {
      printHeaderForCommand(PDMCommand.DELETE);
    }
    if (PDMCommand.IMPORT.equals(aCmd) || (aCmd == null))
    {
      printHeaderForCommand(PDMCommand.IMPORT);
    }

    if (PDMCommand.EXPORT.equals(aCmd) || (aCmd == null))
    {
      printHeaderForCommand(PDMCommand.EXPORT);
    }

    if (PDMCommand.REPORTDATABASEPROPERTIES.equals(aCmd) || (aCmd == null))
    {
      printCreateDBInfoPropertyFile();
    }

    if (PDMCommand.REPORTDATABASENAMES.equals(aCmd) || (aCmd == null))
    {
      printHeaderForCommand(PDMCommand.REPORTDATABASENAMES);
    }

    if (PDMCommand.CREATEDSFILE.equals(aCmd) || (aCmd == null))
    {
      printHeaderForCommand(PDMCommand.CREATEDSFILE);
    }
  }


  /**
   * print a consistent header for a command
   *
   * @param aCommand the command
   */
  private void printHeaderForCommand(PDMCommand aCommand)
  {
    System.out.println();
    System.out.println(aCommand.name() + " - " + aCommand.getDescription());
    System.out.println();
    System.out.print("Syntax: " + aCommand.name());
    for (CommandLineNameValueDefinition def : aCommand.getItems())
    {
      if (def.isMandatory())
      {
        System.out.print(" " + def.getName() + "=" + def.getValueDescription());
      }
    }
    System.out.println();
    System.out.println("Optional items are:");
    System.out.println();
    for (CommandLineNameValueDefinition def : aCommand.getItems())
    {
      if (!def.isMandatory())
      {
        System.out.println("     " + def.getName() + "=" + def.getValueDescription());
      }
    }

    System.out.println();
    System.out.println();
    System.out.println(SYNTAX_INFO);
    System.out.println();
    System.out.println();
  }
}
