// Planon Enterprise Edition Source file: CommandLineNameValueDefinition.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.batchtool.commandinfo;

/**
 * CommandLineNameValueDefinition
 */
public class CommandLineNameValueDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String DEFAULT_END_MARKER = "]";
  public static final String DEFAULT_START_MARKER = "[";

  //~ Instance Variables ---------------------------------------------------------------------------

  private boolean boItem;

  private boolean mandatory;
  private String defaultValue;
  private String name;
  private String valueDescription = "<..>";

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CommandLineNameValueDefinition object.
   *
   * @param aName
   */
  CommandLineNameValueDefinition(String aName)
  {
    this(aName, false);
  }


  /**
   * Creates a new CommandLineNameValueDefinition object.
   *
   * @param aName    name of argument
   * @param aDefault default value of argument
   */
  CommandLineNameValueDefinition(String aName, String aDefault)
  {
    this(aName, false, aDefault);
  }


  /**
   * Creates a new CommandLineNameValueDefinition object.
   *
   * @param aName      name of argument
   * @param aMandatory true/false
   */
  CommandLineNameValueDefinition(String aName, boolean aMandatory)
  {
    this.name = aName;
    this.mandatory = aMandatory;
  }


  /**
   * Creates a new CommandLineNameValueDefinition object.
   *
   * @param aName         name of argument
   * @param aMandatory    true/false
   * @param aDefaultValue String containing default value as entered on command line
   */
  CommandLineNameValueDefinition(String aName, boolean aMandatory, String aDefaultValue)
  {
    this.name = aName;
    this.mandatory = aMandatory;
    this.defaultValue = aDefaultValue;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets default value
   *
   * @return String, or null if not defined
   */
  public String getDefaultValue()
  {
    return this.defaultValue;
  }


  /**
   * gets name of this item
   *
   * @return String
   */
  public String getName()
  {
    return this.name;
  }


  /**
   * gets description
   *
   * @return String
   */
  public String getValueDescription()
  {
    return this.valueDescription;
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public boolean isBOItem()
  {
    return this.boItem;
  }


  /**
   * checks if this item is mandatory
   *
   * @return boolean
   */
  public boolean isMandatory()
  {
    return this.mandatory;
  }


  /**
   * mark this item should be passed as a field to the related BO
   */
  public void setBOItem()
  {
    this.boItem = true;
  }


  /**
   * sets description of value, can be comma separated list of possible values. If default is found
   * in this string it is marked
   *
   * @param aDescription
   */
  void setValueDescription(String aDescription)
  {
    if (this.defaultValue != null)
    {
      int index = aDescription.indexOf(this.defaultValue);
      if (index >= 0)
      {
        aDescription = aDescription.substring(0, index) + DEFAULT_START_MARKER + this.defaultValue + DEFAULT_END_MARKER + aDescription.substring(index + this.defaultValue.length());
      }
    }

    this.valueDescription = aDescription;
  }
}
