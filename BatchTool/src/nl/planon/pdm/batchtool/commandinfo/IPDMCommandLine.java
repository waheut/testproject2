// Planon Enterprise Edition Source file: IPDMCommandLine.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.batchtool.commandinfo;

/**
 * IPDMCommandLine
 *
 * @version $Revision$
 */
public interface IPDMCommandLine
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // Available command line parameters
  public static final String CMDLN_PARAM_ADD_ONLY = "AddOnly";
  public static final String CMDLN_PARAM_COMMAND = "Command";
  public static final String CMDLN_PARAM_DATABASE_NAME = "DatabaseName";
  public static final String CMDLN_PARAM_DATABASE_TYPE = "DatabaseType";
  public static final String CMDLN_PARAM_EXPORT_DUMP_FILE = "ExportDumpFile";
  public static final String CMDLN_PARAM_EXPORT_FILE_TYPE = "ExportFileType";
  public static final String CMDLN_PARAM_HOST_NAME = "HostName";
  public static final String CMDLN_PARAM_IMPORT_DUMP_FILE = "ImportDumpFile";
  public static final String CMDLN_PARAM_LOG_FILE = "LogFile";
  public static final String CMDLN_PARAM_LOG_LEVEL = "LogLevel";
  public static final String CMDLN_PARAM_PASSWORD = "Password";
  public static final String CMDLN_PARAM_PLANON_DS_FILE = "PlanonDsFile";
  public static final String CMDLN_PARAM_PORT_NUMBER = "PortNumber";
  public static final String CMDLN_PARAM_OUTPUT_PROPERTY_FILE = "OutputPropertyFile";
  public static final String CMDLN_PARAM_OUTPUT_FILE = "OutputFile";
  public static final String CMDLN_PARAM_TIMEOUT = "TimeOut";
  public static final String CMDLN_PARAM_USERNAME = "UserName";
  public static final String CMDLN_PARAM_PRIVILEGE_TYPE = "PrivilegeType";

  public static final int INTERVAL_BETWEEN_ATTEMPTS = 30000; // 60000 in milliseconds (= 1/2 min)
  public static final int MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB = 120; // total 1 hour

  public static final String DOES_NOT_EXIST = " does not exist";
  public static final String MSG_TIME_OUT_ERROR = "Job failed, because it took longer than the specified timeout value";

  public static final String INFO = "INFO - ";
  public static final String ERROR = "ERROR - ";

  public static final int EXITCODE_0 = 0;
  public static final int EXITCODE_1 = 1;
  public static final int EXITCODE_2 = 2;
}
