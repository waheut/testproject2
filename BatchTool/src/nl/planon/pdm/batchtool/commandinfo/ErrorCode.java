// Planon Enterprise Edition Source file: ErrorCode.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.batchtool.commandinfo;

/**
 * ErrorCode
 *
 * @version $Revision$
 */
public enum ErrorCode
{
  //~ Enum constants -------------------------------------------------------------------------------

  OK(1), FAILURE(1), FAILURE_DUPLICATE_DATABASE_NAME(2);

  //~ Instance Variables ---------------------------------------------------------------------------

  private int returnValue;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ErrorCode object.
   *
   * @param aReturnValue DOCUMENT ME!
   */
  private ErrorCode(int aReturnValue)
  {
    this.returnValue = aReturnValue;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets return value
   *
   * @return 0,1,2
   */
  public int getReturnValue()
  {
    return this.returnValue;
  }
}
