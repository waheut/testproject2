// Planon Enterprise Edition Source file: ICommandExecutor.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool;

import nl.planon.pdm.batchtool.commandinfo.*;
import nl.planon.pdm.batchtool.exceptions.*;

/**
 * ICommandExecutor
 *
 * @version $Revision$
 */
public interface ICommandExecutor
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * This method first adds a Database, it fills in the values specified via the command line
   * arguments. Then it will try to save the database. If saving fails, because, for example, the bo
   * is under construction, it will repeatedly retry to save with a certain interval (specified by
   * IPDMCommandLine.SLEEP_TIME) until it succeeds or the number of retries (specified by
   * IPDMCommandLine.MAX_NR_OF_ATTEMPTS_TO_EXECUTE_JOB) is exceeded. Same goes for the case in which
   * the webdav (for uploading the dumpfile to the fileserver) is not available.
   *
   * <p>if the AddOnly argument is set to false (is default), then it will be imported directly
   * after the add.</p>
   *
   * @throws CommandExecutionException PnErrorListException
   */
  void addAndImportDB() throws CommandExecutionException;


  /**
   * Create a oracle/mssql Planon-ds.xml get arguments from Argumentsparser
   *
   * @param  aPDMCommand aInfo Planon Ds Oracle or Mssql
   *
   * @throws CommandExecutionException AuthorizationException
   */
  void createPlanonDS(PDMCommand aPDMCommand) throws CommandExecutionException;


  /**
   * try to clean-up the database. if the clean-up finishes successfully then also the delete bom is
   * executed.
   *
   * @throws CommandExecutionException PnErrorListException
   */
  void deleteDB() throws CommandExecutionException;


  /**
   * Export a PDM Database. get his arguments from ArgumentParser
   *
   * @throws CommandExecutionException PnErrorListException
   */
  void exportDB() throws CommandExecutionException;


  /**
   * Start a PDM Database import. Get his Arguments from ArgumentsParser
   *
   * @throws CommandExecutionException AuthorizationException
   */
  void importDB() throws CommandExecutionException;


  /**
   * Read all the PDM database names and print it. Optionally names are writen to OutputFileName
   *
   * @throws CommandExecutionException AuthorizationException
   */
  void reportDBNames() throws CommandExecutionException;


  /**
   * read Host name, database name, Port number and Databasetype values from a Specific PDM3 user!
   *
   * @throws CommandExecutionException AuthorizationException
   */
  void reportDBProperties() throws CommandExecutionException;
}
