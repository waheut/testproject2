// Planon Enterprise Edition Source file: FailedToParseCommandlineException.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.exceptions;

/**
 * FailedToParseCommandlineArgumentsException
 */
public class FailedToParseCommandlineException extends Exception
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new FailedToParseCommandlineArgumentsException object.
   *
   * @param message The message
   */
  public FailedToParseCommandlineException(String message)
  {
    super(message);
  }


  /**
   * Creates a new FailedToParseCommandlineException object.
   *
   * @param message DOCUMENT ME!
   * @param cause   DOCUMENT ME!
   */
  public FailedToParseCommandlineException(String message, Exception cause)
  {
    super(message, cause);
  }
}
