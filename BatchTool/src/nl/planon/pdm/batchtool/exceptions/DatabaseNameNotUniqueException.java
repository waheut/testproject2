// Planon Enterprise Edition Source file: DatabaseNameNotUniqueException.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.exceptions;

/**
 * DatabaseNameNotUniqueException
 */
public class DatabaseNameNotUniqueException extends Exception
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DatabaseNameNotUniqueException object.
   *
   * @param message Tjhe message
   */
  public DatabaseNameNotUniqueException(String message)
  {
    super(message);
  }
}
