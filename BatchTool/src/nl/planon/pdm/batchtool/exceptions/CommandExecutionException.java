// Planon Enterprise Edition Source file: CommandExecutionException.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.exceptions;

/**
 * BOMExecutionException
 */
public class CommandExecutionException extends Exception
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMExecutionException object.
   *
   * @param message The message
   */
  public CommandExecutionException(String message)
  {
    super(message);
  }


  /**
   * Creates a new BOMExecutionException object.
   *
   * @param message The message
   * @param cause   The cause
   */
  public CommandExecutionException(String message, Exception cause)
  {
    super(message, cause);
  }
}
