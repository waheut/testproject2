// Planon Enterprise Edition Source file: ProgramExecutionException.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.exceptions;

import nl.planon.pdm.batchtool.commandinfo.*;

/**
 * ProgramExecutionException
 */
public class ProgramExecutionException extends RuntimeException
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private final ErrorCode errorCode;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ProgramExecutionException object.
   *
   * @param errorCode The error code to quit the programm with.
   */
  public ProgramExecutionException(ErrorCode errorCode)
  {
    this.errorCode = errorCode;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Getter for the error code.
   *
   * @return The error code to quit the program with.
   */
  public ErrorCode getErrorCode()
  {
    return this.errorCode;
  }
}
