// Planon Enterprise Edition Source file: CleanupException.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.exceptions;

/**
 * CleanupException
 */
public class CleanupException extends Exception
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new CleanupException object.
   *
   * @param message The message
   */
  public CleanupException(String message)
  {
    super(message);
  }
}
