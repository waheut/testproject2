// Planon Enterprise Edition Source file: BOMExecutionException.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.batchtool.exceptions;

/**
 * BOMExecutionExcetpion
 */
public class BOMExecutionException extends Exception
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new BOMExecutionExcetpion object.
   *
   * @param message The message
   */
  public BOMExecutionException(String message)
  {
    super(message);
  }


  /**
   * Creates a new BOMExecutionExcetpion object.
   *
   * @param message The message
   * @param cause   The cause
   */
  public BOMExecutionException(String message, Exception cause)
  {
    super(message, cause);
  }
}
