echo off
setlocal

Rem Get location of this script (beeing the 'PDM3 commandclient'; aside of the 'EE swingclient' (assumption)
FOR /F "usebackq delims=="  %%i IN (`echo %0`)  DO set l_commandclientdirbs=%%~dpi
Rem And translate all backslashes to forwardslashes
set l_commandclientdir=%l_commandclientdirbs:\=/%

Rem echo l_commandclientdir (%l_commandclientdir%)

Rem Swing client directory name at any SF live environment
if exist %l_commandclientdirbs%..\liveswingclient set l_livescdname=liveswingclient
Rem Swing client directory name at Planon PDM3 production environment
if exist %l_commandclientdirbs%..\Client32 set l_livescdname=Client32
if defined l_livescdname goto endif_swingclientok
  echo ERROR: cannot find required Swing Client directory beside the PDM3 Command Client directory
  exit /b 1
:endif_swingclientok

set JAVA_HOME=%l_commandclientdirbs%..\%l_livescdname%\jre
if not defined SWINGCLIENT_LOCATION set SWINGCLIENT_LOCATION=%l_commandclientdir%../%l_livescdname%
Rem echo SWINGCLIENT_LOCATION (%SWINGCLIENT_LOCATION%)

set COMMANDCLIENT_LOCATION=%l_commandclientdir%

Rem Define required jboss (client) libraries
set jbossallclientlibs=%SWINGCLIENT_LOCATION%/jboss-common-core.jar;%SWINGCLIENT_LOCATION%/jboss-ejb-api_3.1_spec.jar;%SWINGCLIENT_LOCATION%/jboss-ejb3-ext-api.jar;%SWINGCLIENT_LOCATION%/jboss-j2se.jar;%SWINGCLIENT_LOCATION%/jboss-jms-api_1.1_spec.jar;%SWINGCLIENT_LOCATION%/jboss-jmx-mc-int.jar;%SWINGCLIENT_LOCATION%/jboss-kernel.jar;%SWINGCLIENT_LOCATION%/jboss-vfs.jar;%SWINGCLIENT_LOCATION%/jbossall-client-1.1.0.jar;%SWINGCLIENT_LOCATION%/jbosssx-client.jar;%SWINGCLIENT_LOCATION%/jbosssx-server.jar;%SWINGCLIENT_LOCATION%/jbosssx.jar

echo INFO: starting PDM commandline client: %*
Rem echo %JAVA_HOME%\bin\java -cp %SWINGCLIENT_LOCATION%/pnlogging.jar;%SWINGCLIENT_LOCATION%/log4j-1.2.17.jar;%SWINGCLIENT_LOCATION%/Hades.jar;%jbossallclientlibs%;%COMMANDCLIENT_LOCATION%nl.planon.module.pdmp5module.swing.jar;%COMMANDCLIENT_LOCATION%nl.planon.module.pdmcore.swing.jar;%COMMANDCLIENT_LOCATION%pdmbatchtool.jar -Djava.security.auth.login.config=%SWINGCLIENT_LOCATION%/auth.conf -DJNDIFILE=%SWINGCLIENT_LOCATION%/jndi.properties  nl/planon/pdm/batchtool/Main %*
%JAVA_HOME%\bin\java -cp %SWINGCLIENT_LOCATION%/pnlogging.jar;%SWINGCLIENT_LOCATION%/log4j-1.2.17.jar;%SWINGCLIENT_LOCATION%/Hades.jar;%jbossallclientlibs%;%COMMANDCLIENT_LOCATION%nl.planon.module.pdmp5module.swing.jar;%COMMANDCLIENT_LOCATION%nl.planon.module.pdmcore.swing.jar;%COMMANDCLIENT_LOCATION%pdmbatchtool.jar -Djava.security.auth.login.config=%SWINGCLIENT_LOCATION%/auth.conf -DJNDIFILE=%SWINGCLIENT_LOCATION%/jndi.properties  nl/planon/pdm/batchtool/Main %*
