// Planon Enterprise Edition Source file: PDMAgentType.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core;

import java.io.*;

/**
 * agent type registered in DF (JADE Directory Facilitator, an agent that provides the 'yellow
 * pages' service)
 *
 * @version $Revision$
 */
public enum PDMAgentType implements Serializable
{
  //~ Enum constants -------------------------------------------------------------------------------

  CONTROLLER_AGENT("controllerAgent"), DATABASE_AGENT("dbAgent");

  //~ Instance Variables ---------------------------------------------------------------------------

  private final String code;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMAgentType object.
   *
   * @param aCode code of this type
   */
  private PDMAgentType(String aCode)
  {
    this.code = aCode;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMAgentType with specified code
   *
   * @param  aCode
   *
   * @return null if not existing
   */
  public static PDMAgentType getPDMAgentTypeByCode(String aCode)
  {
    for (PDMAgentType pdmAgentType : PDMAgentType.values())
    {
      if (pdmAgentType.getCode().equals(aCode))
      {
        return pdmAgentType;
      }
    }
    return null;
  }


  /**
   * gets code
   *
   * @return the code
   */
  public String getCode()
  {
    return this.code;
  }


  /**
   * returns string presentation of this type (the code)
   *
   * @return the code
   */
  @Override public String toString()
  {
    return this.code;
  }
}
