// Planon Enterprise Edition Source file: PDMOntology.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core.ontology;

import jade.content.abs.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import jade.content.onto.*;
import jade.content.schema.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;
import nl.planon.pdm.core.datamessage.oracle.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;
import nl.planon.pdm.core.datamessage.response.*;

import nl.planon.util.pnlogging.*;

/**
 * specifies the structure in which the content of the messages in PDM is sent between the agents.
 */
public class PDMOntology extends Ontology
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(PDMOntology.class, PnLogCategory.DEFAULT);

  // The name identifying this ontology
  public static final String PDM_ONTOLOGY_NAME = "PDMOntology";

  public static final Codec CODEC = new SLCodec();
  public static final Ontology ONTOLOGY = PDMOntology.getInstance();

  // version number
  public static String versionNumber = "3";

  // VOCABULARY
  private static final String VERSION_NUMBER = "VersionNumber";
  private static final String EXP_LOCATION = "ExpLocation";
  private static final String IMP_LOCATION = "ImpLocation";

  private static final String PDM_DATABASE_TYPE_CODE = "Code";
  private static final String PDM_DATABASE_TYPE_NAME = "Name";

  private static final String PDM_FILE_TYPE_CODE = "Code";
  private static final String PDM_FILE_TYPE_NAME = "Name";

  private static final String DATABASE_DUMP_INFORMATION_SCHEMA_NAME = "SchemaName";
  private static final String DATABASE_DUMP_INFORMATION_DATABASE_TYPE = "DatabaseType";
  private static final String DATABASE_DUMP_INFORMATION_FILE_TYPE = "FileType";
  private static final String DATABASE_DUMP_INFORMATION_TABLE_SPACE_NAMES = "TableSpaceNames";
  private static final String DATABASE_DUMP_INFORMATION_VERSION_NUMBER = "VersionNumber";
  private static final String DATABASE_DUMP_INFORMATION_SIZE = "DumpSize";
  private static final String DATABASE_DUMP_PLANON_DETAILS = "PlanonDBVersionDetails";

  private static final String DUMP_INFORMATION = "DumpInformation";
  private static final String DUMP_FILE_NAME = "DumpFileName";
  private static final String DUMP_FILE_DIR = "DumpFileDir";
  private static final String DUMP_VERSION_NUMBER = "VersionNumber";

  private static final String DATABASE_LOCATION = "DatabaseLocation";
  private static final String LOCATION = "Location";

  private static final String DATABASE_TRIED_FILE_TYPE = "TriedFileType";
  private static final String DATABASE_USER = "DatabaseUser";
  private static final String DATABASE_PASSWORD = "DatabasePassword";
  private static final String SCHEMA_NAME = "SchemaName";
  private static final String TABLESPACE_NAME = "TableSpaceName";
  private static final String DATABASE_SERVER_NAME = "DatabaseServerName";
  private static final String DATAFILE_LOCATIONS = "DataFileLocations";

  private static final String LOGFILE_NAME = "LogFileName";
  private static final String LOGFILE_DIR = "LogFileDir";
  private static final String ERROR_INFORMATION = "ErrorInformation";

  private static final String STOP_ON_FAILURE = "StopOnFailure";
  private static final String OPTION_NO_IMPORT = "NoImport";
  private static final String TASK_TIMEOUT = "TaskTimeOut";

  private static final String ERROR_CODE = "ErrorCode";
  private static final String MESSAGE_VARIABLES = "MessageVariables";
  private static final String IS_ERROR_FOR_ADMINISTRATOR = "IsErrorForAdministrator";
// analyze info
  private static final String IMPORT_FILE_TYPE = "FileType";

  private static final String REFUSE_REASON = "RefuseReason";

  private static final String REQUESTTYPE_CODE = "RequestTypeCode";
  private static final String PRIMARY_KEY_PDMDATABASE = "PrimaryKeyDatabase";
  private static final String PRIMARY_KEY_REQUEST = "PrimaryKeyRequest";
  private static final String PRIMARY_KEY_TASK = "PrimaryKeyTask";
  private static final String PRIMARY_KEY_SERVER = "PrimaryKeyServer";
  private static final String SUPPORTED_VERSIONS = "SupportedVersions";
  private static final String PRIVILEGE_TYPE_CODE = "PrivilegesTypeCode";

  // Connection info for Database server
  private static final String DATABASE_SERVER_INFORMATION = "DatabaseServerInformation";
  private static final String ADMIN_USER = "AdminUser";
  private static final String ADMIN_PASSWORD = "AdminPassword";
  private static final String HOST_NAME = "HostName";
  private static final String PORT_NUMBER = "PortNumber";
  private static final String DATABASE_INSTANCE_NAME = "DatabaseInstanceName";
  private static final String TEMPORARY_TABLESPACE_NAME = "TemporaryTableSpaceName";
  private static final String EXCEPTION_INFORMATION = "ExceptionInformation";
  // The singleton instance of this ontology
  private static Ontology instance;

  // MSSQL DatabaseInformation
  private static final String CONNECT_SERVER_NAME = "ConnectServerName";
  private static final String SORT_ORDER = "SortOrder";
  private static final String CODE_PAGE = "CodePage";

  //~ Instance Variables ---------------------------------------------------------------------------

  private AgentActionSchema databaseDumpInformationSchema;
  private AgentActionSchema databaseTypeSchema;
  private AgentActionSchema errorInformationSchema;
  private AgentActionSchema exceptionInformationSchema;
  private AgentActionSchema fileTypeSchema;
  private AgentActionSchema fileTypeSchema1;
  private AgentActionSchema mssqlDatabaseServerInformationSchema;
  private AgentActionSchema oracleDatabaseServerInformationSchema;
  private ConceptSchema dataFileLocationSchema;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMOntology object.
   *
   * @throws OntologyException
   */
  private PDMOntology() throws OntologyException
  {
    // The PDM ontology extends the basic ontology
    super(PDM_ONTOLOGY_NAME, BasicOntology.getInstance());

    // Structure of the schema for the import Oracle datapump request action
    addSchemasForOracleDataPumpImport();
    addSchemasForMSSQLAnalyzeDump();
    addSchemasForOracleDatapumpAnalyzeDump();
    addSchemasForOracleOldStyleAnalyzeDump();

    //Structure of the schema for the import Oracle old style request action
    addSchemasForOracleOldStyleImport();

    // Structure of the schema for the import MSSQL request action
    addSchemasForMSSQLImport();

    // Structure of the schema for the export Oracle datapump request action
    addSchemasForOracleDataPumpExport();

    // structure of the schema for the export Oracle oldstyle request action
    addSchemasForOracleOldStyleExport();

    addSchemasForMSSQLExport();

    // Structure of the schema for the delete Oracle database request action
    addSchemasForOracleDelete();

    // Structure of the schema for the delete Oracle database request action
    addSchemasForMSSQLDelete();

    // Structure of the schema for the response of a export datapump request action
    AgentActionSchema schemaRefuseResponse = new AgentActionSchema(RefuseResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaRefuseResponse);
    schemaRefuseResponse.add(REFUSE_REASON, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    add(schemaRefuseResponse, RefuseResponse.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMOntology instance.
   *
   * @return PDMOntology instance.
   */
  public static synchronized Ontology getInstance()
  {
    if (instance == null)
    {
      try
      {
        instance = new PDMOntology();
      }
      catch (OntologyException oe)
      {
        oe.printStackTrace();
      }
    }
    return instance;
  }


  /**
   * {@inheritDoc}
   */
  @Override protected Object toObject(AbsObject aAbs, String aLcType, Ontology aGlobalOnto) throws UnknownSchemaException, UngroundedException, OntologyException
  {
    if (PDMDatabaseType.class.getSimpleName().toLowerCase().equals(aLcType))
    {
      AbsPrimitive prim = (AbsPrimitive) aAbs.getAbsObject(PDM_DATABASE_TYPE_CODE);
      String code = prim.getString();
      PDMDatabaseType pdmDatabaseType = PDMDatabaseType.getPDMDatabaseTypeByCode(code);
      return pdmDatabaseType;
    }
    if (PDMFileType.class.getSimpleName().toLowerCase().equals(aLcType))
    {
      AbsPrimitive prim = (AbsPrimitive) aAbs.getAbsObject(PDM_FILE_TYPE_CODE);
      String code = prim.getString();
      PDMFileType pdmFileType = PDMFileType.getPDMFileTypeByCode(code);
      return pdmFileType;
    }
    return super.toObject(aAbs, aLcType, aGlobalOnto);
  }


  /**
   * Add fields that are mandatory to all database server information schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addCommonFieldsForDatabaseServerInformationSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(ADMIN_USER, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(ADMIN_PASSWORD, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DATABASE_INSTANCE_NAME, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(DATAFILE_LOCATIONS, this.getDataFileLocationSchema(), 0, ObjectSchema.UNLIMITED);
    aActionSchema.add(HOST_NAME, getPrimitiveSchema(BasicOntology.STRING));
  }


  /**
   * Add fields that are mandatory to all schema's (both requests and responses)
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForAllSchemas(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(REQUESTTYPE_CODE, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(PRIMARY_KEY_PDMDATABASE, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(PRIMARY_KEY_REQUEST, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(PRIMARY_KEY_TASK, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(PRIVILEGE_TYPE_CODE, getPrimitiveSchema(BasicOntology.STRING));
  }


  /**
   * Add fields that are mandatory to all analyze request schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForAnalyzeRequestSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(DUMP_FILE_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DUMP_FILE_DIR, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DATABASE_LOCATION, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(VERSION_NUMBER, getPrimitiveSchema(BasicOntology.STRING));
  }


  /**
   * Add fields that are mandatory to all analyze response schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForAnalyzeResponseSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(LOGFILE_NAME, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(LOGFILE_DIR, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(DATABASE_LOCATION, getPrimitiveSchema(BasicOntology.INTEGER), ObjectSchema.OPTIONAL);
    aActionSchema.add(DUMP_INFORMATION, this.getDatabaseDumpInformationSchema());
    aActionSchema.add(DATABASE_TRIED_FILE_TYPE, this.getFileTypeSchema1(), 0, ObjectSchema.UNLIMITED);
    aActionSchema.add(EXCEPTION_INFORMATION, this.getExceptionInformationSchema(), ObjectSchema.OPTIONAL);
  }


  /**
   * Add fields that are mandatory to all delete request schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForDeleteRequestSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(PRIMARY_KEY_PDMDATABASE, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(SCHEMA_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(TABLESPACE_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(VERSION_NUMBER, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DUMP_FILE_DIR, getPrimitiveSchema(BasicOntology.STRING));
  }


  /**
   * Add fields that are mandatory to all delete response schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForDeleteResponseSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(LOGFILE_NAME, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(LOGFILE_DIR, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(EXCEPTION_INFORMATION, this.getExceptionInformationSchema(), ObjectSchema.OPTIONAL);
    aActionSchema.add(LOGFILE_DIR, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(LOGFILE_NAME, getPrimitiveSchema(BasicOntology.STRING));
  }


  /**
   * Add fields that are mandatory to all export request schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForExportRequestSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(PRIMARY_KEY_PDMDATABASE, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(SCHEMA_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DUMP_FILE_DIR, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DUMP_FILE_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(STOP_ON_FAILURE, getPrimitiveSchema(BasicOntology.BOOLEAN));
    aActionSchema.add(TASK_TIMEOUT, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(VERSION_NUMBER, getPrimitiveSchema(BasicOntology.STRING));
  }


  /**
   * Add fields that are mandatory to all export response schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForExportResponseSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(LOGFILE_NAME, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(LOGFILE_DIR, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(DUMP_FILE_DIR, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(DUMP_FILE_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(EXCEPTION_INFORMATION, this.getExceptionInformationSchema(), ObjectSchema.OPTIONAL);
  }


  /**
   * Add fields that are mandatory to all import request schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForImportRequestSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(DUMP_FILE_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DUMP_FILE_DIR, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DATABASE_PASSWORD, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(SCHEMA_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DUMP_INFORMATION, this.getDatabaseDumpInformationSchema());
    aActionSchema.add(TABLESPACE_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(STOP_ON_FAILURE, getPrimitiveSchema(BasicOntology.BOOLEAN));
    aActionSchema.add(TASK_TIMEOUT, getPrimitiveSchema(BasicOntology.INTEGER));
    aActionSchema.add(VERSION_NUMBER, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(OPTION_NO_IMPORT, getPrimitiveSchema(BasicOntology.BOOLEAN));
  }


  /**
   * Add fields that are mandatory to all import response schema's
   *
   * @param  aActionSchema schema to add
   *
   * @throws OntologyException
   */
  private void addMandatoryFieldsForImportResponseSchema(AgentActionSchema aActionSchema) throws OntologyException
  {
    aActionSchema.add(LOGFILE_NAME, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(LOGFILE_DIR, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
    aActionSchema.add(DATABASE_SERVER_NAME, getPrimitiveSchema(BasicOntology.STRING));
    aActionSchema.add(DUMP_INFORMATION, this.getDatabaseDumpInformationSchema());
    aActionSchema.add(EXCEPTION_INFORMATION, this.getExceptionInformationSchema(), ObjectSchema.OPTIONAL);
  }


  /**
   * create and add schema for request and response of an Oracle datapump import request
   *
   * @throws OntologyException
   */
  private void addSchemasForMSSQLAnalyzeDump() throws OntologyException
  {
    AgentActionSchema schemaAnalyzeDump = new AgentActionSchema(MSSQLAnalyzeRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaAnalyzeDump);
    addMandatoryFieldsForAnalyzeRequestSchema(schemaAnalyzeDump);
    schemaAnalyzeDump.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaMSSQL());
    add(schemaAnalyzeDump, MSSQLAnalyzeRequest.class);

    // Structure of the schema for the response of a import datapump request action
    AgentActionSchema schemaAnalyzeResponse = new AgentActionSchema(AnalyzeResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaAnalyzeResponse);
    addMandatoryFieldsForAnalyzeResponseSchema(schemaAnalyzeResponse);
    add(schemaAnalyzeResponse, AnalyzeResponse.class);
  }


  /**
   * create and add schema for request and response of an MSSQL delete database request
   *
   * @throws OntologyException
   */
  private void addSchemasForMSSQLDelete() throws OntologyException
  {
    AgentActionSchema schemaDeleteRequest = new AgentActionSchema(MSSQLDeleteRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaDeleteRequest);
    addMandatoryFieldsForDeleteRequestSchema(schemaDeleteRequest);
    schemaDeleteRequest.add(DATABASE_SERVER_INFORMATION, this.getDatabaseServerInformationSchemaMSSQL());
    add(schemaDeleteRequest, MSSQLDeleteRequest.class);

    // Structure of the schema for the response of a delete datapump request action
    AgentActionSchema schemaDeleteResponse = new AgentActionSchema(DeleteResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaDeleteResponse);
    addMandatoryFieldsForDeleteResponseSchema(schemaDeleteResponse);
    add(schemaDeleteResponse, DeleteResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle datapump export request
   *
   * @throws OntologyException
   */
  private void addSchemasForMSSQLExport() throws OntologyException
  {
    AgentActionSchema schemaExportRequest = new AgentActionSchema(MSSQLExportRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaExportRequest);
    addMandatoryFieldsForExportRequestSchema(schemaExportRequest);
    schemaExportRequest.add(TABLESPACE_NAME, getPrimitiveSchema(BasicOntology.STRING));
    schemaExportRequest.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaMSSQL());
    add(schemaExportRequest, MSSQLExportRequest.class);

    // Structure of the schema for the response of a export datapump request action
    AgentActionSchema schemaExportResponse = new AgentActionSchema(ExportResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaExportResponse);
    addMandatoryFieldsForExportResponseSchema(schemaExportResponse);
    add(schemaExportResponse, ExportResponse.class);
  }


  /**
   * create and add schema for request and response of an MSSQL import request
   *
   * @throws OntologyException
   */
  private void addSchemasForMSSQLImport() throws OntologyException
  {
    AgentActionSchema schemaImportRequest = new AgentActionSchema(MSSQLImportRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaImportRequest);
    addMandatoryFieldsForImportRequestSchema(schemaImportRequest);
    schemaImportRequest.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaMSSQL());
    add(schemaImportRequest, MSSQLImportRequest.class);

    // Structure of the schema for the response of a import datapump request action
    AgentActionSchema schemaImportResponse = new AgentActionSchema(ImportResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaImportResponse);
    addMandatoryFieldsForImportResponseSchema(schemaImportResponse);
    add(schemaImportResponse, ImportResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle datapump import request
   *
   * @throws OntologyException
   */
  private void addSchemasForOracleDatapumpAnalyzeDump() throws OntologyException
  {
    AgentActionSchema schemaAnalyzeDump = new AgentActionSchema(OracleDataPumpAnalyzeRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaAnalyzeDump);
    addMandatoryFieldsForAnalyzeRequestSchema(schemaAnalyzeDump);
    schemaAnalyzeDump.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaOracle());
    add(schemaAnalyzeDump, OracleDataPumpAnalyzeRequest.class);

    // Structure of the schema for the response of a import datapump request action
    AgentActionSchema schemaAnalyzeResponse = new AgentActionSchema(AnalyzeResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaAnalyzeResponse);
    addMandatoryFieldsForAnalyzeResponseSchema(schemaAnalyzeResponse);
    add(schemaAnalyzeResponse, AnalyzeResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle datapump export request
   *
   * @throws OntologyException
   */
  private void addSchemasForOracleDataPumpExport() throws OntologyException
  {
    AgentActionSchema schemaExportRequest = new AgentActionSchema(OracleDataPumpExportRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaExportRequest);
    addMandatoryFieldsForExportRequestSchema(schemaExportRequest);
    schemaExportRequest.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaOracle());
    add(schemaExportRequest, OracleDataPumpExportRequest.class);

    // Structure of the schema for the response of a export datapump request action
    AgentActionSchema schemaExportResponse = new AgentActionSchema(ExportResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaExportResponse);
    addMandatoryFieldsForExportResponseSchema(schemaExportResponse);
    add(schemaExportResponse, ExportResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle datapump import request
   *
   * @throws OntologyException
   */
  private void addSchemasForOracleDataPumpImport() throws OntologyException
  {
    AgentActionSchema schemaImportRequest = new AgentActionSchema(OracleDataPumpImportRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaImportRequest);
    addMandatoryFieldsForImportRequestSchema(schemaImportRequest);
    schemaImportRequest.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaOracle());
    add(schemaImportRequest, OracleDataPumpImportRequest.class);

    // Structure of the schema for the response of a import datapump request action
    AgentActionSchema schemaImportResponse = new AgentActionSchema(ImportResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaImportResponse);
    addMandatoryFieldsForImportResponseSchema(schemaImportResponse);
    add(schemaImportResponse, ImportResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle delete database request
   *
   * @throws OntologyException
   */
  private void addSchemasForOracleDelete() throws OntologyException
  {
    AgentActionSchema schemaDeleteRequest = new AgentActionSchema(OracleDeleteRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaDeleteRequest);
    addMandatoryFieldsForDeleteRequestSchema(schemaDeleteRequest);
    schemaDeleteRequest.add(DATABASE_SERVER_INFORMATION, this.getDatabaseServerInformationSchemaOracle());
    add(schemaDeleteRequest, OracleDeleteRequest.class);

    // Structure of the schema for the response of a delete datapump request action
    AgentActionSchema schemaDeleteResponse = new AgentActionSchema(DeleteResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaDeleteResponse);
    addMandatoryFieldsForDeleteResponseSchema(schemaDeleteResponse);
    add(schemaDeleteResponse, DeleteResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle datapump import request
   *
   * @throws OntologyException
   */
  private void addSchemasForOracleOldStyleAnalyzeDump() throws OntologyException
  {
    AgentActionSchema schemaAnalyzeDump = new AgentActionSchema(OracleOldStyleAnalyzeRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaAnalyzeDump);
    addMandatoryFieldsForAnalyzeRequestSchema(schemaAnalyzeDump);
    schemaAnalyzeDump.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaOracle());
    add(schemaAnalyzeDump, OracleOldStyleAnalyzeRequest.class);

    // Structure of the schema for the response of a import datapump request action
    AgentActionSchema schemaAnalyzeResponse = new AgentActionSchema(AnalyzeResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaAnalyzeResponse);
    addMandatoryFieldsForAnalyzeResponseSchema(schemaAnalyzeResponse);
    add(schemaAnalyzeResponse, AnalyzeResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle datapump export request
   *
   * @throws OntologyException
   */
  private void addSchemasForOracleOldStyleExport() throws OntologyException
  {
    AgentActionSchema schemaExportRequest = new AgentActionSchema(OracleOldStyleExportRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaExportRequest);
    addMandatoryFieldsForExportRequestSchema(schemaExportRequest);
    schemaExportRequest.add(DATABASE_PASSWORD, getPrimitiveSchema(BasicOntology.STRING));
    schemaExportRequest.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaOracle());
    add(schemaExportRequest, OracleOldStyleExportRequest.class);

    // Structure of the schema for the response of a export datapump request action
    AgentActionSchema schemaExportResponse = new AgentActionSchema(ExportResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaExportResponse);
    addMandatoryFieldsForExportResponseSchema(schemaExportResponse);
    add(schemaExportResponse, ExportResponse.class);
  }


  /**
   * create and add schema for request and response of an Oracle datapump import request
   *
   * @throws OntologyException
   */
  private void addSchemasForOracleOldStyleImport() throws OntologyException
  {
    AgentActionSchema schemaImportRequest = new AgentActionSchema(OracleOldStyleImportRequest.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaImportRequest);
    addMandatoryFieldsForImportRequestSchema(schemaImportRequest);
    schemaImportRequest.add(DATABASE_SERVER_INFORMATION, getDatabaseServerInformationSchemaOracle());
    add(schemaImportRequest, OracleOldStyleImportRequest.class);

    // Structure of the schema for the response of a import datapump request action
    AgentActionSchema schemaImportResponse = new AgentActionSchema(ImportResponse.class.getSimpleName());
    addMandatoryFieldsForAllSchemas(schemaImportResponse);
    addMandatoryFieldsForImportResponseSchema(schemaImportResponse);
    add(schemaImportResponse, ImportResponse.class);
  }


  /**
   * create a schema for database dump information. This is a subschema that is used by several
   * schemas for PDM requests
   *
   * @return schema for database dump information
   *
   * @throws OntologyException
   */
  private ConceptSchema getDatabaseDumpInformationSchema() throws OntologyException
  {
    if (this.databaseDumpInformationSchema == null)
    {
      this.databaseDumpInformationSchema = new AgentActionSchema(DatabaseDumpInformation.class.getSimpleName());
      this.databaseDumpInformationSchema.add(DUMP_VERSION_NUMBER, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.databaseDumpInformationSchema.add(DATABASE_DUMP_INFORMATION_DATABASE_TYPE, this.getDatabaseTypeSchema(), ObjectSchema.OPTIONAL);
      this.databaseDumpInformationSchema.add(DATABASE_DUMP_INFORMATION_FILE_TYPE, this.getFileTypeSchema(), ObjectSchema.OPTIONAL);
      this.databaseDumpInformationSchema.add(DATABASE_DUMP_INFORMATION_SCHEMA_NAME, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.databaseDumpInformationSchema.add(DATABASE_DUMP_INFORMATION_TABLE_SPACE_NAMES, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.databaseDumpInformationSchema.add(DATABASE_DUMP_PLANON_DETAILS, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      add(this.databaseDumpInformationSchema, DatabaseDumpInformation.class);
    }
    return this.databaseDumpInformationSchema;
  }


  /**
   * create a schema for MSSQL database server information. This is a subschema that is used by
   * several schemas for PDM requests
   *
   * @return schema for database server information
   *
   * @throws OntologyException
   */
  private AgentActionSchema getDatabaseServerInformationSchemaMSSQL() throws OntologyException
  {
    if (this.mssqlDatabaseServerInformationSchema == null)
    {
      this.mssqlDatabaseServerInformationSchema = new AgentActionSchema(MSSQLDatabaseServerInformation.class.getSimpleName());
      addCommonFieldsForDatabaseServerInformationSchema(this.mssqlDatabaseServerInformationSchema);
      this.mssqlDatabaseServerInformationSchema.add(SUPPORTED_VERSIONS, getPrimitiveSchema(BasicOntology.STRING), 0, ObjectSchema.UNLIMITED);
      this.mssqlDatabaseServerInformationSchema.add(CODE_PAGE, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.mssqlDatabaseServerInformationSchema.add(SORT_ORDER, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.mssqlDatabaseServerInformationSchema.add(DATABASE_DUMP_INFORMATION_VERSION_NUMBER, getPrimitiveSchema(BasicOntology.STRING));
      this.mssqlDatabaseServerInformationSchema.add(CONNECT_SERVER_NAME, getPrimitiveSchema(BasicOntology.STRING));

      add(this.mssqlDatabaseServerInformationSchema, MSSQLDatabaseServerInformation.class);
    }
    return this.mssqlDatabaseServerInformationSchema;
  }


  /**
   * create a schema for database server information. This is a subschema that is used by several
   * schemas for PDM requests
   *
   * @return schema for database server information
   *
   * @throws OntologyException
   */
  private AgentActionSchema getDatabaseServerInformationSchemaOracle() throws OntologyException
  {
    if (this.oracleDatabaseServerInformationSchema == null)
    {
      this.oracleDatabaseServerInformationSchema = new AgentActionSchema(OracleDatabaseServerInformation.class.getSimpleName());
      addCommonFieldsForDatabaseServerInformationSchema(this.oracleDatabaseServerInformationSchema);
      this.oracleDatabaseServerInformationSchema.add(SUPPORTED_VERSIONS, getPrimitiveSchema(BasicOntology.STRING), 0, ObjectSchema.UNLIMITED);
      this.oracleDatabaseServerInformationSchema.add(PRIMARY_KEY_SERVER, getPrimitiveSchema(BasicOntology.INTEGER));
      this.oracleDatabaseServerInformationSchema.add(PORT_NUMBER, getPrimitiveSchema(BasicOntology.INTEGER));
      this.oracleDatabaseServerInformationSchema.add(TEMPORARY_TABLESPACE_NAME, getPrimitiveSchema(BasicOntology.STRING));
      this.oracleDatabaseServerInformationSchema.add(IMP_LOCATION, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.oracleDatabaseServerInformationSchema.add(EXP_LOCATION, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);

      add(this.oracleDatabaseServerInformationSchema, OracleDatabaseServerInformation.class);
    }
    return this.oracleDatabaseServerInformationSchema;
  }


  /**
   * create a schema for database type information. This is a subschema that is used by several
   * schemas for PDM requests
   *
   * @return schema for database type information
   *
   * @throws OntologyException
   */
  private AgentActionSchema getDatabaseTypeSchema() throws OntologyException
  {
    if (this.databaseTypeSchema == null)
    {
      this.databaseTypeSchema = new AgentActionSchema(PDMDatabaseType.class.getSimpleName());
      this.databaseTypeSchema.add(PDM_DATABASE_TYPE_CODE, getPrimitiveSchema(BasicOntology.STRING));
      this.databaseTypeSchema.add(PDM_DATABASE_TYPE_NAME, getPrimitiveSchema(BasicOntology.STRING));
      add(this.databaseTypeSchema, PDMDatabaseType.class);
    }
    return this.databaseTypeSchema;
  }


  /**
   * create a schema for datafile location information. This is a subschema that is used by several
   * schemas for PDM requests
   *
   * @return schema for datafile location information
   *
   * @throws OntologyException
   */
  private ConceptSchema getDataFileLocationSchema() throws OntologyException
  {
    if (this.dataFileLocationSchema == null)
    {
      this.dataFileLocationSchema = new ConceptSchema(DatafileLocation.class.getSimpleName());
      this.dataFileLocationSchema.add(LOCATION, getPrimitiveSchema(BasicOntology.STRING));
      add(this.dataFileLocationSchema, DatafileLocation.class);
    }
    return this.dataFileLocationSchema;
  }


  /**
   * create a schema for error information. This is a subschema that is used by the exception
   * information schema and will consist of all error codes generated by executing a pdm request.
   *
   * @return schema for error information
   *
   * @throws OntologyException
   */
  private AgentActionSchema getErrorInformationSchema() throws OntologyException
  {
    if (this.errorInformationSchema == null)
    {
      this.errorInformationSchema = new AgentActionSchema(ErrorInformation.class.getSimpleName());
      this.errorInformationSchema.add(ERROR_CODE, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.errorInformationSchema.add(MESSAGE_VARIABLES, getPrimitiveSchema(BasicOntology.STRING), 0, ObjectSchema.UNLIMITED);
      this.errorInformationSchema.add(IS_ERROR_FOR_ADMINISTRATOR, getPrimitiveSchema(BasicOntology.BOOLEAN));
      add(this.errorInformationSchema, ErrorInformation.class);
    }
    return this.errorInformationSchema;
  }


  /**
   * create a schema for exception information. This is a subschema that is used by several schemas
   * for PDM requests, especially when an exception occurs when executing a PDM request
   *
   * @return schema for exception information
   *
   * @throws OntologyException
   */
  private AgentActionSchema getExceptionInformationSchema() throws OntologyException
  {
    if (this.exceptionInformationSchema == null)
    {
      this.exceptionInformationSchema = new AgentActionSchema(ExceptionInformation.class.getSimpleName());
      this.exceptionInformationSchema.add(LOGFILE_NAME, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.exceptionInformationSchema.add(LOGFILE_DIR, getPrimitiveSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
      this.exceptionInformationSchema.add(ERROR_INFORMATION, this.getErrorInformationSchema(), 0, ObjectSchema.UNLIMITED);
      add(this.exceptionInformationSchema, ExceptionInformation.class);
    }
    return this.exceptionInformationSchema;
  }


  /**
   * create a schema for file type information. This is a subschema that is used by several schemas
   * for PDM requests
   *
   * @return schema for file type information
   *
   * @throws OntologyException
   */
  private AgentActionSchema getFileTypeSchema() throws OntologyException
  {
    if (this.fileTypeSchema == null)
    {
      this.fileTypeSchema = new AgentActionSchema(PDMFileType.class.getSimpleName());
      this.fileTypeSchema.add(PDM_FILE_TYPE_CODE, getPrimitiveSchema(BasicOntology.STRING));
      this.fileTypeSchema.add(PDM_FILE_TYPE_NAME, getPrimitiveSchema(BasicOntology.STRING));
      add(this.fileTypeSchema, PDMFileType.class);
    }
    return this.fileTypeSchema;
  }


  /**
   * create a schema for file type information. This is a subschema that is used by several schemas
   * for PDM requests
   *
   * @return schema for file type information
   *
   * @throws OntologyException
   */
  private AgentActionSchema getFileTypeSchema1() throws OntologyException
  {
    if (this.fileTypeSchema1 == null)
    {
      this.fileTypeSchema1 = new AgentActionSchema(PDMFileType.class.getSimpleName());
      this.fileTypeSchema1.add(PDM_FILE_TYPE_CODE, getPrimitiveSchema(BasicOntology.STRING));
//      this.fileTypeSchema.add(PDM_FILE_TYPE_NAME, getPrimitiveSchema(BasicOntology.STRING));
      add(this.fileTypeSchema1, PDMFileType.class);
    }
    return this.fileTypeSchema1;
  }


  /**
   * gets PrimitiveSchema for specified schema
   *
   * @param  aName the name of the schema in the vocabulary.
   *
   * @return PrimitiveSchema
   *
   * @throws OntologyException
   */
  private PrimitiveSchema getPrimitiveSchema(String aName) throws OntologyException
  {
    return (PrimitiveSchema) getSchema(aName);
  }
}
