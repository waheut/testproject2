// Planon Enterprise Edition Source file: ErrorInformation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import jade.content.*;
import jade.util.leap.*;

/**
 * ExceptionInformation
 */
public class ErrorInformation implements AgentAction
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private boolean isErrorForAdministrator;

  private List messageVariables = null; // this is not a java.util.List because
                                        // there is no ontology for that (also
                                        // no generics here)
  private String errorCode;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get error code linked to exception
   *
   * @return error code
   */
  public String getErrorCode()
  {
    return this.errorCode;
  }


  /**
   * get the value of for whom the exception is meant
   *
   * @return true if this exception is for administrators, false if it is for 'normal users'
   */
  public boolean getIsErrorForAdministrator()
  {
    return this.isErrorForAdministrator;
  }


  /**
   * get list of variables for the resulting error message of a failed import
   *
   * @return list of message variables
   */
  public List getMessageVariables()
  {
    return this.messageVariables;
  }


  /**
   * set error code linked to exception
   *
   * @param aErrorCode error code
   */
  public void setErrorCode(String aErrorCode)
  {
    this.errorCode = aErrorCode;
  }


  /**
   * set if exception is for administrator or for 'normal' user
   *
   * @param aIsErrorForAdministrator
   */
  public void setIsErrorForAdministrator(boolean aIsErrorForAdministrator)
  {
    this.isErrorForAdministrator = aIsErrorForAdministrator;
  }


  /**
   * set list of variables for the resulting error message of a failed import
   *
   * @param aMessageVariables list of message variables
   */
  public void setMessageVariables(List aMessageVariables)
  {
    this.messageVariables = aMessageVariables;
  }
}
