// Planon Enterprise Edition Source file: OracleDatabaseServerInformation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage.oracle;

import nl.planon.pdm.core.datamessage.*;

/**
 * Holds properties that apply specifically to Oracle database servers
 */
public class OracleDatabaseServerInformation extends DatabaseServerInformation
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private Integer portNumber;
  private String expLocation;

  private String impLocation;
  private String temporaryTableSpaceName;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get export location
   *
   * @return
   */
  public String getExpLocation()
  {
    return this.expLocation;
  }


  /**
   * get import location
   *
   * @return
   */
  public String getImpLocation()
  {
    return this.impLocation;
  }


  /**
   * get port number
   *
   * @return port number
   */
  public Integer getPortNumber()
  {
    return this.portNumber;
  }


  /**
   * get temporary table space name
   *
   * @return temporary table space name
   */
  public String getTemporaryTableSpaceName()
  {
    return this.temporaryTableSpaceName;
  }


  /**
   * set export location
   *
   * @param aExpLocation
   */
  public void setExpLocation(String aExpLocation)
  {
    this.expLocation = aExpLocation;
  }


  /**
   * set import location
   *
   * @param aImpLocation
   */
  public void setImpLocation(String aImpLocation)
  {
    this.impLocation = aImpLocation;
  }


  /**
   * set port number
   *
   * @param aPortNumber port number
   */
  public void setPortNumber(Integer aPortNumber)
  {
    this.portNumber = aPortNumber;
  }


  /**
   * set temporary tablespace name
   *
   * @param aTemporaryTableSpaceName temporary tablespace name
   */
  public void setTemporaryTableSpaceName(String aTemporaryTableSpaceName)
  {
    this.temporaryTableSpaceName = aTemporaryTableSpaceName;
  }
}
