// Planon Enterprise Edition Source file: IRequestInformationFields.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

/**
 * IRequestInformationFields
 *
 * @version $Revision$
 */
public interface IRequestInformationFields
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String FIELD_DATABASE_OWNER_NAME = "databaseOwnerName";
  public static final String FIELD_EXPORTFILETYPE_CODE = "ExportFileTypeCode";
  public static final String FIELD_LOCATION = "Location";
  public static final String FIELD_PDMDATABASE_DUMP_SIZE = "PdmDumpSize";
  public static final String FIELD_PDMDATABASE_OBJECT_OWNER_IN_DUMP = "PdmDatabaseObjectOwnerInDump";
  public static final String FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT = "PdmDatabaseObjectOwnerInImport";
  public static final String FIELD_PDMDATABASE_OPTION_NO_IMPORT = "PdmDbNoImport";
  public static final String FIELD_PDMDATABASE_PASSWORD = "PdmDatabasePassword";
  public static final String FIELD_PDMDATABASE_PDMDUMPFILE = "PdmDumpfile";
  public static final String FIELD_PDMDATABASE_PDMEXPORTFILE = "PdmExportfile";
  public static final String FIELD_PDMDATABASE_SIZE = "SizeDatabase";
  public static final String FIELD_PDMDATABASE_STOP_ON_FAILURE = "PdmDbStopOnFailure";
  public static final String FIELD_PDMDATABASE_TABLESPACENAME_IN_DUMP = "PdmDbTableSpaceNameInDump";
  public static final String FIELD_PDMDATABASE_TASK_TIMEOUT = "PdmDbTaskTimeOut";
  public static final String FIELD_PDMDBSERVER_CODE = "DatabaseServerCode";
  public static final String FIELD_PDMDBTYPE_CODE = "DatabaseTypeCode";
  public static final String FIELD_PDMFILETYPE_CODE = "FileTypeCode";
  public static final String FIELD_PDMTASK_SUBTYPE = "PdmTaskSubtype";
  public static final String FIELD_PK_PDMDATABASE = "PkPdmDatabase";
  public static final String FIELD_PK_PDMDATABASESERVER = "PkPdmDatabaseServer";
  public static final String FIELD_PK_PDMDATABASE_LOCATION = "PDMLocationRef";
  public static final String FIELD_PK_PDMREQUEST = "PkPdmRequest";
  public static final String FIELD_PK_PDMTASK = "PkPdmTask";
  public static final String FIELD_REQUESTTYPE_CODE = "RequestTypeCode";
  public static final String FIELD_PDMDATABASE_PRIVILEGE_TYPE = "PdmDbPrivilegeType";
}
