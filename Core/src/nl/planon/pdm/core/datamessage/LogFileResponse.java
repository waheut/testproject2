// Planon Enterprise Edition Source file: LogFileResponse.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

/**
 * LogFileResponse
 *
 * @version $Revision$
 */
public interface LogFileResponse
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get directory where log file is stored
   *
   * @return log file directory name
   */
  public abstract String getLogFileDir();


  /**
   * get name of log file
   *
   * @return log file name
   */
  public abstract String getLogFileName();


  /**
   * set directory where log file is stored
   *
   * @param aLogFileDir log file directory name
   */
  public abstract void setLogFileDir(String aLogFileDir);


  /**
   * set name of log file
   *
   * @param aLogFileName name of log file
   */
  public abstract void setLogFileName(String aLogFileName);
}
