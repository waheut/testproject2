// Planon Enterprise Edition Source file: WebDAVRequest.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

/**
 * WebDAVRequest
 *
 * @version $Revision$
 */
public interface WebDAVRequest
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get name of directory where dump file resides
   *
   * @return name of dump file directory
   */
  public abstract String getDumpFileDir();


  /**
   * set name of dump file
   *
   * @return dump file name
   */
  public abstract String getDumpFileName();


  /**
   * set name of directory where dump file resides
   *
   * @param aDumpFileDir name of dump file directory
   */
  public abstract void setDumpFileDir(String aDumpFileDir);


  /**
   * set name of dump file
   *
   * @param aDumpFileName dump file name
   */
  public abstract void setDumpFileName(String aDumpFileName);
}
