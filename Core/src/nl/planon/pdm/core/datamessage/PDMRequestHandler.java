// Planon Enterprise Edition Source file: PDMRequestHandler.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import jade.content.*;

import nl.planon.pdm.core.common.*;

/**
 * PDMRequestHandler, e.g. used for export
 */
public class PDMRequestHandler implements AgentAction
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PDMRequestType requestType;

  private String dumpFileName;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestHandler object.
   */
  public PDMRequestHandler()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get name of dump file
   *
   * @return name if dump file
   */
  public String getDumpFileName()
  {
    return this.dumpFileName;
  }


  /**
   * get type of request
   *
   * @return type of request
   */
  public PDMRequestType getRequestType()
  {
    return this.requestType;
  }


  /**
   * set name of dump file
   *
   * @param aDumpFileName name of dump file
   */
  public void setDumpFileName(String aDumpFileName)
  {
    this.dumpFileName = aDumpFileName;
  }


  /**
   * set type of request
   *
   * @param aRequestType type of request
   */
  public void setRequestType(PDMRequestType aRequestType)
  {
    this.requestType = aRequestType;
  }
}
