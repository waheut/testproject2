// Planon Enterprise Edition Source file: PDMRequestRegistry.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import java.util.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.request.base.*;
import nl.planon.pdm.core.datamessage.request.mssql.*;
import nl.planon.pdm.core.datamessage.request.oracle.*;

/**
 * PDMRequestRegistry
 */
public class PDMRequestRegistry
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static Map<String, Class<? extends AbstractRequest>> registry = new HashMap();

  static
  {
    registry.put(PDMTaskType.IMPORT.getCode() + PDMFileType.MSSQL_DUMP.getCode(), MSSQLImportRequest.class);
    registry.put(PDMTaskType.IMPORT.getCode() + PDMFileType.ORA_EXP.getCode(), OracleOldStyleImportRequest.class);
    registry.put(PDMTaskType.IMPORT.getCode() + PDMFileType.DATAPUMP.getCode(), OracleDataPumpImportRequest.class);

    registry.put(PDMTaskType.EXPORT.getCode() + PDMFileType.MSSQL_DUMP.getCode(), MSSQLExportRequest.class);
    registry.put(PDMTaskType.EXPORT.getCode() + PDMFileType.ORA_EXP.getCode(), OracleOldStyleExportRequest.class);
    registry.put(PDMTaskType.EXPORT.getCode() + PDMFileType.DATAPUMP.getCode(), OracleDataPumpExportRequest.class);

    registry.put(PDMTaskType.DELETE.getCode() + PDMFileType.MSSQL_DUMP.getCode(), MSSQLDeleteRequest.class);
    registry.put(PDMTaskType.DELETE.getCode() + PDMFileType.ORA_EXP.getCode(), OracleDeleteRequest.class);
    registry.put(PDMTaskType.DELETE.getCode() + PDMFileType.DATAPUMP.getCode(), OracleDeleteRequest.class);
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get pdm request linked to the given action name
   *
   * @param  aPDMRequestType actionName name of database action
   *
   * @return pdm request class
   */
  public static Class<? extends AbstractRequest> get(String aPDMRequestType)
  {
    return registry.get(aPDMRequestType);
  }
}
