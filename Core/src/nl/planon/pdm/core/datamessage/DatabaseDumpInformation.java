// Planon Enterprise Edition Source file: DatabaseDumpInformation.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import nl.planon.pdm.core.common.*;

/**
 * contains information of a database dump like database type, schema name, tablespaces, etc.
 */
public class DatabaseDumpInformation implements IDatabaseDumpInformation
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private PDMDatabaseType databaseType;
  private PDMFileType fileType;
  private String planonDBVersionDetails;
  private String schemaName;
  private String tableSpaceNames;
  private String versionNumber;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseType getDatabaseType()
  {
    return this.databaseType;
  }


  /**
   * {@inheritDoc}
   */
  @Override public PDMFileType getFileType()
  {
    return this.fileType;
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getPlanonDBVersionDetails()
  {
    return this.planonDBVersionDetails;
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getSchemaName()
  {
    return this.schemaName;
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getTableSpaceNames()
  {
    return this.tableSpaceNames;
  }


  /**
   * get version number
   *
   * @return version number
   */
  @Override public String getVersionNumber()
  {
    return this.versionNumber;
  }


  /**
   * set type of database
   *
   * @param aDatabaseType type of database
   */
  public void setDatabaseType(PDMDatabaseType aDatabaseType)
  {
    this.databaseType = aDatabaseType;
  }


  /**
   * set type of dumpfile
   *
   * @param aFileType type of dumpfile
   */
  public void setFileType(PDMFileType aFileType)
  {
    this.fileType = aFileType;
  }


  /**
   * Sets the Planon database version details namely, EE Metadata version, DB Metadata version, FN
   * Metada verson.
   *
   * @param aPlanonDBVersionDetails String form of Planon DB details.
   */
  public void setPlanonDBVersionDetails(String aPlanonDBVersionDetails)
  {
    this.planonDBVersionDetails = aPlanonDBVersionDetails;
  }


  /**
   * set schema name of dump file
   *
   * @param aSchemaName schema name of dump file
   */
  public void setSchemaName(String aSchemaName)
  {
    this.schemaName = trimValue(aSchemaName);
  }


  /**
   * set table space name of dump file
   *
   * @param aTableSpaceNames comma separated list of table space names found in dump file
   */
  public void setTableSpaceNames(String aTableSpaceNames)
  {
    this.tableSpaceNames = trimValue(aTableSpaceNames);
  }


  /**
   * set version number
   *
   * @param aVersionNumber version number
   */
  public void setVersionNumber(String aVersionNumber)
  {
    this.versionNumber = aVersionNumber;
  }


  /**
   * trim value if defined and return result
   *
   * @param  aValue value to trim
   *
   * @return trimmed value of 1 or more characters or null if no value exists
   */
  private String trimValue(String aValue)
  {
    if (aValue != null)
    {
      aValue = aValue.trim();
      if (aValue.isEmpty())
      {
        aValue = null;
      }
    }
    return aValue;
  }
}
