// Planon Enterprise Edition Source file: DatafileLocation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import jade.content.*;

/**
 * concept for datafile locations on a database server
 */
public class DatafileLocation implements Concept
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private String location = null;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get location for datafile on database server
   *
   * @return data file location
   */
  public String getLocation()
  {
    return this.location;
  }


  /**
   * set location for datafile on database server
   *
   * @param aLocation the datafile location
   */
  public void setLocation(String aLocation)
  {
    this.location = aLocation;
  }
}
