// Planon Enterprise Edition Source file: IDatabaseImportInformation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

/**
 * Describes information needed to import db
 *
 * @version $Revision$
 */
public interface IDatabaseImportInformation
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get name of database server
   *
   * @return null or name of database server
   */
  String getDatabaseServerName();


  /**
   * gets corresponding dump information
   *
   * @return IDatabaseDumpInformation
   */
  IDatabaseDumpInformation getDumpInformation();
}
