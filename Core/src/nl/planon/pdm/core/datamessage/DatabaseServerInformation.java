// Planon Enterprise Edition Source file: DatabaseServerInformation.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import jade.util.leap.*;

/**
 * Holds properties that apply to all supported database servers
 */
public class DatabaseServerInformation implements IDatabaseServerInformation
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private Integer primaryKeyServer;

  private List dataFileLocations = new ArrayList(); // this is not a java.util.List because there is \\
                                                    // no ontology for that (also no generics here)

  private List supportedVersions = new ArrayList();

  private String adminPassword;
  private String adminUser;
  private String databaseInstanceName;
  private String hostName;

  private String versionNumber;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public String getAdminPassword()
  {
    return this.adminPassword;
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getAdminUser()
  {
    return this.adminUser;
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getDatabaseInstanceName()
  {
    return this.databaseInstanceName;
  }


  /**
   * get list of data file locations
   *
   * @return list of datafile locations
   */
  public List getDataFileLocations()
  {
    return this.dataFileLocations;
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getHostName()
  {
    return this.hostName;
  }


  /**
   * get primary key of database server
   *
   * @return primary key of database server
   */
  public Integer getPrimaryKeyServer()
  {
    return this.primaryKeyServer;
  }


  /**
   * get list of supported versions
   *
   * @return list of supported versions
   */
  public List getSupportedVersions()
  {
    return this.supportedVersions;
  }


  /**
   * get the version number
   *
   * @return version number
   */
  public String getVersionNumber()
  {
    return this.versionNumber;
  }


  /**
   * check wether specified version is supported
   *
   * @param  aVersion version to check
   *
   * @return true if version is supported
   */
  public boolean isVersionSupported(String aVersion)
  {
    assert aVersion != null : "version must be specified (null not allowed)";
    boolean isSupported = false;
    List supportedVersions = getSupportedVersions();
    Iterator iter = supportedVersions.iterator();
    while (iter.hasNext() && !isSupported)
    {
      Object supportedVersion = iter.next();
      assert supportedVersion instanceof String : "list of strings expected";
      isSupported = aVersion.equals(supportedVersion);
    }
    return isSupported;
  }


  /**
   * set password of administrator
   *
   * @param aAdminPassword password of administrator
   */
  public void setAdminPassword(String aAdminPassword)
  {
    this.adminPassword = aAdminPassword;
  }


  /**
   * set user name of administrator
   *
   * @param aAdminUser user name of administrator
   */
  public void setAdminUser(String aAdminUser)
  {
    this.adminUser = aAdminUser;
  }


  /**
   * set database instance name
   *
   * @param aDatabaseInstanceName database instance name
   */
  public void setDatabaseInstanceName(String aDatabaseInstanceName)
  {
    this.databaseInstanceName = aDatabaseInstanceName;
  }


  /**
   * set list of data file locations
   *
   * @param aDataFileLocations list of data file locations
   */
  public void setDataFileLocations(List aDataFileLocations)
  {
    this.dataFileLocations = aDataFileLocations;
  }


  /**
   * set host name
   *
   * @param aHostName host name
   */
  public void setHostName(String aHostName)
  {
    this.hostName = aHostName;
  }


  /**
   * set primary key of database server
   *
   * @param aPrimaryKeyServer
   */
  public void setPrimaryKeyServer(Integer aPrimaryKeyServer)
  {
    this.primaryKeyServer = aPrimaryKeyServer;
  }


  /**
   * set list of supported versions
   *
   * @param aSupportedVersions
   */
  public void setSupportedVersions(List aSupportedVersions)
  {
    this.supportedVersions = aSupportedVersions;
  }


  /**
   * set the version number
   *
   * @param aVersionNumber the version number
   */
  public void setVersionNumber(String aVersionNumber)
  {
    this.versionNumber = aVersionNumber;
  }
}
