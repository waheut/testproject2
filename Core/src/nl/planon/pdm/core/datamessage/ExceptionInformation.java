// Planon Enterprise Edition Source file: ExceptionInformation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import jade.content.*;
import jade.util.leap.*;

/**
 * ExceptionInformation
 */
public class ExceptionInformation implements AgentAction, IExceptionInformation
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private List errorInformation = new ArrayList(); // this is not a java.util.List because there is
                                                   // no ontology for that (also no generics here)

  private String logFileDir;
  private String logFileName;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get list of errors
   *
   * @return list of errors
   */
  public List getErrorInformation()
  {
    return this.errorInformation;
  }


  /**
   * get directory of log file
   *
   * @return directory of log file
   */
  public String getLogFileDir()
  {
    return this.logFileDir;
  }


  /**
   * get name of log file
   *
   * @return name of log file
   */
  public String getLogFileName()
  {
    return this.logFileName;
  }


  /**
   * set list errors
   *
   * @param aErrorInformation list of errors
   */
  public void setErrorInformation(List aErrorInformation)
  {
    this.errorInformation = aErrorInformation;
  }


  /**
   * set directory of log file
   *
   * @param aLogFileDir directory of log file
   */
  public void setLogFileDir(String aLogFileDir)
  {
    this.logFileDir = aLogFileDir;
  }


  /**
   * set name of log file
   *
   * @param aLogFileName name of log file
   */
  public void setLogFileName(String aLogFileName)
  {
    this.logFileName = aLogFileName;
  }
}
