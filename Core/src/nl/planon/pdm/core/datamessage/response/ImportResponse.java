// Planon Enterprise Edition Source file: ImportResponse.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.response;

import jade.content.*;

import nl.planon.pdm.core.datamessage.*;

/**
 * MSSQLResponse
 */
public class ImportResponse extends AbstractResponse implements AgentAction, LogFileResponse, IDatabaseImportInformation
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private IDatabaseDumpInformation dumpInformation;
  private String databaseServerName;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public String getDatabaseServerName()
  {
    return this.databaseServerName;
  }


  /**
   * gets dump information
   *
   * @return IDatabaseDumpInformation
   */
  @Override public IDatabaseDumpInformation getDumpInformation()
  {
    if (this.dumpInformation == null)
    {
      this.dumpInformation = new DatabaseDumpInformation();
    }
    return this.dumpInformation;
  }


  /**
   * {@inheritDoc}
   */
  public void setDatabaseServerName(String aDatabaseServerName)
  {
    this.databaseServerName = aDatabaseServerName;
  }


  /**
   * sets database dump information
   *
   * @param aDatabaseDumpInformation information to set
   */
  public void setDumpInformation(IDatabaseDumpInformation aDatabaseDumpInformation)
  {
    this.dumpInformation = aDatabaseDumpInformation;
  }
}
