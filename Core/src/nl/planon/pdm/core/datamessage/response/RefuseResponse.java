// Planon Enterprise Edition Source file: RefuseResponse.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.response;

/**
 * because we always need  primary key of task and request to be able to change its state this new
 * response is created. We cannot always used the response object of the failed task because it is
 * not always known which task did fail
 */
public class RefuseResponse extends AbstractResponse
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private String refuseReason;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get RefuseReason
   *
   * @return string
   */
  public String getRefuseReason()
  {
    return this.refuseReason;
  }


  /**
   * sets reason of refusal
   *
   * @param aReason string
   */
  public void setRefuseReason(String aReason)
  {
    this.refuseReason = aReason;
  }
}
