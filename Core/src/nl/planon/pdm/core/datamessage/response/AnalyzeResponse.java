// Planon Enterprise Edition Source file: AnalyzeResponse.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.response;

import jade.content.*;

import nl.planon.pdm.core.datamessage.*;

/**
 * Response containing result of Analyze
 */
public class AnalyzeResponse extends AbstractResponse implements AgentAction, LogFileResponse, IDatabaseImportInformation
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private IDatabaseDumpInformation dumpInformation;
  private Integer databaseLocation;
  private String databaseServerName;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get location of database
   *
   * @return location of database
   */
  public Integer getDatabaseLocation()
  {
    return this.databaseLocation;
  }


  /**
   * {@inheritDoc}
   */
  @Override public String getDatabaseServerName()
  {
    return this.databaseServerName;
  }


  /**
   * gets dump information
   *
   * @return IDatabaseDumpInformation
   */
  @Override public IDatabaseDumpInformation getDumpInformation()
  {
    if (this.dumpInformation == null)
    {
      this.dumpInformation = new DatabaseDumpInformation();
    }
    return this.dumpInformation;
  }


  /**
   * Database location.
   *
   * @param aPdmLocation Syscode of the PDMLocation
   */
  public void setDatabaseLocation(Integer aPdmLocation)
  {
    this.databaseLocation = aPdmLocation;
  }


  /**
   * {@inheritDoc}
   */
  public void setDatabaseServerName(String aDatabaseServerName)
  {
    this.databaseServerName = aDatabaseServerName;
  }


  /**
   * sets database dump information
   *
   * @param aDatabaseDumpInformation information to set
   */
  public void setDumpInformation(IDatabaseDumpInformation aDatabaseDumpInformation)
  {
    this.dumpInformation = aDatabaseDumpInformation;
  }
}
