// Planon Enterprise Edition Source file: ExportResponse.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.response;

import jade.content.*;

import nl.planon.pdm.core.datamessage.*;

/**
 * OracleDataPumpExportResponse
 */
public class ExportResponse extends AbstractResponse implements AgentAction, LogFileResponse
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private String dumpFileDir;

  private String dumpFileName;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get name of directory where exported dump file can be found
   *
   * @return name of directory where exported dump file can be found
   */
  public String getDumpFileDir()
  {
    return this.dumpFileDir;
  }


  /**
   * get name of exported dump file
   *
   * @return name of exported dump file
   */
  public String getDumpFileName()
  {
    return this.dumpFileName;
  }


  /**
   * sets name of directory where exported dump file can be found
   *
   * @param aDumpFileDir name of directory where exported dump file can be found
   */
  public void setDumpFileDir(String aDumpFileDir)
  {
    this.dumpFileDir = aDumpFileDir;
  }


  /**
   * sets name of exported dump file
   *
   * @param aDumpFileName name of exported
   */
  public void setDumpFileName(String aDumpFileName)
  {
    this.dumpFileName = aDumpFileName;
  }
}
