// Planon Enterprise Edition Source file: AbstractResponse.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core.datamessage.response;

import jade.content.*;
import jade.util.leap.*;

import org.apache.commons.httpclient.*;

import java.io.*;

import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.request.base.*;

import nl.planon.util.webdav.*;

/**
 * AbstractResponse
 */
public abstract class AbstractResponse implements AgentAction
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private IExceptionInformation exceptionInformation;
  private Integer primaryKeyDatabase;
  private Integer primaryKeyRequest;
  private Integer primaryKeyTask;
  private List triedFileType = new ArrayList();

  private String logFileDir;
  private String logFileName;
  private String privilegesTypeCode;
  private String requestTypeCode;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Sets primary keys of Database, request and task
   *
   * @param aAbstractRequest
   */
  public void fillMandatoryResponseData(AbstractRequest aAbstractRequest)
  {
    this.setPrimaryKeyDatabase(aAbstractRequest.getPrimaryKeyDatabase());
    this.setPrimaryKeyRequest(aAbstractRequest.getPrimaryKeyRequest());
    this.setPrimaryKeyTask(aAbstractRequest.getPrimaryKeyTask());
    this.setPrivilegesTypeCode(aAbstractRequest.getPrivilegesTypeCode());
  }


  /**
   * This method gets the content of the logfile that is produced by the pdm request.
   *
   * @return the content of the log file in a string builder
   *
   * @throws IOException
   */
  public StringBuilder getContentOfLogFile() throws IOException
  {
    StringBuilder logFileContent = new StringBuilder();

    // PCIS 209105: first check if there's a logfile available
    String dir = getLogFileDir(); // will contain separator at end...
    String fileName = getLogFileName();
    if ((dir != null) && (fileName != null))
    {
      logFileContent = new StringBuilder("Content of logfile: ");
      String path = dir + fileName;
      try
      {
        PnWebDAVResource resource = new PnWebDAVResource(new HttpURL(path));
        if ((resource != null) && (resource.exists()))
        {
          BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getMethodData()));
          String logFileLine;
          try
          {
            while ((logFileLine = reader.readLine()) != null)
            {
              logFileContent.append("\n").append(logFileLine);
            }
          }
          finally
          {
            reader.close();
          }
        }
      }
      catch (IOException ex)
      {
        logFileContent.append("No logfile found (" + path + ").");
      }
      catch (Exception ex)
      {
        logFileContent.append("Error retrieving logfile (" + path + ").");
      }
    }
    return logFileContent;
  }


  /**
   * gets exception information
   *
   * @return IExceptionInformation
   */
  public IExceptionInformation getExceptionInformation()
  {
    if (this.exceptionInformation == null)
    {
      this.exceptionInformation = new ExceptionInformation();
    }
    return this.exceptionInformation;
  }


  /**
   * get name of directory where log file is stored
   *
   * @return name of directory where log file is stored
   */
  public String getLogFileDir()
  {
    return this.logFileDir;
  }


  /**
   * get name of log file
   *
   * @return name of log file
   */
  public String getLogFileName()
  {
    return this.logFileName;
  }


  /**
   * get primary key value of corresponding pdm database
   *
   * @return primary key value of corresponding pdm database
   */
  public Integer getPrimaryKeyDatabase()
  {
    return this.primaryKeyDatabase;
  }


  /**
   * get primary key value of corresponding pdm request
   *
   * @return primary key value of corresponding pdm request
   */
  public Integer getPrimaryKeyRequest()
  {
    return this.primaryKeyRequest;
  }


  /**
   * get primary key value of corresponding pdm task
   *
   * @return primary key value of corresponding pdm task
   */
  public Integer getPrimaryKeyTask()
  {
    return this.primaryKeyTask;
  }


  /**
   * Returns the privilege code
   *
   * @return String of privilege code
   */
  public String getPrivilegesTypeCode()
  {
    return this.privilegesTypeCode;
  }


  /**
   * get code of requesttype that initiated the action
   *
   * @return code of requesttype that initiated the action
   */
  public String getRequestTypeCode()
  {
    return this.requestTypeCode;
  }


  /**
   * Get the filetype which tried just
   *
   * @return jade.util.leap.List
   */
  public List getTriedFileType()
  {
    return this.triedFileType;
  }


  /**
   * sets exception information
   *
   * @param aExceptionInformation information to set
   */
  public void setExceptionInformation(IExceptionInformation aExceptionInformation)
  {
    this.exceptionInformation = aExceptionInformation;
  }


  /**
   * set name of directory where log file is stored
   *
   * @param aLogFileDir name if directory where log file is stored
   */
  public void setLogFileDir(String aLogFileDir)
  {
    this.logFileDir = aLogFileDir;
  }


  /**
   * set name of log file
   *
   * @param aLogFileName name of log file
   */
  public void setLogFileName(String aLogFileName)
  {
    this.logFileName = aLogFileName;
  }


  /**
   * set primary key value of corresponding pdm database
   *
   * @param aPrimaryKeyDatabase primary key value of corresponding pdm database
   */
  public void setPrimaryKeyDatabase(Integer aPrimaryKeyDatabase)
  {
    this.primaryKeyDatabase = aPrimaryKeyDatabase;
  }


  /**
   * set primary key value of corresponding pdm request
   *
   * @param aPrimaryKeyRequest primary key value of corresponding pdm request
   */
  public void setPrimaryKeyRequest(Integer aPrimaryKeyRequest)
  {
    this.primaryKeyRequest = aPrimaryKeyRequest;
  }


  /**
   * set primary key value of corresponding pdm task
   *
   * @param aPrimaryKeyTask primary key value of corresponding pdm task
   */
  public void setPrimaryKeyTask(Integer aPrimaryKeyTask)
  {
    this.primaryKeyTask = aPrimaryKeyTask;
  }


  /**
   * Sets the privilege type code
   *
   * @param aPrivilegesTypeCode privilege type
   */
  public void setPrivilegesTypeCode(String aPrivilegesTypeCode)
  {
    this.privilegesTypeCode = aPrivilegesTypeCode;
  }


  /**
   * set code of requesttype that initiated the action
   *
   * @param aRequestTypeCode code of requesttype that initiated the action
   */
  public void setRequestTypeCode(String aRequestTypeCode)
  {
    this.requestTypeCode = aRequestTypeCode;
  }


  /**
   * Set the filetype which your tried.
   *
   * @param aFileType jade.util.leap.List.
   */
  public void setTriedFileType(List aFileType)
  {
    this.triedFileType = aFileType;
  }
}
