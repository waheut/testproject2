// Planon Enterprise Edition Source file: IExceptionInformation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import jade.util.leap.*;

/**
 * IExceptionInformation
 *
 * @version $Revision$
 */
public interface IExceptionInformation
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get list of errors
   *
   * @return list of errors
   */
  public List getErrorInformation();


  /**
   * get directory of log file
   *
   * @return directory of log file
   */
  public String getLogFileDir();


  /**
   * get name of log file
   *
   * @return name of log file
   */
  public String getLogFileName();


  /**
   * set list errors
   *
   * @param aErrorInformation list of errors
   */
  public void setErrorInformation(List aErrorInformation);


  /**
   * set directory of log file
   *
   * @param aLogFileDir directory of log file
   */
  public void setLogFileDir(String aLogFileDir);


  /**
   * set name of log file
   *
   * @param aLogFileName name of log file
   */
  public void setLogFileName(String aLogFileName);
}
