// Planon Enterprise Edition Source file: MSSQLDatabaseServerInformation.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.mssql;

import nl.planon.pdm.core.datamessage.*;

/**
 * Holds properties that apply specifically to MSSQL database servers
 */
public class MSSQLDatabaseServerInformation extends DatabaseServerInformation
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PREFIX_CONNECT_SERVER_NAME = "jdbc:sqlserver://";

  //~ Instance Variables ---------------------------------------------------------------------------

  private String codePage;
  private String connectServerName;

  private String sortOrder;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get code page
   *
   * @return code page
   */
  public String getCodePage()
  {
    return this.codePage;
  }


  /**
   * get connect server name, composed of a fixed prefix and the hostname
   *
   * @return connect server name
   */
  public String getConnectServerName()
  {
    return PREFIX_CONNECT_SERVER_NAME + this.getHostName();
  }


  /**
   * get sort order
   *
   * @return sort order
   */
  public String getSortOrder()
  {
    return this.sortOrder;
  }


  /**
   * set code page
   *
   * @param aCodePage code page
   */
  public void setCodePage(String aCodePage)
  {
    this.codePage = aCodePage;
  }


  /**
   * set connect server name
   *
   * @param aConnectServerName connect server name
   */
  public void setConnectServerName(String aConnectServerName)
  {
    this.connectServerName = aConnectServerName;
  }


  /**
   * set sort order
   *
   * @param aSortOrder sort order
   */
  public void setSortOrder(String aSortOrder)
  {
    this.sortOrder = aSortOrder;
  }
}
