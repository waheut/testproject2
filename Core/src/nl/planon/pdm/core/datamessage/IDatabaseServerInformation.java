// Planon Enterprise Edition Source file: IDatabaseServerInformation.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

/**
 * IDatabaseServerInformation
 *
 * @version $Revision$
 */
public interface IDatabaseServerInformation
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get password of administrator
   *
   * @return null or password
   */
  public String getAdminPassword();


  /**
   * get user name of administrator
   *
   * @return null or user name
   */
  public String getAdminUser();


  /**
   * get database instance name
   *
   * @return null or database instance name
   */
  public String getDatabaseInstanceName();


  /**
   * get host name
   *
   * @return null or host name
   */
  public String getHostName();
}
