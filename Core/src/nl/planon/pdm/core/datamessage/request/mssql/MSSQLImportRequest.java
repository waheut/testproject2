// Planon Enterprise Edition Source file: MSSQLImportRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.mssql;

import java.util.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;

/**
 * MSSQLImportRequest
 */
public class MSSQLImportRequest extends BaseMSSQLRequest implements WebDAVRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLImportRequest object.
   */
  public MSSQLImportRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, MSSQLDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) this.getDumpInformation();

    String databasePassword = (String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PASSWORD);

    // In case of a reload latest exported dumpfile:
    // - the exported dumpfile must be passed as dumpfile
    // - the object owner in import must be passed as schemaname (in order to skip the analysis step)
    // - the standard prefix + object owner in import must be passed as tablespace name (also needed in order to skip the analysis step)
    if (PDMRequestType.RELOAD_EXPORT.equals(this.getPDMRequestType()))
    {
      this.setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMEXPORTFILE));
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
      dumpInformation.setTableSpaceNames(DEFAULT_PREFIX_TABLESPACENAME + dumpInformation.getSchemaName());
    }
    // In case of an import or a reload initial dumpfile:
    // - the regular dumpfile must be passed as dumpfile
    // - the object owner in dump must be passed as schemaname (in order to skip the analysis step)
    // - the tablespace names in dump must be passed as tablespace name (also needed in order to skip the analysis step)
    else
    {
      this.setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMDUMPFILE));
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_DUMP));
      dumpInformation.setTableSpaceNames((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TABLESPACENAME_IN_DUMP));
    }
    this.setNoImport(((Boolean) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OPTION_NO_IMPORT)).booleanValue());
    this.setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    this.setDatabasePassword(databasePassword.isEmpty() ? DEFAULT_PASSWORD : databasePassword);
    this.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    this.setTableSpaceName(DEFAULT_PREFIX_TABLESPACENAME + this.getSchemaName());
    this.setStopOnFailure(((Boolean) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_STOP_ON_FAILURE)).booleanValue());

    Integer taskTimeOut = (Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TASK_TIMEOUT);
    this.setTaskTimeOut((taskTimeOut == null) ? 0 : taskTimeOut.intValue());

    this.setDatabaseServerInformation(aDatabaseServerInformation);
  }
}
