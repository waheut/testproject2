// Planon Enterprise Edition Source file: BaseOracleRequest.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import nl.planon.pdm.core.datamessage.oracle.*;
import nl.planon.pdm.core.datamessage.request.base.*;

/**
 * BaseOldStyleOracleRequest
 */
public abstract class BaseOracleRequest extends AbstractRequest<OracleDatabaseServerInformation>
{
}
