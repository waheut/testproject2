// Planon Enterprise Edition Source file: MSSQLExportRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.mssql;

import java.util.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;

/**
 * MSSQLExportRequest
 */
public class MSSQLExportRequest extends BaseMSSQLRequest implements WebDAVRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLExportRequest object.
   */
  public MSSQLExportRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, MSSQLDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMDUMPFILE));
    setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    setStopOnFailure(((Boolean) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_STOP_ON_FAILURE)).booleanValue());
    setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    setTableSpaceName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TABLESPACENAME_IN_DUMP));

    Integer timeOutValue = (Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TASK_TIMEOUT);
    setTaskTimeOut((timeOutValue == null) ? 0 : timeOutValue.intValue());

    setDatabaseServerInformation(aDatabaseServerInformation);
  }


  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseState getBaseStatusDatabase()
  {
    return DATABASE_STATE_AVAILABLE;
  }
}
