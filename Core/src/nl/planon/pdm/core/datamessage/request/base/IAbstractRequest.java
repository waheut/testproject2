// Planon Enterprise Edition Source file: IAbstractRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.base;

import nl.planon.pdm.core.common.*;

/**
 * IAbstractRequest
 *
 * @version $Revision$
 */
public interface IAbstractRequest
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final PDMDatabaseState DATABASE_STATE_INITIAL = PDMDatabaseState.INITIAL;
  public static final PDMDatabaseState DATABASE_STATE_AVAILABLE = PDMDatabaseState.AVAILABLE;
}
