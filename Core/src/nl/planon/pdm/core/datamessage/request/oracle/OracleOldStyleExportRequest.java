// Planon Enterprise Edition Source file: OracleOldStyleExportRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import java.util.*;

import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.oracle.*;

/**
 * OracleOldStyleExportRequest
 */
public class OracleOldStyleExportRequest extends BaseOracleExportRequest implements WebDAVRequest
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private String databasePassword;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, OracleDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    this.setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    this.setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMDUMPFILE));
    this.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    this.setStopOnFailure(((Boolean) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_STOP_ON_FAILURE)).booleanValue());
    this.setDatabasePassword((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PASSWORD));

    Integer timeOutValue = (Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TASK_TIMEOUT);
    this.setTaskTimeOut((timeOutValue == null) ? 0 : timeOutValue.intValue());

    this.setDatabaseServerInformation(aDatabaseServerInformation);
  }


  /**
   * get password of database
   *
   * @return database password
   */
  public String getDatabasePassword()
  {
    return this.databasePassword;
  }


  /**
   * set password value of database
   *
   * @param aPassword database password value
   */
  public void setDatabasePassword(String aPassword)
  {
    this.databasePassword = aPassword;
  }
}
