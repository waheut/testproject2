// Planon Enterprise Edition Source file: MSSQLAnalyzeRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.mssql;

import java.util.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;

/**
 * MSSQLAnalyzeRequest
 */
public class MSSQLAnalyzeRequest extends BaseMSSQLRequest implements WebDAVRequest
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private Integer databaseLocation;
  private PDMFileType fileType;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLAnalyzeRequest object.
   */
  public MSSQLAnalyzeRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, MSSQLDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) getDumpInformation();

    String databasePassword = (String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PASSWORD);
    setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMDUMPFILE));

    // In case of a reload latest exported dumpfile:
    // - the exported dumpfile must be passed as dumpfile
    // - the object owner in import must be passed as schemaname (in order to skip the analysis step)
    // - the standard prefix + object owner in import must be passed as tablespace name (also needed in order to skip the analysis step)
    if (PDMRequestType.RELOAD_EXPORT.equals(getPDMRequestType()))
    {
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
      dumpInformation.setTableSpaceNames(DEFAULT_PREFIX_TABLESPACENAME + dumpInformation.getSchemaName());
    }
    // In case of an import or a reload initial dumpfile:
    // - the regular dumpfile must be passed as dumpfile
    // - the object owner in dump must be passed as schemaname (in order to skip the analysis step)
    // - the tablespace names in dump must be passed as tablespace name (also needed in order to skip the analysis step)
    else
    {
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_DUMP));
      dumpInformation.setTableSpaceNames((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TABLESPACENAME_IN_DUMP));
    }
    setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    setPrimaryKeyRequest((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMREQUEST));
    setDatabasePassword(databasePassword.isEmpty() ? DEFAULT_PASSWORD : databasePassword);
    setDatabaseLocation((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMDATABASE_LOCATION));
    setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    setPDMRequestType(getPDMRequestType());
    setPrimaryKeyDatabase((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMDATABASE));
    setPrimaryKeyRequest((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMREQUEST));
    setPrimaryKeyTask((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMTASK));

    setDatabaseServerInformation(aDatabaseServerInformation);
  }


  /**
   * get database location
   *
   * @return database location
   */
  public Integer getDatabaseLocation()
  {
    return this.databaseLocation;
  }


  /**
   * get type of file
   *
   * @return type of file
   */
  public PDMFileType getFileType()
  {
    return this.fileType;
  }


  /**
   * set location of database
   *
   * @param aDatabaseLocation
   */
  public void setDatabaseLocation(Integer aDatabaseLocation)
  {
    this.databaseLocation = aDatabaseLocation;
  }


  /**
   * set type of file
   *
   * @param aFileType
   */
  public void setFileType(PDMFileType aFileType)
  {
    this.fileType = aFileType;
  }
}
