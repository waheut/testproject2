// Planon Enterprise Edition Source file: OracleDataPumpImportRequest.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import java.util.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.oracle.*;

/**
 * Class associated to the OracleDataPumpRequest schema
 */
public class OracleDataPumpImportRequest extends BaseOracleRequest
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private IDatabaseDumpInformation dumpInformation;

  private String databasePassword;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleDataPumpImportRequest object.
   */
  public OracleDataPumpImportRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, OracleDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) this.getDumpInformation();

    String databasePassword = (String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PASSWORD);

    // In case of a reload latest exported dumpfile:
    // - the exported dumpfile must be passed as dumpfile
    // - the object owner in import must be passed as schemaname (in order to skip the analysis step)
    // - the standard prefix + object owner in import must be passed as tablespace name (also needed in order to skip the analysis step)
    if (PDMRequestType.RELOAD_EXPORT.equals(this.getPDMRequestType()))
    {
      this.setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMEXPORTFILE));
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
      dumpInformation.setTableSpaceNames(DEFAULT_PREFIX_TABLESPACENAME + dumpInformation.getSchemaName());
    }
    // In case of an import or a reload initial dumpfile:
    // - the regular dumpfile must be passed as dumpfile
    // - the object owner in dump must be passed as schemaname (in order to skip the analysis step)
    // - the tablespace names in dump must be passed as tablespace name (also needed in order to skip the analysis step)
    else
    {
      this.setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMDUMPFILE));
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_DUMP));
      dumpInformation.setTableSpaceNames((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TABLESPACENAME_IN_DUMP));
    }
    this.setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    this.setDatabasePassword(databasePassword.isEmpty() ? DEFAULT_PASSWORD : databasePassword);
    this.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    this.setTableSpaceName(DEFAULT_PREFIX_TABLESPACENAME + this.getSchemaName());
    this.setStopOnFailure(((Boolean) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_STOP_ON_FAILURE)).booleanValue());
    this.setNoImport(((Boolean) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OPTION_NO_IMPORT)).booleanValue());
    this.setPrivilegesTypeCode(((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PRIVILEGE_TYPE)));

    Integer taskTimeOut = (Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TASK_TIMEOUT);
    this.setTaskTimeOut((taskTimeOut == null) ? 0 : taskTimeOut.intValue());

    this.setDatabaseServerInformation(aDatabaseServerInformation);
  }


  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseState getBaseStatusDatabase()
  {
    return DATABASE_STATE_INITIAL;
  }


  /**
   * {@inheritDoc}
   */
  public String getDatabasePassword()
  {
    return this.databasePassword;
  }


  /**
   * gets dump information
   *
   * @return IDatabaseDumpInformation
   */
  public IDatabaseDumpInformation getDumpInformation()
  {
    if (this.dumpInformation == null)
    {
      DatabaseDumpInformation info = new DatabaseDumpInformation();
      info.setDatabaseType(PDMDatabaseType.ORACLE);
      info.setFileType(PDMFileType.DATAPUMP);
      this.dumpInformation = info;
    }
    return this.dumpInformation;
  }


  /**
   * {@inheritDoc}
   */
  public void setDatabasePassword(String aDatabasePassword)
  {
    this.databasePassword = aDatabasePassword;
  }


  /**
   * {@inheritDoc}
   */
  @Override public void setDumpFileDir(String aDumpFileDir)
  {
    this.dumpFileDir = DataMessageUtils.trimValue(aDumpFileDir);
  }


  /**
   * {@inheritDoc}
   */
  @Override public void setDumpFileName(String aDumpFileName)
  {
    this.dumpFileName = DataMessageUtils.trimValue(aDumpFileName);
  }


  /**
   * set database dump information to new value
   *
   * @param aDatabaseDumpInformation
   */
  public void setDumpInformation(IDatabaseDumpInformation aDatabaseDumpInformation)
  {
    this.dumpInformation = aDatabaseDumpInformation;
  }
}
