// Planon Enterprise Edition Source file: BaseOracleExportRequest.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import nl.planon.pdm.core.common.*;

/**
 * BaseOracleExportRequest
 */
public abstract class BaseOracleExportRequest extends BaseOracleRequest
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseState getBaseStatusDatabase()
  {
    return DATABASE_STATE_AVAILABLE;
  }
}
