// Planon Enterprise Edition Source file: OracleDataPumpAnalyzeRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;

/**
 * OracleDataPumpAnalyzeRequest
 */
public class OracleDataPumpAnalyzeRequest extends BaseOracleAnalyzeRequest implements WebDAVRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleDataPumpAnalyzeRequest object.
   */
  public OracleDataPumpAnalyzeRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected PDMFileType getPDMFileType()
  {
    return PDMFileType.DATAPUMP;
  }
}
