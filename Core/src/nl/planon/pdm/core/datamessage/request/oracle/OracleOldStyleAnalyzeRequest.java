// Planon Enterprise Edition Source file: OracleOldStyleAnalyzeRequest.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;

/**
 * OracleOldStyleAnalyzeRequest
 */
public class OracleOldStyleAnalyzeRequest extends BaseOracleAnalyzeRequest implements WebDAVRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleOldStyleAnalyzeRequest object.
   */
  public OracleOldStyleAnalyzeRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override protected PDMFileType getPDMFileType()
  {
    return PDMFileType.ORA_EXP;
  }
}
