// Planon Enterprise Edition Source file: AbstractRequest.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.base;

import jade.content.*;
import jade.core.*;
import jade.domain.*;
import jade.lang.acl.*;

import java.util.*;

import nl.planon.pdm.core.*;
import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.ontology.*;

import nl.planon.util.pnlogging.*;

/**
 * AbstractRequest
 */
public abstract class AbstractRequest<DSI extends DatabaseServerInformation> implements AgentAction, IAbstractRequest, WebDAVRequest
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  protected static final String DEFAULT_PASSWORD = "Plan$QL";
  protected static final String DEFAULT_PREFIX_TABLESPACENAME = "PDM_";
  private static final PnLogger LOG = PnLogger.getLogger(AbstractRequest.class, PnLogCategory.DEFAULT);

  //~ Instance Variables ---------------------------------------------------------------------------

  protected String dumpFileDir;
  protected String dumpFileName;
  protected String schemaName;
  protected String tableSpaceName;
  protected String versionNumber;

  private boolean noImport;

  private boolean stopOnFailure;
  private DSI databaseServerInformation;
  private int databaseSize;
  private int taskTimeOut;
  // in each request the primary keys of the database, request and task and the request type are kept.
  // The reason for this is to be able to identify these values after the controller agent is restarted
  // in the time the request is executed. In this way the response message can be handled correctly.
  private Integer primaryKeyDatabase; // this one is also used in the naming conventions of the database and log files
  private Integer primaryKeyRequest;
  private Integer primaryKeyTask;
  private PDMPrivilegeType privilegesType;
  private PDMRequestType pdmRequestType;
  private String baseStatus;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get base status of database. If the request fails the database must return to this status
   *
   * @return status of database to start request from
   */
  public abstract PDMDatabaseState getBaseStatusDatabase();


  /**
   * create a PDM request message that must be sent to the PDM database Agent that will carry out
   * the request
   *
   * @param  aPDMAgent PDM database Agent to send the request message to
   *
   * @return the created request message
   */
  public ACLMessage createRequestMessage(AID aPDMAgent)
  {
    ACLMessage msgPDMRequest = new ACLMessage(ACLMessage.REQUEST);
    msgPDMRequest.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
    msgPDMRequest.setLanguage(PDMOntology.CODEC.getName());
    msgPDMRequest.setOntology(PDMOntology.ONTOLOGY.getName());
    msgPDMRequest.addReceiver(aPDMAgent);
    if (LOG.isInfoEnabled())
    {
      LOG.info("found receiver for request handling: " + aPDMAgent.getName());
    }

    // add object with needed data to message
    msgPDMRequest.addUserDefinedParameter(PDMAgentProperties.PARAM_MESSAGE_TYPE, PDMAgentProperties.MESSAGE_PDM_REQUEST);
    return msgPDMRequest;
  }


  /**
   * fill data that has to be sent along with the PDM request. Must be specifically implemented by
   * the subclasses.
   *
   * @param aMapOfFields               map of fields and their values
   * @param aDatabaseServerInformation database server specific information
   */
  public void fillData(Map<String, Object> aMapOfFields, DSI aDatabaseServerInformation)
  {
    setVersionNumber(PDMOntology.versionNumber);
    setPrimaryKeyDatabase((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMDATABASE));
    setPrimaryKeyRequest((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMREQUEST));
    setPrimaryKeyTask((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMTASK));
    setPrivilegesTypeCode(((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PRIVILEGE_TYPE)));
  }


  /**
   * gets database server information
   *
   * @return IDatabaseServerInformation
   */
  public DSI getDatabaseServerInformation()
  {
    return this.databaseServerInformation;
  }


  /**
   * get directory of dump file
   *
   * @return directory of dump file
   */
  @Override public String getDumpFileDir()
  {
    return this.dumpFileDir;
  }


  /**
   * get name of dump file
   *
   * @return name of dump file
   */
  @Override public String getDumpFileName()
  {
    return this.dumpFileName;
  }


  /**
   * get Value NoImport
   *
   * @return
   */
  public boolean getNoImport()
  {
    return this.noImport;
  }


  /**
   * get type of privilege
   *
   * @return type of privilege
   */
  public PDMPrivilegeType getPDMPrivilegeType()
  {
    return this.privilegesType;
  }


  /**
   * get type of request
   *
   * @return type of request
   */
  public PDMRequestType getPDMRequestType()
  {
    return this.pdmRequestType;
  }


  /**
   * get primary key value of corresponding pdm database
   *
   * @return primary key value of corresponding pdm database
   */
  public Integer getPrimaryKeyDatabase()
  {
    return this.primaryKeyDatabase;
  }


  /**
   * get primary key value of corresponding pdm request
   *
   * @return primary key value of corresponding pdm request
   */
  public Integer getPrimaryKeyRequest()
  {
    return this.primaryKeyRequest;
  }


  /**
   * get primary key value of corresponding pdm task
   *
   * @return primary key value of corresponding pdm task
   */
  public Integer getPrimaryKeyTask()
  {
    return this.primaryKeyTask;
  }


  /**
   * Returns the privilege type code either 'U' or 'S'
   *
   * @return String representation of PrivilegeTypeCode
   */
  public String getPrivilegesTypeCode()
  {
    return getPDMPrivilegeType().getCode();
  }


  /**
   * get code of requesttype that initiates the action. Redundant but required for ontology
   *
   * @return code of requesttype that initiates the action
   */
  public String getRequestTypeCode()
  {
    return getPDMRequestType().getCode();
  }


  /**
   * get schema name
   *
   * @return schema name
   */
  public String getSchemaName()
  {
    return this.schemaName;
  }


  /**
   * get value of stop on failure
   *
   * @return stop on failure
   */
  public boolean getStopOnFailure()
  {
    return this.stopOnFailure;
  }


  /**
   * get name of tablespace
   *
   * @return name of tablespace
   */
  public String getTableSpaceName()
  {
    return this.tableSpaceName;
  }


  /**
   * get value of timeout (in seconds) for pdm task
   *
   * @return timeout in seconds for pdm task
   */
  public int getTaskTimeOut()
  {
    return this.taskTimeOut;
  }


  /**
   * get number of the version
   *
   * @return version number
   */
  public String getVersionNumber()
  {
    return this.versionNumber;
  }


  /**
   * set database server information to new value
   *
   * @param aDatabaseServerInformation
   */
  public void setDatabaseServerInformation(DSI aDatabaseServerInformation)
  {
    assert aDatabaseServerInformation != null : "aDatabaseServerInformation should not be null";
    this.databaseServerInformation = aDatabaseServerInformation;
  }


  /**
   * set name of dump file directory
   *
   * @param aDumpFileDir dump file directory name
   */
  @Override public void setDumpFileDir(String aDumpFileDir)
  {
    this.dumpFileDir = aDumpFileDir;
  }


  /**
   * set name of dump file
   *
   * @param aDumpFileName dump file name
   */
  @Override public void setDumpFileName(String aDumpFileName)
  {
    this.dumpFileName = aDumpFileName;
  }


  /**
   * set Value NoImport
   *
   * @param aSkipImport
   */
  public void setNoImport(boolean aSkipImport)
  {
    this.noImport = aSkipImport;
  }


  /**
   * set type of request
   *
   * @param aPdmRequestType type of request
   */
  public void setPDMRequestType(PDMRequestType aPdmRequestType)
  {
    assert aPdmRequestType != null : "aPdmRequestType should not be null";
    this.pdmRequestType = aPdmRequestType;
  }


  /**
   * set primary key value of corresponding pdm database
   *
   * @param aPrimaryKeyDatabase primary key value of corresponding pdm database
   */
  public void setPrimaryKeyDatabase(Integer aPrimaryKeyDatabase)
  {
    assert aPrimaryKeyDatabase != null : "aPrimaryKeyDatabase should not be null";
    this.primaryKeyDatabase = aPrimaryKeyDatabase;
  }


  /**
   * set primary key value of corresponding pdm request
   *
   * @param aPrimaryKeyRequest primary key value of corresponding pdm request
   */
  public void setPrimaryKeyRequest(Integer aPrimaryKeyRequest)
  {
    assert aPrimaryKeyRequest != null : "aPrimaryKeyRequest should not be null";
    this.primaryKeyRequest = aPrimaryKeyRequest;
  }


  /**
   * set primary key value of corresponding pdm task
   *
   * @param aPrimaryKeyTask primary key value of corresponding pdm task
   */
  public void setPrimaryKeyTask(Integer aPrimaryKeyTask)
  {
    assert aPrimaryKeyTask != null : "aPrimaryKeyTask should not be null";
    this.primaryKeyTask = aPrimaryKeyTask;
  }


  /**
   * Sets the privilege type
   *
   * @param aPrivilegesType the privilege type to be set
   */
  public void setPrivilegesType(PDMPrivilegeType aPrivilegesType)
  {
    assert aPrivilegesType != null : "aPdmRequestType should not be null";
    this.privilegesType = aPrivilegesType;
  }


  /**
   * DOCUMENT ME!
   *
   * @param aPrivilegesTypeCode DOCUMENT ME!
   */
  public void setPrivilegesTypeCode(String aPrivilegesTypeCode)
  {
    assert aPrivilegesTypeCode != null : "aPdmRequestType should not be null";
    setPrivilegesType(PDMPrivilegeType.getPDMPrivilegeTypeByCode(aPrivilegesTypeCode));
  }


  /**
   * set code of requesttype that initiates the action (this method is required by jade, see
   * ontology)
   *
   * @param aRequestTypeCode code of requesttype that initiates the action
   */
  public void setRequestTypeCode(String aRequestTypeCode)
  {
    assert aRequestTypeCode != null : "aRequestTypeCode should not be null";
    setPDMRequestType(PDMRequestType.getPDMRequestType(aRequestTypeCode));
  }


  /**
   * set schemaname
   *
   * @param aSchemaName schemaname, can be null
   */
  public void setSchemaName(String aSchemaName)
  {
    this.schemaName = DataMessageUtils.trimValue(aSchemaName);
  }


  /**
   * set value for stop on failure
   *
   * @param aStopOnFailure value for stop on failure
   */
  public void setStopOnFailure(boolean aStopOnFailure)
  {
    this.stopOnFailure = aStopOnFailure;
  }


  /**
   * set name of tablespace
   *
   * @param aTableSpaceName name of tablespace
   */
  public void setTableSpaceName(String aTableSpaceName)
  {
    this.tableSpaceName = DataMessageUtils.trimValue(aTableSpaceName);
  }


  /**
   * set value for timeout of pdm task
   *
   * @param aTaskTimeOut value for timeout of pdm task
   */
  public void setTaskTimeOut(int aTaskTimeOut)
  {
    this.taskTimeOut = aTaskTimeOut;
  }


  /**
   * not possible to use this.
   *
   * @param aVersionNumber version number
   */
  public void setVersionNumber(String aVersionNumber)
  {
    // Since the implementation of this setter method must obey the rules of the jade ontology concept,
    // an assert is implemented to prevent setting other version number than the one from PDMOntology.
    assert PDMOntology.versionNumber.equals(aVersionNumber) : "It is only possible to set the version number of PDMOntology";
    this.versionNumber = aVersionNumber;
  }
}
