// Planon Enterprise Edition Source file: BaseMSSQLRequest.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.mssql;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;
import nl.planon.pdm.core.datamessage.request.base.*;

/**
 * MSSQLRequest
 */
public abstract class BaseMSSQLRequest extends AbstractRequest<MSSQLDatabaseServerInformation>
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private IDatabaseDumpInformation dumpInformation;
  private String databasePassword;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLRequest object.
   */
  public BaseMSSQLRequest()
  {
    super();
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseState getBaseStatusDatabase()
  {
    return DATABASE_STATE_INITIAL;
  }


  /**
   * get database password
   *
   * @return database password
   */
  public String getDatabasePassword()
  {
    return this.databasePassword;
  }


  /**
   * gets dump information
   *
   * @return IDatabaseDumpInformation
   */
  public IDatabaseDumpInformation getDumpInformation()
  {
    if (this.dumpInformation == null)
    {
      DatabaseDumpInformation info = new DatabaseDumpInformation();
      info.setDatabaseType(PDMDatabaseType.MSSQL);
      info.setFileType(PDMFileType.MSSQL_DUMP);
      this.dumpInformation = info;
    }
    return this.dumpInformation;
  }


  /**
   * set password of database
   *
   * @param aDatabasePassword database password
   */
  public void setDatabasePassword(String aDatabasePassword)
  {
    this.databasePassword = aDatabasePassword;
  }


  /**
   * set database dump information to new value
   *
   * @param aDatabaseDumpInformation
   */
  public void setDumpInformation(IDatabaseDumpInformation aDatabaseDumpInformation)
  {
    this.dumpInformation = aDatabaseDumpInformation;
  }
}
