// Planon Enterprise Edition Source file: MSSQLDeleteRequest.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.mssql;

import java.util.*;

import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.mssql.*;

/**
 * MSSQLDeleteRequest
 */
public class MSSQLDeleteRequest extends BaseMSSQLRequest implements WebDAVRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new MSSQLDeleteRequest object.
   */
  public MSSQLDeleteRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, MSSQLDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    this.setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    this.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    this.setTableSpaceName(DEFAULT_PREFIX_TABLESPACENAME + this.getSchemaName());
    this.setPrimaryKeyDatabase((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMDATABASE));

    this.setDatabaseServerInformation(aDatabaseServerInformation);
  }
}
