// Planon Enterprise Edition Source file: BaseOracleAnalyzeRequest.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import java.util.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.oracle.*;

/**
 * BaseOracleAnalyzeRequest
 */
public abstract class BaseOracleAnalyzeRequest extends BaseOracleRequest
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private IDatabaseDumpInformation dumpInformation;
  private Integer databaseLocation;

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, OracleDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    DatabaseDumpInformation dumpInformation = (DatabaseDumpInformation) getDumpInformation();

    String databasePassword = (String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PASSWORD);

    setDumpFileName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_PDMDUMPFILE));

    // In case of a reload latest exported dumpfile:
    // - the exported dumpfile must be passed as dumpfile
    // - the object owner in import must be passed as schemaname (in order to skip the analysis step)
    // - the standard prefix + object owner in import must be passed as tablespace name (also needed in order to skip the analysis step)
    if (PDMRequestType.RELOAD_EXPORT.equals(getPDMRequestType()))
    {
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
      dumpInformation.setTableSpaceNames(DEFAULT_PREFIX_TABLESPACENAME + dumpInformation.getSchemaName());
    }
    // In case of an import or a reload initial dumpfile:
    // - the regular dumpfile must be passed as dumpfile
    // - the object owner in dump must be passed as schemaname (in order to skip the analysis step)
    // - the tablespace names in dump must be passed as tablespace name (also needed in order to skip the analysis step)
    else
    {
      dumpInformation.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_DUMP));
      dumpInformation.setTableSpaceNames((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_TABLESPACENAME_IN_DUMP));
    }
    setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    setPrimaryKeyRequest((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMREQUEST));
    setDatabaseLocation((Integer) aMapOfFields.get(IRequestInformationFields.FIELD_PK_PDMDATABASE_LOCATION));
    setPDMRequestType(getPDMRequestType());

    setDatabaseServerInformation(aDatabaseServerInformation);
  }


  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseState getBaseStatusDatabase()
  {
    return DATABASE_STATE_INITIAL;
  }


  /**
   * get the location of the database
   *
   * @return location of the database
   */
  public Integer getDatabaseLocation()
  {
    return this.databaseLocation;
  }


  /**
   * gets dump information
   *
   * @return IDatabaseDumpInformation
   */
  public IDatabaseDumpInformation getDumpInformation()
  {
    if (this.dumpInformation == null)
    {
      DatabaseDumpInformation info = new DatabaseDumpInformation();
      info.setDatabaseType(PDMDatabaseType.ORACLE);
      info.setFileType(getPDMFileType());
      this.dumpInformation = info;
    }
    return this.dumpInformation;
  }


  /**
   * set the location of the database
   *
   * @param aDatabaseLocation location of the database
   */
  public void setDatabaseLocation(Integer aDatabaseLocation)
  {
    this.databaseLocation = aDatabaseLocation;
  }


  /**
   * get directory of dump file
   *
   * @param aDumpFileDir directory of dump file
   */
  @Override public void setDumpFileDir(String aDumpFileDir)
  {
    this.dumpFileDir = DataMessageUtils.trimValue(aDumpFileDir);
  }


  /**
   * get name of dump file
   *
   * @param aDumpFileName name of dump file
   */
  @Override public void setDumpFileName(String aDumpFileName)
  {
    this.dumpFileName = DataMessageUtils.trimValue(aDumpFileName);
  }


  /**
   * set database dump information to new value
   *
   * @param aDatabaseDumpInformation
   */
  public void setDumpInformation(IDatabaseDumpInformation aDatabaseDumpInformation)
  {
    this.dumpInformation = aDatabaseDumpInformation;
  }


  /**
   * get pdm file type that corresponds to this request
   *
   * @return pdm file type that corresponds to this request
   */
  protected abstract PDMFileType getPDMFileType();
}
