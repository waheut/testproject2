// Planon Enterprise Edition Source file: OracleDeleteRequest.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage.request.oracle;

import java.util.*;

import nl.planon.pdm.core.common.*;
import nl.planon.pdm.core.datamessage.*;
import nl.planon.pdm.core.datamessage.oracle.*;

/**
 * OracleDeleteRequest
 */
public class OracleDeleteRequest extends BaseOracleRequest
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new OracleDeleteRequest object.
   */
  public OracleDeleteRequest()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  @Override public void fillData(Map<String, Object> aMapOfFields, OracleDatabaseServerInformation aDatabaseServerInformation)
  {
    super.fillData(aMapOfFields, aDatabaseServerInformation);

    this.setDumpFileDir((String) aMapOfFields.get(IRequestInformationFields.FIELD_LOCATION));
    this.setSchemaName((String) aMapOfFields.get(IRequestInformationFields.FIELD_PDMDATABASE_OBJECT_OWNER_IN_IMPORT));
    this.setTableSpaceName(DEFAULT_PREFIX_TABLESPACENAME + this.getSchemaName());
    this.setDatabaseServerInformation(aDatabaseServerInformation);
  }


  /**
   * {@inheritDoc}
   */
  @Override public PDMDatabaseState getBaseStatusDatabase()
  {
    return DATABASE_STATE_AVAILABLE;
  }
}
