// Planon Enterprise Edition Source file: IDatabaseDumpInformation.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

import nl.planon.pdm.core.common.*;

/**
 * contains information of a database dump like database type, schema name, tablespaces, etc.
 *
 * @version $Revision$
 */
public interface IDatabaseDumpInformation
{
  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get type of database
   *
   * @return null or database type
   */
  public PDMDatabaseType getDatabaseType();


  /**
   * get type of dump file
   *
   * @return null or file type
   */
  public PDMFileType getFileType();


  /**
   * Gets the latest planon versions in the loaded dump file. Eg: Versions of FacilityNet, EE
   * Metadata and DB Metada.
   *
   * @return null or Planon versions
   */
  public String getPlanonDBVersionDetails();


  /**
   * get schema name of dump file
   *
   * @return null or schema name of dump file
   */
  public String getSchemaName();


  /**
   * get table space name of dump file
   *
   * @return null or comma separated list of table space names found in dump file
   */
  public String getTableSpaceNames();


  /**
   * get Version number of the dump
   *
   * @return null or database type
   */
  public String getVersionNumber();
}
