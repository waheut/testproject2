// Planon Enterprise Edition Source file: DataMessageUtils.java
// Copyright Planon 1997-2012. All Rights Reserved.
package nl.planon.pdm.core.datamessage;

/**
 * DataMessageUtils, utility class for use in datamessages
 */
public class DataMessageUtils
{
  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new DataMessageUtils object.
   */
  protected DataMessageUtils()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * trim value if defined and return result
   *
   * @param  aValue value to trim
   *
   * @return trimmed value of 1 or more characters or null if no value exists
   */
  public static String trimValue(String aValue)
  {
    if (aValue != null)
    {
      aValue = aValue.trim();
      if (aValue.isEmpty())
      {
        aValue = null;
      }
    }
    return aValue;
  }
}
