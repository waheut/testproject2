// Planon Enterprise Edition Source file: PDMException.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core;

import java.util.*;

import nl.planon.pdm.core.common.error.*;

/**
 * a uniform exception that can be thrown by a database agent
 */
public class PDMException extends Exception
{
  //~ Instance Variables ---------------------------------------------------------------------------

  private boolean isErrorForAdministrator;
  private final ErrorDefinition definition;
  private List<String> messageVariables;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMException object.
   *
   * @param aErrorDefinition Error Definition
   * @param aCause           cause of the exception
   */
  public PDMException(ErrorDefinition aErrorDefinition, Throwable aCause)
  {
    this(aErrorDefinition, false, aCause);
  }


  /**
   * Creates a new PDMException object.
   *
   * @param aErrorDefinition         Error Definition
   * @param aIsErrorForAdministrator true if error is for administrator, false if error for normal
   *                                 user
   * @param aCause                   cause of the exception
   */
  public PDMException(ErrorDefinition aErrorDefinition, boolean aIsErrorForAdministrator, Throwable aCause)
  {
    super();
    this.definition = aErrorDefinition;
    this.initCause(aCause);
    this.isErrorForAdministrator = aIsErrorForAdministrator;
  }


  /**
   * Creates a new PDMException object.
   *
   * @param aErrorDefinition         Error Definition
   * @param aIsErrorForAdministrator true if error is for administrator, false if error for normal
   *                                 user
   * @param aCause                   cause of the exception
   * @param aMessageVariables        list of variables that need to be included in the error message
   */
  public PDMException(ErrorDefinition aErrorDefinition, boolean aIsErrorForAdministrator, Throwable aCause, String... aMessageVariables)
  {
    this(aErrorDefinition, aIsErrorForAdministrator, aCause);
    if (aMessageVariables.length > 0)
    {
      List msgVarList = new ArrayList();
      for (String msgVar : aMessageVariables)
      {
        msgVarList.add(msgVar);
      }
      this.messageVariables = msgVarList;
    }
    else
    {
      this.messageVariables = Collections.emptyList();
    }
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get the verbose description of the exception!
   *
   * @return String
   */
  public String getDescription()
  {
    return this.definition.getCode() + " " + this.definition.getDefaultMessage();
  }


  /**
   * gets error definition
   *
   * @return ErrorDefinition
   */
  public ErrorDefinition getErrorDefinition()
  {
    return this.definition;
  }


  /**
   * get the list of variables that are to be included in the error message
   *
   * @return a list of variables that are to be included in the error message
   */
  public List<String> getMessageVariables()
  {
    return this.messageVariables;
  }


  /**
   * get the value of for whom the exception is meant
   *
   * @return true if this exception is for administrators, false if it is for 'normal users'
   */
  public boolean isErrorForAdministrator()
  {
    return this.isErrorForAdministrator;
  }


  /**
   * set the list of variables that are to be included in the error message
   *
   * @param aMessageVariables a list of variables that are to be included in the error message
   */
  public void setMessageVariables(List<String> aMessageVariables)
  {
    this.messageVariables = aMessageVariables;
  }
}
