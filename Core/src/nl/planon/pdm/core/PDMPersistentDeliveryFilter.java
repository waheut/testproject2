// Planon Enterprise Edition Source file: PDMPersistentDeliveryFilter.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core;

import jade.core.messaging.*;
import jade.lang.acl.*;

import nl.planon.pdm.core.ontology.*;

import nl.planon.util.pnlogging.*;

/**
 * This class defines a filter. This filter is specified on the command line and not used directly
 * in code.
 */
public class PDMPersistentDeliveryFilter implements PersistentDeliveryFilter
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(PDMPersistentDeliveryFilter.class, PnLogCategory.DEFAULT);

  private static final String VMARG_EXPIRATION_TIMEFOR_PDM_MESSAGES = "ExpirationTimeForPDMMessages";
  private static final long DEFAULT_EXPIRATION_TIME = 3600000; // default a max of 1 hour delay for retrying to send a message.

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * set the delay before expiration of sending the undeliverable messages to a default value for
   * PDM messages. During this delay periodically an attempt is made to deliver the message. When
   * the delay expires a failure notification will be sent.
   *
   * <p>All other messages will have no delay and will immediately create a failure notification
   * when they cannot be delivered.</p>
   *
   * @param  aMessage the undeliverable message
   *
   * @return the delay (in milliseconds) in which is periodically tried to deliver the message
   *         again.
   */
  @Override public long delayBeforeExpiration(ACLMessage aMessage)
  {
    long delay = NOW;
    if (aMessage.getOntology().equals(PDMOntology.getInstance().getName()))
    {
      delay = getDelayForPDMMessagesBeforeExpiration();
    }
    if (LOG.isInfoEnabled())
    {
      LOG.info("The delay for expiration of the persistent messages is " + delay + " milliseconds");
    }
    return delay;
  }


  /**
   * Get the vmargument expiration time for pdm messages (set in minutes). default = 60 minutes.
   *
   * @return expiration time for pdm messages (in milliseconds)
   */
  private static long getDelayForPDMMessagesBeforeExpiration()
  {
    String pdmExpirationTime = System.getProperty(VMARG_EXPIRATION_TIMEFOR_PDM_MESSAGES);
    if (pdmExpirationTime != null)
    {
      int result = Integer.parseInt(pdmExpirationTime);
      result = result * 1000 * 60; // convert from minutes to milleseconds
      return result;
    }
    return DEFAULT_EXPIRATION_TIME;
  }
}
