// Planon Enterprise Edition Source file: PDMUtilities.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.client;

import java.io.*;

import nl.planon.hades.basevalue.*;
import nl.planon.hades.businessmodel.*;
import nl.planon.hades.businessobjectsmethod.boms.*;
import nl.planon.hades.client.resourcelocators.*;
import nl.planon.hades.dto.*;
import nl.planon.hades.exception.*;

/**
 * PDMUtilities
 */
public final class PDMUtilities
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static PDMUtilities instance;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMUtilities object.
   */
  private PDMUtilities()
  {
    //
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * get a instance of this class
   *
   * @return PDMUtilities
   */
  public static PDMUtilities getInstance()
  {
    if (instance == null)
    {
      instance = new PDMUtilities();
    }
    return instance;
  }


  /**
   * Retrieve the BO value according to the BOType and primary key that are passed as parameters
   *
   * @param  aBOType     Type of BO for which the BOValue must be retrieved
   * @param  aPrimaryKey primary key of BO of which the BOValue must be retrieved
   *
   * @return IFOValue the BO value according to the BOType and primary key that are passed as
   *         parameters
   *
   * @throws PnErrorListException
   * @throws AuthorizationException
   */
  public IFOValue getBOValueByPrimaryKey(final IBOType aBOType, final IBaseValue aPrimaryKey) throws PnErrorListException, AuthorizationException
  {
    BOMResourceLocator bomResourceLocator = BOMResourceLocator.getInstance();
    IBOMRead readBOM = bomResourceLocator.getActiveBusinessObjectMethods(aBOType).getReadBOMEx();
    readBOM.setPrimaryKeyValue(aPrimaryKey);
    return bomResourceLocator.execute(readBOM);
  }


  /**
   * get stack trace of exception in string format
   *
   * @param  aThrowable exception
   *
   * @return stacktrace of exception in string format
   */
  public String getStackTrace(Throwable aThrowable)
  {
    StringWriter writer = new StringWriter();
    aThrowable.printStackTrace(new PrintWriter(writer));
    return writer.toString();
  }
}
