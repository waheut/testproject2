// Planon Enterprise Edition Source file: JadeUtilities.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core;

import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;

import nl.planon.pdm.core.common.*;

import nl.planon.util.pnlogging.*;

/**
 * JadeUtilities
 */
public abstract class JadeUtilities
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(JadeUtilities.class, PnLogCategory.DEFAULT);

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * deregister agent with DF (JADE Directory Facilitator, an agent that provides the 'yellow pages'
   * service)
   *
   * @param aAgent agent
   * @param aType  agent type (controller agent/db agent)
   */
  public static void deregisterAgent(final Agent aAgent, final PDMAgentType aType)
  {
    try
    {
      DFService.deregister(aAgent);
    }
    catch (FIPAException fe)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("FAILED to deregister agent with DF: " + aAgent.getAID(), fe);
      }
    }
  }


  /**
   * Fill an agents service description in order to search for agents that support these services
   *
   * @param  aType           Service type, can be null
   * @param  aName           Service name, can be null
   * @param  aPropertyValues Other property values to search for
   *
   * @return Filled in service description
   */
  public static ServiceDescription fillServiceDescription(final String aType, final String aName, final String[][] aPropertyValues)
  {
    ServiceDescription serviceDescription = new ServiceDescription();
    if (aType != null)
    {
      serviceDescription.setType(aType);
    }
    if (aName != null)
    {
      serviceDescription.setName(aName);
    }
    if (aPropertyValues != null)
    {
      for (int propertyIndex = 0; propertyIndex < aPropertyValues.length; propertyIndex++)
      {
        serviceDescription.addProperties(new Property(aPropertyValues[propertyIndex][0], aPropertyValues[propertyIndex][1]));
      }
    }

    return serviceDescription;
  }


  /**
   * find agents of given type and name (both may be null)
   *
   * @param  aAgent              agent context
   * @param  aServiceDescription service description, contains name, type and some user defined
   *                             properties for services that is searched for.
   *
   * @return all applicable agents that support the services that is asked for.
   */
  public static DFAgentDescription[] findAgents(final Agent aAgent, final ServiceDescription aServiceDescription)
  {
    DFAgentDescription template = new DFAgentDescription();
    template.addServices(aServiceDescription);
    try
    {
      DFAgentDescription[] result = DFService.search(aAgent, template);
      return result;
    }
    catch (FIPAException fe)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("FAILED to find agents: " + aServiceDescription, fe);
      }
      return new DFAgentDescription[0];
    }
  }


  /**
   * return true iff the agent is registered
   *
   * @param  aAgent agent
   * @param  aType  agent type
   *
   * @return true iff the agent is registered
   */
  public static boolean isAgentRegistered(final Agent aAgent, final PDMAgentType aType)
  {
    AID aid = aAgent.getAID();
    // find if the agent is already register -> deregister
    DFAgentDescription dfd = new DFAgentDescription();
    ServiceDescription serviceDescription = new ServiceDescription();
    serviceDescription.setName(aAgent.getAID().getLocalName());
    serviceDescription.setType(aType.getCode());
    dfd.setName(aid);
    dfd.addServices(serviceDescription);

    try
    {
      DFAgentDescription[] list = DFService.search(aAgent, dfd);
      return list.length > 0;
    }
    catch (FIPAException fe)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("FAILED check whether agent is registered: " + aid, fe);
      }
      return false;
    }
  }


  /**
   * register the agent with given type at the DF
   *
   * @param aAgent agent to register
   * @param aType  type of agent
   */
  public static void registerAgent(final Agent aAgent, final PDMAgentType aType)
  {
    registerAgent(aAgent, aType, null, null, null);
  }


  /**
   * register the agent with given type at the DF
   *
   * @param aAgent          agent to register
   * @param aType           type of agent
   * @param aDatabaseType   code of database type
   * @param aDatabaseServer code of database server on which the agent runs
   * @param aFileTypes      file type (Oracle Datapump, MSSQL dump) the agent supports
   */
  public static void registerAgent(final Agent aAgent, final PDMAgentType aType, final PDMDatabaseType aDatabaseType, final String aDatabaseServer, final PDMFileType[] aFileTypes)
  {
    if (isAgentRegistered(aAgent, aType))
    {
      if (LOG.isInfoEnabled())
      {
        LOG.info("going to de-register agent before registering it again: " + aAgent.getName());
      }
      deregisterAgent(aAgent, aType);
    }

    AID aid = aAgent.getAID();
    try
    {
      // now register the agent
      DFAgentDescription dfd = new DFAgentDescription();
      dfd.setName(aid);

      String databaseTypeCode = ((aDatabaseType != null) ? aDatabaseType.getCode() : null);
      String fileTypeCodeString = null;

      ServiceDescription sd = new ServiceDescription();
      sd.setType(aType.getCode());
      sd.setName(aid.getLocalName());
      sd.addProperties(new Property(PDMAgentProperties.PROP_DBAGENT_ID, aid.getName()));
      sd.addProperties(new Property(PDMAgentProperties.PROP_DB_TYPE, databaseTypeCode));
      sd.addProperties(new Property(PDMAgentProperties.PROP_DB_SERVER, aDatabaseServer));
      if (aFileTypes != null)
      {
        for (PDMFileType fileType : aFileTypes)
        {
          String fileTypeCode = ((fileType != null) ? fileType.getCode() : null);
          sd.addProperties(new Property(PDMAgentProperties.PROP_FILE_TYPE, fileTypeCode));
        }
      }
      dfd.addServices(sd);
      DFService.register(aAgent, dfd);

      if (LOG.isInfoEnabled())
      {
        StringBuilder sbfileTypes = new StringBuilder();
        if (aFileTypes != null)
        {
          for (int i = 0; i < aFileTypes.length; i++)
          {
            PDMFileType fileType = aFileTypes[i];
            if (i > 0)
            {
              sbfileTypes.append(", ");
            }
            sbfileTypes.append(fileType.getName());
          }
        }
        LOG.info("Registered agent with DF: " + aid //
          + "\n\t- agent type         : " + aType.getCode() //
          + "\n\t- database type      : " + ((aDatabaseType != null) ? aDatabaseType.getName() : "undefined") //
          + "\n\t- on database server : " + ((aDatabaseServer != null) ? aDatabaseServer : "undefined") //
          + "\n\t- supporting filetype: " + ((aFileTypes != null) ? sbfileTypes.toString() : "undefined") + "\n");
      }
    }
    catch (FIPAException fe)
    {
      fe.printStackTrace();
      if (LOG.isErrorEnabled())
      {
        LOG.error("FAILED to register agent with DF: " + aid, fe);
      }
    }
  }
}
