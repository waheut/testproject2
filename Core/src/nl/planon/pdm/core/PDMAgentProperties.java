// Planon Enterprise Edition Source file: PDMAgentProperties.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core;

import nl.planon.pdm.core.common.*;

/**
 * PDMDatabaseAgentProperties
 */
public class PDMAgentProperties
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  // agent properties
  public static final String PROP_DB_TYPE = "db_type"; // ORACLE, MSSQL
  public static final String PROP_FILE_TYPE = "file_type"; // ORACLE EXP, ORACLE
                                                           // DATAPUMP, MSSQL
                                                           // DUMP
  public static final String PROP_DBAGENT_ID = "sep_id";
  public static final String PROP_DB_SERVER = "db_server";
  // temporary values to keep the application going...
  public static final String DEFAULT_DB_SERVER = "NL-DEVS-TEMP";
  public static final String DEFAULT_DB_TYPE = "Oracle";
  public static final PDMFileType DEFAULT_FILE_TYPE = PDMFileType.DATAPUMP;

  // possible messages
  public static final String REQUEST_REGISTER_DBAGENT = "register";
  public static final String REQUEST_UNREGISTER_DBAGENT = "unregister";
  public static final String MESSAGE_PDM_REQUEST = "pdm_request";
  public static final String MESSAGE_KILL = "kill";

  public static final String MESSAGE_COPY_RESULT = "copy_result";
  public static final String MESSAGE_COPY_OK = "copy_ok";
  public static final String MESSAGE_COPY_NOK = "copy_not_ok";
  public static final String MESSAGE_DBTYPECHECK_RESULT = "dbtypecheck_result";
  public static final String MESSAGE_DBTYPECHECK_OK = "dbtypecheck_ok";
  public static final String MESSAGE_DBTYPECHECK_NOK = "dbtypecheck_not_ok";
  public static final String MESSAGE_PDMREQUEST_RESULT = "pdmrequest_result";
  public static final String MESSAGE_PDMREQUEST_OK = "pdmrequest_ok";
  public static final String MESSAGE_PDMREQUEST_NOK = "pdmrequest_not_ok";

  // userdefined parameters in messages
  public static final String PARAM_MESSAGE_TYPE = "paramMessageType";
}
