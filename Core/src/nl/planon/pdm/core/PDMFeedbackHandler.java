// Planon Enterprise Edition Source file: PDMFeedbackHandler.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core;

import java.io.*;
import java.text.*;
import java.util.*;

import nl.planon.pdm.core.common.error.*;

import nl.planon.util.pnlogging.*;

/**
 * FeedbackHandler for PDM. Logs all error and other messages of a database agent.
 */
public class PDMFeedbackHandler
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PDMFeedbackHandler INSTANCE = new PDMFeedbackHandler();

  private static final String DATE_FORMAT = "[HH:mm:ss] ";

  private static final PnLogger LOG = PnLogger.getLogger(PDMFeedbackHandler.class, PnLogCategory.DEFAULT);

  // The singleton instance of this PDMFeedbackHandler
  private static PDMFeedbackHandler instance;
  private static final String LIVE_LOGGING_PROPERTY = "LiveLog";

  protected static final boolean LIVE_LOGGING = Boolean.TRUE.toString().equalsIgnoreCase(System.getProperty(LIVE_LOGGING_PROPERTY, Boolean.TRUE.toString()));
  private static final String NEWLINE = "\n";

  //~ Instance Variables ---------------------------------------------------------------------------

  private final List<PDMException> pdmErrors = new ArrayList();

  private final StringBuilder reportBuilder = new StringBuilder();

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMFeedbackHandler instance.
   *
   * @return PDMFeedbackHandler instance.
   */
  public static PDMFeedbackHandler getInstance()
  {
    return INSTANCE;
  }


  /**
   * add exception to error list of error handler
   *
   * @param aPDMException the exception to add
   */
  public void addException(PDMException aPDMException)
  {
    this.pdmErrors.add(aPDMException);
  }


  /**
   * add exception to error list of error handler
   *
   * @param  aErrorDefinition  aErrorCode        error code corresponding to exception
   * @param  aThrowable        the exception to add
   * @param  aMessageVariables list of variables to put in error message
   *
   * @return the pdm exception that is added to the list
   */
  public PDMException addException(ErrorDefinition aErrorDefinition, Throwable aThrowable, String... aMessageVariables)
  {
    return addException(aErrorDefinition, false, aThrowable, aMessageVariables);
  }


  /**
   * add exception to error list of error handler
   *
   * @param  aErrorDefinition         aError                   error code corresponding to exception
   * @param  aIsErrorForAdministrator true if error should be handled by administrator, false if
   *                                  error is for user
   * @param  aThrowable               the exception to add
   * @param  aMessageVariables        list of variables to put in error message
   *
   * @return the pdm exception that is added to the list
   */
  public PDMException addException(ErrorDefinition aErrorDefinition, boolean aIsErrorForAdministrator, Throwable aThrowable, String... aMessageVariables)
  {
    PDMException pdmException = new PDMException(aErrorDefinition, aIsErrorForAdministrator, aThrowable, aMessageVariables);
    addException(pdmException);
    return pdmException;
  }


  /**
   * clear all errors and log lines of the feedbackhandler
   */
  public void clear()
  {
    this.pdmErrors.clear();
    this.reportBuilder.setLength(0);
  }


  /**
   * get all pdmerrors that are thrown during executing a pdm request
   *
   * @return a list of pdm errors
   */
  public List<PDMException> getPdmErrors()
  {
    return this.pdmErrors;
  }


  /**
   * get report builder
   *
   * @return report builder
   */
  public StringBuilder getReportBuilder()
  {
    return this.reportBuilder;
  }


  /**
   * Log a line.
   *
   * @param aLogLines The line to log. A newline character is automatically added.
   */
  public void report(String aLogLines)
  {
    DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT); // may not be static, not thread safe (findbugs)

    String timeStamp = dateFormat.format(new Date());
    String[] lines = aLogLines.split(String.valueOf(NEWLINE));
    StringBuilder builder = new StringBuilder();
    for (String line : lines)
    {
      builder.append(timeStamp).append(line).append(NEWLINE);
    }
    reportString(builder.toString());
  }


  /**
   * reports the contents of the specified text file.
   *
   * @param aFile File.
   */
  public void report(File aFile)
  {
    FileReader fileReader;
    try
    {
      fileReader = new FileReader(aFile);
    }
    catch (FileNotFoundException ex)
    {
      report("!! File " + aFile.getAbsolutePath() + " cannot be found: " + ex.getLocalizedMessage());
      return;
    }

    BufferedReader reader = new BufferedReader(fileReader);
    try
    {
      report("<------------------ START - Contents of file '" + aFile.getAbsolutePath() + "' ------------------>");

      String lineSep = System.getProperty("line.separator");
      String fileNamePrefix = "[" + aFile.getName() + "] ";
      String line;
      try
      {
        while ((line = reader.readLine()) != null)
        {
          reportString(fileNamePrefix);
          reportString(line);
          // note: BufferedReader strips the EOL character so we add a new one!
          reportString(lineSep);
        }
      }
      catch (IOException ex)
      {
        report("!! Error reading file " + aFile.getAbsolutePath() + ": " + ex.getLocalizedMessage());
      }
      report("<------------------ END   - Contents of file '" + aFile.getAbsolutePath() + "' ------------------>");
    }
    finally
    {
      try
      {
        reader.close();
      }
      catch (IOException ex)
      {
        report("!! File " + aFile.getAbsolutePath() + " cannot be closed: " + ex.getLocalizedMessage());
      }
    }
  }


  /**
   * add all generated errors to the logfile
   */
  public void reportErrors()
  {
    for (int indexPDMError = this.pdmErrors.size() - 1; indexPDMError >= 0; indexPDMError--)
    {
      final PDMException pdmError = this.pdmErrors.get(indexPDMError);
      report(MessageFormat.format(pdmError.getErrorDefinition().getCode(), pdmError.getMessageVariables().toArray()));
      if (pdmError.getErrorDefinition().isStackTraceShown())
      {
        report(getStackTraceAsString(pdmError));
      }
    }
  }


  /**
   * get stack trace of exception in string format
   *
   * @param  aThrowable exception
   *
   * @return stacktrace of exception in string format
   */
  private String getStackTraceAsString(Throwable aThrowable)
  {
    StringWriter writer = new StringWriter();
    aThrowable.printStackTrace(new PrintWriter(writer));
    return writer.toString();
  }


  /**
   * Internal low-level call to output a log line.
   *
   * @param aLogLine the log string to log.
   */
  private void reportString(String aLogLine)
  {
    // Store input in logfile
    this.reportBuilder.append(aLogLine);

    // If live logging is enabled, write out the string directly.
    if (LIVE_LOGGING)
    {
      System.out.print(aLogLine);
    }
  }
}
