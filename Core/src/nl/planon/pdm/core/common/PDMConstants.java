// Planon Enterprise Edition Source file: PDMConstants.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.common;

/**
 * PDMConstants
 */
public final class PDMConstants
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final String PRODUCT_KEY = "PDM";
  public static final String EXTENSION_DPD = "dpd";
  public static final String EXTENSION_DMP = "dmp";
  public static final String EXTENSION_BAK = "bak";
}
