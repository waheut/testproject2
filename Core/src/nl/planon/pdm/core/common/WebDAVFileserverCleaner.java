// Planon Enterprise Edition Source file: WebDAVFileserverCleaner.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.common;

import org.apache.commons.httpclient.*;

import java.io.*;

import nl.planon.util.pnlogging.*;
import nl.planon.util.webdav.*;

/**
 * utility to cleanup files stored on WebDAV Fileserver
 */
public class WebDAVFileserverCleaner
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  private static final PnLogger LOG = PnLogger.getLogger(WebDAVFileserverCleaner.class);

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new WebDAVFileserverCleaner object.
   */
  public WebDAVFileserverCleaner()
  {
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * Tries to remove a single file from a webdav location.
   *
   * @param  aFile The file to remove.
   *
   * @return True when aFile is null. True if it succeeds in the remopve proces. False if it failed.
   */
  public boolean clean(String aFile)
  {
    if (aFile == null)
    {
      return true;
    }

    if (LOG.isInfoEnabled())
    {
      LOG.info("Will try to remove file: '" + aFile + "'.");
    }

    try
    {
      PnWebDAVResource resource = new PnWebDAVResource(new HttpURL(aFile));
      boolean succesful = resource.deleteMethod();

      if (LOG.isWarnEnabled())
      {
        LOG.info("Removal of aFile: '" + aFile + "' returned: '" + succesful + "'.");
      }
      return succesful;
    }
    catch (IOException e)
    {
      if (LOG.isErrorEnabled())
      {
        LOG.error("Removal of aFile: '" + aFile + "' resulted in an error. See the stacktrace for more information.", e);
      }
      return false;
    }
  }
}
