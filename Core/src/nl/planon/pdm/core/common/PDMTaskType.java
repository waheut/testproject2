// Planon Enterprise Edition Source file: PDMTaskType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.common;

import java.io.*;

/**
 * PDMTaskType
 *
 * @version $Revision$
 */
public enum PDMTaskType implements Serializable
{
  //~ Enum constants -------------------------------------------------------------------------------

  IMPORT("PDM_TASK_IMPORT"), EXPORT("PDM_TASK_EXPORT"), DELETE("PDM_TASK_DELETE"), IMPORT_ANALYZE("PDM_TASK_IMPORT_ANALYZE");

  //~ Instance Variables ---------------------------------------------------------------------------

  private final String code;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTaskType object.
   *
   * @param aCode code of this type
   */
  private PDMTaskType(String aCode)
  {
    this.code = aCode;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMTaskType with specified code
   *
   * @param  aCode
   *
   * @return null if not existing
   */
  public static PDMTaskType getPDMTaskType(String aCode)
  {
    for (PDMTaskType pdmTaskType : PDMTaskType.values())
    {
      if (pdmTaskType.getCode().equals(aCode))
      {
        return pdmTaskType;
      }
    }
    return null;
  }


  /**
   * gets code
   *
   * @return the code
   */
  public String getCode()
  {
    return this.code;
  }


  /**
   * returns string presentation of this type (the code)
   *
   * @return the code
   */
  @Override public String toString()
  {
    return this.code;
  }
}
