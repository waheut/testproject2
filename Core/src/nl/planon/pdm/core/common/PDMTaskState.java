// Planon Enterprise Edition Source file: PDMTaskState.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.common;

/**
 * PDMTaskState
 *
 * @version $Revision$
 */
public enum PDMTaskState
{
  //~ Enum constants -------------------------------------------------------------------------------

  INITIAL("Initial"), //
  IN_PROGRESS("InProgress"), //
  OK("Ok"), //
  NOT_OK("NotOk");

  //~ Instance Variables ---------------------------------------------------------------------------

  private String pnName;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMTaskState object.
   *
   * @param aPnName name of task state
   */
  private PDMTaskState(String aPnName)
  {
    this.pnName = aPnName;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets state by pn name
   *
   * @param  aPnName PnName of state to find
   *
   * @return PDMTaskState
   */
  public static PDMTaskState getByPnName(String aPnName)
  {
    for (PDMTaskState state : PDMTaskState.values())
    {
      if (state.getPnName().equals(aPnName))
      {
        return state;
      }
    }
    return null;
  }


  /**
   * gets name of state
   *
   * @return String
   */
  public String getPnName()
  {
    return this.pnName;
  }
}
