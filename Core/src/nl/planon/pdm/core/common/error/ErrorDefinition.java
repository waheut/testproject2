// Planon Enterprise Edition Source file: ErrorDefinition.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core.common.error;

/**
 * ErrorDefinition, also update MessageTypes in ErrorNumberPDMP5Module
 */
public class ErrorDefinition
{
  //~ Static Variables & Initializers --------------------------------------------------------------

  public static final ErrorDefinition ALL_POSSIBLE_FILETYPES_TRIED = createError("0068", "ecAllPossibleFileTypesTried", "All the possible filtypes tried, import failed");
  public static final ErrorDefinition CORRUPTED_DUMP_FILE = createError("0073", "ecCorruptedDumpFile", "The dump file is corrupt.").hideStackTrace();
  public static final ErrorDefinition DATABASE_SERVER_VERSION_NOT_MATCH = createError("0054", "ecDatabaseServerVersionNotMatch", "Database Server Version {0}, dont equals with Dump Version number {1} ");
  public static final ErrorDefinition DUMP_VERSION_NOT_SUPPORT = createError("0057", "ecDumpVersionNotSupported", "database server does not support the dump version : {0}");
  public static final ErrorDefinition ERROR_ON_EXPORTING_DUMPFILE = createError("0026", "ecErrorOnExportingDumpfile", "Error on exporting dump file");
  public static final ErrorDefinition ERROR_ON_FILLING_RESPONSE_MESSAGE = createError("0020", "ecErrorOnFillingResponseMessage", "Error on filling message content for response");
  public static final ErrorDefinition ERROR_ON_IMPORTING_DUMPFILE = createError("0024", "ecErrorOnImportingDumpfile", "Error on importing Oracle Datapump dump file");
  public static final ErrorDefinition ERROR_ON_IMPORTING_DUMPFILE_MSSQL = createError("0055", "ecErrorOnImportingDumpfileMssql", "Error on importing MSSQL dump file");
  public static final ErrorDefinition ERROR_ON_IMPORTING_DUMPFILE_OLDSTYLE = createError("0053", "ecErrorOnImportingDumpfileOldStyle", "Error on importing Oracle Oldstyle dump file");
  public static final ErrorDefinition EXPORT_LOCATION_NOT_SPECIFIED = createError("0064", "ecExportLocationNotSpecified", "Location of Export executable is not specified for this database server");
  public static final ErrorDefinition FAILED_DELETING_FILE_ON_DBSERVER = createError("0044", "ecFailedDeletingFileOnDbServer", "Failed to delete original file on database server: {0}");
  public static final ErrorDefinition FAILED_INSTANTIATION_AGENT_ACTION = createError("0022", "ecFailedInstantiationAgentAction", "Error when trying to instantiate database action");
  public static final ErrorDefinition FAILED_LOGGING_TO_WEBDAV_LOCATION = createError("0023", "ecFailedLoggingToWebDAVLocation", "Error when logging output to webdav location");
  public static final ErrorDefinition FAILED_PARSING_NO_SCHEMA_FOUND = createError("0028", "ecFailedParsingNoSchemaFound", "Failed to retrieve schema/username when parsing analyse file");
  public static final ErrorDefinition FAILED_PARSING_NO_TABLESPACE_FOUND = createError("0027", "ecFailedParsingNoTablespaceFound", "Failed to retrieve tablespace name when parsing analyse file");
  public static final ErrorDefinition FAILED_RETRIEVAL_DUMPFILE = createError("0025", "ecFailedRetrievalDumpfile", "Error when trying to retrieve dumpfile from webdav location");
  public static final ErrorDefinition FAILED_UPLOADING_TO_WEBDAV_LOCATION = createError("0033", "ecFailedUploadingToWebDAVLocation", "Error while uploading file {0}: {1} - {2}");
  public static final ErrorDefinition FILE_NOT_FOUND = createError("0074", "ecFileNotFound", "File {0} not found").hideStackTrace();
  public static final ErrorDefinition FILE_TO_PARSE_NOT_FOUND = createError("0012", "ecFileToParseNotFound", "File not found when trying to parse analyze file");
  public static final ErrorDefinition IMPORT_LOCATION_NOT_SPECIFIED = createError("0065", "ecImportLocationNotSpecified", "Location of Import executable is not specified for this database server");
  public static final ErrorDefinition INTERNAL_ERROR = createError("0072", "ecInternalError", "An internal error has occurred.");
  public static final ErrorDefinition IO_ERROR_READING_ANALYZE_FILE = createError("0013", "ecIOErrorReadingAnalyzeFile", "I/O error when reading the analyze file");
  public static final ErrorDefinition MESSAGE_CONTENT_CANNOT_BE_HANDLED = createError("0019", "ecMessageContentCannotBeHandled", "Error when trying to extract the content of the request message");
  public static final ErrorDefinition MULTIPLE_SCHEMAS_FOUND_IN_DUMP = createError("0031", "ecMultipleSchemasFoundInDump", "More than one schema name and/or table space found when parsing analyse file").hideStackTrace();
  public static final ErrorDefinition NOT_MSSQL_DUMPFILE = createError("0069", "ecNotMssqlDumpfile", "The import file is not a MSSQL dump file").hideStackTrace();
  public static final ErrorDefinition NOT_ORACLE_DATAPUMPFILE = createError("0070", "ecNotOracleDataPumpfile", "The import file is not an Oracle datapump file").hideStackTrace();
  public static final ErrorDefinition NOT_ORACLE_DMP_DUMPFILE = createError("0071", "ecNotOracleDMPDumpfile", "The import file is not an Oracle old style dump file").hideStackTrace();
  public static final ErrorDefinition SQL_ERROR_ANALYZING_DB_DUMPFILE = createError("0016", "ecSQLErrorAnalyzingDBDumpfile", "SQL error when trying to analyse the database dump file");
  public static final ErrorDefinition SQL_ERROR_CLOSING_CONNECTION_DB_SERVER = createError("0015", "ecSQLErrorClosingConnectionDBServer", "SQL error when trying to close the connection to the database server");
  public static final ErrorDefinition SQL_ERROR_CONNECTING_TO_DB_SERVER = createError("0014", "ecSQLErrorConnectingToDBServer", "SQL error when trying to get connection to database server ({0})");
  public static final ErrorDefinition SQL_ERROR_CREATING_TABLESPACE = createError("0017", "ecSQLErrorCreatingTableSpace", "SQL error when trying to create schema and tablespace also there may be insufficient privileges");
  public static final ErrorDefinition SQL_ERROR_DELETING_DATABASE = createError("0030", "ecSqlErrorDeletingDatabase", "SQL error when trying to delete the database");
  public static final ErrorDefinition SQL_ERROR_DROPPING_DATABASE = createError("0050", "ecSqlErrorDroppingDatabase", "SQl error when trying to drop the database");
  public static final ErrorDefinition SQL_ERROR_IMPORTING_DB_DUMPFILE = createError("0018", "ecSQLErrorImportingDBDumpfile", "SQL error when trying to import the database dump file");
  public static final ErrorDefinition TABLESPACE_TO_SMALL_FOR_DUMP = createError("0056", "ecTablespaceToSmallForDump", "Dump size bigger then given database/tablespace size");
  public static final ErrorDefinition TIMEOUT_ERROR_ON_EXECUTING_DBTASK = createError("0038", "ecTimeoutErrorOnExecutingDbTask", "A timeout error occurred when executing the database task");
  public static final ErrorDefinition UNKNOWN_MESSAGE_CONTENT = createError("0021", "ecUnknownMessageContent", "Database action cannot be determined from request content");
  public static final ErrorDefinition USER_STILL_CONNECTED = createError("0035", "ecUserStillConnected", "User {0} still connected");

  //~ Instance Variables ---------------------------------------------------------------------------

  private boolean showStackTrace;

  private String code;
  private String defaultMessage;
  private String id;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new ErrorDefinition object.
   *
   * @param aCode           DOCUMENT ME!
   * @param aId             DOCUMENT ME!
   * @param aDefaultMessage DOCUMENT ME!
   */
  ErrorDefinition(String aCode, String aId, String aDefaultMessage)
  {
    this.showStackTrace = true;
    this.code = aCode;
    this.id = aId;
    this.defaultMessage = aDefaultMessage;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public static String getPrefix()
  {
    return "PDMS";
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public String getCode()
  {
    return "PDMS_" + this.code;
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public String getDefaultMessage()
  {
    return this.defaultMessage;
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public String getID()
  {
    return this.id;
  }


  /**
   * DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  public String getShortCode()
  {
    return this.code;
  }


  /**
   * do not report stack trace in log when error is occurring
   *
   * @return this
   */
  public ErrorDefinition hideStackTrace()
  {
    this.showStackTrace = false;
    return this;
  }


  /**
   * returns true if stack trace should be shown during error handling
   *
   * @return boolean
   */
  public boolean isStackTraceShown()
  {
    return this.showStackTrace;
  }


  /**
   * DOCUMENT ME!
   *
   * @param  aCode           DOCUMENT ME!
   * @param  aId             DOCUMENT ME!
   * @param  aDefaultMessage DOCUMENT ME!
   *
   * @return DOCUMENT ME!
   */
  private static ErrorDefinition createError(String aCode, String aId, String aDefaultMessage)
  {
    return new ErrorDefinition(aCode, aId, aDefaultMessage);
  }
}
