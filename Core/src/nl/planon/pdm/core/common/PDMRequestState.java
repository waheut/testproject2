// Planon Enterprise Edition Source file: PDMRequestState.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.common;

/**
 * PDMRequestState
 *
 * @version $Revision$
 */
public enum PDMRequestState
{
  //~ Enum constants -------------------------------------------------------------------------------

  INITIAL("Initial"), //
  IN_PROGRESS("InProgress"), //
  OK("Ok"), //
  NOT_OK("NotOk");

  //~ Instance Variables ---------------------------------------------------------------------------

  private String pnName;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestState object.
   *
   * @param aPnName name of task state
   */
  private PDMRequestState(String aPnName)
  {
    this.pnName = aPnName;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets state by pn name
   *
   * @param  aPnName PnName of state to find
   *
   * @return PDMRequestState
   */
  public static PDMRequestState getByPnName(String aPnName)
  {
    for (PDMRequestState state : PDMRequestState.values())
    {
      if (state.getPnName().equals(aPnName))
      {
        return state;
      }
    }
    return null;
  }


  /**
   * gets name of state
   *
   * @return String
   */
  public String getPnName()
  {
    return this.pnName;
  }
}
