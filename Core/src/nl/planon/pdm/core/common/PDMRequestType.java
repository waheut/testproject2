// Planon Enterprise Edition Source file: PDMRequestType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.common;

import java.io.*;

/**
 * PDMRequestType
 *
 * @version $Revision$
 */
public enum PDMRequestType implements Serializable
{
  //~ Enum constants -------------------------------------------------------------------------------

  IMPORT("0"), EXPORT("1"), DELETE("2"), RELOAD_INITIAL("3"), RELOAD_EXPORT("4");

  //~ Instance Variables ---------------------------------------------------------------------------

  private final String code;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMRequestType object.
   *
   * @param aCode code of this type
   */
  private PDMRequestType(String aCode)
  {
    this.code = aCode;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMRequestType with specified code
   *
   * @param  aCode
   *
   * @return null if not existing
   */
  public static PDMRequestType getPDMRequestType(String aCode)
  {
    for (PDMRequestType pdmRequestType : PDMRequestType.values())
    {
      if (pdmRequestType.getCode().equals(aCode))
      {
        return pdmRequestType;
      }
    }
    return null;
  }


  /**
   * gets code
   *
   * @return the code
   */
  public String getCode()
  {
    return this.code;
  }


  /**
   * Get the previous database state for current state
   *
   * @return String State
   */
  public PDMDatabaseState getPreviousDatabaseState()
  {
    PDMDatabaseState state = null;
    switch (this)
    {
      case DELETE :
      case EXPORT :
      case RELOAD_EXPORT :
      case RELOAD_INITIAL :
        state = PDMDatabaseState.AVAILABLE;
        break;
      case IMPORT :
        state = PDMDatabaseState.INITIAL;
        break;
      default :
        assert false : "unknown state";
    }
    return state;
  }


  /**
   * returns string presentation of this type (the code)
   *
   * @return the code
   */
  @Override public String toString()
  {
    return this.code;
  }
}
