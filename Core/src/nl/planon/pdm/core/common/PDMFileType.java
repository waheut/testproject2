// Planon Enterprise Edition Source file: PDMFileType.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.common;

import java.io.*;

/**
 * PDMFileType
 *
 * @version $Revision$
 */
public enum PDMFileType implements Serializable
{
  //~ Enum constants -------------------------------------------------------------------------------

  MSSQL_DUMP("M", "MSSQL Dump"), ORA_EXP("O", "Oracle old export"), DATAPUMP("D", "Oracle datapump"), DATAPUMP_AND_EXP("DandO", "Oracle datapump and old export");

  //~ Instance Variables ---------------------------------------------------------------------------

  private final String code;
  private final String name;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseType object.
   *
   * @param aCode code of this file type
   * @param aName description of this file type
   */
  private PDMFileType(String aCode, String aName)
  {
    this.code = aCode;
    this.name = aName;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMFileType with specified code
   *
   * @param  aCode The code to look for
   *
   * @return null if not existing
   */
  public static PDMFileType getPDMFileTypeByCode(String aCode)
  {
    for (PDMFileType pdmFileType : PDMFileType.values())
    {
      if (pdmFileType.getCode().equals(aCode))
      {
        return pdmFileType;
      }
    }
    return null;
  }


  /**
   * gets PDMFileType with specified name
   *
   * @param  aName The name to look for
   *
   * @return null if not existing
   */
  public static PDMFileType getPDMFileTypeByName(String aName)
  {
    for (PDMFileType pdmFileType : PDMFileType.values())
    {
      if (pdmFileType.getName().equalsIgnoreCase(aName))
      {
        return pdmFileType;
      }
    }
    return null;
  }


  /**
   * gets code
   *
   * @return the code
   */
  public String getCode()
  {
    return this.code;
  }


  /**
   * gets name
   *
   * @return the name
   */
  public String getName()
  {
    return this.name;
  }


  /**
   * returns string presentation of this type (the code)
   *
   * @return the code
   */
  @Override public String toString()
  {
    return this.code;
  }
}
