// Planon Enterprise Edition Source file: PDMDatabaseType.java
// Copyright Planon 1997-2016. All Rights Reserved.
package nl.planon.pdm.core.common;

import java.io.*;

/**
 * PDMDatabaseType. Represents all possible database types.<br>
 * Please note that this class is also used by reflection in PDMOntology
 *
 * @version $Revision$
 */
public enum PDMDatabaseType implements Serializable
{
  //~ Enum constants -------------------------------------------------------------------------------

  // ~ Enum constants

  MSSQL("M", "MSSQL", "com.microsoft.sqlserver.jdbc.SQLServerDriver", "PLN_SET_P5_SESSION", "SELECT 1"), //
  ORACLE("O", "Oracle", "oracle.jdbc.driver.OracleDriver", "BEGIN PLN_PCK_SESSION_OPTIONS.PLN_SET_P5_SESSION; END;", "SELECT 1 FROM DUAL");

  //~ Instance Variables ---------------------------------------------------------------------------

  private final String code;
  private final String driverName;
  private final String name;
  private final String newConnectionSQL;
  private final String validConnectionSQL;

  // ~ Constructors

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseType object.
   *
   * @param aCode               code of this type
   * @param aName               name of this type
   * @param aDriverName         driver name
   * @param aNewConnectionSQL   sql statement required for new connections
   * @param aValidConnectionSQL some arbitrary sql statement for checking valid connection
   */
  private PDMDatabaseType(String aCode, String aName, String aDriverName, String aNewConnectionSQL, String aValidConnectionSQL)
  {
    this.code = aCode;
    this.name = aName;
    this.driverName = aDriverName;
    this.newConnectionSQL = aNewConnectionSQL;
    this.validConnectionSQL = aValidConnectionSQL;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMDatabaseType with specified code
   *
   * @param  aCode
   *
   * @return null if not existing
   */
  public static PDMDatabaseType getPDMDatabaseTypeByCode(String aCode)
  {
    for (PDMDatabaseType pdmDatabaseType : PDMDatabaseType.values())
    {
      if (pdmDatabaseType.getCode().equals(aCode))
      {
        return pdmDatabaseType;
      }
    }
    return null;
  }


  /**
   * gets PDMDatabaseType with specified name (case-insensitive)
   *
   * @param  aName
   *
   * @return null if not existing
   */
  public static PDMDatabaseType getPDMDatabaseTypeByName(String aName)
  {
    for (PDMDatabaseType pdmDatabaseType : PDMDatabaseType.values())
    {
      if (pdmDatabaseType.getName().equalsIgnoreCase(aName))
      {
        return pdmDatabaseType;
      }
    }
    return null;
  }


  /**
   * gets code
   *
   * @return the code
   */
  public String getCode()
  {
    return this.code;
  }


  /**
   * gets driver name
   *
   * @return the driver name
   */
  public String getDriverName()
  {
    return this.driverName;
  }


  /**
   * gets name
   *
   * @return the name
   */
  public String getName()
  {
    return this.name;
  }


  /**
   * gets NewConnectionSQL to be used in data source configuration
   *
   * @return the NewConnectionSQL
   */
  public String getNewConnectionSQL()
  {
    return this.newConnectionSQL;
  }


  /**
   * gets ValidConnectionSQL to be used in data source configuration
   *
   * @return the ValidConnectionSQL
   */
  public String getValidConnectionSQL()
  {
    return this.validConnectionSQL;
  }


  /**
   * returns string presentation of this type (the code)
   *
   * @return the code
   */
  @Override public String toString()
  {
    return this.code + " - " + this.name;
  }
}
