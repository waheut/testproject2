// Planon Enterprise Edition Source file: PDMPrivilegeType.java
// Copyright Planon 1997-2017. All Rights Reserved.
package nl.planon.pdm.core.common;

import java.io.*;

/**
 * PDMPrivilegeType
 */
public enum PDMPrivilegeType implements Serializable
{
  //~ Enum constants -------------------------------------------------------------------------------

  STANDARD("S", "Standard user"), UPGRADE("U", "Upgrade user"), ADMIN("A", "Admin user");

  //~ Instance Variables ---------------------------------------------------------------------------

  private final String code;
  private final String name;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMPrivilegeType object.
   *
   * @param aCode code of this file type
   * @param aName description of this file type
   */
  private PDMPrivilegeType(String aCode, String aName)
  {
    this.code = aCode;
    this.name = aName;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets PDMPrivilegeType with specified code
   *
   * @param  aCode The code to look for
   *
   * @return null if not existing
   */
  public static PDMPrivilegeType getPDMPrivilegeTypeByCode(String aCode)
  {
    for (PDMPrivilegeType pdmPrivilegeType : PDMPrivilegeType.values())
    {
      if (pdmPrivilegeType.getCode().equals(aCode))
      {
        return pdmPrivilegeType;
      }
    }

    return null;
  }


  /**
   * gets PDMPrivilegeType with specified name
   *
   * @param  aName The name to look for
   *
   * @return null if not existing
   */
  public static PDMPrivilegeType getPDMPrivilegeTypeByName(String aName)
  {
    for (PDMPrivilegeType pdmPrivilegeType : PDMPrivilegeType.values())
    {
      if (pdmPrivilegeType.getName().equalsIgnoreCase(aName))
      {
        return pdmPrivilegeType;
      }
    }

    return null;
  }


  /**
   * gets code
   *
   * @return the code
   */
  public String getCode()
  {
    return this.code;
  }


  /**
   * gets name
   *
   * @return the name
   */
  public String getName()
  {
    return this.name;
  }


  /**
   * returns string presentation of this type (the code)
   *
   * @return the code
   */
  @Override public String toString()
  {
    return this.code;
  }
}
