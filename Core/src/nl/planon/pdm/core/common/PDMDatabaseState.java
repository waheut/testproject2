// Planon Enterprise Edition Source file: PDMDatabaseState.java
// Copyright Planon 1997-2013. All Rights Reserved.
package nl.planon.pdm.core.common;

/**
 * PDMDatabaseState
 *
 * @version $Revision$
 */
public enum PDMDatabaseState
{
  //~ Enum constants -------------------------------------------------------------------------------

  INITIAL("Initial"), //
  BUSY("Busy"), //
  AVAILABLE("Available");

  //~ Instance Variables ---------------------------------------------------------------------------

  private String pnName;

  //~ Constructors ---------------------------------------------------------------------------------

  /**
   * Creates a new PDMDatabaseState object.
   *
   * @param aPnName name of task state
   */
  private PDMDatabaseState(String aPnName)
  {
    this.pnName = aPnName;
  }

  //~ Methods --------------------------------------------------------------------------------------

  /**
   * gets state by pn name
   *
   * @param  aPnName PnName of state to find
   *
   * @return PDMDatabaseState
   */
  public static PDMDatabaseState getByPnName(String aPnName)
  {
    for (PDMDatabaseState state : PDMDatabaseState.values())
    {
      if (state.getPnName().equals(aPnName))
      {
        return state;
      }
    }
    return null;
  }


  /**
   * gets name of state
   *
   * @return String
   */
  public String getPnName()
  {
    return this.pnName;
  }
}
