-- Add locations in PLN_PDMLOCATION
Declare
  l_Counter Integer := 0;
  l_NewId   Integer := 0;
Begin
  PLN_GETNEWID('PLN_PDMLOCATION', l_NewId);
  Insert Into PLN_PDMLOCATION(SYSCODE,BEHCODE,CODE,GEBRCODE,LOCATION,NAME) Values(l_NewId,'DEMO','NL',(Select SYSCODE from PLANUSER Where V1='SYS'),'[web-file-server-url]','File server - dev NL (live env)');
  PLN_GETNEWID('PLN_PDMLOCATION', l_NewId);
  Insert Into PLN_PDMLOCATION(SYSCODE,BEHCODE,CODE,GEBRCODE,LOCATION,NAME) Values(l_NewId,'DEMO','IN',(Select SYSCODE from PLANUSER Where V1='SYS'),'http://in-devs02:18070/FileServer-IN/','File server - India');
End;
/

-- Add database vendor versions
Declare
  -- Insert record PLN_BODEFINITION for PDMDatabaseVersionOracle
  -- Insert record PLN_BODEFINITION for PDMDatabaseVersionMSSQL
  l_Counter Integer := 0;
  l_NewId   Integer := 0;
Begin  
  -- Oracle
  Select Count(*)
  Into l_Counter
  From PLN_BODEFINITION
  Where PNNAME = 'PDMDatabaseVersionOracle';
  
  If (l_Counter = 0) Then
    PLN_GETNEWID('PLN_BODEFINITION', l_NewId);
    Insert Into PLN_BODEFINITION
	  (
      SYSCODE,
      DESCRIPTION,
      ICONNAME,
      MAXPVVALUES,
      PNNAME,
      PNNAME_TRANSKEY,
      TABLENAME,
      TYPE,
      UPGRADE_GUID
    )
    Values(
      l_NewId,
      'PDMDatabaseVersionOracle',
      'BOType_PDMDatabaseVersionOracle',
      -1,
      'PDMDatabaseVersionOracle',
      'BOType.PDMDatabaseVersionOracle',
      'PLN_PDMDATABASEVERSION',
      'SYSTEM',
      '47068ADC-190D-111A-EFA8-9B42D9278A1E'
    );
  End If;

  -- MsSql
  Select Count(*)
  Into l_Counter
  From PLN_BODEFINITION
  Where PNNAME = 'PDMDatabaseVersionMSSQL';
  
  If (l_Counter = 0) Then
    PLN_GETNEWID('PLN_BODEFINITION', l_NewId);
    Insert Into PLN_BODEFINITION
    (
      SYSCODE,
      DESCRIPTION,
      ICONNAME,
      MAXPVVALUES,
      PNNAME,
      PNNAME_TRANSKEY,
      TABLENAME,
      TYPE,
      UPGRADE_GUID
    )
    Values(
      l_NewId,
      'PDMDatabaseVersionMSSQL',
      'BOType_PDMDatabaseVersionMSSQL',
      -1,
      'PDMDatabaseVersionMSSQL',
      'BOType.PDMDatabaseVersionMSSQL',
      'PLN_PDMDATABASEVERSION',
      'SYSTEM',
      '12F35D7F-FDCB-647B-71DD-D3FF8EA21CB0'
    );
  End If;
End;
/



Declare
  -- PLN_PDMDATABASEVERSION
  l_NewId            Integer := 0;
  l_OracleVersionBO  Integer := 0;
  l_MsSqlVersionBO   Integer := 0;
Begin  
  Select SYSCODE
  Into l_OracleVersionBO
  From PLN_BODEFINITION
  Where PNNAME = 'PDMDatabaseVersionOracle';

  Select SYSCODE
  Into l_MsSqlVersionBO
  From PLN_BODEFINITION
  Where PNNAME = 'PDMDatabaseVersionMSSQL';

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.02.00.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle11.02');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.01.00.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.01');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','10.02.01.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 10.2.1');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.1.0.7.0',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.1.0.7.0');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','10.02.00.05.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 10.2');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.02.00.01.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.2.0.1');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','10.02.01',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 10.02.01');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.01.00.07.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.1');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','09.02.00.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 9.2');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','09.00.00.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 9.0');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','09.00.01.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 9.0.1');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','09.02.08.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 9.2.8');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.02.01.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.2.1');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.02.02.00.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.2.2');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.02.00.02.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.2.0.2');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.02.00.03.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 11.2.0.3');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','10.02.00.04.00',l_OracleVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'Oracle 10.2.0.4');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','10.00.00.00.00',l_MsSqlVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'MSSQL 2008');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','11.00.00.00.00',l_MsSqlVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'MSSQL 2012');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','10.50.00.00.00',l_MsSqlVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'MSSQL 2008R2');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','10.00.55.00.00',l_MsSqlVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'MSSQL 2008 SP3');

  PLN_GETNEWID('PLN_PDMDATABASEVERSION', l_NewId);
  Insert Into PLN_PDMDATABASEVERSION(SYSCODE, BEHCODE,CODE,FK_BODEFINITION,GEBRCODE,NAME) Values(l_NewId, 'DEMO','09.00.40.53.00',l_MsSqlVersionBO,(Select SYSCODE from PLANUSER Where V1='SYS'),'MSSQL 2005');
End;
/


Declare
  -- Insert records PLN_BODEFINITION for PLN_PDMDATABASESERVER Oracle and Ms
  l_Counter Integer := 0;
  l_NewId   Integer := 0;
Begin  
  -- Oracle
  Select Count(*)
  Into l_Counter
  From PLN_BODEFINITION
  Where PNNAME = 'PDMDatabaseServerOracle';
  
  If (l_Counter = 0) Then
    PLN_GETNEWID('PLN_BODEFINITION', l_NewId);
    Insert Into PLN_BODEFINITION
    (
      SYSCODE,
      DESCRIPTION,
      ICONNAME,
      MAXPVVALUES,
      PNNAME,
      PNNAME_TRANSKEY,
      TABLENAME,
      TYPE,
      UPGRADE_GUID
    )
    Values(
      l_NewId,
      'PDMDatabaseServerOracle',
      'BOType_PDMDatabaseServerOracle',
      -1,
      'PDMDatabaseServerOracle',
      'BOType.PDMDatabaseServerOracle',
      'PLN_PDMDATABASESERVER',
      'SYSTEM',
      'E95CF2CD-9058-99D3-FA31-4F71D41AA395'
    );
  End If;

  -- MsSql
  Select Count(*)
  Into l_Counter
  From PLN_BODEFINITION
  Where PNNAME = 'PDMDatabaseServerMSSQL';
  
  If (l_Counter = 0) Then
    PLN_GETNEWID('PLN_BODEFINITION', l_NewId);
    Insert Into PLN_BODEFINITION
    (
      SYSCODE,
      DESCRIPTION,
      ICONNAME,
      MAXPVVALUES,
      PNNAME,
      PNNAME_TRANSKEY,
      TABLENAME,
      TYPE,
      UPGRADE_GUID
    )
    Values(
      l_NewId,
      'PDMDatabaseServerMSSQL',
      'BOType_PDMDatabaseServerOracle',
      -1,
      'PDMDatabaseServerMSSQL',
      'BOType.PDMDatabaseServerMSSQL',
      'PLN_PDMDATABASESERVER',
      'SYSTEM',
      'A2BD0A9D-C000-614A-7B83-8C745AC11EDA'
    );
  End If;
End;
/


Declare
  l_NewId   Integer := 0;
Begin  
  -- Specific inserts for database servers. 
  -- DV3112 (NL-DEVDB09)
  PLN_GETNEWID('PLN_PDMDATABASESERVER', l_NewId);
  Insert Into PLN_PDMDATABASESERVER(
    SYSCODE,
    BEHCODE,
    CODE,
    ADMIN_USER,
    ADMIN_PASS,
    CODE_PAGE,
    CONNECT_SERVER_NAME,
    CONNECT_TNS_NAME,
    DATABASE_INSTANCENAME,
    FK_BODEFINITION,
    FK_PDMDATABASEVERSION,
    FK_PDMLOCATION,
    FK_PLC_PDMFILETYPE,
    FREE01,
    GEBRCODE,
    HOST_NAME,
    IS_ACTIVE,
    NAME,
    PORT_NUMBER,
    SORT_ORDER,
    TEMPORARY_TABLESPACENAME,
    EXP_LOCATION,
    IMP_LOCATION
  )
  Values(
    l_NewId,
    'DEMO',
    'DV9112',
    'system',
    '2F10055238060D001F1C',
    Null,
    Null,
    'DV9112',
    'DV9112',
    (Select SYSCODE From PLN_BODEFINITION Where PNNAME = 'PDMDatabaseServerOracle'),
    (Select SYSCODE From PLN_PDMDATABASEVERSION Where CODE = '11.02.00.03.00'),
    (Select SYSCODE From PLN_PDMLOCATION Where CODE = 'NL'),
    (Select SYSCODE From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'DandO'),
    'Development',
    (Select SYSCODE from PLANUSER Where V1='SYS'),
    'NL-DEVDB09',
    'T',
    'NL-DEVDB09/DV9112',
    '1521',
    Null,
    'TEMP',
    'D:\Oracle2\product\11.2.0\dbhome_1\BIN\exp.exe',
    'D:\Oracle2\product\11.2.0\dbhome_1\BIN\imp.exe'
  );  

  -- Specific inserts for database servers. 
  -- IN-DEVS25\DEVS25MSSQL2012
  PLN_GETNEWID('PLN_PDMDATABASESERVER', l_NewId);
  Insert Into PLN_PDMDATABASESERVER(
    SYSCODE,
    BEHCODE,
    CODE,
    ADMIN_USER,
    ADMIN_PASS,
    CODE_PAGE,
    CONNECT_SERVER_NAME,
    CONNECT_TNS_NAME,
    DATABASE_INSTANCENAME,
    FK_BODEFINITION,
    FK_PDMDATABASEVERSION,
    FK_PDMLOCATION,
    FK_PLC_PDMFILETYPE,
    FREE01,
    GEBRCODE,
    HOST_NAME,
    IS_ACTIVE,
    NAME,
    PORT_NUMBER,
    SORT_ORDER,
    TEMPORARY_TABLESPACENAME
  )
  Values(
    l_NewId,
    'DEMO',
    'DEVS25MSSQL2012',
    'sa',
    '33115036050B02011E',
    'NotUsed',
    Null,
    Null,
    'DEVS25MSSQL2012',
    (Select SYSCODE From PLN_BODEFINITION Where PNNAME = 'PDMDatabaseServerMSSQL'),
    (Select SYSCODE From PLN_PDMDATABASEVERSION Where CODE = '11.00.00.00.00'),
    (Select SYSCODE From PLN_PDMLOCATION Where CODE = 'IN'),
    (Select SYSCODE From PLANCODE Where GRP = 'PDMFILETYPE' And CODE = 'M'),
    'Development',
    (Select SYSCODE from PLANUSER Where V1='SYS'),
    'IN-DEVS25',
    'T',
    'IN-DEVS25\DEVS25MSSQL2012',
    Null,
    '1',
    Null
  );  

End;

/


-- Specific inserts for database servers file locations. 
Declare
  l_NewId   Integer := 0;
Begin  
  -- DV9112 (NL-DEVDB09)
  PLN_GETNEWID('PLN_PDMDBSERVER_DATAFILELOC', l_NewId);
  Insert Into PLN_PDMDBSERVER_DATAFILELOC (
    SYSCODE, 
    BEHCODE, 
    FK_PDMDATABASESERVER, 
    FILE_LOCATION_LOCAL
  )
  Values(
  l_NewId,
  'DEMO',
  (Select SYSCODE From PLN_PDMDATABASESERVER Where CODE = 'DV9112'),
  'D:\Oracle2\oradata\DV9112'
  );

  -- Specific inserts for database servers file locations. 
  -- DEVS25MSSQL2012 (IN-DEVS25)
  PLN_GETNEWID('PLN_PDMDBSERVER_DATAFILELOC', l_NewId);
  Insert Into PLN_PDMDBSERVER_DATAFILELOC (
    SYSCODE, 
    BEHCODE, 
    FK_PDMDATABASESERVER, 
    FILE_LOCATION_LOCAL
  )
  Values(
  l_NewId,
  'DEMO',
  (Select SYSCODE From PLN_PDMDATABASESERVER Where CODE = 'DEVS25MSSQL2012'),
  'D:\MSSQL11.DEV25MSSQL2012\MSSQL\DATA'
  );

End; 
/

-- Specific inserts for database servers supported versions. 
Declare
  l_NewId   Integer := 0;
Begin  
  -- DV9112 (NL-DEVDB09)
  PLN_GETNEWID('PLN_PDMCOMPATIBILITYMATRIX', l_NewId);
  Insert Into PLN_PDMCOMPATIBILITYMATRIX(
    SYSCODE,
    BEHCODE,
    FK_PDMDATABASESERVER,
    FK_PDMDATABASEVERSION
  )
  Values(
    l_NewId,
    'DEMO',
    (Select SYSCODE From PLN_PDMDATABASESERVER Where CODE = 'DV9112'),
    (Select SYSCODE From PLN_PDMDATABASEVERSION Where CODE = '11.02.00.03.00')
  );

  -- DEVS25MSSQL2012 (IN-DEVS25)
  PLN_GETNEWID('PLN_PDMCOMPATIBILITYMATRIX', l_NewId);
  Insert Into PLN_PDMCOMPATIBILITYMATRIX(
    SYSCODE,
    BEHCODE,
    FK_PDMDATABASESERVER,
    FK_PDMDATABASEVERSION
  )
  Values(
    l_NewId,
    'DEMO',
    (Select SYSCODE From PLN_PDMDATABASESERVER Where CODE = 'DEVS25MSSQL2012'),
    (Select SYSCODE From PLN_PDMDATABASEVERSION Where CODE = '11.00.00.00.00')
  );

End;
/

Update PLN_LANGSETTING Set FK_LANGP5DEFINITION = (Select SYSCODE From PLN_LANGP5DEFINITION Where LANGUAGE_CODE='PLA')
/
-- Specific lauch task group /task/tsi
Declare
  l_LTGId   Integer := 0;
  l_LangLTGId  Integer := 0;
  l_LTId  Integer := 0;
  l_LangLTId  Integer := 0;
  l_AGGLTOId  Integer := 0;
Begin  
  PLN_GETNEWID('PLN_LAUNCHTASKGROUP', l_LTGId);
  Insert Into PLN_LAUNCHTASKGROUP(
    SYSCODE,
    PNNAME,
    PNNAME_TRANSKEY
  )
  Values(
     l_LTGId,
    '0000000035',
    'LTG.Default.PDM3'
  );

  PLN_GETNEWID('PLN_LANGLAUNCHTASKGROUP', l_LangLTGId);
  Insert Into PLN_LANGLAUNCHTASKGROUP(
    SYSCODE,
    FK_LANGP5DEFINITION,
    FK_LAUNCHTASKGROUP,
    TRANSLATIONSTATUS,
    TRANSLATIONTEXT
  )
  Values(
    l_LangLTGId,
    4, 
    l_LTGId,
    2,
    'PDM3'
  );

  PLN_GETNEWID('PLN_LANGLAUNCHTASKGROUP', l_LangLTGId);
  Insert Into PLN_LANGLAUNCHTASKGROUP(
    SYSCODE,
    FK_LANGP5DEFINITION,
    FK_LAUNCHTASKGROUP,
    TRANSLATIONSTATUS,
    TRANSLATIONTEXT
  )
  Values(
    l_LangLTGId,
    3, 
    l_LTGId,
    2,
    'PDM3'
  );

  PLN_GETNEWID('PLN_LANGLAUNCHTASKGROUP', l_LangLTGId);
  Insert Into PLN_LANGLAUNCHTASKGROUP(
    SYSCODE,
    FK_LANGP5DEFINITION,
    FK_LAUNCHTASKGROUP,
    TRANSLATIONSTATUS,
    TRANSLATIONTEXT
  )
  Values(
    l_LangLTGId,
    2, 
    l_LTGId,
    2,
    'PDM3'
  );

  PLN_GETNEWID('PLN_LAUNCHTASK', l_LTId);
  Insert Into PLN_LAUNCHTASK(
    SYSCODE,
    FK_LAUNCHTASKGROUP,
    ICONNAME,
    INTEGERVALUE,
    PNNAME,
    PNNAME_TRANSKEY,
    TASKTYPE   
  )
  Values(
    l_LTId,
    l_LTGId,
    'Navigator_LaunchTaskTSI',
    (Select SYSCODE From PLN_TSIENTRY Where DESCRIPTION = 'TSI-PDMAdminModuleEntry'),
    (Select SYSCODE From PLN_TSIENTRY Where DESCRIPTION = 'TSI-PDMAdminModuleEntry'),
    'Task.Default.PDM3',
    'TSI'
  );

  PLN_GETNEWID('PLN_LANGLAUNCHTASK', l_LangLTId);
  Insert Into PLN_LANGLAUNCHTASK(
    SYSCODE,
    FK_LANGP5DEFINITION,
    FK_LAUNCHTASK,
    TRANSLATIONSTATUS,
    TRANSLATIONTEXT
  )
  Values(
    l_LangLTId,
    4, 
    l_LTId,
    2,
    'PDM3'
  );

  PLN_GETNEWID('PLN_LANGLAUNCHTASK', l_LangLTId);
  Insert Into PLN_LANGLAUNCHTASK(
    SYSCODE,
    FK_LANGP5DEFINITION,
    FK_LAUNCHTASK,
    TRANSLATIONSTATUS,
    TRANSLATIONTEXT
  )
  Values(
    l_LangLTId,
    3, 
    l_LTId,
    2,
    'PDM3'
  );

  PLN_GETNEWID('PLN_LANGLAUNCHTASK', l_LangLTId);
  Insert Into PLN_LANGLAUNCHTASK(
    SYSCODE,
    FK_LANGP5DEFINITION,
    FK_LAUNCHTASK,
    TRANSLATIONSTATUS,
    TRANSLATIONTEXT
  )
  Values(
    l_LangLTId,
    2, 
    l_LTId,
    2,
    'PDM3'
  );
  

 PLN_GETNEWID('PLN_ACCOUNTGROUPLTO', l_AGGLTOId);
  Insert Into PLN_ACCOUNTGROUPLTO(
    SYSCODE,
    FK_ACCOUNTGROUP,
    FK_LAUNCHTASKGROUP
  )
  Values(
    l_AGGLTOId,
    2,
    l_LTGId
  );
End;
/
